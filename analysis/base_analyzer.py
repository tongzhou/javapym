#! /usr/bin/env python

# set PYTHONPATH
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from lib.globals import *
from lib.utils import *
from lib.exp import *
from lib.report import *
print "Load pymcolor globals, utils, exp, and report success"

import re
import regex
import numpy as np
from scipy import stats as scistats
import pprint
from tabulate import tabulate
import traceback
import copy
import json
import textwrap
import xlsxwriter

class ParsingException(Exception):
    pass

class BaseAnalyzer:
    # static
    pprint = pprint.PrettyPrinter(indent=4)
    
    def __init__(self):
        self.xlsx_table = []
        self.xlsx_header = ''
        self.stacked_table = []
        self.header = []
        self.benches = []
        self.bench_num = 0
        self.bench_name = ''
        self.ylogbase = -1
        self.failed = []

        self.workbook = None
        self.worksheet = None
        self.chart = None
        self.sheet_num = 0
        self.chart_fn = ''
        self.chart_type = None
        self.chart_title = ''
        self.chart_ylabel = ''
        self.table_fn = ''
        self.totext_fn = ''

        self.overrideResult = True

    def help(self):
        pass

    def add_mean_row(self, mean="geometric"):
        column_num = len(self.xlsx_table[0])
        row = ['geomean']
        print 'xlsx table:', self.xlsx_table
        for col in range(1, column_num):
            cols = map(lambda x: x[col], self.xlsx_table)
            row.append(geomean(cols))
        self.xlsx_table.append(row)

    def analyze_benches(self, routine):
        for b in self.benches:
            routine(b)
        self.add_mean_row()

    def to_text(self):
        print 'write to %s' % self.totext_fn
        with open(self.totext_fn, 'w') as fout:
            for ea in self.xlsx_table:
                print >> fout, ea

    def add_xlsx_textbox(self, text):
        row = 9
        col = 9

        # The examples below show different textbox options and formatting. In each
        # example the text describes the formatting.

        self.worksheet.insert_textbox(row, col, text)
        
    def add_xlsx_table(self):
        # # Create a workbook and add a worksheet.

        # for row, line in enumerate(self.xlsx_table):
        #     for col, field in enumerate(line):
        #         worksheet.write(row, col, field)
        #     row += 1

        if self.workbook is None:
            print 'write to %s' % self.table_fn
            self.workbook = xlsxwriter.Workbook(self.table_fn)

        worksheet = self.workbook.add_worksheet()
        self.sheet_num += 1

        worksheet.write_row('A1', self.xlsx_header)
        for row, line in enumerate(self.xlsx_table):
            for col, field in enumerate(line):
                worksheet.write(row+1, col, field)


    def set_chart_type(self, typename='stack'):
        if typename == 'column':
            self.chart_type = {'type': 'column'}
        elif typename == 'stack':
            self.chart_type = {'type': 'column', 'subtype': 'stacked'}
        else:
            print 'unknown chart type, exit'
            exit(0)
            
    def set_chart_meta(self, typename='stack', title='title', ylabel='y axis', ylogbase=-1):
        self.set_chart_type(typename)
        self.chart_title = title
        self.chart_ylabel = ylabel
        self.ylogbase = ylogbase

    def reset_one_round(self):
        self.xlsx_table = []
        self.failed = []
    
    def add_xlsx_chart(self):
        if self.workbook is None:
            if not self.overrideResult:
                timestr = time.strftime("%Y%m%d-%H%M%S")
                self.chart_fn.replace('.xlsx', '')
                self.chart_fn += '-' + timestr + '.xlsx'
                
            self.workbook = xlsxwriter.Workbook(self.chart_fn)
            print 'write to %s' % self.chart_fn
            
        self.worksheet = self.workbook.add_worksheet()
        self.sheet_num += 1

        self.worksheet.write_row('A1', self.xlsx_header)
        for row, line in enumerate(self.xlsx_table):
            for col, field in enumerate(line):
                self.worksheet.write(row+1, col, field)

        #######################################################################
        #
        # Create a percentage stacked chart sub-type.
        #
        #chart = self.workbook.add_chart({'type': 'column', 'subtype': 'stacked'})

        
        self.chart = self.workbook.add_chart(self.chart_type)

        series_num = len(self.xlsx_table[0]) - 1
        series_cols = map(lambda x: chr(ord('B')+x), range(series_num))

        for i, col in enumerate(series_cols):
            color = ''
            transparency = 0
            if 'blue' in self.xlsx_header[i+1]:
                color = '#3498DB'
            elif 'red' in self.xlsx_header[i+1]:
                color = '#EC644B'

            if 'survived' in self.xlsx_header[i+1]:
                print 'set transparency'
                transparency = 60
                
            series_options = {
                'name':       '=Sheet%d!$%s$1' % (self.sheet_num, col),
                'categories': '=Sheet%d!$A$2:$A$%d' % (self.sheet_num, self.bench_num+2),
                'values':     '=Sheet%d!$%s$2:$%s$%d' % (self.sheet_num, col, col, self.bench_num+2)
            }
            if color != '':
                series_options['fill'] = {'color': color, 'transparency': transparency}
                
            self.chart.add_series(series_options)

        self.chart.set_title({'name': self.chart_title})
        self.chart.set_x_axis({'name': self.bench_name})
        # self.chart.set_y_axis({'name': self.chart_ylabel, 'max': 1.0})

        yoptions = {'name': self.chart_ylabel}
        # yoptions['max'] = 1.0
        if self.ylogbase != -1:
            yoptions['log_base'] = self.ylogbase
        self.chart.set_y_axis(yoptions)

        # Set an Excel chart style.
        self.chart.set_style(13)
        self.chart.set_size({'x_scale': 2, 'y_scale': 2})
        # Insert the chart into the worksheet (with an offset).
        self.worksheet.insert_chart('D20', self.chart, {'x_offset': 10, 'y_offset': 10})
        
    # explicitly call __clean__ instead of using __del__
    def __clean__(self):
        if self.workbook is not None:
            self.workbook.close()
        
