#! /usr/bin/env python

from base_analyzer import *

class DacapoAnalyzer(BaseAnalyzer):
    def __init__(self):
        BaseAnalyzer.__init__(self)
        self.allbenches = [
            "pmd",
            "fop",
            "lusearch",
            "xalan",
            "luindex",
            "tradebeans",
            "sunflow",
            "eclipse",
            "jython",
            "avrora",
            "batik",
            "h2",
            "tomcat",
            "tradesoap",
        ]
        self.bench_name = 'DaCapo-9.12-bach'
        self.xlsx_header = []
        self.parse_argv()
        self.create_rst_dir()
        
        self.chart_fn = '%schart_%s.xlsx' % (self.rst_dir, self.bench_sz)
        self.table_fn = '%stable_%s.xlsx' % (self.rst_dir, self.bench_sz)
        self.totext_fn = '%stext_%s.txt' % (self.rst_dir, self.bench_sz)

        

    def create_rst_dir(self):
        self.rst_dir = analysisdir + self.rst_dir

        if not os.path.exists(self.rst_dir):
            os.makedirs(self.rst_dir)
        
    def parse_argv(self):
        parsing_optional = True
        arg_num = len(sys.argv)
        p = 1

        if arg_num == 1 or (arg_num == 2 and sys.argv[1] in ('-h', '--help')):
            self.help()
            exit(0)

        # default values
        self.bench_sz = 'small'
        self.rst_dir = 'results/' + sys.argv[0].split('/')[-1][:-3] + '/'
        excludes = []
        while p < arg_num:
            ar = sys.argv[p]
            if ar in ['-ca', '--scratch']:
                self.rst_dir += 'scratch/'
            elif ar == '-de' or ar == '--default':
                self.bench_sz = 'default'
            elif ar == '-sm' or ar == '--small':
                self.bench_sz = 'small'
            elif ar in ['-ex', '--exclude']:
                p += 1
                excludes.append(sys.argv[p])
            elif ar in ['-no', '--no-overide']:
                self.overrideResult = False
            else:
                self.benches.append(ar)
            p += 1

        if 'all' in self.benches:
            self.benches = self.allbenches

        for b in excludes:
            self.benches.remove(b)
        self.benches = [b + '-' + self.bench_sz for b in self.benches]
        self.bench_num = len(self.benches)
        print self.benches

    def __clean__(self):
        BaseAnalyzer.__clean__(self)
        
    def help(self):
        print textwrap.dedent('''
            Usage: ./script [option]... [bench]...
            Run the analysis script on the given benchmarks.
            Arguments.
                -ca, --scratch                            Use a scratch directory to store results, which defaults to scriptname_results.
                -de, --default                            Use default size benches. Bench size defaults to small.
                -sm, --small                              Use small size benches. Default option.
                -ex, --exclude bench                      Exclude the specified bench.
                -no, --no-override                        Does not override the previous result. Defaults to override.
              
            Example:
                ./script pmd # small size pmd
                ./script pmd fop
                ./script all
                ./script -de all # default size all
                ./script -de -ex -no pmd all
        ''')

if __name__ == '__main__':
    ana = DacapoAnalyzer()
    ana.parse_argv()
    ana.__clean__()
