#! /usr/bin/env python

from base_dacapo import *

class APColorAnalyzer(DacapoAnalyzer):
    def __init__(self):
        DacapoAnalyzer.__init__(self)
        self.survives = []
        #self.xlsx_header = ['benches', 'red all', 'red survived', 'blue all', 'blue survived']
        #self.set_chart_meta('column', 'Allocated and survived objects in eden space', 'KB')

        
    def parseRawRatio(self, bench, config=TZ_COLOR_AP):
        readGCBlock = False
        rawout = rawoutfile(bench, config, 0)
        GCBlock = ''
        GCBeforeBlock = ''
        GCAfterBlock = ''
        readGCBefore = 0
        readGCAfter = 0
        colornum = 2
        eden_alloc = [0.0] * colornum
        eden_survived = [0.0] * colornum

        for line in open(rawout):
            if 'A fatal error has been detected' in line:
                raise ParsingException("error occured when parsing raw.out")
                
            if line.strip() == 'finish printing heap':
                #eden_before = self.matchAllocEdenSpace(GCBeforeBlock)
                
                self.survives = []
                GCBeforeBlock = ''
                GCAfterBlock = ''
                readGCBefore = 0
                readGCAfter = 0
                continue

            if 'Heap before GC' in line:
                readGCBefore = 1

            if readGCBefore:
                GCBeforeBlock += line

            if 'Heap after GC' in line:
                readGCAfter = 1

            if readGCAfter:
                GCAfterBlock += line

            if 'running GC' in line:
                readGCBefore = 0
                readGCAfter = 0

            if 'promoted from eden space' in line:
                eden_before = self.matchAllocEdenSpace(GCBeforeBlock)
                matchline = re.match(r"promoted from eden space: (\d+)K.*(\d+)K.+\n", line)
                sur_red, sur_blue = matchline.groups()
                eden_alloc[0] += eden_before[0]
                eden_alloc[1] += eden_before[1]
                eden_survived[0] += float(sur_red)
                eden_survived[1] += float(sur_blue)
                
        ratios = [0] * colornum
        for i in range(colornum):
            if eden_survived[i] != 0:
                ratios[i] = eden_survived[i] / eden_alloc[i]
            else:
                ratios[i] = 0
        self.xlsx_table.append([bench] + ratios)
        self.xlsx_header = ['benches', 'red survivor ratio', 'blue survivor ratio']
        self.set_chart_meta('column', 'Allocated and survived objects in eden space', 'ratio')
        

    def parseRaw(self, bench, config=TZ_COLOR_AP):
        readGCBlock = False
        rawout = rawoutfile(bench, config, 0)
        GCBlock = ''
        GCBeforeBlock = ''
        GCAfterBlock = ''
        readGCBefore = 0
        readGCAfter = 0
        eden_alloc = [0.0] * 2
        eden_survived = [0.0] * 2

        for line in open(rawout):
            if 'A fatal error has been detected' in line:
                raise ParsingException("error occured when parsing raw.out")
                
            if line.strip() == 'finish printing heap':
                #eden_before = self.matchAllocEdenSpace(GCBeforeBlock)
                
                self.survives = []
                GCBeforeBlock = ''
                GCAfterBlock = ''
                readGCBefore = 0
                readGCAfter = 0
                continue

            if 'Heap before GC' in line:
                readGCBefore = 1

            if readGCBefore:
                GCBeforeBlock += line

            if 'Heap after GC' in line:
                readGCAfter = 1

            if readGCAfter:
                GCAfterBlock += line

            if 'running GC' in line:
                readGCBefore = 0
                readGCAfter = 0

            if 'promoted from eden space' in line:
                eden_before = self.matchAllocEdenSpace(GCBeforeBlock)
                matchline = re.match(r"promoted from eden space: (\d+)K.*(\d+)K.+\n", line)
                sur_red, sur_blue = matchline.groups()
                eden_alloc[0] += eden_before[0]
                eden_alloc[1] += eden_before[1]
                eden_survived[0] += int(sur_red)
                eden_survived[1] += int(sur_blue)
                self.survives.append(eden_before + [int(sur_red), int(sur_blue)])
        #self.xlsx_table.append([bench] + eden_alloc + eden_survived)
        self.xlsx_table.append([bench, eden_alloc[0]/1024, eden_survived[0]/1024, eden_alloc[1]/1024, eden_survived[1]/1024])
        self.xlsx_header = ['benches', 'red all', 'red survived', 'blue all', 'blue survived']
        self.set_chart_meta('column', 'Allocated and survived objects in eden space', 'KB', 10)

        
        # with open(rawout) as fin:
        #     line = fin.readline()
        #     while line != "":
        #         if 'Heap before GC' in line.strip():                    
        #             while 'Heap after GC' not in line.strip():
        #                 GCBeforeBlock += line
        #                 line = fin.readline()
                    
        #             while line != "\n":
        #                 GCAfterBlock += line
        #                 line = fin.readline()

        #             if GCBeforeBlock == '':
        #                 print 'line:', line

        #             # process block
        #             self.survives.append(self.matchPrintHeap(GCBeforeBlock, GCAfterBlock))
        #             GCBeforeBlock = ''
        #             GCAfterBlock = ''
                    
        #         line = fin.readline()

        
        # red_total = sum(map(lambda x: x[0], self.survives))
        # red_survived = sum(map(lambda x: x[2], self.survives))
        # blue_total = sum(map(lambda x: x[1], self.survives))
        # blue_survived = sum(map(lambda x: x[3], self.survives))
        # red_surv_ratio = red_survived/float(red_total)
        # blue_surv_ratio = blue_survived/float(blue_total)
        # print 'red/blue:', red_surv_ratio, blue_surv_ratio        
        # self.xlsx_table.append([bench] + [red_surv_ratio, blue_surv_ratio])

        
    def matchPrintHeap(self, btext, atext):
        pattern = r'Heap (?P<pos>.*) GC invocations=(?P<invoke_id>\d+).*\n'\
        ' *PSYoungGen *total \d+K, used \d+K.*\n'\
        ' *eden *space \d+K, \d+% used \(\d+K\).*\n'\
        ' *color red space \d+K, \d+% used \((?P<eden_red>\d+)K\).*\n'\
        ' *color blue space \d+K, \d+% used \((?P<eden_blue>\d+)K\).*\n'\
        ' *from *space \d+K, \d+% used \(\d+K\).*\n'\
        ' *color red space \d+K, \d+% used \((?P<from_red>\d+)K\).*\n'\
        ' *color blue space \d+K, \d+% used \((?P<from_blue>\d+)K\).*\n'\
        ' *to *space \d+K, \d+% used \(\d+K\).*\n'\
        ' *color red space \d+K, \d+% used \((?P<to_red>\d+)K\).*\n'\
        ' *color blue space \d+K, \d+% used \((?P<to_blue>\d+)K\).*\n'
        
        for match in regex.finditer(pattern, btext):
            mdict =  match.groupdict()
            assert mdict['pos'] == 'before'
            for key in mdict:
                if key != 'pos':
                    mdict[key] = int(mdict[key])
            #alloc_size = mdict['eden_red'] + mdict['eden_blue']
            #eden_plus_from = mdict['from_red'] + mdict['from_blue']
            red_before = mdict['eden_red'] + mdict['from_red']
            blue_before = mdict['eden_blue'] + mdict['from_blue']
            
            
        for match in regex.finditer(pattern, atext):
            mdict =  match.groupdict()
            assert mdict['pos'] == 'after'
            for key in mdict:
                if key != 'pos':
                    mdict[key] = int(mdict[key])
            #alloc_size = mdict['eden_red'] + mdict['eden_blue']
            #eden_plus_from = mdict['from_red'] + mdict['from_blue']
            red_after = mdict['from_red']
            blue_after = mdict['from_blue']
            print [red_before, blue_before, red_after, blue_after]
            return [red_before, blue_before, red_after, blue_after]

    def setChartExtra(self):
        # set self.chart here
        
        # self.chart.set_y_axis({'log_base': 10, 'name': 'KB'})
        pass

    def matchAllocEdenSpace(self, btext):
        pattern = r'Heap (?P<pos>.*) GC invocations=(?P<invoke_id>\d+).*\n'\
        ' *PSYoungGen *total \d+K, used \d+K.*\n'\
        ' *eden *space \d+K, \d+% used \(\d+K\).*\n'\
        ' *color red space \d+K, \d+% used \((?P<eden_red>\d+)K\).*\n'\
        ' *color blue space \d+K, \d+% used \((?P<eden_blue>\d+)K\).*\n'\
        ' *from *space \d+K, \d+% used \(\d+K\).*\n'\
        ' *color red space \d+K, \d+% used \((?P<from_red>\d+)K\).*\n'\
        ' *color blue space \d+K, \d+% used \((?P<from_blue>\d+)K\).*\n'\
        ' *to *space \d+K, \d+% used \(\d+K\).*\n'\
        ' *color red space \d+K, \d+% used \((?P<to_red>\d+)K\).*\n'\
        ' *color blue space \d+K, \d+% used \((?P<to_blue>\d+)K\).*\n'
        
        for match in regex.finditer(pattern, btext):
            mdict =  match.groupdict()
            assert mdict['pos'] == 'before'
            for key in mdict:
                if key != 'pos':
                    mdict[key] = int(mdict[key])
            return [mdict['eden_red'], mdict['eden_blue']]
            
    def generate_color_file(self, bench, ESCAPE=True):
        vm_num = 0.0
        app_num = 0.0
        unused_num = 0.0
        esc_num = 0.0
        tiers = 3

        app_size = 0.0
        target_size = [0.0] * tiers
        target_size_ratio = [0.0] * tiers
        target_num = [0.0] * tiers
        target_num_ratio = [0.0] * tiers
        unused_size = 0.0
        esc_size = 0.0
        
        totalmp = {}
        vmmp = {}
        appmp = {}
        
        info = getObjAddrTable(bench, AP_TLAB_INTERVAL)
        
        escaped_aps = []
        if ESCAPE:
            escaped_aps = getEscapedAP(bench, AP_TLAB_INTERVAL)
            #print 'esc:', escaped_aps

        max_poid = 0
        max_apid = 0
        for oneval in info:
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple
                if poid > max_poid:
                    max_poid = poid

                if apid > max_apid:
                    max_apid = apid

        #print 'max poid:', max_poid, 'max apid:', max_apid
                
        appobjmp = [None] * (max_poid+1)        
        for val, oneval in enumerate(info):
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple

                if apid == -1 or poid == -1:
                    continue
                if apid in escaped_aps:
                    esc_num += 1
                    esc_size += size
                    continue
                if ref == -1:
                    vm_num += 1
                else:
                    # if new
                    if appobjmp[poid] is None:
                        pattern = 1
                        allocval = val
                        appobjmp[poid] = [apid, size, ref, allocval, pattern]
                    # if not new
                    else:
                        appobjmp[poid][2] += ref
                        if val == appobjmp[poid][3] + 1: # if used in the second interval
                            appobjmp[poid][-1] = 2       # change pattern to 2
                        else:
                            appobjmp[poid][-1] = -1

        
        apmp = [None] * (max_apid+1) 
        ################## apmap structure #####################
        # apid => [
        #           pattern,
        #           total size,
        #           total objects number,
        #           target size,
        #           target number (number of pattern 1 objects),
        #           #(target size)/#(total size),
        #         ]
        # The last ratio is calculated in the second pass

        ################### ap pattern ####################
        #  1: 100% type 1 objects
        #  2: 98% type 1 objects
        #  3: 95% type 1 objects
        # -1: not aobve
        
        
        ap_cnt = 0
        
        for poid, v in enumerate(appobjmp):
            if v is None:
                continue
            apid, size, ref, allocval, pattern = v
            app_size += size

            if apmp[apid] is None:                    
                apmp[apid] = [1, 0, 0, 0, 0, None]
                
            apmp[apid][1] += size
            apmp[apid][2] += 1
            
            if pattern == 1:
                apmp[apid][3] += size
                apmp[apid][4] += 1


        ap_num = 0
        debug_size1 = 0
        color_file_dir = '/home/tzhou/projects/mm9/hotspot9/results/%s/' % (bench)
        if not os.path.exists(color_file_dir):
            os.makedirs(color_file_dir)
        
        fouts = [open(color_file_dir+'.apc-cmds%d.txt'%i, 'w') for i in range(tiers)]
        apnamemp = allocPointMap(bench, AP_TLAB_INTERVAL, 0)

        for i, v in enumerate(apmp):
            if v is None:
                continue

            ap_num += 1
            pattern_v, total_size_v, total_num_v, target_size_v, target_num_v, ratio_v = v
            v[-1] = target_size_v/float(total_size_v)
            #print target_size_v, float(total_size_v)
            
            if v[-1] == 1:
                v[0] = 1
            elif v[-1] > 0.98:
                v[0] = 2
            elif v[-1] > 0.95:
                v[0] = 3
            else:
                v[0] = -1

            apcolor = 1
            apname = apnamemp[i]['NAME']
            # for i in range(1, 1+tiers):
            #     if v[0] == i:
            #         for j in range(i):
            #             print "write to cmd[%d] a %d" % (j, 1)
            #             print >> fouts[j], 'apcolor %s %s' % (apname, 1)
            #         #target_size[i-1] += target_size_v

            # if v[0] == -1:
            #     for i in range(tiers):
            #         print "write to cmd[%d] a %d" % (i, 0)
            #         print >> fouts[i], 'apcolor %s %s' % (apname, 0)


            
            if v[0] == 1:
                print >> fouts[0], 'apcolor %s %s' % (apname, 1)
                print >> fouts[1], 'apcolor %s %s' % (apname, 1)
                print >> fouts[2], 'apcolor %s %s' % (apname, 1)
            elif v[0] == 2:
                print >> fouts[0], 'apcolor %s %s' % (apname, 0)
                print >> fouts[1], 'apcolor %s %s' % (apname, 1)
                print >> fouts[2], 'apcolor %s %s' % (apname, 1)
            elif v[0] == 3:
                print >> fouts[0], 'apcolor %s %s' % (apname, 0)
                print >> fouts[1], 'apcolor %s %s' % (apname, 0)
                print >> fouts[2], 'apcolor %s %s' % (apname, 1)
            else:
                print >> fouts[0], 'apcolor %s %s' % (apname, 0)
                print >> fouts[1], 'apcolor %s %s' % (apname, 0)
                print >> fouts[2], 'apcolor %s %s' % (apname, 0)
            
        
        for i in range(tiers):
            target_size_ratio[i] = target_size[i]/app_size

        print "target_size_ratio", target_size_ratio
        # self.xlsx_header = ['benchmark', '100%', '98%', '95%']
        # self.xlsx_table.append([bench] + target_size_ratio)
        for each in fouts:
            each.close()


    def do_mutilpe(self):
        for b in self.benches:
            self.analyze_ap(b)

    def run(self):
        for b in self.benches:
            self.generate_color_file(b)

    def do_one_config(self, config=TZ_COLOR_AP):
        for b in self.benches:
            print b
            #self.generate_color_file(b)
            try:
                self.parseRaw(b, config=config)
            except ParsingException as pe:
                self.failed.append(b)
        
        self.add_mean_row()
        self.setChartExtra()
        self.add_xlsx_chart()
        
        if len(self.failed) != 0:
            self.add_xlsx_textbox("failed:\n"+'\n'.join(self.failed))
        print 'failed:', self.failed
        self.reset_one_round()

    def do_mul_thresh(self, configs=[TZ_COLOR_AP]):
        for cfg in configs:
            self.do_one_config(cfg)
            
    
    def do_escape(self, rountine):
        print 'escape analysis off'
        for b in self.benches:
            print b
            rountine(b, ESCAPE=False)
        self.add_mean_row()
        self.add_xlsx_chart(title='Unused Ratio With Escape Analysis Off', ylabel='Size Percentage')
        self.xlsx_table = []
        print 'escape analysis on'
        for b in self.benches:
            print b
            rountine(b, ESCAPE=True)
        self.add_mean_row()
        self.add_xlsx_chart(title='Unused Ratio With Escape Analysis On', ylabel='Size Percentage')

    
    def __clean__(self):
        DacapoAnalyzer.__clean__(self)

        
if __name__ == '__main__':
    ana = APColorAnalyzer()
    # ana.analyze_benches(ana.analyze_ap)
    # ana.to_stacked_bar(title='Unused Analysis', ylabel='Size Percentage')

    #ana.do_escape(ana.analyze_ap)
    #ana.do_rank()
    ana.do_mul_thresh(configs=[TZ_COLOR_AP, TZ_COLOR_AP_98, TZ_COLOR_AP_95])
    
    #ana.do_escape(ana.analyze_ap_tiered)
    # for bench in ana.benches:
    #     ana.analyze_obj(bench)
        
    #     #ana.testescape(bench)

    # ana.analyze_benches(ana.analyze_obj)
    # ana.add_xlsx_chart(title='Unused Ratio With Escape Analysis Off', ylabel='Size Percentage')
    # ana.add_xlsx_chart(title='Unused Ratio With Escape Analysis On', ylabel='Size Percentage')

    #ana.do_obj()

    ## always call __clean__()
    ana.__clean__()
