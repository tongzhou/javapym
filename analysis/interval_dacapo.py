#! /usr/bin/env python

from base_dacapo import *
from base_dacapo import DacapoAnalyzer

class UnusedapAnalyzer(DacapoAnalyzer):
    def analyze_obj(self, bench, ESCAPE=True):
        vm_num = 0.0
        app_num = 0.0
        unused_num = 0.0
        esc_num = 0.0

        app_size = 0.0
        target_size = [0.0, 0.0]
        target_size_ratio = [0.0, 0.0]
        unused_size = 0.0
        esc_size = 0.0
        
        totalmp = {}
        vmmp = {}
        appmp = {}
        
        info = getObjAddrTable(bench, AP_TLAB_INTERVAL)
        print 'get addr info'
        escaped_aps = []
        if ESCAPE:
            escaped_aps = getEscapedAP(bench, AP_TLAB_INTERVAL)
            #print 'esc:', escaped_aps

        max_poid = 0
        for oneval in info:
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple
                if poid > max_poid:
                    max_poid = poid

        print 'max_poid:', max_poid
                
        appmp = [None] * (max_poid+1)        
        for val, oneval in enumerate(info):
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple
                if apid == -1 or poid == -1:
                    continue
                if apid in escaped_aps:
                    esc_num += 1
                    esc_size += size
                    continue
                if ref == -1:
                    vm_num += 1
                else:
                    # if new
                    if appmp[poid] is None:
                        pattern = 1
                        allocval = val
                        appmp[poid] = [apid, size, ref, allocval, pattern]
                    # if not new
                    else:
                        appmp[poid][2] += ref
                        if val == appmp[poid][3] + 1: # if used in the second interval
                            appmp[poid][-1] = 2       # change pattern to 2
                        else:
                            appmp[poid][-1] = -1

        debug_size1 = 0
        for poid, v in enumerate(appmp):
            if v is None:
                continue
            apid, size, ref, allocval, pattern = v
            app_size += size

            if pattern == 1:
                target_size[0] += size
            elif pattern == 2:
                target_size[1] += size
            else:
                pass
            

        # #esc_size_ratio = esc_size/app_size
        target_size_ratio[0] = target_size[0]/app_size
        target_size_ratio[1] = target_size[1]/app_size

        print debug_size1
        print "used 1: %.2f, used 2: %.2f" % tuple(target_size_ratio)
        self.xlsx_header = ['benchmark', 'only used in first interval', 'only in first two intervals']
        self.xlsx_table.append([bench] + target_size_ratio)

    def analyze_ap(self, bench, ESCAPE=True):
        vm_num = 0.0
        app_num = 0.0
        unused_num = 0.0
        esc_num = 0.0

        app_size = 0.0
        target_size = [0.0, 0.0]
        target_size_ratio = [0.0, 0.0]
        target_num = [0.0, 0.0]
        target_num_ratio = [0.0, 0.0]
        unused_size = 0.0
        esc_size = 0.0
        
        totalmp = {}
        vmmp = {}
        appmp = {}
        
        info = getObjAddrTable(bench, AP_TLAB_INTERVAL)
        
        escaped_aps = []
        if ESCAPE:
            escaped_aps = getEscapedAP(bench, AP_TLAB_INTERVAL)
            #print 'esc:', escaped_aps

        max_poid = 0
        max_apid = 0
        for oneval in info:
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple
                if poid > max_poid:
                    max_poid = poid

                if apid > max_apid:
                    max_apid = apid

        #print 'max poid:', max_poid, 'max apid:', max_apid
                
        appobjmp = [None] * (max_poid+1)        
        for val, oneval in enumerate(info):
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple

                if apid == -1 or poid == -1:
                    continue
                if apid in escaped_aps:
                    esc_num += 1
                    esc_size += size
                    continue
                if ref == -1:
                    vm_num += 1
                else:
                    # if new
                    if appobjmp[poid] is None:
                        pattern = 1
                        allocval = val
                        appobjmp[poid] = [apid, size, ref, allocval, pattern]
                    # if not new
                    else:
                        appobjmp[poid][2] += ref
                        if val == appobjmp[poid][3] + 1: # if used in the second interval
                            appobjmp[poid][-1] = 2       # change pattern to 2
                        else:
                            appobjmp[poid][-1] = -1

        
        apmp = [None] * (max_apid+1) 
        ##### apmap structure #####
        # apid => [
        #           pattern,
        #           total size,
        #           total objects number,
        #           target size,
        #           target number (number of pattern 1 objects),
        #           #(target size)/#(total size),
        #         ]
        # The last ratio is calculated in the second pass

        
        ap_cnt = 0
        
        for poid, v in enumerate(appobjmp):
            if v is None:
                continue
            apid, size, ref, allocval, pattern = v
            app_size += size

            if apmp[apid] is None:                    
                apmp[apid] = [1, size, 1, size, 1, None]
                
            apmp[apid][1] += size
            apmp[apid][2] += 1
            
            if pattern == 1:
                apmp[apid][3] += size
                apmp[apid][4] += 1

            elif pattern == 2:
                apmp[apid][3] += size
                apmp[apid][4] += 1
                apmp[apid][0] = 2
            else:
                # if apid == 616:
                #     print v, apmp[apid]
                apmp[apid][0] = -1

            
            # if pattern == 1:
            #     if apmp[apid] is None:                    
            #         apmp[apid] = [1, size, 1, size, 1, None]

            #     else:
            #         apmp[apid][1] += size
            #         apmp[apid][2] += 1
            #         apmp[apid][3] += size
            #         apmp[apid][4] += 1
                
            # else:
            #     if apid == 616:
            #         print v, apmp[apid]
            #     if apmp[apid] is None:
            #         apmp[apid] = [-1, size, 1, 0, 0, None]
 
            #     else:
            #         apmp[apid][0] = -1
            #         apmp[apid][2] += 1
            #         apmp[apid][1] += size

        ap_num = 0
        debug_size1 = 0
        for i, v in enumerate(apmp):
            if v is None:
                continue

            ap_num += 1
            pattern_v, total_size_v, total_num_v, target_size_v, target_num_v, ratio_v = v
            
            #v[-1] = target_num_v/float(total_num_v)
            v[-1] = target_size_v/float(total_size_v)

            if pattern_v == 1:
                target_num[0] += 1
                target_size[0] += total_size_v
            elif pattern_v == 2:
                target_size[1] += total_size_v

        print app_size, target_size[0]
        self.rank_ap(apmp)

        # target_size_ratio[0] = target_size[0]/app_size
        # target_size_ratio[1] = target_size[1]/app_size
        # target_num_ratio[0] = target_num[0]/ap_num
        # # print "used 1 size: %.3f" % target_size_ratio[0]
        # # print "used 1 num: %.3f" % target_num_ratio[0]
        # # print "total ave size: %.3f" % (app_size/ap_num)
        # # print "used 1 ave size: %.3f" % (target_size[0]/target_num[0])

        # print target_size_ratio[0], target_size_ratio[1]
        # self.xlsx_header = ['benchmark', 'only used in first interval', 'second interval']
        # self.xlsx_table.append([bench] + [target_size_ratio[0], target_size_ratio[1]-target_size_ratio[0]])


    def analyze_ap_tiered(self, bench, ESCAPE=True):
        vm_num = 0.0
        app_num = 0.0
        unused_num = 0.0
        esc_num = 0.0
        tiers = 3

        app_size = 0.0
        target_size = [0.0] * tiers
        target_size_ratio = [0.0] * tiers
        target_num = [0.0] * tiers
        target_num_ratio = [0.0] * tiers
        unused_size = 0.0
        esc_size = 0.0
        
        totalmp = {}
        vmmp = {}
        appmp = {}
        
        info = getObjAddrTable(bench, AP_TLAB_INTERVAL)
        
        escaped_aps = []
        if ESCAPE:
            escaped_aps = getEscapedAP(bench, AP_TLAB_INTERVAL)
            #print 'esc:', escaped_aps

        max_poid = 0
        max_apid = 0
        for oneval in info:
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple
                if poid > max_poid:
                    max_poid = poid

                if apid > max_apid:
                    max_apid = apid

        #print 'max poid:', max_poid, 'max apid:', max_apid
                
        appobjmp = [None] * (max_poid+1)        
        for val, oneval in enumerate(info):
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple

                if apid == -1 or poid == -1:
                    continue
                if apid in escaped_aps:
                    esc_num += 1
                    esc_size += size
                    continue
                if ref == -1:
                    vm_num += 1
                else:
                    # if new
                    if appobjmp[poid] is None:
                        pattern = 1
                        allocval = val
                        appobjmp[poid] = [apid, size, ref, allocval, pattern]
                    # if not new
                    else:
                        appobjmp[poid][2] += ref
                        if val == appobjmp[poid][3] + 1: # if used in the second interval
                            appobjmp[poid][-1] = 2       # change pattern to 2
                        else:
                            appobjmp[poid][-1] = -1

        
        apmp = [None] * (max_apid+1) 
        ################## apmap structure #####################
        # apid => [
        #           pattern,
        #           total size,
        #           total objects number,
        #           target size,
        #           target number (number of pattern 1 objects),
        #           #(target size)/#(total size),
        #         ]
        # The last ratio is calculated in the second pass

        ################### ap pattern ####################
        #  1: 100% type 1 objects
        #  2: 98% type 1 objects
        #  3: 95% type 1 objects
        # -1: not aobve
        
        
        ap_cnt = 0
        
        for poid, v in enumerate(appobjmp):
            if v is None:
                continue
            apid, size, ref, allocval, pattern = v
            app_size += size

            if apmp[apid] is None:                    
                apmp[apid] = [1, 0, 0, 0, 0, None]
                
            apmp[apid][1] += size
            apmp[apid][2] += 1
            
            if pattern == 1:
                apmp[apid][3] += size
                apmp[apid][4] += 1


        ap_num = 0
        debug_size1 = 0
        for i, v in enumerate(apmp):
            if v is None:
                continue

            ap_num += 1
            pattern_v, total_size_v, total_num_v, target_size_v, target_num_v, ratio_v = v
            v[-1] = target_size_v/float(total_size_v)
            #print target_size_v, float(total_size_v)
            
            if v[-1] == 1:
                v[0] = 1
            elif v[-1] > 0.98:
                v[0] = 2
            elif v[-1] > 0.95:
                v[0] = 3
            else:
                v[0] = -1

            
            for i in range(1, 1+tiers):
                if v[0] == i:
                    target_size[i-1] += target_size_v

                # if v[0] == 2:
                #     debug_size1 += target_size_v
                    


        
        for i in range(tiers):
            target_size_ratio[i] = target_size[i]/app_size

        print app_size, debug_size1
        print target_size_ratio
        self.xlsx_header = ['benchmark', '100%', '98%', '95%']
        self.xlsx_table.append([bench] + target_size_ratio)


        
    def rank_ap(self, apmp):
        # tar_apmp = filter(lambda x: x!=None and x[0]==1, apmp)
        # nor_apmp = filter(lambda x: x!=None and x[0]==-1, apmp)
        # tar_apmp = sorted(tar_apmp, key=lambda k: k[1]*k[2])
        # nor_apmp = sorted(nor_apmp, key=lambda k: k[1]*k[2])

        ##### apmap structure #####
        # apid => [
        #           pattern,
        #           total size,
        #           total objects number,
        #           target size,
        #           target number (number of pattern 1 objects),
        #           #(target size)/#(total size),
        #         ]
        
        total_ap_size = 0.0
        total_target_size = 0.0
        target_ap_size = 0.0
        total_obj_num = 0.0
        newapmp = []
        for i, ap in enumerate(apmp):
            if ap is None:
                continue

            ap_size = ap[1]
            total_ap_size += ap_size            
            target_size = ap[3]
            total_target_size += target_size
            if ap[0] == 1:
                target_ap_size += ap_size
            total_obj_num += ap[2]
            thisap = [i] + ap
            newapmp.append(thisap)

        sumrow = ['target ap sum', total_ap_size, 'nil', target_ap_size, 'nil', target_ap_size/total_ap_size]
        sumrow1 = ['sum', total_ap_size, 'nil', total_target_size, 'nil', total_target_size/total_ap_size]
        print total_ap_size, total_target_size
        self.xlsx_table = sorted(newapmp, reverse=True, key=lambda k: k[4])
        self.xlsx_table.append(sumrow)
        self.xlsx_table.append(sumrow1)
        self.xlsx_header = ['id', 'type', 'total_size', 'total objects', 'target size', 'target_num', 'target_ratio']
        self.add_xlsx_table()
        # apmp = sorted(apmp, reverse=True, key=lambda k: k[1]*k[2] if k is not None else -1)
        # for ap in apmp:
        #     if ap is not None:
        #         self.xlsx_table.append([])
        # self.xlsx_header = ['type', '']
        # print self.xlsx_table
        # #pprint.pprint(apmp)
        

    def do_rank(self):
        for b in self.benches:
            self.analyze_ap(b)

    def run(self):
        for b in self.benches:
            self.analyze_ap_tiered(b)
    
    def do_escape(self, rountine):
        print 'escape analysis off'
        for b in self.benches:
            print b
            rountine(b, ESCAPE=False)
        self.add_mean_row()
        self.add_xlsx_chart(title='Unused Ratio With Escape Analysis Off', ylabel='Size Percentage')
        self.xlsx_table = []
        print 'escape analysis on'
        for b in self.benches:
            print b
            rountine(b, ESCAPE=True)
        self.add_mean_row()
        self.add_xlsx_chart(title='Unused Ratio With Escape Analysis On', ylabel='Size Percentage')

    
    def __clean__(self):
        DacapoAnalyzer.__clean__(self)

        
if __name__ == '__main__':
    ana = UnusedapAnalyzer()
    # ana.analyze_benches(ana.analyze_ap)
    # ana.to_stacked_bar(title='Unused Analysis', ylabel='Size Percentage')

    #ana.do_escape(ana.analyze_ap)
    #ana.do_rank()
    #ana.run()
    ana.do_escape(ana.analyze_ap_tiered)
    # for bench in ana.benches:
    #     ana.analyze_obj(bench)
        
    #     #ana.testescape(bench)

    # ana.analyze_benches(ana.analyze_obj)
    # ana.add_xlsx_chart(title='Unused Ratio With Escape Analysis Off', ylabel='Size Percentage')
    # ana.add_xlsx_chart(title='Unused Ratio With Escape Analysis On', ylabel='Size Percentage')

    #ana.do_obj()

    ## always call __clean__()
    ana.__clean__()
