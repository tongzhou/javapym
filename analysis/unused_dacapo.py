#! /usr/bin/env python

from base_dacapo import *
from base_dacapo import DacapoAnalyzer

class UnusedapAnalyzer(DacapoAnalyzer):    
    def analyze_ap(self, bench):
        ap = simplyGetAPInfo(bench,AP_TLAB_INTERVAL,0,sumvals=True)

        used_intervals = [-1, 0, 1] # meaning an ap is never used after this interval
        used_inter_num = 3
        unused_ap_num = [0.0 for i in range(0, used_inter_num)]
        ri_ap_num = [0.0 for i in range(0, used_inter_num)]
        ro_ap_num = 0.0
        
        all_ap_num = len(ap['APID_FULL'])

        unused_size = [0.0 for i in range(0, used_inter_num)]
        ri_size = [0.0 for i in range(0, used_inter_num)]
        ro_size = 0.0
        all_size = 0.0

        print 'apnum:', all_ap_num
        for apid in ap['APID_FULL']:
            if apid == 0:
                continue

            cases = len(used_intervals)
            qualified = [True for i in range(0, cases)]
            this_ap_size = 0.0

            for i, info in enumerate(ap['APID_FULL'][apid]):
                objs = info['new_objs']
                new_size = int(info['new_size'])
                reads = info['reads']
                writes = info['writes']
                refs = int(info['refs'])
                #print refs,

                if i == 0 and refs != 0:
                    qualified[0] = False
                if i == 1 and refs != 0:
                    qualified[0] = False
                    qualified[1] = False
                if i > 1 and refs != 0:
                    qualified[0] = False
                    qualified[1] = False
                    qualified[2] = False

                all_size += new_size
                this_ap_size += new_size

            if qualified[0]:
                unused_size[0] += this_ap_size
                unused_ap_num[0] += 1
            elif qualified[1]:
                unused_size[1] += this_ap_size
                unused_ap_num[1] += 1
            elif qualified[2]:
                unused_size[2] += this_ap_size
                unused_ap_num[2] += 1

        # print unused_ap_num[0], unused_size[0]
        # print unused_ap_num[1], unused_size[1]
        unused_rate = [0.0 for i in range(0, used_inter_num)]
        for i in range(0, used_inter_num):
            unused_rate[i] = unused_size[i]/all_size
            #unused_rate[i] = unused_ap_num[i]/all_ap_num
        print unused_rate
        unused_rate.insert(0, bench)
        self.xlsx_header = ['benchmark', 'used 0', 'used 1', 'used 2']
        self.xlsx_table.append(unused_rate)

    #@deprecated
    def analyze_objaddr(self, bench, ESCAPE=True):
        '''
        treat each address as a new object each time after a GC
        does not use poid
        '''
        total_obj_num = 0.0
        vm_obj_num = 0.0
        app_obj_num = 0.0
        unused_obj_num = 0.0
        esc_obj_num = 0.0

        app_obj_size = 0.0
        unused_obj_size = 0.0
        esc_obj_size = 0.0
        
        total_objmp = {}
        vm_objmp = {}
        app_objmp = {}
        
        info = getObjAddrTable(bench, AP_TLAB_INTERVAL)
        escaped_aps = []
        if ESCAPE:
            escaped_aps = getEscapedAP(bench, AP_TLAB_INTERVAL)
        for val, oneval in enumerate(info):
            total_obj_num += len(oneval)
            for objtuple in oneval:
                apid, addr, size, ref, new = objtuple
                if apid in escaped_aps:
                    esc_obj_num += 1
                    continue
                if objtuple[3] == -1:
                    vm_obj_num += 1
                else:
                    app_obj_num += 1
                    app_obj_size += size
                    if size == ref*8:
                        unused_obj_num += 1
                        unused_obj_size += size

        esc_ratio = esc_obj_num/total_obj_num
        unused_size_ratio = unused_obj_size/app_obj_size
        print 'esc %:', esc_ratio
        print unused_obj_size, app_obj_size, unused_size_ratio, '\n'

        self.xlsx_header = ['benchmark', 'unused']
        self.xlsx_table.append([bench, unused_size_ratio])


        
    def analyze_obj(self, bench, ESCAPE=True):
        total_obj_num = 0.0
        vm_obj_num = 0.0
        app_obj_num = 0.0
        unused_obj_num = 0.0
        esc_obj_num = 0.0

        app_obj_size = 0.0
        unused_obj_size = 0.0
        esc_obj_size = 0.0
        
        total_objmp = {}
        vm_objmp = {}
        app_objmp = {}
        
        info = getObjAddrTable(bench, AP_TLAB_INTERVAL)
        escaped_aps = []
        if ESCAPE:
            escaped_aps = getEscapedAP(bench, AP_TLAB_INTERVAL)
            #print 'esc:', escaped_aps
        for val, oneval in enumerate(info):
            for objtuple in oneval:
                apid, poid, addr, size, ref, new = objtuple
                if apid == -1 or poid == -1:
                    continue
                if apid in escaped_aps:
                    esc_obj_num += 1
                    esc_obj_size += size
                    continue
                if ref == -1:
                    vm_obj_num += 1
                else:
                    if poid not in app_objmp:
                        app_objmp[poid] = []
                    app_objmp[poid].append(objtuple)

        app_sumobjmp = {}
        for k, v in app_objmp.iteritems():
            app_sumobjmp[k] = v[0]
            apid, poid, addr, size, ref, new = v[0]
            app_obj_num += 1
            app_obj_size += size
            if len(v) > 1:
                #print v
                for t in range(1, len(v)):                    
                    if app_sumobjmp[k][3] != v[t][3] and OAR_MARKER != v[t][2]:
                        print 'inconsitent poi:', v
                    else:
                        ref += v[t][-2]
                app_sumobjmp[k][-2] = ref
            if ref*8 == size:
                #print app_sumobjmp[k]
                unused_obj_num += 1
                unused_obj_size += size

        esc_size_ratio = esc_obj_size/app_obj_size
        unused_num_ratio = unused_obj_num/app_obj_num
        unused_size_ratio = unused_obj_size/app_obj_size
        #print esc_ratio, unused_num_ratio, unused_size_ratio
        print "escaped: %.2f%%, unused: %.2f%%" % (esc_size_ratio*100, unused_size_ratio*100)
        self.xlsx_header = ['benchmark', 'unused']
        self.xlsx_table.append([bench, unused_size_ratio])
    
    
    def do_obj(self):
        print 'escape analysis off'
        for b in self.benches:
            print b
            self.analyze_obj(b, ESCAPE=False)
        self.add_mean_row()
        self.add_sheet(title='Unused Ratio With Escape Analysis Off', ylabel='Size Percentage')
        self.xlsx_table = []
        print 'escape analysis on'
        for b in self.benches:
            print b
            self.analyze_obj(b, ESCAPE=True)
        self.add_mean_row()
        self.add_sheet(title='Unused Ratio With Escape Analysis On', ylabel='Nu Percentage')

    
    def __clean__(self):
        DacapoAnalyzer.__clean__(self)

        
if __name__ == '__main__':
    ana = UnusedapAnalyzer()
    # ana.analyze_benches(ana.analyze_ap)
    # ana.to_stacked_bar(title='Unused Analysis', ylabel='Size Percentage')

    ana.do_obj()
    # for bench in ana.benches:
    #     ana.analyze_obj(bench)
        
    #     #ana.testescape(bench)

    # ana.analyze_benches(ana.analyze_obj)
    # ana.add_sheet(title='Unused Ratio With Escape Analysis Off', ylabel='Size Percentage')
    # ana.add_sheet(title='Unused Ratio With Escape Analysis On', ylabel='Size Percentage')

    #ana.do_obj()

    ## always call __clean__()
    ana.__clean__()
