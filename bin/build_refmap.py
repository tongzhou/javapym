#!/usr/bin/env python

import sys
from lib.utils import *
from optparse import OptionParser
from operator import itemgetter

newCollectionRE = re.compile("(\W)*(\S)+ ObjectInfoCollection:(\W)+(\d+)")
objrefRE        = re.compile("(\W)*(\d+)(\W)+(\d+)")

def get_time(line):
  return long(line.split()[-1].strip('()'))

usage = "usage: %prog BENCH CFG ITER [OPTIONS]"
def build_refmap(args):

  parser = OptionParser()

  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  bench = args[0]
  cfg   = args[1]
  iter  = int(args[2])

  infof = try_open_read(objInfoLog(bench, cfg, iter))
  mapf  = try_open_write(refMapLog(bench, cfg, iter))

  first_line = infof.readline()
  if not newCollectionRE.match(first_line):
    print "%s is not an objinfo file" % infofile
    raise SystemExit(1)

  objs = {}
  vals = []

  start_time = get_time(first_line)
  cur_time_val = 0
  vals.append(cur_time_val)

  infof.seek(0,0)
  for line in infof:
    if newCollectionRE.match(line):
      cur_time_val = (get_time(line) - start_time)
      vals.append(cur_time_val)

      for vline in infof:
        xline = vline.lstrip(' *')
        if objrefRE.match(xline):
          objid,refs = [ long(x) for x in xline.split() ]
          if not objs.has_key(objid):
            objs[objid] = {}
          objs[objid][cur_time_val] = refs
          #print objid, cur_time_val, refs
          #print vals
          #raise SystemExit(1)
        else:
          break
  infof.close()
  #print vals
  #raise SystemExit(1)

  print >> mapf, "".ljust(10),
  for val in vals:
    print >> mapf, ("%13d" % val),
  print >> mapf, ""

  for okey in sorted (objs.keys()):
    print >> mapf, ("%10d" % okey),
    refs = objs[okey]
    for val in vals:
      if refs.has_key(val):
        print >> mapf, ("%13d" % refs[val]),
      else:
        print >> mapf, ("%13d" % long(0)),
    print >> mapf, ""
  mapf.close()

if __name__ == '__main__':
  build_refmap(sys.argv)

