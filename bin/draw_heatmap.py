#!/usr/bin/env python

import sys
from lib.globals import *
from lib.utils import *

from optparse import OptionParser
import math
import numpy as np
import numpy.ma as ma
import matplotlib
matplotlib.use('Agg')

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import ListedColormap

from pylab import pcolor,colorbar,rand
from pickle import load
import random

# gradients per colormap
gpc = 20

refmax = 7.0
refmin = 0.0
fuzz   = 0.0000000001

def multiColorMap(colors):

  # shape is the number of gradients per color over 4 -- 4 because each
  # color is defined by a quadruple: (r,g,b,a)
  #
  rgbs = np.empty(((gpc*len(colors)),4))
  for i,color in enumerate(colors):
    curmap = cm.get_cmap(colord[color],gpc)
    for j,rgb in enumerate(curmap(np.arange(gpc))):
      rgbs[(i*gpc)+j] = rgb

  return ListedColormap(rgbs)

def draw_heatmap(args):

  parser = OptionParser()
  parser.add_option("-s", "--sample", dest="sample",
                    help="sample number of objects", default=None)
  parser.add_option("-c", "--clusters", dest="clusters",
                    help="number of clusters to use", default="3")

  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  bench = args[0]
  cfg   = args[1]
  iter  = int(args[2])

  objsample = int(opts.sample) if opts.sample else None
  nclusters = int(opts.clusters)

  print "getting data"
  clusterf = try_open_read(clusterToObjectFile(bench, cfg, iter, nclusters))
  cvals = load(clusterf)
  clusterf.close()

  print "do the colors"
  clusters = sorted(cvals.keys())
  colors = [ cvals[k][COLOR] for k in clusters ]
  mcm = multiColorMap(colors)

  vmin,vmax = (0.0, float((len(mcm.colors))))
  norm = matplotlib.colors.Normalize(vmin=vmin,vmax=vmax)

  if objsample:
    print "random selection of %d samples in each cluster" % objsample
    for c in clusters:
      if len(cvals[c][ITEMS]) > objsample:
        cvals[c][ITEMS] = random.sample(cvals[c][ITEMS],objsample)

  print "preprocess"
  nobjs = sum([ len(cvals[c][ITEMS]) for c in clusters ])
  maxref = max([ m for n in \
                 [ i[1] for s in \
                   [ cvals[k][ITEMS] for k in cvals.keys() ] \
                 for i in s ] \
               for m in n ])

  nvals = len( cvals[clusters[0]][ITEMS][0][1] )
  refmax = math.ceil(math.log(float(maxref),10))

  print "making array"
  data = np.empty((nvals,nobjs))
  for i in range(nvals):
    oinc = 0
    for cnum,cluster in enumerate(clusters):
      scale = gpc * cnum
      #print "cnum=%d" % cnum
      #print "%s (%d) %d" % (colors[cnum], len(cvals[cluster][ITEMS][:10]), oinc)
      #print "%s (%d) %d" % (colors[cnum], len(cvals[cluster][ITEMS]), oinc)
      #for j,(obj,refs) in enumerate(cvals[cluster][ITEMS][:10]):
      items = cvals[cluster][ITEMS]
      for j,(obj,refs) in enumerate(items):
        assert (refs[i] < (maxref+1)), "invalid max ref: %d" % ref
        rval = math.log(float(refs[i]),10) if not (refs[i] == 0) else 0.0
        assert (rval < refmax+fuzz and rval > refmin-fuzz), "invalid rval: %f" % rval
        #rval = (rval / (refmax - refmin)) + scale
        rval = ((rval / (refmax - refmin)) * gpc) + scale
        data[i][j+oinc] = rval
      #oinc += len(cvals[cluster][ITEMS][:10])
      #oinc += len(cvals[cluster][ITEMS])
      oinc += len(items)

  print "plotting data"
  fig, ax = plt.subplots()
  heatmap = ax.pcolor(data, cmap=mcm, norm=norm)
  plt.savefig(refPDF(bench,cfg,iter,nclusters,objsample))

#  data = np.empty((nobjs,nvals))
#  oinc = 0
#  for cnum,cluster in enumerate(clusters):
#    scale = gpc * cnum
#    #print "cnum=%d" % cnum
#    #print "%s (%d) %d" % (colors[cnum], len(cvals[cluster][ITEMS][:10]), oinc)
#    print "%s (%d) %d" % (colors[cnum], len(cvals[cluster][ITEMS]), oinc)
#    #for i,(obj,refs) in enumerate(cvals[cluster][ITEMS][:10]):
#    for i,(obj,refs) in enumerate(cvals[cluster][ITEMS]):
#      for j in range(nvals):
#        rval = math.log(float(refs[j]),10) if not (refs[j] == 0) else 0.0
#        assert (rval < refmax+fuzz and rval > refmin-fuzz), "invalid rval: %f" % rval
#        #rval = (rval / (refmax - refmin)) + scale
#        rval = ((rval / (refmax - refmin)) * gpc) + scale
#        data[i+oinc][j] = rval
#    #oinc += len(cvals[cluster][ITEMS][:10])
#    oinc += len(cvals[cluster][ITEMS])

        #print "data[%d][%d]=%f" % (i,j,data[i][j])
#        if rval != 0:
#          data[i][j] = math.log(float(refs[j]))
#        else:
#          data[i][j] = 0.0
     
#  data = np.empty(((len(colors)*4),4))
#  for i in range(len(colors)):
#    for j in range(4):
#      #data[(i*4)+j] = np.random.uniform(0.0+(i*gpc), 20.0+(i*gpc), 4)
#      data[(i*4)+j] = np.array([ (5.9 + (j*4.0) + (i*gpc)),
#                                 (6.9 + (j*4.0) + (i*gpc)),
#                                 (7.9 + (j*4.0) + (i*gpc)),
#                                 (8.9 + (j*4.0) + (i*gpc))])
#      data[(i*4)+j] = np.array([ 0.0 + (j*4.0) + (i*gpc),
#                                 1.0 + (j*4.0) + (i*gpc),
#                                 2.0 + (j*4.0) + (i*gpc),
#                                 3.0 + (j*4.0) + (i*gpc)])

  #print len(data)
  #print len(data[0])
  #print mcm.colors[19], mcm.colors[20]
  #print len(mcm.colors)

##  masked = ma.masked_all((4,4))
##  blues  = ma.asarray(np.random.rand(4,4))
##  data   = ma.concatenate([blues,masked])
##  print data
##  heatmap = ax.pcolor(data, cmap=plt.cm.Blues)
##  greens  = ma.asarray(np.random.rand(4,4))
##  data    = ma.concatenate([greens,masked]) 

#
#  # put the major ticks at the middle of each cell
#  ax.set_xticks(np.arange(data.shape[0])+0.5, minor=False)
#  ax.set_yticks(np.arange(data.shape[1])+0.5, minor=False)
#
#  # want a more natural, table-like display
#  ax.invert_yaxis()
#  ax.xaxis.tick_top()
#
#  ax.set_xticklabels(row_labels, minor=False)
#  ax.set_yticklabels(column_labels, minor=False)

#  cdict = {'red': ((0.0, 0.0, 0.0),
#                   (0.33, 1.0, 0.0),
#                   (1.0, 0.0, 0.0)),
#           'green': ((0.0, 0.0, 0.0),
#                     (0.33, 1.0, 0.0),
#                     (0.66, 1.0, 0.0),
#                     (1.0, 0.0, 0.0)),
#           'blue': ((0.0, 0.0, 0.0),
#                    (0.33, 1.0, 0.0),
#                    (0.66, 0.0, 0.0),
#                    (1.0, 1.0, 1.0))}
#  fig, ax = plt.subplots()
#  my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
#
#  #data = np.random.rand(4,4)
#  a=np.outer(np.arange(0,1,0.01),np.ones(10))
#  plt.imshow(a,aspect='auto',cmap=my_cmap,origin="lower")
#  #plt.title(m,rotation=90,fontsize=10,verticalalignment='bottom')
#  plt.savefig(refPDF(bench,cfg,iter))

  #ax.pcolor(rand(10,10),cmap=my_cmap)
  #colorbar()


#  fig = plt.figure(figsize=(80.0,60.0),dpi=4400)
#  ax = fig.add_subplot(111, projection='3d')
#
#  print "drawing figure"
#  n  = 0
#  zs = numpy.array(range(len(vals)))
#  for ckey in cvals.keys():
#    color = cvals[ckey][COLOR]
#    items = cvals[ckey][ITEMS]
#
#    for i,(obj,refs) in enumerate(items):
#      #xs = numpy.array( [obj for _ in range(len(refs))] )
#      xs = numpy.array( [n for _ in range(len(refs))] )
#      ys = numpy.log10(numpy.array([1 if x == 0 else x for x in refs]))
#      #ys = numpy.array(refs)
#      cs = [color] * len(xs)
#      ax.bar(xs, ys, zs, zdir='y', color=cs, alpha=0.8)
#      #dx = 0.5
#      #dy = 0.5
#      #dz = 0.0
#      #ax.bar3d(xs, ys, zs, dx, dy, dz, color=cs, alpha=0.8)
#      n += 1
#
#      if (i+1) % 300 == 0:
#        print "i = %5d, c = \'%s\'" % (n,color)
#      #if (i+1) == 300:
#      #  break
#
#  ax.set_xlabel('objects')
#  ax.set_ylabel('sample')
#  ax.set_zlabel('refs')
#
#  plt.savefig(refPDF(bench,cfg,iter))


#  for c, z in zip(['r', 'g', 'b', 'y'], [30, 20, 10, 0]):
#      xs = np.arange(20)
#      ys = np.random.rand(20)
#
#      # You can provide either a single color or an array. To demonstrate this,
#      # the first bar of each set will be colored cyan.
#      cs = [c] * len(xs)
#      cs[0] = 'c'
#      ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

#  oids = sorted( objs.keys() )
#  for oid in oids:
#    print oid, objs[oid]

#  input_data = numpy.array(objs.values())
#
#  for k in range(2,6):
#    labels,error,nfound = pc.kcluster(input_data,k,npass=20)
#  print "done!\n"
#  print "labels\n\n", list(labels)
#  print "\nerror:  ", error, "\n"
#  print "\nnfound: ", nfound

#  pt1 = numpy.random.normal(1, 0.2, (100,2))
#  pt2 = numpy.random.normal(2, 0.5, (300,2))
#  pt3 = numpy.random.normal(3, 0.3, (100,2))
#   
#  # slightly move sets 2 and 3 (for a prettier output)
#  pt2[:,0] += 1
#  pt3[:,0] -= 0.5
#
#  print "pt1:\n\n", pt1
#  print "\npt2:\n\n", pt2
#  print "\npt3:\n\n", pt3
#   
#  xy = numpy.concatenate((pt1, pt2, pt3))
#
#  print "\nxy\n\n", xy
#  print "\ninput\n\n", numpy.array(zip(xy[:,0],xy[:,1]))
#   
#  # kmeans for 3 clusters
#  res, idx = kmeans2(numpy.array(zip(xy[:,0],xy[:,1])),3)
#
#   
#  colors = ([([0.4,1,0.4],[1,0.4,0.4],[0.1,0.8,1])[i] for i in idx])
#   
#  # plot colored points
#  pylab.scatter(xy[:,0],xy[:,1], c=colors)
#   
#  # mark centroids as (X)
#  pylab.scatter(res[:,0],res[:,1], marker='o', s = 500, linewidths=2, c='none')
#  pylab.scatter(res[:,0],res[:,1], marker='x', s = 500, linewidths=2)
#   
#  pylab.savefig('/home/mjantz/kmeans.png')

#  a=np.outer(np.arange(0,1,0.01),np.ones(10))
#  print a
#  plt.imshow(a,aspect='auto',cmap=mcm,origin="lower")
#  plt.savefig(refPDF(bench,cfg,iter))


if __name__ == '__main__':
  draw_heatmap(sys.argv)

