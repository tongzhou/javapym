#!/usr/bin/env python

import sys
from lib.utils import *
from optparse import OptionParser
from operator import itemgetter

from pickle import load, dump
from math import ceil

newCollectionRE = re.compile("(\W)*(\S)+ ObjectInfoCollection:(\W)+(\d+)")
objrefRE        = re.compile("(\W)*(\d+)(\W)+(\d+)")

headerEndRE = re.compile("----------------------------------------")
klassRE     = re.compile("(\W)*(\d+):")
objRE       = re.compile("(\W)*(\d+)(\W)+(\d+)")

def get_time(line):
  return long(line.split()[-1].strip('()'))

def get_val(line):
  return long(line.split()[-2])

def get_rat(num,den):
  if den == 0:
    assert num == 0, "zero total with non-zero value"
    return float(0)
  else:
    return (float(num) / float(den))

usage = "usage: %prog BENCH CFG ITER [OPTIONS]"
def dynamic_hotsets(args):

  parser = OptionParser()
  parser.add_option("-t", "--hot_threshold", dest="hot_threshold",
                    help="minimum number of refs for hot objects",
                    default="100")
  parser.add_option("-r", "--hot_ratio", dest="hot_ratio",
                    help="maximum ratio of objects colored red each"
                    "interval", default="0.1")
  parser.add_option("-d", "--dumpreds", dest="dumpreds",
                    help="dump red objects to file every interval",
                    default=None)
  parser.add_option("-v", "--red_verbose", dest="red_verbose",
                    action="store_true", help="print red objects to dumpreds"
                    "file", default=False)

  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  bench       = args[0]
  cfg         = args[1]
  iter        = int(args[2])
  thresh      = int(opts.hot_threshold)
  ratio       = float(opts.hot_ratio)
  red_verbose = opts.red_verbose
  dumpredsf   = try_open_write(opts.dumpreds) if opts.dumpreds else None

  print "getting object info"
  allocf = try_open_read(objAllocLog(bench, cfg, iter))

  for line in allocf:
    if headerEndRE.match(line):
      break

  allocd = {}
  next_color = {}
  for line in allocf:
    if klassRE.match(line):
      cur_klass = line.split()[1]
    if objRE.match(line):
      oid   = int(line.split()[0])
      osize = int(line.split()[1])
      allocd[oid] = (cur_klass, osize)
      next_color[oid] = BLUE
  allocf.close()

  infof = try_open_read(objInfoLog(bench, cfg, iter))

  first_line = infof.readline()
  if not newCollectionRE.match(first_line):
    print "%s is not an objinfo file" % infofile
    raise SystemExit(1)

  start_time = get_time(first_line)
  cur_time_val = 0

  color_sets = [RED, BLUE]

  print "building valsets"
  valsets    = {}
  next_color = {}
  vali       = 0
  infof.seek(0,0)
  for line in infof:
    if newCollectionRE.match(line):
      vali += 1
      cur_time_val = (get_time(line) - start_time)
      print cur_time_val
      valsets[cur_time_val] = {}
      for color in color_sets:
        valsets[cur_time_val][color]  = []

      live_objs = []
      for vline in infof:
        xline = vline.lstrip(' *')
        if objrefRE.match(xline):
          objid,refs = [ long(x) for x in xline.split() ]
          color = next_color[objid] if next_color.has_key(objid) else BLUE

#          if not valsets[cur_time_val].has_key(color):
#            valsets[cur_time_val][color] = []

          valsets[cur_time_val][color].append((objid,refs,allocd[objid][1]))
          live_objs.append((objid,refs,allocd[objid][1]))
        else:
          break

      for objid in next_color.keys():
        next_color[objid] = BLUE

      nobjs = len(live_objs)
      nrefs = sum ( [ x[1] for x in live_objs ] )
      tsize = sum ( [ x[2] for x in live_objs ] )

      nreds = ceil(float(nobjs) * ratio)
      if opts.dumpreds:
        redsd = {}

      redobjs = redrefs = redsize = 0
      for objid,refs,size in sorted(live_objs,key=itemgetter(1),reverse=True):
        if nreds == 0:
          break
 
        if refs >= thresh:
          next_color[objid] = RED
          nreds -= 1
          if opts.dumpreds:
            klass = allocd[objid][0]
            if not redsd.has_key(klass):
              redsd[klass] = []
            redsd[klass].append((objid, refs, size))

      if opts.dumpreds:
        print >> dumpredsf, "val: %-8d (%d)" % (vali, cur_time_val)
        klasses = [ x[0] for x in \
                    sorted ( [ (klass, sum ( [ x[1] for x in redsd[klass] ] )) \
                               for klass in redsd.keys() ],
                             key=itemgetter(1),
                             reverse=True) ]
        for klass in klasses:

          kobjs = len(redsd[klass])
          krefs = sum ([ x[1] for x in redsd[klass] ])
          ksize = sum ([ x[2] for x in redsd[klass] ])

          if red_verbose:
            print >> dumpredsf, "".ljust(2), ("%-50s" % klass), \
                                ("objs = %-16d" % kobjs), \
                                ("refs = %-16d" % krefs), \
                                ("size = %-16d" % ksize)

          redobjs += kobjs
          redrefs += krefs
          redsize += ksize

          if red_verbose:
            for objid, refs, size in redsd[klass]:
              print >> dumpredsf, "".ljust(4), "%-13d" % objid,
              print >> dumpredsf, "%-16d" % refs,
              print >> dumpredsf, "%-16d" % size

        print >> dumpredsf, "totals: objs = %2.4f %-30s" % \
                 (get_rat(redobjs,nobjs), "(%d / %d)" % (redobjs, nobjs)),
        print >> dumpredsf, "refs = %2.4f %-30s" % \
                 (get_rat(redrefs,nrefs), "(%d / %d)" % (redrefs, nrefs)),
        print >> dumpredsf, "size = %2.4f %-30s" % \
                 (get_rat(redsize,tsize), "(%d / %d)" % (redsize, tsize))
        dumpredsf.flush()

  if opts.dumpreds:
    dumpredsf.close()

  infof.close()
  print "finished building valsets -- dumping pkl"
  dump(valsets, try_open_write(valSetPkl(bench, cfg, iter, thresh, ratio)))

if __name__ == '__main__':
  dynamic_hotsets(sys.argv)
