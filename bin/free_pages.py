#!/usr/bin/env python

import sys
from optparse import OptionParser
from subprocess import Popen, PIPE, STDOUT

ptitcmd = "ptit.py"
usage   = "usage: %prog [options]"
def free_pages(args):
  parser = OptionParser()
  parser.add_option("-n", "--node", dest="node",
                    help="node", default="*")
  parser.add_option("-z", "--zone", dest="zone",
                    help="zone", default="*")
  parser.add_option("-t", "--tray", dest="tray",
                    help="tray", default="*")
  parser.add_option("-m", "--type", dest="type",
                    help="type", default="*")
  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  proc = Popen([ptitcmd],stdout=PIPE,stderr=PIPE)
  (ptit, err) = proc.communicate()

  if err:
    print sys.stderr, "error running cmd: %s" % ptitcmd
    print sys.stderr, err
    return

  for line in ptit.split("\n"):
    node = line.split()[1].strip(",")
    zone = line.split()[3].strip(",")
    tray = line.split()[5].strip(",")
    type = line.split()[7].strip(",")

    if node == opts.node and zone == opts.zone \
    and tray == opts.tray and type == opts.type:
      print line.split()[8]
      break

  return

if __name__ == '__main__':
  free_pages(sys.argv)
