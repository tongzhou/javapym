#!/usr/bin/env python

import re
import sys
import os
from optparse import OptionParser
from subprocess import Popen, PIPE, STDOUT

pidRE    = re.compile("\(\d+\)")
def page2kB(pages):
  return ((pages * 4096) / 1024)

def readProcRss(pid,verbose=False):
  pfname = "/proc/%s/stat" % pid
  try:
    pf = open(pfname)
  except:
    raise IOError("invalid proc file: %s" % pfname)

  rss = page2kB(long(pf.read().split()[23]))
  if verbose:
    print "got rss for pid %s: %d kB" % (pid, rss)
  return rss

def getrss(args):

  parser = OptionParser()
  parser.add_option("-v", "--verbose", dest="verbose", action="store_true",
                    help="print rss for each child", default=False)
  parser.add_option("-r", "--follow-children", dest="follow_children", \
                    action="store_true", help="include children", default=False)

  (opts, args) = parser.parse_args()
  ppid = args[0]

  total_rss = 0
  try:
    total_rss += readProcRss(ppid, opts.verbose)
  except:
    print >> sys.stderr, "could not get rss for pid: %s" % ppid
    raise SystemExit(1)

  if opts.follow_children:
    cmd = "pstree -p -A %s" % ppid
    psproc = Popen(cmd.split(),stdout=PIPE,stderr=PIPE)
    (pstree, err) = psproc.communicate()

    if err:
      print sys.stderr, "error getting process tree for pid: %s" % ppid
      print sys.stderr, err
      return

    for pidstr in pidRE.findall(pstree):
      pid = pidstr.strip("()")
      if pid == ppid:
        continue
      try:
        total_rss += readProcRss(pid,opts.verbose)
      except:
        continue

  print "rss: %d kB" % total_rss

if __name__ == '__main__':
  getrss(sys.argv)
