#!/usr/bin/env python

import sys
from lib.globals import *
from lib.utils import *

from optparse import OptionParser
from pickle import dump,load

headerEndRE = re.compile("----------------------------------------")
klassRE     = re.compile("(\W)*(\d+):")
objRE       = re.compile("(\W)*(\d+)(\W)+(\d+)")

NAME   = "name"
COLORS = "colors"

def klass_cluster_info(args):

  parser = OptionParser()
  parser.add_option("-c", "--clusters", dest="clusters",
                    help="number of clusters to use", default="3")
  parser.add_option("-s", "--sample", dest="sample",
                    help="sample number of objects", default=None)

  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  bench = args[0]
  cfg   = args[1]
  iter  = int(args[2])

  objsample = int(opts.sample) if opts.sample else None
  nclusters = int(opts.clusters)

  objf  = try_open_read(objectToClusterFile(bench, cfg, iter, nclusters))
  ovals = load(objf)
  objf.close()

  allocf = try_open_read(objAllocLog(bench, cfg, iter))

  for line in allocf:
    if headerEndRE.match(line):
      break

  klassd = {}
  for line in allocf:
    if klassRE.match(line):
      pts = line.split()
      kid   = int(pts[0].strip(':'))
      kname = pts[1]

      # initialize the klass entry
      assert not klassd.has_key(kid), "duplicate kid: %d" % kid
      klassd[kid] = {}
      klassd[kid][NAME]   = kname
      klassd[kid][COLORS] = {}
      for color in cluster_colors[:nclusters]:
        klassd[kid][COLORS][color] = 0

    if objRE.match(line):
      oid = int(line.split()[0])
      klassd[kid][COLORS][ovals[oid]] += 1 

  klassf = try_open_write(klassClusterInfoFile(bench, cfg, iter, \
                          nclusters, samps=objsample))

  for kid in sorted(klassd.keys()):
    print >> klassf, "%6d : %s" % (kid, klassd[kid][NAME])
    tot = sum ( [ klassd[kid][COLORS][color] \
                  for color in cluster_colors[:nclusters] ] )
    for color in cluster_colors[:nclusters]:
      print >> klassf, "".ljust(4), "%8s : %2.4f (%d)" % \
              (color, float(klassd[kid][COLORS][color]) / tot, \
               klassd[kid][COLORS][color])

  klassf.close()

if __name__ == '__main__':
  klass_cluster_info(sys.argv)
