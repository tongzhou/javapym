#############################################################################
# functions to build commands and run bench style experiments
#############################################################################
from lib.globals import *
from lib.utils import *
from lib.report import printEmonMetricDataBench, printCASCountsBench, \
                       ppemetsBench
import time
from glob import glob

def getProfCfg(cfg):
  return cfgs[cfg][BC_PROFCFG]

def oprofEnabled(cfg):
  oprofs  = [ FULL_OPROF, EMON_OPROF, POWER_GOV_OPROF ]
  oprofs += (objinfo_cfgs + pcm_cfgs)
  oprofs += [ ADDR_INFO_DEFAULT, ADDR_INFO_INTERVAL,
              ADDR_TLAB_DEFAULT, ADDR_TLAB_INTERVAL,
              ADDR_TLAB_PREGC,   FIELD_TLAB_INTERVAL,
              FIELD_TLAB_PREGC,  FIELD_INFO_INTERVAL,
              ADDR_TLAB_XVAL,    OBJ_INFO_XSIM,
              OBJ_INFO_SHSIM, OBJ_INFO_XSHSIM ]
  for vr in vrates:
    oprofs += [ (OBJ_INFO_VXSIM%vr) ]
    oprofs += [ (OBJ_INFO_NVXSIM%vr) ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def hprofEnabled(cfg):
  hprofs   = [ FULL_HPROF, EMON_HPROF, POWER_GOV_HPROF ]
  return (cfgs[cfg][BC_PROFCFG] in hprofs)

def profilingEnabled(cfg):
  return (oprofEnabled(cfg) or hprofEnabled(cfg))

def oemonEnabled(cfg):
  oprofs   = [ FULL_OPROF, EMON_OPROF ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def hemonEnabled(cfg):
  hprofs   = [ FULL_HPROF, EMON_HPROF ]
  return (cfgs[cfg][BC_PROFCFG] in hprofs)

def emonEnabled(cfg):
  return (oemonEnabled(cfg) or hemonEnabled(cfg))

def opgovEnabled(cfg):
  oprofs  = [ FULL_OPROF, POWER_GOV_OPROF ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def hpgovEnabled(cfg):
  hprofs = [ FULL_HPROF, POWER_GOV_HPROF ]
  return (cfgs[cfg][BC_PROFCFG] in hprofs)

def pgovEnabled(cfg):
  return (opgovEnabled(cfg) or hpgovEnabled(cfg))

def pcmEnabled(cfg):
  return (cfgs[cfg][BC_PROFCFG] in pcm_cfgs)

def ptitSampleEnabled(cfg):
  pscfgs = [DEFAULT_ALLOCATIONS, COLOR_ALLOCATIONS, EDEN_ALWAYS_RED]
  return (cfgs[cfg][BC_RUNCFG] in pscfgs)

#def pcmMemoryEnabled(cfg):
#  pprofs  = [PCM_MEMORY]
#  return (cfgs[cfg][BC_PROFCFG] in pprofs)
#
#def pcmEnabled(cfg):
#  pprofs  = [PCM]
#  return (cfgs[cfg][BC_PROFCFG] in pprofs)

def doDropCaches(cfg):
  pprofs  = objinfo_cfgs + [PCM_POWER]
  return (cfgs[cfg][BC_PROFCFG] in pprofs)

def trayctlEnabled(cfg):
  traycfgs = [ RESTRICT_N1C0, \
               RESTRICT_N1C1, \
               RESTRICT_N1C2, \
               RESTRICT_N1C3
             ]
  return (cfgs[cfg][BC_TRAYCFG] in traycfgs)

def mscramEnabled(cfg):
  mscramcfgs = [  
                 SCRAMBLE_HOLD_11_OVER_1,  \
                 SCRAMBLE_HOLD_15_OVER_1,  \
                 SCRAMBLE_HOLD_11_OVER_4,  \
                 SCRAMBLE_HOLD_11_OVER_8,  \
                 SCRAMBLE_HOLD_11_OVER_9,  \
                 SCRAMBLE_HOLD_11_OVER_11, \
                 SCRAMBLE_HOLD_15_OVER_8,  \
                 SCRAMBLE_HOLD_15_OVER_9,  \
                 SCRAMBLE_HOLD_15_OVER_11, \
                 SCRAMBLE_HOLD_30_OVER_2,  \
               ]
  return (cfgs[cfg][BC_MSCRAMCFG] in mscramcfgs)

def objectAddrInfoEnabled(cfg):
  oprofs = [ ADDR_INFO_DEFAULT, ADDR_INFO_INTERVAL,
             ADDR_TLAB_DEFAULT, ADDR_TLAB_INTERVAL,
             ADDR_TLAB_PREGC,   FIELD_TLAB_INTERVAL,
             FIELD_TLAB_PREGC,  FIELD_INFO_INTERVAL,
             ADDR_TLAB_XVAL,    OBJ_INFO_XSIM,
             OBJ_INFO_SHSIM, OBJ_INFO_XSHSIM ]
  for vr in vrates:
    oprofs += [ (OBJ_INFO_VXSIM%vr) ]
    oprofs += [ (OBJ_INFO_NVXSIM%vr) ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def objectInfoEnabled(cfg):
  oprofs   = objinfo_cfgs + [ OBJ_INFO_FULL, OBJ_INFO_TENURED, \
             OBJ_INFO_FULL_TEN, OBJ_VALS_DEFAULT, OBJ_VALS_TENURED, \
             OBJ_GCVALS_DEFAULT, OBJ_GCVALS_TENURED ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def coloredSpacesEnabled(cfg):
  return ((optionOn(UseColoredSpaces)) in javaRunOpts[cfgs[cfg][BC_RUNCFG]])

def stackSamplesEnabled(cfg):
  oprofs = [ OBJ_INFO_XSIM, OBJ_INFO_SHSIM, OBJ_INFO_XSHSIM ] 
  for vr in vrates:
    oprofs += [ (OBJ_INFO_VXSIM%vr) ]
    oprofs += [ (OBJ_INFO_NVXSIM%vr) ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def allocPointInfoEnabled(cfg):
  oprofs = objinfo_cfgs + [ OBJ_INFO_XSIM, OBJ_INFO_SHSIM, OBJ_INFO_XSHSIM ]
  oprofs += [ FIELD_TLAB_INTERVAL ]
  for vr in vrates:
    oprofs += [ (OBJ_INFO_VXSIM%vr) ]
    oprofs += [ (OBJ_INFO_NVXSIM%vr) ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def trimObjectInfo(cfg):
  oprofs   = objinfo_cfgs + [ OBJ_INFO_TENURED, \
               OBJ_VALS_DEFAULT, OBJ_VALS_TENURED, \
               OBJ_GCVALS_DEFAULT, OBJ_GCVALS_TENURED ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def needCorFile(cfg):
  cor_cfgs = [ STEADY_CCF, STEADY_CCF_REPLAY, REPLAY_NO_RESET, CCF_ORACLE_DIFF ]
  return (cfgs[cfg][BC_RUNCFG] in cor_cfgs)

def needAPCCmds(cfg):
  apc_cfgs = [ OBJ_INFO_GUIDED, COLOR_ALLOCATIONS, COLOR_ALLOCATIONS_A,
               COLOR_ALLOCATIONS_B, COLOR_ALLOCATIONS_C, COLOR_ALLOCATIONS_X,
               COLOR_ALLOCATIONS_Y, COLOR_ALLOCATIONS_Z, EDEN_ALWAYS_RED,
               EDEN_ALWAYS_RED_A, EDEN_ALWAYS_RED_X, EDEN_ALWAYS_RED_Y,
               EDEN_ALWAYS_RED_Z, MEMBENCH_COLOR_ALLOC, COLOR_ALLOC_BASE,
               COLOR_ALLOC_TUNE, COLOR_ALLOC_TUNE_V2, COLOR_ALLOC_TUNE_X,
               HOKM_ALLOC_BASE ]
  return (cfgs[cfg][BC_RUNCFG] in apc_cfgs)

def valObjectInfo(cfg):
  oprofs   = [ OBJ_VALS_DEFAULT, OBJ_VALS_TENURED, \
               OBJ_GCVALS_DEFAULT, OBJ_GCVALS_TENURED ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def gcvalObjectInfo(cfg):
  oprofs   = [ OBJ_GCVALS_DEFAULT, OBJ_GCVALS_TENURED ]
  return (cfgs[cfg][BC_PROFCFG] in oprofs)

def needHeapSizes(cfg):
  nhss = [ COLORED_SPACES, ORGANIZE_OBJECTS, COLOR_ALLOCATIONS, \
           RED_OBJECTS, BLUE_OBJECTS, DEFAULT_ALLOCATIONS, \
           EDEN_ALWAYS_RED, COLOR_ALLOCATIONS_X, DEFAULT_ALLOCATIONS_X ]
#  nhss = [ COLORED_SPACES, ORGANIZE_OBJECTS, COLOR_ALLOCATIONS, \
#           RED_OBJECTS, BLUE_OBJECTS, EDEN_ALWAYS_RED, ]
  return (cfgs[cfg][BC_RUNCFG] in nhss)

def objectOrganizer(cfg):
  organizers = [ ORGANIZE_OBJECTS, RED_OBJECTS, BLUE_OBJECTS, \
                 COLOR_ALLOCATIONS_X, COLOR_ALLOCATIONS_C ]
  return (cfgs[cfg][BC_RUNCFG] in organizers)

def getRecordConfig(cfg):
  rcfg = METHOD_DATA_RECORD
  if cfg in md_ct_replays:
    rcfg = MD_CT_RECORD%( float(cfg.split('_')[-1]) )
  elif cfg in steady_md_ct_replays:
    #rcfg = MD_CT_RECORD%( float(cfg.split('_')[-1]) )
    rcfg = STEADY_MD_CT_RECORD%( float(cfg.split('_')[-1]) )
  elif cfg in xss_md_ct_replays:
    rcfg = XSS_MD_CT_RECORD%( float(cfg.split('_')[-1]) )
  elif cfg in md_replays:
    rcfg = MD_RECORD%( int(cfg.split('_')[1].strip('n')) )
  return rcfg

def isIterOpt(opt):
  if re.match(iterOptRE, opt):
    return True
  return False

def hasIterOpt(opts):
  return any ( [ True if isIterOpt(opt) else False for opt in opts ] )

def getObjRate(objrate, cfg):
  if objrate != None:
    return objrate

  if objrated.has_key(cfg):
    return objrated[cfg]

  return defobjrate

def getHeapTuneArgs(bench, cfg):
  x = getGCSpaceInfo(bench, HEAP_TRIAL, 0, colored=False)
  maxrec = sorted( [ (i, (rec[HEAP_BEFORE][EDEN][1]      + \
                          rec[HEAP_BEFORE][SURVIVOR][1]  + \
                          rec[HEAP_BEFORE][TENURED][1]))   \
                     for i,rec in enumerate(x) ], \
                    key=itemgetter(1), reverse=True)[0][0]

  eden = x[maxrec][HEAP_BEFORE][EDEN][1]
  surv = x[maxrec][HEAP_BEFORE][SURVIVOR][1]
  tend = x[maxrec][HEAP_BEFORE][TENURED][1]

  mxtd = sorted( [ (i, (rec[HEAP_BEFORE][TENURED][0])) \
                          for i,rec in enumerate(x) ], \
                    key=itemgetter(1), reverse=True)[0][0]

  max_tenured = x[mxtd][HEAP_BEFORE][TENURED][0]
  eden += max_tenured
  surv += max_tenured

  if cfg in ([COLOR_ALLOC_TUNE, COLOR_ALLOC_TUNE_V2, COLOR_ALLOC_TUNE_X] + occfgs):
    #eden = min([eden*2,(GB(14)/1000)])
    #surv = min([surv*2,(GB(14)/1000)])
    eden = min([eden*2,(GB(14)/1000)])
    surv = min([surv*2,(GB(14)/1000)])

  surv_rat     = float(eden)/float(surv)
  # so we dont screw up sizes going to CA_REF_10 and CA_REF_20
  if cfg in ([COLOR_ALLOC_TUNE, COLOR_ALLOC_TUNE_V2, COLOR_ALLOC_TUNE_X,
             COLOR_ILV_TUNE] + occfgs)  \
  and bench in jvm2008_defs[5:]:
    new_size     = min([eden+surv,    (GB(16)/1000)])
    max_new_size = min([(eden+surv)*3,(GB(16)/1000)])
  else:
    new_size     = eden+surv
    max_new_size = eden*3

  args = [ 
    optionSet(NewSize, "%dK"%new_size),
    optionSet(MaxNewSize, "%dK"%(max_new_size)),
    optionSet(SurvivorRatio, max([1,int(surv_rat)])),
    minHeapSize("30g"),
    maxHeapSize("30g")
    #optionSet(NewSize, "6g"),
    #optionSet(SurvivorRatio, "2"),
    #minHeapSize("8g"),
    #maxHeapSize("8g")
  ]

  # this is to prevent the heap from blowing up on some benchmarks
  # (specifically, montecarlo in jgf) because we cannot do full GC on some of
  # our experimental runs
  #
  #if bench in badtunes:
  #  args += [optionSet(SurvivorRatio, "1")]
  #else:
  #  args += [optionSet(SurvivorRatio, max([1,int(surv_rat)]))]

  return args

def startEmon(bench, cfg, iter, verbose=V1):

  edashvcmd = emoncmd + " -v"
  execmd(edashvcmd, emonDashvFile(bench, cfg, iter), verbose=verbose)

  edashMcmd = emoncmd + " -M"
  execmd(edashMcmd, emonDashMFile(bench, cfg, iter), verbose=verbose)

  start_time = time.time()
  ecmd   = emoncmd + " -i %s" % emonEventsFile(bench, cfg, iter)
  execmd(ecmd, emonRawFile(bench, cfg, iter), wait=False, verbose=verbose)

  edashvf = open(emonDashvFile(bench, cfg, iter), 'a')
  edashvf.write("emon started: %ld\n" % long((start_time * 1000.0)))
  edashvf.close()
  return

def stopEmon(verbose=V1):

  print "killing emon"
  execmd(killemoncmd, wait=False, verbose=verbose)
  return

def startPGov(bench, cfg, iter, powrate=defpowrate, verbose=V1):

  pgovcmd = powergovcmd + (" -e %d" % int(powrate*1000))
  execmd(pgovcmd, pgovRawFile(bench, cfg, iter), wait=False, verbose=verbose)
  return

def stopPGov(verbose=V1):

  execmd(killpgovcmd, verbose=verbose)
  return

def startPcmPower(bench, cfg, iter, powrate=defpowrate, verbose=V1):

  pcmpowcmd = (pcmpowercmd % int(powrate))
  execmd(pcmpowcmd, pcmPowerFile(bench, cfg, iter), wait=False, verbose=verbose)

def startPcmMemory(bench, cfg, iter, powrate=defpowrate, verbose=V1):
  pcmmemcmd = (pcmmemorycmd % int(powrate))
  execmd(pcmmemcmd, pcmMemoryFile(bench, cfg, iter), wait=False, verbose=verbose)

def startPcmSelfRefresh(bench, cfg, iter, powrate=defpowrate, verbose=V1):

  pcmsr = (pcmsrcmd % int(powrate))
  execmd(pcmsr, pcmSelfRefreshFile(bench, cfg, iter), wait=False, verbose=verbose)

def startPcm(bench, cfg, iter, powrate=defpowrate, verbose=V1):
  locpcmcmd = (pcmcmd % int(powrate))
  execmd(locpcmcmd, pcmFile(bench, cfg, iter), wait=False, verbose=verbose)
  return

def stopPcmPower(verbose=V1):
  execmd(killpcmpowcmd, verbose=verbose)
  return

def stopPcmSelfRefresh(verbose=V1):
  execmd(killpcmpowcmd, verbose=verbose)
  return

def stopPcmMemory(verbose=V1):
  execmd(killpcmmemcmd, verbose=verbose)
  return

def stopPcm(verbose=V1):
  execmd(killpcmcmd, verbose=verbose)
  return

def startPTITSample(bench, cfg, iter, rate=defpowrate, verbose=V1):

  ptitscmd = (ptitsamplecmd % int(rate))
  execmd(ptitscmd, ptitSampleFile(bench, cfg, iter), wait=False, verbose=verbose)

def stopPTITSample(verbose=V1):
  gproc = Popen("ps ux | grep ptit-sample.py | grep python", shell=True, stdout=PIPE)
  os.waitpid(gproc.pid,0)
  gout = gproc.stdout.readlines()
  for line in gout:
    pts = line.split()
    pid = pts[1]
    if not "grep" in pts:
      if verbose > V0:
        print "stopping ptit-sample.py: ",
      execmd(("kill -9 %s" % pid), verbose=verbose)
  return

def startMscram(bench, cfg, verbose=V1):

  numacfg, mcolorcfg, sleeptime, mscramargs = mscramopts[cfgs[cfg][BC_MSCRAMCFG]]

  numapt = ""
  if numacfg:
    if numaopts.has_key(numacfg): 
      numapt = numactlcmd + " " + __getNumactlOpts(numacfg)
    else:
      print "startMscram: invalid numacfg: %s" % numacfg
      raise SystemExit(1)

  mcolorpt = ""
  if mcolorcfg:
    if mcoloropts.has_key(mcolorcfg):
     mcolorpt = mcolorctlcmd + " " + __getMcolorctlOpts(mcolorcfg)
    else:
      print "startMscram: invalid mcolorcfg: %s" % mcolorcfg
      raise SystemExit(1)

  mscrampt = mscramblecmd + " " + " ".join(mscramargs)

  mcmd = " ".join([numapt,mcolorpt,mscrampt])
  scmd = "sleep %d" % sleeptime
  execmd(dropcachescmd, shell=True, verbose=verbose)
  execmd(mcmd, mscramOutFile(bench, cfg), wait=False, verbose=verbose)
  execmd(scmd, verbose=verbose)
  return

def stopMscram(verbose=V1):

  execmd(killmscramcmd, wait=False, verbose=verbose)
  return

def doTrayctlBefore(cfg, verbose=V1):

  topts = " ".join(trayopts[cfgs[cfg][BC_TRAYCFG]][0])
  tcmd  = " ".join([trayctlcmd,topts])
  execmd(tcmd,verbose=verbose)

def doTrayctlAfter(cfg, verbose=V1):

  topts = " ".join(trayopts[cfgs[cfg][BC_TRAYCFG]][1])
  tcmd  = " ".join([trayctlcmd,topts])
  execmd(tcmd,verbose=verbose)

def __getNumactlOpts(numacfg):
  opts = []
  if numaopts.has_key(numacfg):
    opts = numaopts[numacfg]
  return " ".join(opts)

def __getMcolorctlOpts(mcolorcfg):
  opts = []
  if mcoloropts.has_key(mcolorcfg):
    opts = mcoloropts[mcolorcfg]
  return " ".join(opts)

def getNumactlOpts(cfg):
  numacfg = cfgs[cfg][BC_NUMACFG]
  return __getNumactlOpts(numacfg)

def getMcolorctlOpts(cfg):
  mcolorcfg = cfgs[cfg][BC_MCOLORCFG]
  return __getMcolorctlOpts(mcolorcfg)

def getJavaOptsPart(bench, cfg, iter, objrate=None, objcrash=False, \
                    xjopts=[], agethresh=defagethresh, \
                    refthresh=defrefthresh, cutoff=defcutoff):
  runcfg  = cfgs[cfg][BC_RUNCFG]
  profcfg = cfgs[cfg][BC_PROFCFG]

  jopts = []
  if javaRunOpts.has_key(runcfg):
    jopts += javaRunOpts[runcfg]
  else:
    jopts += javaRunOpts[DEFAULT]

  if javaProfOpts.has_key(profcfg):
    jopts += javaProfOpts[profcfg]

  if cfgs[cfg][BC_RUNCFG] in tunecfgs:
    print bench, cfgs[cfg][BC_RUNCFG]
    jopts += getHeapTuneArgs(bench, cfgs[cfg][BC_RUNCFG])

  if custbench_cfgs.has_key(bench):
    jopts += custbench_cfgs[bench][0]

  # heap size is already specified for custom benches in custbench_cfgs dict
  #if needHeapSizes(cfg) and not bench in customs:
    #jopts += [ minHeapSize("28g"), maxHeapSize("28g") ]
    #hs = heap_sizes[bench]
    #jopts += [ minHeapSize(hs), maxHeapSize(hs) ]
    #jopts += [ optionSet("NewRatio", 200) ]
    #jopts += [ optionSet(NewSize,    (to_bytes(hs) / 100)) ]
    #jopts += [ optionSet(MaxNewSize, (to_bytes(hs) / 100)) ]
    #jopts += [ optionSet(MinSurvivorRatio,  1) ]
    #jopts += [ optionSet(InitialSurvivorRatio, 1) ]

  if objectOrganizer(cfg):
    jopts += [ optionSet(ObjectLayoutInterval, str(objrate)) ]
    if not profcfg in [OBJ_INFO_AT0_RT0, OBJ_INFO_XXX_RT0]:
      jopts += [ optionSet(ColorAgeThreshold,  str(agethresh)) ]
      jopts += [ optionSet(ColorRefThreshold,  str(refthresh)) ]

  if objectInfoEnabled(cfg):

    # mrj -- fix this
    if not gcvalObjectInfo(cfg):
      jopts += [ optionSet(ObjectInfoInterval, str(getObjRate(objrate, cfg))),
               ]

    if objcrash:
      jopts += [ optionOn(CrashOnObjectInfoDump) ]

    jopts += [ optionSet(ObjectInfoLog, objInfoLog(bench, cfg, iter)) ]

    if trimObjectInfo(cfg):
      jopts += [ optionSet(ObjectAllocationLog, objAllocLog(bench, cfg, iter)) ]
      jopts += [ optionSet(DeadObjectLog,       deadObjLog(bench, cfg, iter))  ]

  if objectAddrInfoEnabled(cfg):
    jopts += [ optionSet(ObjectAddressInfoLog,      objAddrInfoLog(bench, cfg, iter)),
               optionSet(ObjectAddressInfoBin,      objAddrInfoBin(bench, cfg, iter)),
               optionSet(KlassRecordInfoLog,        klassRecordInfoLog(bench, cfg, iter)),
               optionSet(KlassRecordInfoBin,        klassRecordInfoBin(bench, cfg, iter)),
               optionSet(KlassMapLog,               klassMapLog(bench, cfg, iter)),
               optionSet(ObjectAddressInfoInterval, str(getObjRate(objrate, cfg)))
             ]

  if allocPointInfoEnabled(cfg):
    jopts += [ optionSet(AllocPointMapLog,    apMapLog(bench, cfg, iter))    ]
    jopts += [ optionSet(AllocPointInfoLog,   apInfoLog(bench, cfg, iter))   ]
    jopts += [ optionSet(AllocPointInfoBin,   apInfoBin(bench, cfg, iter))   ]

  if stackSamplesEnabled(cfg):
    jopts += [ optionSet(StackSampleLog, stackSampleLog(bench, cfg, iter)) ]

  if cfg in [METHOD_KAL_INFO, METHOD_KAL_INFOX, METHOD_KAL_INFONX]:
    jopts += [ optionSet(MethodKlassAccessListsLog, mkalsLog(bench, cfg, iter)) ]

  if cfg in ([METHOD_DATA_RECORD] +
              [x for x in md_ct_records        if not x == MD_CT_RECORD%0] +
              [x for x in steady_md_ct_records if not x == STEADY_MD_CT_RECORD%0] +
              [x for x in xss_md_ct_records    if not x == XSS_MD_CT_RECORD%0] +
              [x for x in md_records           if not x == MD_RECORD%0]):
    jopts += [ optionSet(ReplayDataLogFile, replayDataLogFile(bench, cfg, iter)),
               optionSet(ReplayDataBinFile, replayDataBinFile(bench, cfg, iter)),
               optionSet(ReplayOopsFile,    replayOopsFile(bench, cfg, iter))
             ]

  if cfg in ([METHOD_DATA_REPLAY, STEADY_CCF_REPLAY, REPLAY_NO_RESET,
              STEADY_REPLAY, STEADY_REPLAY_LC] +
              [x for x in md_ct_replays        if not x == MD_CT_REPLAY%0] +
              [x for x in steady_md_ct_replays if not x == STEADY_MD_CT_REPLAY%0] +
              [x for x in xss_md_ct_replays    if not x == XSS_MD_CT_REPLAY%0] +
              [x for x in md_replays           if not x == MD_REPLAY%0]):
    rcfg = getRecordConfig(cfg)
    jopts += [ optionSet(ReplayDataLogFile, replayDataLogFile(bench, rcfg, 0)),
               optionSet(ReplayDataBinFile, replayDataBinFile(bench, rcfg, 0)),
               optionSet(ReplayOopsFile,       replayOopsFile(bench, rcfg, 0))
             ]

  if needCorFile(cfg):
    if not corcfgs.has_key(cfgs[cfg][BC_RUNCFG]):
      print "%s,%s has no corcfg mapping!" % cfg, cfgs[cfg][BC_RUNCFG]
      raise SystemExit(1)
    mkdir_p(expdir(bench, cfg))
    writeCorFile(bench, corcfgs[cfgs[cfg][BC_RUNCFG]])
    copyfile(corFile(bench, corcfgs[cfgs[cfg][BC_RUNCFG]]), 
             corFile(bench, cfg))
    jopts += [ optionSet(CompileCommandFile, corFile(bench, cfg)) ]

  elif needAPCCmds(cfg):
    if not apcfgs.has_key(cfgs[cfg][BC_RUNCFG]):
      print "%s has no apcfg mapping!" % cfg
      raise SystemExit(1)
#    try:
#      copyfile(apcCmdsFile(bench, apcfgs[cfg]), apcCmdsFile(bench, cfg))
#    except (IOError, OSError) as exc:
#      if exc.errno == errno.ENOENT:
#        print "No apc-cmds file for: %s-%s" % (bench, apcfgs[cfg])
#      raise
    #if bench in dacapos and not bench == "lusearch-large":
    apbench = getAPBench(bench, cfg)

    if customAPC(cfg):
      cutoff = float("0.%s" % cfg.split('_')[-1])
      style  = REFS if 'ref' in cfg.split('_') else SIZE
      #cutoff = float("0.%s" % cfg.split('_')[-1]) * 100.0
      #cutoff = 2000000
      #style  = REFS_PER_VAL
      #cutoff = 0.05
      #style  = REFS
      #print apbench, apcfgs[cfgs[cfg][BC_RUNCFG]], cutoff, style
      writeKnapsackApcCmdsFile(apbench, apcfgs[cfgs[cfg][BC_RUNCFG]], \
                               cutoff=cutoff, style=style)
    else:
      #cutoff = 2000000
      #style  = REFS_PER_VAL
      #cutoff = 1.0
      if not bench in storebenches:
        writeKnapsackApcCmdsFile(apbench, apcfgs[cfgs[cfg][BC_RUNCFG]], \
                                 cutoff=cutoff, style=REFS)
    copyfile(apcCmdsFile(apbench, apcfgs[cfgs[cfg][BC_RUNCFG]]), \
                         apcCmdsFile(bench, cfg))
    jopts += [ optionSet(CompileCommandFile, apcCmdsFile(bench, cfg)) ]
  elif cfg in [BWBENCH_COLOR, BWBENCH_COLOR_ILV]:
    jopts += [ optionSet(CompileCommandFile, bwColorAPCFile(bench)) ]

  return " ".join(jopts + xjopts)

def getJvm2008Hopts(bench, cfg, iter, xhopts=[], powrate=defpowrate):
  runcfg  = cfgs[cfg][BC_RUNCFG]
  profcfg = cfgs[cfg][BC_PROFCFG]

  hopts = []
  if bench in jvm2008bts or bench in scimarkbts:
    tmpbench = bench.split('-')[0]
    if tmpbench in scihuges:
      tmpbench = ".".join(tmpbench.split(".")[:2] + ["large"])
    hopts.append(tmpbench)
  elif bench in scihuges:
    tmpbench = ".".join(bench.split(".")[:2] + ["large"])
    hopts.append(tmpbench)
  else:
    hopts.append(bench)

  hopts.append(" ".join([ IgnoreKitValidation ]))
  if jvm2008RunOpts.has_key(runcfg):
    hopts.append(" ".join(jvm2008RunOpts[runcfg]))

  if jvm2008ProfOpts.has_key(profcfg):
    hopts.append(" ".join(jvm2008ProfOpts[profcfg]))

  if hprofEnabled(cfg):
    rdatadir = getdir(rawDataDir(bench, cfg, iter))
    hopts.append(specjvmOpt(specjvmRawDataDir, rdatadir.rstrip('/')))

  if hemonEnabled(cfg):
    hopts.append(specjvmOpt(specjvmEmonEventsFile, \
                 emonEventsFile(bench, cfg, iter)))

  if hpgovEnabled(cfg):
    hopts.append(specjvmOpt(specjvmPowerGovFreq, ("%d" % int(powrate*1000))))

  if bench in jvm2008bts or bench in scimarkbts:
    numbts = int(bench.split('-')[1].strip('bt'))
    hopts.append(("-bt %d" % numbts))

  if objectInfoEnabled(cfg) or objectAddrInfoEnabled(cfg):
    hopts.append("-wt 0")
  else:
    hopts.append(("-wt %d" % jvm2008_warmup_time))
  hopts.append(" ".join(xhopts))
  return " ".join(hopts)

def getDacapoHopts(bench, cfg, iter, xhopts=[]):
  runcfg  = cfgs[cfg][BC_RUNCFG]
  profcfg = cfgs[cfg][BC_PROFCFG]

  hopts = []
  hopts.append("%s -s %s" % tuple(bench.split("-")))

  if dacapoRunOpts.has_key(runcfg):
    for hopt in dacapoRunOpts[runcfg]:
      if isIterOpt(hopt) and hasIterOpt(xhopts):
        continue 
      hopts.append(hopt)

  if not hasIterOpt(hopts) and not hasIterOpt(xhopts):
    hopts.append(iteropt(defdaciters))

  hopts.append(" ".join(xhopts))
  if dacapoProfOpts.has_key(profcfg):
    hopts.append(" ".join(dacapoProfOpts[profcfg]))

  hopts.append(scratchdiropt(scratchdir(bench, cfg, iter)))
  return " ".join(hopts)

def getJGFHopts(bench, cfg, iter, xhopts=[]):
  hopts = []
  pts   = bench.split('-')
  name  = pts[0]
  size  = jgfSizes[pts[1]]
  hopts.append("%s %d" % (name, size))
  if bench in jgf_multis:
    if bench in jgfmultibts:
      hopts.append("%d" % int(pts[2].strip('bt')))
    else:
      hopts.append("1")
  if objectInfoEnabled(cfg):
    hopts.append("0")
  else:
    hopts.append("%d" % jgf_warmup_time)
  if size >= 90:
    hopts.append("0")
  else:
    hopts.append("%d" % jgf_run_time)
  return " ".join(hopts)

def getCustomHopts(bench, xhopts=[]):
  hopts = []
  pts = bench.split('-')
  name = pts[0]
  size = pts[1]
  hopts.append("-classpath %s %s" % (customDir(name), name))
  if bench in custbench_cfgs.keys():
    hopts.append(" ".join(custbench_cfgs[bench][1]))
  else:
    print "invalid custom benchmark: %s" % bench
    raise SystemExit(1)
  return " ".join(hopts)

def getHarnessPart(bench, cfg, iter, xhopts=[], powrate=defpowrate):

  hparts = []
  if bench in jvm2008s:
    hparts.append(specjvmOpt(specjvmHomeDir, jvm2008sdir))
    if bench in scihuges:
      hparts.append( ("-jar %s" % hugejvm2008jar) )
    else:
      hparts.append( ("-jar %s" % origjvm2008jar) )
    hparts.append(specjvmOpt(specjvmResDir, jvm2008results))
    hparts.append(specjvmOptFalse(specjvmRunInitialCheck))
    hparts.append( ("%s" % getJvm2008Hopts(bench, cfg, iter, xhopts=xhopts, \
                                           powrate=powrate)) )

  elif bench in dacapos:
    hparts.append("-jar %s" % dacapojar)
    hparts.append("%s" % getDacapoHopts(bench, cfg, iter, xhopts=xhopts))

  elif bench in customs:
    hparts.append("%s" % getCustomHopts(bench, xhopts=xhopts))
  
  elif bench in jgf_multis:
    hparts.append("-classpath %s %s" % (jgfmultidir, jgfharness))
    hparts.append("%s" % getJGFHopts(bench, cfg, iter, xhopts=xhopts))

  elif bench in jgf_singles:
    hparts.append("-classpath %s %s" % (jgfsingledir, jgfharness))
    hparts.append("%s" % getJGFHopts(bench, cfg, iter, xhopts=xhopts))

  else:
    print "getHarnessPart: invalid bench: %s" % bench
    raise SystemExit(1)

  return " ".join(hparts)

def javacmd(bench, cfg, iter, clcfg=None, xjopts=[], xhopts=[], \
            powrate=defpowrate, objrate=None, objcrash=False, \
            agethresh=defagethresh, refthresh=defrefthresh, cutoff=defcutoff):

  needCfgDel = False
  if not cfgs.has_key(cfg):
    if clcfg == None:
      print "you must specify clcfg for unknown cfg: %s" % cfg
      raise SystemExit(1)
    else:
      needCfgDel = True
      cfgs[cfg]  = clcfg

  cmdpts = []
  if cfgs[cfg][BC_NUMACFG] and rootAccess:
    cmdpts.append(numactlcmd + " " + getNumactlOpts(cfg))
  if cfgs[cfg][BC_MCOLORCFG] and rootAccess:
    cmdpts.append(mcolorctlcmd + " " + getMcolorctlOpts(cfg))

  if objectInfoEnabled(cfg):
    cmdpts.append(photspot)
  elif objectAddrInfoEnabled(cfg):
    cmdpts.append(ahotspot)
  elif coloredSpacesEnabled(cfg):
    cmdpts.append(thotspot)
  else:
    #cmdpts.append(defhotspot)
    cmdpts.append(ohotspot)
    #cmdpts.append(ohotspot)
    #cmdpts.append(ehotspot)

  cmdpts.append(getJavaOptsPart(bench, cfg, iter, objrate=objrate, \
                objcrash=objcrash, agethresh=agethresh, \
                refthresh=refthresh, xjopts=xjopts, cutoff=cutoff))
  cmdpts.append(getHarnessPart(bench, cfg, iter, xhopts=xhopts, \
                powrate=powrate))

  if needCfgDel:
    del cfgs[cfg]

  return " ".join(cmdpts)
  #return ("numactl --cpunodebind=1 --membind=1 " + " ".join(cmdpts))

def writeEmonEventsFile(bench, cfg, iter, emonevf=None, emonevts=[], \
                        emonrate=deferate, emonloops=defeloops, verbose=V1):

  try:
    emonf = open(emonEventsFile(bench, cfg, iter), 'w')
  except:
    print "error opening emon events file: %s" % \
          emonEventsFile(bench, cfg, iter)
    raise SystemExit(1)

  evtstr  = ""
  evtstr += "-u -t%2.4f -l%d -C (\n" % (emonrate, emonloops)
  for eev in emonevts:
    evtstr += "%s,\n" % eev
  evtstr += ")\n"

  emonf.write(evtstr)
  emonf.close()

def writeEmonEvents(bench, cfg, iter, emonevf=None, emonevts=[], \
                    emonrate=deferate, emonloops=defeloops, verbose=V1):
  if emonevf:
    if emonevts:
      print "warning: ignoring emonevts and using events file: %s" % emonevf
    try:
      execmd("cp %s %s" % (emonevf, emonEventsFile(bench, cfg, iter)), \
             verbose=verbose)
    except:
      print "error copying: %s to %s" % \
            (emonevf, emonEventsFile(bench, cfg, iter))
      raise SystemExit(1)
  else:
    writeEmonEventsFile(bench, cfg, iter, emonevts=emonevts, \
                        emonrate=emonrate, emonloops=emonloops, \
                        verbose=verbose)

def runiter(bench, cfg, iter, clcfg=None, xjopts=[], xhopts=[], \
            emonevf=None, emonevts=[], emonrate=deferate, \
            emonloops=defeloops, powrate=defpowrate, ppemets=[], \
            objrate=None, objcrash=False, agethresh=defagethresh, \
            refthresh=defrefthresh, pcmtype=PCM_POWER, dotime=True, \
            cutoff=defcutoff, verbose=V1):

  needStopEmon           = False
  needStopPGov           = False
  needStopPcmPower       = False
  needStopPcmMemory      = False
  needStopPcmSelfRefresh = False
  needStopPcm            = False
  needStopMscram         = False
  needStopPTITSample     = False
  emonStarted            = False

  needCfgDel = False
  if not cfgs.has_key(cfg):
    if clcfg == None:
      print "you must specify clcfg for unknown cfg: %s" % cfg
      raise SystemExit(1)
    else:
      needCfgDel = True
      cfgs[cfg]  = clcfg
      print "here"

  if emonEnabled(cfg) and not os.path.exists("/dev/emon"):
    print "Error: /dev/emon not present. Install emon."
    raise SystemExit(1)

  getdir(iterdir(bench, cfg, iter), create=True)

  if profilingEnabled(cfg):
    getdir(rawDataDir(bench, cfg, iter), create=True)

  if doDropCaches(cfg):
    execmd(dropcachescmd, shell=True, verbose=verbose)
    #execmd("sleep 10", verbose=verbose)

  # start outside emon
  if oemonEnabled(cfg):
    try:
      getdir(rawDataDir(bench, cfg, iter), create=True)
      writeEmonEvents(bench, cfg, iter, emonevf=emonevf, emonevts=emonevts, \
                      emonrate=emonrate, emonloops=emonloops, verbose=verbose)
      emonStarted = needStopEmon = True
      startEmon(bench, cfg, iter, verbose=verbose)
    except Exception as e:
      print "error starting outside emon for (%s-%s-i%d)!" \
            % (bench, cfg, iter)
      raise e

  # start outside power_gov
  if opgovEnabled(cfg):
    try:
      startPGov(bench, cfg, iter, powrate=powrate, verbose=verbose)
      needStopPGov = True
    except Exeption as e:
      print "error starting outside power_gov for (%s-%s-i%d)!" \
            % (bench, cfg, iter)
      raise e

  # start pcm tool
  if pcmEnabled(cfg):
    if pcmtype == PCM_POWER:
      try:
        startPcmPower(bench, cfg, iter, powrate=powrate, verbose=verbose)
        needStopPcmPower = True
      except Exception as e:
        print "error starting outside pcm-power.x for (%s-%s-i%d)!" \
              % (bench, cfg, iter)
        raise e
    elif pcmtype == PCM_MEMORY:
      try:
        startPcmMemory(bench, cfg, iter, powrate=powrate, verbose=verbose)
        needStopPcmMemory = True
      except Exception as e:
        print "error starting outside pcm-memory.x for (%s-%s-i%d)!" \
              % (bench, cfg, iter)
        raise e
    elif pcmtype == PCM_SELF_REFRESH:
      try:
        startPcmSelfRefresh(bench, cfg, iter, powrate=powrate, verbose=verbose)
        needStopPcmSelfRefresh = True
      except Exception as e:
        print "error starting outside pcm-power.x for (%s-%s-i%d)!" \
              % (bench, cfg, iter)
        raise e
    elif pcmtype == PCM:
      try:
        startPcm(bench, cfg, iter, powrate=powrate, verbose=verbose)
        needStopPcm = True
      except Exception as e:
        print "error starting outside pcm.x for (%s-%s-i%d)!" \
              % (bench, cfg, iter)
        raise e

  if ptitSampleEnabled(cfg):
    try:
      startPTITSample(bench, cfg, iter, rate=powrate, verbose=verbose)
      needStopPTITSample = True
    except Exception as e:
      print "error starting outside ptit-sample for (%s-%s-i%d)!" \
            % (bench, cfg, iter)
      raise e

  # create the emon events file
  if not emonStarted and emonEnabled(cfg):
    writeEmonEvents(bench, cfg, iter, emonevf=emonevf, emonevts=emonevts, \
                    emonrate=emonrate, emonloops=emonloops, verbose=verbose)
    emonStarted = True

  if bench in idles:
    idle_time = int(bench.split('-')[1])
    cmd = ("sleep %d" % idle_time)
  else:
    cmd = javacmd(bench, cfg, iter, clcfg=clcfg, xjopts=xjopts, \
                  xhopts=xhopts, powrate=powrate, objrate=objrate, \
                  agethresh=agethresh, refthresh=refthresh, \
                  objcrash=objcrash, cutoff=cutoff)
  outfile = rawoutfile(bench, cfg, iter)

  # these commands are pretty long and complicated - print them out
  cmdoutf = open(cmdoutfile(bench,cfg,iter),'w')
  print >> cmdoutf, "%s > %s" % (cmd, outfile)
  cmdoutf.close()

  p = execmd(cmd, outfile, dotime=dotime, verbose=verbose)

  try:
    if needStopEmon:
      stopEmon(verbose=verbose)
  except Exception as e:
    print "error stopping outside emon for (%s-%s-%d)!" % (bench, cfg, iter)
    raise e

  try:
    if needStopPGov:
      stopPGov(verbose=verbose)
  except Exception as e:
    print "error stopping power_gov for (%s-%s-i%d)!" % (bench, cfg, iter)
    raise e

  try:
    if needStopPcmPower:
      stopPcmPower(verbose=verbose)
  except Exception as e:
    print "error stopping pcm-power.x for (%s-%s-i%d)!" % (bench, cfg, iter)
    raise e

  try:
    if needStopPcmMemory:
      stopPcmMemory(verbose=verbose)
  except Exception as e:
    print "error stopping pcm-memory.x for (%s-%s-i%d)!" % (bench, cfg, iter)
    raise e

  try:
    if needStopPcmSelfRefresh:
      stopPcmSelfRefresh(verbose=verbose)
  except Exception as e:
    print "error stopping pcm-power.x for (%s-%s-i%d)!" % (bench, cfg, iter)
    raise e

  try:
    if needStopPcm:
      stopPcm(verbose=verbose)
  except Exception as e:
    print "error stopping pcm.x for (%s-%s-i%d)!" % (bench, cfg, iter)
    raise e

  try:
    if needStopPTITSample:
      stopPTITSample(verbose=verbose)
  except Exception as e:
    print "error stopping ptit-sample.py for (%s-%s-i%d)!" % (bench, cfg, iter)
    raise e

  # pretty print emon metrics
  if ppemets and emonEnabled(cfg):
    ppemetsBench(bench, cfg, iter, ppemets)

  #if trimObjectInfo(cfg):
  #  sortObjectAllocationLog(bench, cfg, iter)
  
  if needCfgDel:
    del cfgs[cfg]

  return p.returncode

def runexp(bench, cfg, iters=defiters, xjopts=[], xhopts=[], clcfg=None, \
           miniters=defminiters, emonevf=None, emonevts=[], \
           emonrate=deferate, emonloops=defeloops,  powrate=defpowrate, \
           ppemets=[], objrate=None, objcrash=False, \
           agethresh=defagethresh, refthresh=defrefthresh, \
           pcmtype=PCM_POWER, freshpcm=False, dotime=True, cutoff=defcutoff, \
           dismscram=False, keepmscram=False, skipiter=0, never_fail=False,
           verbose=V1):

  needMemCopies = []
  needSRCopies  = []
  needPcmCopies = []
  if pcmtype == PCM_MEMORY and not freshpcm:
    for path in glob(expdir(bench, cfg) + "i*"):
      iter = int(path[-1])
      execmd("cp -r %s %s" % (path, ("/tmp/orig-i%d" % iter)))
      needMemCopies.append(iter)
  elif pcmtype == PCM_SELF_REFRESH and not freshpcm:
    for path in glob(expdir(bench, cfg) + "i*"):
      iter = int(path[-1])
      execmd("cp -r %s %s" % (path, ("/tmp/orig-i%d" % iter)))
      needSRCopies.append(iter)
  elif pcmtype == PCM and not freshpcm:
    for path in glob(expdir(bench, cfg) + "i*"):
      iter = int(path[-1])
      execmd("cp -r %s %s" % (path, ("/tmp/orig-i%d" % iter)))
      needPcmCopies.append(iter)

  if skipiter == 0:
    getdir(expdir(bench, cfg), create=True, clean=True)

  if verbose > V0:
    print "exp: %s-%s" % (bench, cfg)

  nfailed  = 0
  miniters = min(iters, miniters)

  needCfgDel = False
  if not cfgs.has_key(cfg):
    if clcfg == None:
      print "you must specify clcfg for unknown cfg: %s" % cfg
      raise SystemExit(1)
    else:
      needCfgDel = True
      cfgs[cfg]  = clcfg

  if trayctlEnabled(cfg):
    doTrayctlBefore(cfg, verbose=verbose)

  if mscramEnabled(cfg) and not dismscram:
    startMscram(bench, cfg, verbose=verbose)

#  if not dismscram:
#    needStopMscram = False
#    if mscramEnabled(cfg):
#      startMscram(bench, cfg, verbose=verbose)
#      if not keepmscram:
#        needStopMscram = True

  for iter in range(skipiter,iters):

    if verbose > V0:
      print "".ljust(2), ("i%d" % iter).ljust(4), "--> ",
      if verbose > V1:
        print ""

    if never_fail:
      success = False
      while not success:
        rm_rf(iterdir(bench, cfg, iter))
        returncode = runiter(bench, cfg, iter, xjopts=xjopts, xhopts=xhopts, \
                             clcfg=clcfg, emonevf=emonevf, emonevts=emonevts, \
                             emonrate=emonrate, emonloops=emonloops, \
                             powrate=powrate, ppemets=ppemets, objrate=objrate,\
                             objcrash=objcrash, agethresh=agethresh, \
                             refthresh=refthresh, pcmtype=pcmtype, dotime=dotime, \
                             cutoff=cutoff, verbose=verbose)

        if verbose > V0:
          if returncode:
            print "returned error code %d - retrying ..." % returncode
          else:
            if dotime:
              elapsed,rss = getUnixTime(bench, cfg, iter)
              result = getHarnessTime(bench, cfg, iter)
              if type(elapsed) is float and type(rss) is int:
                print "elapsed: %2.4f, rss(KB): %-12d" % (elapsed,rss)
              else:
                print "returned bad result: %s,%s - retrying ..." % (elapsed,rss)
              if type(result) is float:
                success = True
              else:
                print "retrying ..."
            else:
              result = getHarnessTime(bench, cfg, iter)
              if type(result) is float:
                print "%2.4f" % result
                success = True
              else:
                print "returned bad result: %s - retrying ..." % result
    else:
      rm_rf(iterdir(bench, cfg, iter))
      returncode = runiter(bench, cfg, iter, xjopts=xjopts, xhopts=xhopts, \
                           clcfg=clcfg, emonevf=emonevf, emonevts=emonevts, \
                           emonrate=emonrate, emonloops=emonloops, \
                           powrate=powrate, ppemets=ppemets, objrate=objrate,\
                           objcrash=objcrash, agethresh=agethresh, \
                           refthresh=refthresh, pcmtype=pcmtype, dotime=dotime, \
                           cutoff=cutoff, verbose=verbose)

      if verbose > V0:
        if returncode:
          print "returned error code %d" % returncode
          nfailed += 1
        else:
          if dotime:
            elapsed,rss = getUnixTime(bench, cfg, iter)
            if type(elapsed) is float and type(rss) is int:
              print "elapsed: %2.4f, rss(KB): %-12d" % (elapsed,rss)
            else:
              print "returned bad result: %s,%s" % (elapsed,rss)
              nfailed += 1
          else:
            result = getHarnessTime(bench, cfg, iter)
            if type(result) is float:
              print "%2.4f" % result
            else:
              print "returned bad result: %s" % result
              nfailed += 1

      if nfailed > (iters - miniters):
        break

    #if mscramEnabled(cfg):
    #  execmd("sleep 5", verbose=verbose)

  if verbose > V0:
    print "".ljust(2), "avg".ljust(4), "--> ",
    if dotime:
      stats1 = getUnixTimeStats(bench, cfg, iters=iters, miniters=1)
      if stats1 is None:
        print "no valid runs"
      else:
        estats1,mstats1 = stats1

        stats = getUnixTimeStats(bench, cfg, iters=iters, \
                                 miniters=miniters)
        if stats is None:
          print "elapsed: %2.4f, rss(KB): %-12d (of %d iterations)" % \
                (estats1[MEAN], mstats1[MEAN], len(getUnixTimes(bench,cfg,iters)))
        else:
          print "elapsed: %2.4f, rss(KB): %-12d" % \
                (stats[0][MEAN],stats[1][MEAN])
    else:
      htstats1 = getHarnessTimeStats(bench, cfg, iters=iters, miniters=1)
      if htstats1 is None:
        print "no valid runs"
      else:
        htstats = getHarnessTimeStats(bench, cfg, iters=iters, \
                                  miniters=miniters)
        if htstats is None:
          print "%2.4f (of %d iterations)" % \
                (htstats1[MEAN], len(getHarnessTimes(bench,cfg,iters)))
        else:
          print "%2.4f" % htstats[MEAN]

#  if not dismscram:
#    try:
#      if needStopMscram and not keepmscram:
#        stopMscram(verbose=verbose)
#    except:
#      print "error stopping mscramble for (%s-%s-i%d)!" % (bench, cfg, iter)
#      raise SystemExit(1)

  if mscramEnabled(cfg) and not keepmscram:
    try:
        stopMscram(verbose=verbose)
    except:
      print "error stopping mscramble for (%s-%s-i%d)!" % (bench, cfg, iter)
      raise SystemExit(1)

  if trayctlEnabled(cfg):
    doTrayctlAfter(cfg, verbose=verbose)

  # copy pcm files
  if needMemCopies:
    for iter in range(iters):
      tmpfile = ("/tmp/%s-i%d" % (pcmmemoryfile, iter))
      execmd(("cp %s %s" % (pcmMemoryFile(bench, cfg, iter), tmpfile)))

      tmpraw  = ("/tmp/%s-i%d" % (rawmemoryfile, iter))
      execmd(("cp %s %s" % (rawoutfile(bench, cfg, iter), tmpraw)))

      execmd(("rm -rf %s" % iterdir(bench, cfg, iter)))

    for iter in needMemCopies:

      execmd(("mv %s %s" % (("/tmp/orig-i%d" % iter), iterdir(bench, cfg, iter))))

      tmpfile = ("/tmp/%s-i%d" % (pcmmemoryfile, iter))
      if os.path.exists(tmpfile):
        dstfile = (rawDataDir(bench, cfg, iter) + pcmmemoryfile)
        execmd(("mv %s %s" % (tmpfile, dstfile)))

      tmpraw  = ("/tmp/%s-i%d" % (rawmemoryfile, iter))
      if os.path.exists(tmpraw):
        execmd(("mv %s %s" % (tmpraw, rawMemoryFile(bench, cfg ,iter))))

  if needSRCopies:
    for iter in range(iters):
      tmpfile = ("/tmp/%s-i%d" % (pcmsrfile, iter))
      execmd(("cp %s %s" % (pcmSelfRefreshFile(bench, cfg, iter), tmpfile)))

      tmpraw  = ("/tmp/%s-i%d" % (rawsrfile, iter))
      execmd(("cp %s %s" % (rawoutfile(bench, cfg, iter), tmpraw)))

      execmd(("rm -rf %s" % iterdir(bench, cfg, iter)))

    for iter in needSRCopies:

      execmd(("mv %s %s" % (("/tmp/orig-i%d" % iter), iterdir(bench, cfg, iter))))

      tmpfile = ("/tmp/%s-i%d" % (pcmsrfile, iter))
      if os.path.exists(tmpfile):
        dstfile = (rawDataDir(bench, cfg, iter) + pcmsrfile)
        execmd(("mv %s %s" % (tmpfile, dstfile)))

      tmpraw  = ("/tmp/%s-i%d" % (rawsrfile, iter))
      if os.path.exists(tmpraw):
        execmd(("mv %s %s" % (tmpraw, rawSelfRefreshFile(bench,cfg,iter))))

  if needPcmCopies:
    for iter in range(iters):
      tmpfile = ("/tmp/%s-i%d" % (pcmfile, iter))
      execmd(("cp %s %s" % (pcmFile(bench, cfg, iter), tmpfile)))
      execmd(("rm -rf %s" % iterdir(bench, cfg, iter)))
    for iter in needPcmCopies:
      execmd(("mv %s %s" % (("/tmp/orig-i%d" % iter), iterdir(bench, cfg, iter))))
      tmpfile = ("/tmp/%s-i%d" % (pcmfile, iter))
      if os.path.exists(tmpfile):
        dstfile = (rawDataDir(bench, cfg, iter) + pcmfile)
        execmd(("mv %s %s" % (tmpfile, dstfile)))

  if freshpcm:
    if pcmtype == PCM_MEMORY:
      for iter in range(iters):
        execmd(("mv %s %s" % (rawoutfile(bench, cfg, iter), rawMemoryFile(bench, cfg, iter))))
    elif pcmtype == PCM_SELF_REFRESH:
      for iter in range(iters):
        execmd(("mv %s %s" % (rawoutfile(bench, cfg, iter), rawSelfRefreshFile(bench, cfg, iter))))

  if needCfgDel:
    del cfgs[cfg]

def runexps(benches=jvm2008s, cfgs=defcfgs, iters=defiters, \
            clcfgs=None, xjopts=[], xhopts=[], emonevf=None, \
            emonevts=[], emonrate=deferate, emonloops=defeloops, \
            powrate=defpowrate, ppemets=[], objrate=None, \
            objcrash=False, agethresh=defagethresh, \
            refthresh=defrefthresh, pcmtype=None, dotime=True, \
            cutoff=defcutoff, dismscram=False, keepmscram=False, skipiter=0, \
            freshpcm=True, domscram=True, never_fail=False, verbose=V1):

  if clcfgs == None:
    clcfgs = [None for x in cfgs]

  pos = 0
  for bench in benches:
    for cfg,clcfg in zip(cfgs,clcfgs):

      exppcm = pcmtype
      if exppcm == None:
        exppcm = getProfCfg(cfg)

      pos += 1
      if exppcm == PCM_ENERGY_MODEL:

        dismscram = True
        if pos == 1 and domscram:
          dismscram = False

        runexp(bench, cfg, iters=iters, clcfg=clcfg, xjopts=xjopts, \
               xhopts=xhopts, emonevf=emonevf, emonevts=emonevts, \
               emonrate=emonrate, emonloops=emonloops, powrate=powrate, \
               ppemets=ppemets, objrate=objrate, objcrash=objcrash, \
               agethresh=agethresh, refthresh=refthresh, pcmtype=PCM_MEMORY, \
               freshpcm=True, dotime=dotime, cutoff=cutoff, dismscram=dismscram, \
               keepmscram=True, skipiter=skipiter, never_fail=never_fail, \
               verbose=verbose)

        keepmscram = True
        if pos == (len(benches)*len(cfgs)):
          keepmscram = False

        runexp(bench, cfg, iters=iters, clcfg=clcfg, xjopts=xjopts, \
               xhopts=xhopts, emonevf=emonevf, emonevts=emonevts, \
               emonrate=emonrate, emonloops=emonloops, powrate=powrate, \
               ppemets=ppemets, objrate=objrate, objcrash=objcrash, \
               agethresh=agethresh, refthresh=refthresh, pcmtype=PCM_SELF_REFRESH, \
               freshpcm=False, dotime=dotime, cutoff=cutoff, dismscram=True, \
               keepmscram=keepmscram, skipiter=skipiter, \
               never_fail=never_fail, verbose=verbose)
      else:

        dismscram = True
        if pos == 1 and domscram:
          dismscram = False
        keepmscram = True
        if pos == (len(benches)*len(cfgs)):
          keepmscram = False

        runexp(bench, cfg, iters=iters, clcfg=clcfg, xjopts=xjopts, \
               xhopts=xhopts, emonevf=emonevf, emonevts=emonevts, \
               emonrate=emonrate, emonloops=emonloops, powrate=powrate, \
               ppemets=ppemets, objrate=objrate, objcrash=objcrash, \
               agethresh=agethresh, refthresh=refthresh, pcmtype=exppcm, \
               freshpcm=freshpcm, dotime=dotime, cutoff=cutoff, dismscram=dismscram, \
               keepmscram=keepmscram, skipiter=skipiter, \
               never_fail=never_fail, verbose=verbose)

  if exppcm == PCM_ENERGY_MODEL:
    cpSRResults(benches=benches, cfgs=[cfg], iters=iters)

