#############################################################################
# functions for generating reports
#############################################################################
from lib.globals import *
from lib.utils import *
from math import sqrt

oihotcfg = [
  ("nvals",        9,  str.ljust, "%d",    None,             None, None),
  ("dur (ms)",    12,  str.ljust, "%d",    [DURATION],       None, WGT_AVG),
  ("size (KB)",   12,  str.rjust, "%d",    [USED_BYTES],     B2KB, WGT_AVG),
  ("ref+vm (KB)", 12,  str.rjust, "%d",    [HOT_SIZE],       B2KB, WGT_AVG),
  ("vm (KB)",     12,  str.rjust, "%d",    [VM_SIZE],        B2KB, WGT_AVG),
  ("pages",       12,  str.rjust, "%d",    [TOTAL_PAGES],    None, WGT_AVG),
  ("hco+vm (KB)", 12,  str.rjust, "%d",    [HCO_SIZE],       B2KB, WGT_AVG),
  ("hco ref %",   12,  str.rjust, "%5.4f", [HCO_REF_RATIO],  None, WGT_AVG),
  ("hco pages",   12,  str.rjust, "%d",    [HCO_PAGES],      None, WGT_AVG),
  ("hco pages %", 12,  str.rjust, "%5.4f", [HCO_PAGE_RATIO], None, WGT_AVG),
  ("sim pages",   12,  str.rjust, "%d",    [SIM_PAGES],      None, WGT_AVG),
  ("sim pages %", 12,  str.rjust, "%5.4f", [SIM_PAGE_RATIO], None, WGT_AVG)
]

oiappcfg = [
  ("nvals",        9,  str.ljust, "%d",    None,              None, None),
  ("dur (ms)",    12,  str.ljust, "%d",    [DURATION],        None, WGT_AVG),
  ("objects",     12,  str.rjust, "%d",    [APP_OBJECTS],     None, WGT_AVG),
  ("hot obj %",   12,  str.rjust, "%5.4f", [HLVM_OBJ_RATIO],  None, WGT_AVG),
  ("hco obj %",   12,  str.rjust, "%5.4f", [CLVM_OBJ_RATIO],  None, WGT_AVG),
  ("size (KB)",   12,  str.rjust, "%d",    [APP_SIZE],        B2KB, WGT_AVG),
  ("hot size %",  12,  str.rjust, "%5.4f", [HLVM_SIZE_RATIO], None, WGT_AVG),
  ("hco size %",  12,  str.rjust, "%5.4f", [CLVM_SIZE_RATIO], None, WGT_AVG),
  ("refs",        12,  str.rjust, "%d",    [APP_REFS],        None, WGT_AVG),
  ("hco ref %",   12,  str.rjust, "%5.4f", [HCO_REF_RATIO],   None, WGT_AVG),
]

oiNewObjectConfig = [
  ("nvals",      9,  str.ljust, "%d", None,                              None, None),
  ("dur (ms)",  12,  str.ljust, "%d", [DURATION],                        None, WGT_AVG),
  ("new obj",   12,  str.rjust, "%d", [NEW_APP_OBJECTS],                 None, SUM_AGG),
  ("never %",   12,  str.rjust, "%d", [VAL_FUTURE, NEVER_NEW,  OBJECTS], None, SUM_AGG),
  ("1_val %",   12,  str.rjust, "%d", [VAL_FUTURE, NEWV_1,     OBJECTS], None, SUM_AGG),
  ("2_val %",   12,  str.rjust, "%d", [VAL_FUTURE, NEWV_2,     OBJECTS], None, SUM_AGG),
  ("4_val %",   12,  str.rjust, "%d", [VAL_FUTURE, NEWV_4,  OBJECTS],    None, SUM_AGG),
  ("8_val %",   12,  str.rjust, "%d", [VAL_FUTURE, NEWV_8,  OBJECTS],    None, SUM_AGG),
  ("16_val %",  12,  str.rjust, "%d", [VAL_FUTURE, NEWV_16, OBJECTS],    None, SUM_AGG),
  ("max_val %", 12,  str.rjust, "%d", [VAL_FUTURE, NEWV_MAX,   OBJECTS], None, SUM_AGG)
]

oiNewSizeConfig = [
  ("nvals",      9,  str.ljust, "%d", None,                          None, None),
  ("dur (ms)",  12,  str.ljust, "%d", [DURATION],                    None, WGT_AVG),
  ("new obj",   12,  str.rjust, "%d", [NEW_APP_SIZE],                B2KB, SUM_AGG),
  ("never %",   12,  str.rjust, "%d", [VAL_FUTURE, NEVER_NEW, SIZE], B2KB, SUM_AGG),
  ("1_val %",   12,  str.rjust, "%d", [VAL_FUTURE, NEWV_1,    SIZE], B2KB, SUM_AGG),
  ("2_val %",   12,  str.rjust, "%d", [VAL_FUTURE, NEWV_2,    SIZE], B2KB, SUM_AGG),
  ("4_val %",   12,  str.rjust, "%d", [VAL_FUTURE, NEWV_4,    SIZE], B2KB, SUM_AGG),
  ("8_val %",   12,  str.rjust, "%d", [VAL_FUTURE, NEWV_8,    SIZE], B2KB, SUM_AGG),
  ("16_val %",  12,  str.rjust, "%d", [VAL_FUTURE, NEWV_16,   SIZE], B2KB, SUM_AGG),
  ("max_val %", 12,  str.rjust, "%d", [VAL_FUTURE, NEWV_MAX,  SIZE], B2KB, SUM_AGG)
]

oiNewObjectRatioConfig = [
  ("nvals",      9,  str.ljust, "%d", None,                                       None, None),
  ("dur (ms)",  12,  str.ljust, "%d", [DURATION],                                 None, WGT_AVG),
  ("new obj",   12,  str.rjust, "%d", [NEW_APP_OBJECTS],                          None, SUM_AGG),
  ("never %",   12,  str.rjust, "%s", [VAL_FUTURE, NEVER_NEW, NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("1_val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_1,    NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("2_val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_2,    NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("4_val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_4,    NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("8_val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_8,    NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("16_val %",  12,  str.rjust, "%s", [VAL_FUTURE, NEWV_16,   NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("max_val %", 12,  str.rjust, "%s", [VAL_FUTURE, NEWV_MAX,  NEW_APP_OBJ_RATIO], ratstr, SUM_AGG)
]

oiNewSizeRatioConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                                        None,   None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                                  None,   WGT_AVG),
  ("new obj",    12,  str.rjust, "%d", [NEW_APP_SIZE],                              B2KB,   SUM_AGG),
  ("never %",    12,  str.rjust, "%s", [VAL_FUTURE, NEVER_NEW, NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("1_val %",    12,  str.rjust, "%s", [VAL_FUTURE, NEWV_1,    NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("2_val %",    12,  str.rjust, "%s", [VAL_FUTURE, NEWV_2,    NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("4_val %",    12,  str.rjust, "%s", [VAL_FUTURE, NEWV_4,    NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("8_val %",    12,  str.rjust, "%s", [VAL_FUTURE, NEWV_8,    NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("16_val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_16,   NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("max_val %",  12,  str.rjust, "%s", [VAL_FUTURE, NEWV_MAX,  NEW_APP_SIZE_RATIO], ratstr, SUM_AGG)
]

oiHotObjectConfig = [
  ("nvals",      9,  str.ljust, "%d", None,                           None,   None),
  ("dur (ms)",  12,  str.ljust, "%d", [DURATION],                     None,   WGT_AVG),
  ("total obj", 12,  str.rjust, "%d", [APP_OBJECTS],                  None,   WGT_AVG),
  ("hot obj",   12,  str.rjust, "%d", [HLVM_OBJECTS],                 None,   WGT_AVG),
  ("never",     12,  str.rjust, "%d", [VAL_FUTURE, NEVER,   OBJECTS], None, WGT_AVG),
  ("1_val",     12,  str.rjust, "%d", [VAL_FUTURE, VAL_1,   OBJECTS], None, WGT_AVG),
  ("2_val",     12,  str.rjust, "%d", [VAL_FUTURE, VAL_2,   OBJECTS], None, WGT_AVG),
  ("4_val",     12,  str.rjust, "%d", [VAL_FUTURE, VAL_4,   OBJECTS], None, WGT_AVG),
  ("8_val",     12,  str.rjust, "%d", [VAL_FUTURE, VAL_8,   OBJECTS], None, WGT_AVG),
  ("16_val",    12,  str.rjust, "%d", [VAL_FUTURE, VAL_16,  OBJECTS], None, WGT_AVG),
  ("max_val",   12,  str.rjust, "%d", [VAL_FUTURE, VAL_MAX, OBJECTS], None, WGT_AVG)
]

oiHotSizeConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                        None,   None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                  None,   WGT_AVG),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                  B2KB,   WGT_AVG),
  ("hot size",   12,  str.rjust, "%d", [HLVM_SIZE],                 B2KB,   WGT_AVG),
  ("never",      12,  str.rjust, "%d", [VAL_FUTURE, NEVER,   SIZE], B2KB,   WGT_AVG),
  ("1_val",      12,  str.rjust, "%d", [VAL_FUTURE, VAL_1,   SIZE], B2KB,   WGT_AVG),
  ("2_val",      12,  str.rjust, "%d", [VAL_FUTURE, VAL_2,   SIZE], B2KB,   WGT_AVG),
  ("4_val",      12,  str.rjust, "%d", [VAL_FUTURE, VAL_4,   SIZE], B2KB,   WGT_AVG),
  ("8_val",      12,  str.rjust, "%d", [VAL_FUTURE, VAL_8,   SIZE], B2KB,   WGT_AVG),
  ("16_val",     12,  str.rjust, "%d", [VAL_FUTURE, VAL_16,  SIZE], B2KB,   WGT_AVG),
  ("max_val",    12,  str.rjust, "%d", [VAL_FUTURE, VAL_MAX, SIZE], B2KB,   WGT_AVG)
]

oiHotObjectRatioConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                                    None,   None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                              None,   WGT_AVG),
  ("total obj",  12,  str.rjust, "%d", [APP_OBJECTS],                           None,   WGT_AVG),
  ("hot obj %",  12,  str.rjust, "%s", [HLVM_OBJ_RATIO],                        ratstr, WGT_AVG),
  ("never %",    12,  str.rjust, "%s", [VAL_FUTURE, NEVER,   HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("1_val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_1,   HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("2_val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_2,   HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("4_val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_4,   HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("8_val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_8,   HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("16_val %",   12,  str.rjust, "%s", [VAL_FUTURE, VAL_16,  HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("max_val %",  12,  str.rjust, "%s", [VAL_FUTURE, VAL_MAX, HOT_OBJECT_RATIO], ratstr, WGT_AVG)
]

oiHotSizeRatioConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                                  None,   None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                            None,   WGT_AVG),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                            B2KB,   WGT_AVG),
  ("hot size %", 12,  str.rjust, "%s", [HLVM_SIZE_RATIO],                     ratstr, WGT_AVG),
  ("never %",    12,  str.rjust, "%s", [VAL_FUTURE, NEVER,   HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("1_val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_1,   HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("2_val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_2,   HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("4_val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_4,   HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("8_val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_8,   HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("16_val %",   12,  str.rjust, "%s", [VAL_FUTURE, VAL_16,  HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("max_val %",  12,  str.rjust, "%s", [VAL_FUTURE, VAL_MAX, HOT_SIZE_RATIO], ratstr, WGT_AVG)
]

oiNewObjectGroupConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                              None, None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                        None, WGT_AVG),
  ("new obj",    12,  str.rjust, "%d", [NEW_APP_OBJECTS],                 None, SUM_AGG),
  ("never %",    12,  str.rjust, "%d", [GRP_FUTURE, NEVER_NEW,  OBJECTS], None, SUM_AGG),
  ("1_grp %",    12,  str.rjust, "%d", [GRP_FUTURE, NEWG_1,     OBJECTS], None, SUM_AGG),
  ("2_grp %",    12,  str.rjust, "%d", [GRP_FUTURE, NEWG_2,     OBJECTS], None, SUM_AGG),
  ("3,4_grp %",  12,  str.rjust, "%d", [GRP_FUTURE, NEWG_3to4,  OBJECTS], None, SUM_AGG),
  ("5-8_grp %",  12,  str.rjust, "%d", [GRP_FUTURE, NEWG_5to8,  OBJECTS], None, SUM_AGG),
  ("9-16_grp %", 12,  str.rjust, "%d", [GRP_FUTURE, NEWG_9to16, OBJECTS], None, SUM_AGG),
  ("max %",      12,  str.rjust, "%d", [GRP_FUTURE, NEWG_MAX,   OBJECTS], None, SUM_AGG)
]

oiNewSizeGroupConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                           None, None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                     None, WGT_AVG),
  ("new obj",    12,  str.rjust, "%d", [NEW_APP_SIZE],                 None, SUM_AGG),
  ("never %",    12,  str.rjust, "%d", [GRP_FUTURE, NEVER_NEW,  SIZE], None, SUM_AGG),
  ("1_grp %",    12,  str.rjust, "%d", [GRP_FUTURE, NEWG_1,     SIZE], None, SUM_AGG),
  ("2_grp %",    12,  str.rjust, "%d", [GRP_FUTURE, NEWG_2,     SIZE], None, SUM_AGG),
  ("3,4_grp %",  12,  str.rjust, "%d", [GRP_FUTURE, NEWG_3to4,  SIZE], None, SUM_AGG),
  ("5-8_grp %",  12,  str.rjust, "%d", [GRP_FUTURE, NEWG_5to8,  SIZE], None, SUM_AGG),
  ("9-16_grp %", 12,  str.rjust, "%d", [GRP_FUTURE, NEWG_9to16, SIZE], None, SUM_AGG),
  ("max %",      12,  str.rjust, "%d", [GRP_FUTURE, NEWG_MAX,   SIZE], None, SUM_AGG)
]

oiNewObjectGroupRatioConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                                          None, None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                                    None, WGT_AVG),
  ("new obj",    12,  str.rjust, "%d", [NEW_APP_OBJECTS],                             None, SUM_AGG),
  ("never %",    12,  str.rjust, "%s", [GRP_FUTURE, NEVER_NEW,  NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("1_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, NEWG_1,     NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("2_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, NEWG_2,     NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("3,4_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, NEWG_3to4,  NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("5-8_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, NEWG_5to8,  NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("9-16_grp %", 12,  str.rjust, "%s", [GRP_FUTURE, NEWG_9to16, NEW_APP_OBJ_RATIO], ratstr, SUM_AGG),
  ("max %",      12,  str.rjust, "%s", [GRP_FUTURE, NEWG_MAX,   NEW_APP_OBJ_RATIO], ratstr, SUM_AGG)
]

oiNewSizeGroupRatioConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                                         None,   None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                                   None,   WGT_AVG),
  ("new obj",    12,  str.rjust, "%d", [NEW_APP_SIZE],                               B2KB,   SUM_AGG),
  ("never %",    12,  str.rjust, "%s", [GRP_FUTURE, NEVER_NEW,  NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("1_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, NEWG_1,     NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("2_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, NEWG_2,     NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("3,4_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, NEWG_3to4,  NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("5-8_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, NEWG_5to8,  NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("9-16_grp %", 12,  str.rjust, "%s", [GRP_FUTURE, NEWG_9to16, NEW_APP_SIZE_RATIO], ratstr, SUM_AGG),
  ("max %",      12,  str.rjust, "%s", [GRP_FUTURE, NEWG_MAX,   NEW_APP_SIZE_RATIO], ratstr, SUM_AGG)
]

oiHotObjectGroupConfig = [
  ("nvals",      9,  str.ljust, "%d", None,                             None, None),
  ("dur (ms)",  12,  str.ljust, "%d", [DURATION],                       None, WGT_AVG),
  ("total obj", 12,  str.rjust, "%d", [APP_OBJECTS],                    None, WGT_AVG),
  ("hot obj",   12,  str.rjust, "%d", [HLVM_OBJECTS],                   None, WGT_AVG),
  ("never",     12,  str.rjust, "%d", [GRP_FUTURE, NEVER,     OBJECTS], None, WGT_AVG),
  ("1_grp",     12,  str.rjust, "%d", [GRP_FUTURE, GRP_1,     OBJECTS], None, WGT_AVG),
  ("2_grp",     12,  str.rjust, "%d", [GRP_FUTURE, GRP_2,     OBJECTS], None, WGT_AVG),
  ("3,4_grp",   12,  str.rjust, "%d", [GRP_FUTURE, GRP_3to4,  OBJECTS], None, WGT_AVG),
  ("5-8_grp",   12,  str.rjust, "%d", [GRP_FUTURE, GRP_5to8,  OBJECTS], None, WGT_AVG),
  ("9-16_grp",  12,  str.rjust, "%d", [GRP_FUTURE, GRP_9to16, OBJECTS], None, WGT_AVG),
  ("max",       12,  str.rjust, "%d", [GRP_FUTURE, GRP_MAX,   OBJECTS], None, WGT_AVG)
]

oiHotSizeGroupConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                          None, None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                    None, WGT_AVG),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                    B2KB, WGT_AVG),
  ("hot size",   12,  str.rjust, "%d", [HLVM_SIZE],                   B2KB, WGT_AVG),
  ("never",      12,  str.rjust, "%d", [GRP_FUTURE, NEVER,     SIZE], B2KB, WGT_AVG),
  ("1_grp",      12,  str.rjust, "%d", [GRP_FUTURE, GRP_1,     SIZE], B2KB, WGT_AVG),
  ("2_grp",      12,  str.rjust, "%d", [GRP_FUTURE, GRP_2,     SIZE], B2KB, WGT_AVG),
  ("3,4_grp",    12,  str.rjust, "%d", [GRP_FUTURE, GRP_3to4,  SIZE], B2KB, WGT_AVG),
  ("5-8_grp",    12,  str.rjust, "%d", [GRP_FUTURE, GRP_5to8,  SIZE], B2KB, WGT_AVG),
  ("9-16_grp",   12,  str.rjust, "%d", [GRP_FUTURE, GRP_9to16, SIZE], B2KB, WGT_AVG),
  ("max",        12,  str.rjust, "%d", [GRP_FUTURE, GRP_MAX,   SIZE], B2KB, WGT_AVG)
]

oiHotObjectGroupRatioConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                                      None,   None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                                None,   WGT_AVG),
  ("total obj",  12,  str.rjust, "%d", [APP_OBJECTS],                             None,   WGT_AVG),
  ("hot obj %",  12,  str.rjust, "%s", [HLVM_OBJ_RATIO],                          ratstr, WGT_AVG),
  ("never %",    12,  str.rjust, "%s", [GRP_FUTURE, NEVER,     HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("1_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, GRP_1,     HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("2_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, GRP_2,     HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("3,4_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, GRP_3to4,  HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("5-8_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, GRP_5to8,  HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("9-16_grp %", 12,  str.rjust, "%s", [GRP_FUTURE, GRP_9to16, HOT_OBJECT_RATIO], ratstr, WGT_AVG),
  ("max %",      12,  str.rjust, "%s", [GRP_FUTURE, GRP_MAX,   HOT_OBJECT_RATIO], ratstr, WGT_AVG)
]

oiHotSizeGroupRatioConfig = [
  ("nvals",       9,  str.ljust, "%d", None,                                    None,   None),
  ("dur (ms)",   12,  str.ljust, "%d", [DURATION],                              None,   WGT_AVG),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                              B2KB,   WGT_AVG),
  ("hot size %", 12,  str.rjust, "%s", [HLVM_SIZE_RATIO],                       ratstr, WGT_AVG),
  ("never %",    12,  str.rjust, "%s", [GRP_FUTURE, NEVER,     HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("1_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, GRP_1,     HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("2_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, GRP_2,     HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("3,4_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, GRP_3to4,  HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("5-8_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, GRP_5to8,  HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("9-16_grp %", 12,  str.rjust, "%s", [GRP_FUTURE, GRP_9to16, HOT_SIZE_RATIO], ratstr, WGT_AVG),
  ("max %",      12,  str.rjust, "%s", [GRP_FUTURE, GRP_MAX,   HOT_SIZE_RATIO], ratstr, WGT_AVG)
]

oavrcfg = [
  ("dur (ms)",     9,  str.ljust, "%d", [DURATION],       None,   False),
  ("reason",      12,  str.ljust, "%s", [REASON],         None,   False),
  ("size (KB)",   12,  str.rjust, "%d", [USED_BYTES],     B2KB,   True ),
  ("hot (KB)",    12,  str.rjust, "%d", [HCO_SIZE],       B2KB,   True ),
  ("vm (KB)",     12,  str.rjust, "%d", [VM_SIZE],        B2KB,   True ),
  ("hot refs %",  12,  str.rjust, "%s", [HCO_REF_RATIO],  ratstr, True ),
  ("pages",       12,  str.rjust, "%d", [TOTAL_PAGES],    None,   True ),
  ("hot pages",   12,  str.rjust, "%d", [HCO_PAGES],      None,   True ),
  ("hot pages %", 12,  str.rjust, "%s", [HCO_PAGE_RATIO], ratstr, True ),
  ("sim pages",   12,  str.rjust, "%d", [SIM_PAGES],      None,   True ),
  ("sim pages %", 12,  str.rjust, "%s", [SIM_PAGE_RATIO], ratstr, True )
]

newObjectFutureConfig = [
  ("dur (ms)",   9,  str.ljust, "%d", [DURATION],                       None,   False),
  ("reason",    12,  str.ljust, "%s", [REASON],                         None,   False),
  ("total obj", 12,  str.rjust, "%d", [APP_OBJECTS],                    None,   True ),
  ("new obj",   12,  str.rjust, "%d", [NEW_APP_OBJECTS],                None,   True ),
  ("new obj %", 12,  str.rjust, "%s", [NEW_APP_OBJ_RATIO],              ratstr, True ),
  ("never",     12,  str.rjust, "%d", [VAL_FUTURE, NEVER_NEW, OBJECTS], None,   True ),
  ("1-val",     12,  str.rjust, "%d", [VAL_FUTURE, NEWV_1,    OBJECTS], None,   True ),
  ("2-val",     12,  str.rjust, "%d", [VAL_FUTURE, NEWV_2,    OBJECTS], None,   True ),
  ("4-val",     12,  str.rjust, "%d", [VAL_FUTURE, NEWV_4,    OBJECTS], None,   True ),
  ("8-val",     12,  str.rjust, "%d", [VAL_FUTURE, NEWV_8,    OBJECTS], None,   True ),
  ("max",       12,  str.rjust, "%d", [VAL_FUTURE, NEWV_MAX,  OBJECTS], None,   True ),
]

newSizeFutureConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                    None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                      None,   False),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                    B2KB,   True ),
  ("new size",   12,  str.rjust, "%d", [NEW_APP_SIZE],                B2KB,   True ),
  ("new size %", 12,  str.rjust, "%s", [NEW_APP_SIZE_RATIO],          ratstr, True ),
  ("never",      12,  str.rjust, "%d", [VAL_FUTURE, NEVER_NEW, SIZE], B2KB,   True ),
  ("1-val",      12,  str.rjust, "%d", [VAL_FUTURE, NEWV_1,    SIZE], B2KB,   True ),
  ("2-val",      12,  str.rjust, "%d", [VAL_FUTURE, NEWV_2,    SIZE], B2KB,   True ),
  ("4-val",      12,  str.rjust, "%d", [VAL_FUTURE, NEWV_4,    SIZE], B2KB,   True ),
  ("8-val",      12,  str.rjust, "%d", [VAL_FUTURE, NEWV_8,    SIZE], B2KB,   True ),
  ("max-val",    12,  str.rjust, "%d", [VAL_FUTURE, NEWV_MAX,  SIZE], B2KB,   True ),
]

newObjectFutureRatioConfig = [
  ("dur (ms)",   9,  str.ljust, "%d", [DURATION],                                 None,   False),
  ("reason",    12,  str.ljust, "%s", [REASON],                                   None,   False),
  ("total obj", 12,  str.rjust, "%d", [APP_OBJECTS],                              None,   True ),
  ("new obj",   12,  str.rjust, "%d", [NEW_APP_OBJECTS],                          None,   True ),
  ("new obj %", 12,  str.rjust, "%s", [NEW_APP_OBJ_RATIO],                        ratstr, True ),
  ("never %",   12,  str.rjust, "%s", [VAL_FUTURE, NEVER_NEW, NEW_APP_OBJ_RATIO], ratstr, True ),
  ("1-val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_1,    NEW_APP_OBJ_RATIO], ratstr, True ),
  ("2-val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_2,    NEW_APP_OBJ_RATIO], ratstr, True ),
  ("4-val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_4,    NEW_APP_OBJ_RATIO], ratstr, True ),
  ("8-val %",   12,  str.rjust, "%s", [VAL_FUTURE, NEWV_8,    NEW_APP_OBJ_RATIO], ratstr, True ),
  ("max-val %", 12,  str.rjust, "%s", [VAL_FUTURE, NEWV_MAX,  NEW_APP_OBJ_RATIO], ratstr, True ),
]

newSizeFutureRatioConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                                  None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                                    None,   False),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                                  B2KB,   True ),
  ("new size",   12,  str.rjust, "%d", [NEW_APP_SIZE],                              B2KB,   True ),
  ("new size %", 12,  str.rjust, "%s", [NEW_APP_SIZE_RATIO],                        ratstr, True ),
  ("never %",    12,  str.rjust, "%s", [VAL_FUTURE, NEVER_NEW, NEW_APP_SIZE_RATIO], ratstr, True ),
  ("1-val %",    12,  str.rjust, "%s", [VAL_FUTURE, NEWV_1,    NEW_APP_SIZE_RATIO], ratstr, True ),
  ("2-val %",    12,  str.rjust, "%s", [VAL_FUTURE, NEWV_2,    NEW_APP_SIZE_RATIO], ratstr, True ),
  ("4-val %",    12,  str.rjust, "%s", [VAL_FUTURE, NEWV_4,    NEW_APP_SIZE_RATIO], ratstr, True ),
  ("8-val %",    12,  str.rjust, "%s", [VAL_FUTURE, NEWV_8,    NEW_APP_SIZE_RATIO], ratstr, True ),
  ("max-val %",  12,  str.rjust, "%s", [VAL_FUTURE, NEWV_MAX,  NEW_APP_SIZE_RATIO], ratstr, True ),
]

hotObjectFutureConfig = [
  ("dur (ms)",   9,  str.ljust, "%d", [DURATION],                     None,   False),
  ("reason",    12,  str.ljust, "%s", [REASON],                       None,   False),
  ("total obj", 12,  str.rjust, "%d", [APP_OBJECTS],                  None,   True ),
  ("hot obj",   12,  str.rjust, "%d", [HLVM_OBJECTS],                 None,   True ),
  ("hot obj %", 12,  str.rjust, "%s", [HLVM_OBJ_RATIO],               ratstr, True ),
  ("never",     12,  str.rjust, "%s", [VAL_FUTURE, NEVER,   OBJECTS], ratstr, True ),
  ("1-val",     12,  str.rjust, "%s", [VAL_FUTURE, VAL_1,   OBJECTS], ratstr, True ),
  ("2-val",     12,  str.rjust, "%s", [VAL_FUTURE, VAL_2,   OBJECTS], ratstr, True ),
  ("4-val",     12,  str.rjust, "%s", [VAL_FUTURE, VAL_4,   OBJECTS], ratstr, True ),
  ("8-val",     12,  str.rjust, "%s", [VAL_FUTURE, VAL_8,   OBJECTS], ratstr, True ),
  ("max",       12,  str.rjust, "%s", [VAL_FUTURE, VAL_MAX, OBJECTS], ratstr, True ),
]

hotSizeFutureConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                  None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                    None,   False),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                  B2KB,   True ),
  ("hot size",   12,  str.rjust, "%d", [HLVM_SIZE],                 B2KB,   True ),
  ("hot size %", 12,  str.rjust, "%s", [HLVM_SIZE_RATIO],           ratstr, True ),
  ("never",      12,  str.rjust, "%s", [VAL_FUTURE, NEVER,   SIZE], ratstr, True ),
  ("1-val",      12,  str.rjust, "%s", [VAL_FUTURE, VAL_1,   SIZE], ratstr, True ),
  ("2-val",      12,  str.rjust, "%s", [VAL_FUTURE, VAL_2,   SIZE], ratstr, True ),
  ("4-val",      12,  str.rjust, "%s", [VAL_FUTURE, VAL_4,   SIZE], ratstr, True ),
  ("8-val",      12,  str.rjust, "%s", [VAL_FUTURE, VAL_8,   SIZE], ratstr, True ),
  ("max-val",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_MAX, SIZE], ratstr, True ),
]

hotObjectFutureRatioConfig = [
  ("dur (ms)",   9,  str.ljust, "%d", [DURATION],                              None,   False),
  ("reason",    12,  str.ljust, "%s", [REASON],                                None,   False),
  ("total obj", 12,  str.rjust, "%d", [APP_OBJECTS],                           None,   True ),
  ("hot obj",   12,  str.rjust, "%d", [HLVM_OBJECTS],                          None,   True ),
  ("hot obj %", 12,  str.rjust, "%s", [HLVM_OBJ_RATIO],                        ratstr, True ),
  ("never %",   12,  str.rjust, "%s", [VAL_FUTURE, NEVER,   HOT_OBJECT_RATIO], ratstr, True ),
  ("1-val %",   12,  str.rjust, "%s", [VAL_FUTURE, VAL_1,   HOT_OBJECT_RATIO], ratstr, True ),
  ("2-val %",   12,  str.rjust, "%s", [VAL_FUTURE, VAL_2,   HOT_OBJECT_RATIO], ratstr, True ),
  ("4-val %",   12,  str.rjust, "%s", [VAL_FUTURE, VAL_4,   HOT_OBJECT_RATIO], ratstr, True ),
  ("8-val %",   12,  str.rjust, "%s", [VAL_FUTURE, VAL_8,   HOT_OBJECT_RATIO], ratstr, True ),
  ("max %",     12,  str.rjust, "%s", [VAL_FUTURE, VAL_MAX, HOT_OBJECT_RATIO], ratstr, True ),
]

hotSizeFutureRatioConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                            None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                              None,   False),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                            B2KB,   True ),
  ("hot size",   12,  str.rjust, "%d", [HLVM_SIZE],                           B2KB,   True ),
  ("hot size %", 12,  str.rjust, "%s", [HLVM_SIZE_RATIO],                     ratstr, True ),
  ("never %",    12,  str.rjust, "%s", [VAL_FUTURE, NEVER,   HOT_SIZE_RATIO], ratstr, True ),
  ("1-val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_1,   HOT_SIZE_RATIO], ratstr, True ),
  ("2-val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_2,   HOT_SIZE_RATIO], ratstr, True ),
  ("4-val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_4,   HOT_SIZE_RATIO], ratstr, True ),
  ("8-val %",    12,  str.rjust, "%s", [VAL_FUTURE, VAL_8,   HOT_SIZE_RATIO], ratstr, True ),
  ("max-val %",  12,  str.rjust, "%s", [VAL_FUTURE, VAL_MAX, HOT_SIZE_RATIO], ratstr, True ),
]

hotObjectFutureGroupConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                        None, False),
  ("reason",     12,  str.ljust, "%s", [REASON],                          None, False),
  ("total obj",  12,  str.rjust, "%d", [APP_OBJECTS],                     None, True ),
  ("hot obj",    12,  str.rjust, "%d", [HLVM_OBJECTS],                    None, True ),
  ("never",      12,  str.rjust, "%d", [GRP_FUTURE, NEVER,     OBJECTS],  None, True ),
  ("1_grp",      12,  str.rjust, "%d", [GRP_FUTURE, GRP_1,     OBJECTS],  None, True ),
  ("2_grp",      12,  str.rjust, "%d", [GRP_FUTURE, GRP_2,     OBJECTS],  None, True ),
  ("3,4_grp",    12,  str.rjust, "%d", [GRP_FUTURE, GRP_3to4,  OBJECTS],  None, True ),
  ("5-8_grp",    12,  str.rjust, "%d", [GRP_FUTURE, GRP_5to8,  OBJECTS],  None, True ),
  ("9-16_grp",   12,  str.rjust, "%d", [GRP_FUTURE, GRP_9to16, OBJECTS],  None, True ),
  ("max",        12,  str.rjust, "%d", [GRP_FUTURE, GRP_MAX,   OBJECTS],  None, True ),
]

hotSizeFutureGroupConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                     None, False),
  ("reason",     12,  str.ljust, "%s", [REASON],                       None, False),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                     B2KB, True ),
  ("hot size",   12,  str.rjust, "%d", [HLVM_SIZE],                    B2KB, True ),
  ("never",      12,  str.rjust, "%d", [GRP_FUTURE, NEVER,     SIZE],  B2KB, True ),
  ("1_grp",      12,  str.rjust, "%d", [GRP_FUTURE, GRP_1,     SIZE],  B2KB, True ),
  ("2_grp",      12,  str.rjust, "%d", [GRP_FUTURE, GRP_2,     SIZE],  B2KB, True ),
  ("3,4_grp",    12,  str.rjust, "%d", [GRP_FUTURE, GRP_3to4,  SIZE],  B2KB, True ),
  ("5-8_grp",    12,  str.rjust, "%d", [GRP_FUTURE, GRP_5to8,  SIZE],  B2KB, True ),
  ("9-16_grp",   12,  str.rjust, "%d", [GRP_FUTURE, GRP_9to16, SIZE],  B2KB, True ),
  ("max",        12,  str.rjust, "%d", [GRP_FUTURE, GRP_MAX,   SIZE],  B2KB, True ),
]

hotObjectFGRatioConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                                 None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                                   None,   False),
  ("total obj",  12,  str.rjust, "%d", [APP_OBJECTS],                              None,   True ),
  ("hot obj %",  12,  str.rjust, "%s", [HLVM_OBJ_RATIO],                           ratstr, True ),
  ("never %",    12,  str.rjust, "%s", [GRP_FUTURE, NEVER,     HOT_OBJECT_RATIO],  ratstr, True ),
  ("1_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, GRP_1,     HOT_OBJECT_RATIO],  ratstr, True ),
  ("2_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, GRP_2,     HOT_OBJECT_RATIO],  ratstr, True ),
  ("3,4_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, GRP_3to4,  HOT_OBJECT_RATIO],  ratstr, True ),
  ("5-8_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, GRP_5to8,  HOT_OBJECT_RATIO],  ratstr, True ),
  ("9-16_grp %", 12,  str.rjust, "%s", [GRP_FUTURE, GRP_9to16, HOT_OBJECT_RATIO],  ratstr, True ),
  ("max %",      12,  str.rjust, "%s", [GRP_FUTURE, GRP_MAX,   HOT_OBJECT_RATIO],  ratstr, True ),
]

hotSizeFGRatioConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                               None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                                 None,   False),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                               B2KB,   True ),
  ("hot size %", 12,  str.rjust, "%s", [HLVM_SIZE_RATIO],                        ratstr, True ),
  ("never %",    12,  str.rjust, "%s", [GRP_FUTURE, NEVER,     HOT_SIZE_RATIO],  ratstr, True ),
  ("1_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, GRP_1,     HOT_SIZE_RATIO],  ratstr, True ),
  ("2_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, GRP_2,     HOT_SIZE_RATIO],  ratstr, True ),
  ("3,4_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, GRP_3to4,  HOT_SIZE_RATIO],  ratstr, True ),
  ("5-8_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, GRP_5to8,  HOT_SIZE_RATIO],  ratstr, True ),
  ("9-16_grp %", 12,  str.rjust, "%s", [GRP_FUTURE, GRP_9to16, HOT_SIZE_RATIO],  ratstr, True ),
  ("max %",      12,  str.rjust, "%s", [GRP_FUTURE, GRP_MAX,   HOT_SIZE_RATIO],  ratstr, True ),
]

newObjectFutureGroupConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                        None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                          None,   False),
  ("total obj",  12,  str.rjust, "%d", [APP_OBJECTS],                     None,   True ),
  ("new obj",    12,  str.rjust, "%d", [NEW_APP_OBJECTS],                 None,   True ),
  ("new obj %",  12,  str.rjust, "%s", [NEW_APP_OBJ_RATIO],               ratstr, True ),
  ("never %",    12,  str.rjust, "%d", [GRP_FUTURE, NEVER_NEW,  OBJECTS], None,   True ),
  ("1_grp %",    12,  str.rjust, "%d", [GRP_FUTURE, NEWG_1,     OBJECTS], None,   True ),
  ("2_grp %",    12,  str.rjust, "%d", [GRP_FUTURE, NEWG_2,     OBJECTS], None,   True ),
  ("3,4_grp %",  12,  str.rjust, "%d", [GRP_FUTURE, NEWG_3to4,  OBJECTS], None,   True ),
  ("5-8_grp %",  12,  str.rjust, "%d", [GRP_FUTURE, NEWG_5to8,  OBJECTS], None,   True ),
  ("9-16_grp %", 12,  str.rjust, "%d", [GRP_FUTURE, NEWG_9to16, OBJECTS], None,   True ),
  ("max %",      12,  str.rjust, "%d", [GRP_FUTURE, NEWG_MAX,   OBJECTS], None,   True ),
]

newSizeFutureGroupConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                     None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                       None,   False),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                     B2KB,   True ),
  ("new size",   12,  str.rjust, "%d", [NEW_APP_SIZE],                 B2KB,   True ),
  ("new size %", 12,  str.rjust, "%s", [NEW_APP_SIZE_RATIO],           ratstr, True ),
  ("never %",    12,  str.rjust, "%d", [GRP_FUTURE, NEVER_NEW,  SIZE], B2KB,   True ),
  ("1_grp %",    12,  str.rjust, "%d", [GRP_FUTURE, NEWG_1,     SIZE], B2KB,   True ),
  ("2_grp %",    12,  str.rjust, "%d", [GRP_FUTURE, NEWG_2,     SIZE], B2KB,   True ),
  ("3,4_grp %",  12,  str.rjust, "%d", [GRP_FUTURE, NEWG_3to4,  SIZE], B2KB,   True ),
  ("5-8_grp %",  12,  str.rjust, "%d", [GRP_FUTURE, NEWG_5to8,  SIZE], B2KB,   True ),
  ("9-16_grp %", 12,  str.rjust, "%d", [GRP_FUTURE, NEWG_9to16, SIZE], B2KB,   True ),
  ("max %",      12,  str.rjust, "%d", [GRP_FUTURE, NEWG_MAX,   SIZE], B2KB,   True ),
]

newObjectFGRatioConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                                  None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                                    None,   False),
  ("total obj",  12,  str.rjust, "%d", [APP_OBJECTS],                               None,   True ),
  ("new obj",    12,  str.rjust, "%d", [NEW_APP_OBJECTS],                           None,   True ),
  ("new obj %",  12,  str.rjust, "%s", [NEW_APP_OBJ_RATIO],                         ratstr, True ),
  ("never %",    12,  str.rjust, "%s", [GRP_FUTURE, NEVER_NEW,  NEW_APP_OBJ_RATIO], ratstr, True ),
  ("1_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, NEWG_1,     NEW_APP_OBJ_RATIO], ratstr, True ),
  ("2_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, NEWG_2,     NEW_APP_OBJ_RATIO], ratstr, True ),
  ("3,4_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, NEWG_3to4,  NEW_APP_OBJ_RATIO], ratstr, True ),
  ("5-8_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, NEWG_5to8,  NEW_APP_OBJ_RATIO], ratstr, True ),
  ("9-16_grp %", 12,  str.rjust, "%s", [GRP_FUTURE, NEWG_9to16, NEW_APP_OBJ_RATIO], ratstr, True ),
  ("max %",      12,  str.rjust, "%s", [GRP_FUTURE, NEWG_MAX,   NEW_APP_OBJ_RATIO], ratstr, True ),
]

newSizeFGRatioConfig = [
  ("dur (ms)",    9,  str.ljust, "%d", [DURATION],                                   None,   False),
  ("reason",     12,  str.ljust, "%s", [REASON],                                     None,   False),
  ("total size", 12,  str.rjust, "%d", [APP_SIZE],                                   B2KB,   True ),
  ("new size",   12,  str.rjust, "%d", [NEW_APP_SIZE],                               B2KB,   True ),
  ("new size %", 12,  str.rjust, "%s", [NEW_APP_SIZE_RATIO],                         ratstr, True ),
  ("never %",    12,  str.rjust, "%s", [GRP_FUTURE, NEVER_NEW,  NEW_APP_SIZE_RATIO], ratstr, True ),
  ("1_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, NEWG_1,     NEW_APP_SIZE_RATIO], ratstr, True ),
  ("2_grp %",    12,  str.rjust, "%s", [GRP_FUTURE, NEWG_2,     NEW_APP_SIZE_RATIO], ratstr, True ),
  ("3,4_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, NEWG_3to4,  NEW_APP_SIZE_RATIO], ratstr, True ),
  ("5-8_grp %",  12,  str.rjust, "%s", [GRP_FUTURE, NEWG_5to8,  NEW_APP_SIZE_RATIO], ratstr, True ),
  ("9-16_grp %", 12,  str.rjust, "%s", [GRP_FUTURE, NEWG_9to16, NEW_APP_SIZE_RATIO], ratstr, True ),
  ("max %",      12,  str.rjust, "%s", [GRP_FUTURE, NEWG_MAX,   NEW_APP_SIZE_RATIO], ratstr, True ),
]

misses_fns = [
  get_objects_misses_idx,
  get_size_misses_idx,
  get_refs_misses_idx
]

over_under_fns = [
  get_over_under_ratio,
  get_ap_over_under_ratio,
  get_agg_over_under_ratio,
]

ksiapcfg = [
  ("nvals",       9,  str.ljust, "%d",    ["__arg_kstype",         NVALS],                     None,   True ),
  ("total APs",  12,  str.rjust, "%d",    ["__arg_kstype",         PER_VAL, APS, ALL_STATS],   None,   True ),
  ("ideal APs",  12,  str.rjust, "%d",    [AGG_IDEAL_KS_PPV, PER_VAL, APS, KS_STATS],    None,   True ),
  ("ks APs",     12,  str.rjust, "%d",    ["__arg_kstype",         PER_VAL, APS, KS_STATS],    None,   True ),
  ("over %",     12,  str.rjust, "%5.4f", ["__arg_kstype",         PER_VAL, APS, OVER_RATIO],  None,   True ),
  ("under %",    12,  str.rjust, "%5.4f", ["__arg_kstype",         PER_VAL, APS, UNDER_RATIO], None,   True ),
  ("ov+un %",    12,  str.rjust, "%5.4f", ["__arg_kstype",         PER_VAL, APS], get_agg_over_under_ratio,True ),
]

ksiobjcfg = [
  ("nvals",       9,  str.ljust, "%d",    ["__arg_kstype",        NVALS],                                  None,   True),
  ("total objs", 12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", OBJECTS, ALL_STATS],       None,   True ),
  ("ideal objs", 12,  str.rjust, "%d",    [AGG_IDEAL_KS_PPV, "__arg_aggtype", OBJECTS, KS_STATS],    None,   True ),
  ("ks objs",    12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", OBJECTS, KS_STATS],    None,   True ),
  ("over objs",  12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", OBJECTS, OVER_STATS],  None,   True ),
  ("over %",     12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", OBJECTS, OVER_RATIO],  None,   True ),
  ("under objs", 12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", OBJECTS, UNDER_STATS], None,   True ),
  ("under %",    12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", OBJECTS, UNDER_RATIO], None,   True ),
  ("ov+un %",    12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", OBJECTS], get_agg_over_under_ratio, True ),
]

ksisizecfg = [
  ("nvals",       9,  str.ljust, "%d",    ["__arg_kstype",        NVALS],                               None,   True),
  ("total size", 12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", SIZE, ALL_STATS],   B2KB,   True ),
  ("ideal size", 12,  str.rjust, "%d",    [AGG_IDEAL_KS_PPV, "__arg_aggtype", SIZE, KS_STATS],    B2KB,   True ),
  ("ks size",    12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", SIZE, KS_STATS],    B2KB,   True ),
  ("over size",  12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", SIZE, OVER_STATS],  B2KB,   True ),
  ("over %",     12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", SIZE, OVER_RATIO],  None,   True ),
  ("under size", 12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", SIZE, UNDER_STATS], B2KB,   True ),
  ("under %",    12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", SIZE, UNDER_RATIO], None,   True ),
  ("ov+un %",    12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", SIZE], get_agg_over_under_ratio, True ),
]

ksirefscfg = [
  ("nvals",       9,  str.ljust, "%d",    ["__arg_kstype",        NVALS],                               None,   True),
  ("total refs", 12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", REFS, ALL_STATS],   None,   True ),
  ("ideal refs", 12,  str.rjust, "%d",    [AGG_IDEAL_KS_PPV, "__arg_aggtype", REFS, KS_STATS],    None,   True ),
  ("ks refs",    12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", REFS, KS_STATS],    None,   True ),
  ("over refs",  12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", REFS, OVER_STATS],  None,   True ),
  ("over %",     12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", REFS, OVER_RATIO],  None,   True ),
  ("under refs", 12,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", REFS, UNDER_STATS], None,   True ),
  ("under %",    12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", REFS, UNDER_RATIO], None,   True ),
  ("ov+un %",    12,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", REFS], get_agg_over_under_ratio, True ),
]

ksicfg = [
  ("nvals",       5,  str.ljust, "%d",    ["__arg_kstype",         NVALS],                              None,   True ),
  ("total size", 14,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", SIZE, ALL_STATS],   B2KB,   True ),
  ("id-size",     9,  str.rjust, "%5.4f", [AGG_IDEAL_KS_PPV, "__arg_aggtype", SIZE, KS_RATIO],    None,   True ),
  ("ks-size",     9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", SIZE, KS_RATIO],    None,   True ),
  ("over %",      9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", SIZE, OVER_RATIO],  None,   True ),
  ("under %",     9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", SIZE, UNDER_RATIO], None,   True ),
  ("ov+un %",     9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", SIZE],get_agg_over_under_ratio, True ),
  ("total refs", 14,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", REFS, ALL_STATS],   None,   True ),
  ("id-refs",     9,  str.rjust, "%5.4f", [AGG_IDEAL_KS_PPV, "__arg_aggtype", REFS, KS_RATIO],    None,   True ),
  ("ks-refs",     9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", REFS, KS_RATIO],    None,   True ),
  ("over %",      9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", REFS, OVER_RATIO],  None,   True ),
  ("under %",     9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", REFS, UNDER_RATIO], None,   True ),
  ("ov+un %",     9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", REFS],get_agg_over_under_ratio, True ),
]

ksircfg = [
  ("nvals",       5,  str.ljust, "%d",    ["__arg_kstype",  NVALS],                                     None,   True ),
  ("total size", 14,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", SIZE, ALL_STATS],   B2KB,   True ),
  ("id-size",     9,  str.rjust, "%5.4f", [AGG_IDEAL_KS_PPV, "__arg_aggtype", SIZE, KS_RATIO],    None,   True ),
  ("ks-size",     9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", SIZE, KS_RATIO],    None,   True ),
  ("lo miss",    12,  str.rjust, "%5.4f", [[AGG_VAL, average],    ["__arg_vkstype", IDEAL_KS_PPV, LOW, True]],   get_size_misses_idx, True ),
  ("hi miss",    12,  str.rjust, "%5.4f", [[AGG_VAL, average],    ["__arg_vkstype", IDEAL_KS_PPV, HIGH, True]],  get_size_misses_idx, True ),
  ("total refs", 14,  str.rjust, "%d",    ["__arg_kstype",         "__arg_aggtype", REFS, ALL_STATS],   None,   True ),
  ("id-refs",     9,  str.rjust, "%5.4f", [AGG_IDEAL_KS_PPV, "__arg_aggtype", REFS, KS_RATIO],    None,   True ),
  ("ks-refs",     9,  str.rjust, "%5.4f", ["__arg_kstype",         "__arg_aggtype", REFS, KS_RATIO],    None,   True ),
  ("lo miss",    12,  str.rjust, "%5.4f", [[AGG_VAL, average],    ["__arg_vkstype", IDEAL_KS_PPV, LOW,  True]],  get_refs_misses_idx, True ),
  ("hi miss",    12,  str.rjust, "%5.4f", [[AGG_VAL, average],    ["__arg_vkstype", IDEAL_KS_PPV, HIGH, True]],  get_refs_misses_idx, True ),
]

kvrcfg = [
  ("dur (ms)",    9,  str.ljust, "%d",    [DURATION],                                            None,                    False),
  ("objects",    12,  str.rjust, "%d",    ["__arg_kstype",  ALL_STATS, VAL_ALIVE_OBJECTS_IDX],   None,                    True ),
  ("obj %",      12,  str.rjust, "%5.4f", ["__arg_kstype",  KS_RATIO,  VAL_ALIVE_OBJECTS_IDX],   None,                    True ),
  ("lo miss",    12,  str.rjust, "%5.4f", [["__arg_kstype", IDEAL_KS_PPV, LOW, True]],     get_objects_misses_idx,  True ),
  ("hi miss",    12,  str.rjust, "%5.4f", [["__arg_kstype", IDEAL_KS_PPV, HIGH, True]],    get_objects_misses_idx,  True ),
  ("size (KB)",  12,  str.rjust, "%d",    ["__arg_kstype",  ALL_STATS, VAL_ALIVE_SIZE_IDX],      None,                    True ),
  ("size %",     12,  str.rjust, "%5.4f", ["__arg_kstype",  KS_RATIO,  VAL_ALIVE_SIZE_IDX],      None,                    True ),
  ("lo miss",    12,  str.rjust, "%5.4f", [["__arg_kstype", IDEAL_KS_PPV, LOW, True]],     get_size_misses_idx,     True ),
  ("hi miss",    12,  str.rjust, "%5.4f", [["__arg_kstype", IDEAL_KS_PPV, HIGH, True]],    get_size_misses_idx,     True ),
  ("refs",       12,  str.rjust, "%d",    ["__arg_kstype",  ALL_STATS, VAL_REFS_IDX],            None,                    True ),
  ("refs %",     12,  str.rjust, "%5.4f", ["__arg_kstype",  KS_RATIO,  VAL_REFS_IDX],            None,                    True ),
  ("lo miss",    12,  str.rjust, "%5.4f", [["__arg_kstype", IDEAL_KS_PPV, LOW, True]],     get_refs_misses_idx,     True ),
  ("hi miss",    12,  str.rjust, "%5.4f", [["__arg_kstype", IDEAL_KS_PPV, HIGH, True]],    get_refs_misses_idx,     True ),
]

ksstratcfg = [
  ("cold size %",  12,  str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", SIZE,  KS_RATIO],    None, True ),
  ("over size %",  12,  str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", SIZE,  OVER_RATIO],  None, True ),
  ("under size %", 12,  str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", SIZE,  UNDER_RATIO], None, True ),
  ("ov+un size %", 12,  str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", SIZE], get_agg_over_under_ratio, True ),
  ("cold refs %",  12,  str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", REFS,  KS_RATIO],    None, True ),
  ("over refs %",  12,  str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", REFS,  OVER_RATIO],  None, True ),
  ("under refs %", 12,  str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", REFS,  UNDER_RATIO], None, True ),
  ("ov+un refs %", 12,  str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", REFS], get_agg_over_under_ratio, True ),
]

ksstmisscfg = [
  ("cold size %", 12, str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", SIZE, KS_RATIO],    None, True ),
  ("size lo %",   12, str.rjust, "%5.4f", [[AGG_VAL, average], ["__arg_vkstype", IDEAL_KS_PPV, LOW,  True, False, False]],  get_size_misses_idx, True ),
  ("size hi %",   12, str.rjust, "%5.4f", [[AGG_VAL, average], ["__arg_vkstype", IDEAL_KS_PPV, HIGH, True, False, False]],  get_size_misses_idx, True ),
  ("cold refs %", 12, str.rjust, "%5.4f", ["__arg_kstype", "__arg_aggtype", REFS, KS_RATIO],    None, True ),
  ("refs lo %",   12, str.rjust, "%5.4f", [[AGG_VAL, average], ["__arg_vkstype", IDEAL_KS_PPV, LOW,  True, False, False]],  get_refs_misses_idx, True ),
  ("refs hi %",   12, str.rjust, "%5.4f", [[AGG_VAL, average], ["__arg_vkstype", IDEAL_KS_PPV, HIGH, True, False, False]],  get_refs_misses_idx, True ),
]

kvapcfg = [
  ("dur (ms)",    9,  str.ljust, "%d",    [DURATION],                       None,   False),
  ("total APs",  12,  str.rjust, "%d",    ["__arg_kstype",  AP_STATS, 0],    None,   True ),
  ("ideal APs",  12,  str.rjust, "%d",    ["__arg_kstype",  AP_STATS, 1, 0], None,   True ),
  ("ks APs",     12,  str.rjust, "%d",    ["__arg_kstype",  AP_STATS, 2, 0], None,   True ),
  ("over APs",   12,  str.rjust, "%d",    ["__arg_kstype",  AP_STATS, 3, 0], None,   True ),
  ("over %",     12,  str.rjust, "%5.4f", ["__arg_kstype",  AP_STATS, 3, 1], None,   True ),
  ("under APs",  12,  str.rjust, "%d",    ["__arg_kstype",  AP_STATS, 4, 0], None,   True ),
  ("under %",    12,  str.rjust, "%5.4f", ["__arg_kstype",  AP_STATS, 4, 1], None,   True ),
  ("ov+un %",    12,  str.rjust, "%5.4f", ["__arg_kstype"], get_ap_over_under_ratio, True )
]

kvobjcfg = [
  ("dur (ms)",    9,  str.ljust, "%d",    [DURATION],                            None, False),
  ("total objs", 12,  str.rjust, "%d",    ["__arg_kstype",       ALL_STATS,   0], None, True ),
  ("ideal objs", 12,  str.rjust, "%d",    [IDEAL_KS_PPV,  KS_STATS,    0], None, True ),
  ("ks objs",    12,  str.rjust, "%d",    ["__arg_kstype",       KS_STATS,    0], None, True ),
  ("over objs",  12,  str.rjust, "%d",    ["__arg_kstype",       OVER_STATS,  0], None, True ),
  ("over %",     12,  str.rjust, "%5.4f", ["__arg_kstype",       OVER_RATIO,  0], None, True ),
  ("under objs", 12,  str.rjust, "%d",    ["__arg_kstype",       UNDER_STATS, 0], None, True ),
  ("under %",    12,  str.rjust, "%5.4f", ["__arg_kstype",       UNDER_RATIO, 0], None, True ),
  ("ov+un %",    12,  str.rjust, "%5.4f", [["__arg_kstype", 0]], get_over_under_ratio,  True )
]

kvsizecfg = [
  ("dur (ms)",    9,  str.ljust, "%d",    [DURATION],                            None, False),
  ("total size", 12,  str.rjust, "%d",    ["__arg_kstype",       ALL_STATS,   1], B2KB, True ),
  ("ideal size", 12,  str.rjust, "%d",    [IDEAL_KS_PPV,  KS_STATS,    1], B2KB, True ),
  ("ks size",    12,  str.rjust, "%d",    ["__arg_kstype",       KS_STATS,    1], B2KB, True ),
  ("over size",  12,  str.rjust, "%d",    ["__arg_kstype",       OVER_STATS,  1], B2KB, True ),
  ("over %",     12,  str.rjust, "%5.4f", ["__arg_kstype",       OVER_RATIO,  1], None, True ),
  ("under size", 12,  str.rjust, "%d",    ["__arg_kstype",       UNDER_STATS, 1], B2KB, True ),
  ("under %",    12,  str.rjust, "%5.4f", ["__arg_kstype",       UNDER_RATIO, 1], None, True ),
  ("ov+un %",    12,  str.rjust, "%5.4f", [["__arg_kstype", 1]], get_over_under_ratio,  True )
]

kvrefscfg = [
  ("dur (ms)",    9,  str.ljust, "%d",    [DURATION],                            None, False),
  ("total refs", 12,  str.rjust, "%d",    ["__arg_kstype",       ALL_STATS,   6], None, True ),
  ("ideal refs", 12,  str.rjust, "%d",    [IDEAL_KS_PPV,   KS_STATS,    6], None, True ),
  ("ks refs",    12,  str.rjust, "%d",    ["__arg_kstype",       KS_STATS,    6], None, True ),
  ("over refs",  12,  str.rjust, "%d",    ["__arg_kstype",       OVER_STATS,  6], None, True ),
  ("over %",     12,  str.rjust, "%5.4f", ["__arg_kstype",       OVER_RATIO,  6], None, True ),
  ("under refs", 12,  str.rjust, "%d",    ["__arg_kstype",       UNDER_STATS, 6], None, True ),
  ("under %",    12,  str.rjust, "%5.4f", ["__arg_kstype",       UNDER_RATIO, 6], None, True ),
  ("ov+un %",    12,  str.rjust, "%5.4f", [["__arg_kstype", 6]], get_over_under_ratio,  True )
]

kvcfg = [
  ("dur (ms)",    9,  str.ljust, "%d",    [DURATION],                             None, False),
  ("total size", 12,  str.rjust, "%d",    ["__arg_kstype",       ALL_STATS,   1], B2KB, True ),
  ("id-size",     9,  str.rjust, "%5.4f", [IDEAL_KS_PPV,   KS_RATIO,    1], None, True ),
#  ("id-size",     9,  str.rjust, "%5.4f", ['PRE_REFS_KS_TOTALS',   KS_RATIO,    1], None, True ),
  ("ks-size",     9,  str.rjust, "%5.4f", ["__arg_kstype",       KS_RATIO,    1], None, True ),
  ("over %",      9,  str.rjust, "%5.4f", ["__arg_kstype",       OVER_RATIO,  1], None, True ),
  ("under %",     9,  str.rjust, "%5.4f", ["__arg_kstype",       UNDER_RATIO, 1], None, True ),
  ("ov+un %",     9,  str.rjust, "%5.4f", [["__arg_kstype", 1]], get_over_under_ratio,  True ),
  ("total refs", 12,  str.rjust, "%d",    ["__arg_kstype",       ALL_STATS,   6], None, True ),
  ("id-refs",     9,  str.rjust, "%5.4f", [IDEAL_KS_PPV,   KS_RATIO,    6], None, True ),
#  ("id-refs",     9,  str.rjust, "%5.4f", ['PRE_REFS_KS_TOTALS',   KS_RATIO,    6], None, True ),
  ("ks-refs",     9,  str.rjust, "%5.4f", ["__arg_kstype",       KS_RATIO,    6], None, True ),
  ("over %",      9,  str.rjust, "%5.4f", ["__arg_kstype",       OVER_RATIO,  6], None, True ),
  ("under %",     9,  str.rjust, "%5.4f", ["__arg_kstype",       UNDER_RATIO, 6], None, True ),
  ("ov+un %",     9,  str.rjust, "%5.4f", [["__arg_kstype", 6]], get_over_under_ratio,  True )
]

kvzcfg = [
  ("dur (ms)",    9,  str.ljust, "%d",    [DURATION],        None, False),
  ("total size", 12,  str.rjust, "%d",    ["__arg_kstype",   ALL_STATS,    1], B2KB, True ),
  ("id-size",     9,  str.rjust, "%5.4f", [IDEAL_KS_PPV,     KS_RATIO,     1], None, True ),
  ("ks-size",     9,  str.rjust, "%5.4f", ["__arg_kstype",   KS_RATIO,     1], None, True ),
#  ("ks-adj",      9,  str.rjust, "%5.4f", ["__arg_kstype",   KS_ADJ_RATIO, 1], None, True ),
  ("total refs", 12,  str.rjust, "%d",    ["__arg_kstype",   ALL_STATS,        6], None, True ),
  ("id-refs",     9,  str.rjust, "%5.4f", [IDEAL_KS_PPV,     KS_RATIO,     6], None, True ),
  ("ks-refs",     9,  str.rjust, "%5.4f", ["__arg_kstype",   KS_RATIO,     6], None, True ),
#  ("ks-adj",      9,  str.rjust, "%5.4f", ["__arg_kstype",   KS_ADJ_RATIO, 6], None, True ),
  ("lo miss",     9,  str.rjust, "%5.4f", [["__arg_kstype",  IDEAL_KS_PPV, LOW,  True, False, False]], get_refs_misses_idx, True ),
  ("hi miss",     9,  str.rjust, "%5.4f", [["__arg_kstype",  IDEAL_KS_PPV, HIGH, True, False, False]], get_refs_misses_idx, True ),
#  ("id-size",     9,  str.rjust, "%5.4f", [IDEAL_KS_PPV,  OUT_RATIO,     1], None, True ),
#  ("ks-size",     9,  str.rjust, "%5.4f", ["__arg_kstype",   OUT_RATIO,     1], None, True ),
#  ("ks-adj",      9,  str.rjust, "%5.4f", ["__arg_kstype",   OUT_ADJ_RATIO, 1], None, True ),
#  ("id-refs",     9,  str.rjust, "%5.4f", [IDEAL_KS_PPV,  OUT_RATIO,     6], None, True ),
#  ("ks-refs",     9,  str.rjust, "%5.4f", ["__arg_kstype",   OUT_RATIO,     6], None, True ),
#  ("ks-adj",      9,  str.rjust, "%5.4f", ["__arg_kstype",   OUT_ADJ_RATIO, 6], None, True ),
#  ("lo miss",     9,  str.rjust, "%5.4f", [["__arg_kstype",  IDEAL_KS_PPV, LOW,  True, True, True]], get_refs_misses_idx, True ),
#  ("hi miss",     9,  str.rjust, "%5.4f", [["__arg_kstype",  IDEAL_KS_PPV, HIGH, True, True, True]], get_refs_misses_idx, True ),
]

kszcfg = [
  ("nvals",       5,  str.ljust, "%d",    ["__arg_kstype",         NVALS],                          None,   True ),
  ("total size", 14,  str.rjust, "%d",    ["__arg_kstype",       "__arg_aggtype", SIZE, ALL_STATS], B2KB,   True ),
  ("id-size",     9,  str.rjust, "%5.4f", [AGG_IDEAL_KS_PPV,     "__arg_aggtype", SIZE, KS_RATIO],  None,   True ),
  ("ks-size",     9,  str.rjust, "%5.4f", ["__arg_kstype",       "__arg_aggtype", SIZE, KS_RATIO],  None,   True ),
  ("total refs", 14,  str.rjust, "%d",    ["__arg_kstype",       "__arg_aggtype", REFS, ALL_STATS], None,   True ),
  ("id-refs",     9,  str.rjust, "%5.4f", [AGG_IDEAL_KS_PPV,     "__arg_aggtype", REFS, KS_RATIO],  None,   True ),
  ("ks-refs",     9,  str.rjust, "%5.4f", ["__arg_kstype",       "__arg_aggtype", REFS, KS_RATIO],  None,   True ),
  ("lo miss",     9,  str.rjust, "%5.4f", [[AGG_VAL, average],  ["__arg_vkstype", IDEAL_KS_PPV, LOW,  True, False, False]],  get_refs_misses_idx, True ),
  ("hi miss",     9,  str.rjust, "%5.4f", [[AGG_VAL, average],  ["__arg_vkstype", IDEAL_KS_PPV, HIGH, True, False, False]],  get_refs_misses_idx, True ),
]

class TeeFile(object):
  def __init__(self,*files):
    self.files = files
  def write(self,txt):
    for fp in self.files:
      fp.write(txt)
  def flush(self):
    for fp in self.files:
      fp.flush()

def get95CI(basemean, expmean, basestd, expstd, nruns):

  foo   = ( (basestd**2) / nruns) + ( (expstd**2) / nruns)
  s_x   = sqrt( foo )

  if nruns < 30:
    
    faz  = (( (basestd**2) / nruns) **2) / (nruns-1)
    fez  = (( (expstd**2)  / nruns) **2) / (nruns-1)
    n_df = int(round( ((foo**2) / (faz + fez)) ))
    
    ci = (student_t_95[n_df] * s_x)

  else:

    # assume normal distribution
    ci = 1.96 * s_x

  return ci

def get95ConfVals(basemean, expmean, basestd, expstd, nruns):

  x_bar = basemean - expmean
  ci    = get95CI(basemean, expmean, basestd, expstd, nruns)
  c1    = x_bar - ci
  c2    = x_bar + ci
  return (c1,c2)

def getResults(benches=jvm2008s, cfgs=defcfgs, metric=HARNESS_TIME, \
               pkg=1, gctype=None, node=0, iters=defiters, \
               miniters=defminiters, benchiters=startiters, chan=0, \
               vnum=defvnum, per_second=False):

  results = {}
  for bench,cfg in expiter(benches, cfgs):

    if not results.has_key(bench):
      results[bench] = {}
    results[bench][cfg] = {}

    if metric in edprocMetrics:
      results[bench][cfg] = getEmonMetricStats(bench, cfg, metric, \
                            iters=iters, miniters=miniters)

    elif metric in pgovMetrics:
      results[bench][cfg] = getPGovMetricStats(bench, cfg, metric, pkg=pkg,
                            iters=iters, miniters=miniters)
    
    elif metric == HARNESS_TIME:
      results[bench][cfg] = getHarnessTimeStats(bench, cfg, iters=iters, \
                            benchiters=benchiters, miniters=miniters)

    elif metric == APP_THREAD_TIME:
      results[bench][cfg] = getAppThreadTimeStats(bench, cfg, iters=iters, \
                            miniters=miniters, benchiters=benchiters)

    elif metric == COMPILE_THREAD_TIME:
      results[bench][cfg] = getCompileThreadTimeStats(bench, cfg, iters=iters, \
                            miniters=miniters, benchiters=benchiters)

    elif metric in gcmets:
      results[bench][cfg] = getGCPowerStats(bench, cfg, metric=metric, \
                            iters=iters, miniters=miniters)

    elif metric == UNIX_TIME:
      results[bench][cfg] = getUnixTimeStats(bench, cfg, \
                            iters=iters, miniters=miniters)[0]

    elif metric == RSS:
      results[bench][cfg] = getUnixTimeStats(bench, cfg, \
                            iters=iters, miniters=miniters)[1]

    elif metric == GC_RUNTIME:
      results[bench][cfg] = getGCRuntimeStats(bench, cfg, gctype=gctype, \
                            iters=iters, miniters=miniters)

    elif metric == NUM_GC:
      results[bench][cfg] = getNumGCStats(bench, cfg, gctype=gctype, \
                            iters=iters, miniters=miniters)

    # MRJ -- need to fix these
    elif metric == MB_ALLOC_TIME:
      results[bench][cfg] = getMemBenchAllocTimeStats(bench, cfg, \
                            iters=iters, miniters=miniters)

    elif metric == MB_THREAD_TIME:
      results[bench][cfg] = getMemBenchThreadTimeStats(bench, cfg, \
                            iters=iters, miniters=miniters)

    elif metric == MB_THREAD_POWER:
      results[bench][cfg] = getMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True)[0]

    elif metric == MB_THREAD_ENERGY:
      results[bench][cfg] = getMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True)[1]

    elif metric == MB_THREAD_READ_BW:
      results[bench][cfg] = getMemBandwidthStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True,
                            vnum=vnum, per_second=per_second)[0]

    elif metric == MB_THREAD_WRITE_BW:
      results[bench][cfg] = getMemBandwidthStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True,
                            vnum=vnum, per_second=per_second)[1]

    elif metric == MB_THREAD_MEMORY_BW:
      results[bench][cfg] = getMemBandwidthStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True,
                            vnum=vnum, per_second=per_second)[2]

    elif metric == CHAN_BANDWIDTH:
      results[bench][cfg] = getChanBandwidthStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, chan=chan, vnum=vnum,
                            per_second=per_second)[2]

    elif metric == MEM_POWER:
      results[bench][cfg] = getMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True)[0]

    elif metric == MEM_ENERGY:
      results[bench][cfg] = getMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True)[1]

    elif metric == MODEL_MEM_POWER:
      results[bench][cfg] = getModelMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, vnum=vnum)[0][0]

    elif metric == MODEL_MEM_ENERGY:
      results[bench][cfg] = getModelMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, vnum=vnum)[0][1]

    elif metric == MODEL_MEM_BG_POWER:
      results[bench][cfg] = getModelMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, vnum=vnum)[1][0]

    elif metric == MODEL_MEM_BG_ENERGY:
      results[bench][cfg] = getModelMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, vnum=vnum)[1][1]

    elif metric == MODEL_MEM_OP_POWER:
      results[bench][cfg] = getModelMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, vnum=vnum)[2][0]

    elif metric == MODEL_MEM_OP_ENERGY:
      results[bench][cfg] = getModelMemPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, vnum=vnum)[2][1]

    elif metric == MODEL_TOTAL_POWER:
      results[bench][cfg] = getModelTotalPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, vnum=vnum)[0]

    elif metric == MODEL_TOTAL_ENERGY:
      results[bench][cfg] = getModelTotalPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, vnum=vnum)[1]

    elif metric == MODEL_CHAN_POWER:
      results[bench][cfg] = getModelChanPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, chan=chan,
                            vnum=vnum)[0][0]

    elif metric == MODEL_CHAN_ENERGY:
      results[bench][cfg] = getModelChanPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, chan=chan,
                            vnum=vnum)[0][1]

    elif metric == MODEL_CHAN_BG_POWER:
      results[bench][cfg] = getModelChanPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, chan=chan,
                            vnum=vnum)[1][0]

    elif metric == MODEL_CHAN_BG_ENERGY:
      results[bench][cfg] = getModelChanPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, chan=chan,
                            vnum=vnum)[1][1]

    elif metric == MODEL_CHAN_OP_POWER:
      results[bench][cfg] = getModelChanPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, chan=chan,
                            vnum=vnum)[2][0]

    elif metric == MODEL_CHAN_OP_ENERGY:
      results[bench][cfg] = getModelChanPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, chan=chan,
                            vnum=vnum)[2][1]

    elif metric == CPU_POWER:
      results[bench][cfg] = getSockPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node)[0]

    elif metric == CPU_ENERGY:
      results[bench][cfg] = getSockPowerStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node)[1]

    elif metric == MEM_READ_BW:
      results[bench][cfg] = getMemBandwidthStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True,
                            per_second=per_second)[0]

    elif metric == MEM_WRITE_BW:
      results[bench][cfg] = getMemBandwidthStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True,
                            per_second=per_second)[1]

    elif metric == MEM_TOTAL_BW:
      results[bench][cfg] = getMemBandwidthStats(bench, cfg, iters=iters,
                            miniters=miniters, node=node, filter=True,
                            per_second=per_second)[2]

  return results

def getReportStrs(benches=jvm2008s, cfgs=defcfgs, metric=HARNESS_TIME, \
                  basecfg=DEFAULT, absolute=False, stat=MEAN, pkg=1, \
                  gctype=None, node=0, chan=0, iters=defiters, \
                  miniters=defminiters, benchiters=startiters, vnum=defvnum,
                  cis=True, per_second=False):

  results = getResults(benches=benches, cfgs=cfgs, metric=metric, pkg=pkg, \
                       gctype=gctype, node=node, chan=chan, iters=iters, \
                       miniters=miniters, benchiters=benchiters, vnum=vnum, \
                       per_second=per_second)

  rstrs = {}
  for bench,cfg in expiter(benches, cfgs):

    # get the baseval
    try:
      baseval = results[bench][basecfg][stat]
    except:
      baseval = None

    # and the expval
    try:
      expval = results[bench][cfg][stat]
    except:
      expval = None
 
    ci = None
    if cis and results[bench][cfg]:
      bstd = results[bench][basecfg][STDEV]
      estd = results[bench][cfg][STDEV]
      ci = get95CI(baseval, expval, bstd, estd, iters)

    # compute the relative value and if its valid
    if not absolute:
      if expval and baseval:
        myval = (expval / baseval)
        myci  = (ci / baseval) if cis else None
      else:
        myval = None
    else:
      if expval:
        myval = expval
        myci  = ci if cis else None
      else:
        myval = None
    
    # store valid values into an average list
    if not rstrs.has_key(AGGREGATE):
      rstrs[AGGREGATE] = {}
    if not rstrs[AGGREGATE].has_key(cfg):
      rstrs[AGGREGATE][cfg] = []

    if myval:
      rstrs[AGGREGATE][cfg].append(myval)

    # store the value string
    if not rstrs.has_key(bench):
      rstrs[bench] = {}

    if myval:
      valstr = "%2.4f" % myval
    else:
      valstr = "N/A"

    if ci:
      cistr = "%2.4f" % myci
    else:
      cistr = "N/A"

    rstrs[bench][cfg] = (valstr, cistr)

  for cfg in cfgs:
    if rstrs[AGGREGATE][cfg]:
      if absolute:
        rstrs[AGGREGATE][cfg] = ("%2.4f" % average(rstrs[AGGREGATE][cfg]), "-")
      else:
        rstrs[AGGREGATE][cfg] = ("%2.4f" % geomean(rstrs[AGGREGATE][cfg]), "-")
    else:
      rstrs[AGGREGATE][cfg] = ("N/A", "-")

  return rstrs

def report(benches=jvm2008s, cfgs=defcfgs, metric=HARNESS_TIME, pkg=1, \
           stat=MEAN, basecfg=DEFAULT, gctype=None, node=0, chan=0, \
           vnum=defvnum, absolute=False, iters=defiters, \
           miniters=defminiters, benchiters=startiters, cis=False,
           per_second=False):

  # hack to remove old bg procs:
  if bgprocs:
    clearBGProcs()
  
  rstrs = getReportStrs(benches=benches, cfgs=cfgs, metric=metric, pkg=pkg, \
                        stat=stat, basecfg=basecfg, absolute=absolute, \
                        gctype=gctype, node=node, chan=chan, iters=iters, \
                        miniters=miniters, benchiters=benchiters, vnum=vnum, \
                        cis=cis, per_second=per_second)

  print "Report for metric: %s\n" % metric

  cfgs = [basecfg] + [ x for x in cfgs if not x == basecfg ]
  print "".ljust(30),
  for cfg in cfgs:
    print ("%s" % cfg).ljust(22),
  print ""

  benches = (benches + [AGGREGATE])
  for bench in benches:
    if bench == AGGREGATE:
      if absolute:
        print "average".ljust(30),
      else:
        print "geomean".ljust(30),
    else:
      print ("%s" % bench).ljust(30),
    for cfg in cfgs:
      if cis:
        print ("%s" % rstrs[bench][cfg][0]).ljust(7),
        print ("%s" % rstrs[bench][cfg][1]).ljust(14),
      else:
        print ("%s" % rstrs[bench][cfg][0]).ljust(22),
    print ""

def printPowerGovData(bench, cfg, iter, outf=sys.stdout):
  try:
    pgf = open(pgovRawFile(bench, cfg, iter))
  except:
    print "error opening %s" % pgovRawFile(bench, cfg, iter)
    raise SystemExit(1)

  for line in pgf:
    print >> outf, line.strip()

def ppemetric(edd, metric, pkgs=[0,1], outf=sys.stdout):

  print >> outf, "TIME".ljust(20),
  for p,pkg in enumerate(edd[0][METRECS][metric]):
    if p in pkgs:
      for c,chn in enumerate(pkg):
        print >> outf, ("PKG%d_CHN%d" % (p,c)).ljust(14),
  print >> outf, ""

  for vrec in edd:
    print >> outf, ("%d" % vrec[TIMEVAL][1]).ljust(20),
    for p,pkg in enumerate(vrec[METRECS][metric]):
      if p in pkgs:
        for dat in pkg:
          print >> outf, ("%d" % dat).ljust(14),
    print >> outf, ""

def getedd(rawfile, edashvfile, metrics=[], timeshift=defts):

  emon_start = None
  try:
    edashvf = open(edashvfile)
  except:
    print "error opening %s" % edashvfile
    raise SystemExit(1)

  for line in edashvf:
    if emonStartRE.match(line):
      emon_start = long(line.split()[2])
      break

  if emon_start is None:
    print "error parsing %s - no emon start time!" % edashvfile
    raise SystemExit(1)
  edashvf.close()

  try:
    rawf = open(rawfile)
  except:
    print "error opening %s" % rawfile
    raise SystemExit(1)

  edd = []
  for line in rawf:

    # scan to time val
    if emonTimeRE.match(line):
      valstart = float(line.split()[6])
      valend   = float(line.split()[8])
      adjstart = (valstart * 1000) + emon_start
      adjend   = (valend * 1000)   + emon_start

      valrec = {}
      valrec[TIMEVAL] = (adjstart, adjend)
      valrec[METRECS] = {}

      for line in rawf:

        # scan until the next counter
        if emonCounterRE.match(line):
          counter = line.split()[2]
          if not counter in metrics:
            continue

          # ignore irrelevant lines
          rawf.next()
          rawf.next()

          # now we expect the pkg lines
          valrec[METRECS][counter] = []
          for line in rawf:
            if not emonPkgRE.match(line):
              break
            pdata = tuple([ int(x) for x in line.split()[2:] ])
            valrec[METRECS][counter].append(pdata)

        elif emonSeparatorRE.match(line):
          break

      # only add this record if we have at least one metric at that val
      if valrec[METRECS]:
        edd.append(valrec)

  return edd

def getecc(rawfile, edashvfile, timeshift=defts):

  emon_start = None
  try:
    edashvf = open(edashvfile)
  except:
    print "error opening %s" % edashvfile
    raise SystemExit(1)

  for line in edashvf:
    if emonStartRE.match(line):
      emon_start = long(line.split()[2])
      break

  if emon_start is None:
    print "error parsing %s - no emon start time!" % edashvfile
    raise SystemExit(1)
  edashvf.close()

  try:
    rawf = open(rawfile)
  except:
    print "error opening %s" % rawfile
    raise SystemExit(1)

  ecc = []
  for line in rawf:

    # scan to time val
    if emonTimeRE.match(line):
      valstart = float(line.split()[6])
      valend   = float(line.split()[8])
      adjstart = (valstart * 1000) + emon_start
      adjend   = (valend * 1000)   + emon_start

      valrec = {}
      valrec[TIMEVAL] = (adjstart, adjend)
      valrec[METRECS] = {}
      valrec[METRECS][CAS_RD] = []
      valrec[METRECS][CAS_WR] = []
      valrec[METRECS][CAS_RW] = []

      for type in valrec[METRECS].keys():
        for pkg in [0,1]:
          valrec[METRECS][type].append(tuple([0,0,0]))

      for line in rawf:

        # scan until the next counter
        if emonCounterRE.match(line):
          counter = line.split()[2]
          if not counter in cas_rw_evts:
            continue

          # skip over junk before the package lines
          for line in rawf:
            if line == "\n":
              break

          # now we expect the pkg lines
          for line in rawf:
            if not emonPkgRE.match(line):
              break

            pnum   = int(line.split()[1].strip(":"))
            val    = int(line.split()[2])
            cur_rd = valrec[METRECS][CAS_RD][pnum]
            cur_wr = valrec[METRECS][CAS_WR][pnum]
            cur_rw = valrec[METRECS][CAS_RW][pnum]

            if counter == UNC_DRAM_READ_CAS_dot_CH0:
              valrec[METRECS][CAS_RD][pnum] = (cur_rd[0]+val,cur_rd[1],cur_rd[2])
              valrec[METRECS][CAS_RW][pnum] = (cur_rw[0]+val,cur_rw[1],cur_rw[2])
            elif counter == UNC_DRAM_READ_CAS_dot_CH1:
              valrec[METRECS][CAS_RD][pnum] = (cur_rd[0],cur_rd[1]+val,cur_rd[2])
              valrec[METRECS][CAS_RW][pnum] = (cur_rw[0],cur_rw[1]+val,cur_rw[2])
            elif counter == UNC_DRAM_READ_CAS_dot_CH2:
              valrec[METRECS][CAS_RD][pnum] = (cur_rd[0],cur_rd[1],cur_rd[2]+val)
              valrec[METRECS][CAS_RW][pnum] = (cur_rw[0],cur_rw[1],cur_rw[2]+val)
            elif counter == UNC_DRAM_WRITE_CAS_dot_CH0:
              valrec[METRECS][CAS_WR][pnum] = (cur_wr[0]+val,cur_wr[1],cur_wr[2])
              valrec[METRECS][CAS_RW][pnum] = (cur_rw[0]+val,cur_rw[1],cur_rw[2])
            elif counter == UNC_DRAM_WRITE_CAS_dot_CH1:
              valrec[METRECS][CAS_WR][pnum] = (cur_wr[0],cur_wr[1]+val,cur_wr[2])
              valrec[METRECS][CAS_RW][pnum] = (cur_rw[0],cur_rw[1]+val,cur_rw[2])
            elif counter == UNC_DRAM_WRITE_CAS_dot_CH2:
              valrec[METRECS][CAS_WR][pnum] = (cur_wr[0],cur_wr[1],cur_wr[2]+val)
              valrec[METRECS][CAS_RW][pnum] = (cur_rw[0],cur_rw[1],cur_rw[2]+val)
            else:
              # should not reach here
              raise SystemExit(1)

        elif emonSeparatorRE.match(line):
          break

      ecc.append(valrec)

  return ecc

def getEmonDataBench(bench, cfg, iter, metrics=[], timeshift=defts):

  rawfile    = emonRawFile(bench, cfg, iter)
  edashvfile = emonDashvFile(bench, cfg, iter)
  return getedd(rawfile, edashvfile, metrics=metrics, timeshift=timeshift)

def printEmonMetricDataBench(bench, cfg, iter, metric, pkgs=[0,1], \
                             timeshift=defts, outf=sys.stdout):

  edd = getEmonDataBench(bench, cfg, iter, metrics=[metric], timeshift=timeshift)

  if not edd:
    print >> outf, "no emon data for exp: (%s, %s, i%d), metric = %s" % \
          (bench, cfg, iter, metric)
    return

  print >> outf, "Emon metric data for exp: (%s, %s, i%d), metric = %s" % \
          (bench, cfg, iter, metric)

  ppemetric(edd, metric, pkgs=pkgs, outf=outf)

def printEmonDataBench(bench, cfg, iter, metrics=[], pkgs=[0,1], timeshift=defts, \
                       outf=sys.stdout):

  if len(metrics) == 1:
    printEmonMetricDataBench(bench, cfg, iter, metrics[0], pkgs=pkgs, \
                             timeshift=timeshift, outf=outf)
  else:
    printEmonMetricsDataBench(bench, cfg, iter, metrics[0], pkgs=pkgs, \
                              timeshift=timeshift, outf=outf)

def getCASCountsBench(bench, cfg, iter, timeshift=defts):

  rawfile    = emonRawFile(bench, cfg, iter)
  edashvfile = emonDashvFile(bench, cfg, iter)
  return getecc(rawfile, edashvfile, timeshift=timeshift)

def printCASCountsBench(bench, cfg, iter, metric, pkgs=[0,1], timeshift=defts,
                        outf=sys.stdout):

  ecc = getCASCountsBench(bench, cfg, iter, timeshift=timeshift)

  if not ecc:
    print >> outf, "no CAS counts for exp: (%s, %s, i%d), metric=%s" % \
          (bench, cfg, iter, metric)
    return

  print >> outf, "CAS counts for exp: (%s, %s, i%d) metric=%s" % \
          (bench, cfg, iter, metric)

  ppemetric(ecc, metric, pkgs=pkgs, outf=outf)

def ppemetsBench(bench, cfg, iter, ppemets):
  for metric in ppemets:
    outf = open(ppEmonMetricFile(bench, cfg, iter, metric), 'w')
    if getcpu() == ecuador:
      printCASCountsBench(bench, cfg, iter, metric, outf=outf)
    else:
      printEmonMetricDataBench(bench, cfg, iter, metric, outf=outf)

def generic_val_report(dynsets, outf=sys.stdout):

  print >> outf, "".ljust(2), "VAL".ljust(13),
  dyncolors = sorted(dynsets[0L].keys())
  for color in dyncolors:
    print >> outf, ("%s" % color.upper()).ljust(34),
  print >> outf, "total".ljust(34),
  print >> outf, ""

  print >> outf, "".ljust(16),
  for color in dyncolors:
    print >> outf, "ref".ljust(10), "obj".ljust(10), "size".ljust(12),
  print >> outf, "ref".ljust(10), "obj".ljust(10), "size".ljust(12),
  print >> outf, ""

  for val in sorted(dynsets.keys()):
    print >> outf, "".ljust(2),("%-13d" % val),

    tcolor = {}
    for color in dyncolors:
      tcolor[color] = ( sum ([ x[1] for x in dynsets[val][color] ]),
                        len(dynsets[val][color]),
                        sum ([ x[2] for x in dynsets[val][color] ]) )

    tots = [ sum([ tcolor[color][i] for color in tcolor.keys() ]) \
                                    for i in range(3) ]

    for color in dyncolors:
      for i,tot in enumerate(tots):
        if tot == 0:
          assert tcolor[color][i] == 0, "zero total with non-zero value"
          print >> outf, ("%2.4f" % float(0)).ljust(10),
        else:
          print >> outf, ("%2.4f" % \
            (float(tcolor[color][i]) / float(tot))).ljust(10),
      print >> outf,"".ljust(1),

    for i,tot in enumerate(tots):
      print >> outf, ("%d" % tot).ljust(10),
    print >> outf,"".ljust(1), ""

def hotset_val_report(bench, cfg, iter, dynsets=None, threshold=100, \
                       ratio=0.1, quiet=False):

  if dynsets is None:
    dynsets = getDynamicHotSets(bench, cfg, iter, threshold=threshold, \
                                 ratio=ratio)

  hvrf = try_open_write(hotsetValReport(bench, cfg, iter, threshold, ratio))
  if quiet:
    outf = hvrf
  else:
    outf = TeeFile(sys.stdout,hvrf)

  print >> outf, "Dynamic hot sets: %s-%s-%d-t%d-r%d\n" % \
                 (bench,cfg,iter,threshold,int(ratio*100))

  generic_val_report(dynsets, outf=outf)
  hvrf.close()

def coldset_val_report(bench, cfg, iter, dynsets=None, threshold=0, \
                      quiet=False):

  if dynsets is None:
    dynsets = getDynamicColdSets(bench, cfg, iter, threshold=threshold)

  cvrf = try_open_write(coldsetValReport(bench, cfg, iter, threshold))
  if quiet:
    outf = cvrf
  else:
    outf = TeeFile(sys.stdout,cvrf)

  print >> outf, "Dynamic cold sets: %s-%s-%d-t%d\n" % \
                 (bench,cfg,iter,threshold)

  generic_val_report(dynsets, outf=outf)
  cvrf.close()

def getInvCntResults(benches=jvm2008s, cfgs=defcfgs):

  results = {}
  for bench,cfg in expiter(benches, cfgs):

    # store the value string
    if not results.has_key(bench):
      results[bench] = {}

    cnts = getInvocationCounts(bench, cfg)

    for method,ic,bec in cnts:
      if ic is None or bec is None:
        print method

    compiles    = [ (ic,bec) for method,ic,bec in cnts \
                    if (ic+bec) > (compile_threshold) ]
    mispredicts = [ (ic,bec) for ic,bec in compiles \
                    if (ic+bec) < (compile_threshold*2) ]

    results[bench][cfg] = (len(compiles), len(mispredicts))

  return results

def getInvCntReportStrs(res=None, benches=jvm2008s, cfgs=defcfgs, absolute=False):

  if not res:
    results = getInvCntResults(benches=benches, cfgs=cfgs)
  else:
    results = res

  rstrs = {}
  for bench,cfg in expiter(benches, cfgs):

    if not rstrs.has_key(bench):
      rstrs[bench] = {}

    if not rstrs.has_key(GEOMEAN):
      rstrs[GEOMEAN] = {}
    if not rstrs[GEOMEAN].has_key(cfg):
      rstrs[GEOMEAN][cfg] = []

    try:
      ncomps,nmisses = results[bench][cfg]
    except:
      ncomps = nmisses = None

    if absolute:
      if ncomps:
        rstrs[bench][cfg] = "%7d / %7d" % (nmisses,ncomps)
        rstrs[GEOMEAN][cfg].append((nmisses,ncomps))
      else:
        rstrs[bench][cfg] = "N/A"
    else:
      if ncomps:
        val = float(nmisses) / float(ncomps)
        rstrs[bench][cfg] = "%2.4f" % val
        rstrs[GEOMEAN][cfg].append(val)
      else:
        rstrs[bench][cfg] = "N/A"

  for cfg in cfgs:
    if absolute:
      if rstrs[GEOMEAN][cfg]:
        num = sum([ x[0] for x in rstrs[GEOMEAN][cfg] ])
        den = sum([ x[1] for x in rstrs[GEOMEAN][cfg] ])
        #rstrs[GEOMEAN][cfg] = "%7d / %7d (%5.4f)" % (num, den, rat)
        rstrs[GEOMEAN][cfg] = "%7d / %7d" % (num, den)
      else:
        rstrs[GEOMEAN][cfg] = "N/A"
    else:
      if rstrs[GEOMEAN][cfg]:
        rstrs[GEOMEAN][cfg] = "%2.4f" % geomean(rstrs[GEOMEAN][cfg])
      else:
        rstrs[GEOMEAN][cfg] = "N/A"
  return rstrs

def invocationCountReport(res=None, benches=jvm2008s, cfgs=defcfgs, absolute=False):

  rstrs = getInvCntReportStrs(res=res, benches=benches, cfgs=cfgs, absolute=absolute)

  print "Invocation Count Report\n"

  #cfgs = [basecfg] + [ x for x in cfgs if not x == basecfg ]
  print "".ljust(30),
  for cfg in cfgs:
    print ("%s" % cfg).rjust(20),
  print ""

  benches = (benches + [GEOMEAN])
  for bench in benches:
    if absolute and bench == GEOMEAN:
      print ("sum").ljust(30),
      for cfg in cfgs:
        print ("%s" % rstrs[bench][cfg]).rjust(20),
        num = float(rstrs[bench][cfg].split()[0])
        den = float(rstrs[bench][cfg].split()[2]) 
        print ("(%5.4f)" % (num/den)),
    else:
      print ("%s" % bench).ljust(30),
      for cfg in cfgs:
        print ("%s" % rstrs[bench][cfg]).rjust(20),
    print ""

def color_obj_info_val_report(bench, cfg, iter=0, rssmet=SIZE, \
  gens=coloredGenKeys, spaces=coloredSpaceKeys, den_gens=coloredGenKeys, \
  den_spaces=coloredSpaceKeys, oinfo=None, quiet=False, misspec=False):

  if oinfo is None:
    oinfo = objInfoVals(bench, cfg, iter=iter)

  coivrf = try_open_write(colorValReport(bench, cfg, iter))
  if quiet:
    outf = coivrf
  else:
    outf = TeeFile(sys.stdout,coivrf)
 
  if not rssmet in [OBJECTS, SIZE]:
    print "unknown rssmet, using size"
    rssmet = SIZE

  if rssmet == OBJECTS:
    liverss = LIVE_OBJECTS
    hotrss  = HOT_OBJECTS
    rsshead = "objs"
  else:
    liverss = LIVE_SIZE
    hotrss  = HOT_SIZE
    rsshead = "size (KB)"

  print >> outf, "Object Info Val Report: gens: ", gens, " spaces: ", spaces
  print >> outf, "".ljust(2),
  print >> outf, "val".ljust(6),
  print >> outf, ("%s"%rsshead).ljust(20),
  print >> outf, ("hot %s"%rsshead).ljust(20),
  print >> outf, "refs".ljust(20),
  print >> outf, ("surv %s"%rsshead).ljust(20),
  print >> outf, ("hot surv %s"%rsshead).ljust(20),
  print >> outf, "surv refs".ljust(20),
  if misspec:
    print >> outf, "mis-spec".ljust(20)
  else:
    print >> outf, "to reds".ljust(20),
    print >> outf, "to blues".ljust(20)

  for vnum,valinfo in enumerate(oinfo):
    print >> outf, "".ljust(2),
    print >> outf, ("%d"%vnum).ljust(6),

    # lives
    colorlives = getval(valinfo, PRE, gens, spaces, liverss)
    liverat = getrat(valinfo, \
                    PRE, gens, spaces, liverss, \
                    PRE, den_gens, den_spaces,  liverss)
    liveratstr = ("%2.3f"%liverat) if not liverat is None else "None"
    print >> outf, ("%-9d (%s)" % (colorlives, liveratstr)).ljust(20),

    # hots
    colorhots = getval(valinfo, PRE, gens, spaces, hotrss)
    hotrat = getrat(valinfo, \
                    PRE, gens, spaces, hotrss, \
                    PRE, den_gens, den_spaces,  hotrss)
    hotratstr = ("%2.3f"%hotrat) if not hotrat is None else "None"
    print >> outf, ("%-9d (%s)" % (colorhots, hotratstr)).ljust(20),

    # refs
    colorrefs = getval(valinfo, PRE, gens, spaces, LIVE_REFS)
    refsrat = getrat(valinfo, \
                     PRE, gens, spaces, LIVE_REFS, \
                     PRE, den_gens, den_spaces,  LIVE_REFS)
    refsratstr = ("%2.3f"%refsrat) if not refsrat is None else "None"
    print >> outf, ("%-9d (%s)" % (colorrefs, refsratstr)).ljust(20),

    # surv lives
    #survlives      = getval(valinfo, YCS, gens, ycsFromSpaces(spaces), liverss)
    survlives      = getval(valinfo, POST, gens, spaces, liverss)
    survliverat    = getSurvRat(valinfo, gens, spaces, liverss)
    survliveratstr = ("%2.3f"%survliverat) if not survliverat is None else "None"
    print >> outf, ("%-9d (%s)" % (survlives, survliveratstr)).ljust(20),

    # surv hots
    #survhots      = getval(valinfo, YCS, gens, ycsFromSpaces(spaces), hotrss)
    survhots      = getval(valinfo, POST, gens, spaces, hotrss)
    survhotrat    = getSurvRat(valinfo, gens, spaces, hotrss)
    survhotratstr = ("%2.3f"%survhotrat) if not survhotrat is None else "None"
    print >> outf, ("%-9d (%s)" % (survhots, survhotratstr)).ljust(20),

    # surv refs
    #survrefs      = getval(valinfo, YCS, gens, ycsFromSpaces(spaces), LIVE_REFS)
    survrefs      = getval(valinfo, POST, gens, spaces, LIVE_REFS)
    survrefrat    = getSurvRat(valinfo, gens, spaces, LIVE_REFS)
    survrefratstr = ("%2.3f"%survrefrat) if not survrefrat is None else "None"
    print >> outf, ("%-9d (%s)" % (survrefs, survrefratstr)).ljust(20),

    if misspec:
      nmissed = 0
      if RED in spaces:
        nmissed += (getval(valinfo, PRE, gens, [RED], liverss) - \
                    getval(valinfo, PRE, gens, [RED], hotrss))
      if BLUE in spaces:
        nmissed += getval(valinfo, PRE, gens, [BLUE], hotrss)
      den      = getval(valinfo, PRE, gens, spaces, liverss)
      nmrat    = (float(nmissed) / den) if den > 0 else None
      nmratstr = ("%2.3f"%nmrat) if not nmrat is None else "None"
      print >> outf, ("%-9d (%s)" % (nmissed, nmratstr)).ljust(20)
    else:
      # to reds
      to_red_spaces = [ x for x in ycsFromSpaces(spaces) \
                        if x in ycsToSpaces([RED]) ]

      r2rlives    = getval(valinfo, YCS, gens, to_red_spaces, liverss)
      r2rlivesrat = getrat(valinfo, \
                          YCS, gens, to_red_spaces,           liverss, \
                          YCS, gens, ycsFromSpaces(spaces), liverss)
      r2rlivesratstr = ("%2.3f"%r2rlivesrat) if not r2rlivesrat is None else "None"
      print >> outf, ("%-9d (%s)" % (r2rlives, r2rlivesratstr)).ljust(20),

      # to blues
      to_blue_spaces = [ x for x in ycsFromSpaces(spaces) \
                         if x in ycsToSpaces([BLUE]) ]

      r2blives    = getval(valinfo, YCS, gens, to_blue_spaces, liverss)
      r2blivesrat = getrat(valinfo, \
                          YCS, gens, to_blue_spaces,           liverss, \
                          YCS, gens, ycsFromSpaces(spaces), liverss)
      r2blivesratstr = ("%2.3f"%r2blivesrat) if not r2blivesrat is None else "None"
      print >> outf, ("%-9d (%s)" % (r2blives, r2blivesratstr)).ljust(20)

def batch_color_val_reports(benches=dacapo_defs, cfgs=[OBJ_ORG_PROFILE], pos=PRE, \
  iters=1, include_perms=False, quiet=True):

  for bench,cfg,iter in expiteriter(benches, cfgs, iters):
    print "%s-%s-%d" % (bench, cfg, iter)
    color_obj_info_val_report(bench, cfg, iter, pos=pos, \
      include_perms=include_perms, quiet=quiet)

#def active_set_val_report(bench, cfg, iter=0, include_perms=False, oinfo=None, \
#  quiet=False):
#
#  if oinfo is None:
#    oinfo = objInfoVals(bench, cfg, iter=iter)
#
#  asvrf = try_open_write(activeSetValReport(bench, cfg, iter))
#  if quiet:
#    outf = asvrf
#  else:
#    outf = TeeFile(sys.stdout,asvrf)
#
#  spaces = RED_BLUE
#  if include_perms:
#    spaces = ALL_SPACES
#
#  print >> outf, "Active Set Val Report\n"
#  print >> outf, "".ljust(2),
#  print >> outf, "val".ljust(6),
#  print >> outf, "live_objs".ljust(16),
#  print >> outf, "live_size (KB)".ljust(16),
#  print >> outf, "cold_objs".ljust(16),
#  print >> outf, "cold_size".ljust(16)
#
#  for vnum,valinfo in enumerate(oinfo):
#    print >> outf, "".ljust(2),
#    print >> outf, ("%d"%vnum).ljust(6),
#
#    print >> outf, ("%d"%liveObjs(valinfo,pos=POST,spaces=spaces)).ljust(16),
#    print >> outf, ("%d"%liveSize(valinfo,pos=POST,spaces=spaces)).ljust(16),
#    print >> outf, ("%2.4f"%coldObjsRat(valinfo,pos=POST,spaces=spaces)).ljust(16),
#    print >> outf, ("%2.4f"%coldSizeRat(valinfo,pos=POST,spaces=spaces))
#
#def batch_active_val_reports(benches=dacapo_defs, cfgs=[OBJ_ORG_PROFILE], iters=1, \
#  include_perms=False, quiet=True):
#
#  for bench,cfg,iter in expiteriter(benches, cfgs, iters):
#    print "%s-%s-%d" % (bench, cfg, iter)
#    active_set_val_report(bench, cfg, iter, \
#      include_perms=include_perms, quiet=quiet)

def getActiveSetReportStrs(benches=dacapo_defs, cfgs=[OBJ_ORG_PROFILE], \
  iter=0, infos=stdinfos, rssmet=SIZE, gens=coloredGenKeys, \
  spaces=coloredSpaceKeys, den_gens=coloredGenKeys,
  den_spaces=coloredSpaceKeys, stat=AVERAGE, ratstat=GEOMEAN, sumrat=False, \
  res=None):

  if not res:
    oinfo = getObjInfos(benches=benches, cfgs=cfgs, iter=iter)
  else:
    oinfo = res

  if not rssmet in [OBJECTS, SIZE]:
    print "invalid rssmet: %s" % rssmet
    raise SystemExit(1)

  if not stat in [AVERAGE, MEDIAN]:
    print "unknown stat, using average"
    val_agg_func = average

  if stat == AVERAGE:
    val_agg_func = average
  else:
    val_agg_func = median

  if not ratstat in [GEOMEAN]:
    print "unknown ratstat, using geomean"
  rat_agg_func = geomean

  if rssmet == OBJECTS:
    liverss = LIVE_OBJECTS
    hotrss  = HOT_OBJECTS
  else:
    liverss = LIVE_SIZE
    hotrss  = HOT_SIZE

  to_red_spaces  = [ x for x in ycsFromSpaces(spaces) \
                     if x in ycsToSpaces([RED]) ]

  to_blue_spaces = [ x for x in ycsFromSpaces(spaces) \
                     if x in ycsToSpaces([BLUE]) ]

  rstrs = {}
  for bench,cfg in expiter(benches, cfgs):

    if not rstrs.has_key(bench):
      rstrs[bench] = {}

    if not rstrs.has_key(AGGREGATE):
      rstrs[AGGREGATE] = {}
      rstrs[AGGREGATE]["*"] = {}
      for info in infos:
        rstrs[AGGREGATE]["*"][info] = []

    if not rstrs[AGGREGATE].has_key(cfg):
      rstrs[AGGREGATE][cfg] = {}
      for info in infos:
        rstrs[AGGREGATE][cfg][info] = []

    if not rstrs[bench].has_key(cfg):
      rstrs[bench][cfg] = {}

    try:
      bcinfo = oinfo[bench][cfg]
    except:
      bcinfo = None

    if bcinfo:
      for info in infos:
        val = ratval = None

        if info == INFO_NVALS:
          val = len(bcinfo)

        elif info == INFO_LIVE_RSS:
          val = asinfo_val_agg(val_agg_func, bcinfo, PRE, gens, spaces, liverss)
          if sumrat:
            ratval = asinfo_sum_rat(bcinfo, \
                                    PRE,     gens,     spaces, liverss, \
                                    PRE, den_gens, den_spaces, liverss)
          else:
            ratval = asinfo_rat_agg(rat_agg_func, bcinfo, \
                                    PRE,     gens,     spaces, liverss, \
                                    PRE, den_gens, den_spaces, liverss)

        elif info == INFO_HOT_RSS:
          val = asinfo_val_agg(val_agg_func, bcinfo, PRE, gens, spaces, hotrss)
          if sumrat:
            ratval = asinfo_sum_rat(bcinfo, \
                                    PRE,     gens,     spaces, hotrss, \
                                    PRE, den_gens, den_spaces, hotrss)
          else:
            ratval = asinfo_rat_agg(rat_agg_func, bcinfo, \
                                    PRE,     gens,     spaces, hotrss, \
                                    PRE, den_gens, den_spaces, hotrss)

        elif info == INFO_REFS: 
          val = asinfo_val_agg(val_agg_func, bcinfo, PRE, gens, spaces, LIVE_REFS)
          if sumrat:
            ratval = asinfo_sum_rat(bcinfo, \
                                    PRE,     gens,     spaces, LIVE_REFS, \
                                    PRE, den_gens, den_spaces, LIVE_REFS)
          else:
            ratval = asinfo_rat_agg(rat_agg_func, bcinfo, \
                                    PRE,     gens,     spaces, LIVE_REFS, \
                                    PRE, den_gens, den_spaces, LIVE_REFS)

        elif info == INFO_SURV_RSS:
#          val = asinfo_val_agg(val_agg_func, bcinfo, YCS, gens, \
#                               ycsFromSpaces(spaces), liverss)
          val = asinfo_val_agg(val_agg_func, bcinfo, POST, gens, \
                               spaces, liverss)
          if sumrat:
            ratval = asinfo_surv_sum_rat(bcinfo, gens, spaces, liverss)
          else:
            ratval = asinfo_surv_rat_agg(rat_agg_func, bcinfo, gens, \
                                         spaces, liverss)

        elif info == INFO_HOT_SURV_RSS:
#          val = asinfo_val_agg(val_agg_func, bcinfo, YCS, gens, \
#                               ycsFromSpaces(spaces), hotrss)
          val = asinfo_val_agg(val_agg_func, bcinfo, POST, gens, \
                               spaces, hotrss)
          if sumrat:
            ratval = asinfo_surv_sum_rat(bcinfo, gens, spaces, hotrss)
          else:
            ratval = asinfo_surv_rat_agg(rat_agg_func, bcinfo, gens, \
                                         spaces, hotrss)

        elif info == INFO_SURV_REFS:
#          val = asinfo_val_agg(val_agg_func, bcinfo, YCS, gens, \
#                               ycsFromSpaces(spaces), LIVE_REFS)
          val = asinfo_val_agg(val_agg_func, bcinfo, POST, gens, \
                               spaces, LIVE_REFS)
          if sumrat:
            ratval = asinfo_surv_sum_rat(bcinfo, gens, spaces, LIVE_REFS)
          else:
            ratval = asinfo_surv_rat_agg(rat_agg_func, bcinfo, gens, \
                                         spaces, LIVE_REFS)

        elif info == INFO_TO_REDS:
          val = asinfo_val_agg(val_agg_func, bcinfo, YCS, gens, \
                               to_red_spaces, liverss)
          if sumrat:
            ratval = asinfo_sum_rat(bcinfo, \
                                    YCS, gens, to_red_spaces,         liverss, \
                                    YCS, gens, ycsFromSpaces(spaces), liverss)
          else:
            ratval = asinfo_rat_agg(rat_agg_func, bcinfo, \
                                    YCS, gens, to_red_spaces,         liverss, \
                                    YCS, gens, ycsFromSpaces(spaces), liverss)

        elif info == INFO_TO_BLUES:
          val = asinfo_val_agg(val_agg_func, bcinfo, YCS, gens, \
                               to_blue_spaces, liverss)
          if sumrat:
            ratval = asinfo_sum_rat(bcinfo, \
                                    YCS, gens, to_blue_spaces,        liverss, \
                                    YCS, gens, ycsFromSpaces(spaces), liverss)
          else:
            ratval = asinfo_rat_agg(rat_agg_func, bcinfo, \
                                    YCS, gens, to_blue_spaces,        liverss, \
                                    YCS, gens, ycsFromSpaces(spaces), liverss)

        elif info == INFO_MIS_SPEC:
          val = 0
          if RED in spaces:
            val +=  cold_agg(val_agg_func, bcinfo, gens, [RED], liverss, hotrss)
          if BLUE in spaces:
            val +=  asinfo_val_agg(val_agg_func, bcinfo, PRE, gens, \
                                   [BLUE], hotrss)
          if sumrat:
            misses = 0 
            if RED in spaces:
              misses +=  cold_agg(sum, bcinfo, gens, [RED], liverss, hotrss)
            if BLUE in spaces:
              misses +=  asinfo_val_agg(sum, bcinfo, PRE, gens, [BLUE], hotrss)
            lives  = asinfo_val_agg(sum, bcinfo, \
                                    PRE, den_gens, den_spaces, liverss)
            #print misses, lives
            ratval = float(misses) / lives
          else:
            ratval = None

        if info in ratinfos:
          ratstr = ("%2.3f"%ratval) if not ratval is None else "None"
          rstrs[bench][cfg][info] = "%10.1f  %s" % (val, ratstr)
        else:
          rstrs[bench][cfg][info] = "%10.1f" % (val)

        rstrs[AGGREGATE][cfg][info].append((val,ratval))
        rstrs[AGGREGATE]["*"][info].append((val,ratval))
    else:
      for info in infos:
        rstrs[bench][cfg][info] = "N/A"

  for cfg in rstrs[AGGREGATE].keys():
    for info in rstrs[AGGREGATE][cfg].keys():
      #print cfg
      #print rstrs[AGGREGATE][cfg]
      vals = [ x[0] for x in rstrs[AGGREGATE][cfg][info] ]
      if info in ratinfos:
        ratvals = [ x[1] for x in rstrs[AGGREGATE][cfg][info] ]
        ratstr  = ("%2.3f"%rat_agg_func(ratvals))
        rstrs[AGGREGATE][cfg][info] = ("%10.1f  %2.3f" % \
                                       (val_agg_func(vals), rat_agg_func(ratvals)))
      else:
        rstrs[AGGREGATE][cfg][info] = ("%10.1f" % val_agg_func(vals))

  return rstrs

def activeSetReport(benches=dacapo_defs, cfgs=[OBJ_ORG_PROFILE], \
  iter=0, infos=stdinfos, rssmet=SIZE, gens=coloredGenKeys, \
  spaces=coloredSpaceKeys, den_gens=coloredGenKeys,
  den_spaces=coloredSpaceKeys, stat=AVERAGE, ratstat=GEOMEAN, sumrat=False,\
  res=None):

  if not rssmet in [OBJECTS, SIZE]:
    print "unknown rssmet, using size"
    rssmet = SIZE

  rstrs = getActiveSetReportStrs(benches=benches, cfgs=cfgs, iter=iter, \
            infos=infos, rssmet=rssmet, gens=gens, spaces=spaces, \
            den_gens=den_gens, den_spaces=den_spaces, stat=stat, \
            ratstat=ratstat, sumrat=sumrat, res=res)

  if rssmet == OBJECTS:
    rsshead = "objs"
  else:
    rsshead = "size (KB)"

  sortedInfos = [ x[0] for x in \
                  sorted( [ (i, (orderedInfos[i])) for i in infos ], \
                          key=itemgetter(1) ) ]

  print "Active Set Report: gens: ", gens, " spaces: ", spaces, "\n"
  print "".ljust(2),
  print "bench".ljust(20),
  print "config".ljust(20),
  for info in sortedInfos:
    header = activeSetHeaders[info]
    if info in rssinfos:
      header = header%rsshead
    if info in ratinfos:
      print header.rjust(20),
    else:
      print header.rjust(10),
  print ""

  bswagg = benches + [AGGREGATE]
  for bench in bswagg:
    for cfg in sorted(rstrs[bench],reverse=True):
      print "".ljust(2),
      print bench.ljust(20),
      print cfg.ljust(20),

      for info in sortedInfos:
        if info in ratinfos:
          print rstrs[bench][cfg][info].rjust(20),
        else:
          print rstrs[bench][cfg][info].rjust(10),
      print ""

#def getHotInfoReportStrs(benches=dacapo_defs, cfgs=[OBJ_ORG_PROFILE], iter=0, \
#  rssmet=SIZE):
#
##  if not res:
##    hinfo = getObjInfos(benches=benches, cfgs=cfgs, iter=iter)
##  else:
##    hinfo = res
#
#  if not rssmet in [OBJECTS, SIZE]:
#    print "invalid rssmet: %s" % rssmet
#    raise SystemExit(1)
#
#  rstrs = {}
#  for bench,cfg in expiter(benches, cfgs):
#
#    if not rstrs.has_key(bench):
#      rstrs[bench] = {}
#
#    if not rstrs[bench].has_key(cfg):
#      rstrs[bench][cfg] = {}
#
#    try:
#      bcinfo = oinfo[bench][cfg]
#    except:
#      bcinfo = None
#
#    if bcinfo:
#      hotd = shelve.open(shmHotDict(bench, cfg, iter))
#

def getOldObjectReportStrs(benches=scilarges, cfgs=[OBJ_INFO_DEFAULT], iter=0, \
  aids=None, clean=False, ll_val=3, cold_val=3):

  if not aids:
    aids = getAccessInfoDicts(benches=benches, cfgs=cfgs, iter=iter, \
                              clean=clean)

  oors = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not oors.has_key(bench):
      oors[bench] = {}
    if not oors[bench].has_key(cfg):
      oors[bench][cfg] = {}

    odict = oors[bench][cfg]
    adict = aids[bench][cfg]

    objs = adict[OBJECTS]
    ll_objs = [ o for o in objs.keys() \
      if (objs[o][LIVE_VALS][1] - objs[o][LIVE_VALS][0]) > ll_val ]

    cold_objs = [ o for o in ll_objs \
      if ((objs[o][LIVE_VALS][1] - objs[o][LIVE_VALS][0]) - \
          len(objs[o][HOT_VALS])) > cold_val ]

    total_nobjs = len( objs.keys() )
    total_size  = (sum( [ objs[o][SIZE] for o in objs.keys() ] ) / 1024)
    ll_nobjs    = len( ll_objs )
    ll_size     = (sum( [ objs[o][SIZE] for o in ll_objs ] ) / 1024)
    ll_span     = average( [ (objs[o][LIVE_VALS][1] - objs[o][LIVE_VALS][0]) \
                             for o in ll_objs ] )

    ll_orat     = float(ll_nobjs) / total_nobjs
    ll_srat     = float(ll_size) / total_size

    cold_nobjs  = len( cold_objs )
    cold_size   = (sum( [ objs[o][SIZE] for o in cold_objs ] ) / 1024)
    cold_span   = average( [ (objs[o][LIVE_VALS][1] - objs[o][LIVE_VALS][0]) \
                             for o in cold_objs ] )

    cold_orat   = float(cold_nobjs) / total_nobjs
    cold_srat   = float(cold_size) / total_size

    odict[TOTAL_VALS]    = "%d" % adict[TOTAL_VALS]
    odict[TOTAL_OBJECTS] = "%d" % total_nobjs
    odict[TOTAL_SIZE]    = "%d" % total_size
    odict[LL_OBJECTS]    = "%6.2f" % ll_orat
    odict[LL_SIZE]       = "%6.2f" % ll_srat
    odict[LL_SPAN]       = "%6.2f" % ll_span
    odict[COLD_OBJECTS]  = "%6.2f" % cold_orat
    odict[COLD_SIZE]     = "%6.2f" % cold_srat
    odict[COLD_SPAN]     = "%6.2f" % cold_span
  return oors

def oldObjectReport(benches=scilarges, cfgs=[OBJ_INFO_INTERVAL], iter=0, \
  aids=None, clean=False, ll_val=3, cold_val=3, outf=sys.stdout):

  oors = getOldObjectReportStrs(benches=benches, cfgs=cfgs, iter=iter, \
                                aids=aids, clean=clean, ll_val=ll_val, \
                                cold_val=cold_val)

  print >> outf, "Old Object Report\n"

  print >> outf, "".ljust(2),
  print >> outf, "cfg".ljust(20),
  print >> outf, "bench".ljust(30),
  print >> outf, "vals".rjust(8),

  print >> outf, "total objs".rjust(13),
  print >> outf, "total size".rjust(13),

  print >> outf, "ll objs".rjust(13),
  print >> outf, "ll size".rjust(13),
  print >> outf, "ll span".rjust(13),

  print >> outf, "cold objs".rjust(13),
  print >> outf, "cold size".rjust(13),
  print >> outf, "cold span".rjust(13),

  print >> outf, ""

  allavgs = {}
  for cfg in cfgs:
    cfgavgs = {}
    for bench in benches:

      oor = oors[bench][cfg]
      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, ("%s"%bench).ljust(30),
      print >> outf, ("%s"%oor[TOTAL_VALS]).rjust(8),
      print >> outf, ("%s"%oor[TOTAL_OBJECTS]).rjust(13),
      print >> outf, ("%s"%oor[TOTAL_SIZE]).rjust(13),
      print >> outf, ("%s"%oor[LL_OBJECTS]).rjust(13),
      print >> outf, ("%s"%oor[LL_SIZE]).rjust(13),
      print >> outf, ("%s"%oor[LL_SPAN]).rjust(13),
      print >> outf, ("%s"%oor[COLD_OBJECTS]).rjust(13),
      print >> outf, ("%s"%oor[COLD_SIZE]).rjust(13),
      print >> outf, ("%s"%oor[COLD_SPAN]).rjust(13),
      print >> outf, ""

def pcmPowerInfoReport(bench, cfg, iter, node, extra=False, quiet=False):

  prf = try_open_write(pcmPowerReportFile(bench, cfg, iter))
  if quiet:
    outf = prf
  else:
    outf = TeeFile(sys.stdout,prf)

  pcmvals = pcmPowerInfoVals(bench, cfg, iter, node)

  # print the header
  pcmf = try_open_read(pcmPowerFile(bench, cfg, iter))
  for line in pcmf:
    if sampleRateRE.match(line):
      sample_rate = int(line.split()[2])
      break
  pcmf.close()

  print >> outf, "PCM Power for Node: %d (update every %d seconds)\n" % (node, sample_rate)

  chans = range(0,4)
  ranks = range(0,2)
  print >> outf, "".ljust(2),
  print >> outf, "val".ljust(5),
  print >> outf, "elapsed (ms)".ljust(12),
  for chan in chans:
    for rank in ranks:
      print >> outf, ("CH%dR%d" % (chan, rank)).ljust(9),

  print >> outf, "DRAMW".ljust(9),
  if extra:
    print >> outf, "DRAMJ".ljust(9),
    print >> outf, "SKW".ljust(9),
  print >> outf, ""

  for i,val in enumerate(pcmvals):

    print >> outf, "".ljust(2),
    print >> outf, ("%d" % i).ljust(5),
    print >> outf, ("%d" % val[TIME_ELAPSED]).ljust(12),

    for chan in chans:
      for rank in ranks:
        print >> outf, ("%5.2f" % val[CKEOFF][chan][rank]).ljust(9),

    print >> outf, ("%5.2f" % val[DRAM_WATTS]).ljust(9),
    if extra:
      print >> outf, ("%5.2f" % val[DRAM_JOULES]).ljust(9),
      print >> outf, ("%5.2f" % val[SOCK_WATTS]).ljust(9),
    print >> outf, ""
 
def pcmMemoryInfoReport(bench, cfg, iter, node, extra=False, quiet=False):

  prf = try_open_write(pcmMemoryReportFile(bench, cfg, iter))
  if quiet:
    outf = prf
  else:
    outf = TeeFile(sys.stdout,prf)

  pcmvals = pcmMemoryInfoVals(bench, cfg, iter, node)

  # print the header
  pcmf = try_open_read(pcmMemoryFile(bench, cfg, iter))
  for line in pcmf:
    if sampleRateRE.match(line):
      sample_rate = int(line.split()[2])
      break
  pcmf.close()

  print >> outf, "PCM Memory for Node: %d (update every %d seconds)\n" % (node, sample_rate)

  chans = range(0,4)
  print >> outf, "".ljust(2),
  print >> outf, "val".ljust(5),
  print >> outf, "elapsed (ms)".ljust(12),
  for chan in chans:
    print >> outf, ("CH%dRD" % chan).rjust(12),
    print >> outf, ("CH%dWR" % chan).rjust(12),

  print >> outf, "NODERD".rjust(12),
  print >> outf, "NODEWR".rjust(12),
  print >> outf, "NODEMEM".rjust(12),
  if extra:
    print >> outf, "SYSRD".rjust(12),
    print >> outf, "SYSWR".rjust(12),
    print >> outf, "SYSMEM".rjust(12),
  print >> outf, ""

  for i,val in enumerate(pcmvals):

    print >> outf, "".ljust(2),
    print >> outf, ("%d" % i).ljust(5),
    print >> outf, ("%d" % val[TIME_ELAPSED]).ljust(12),

    for chan in chans:
      print >> outf, ("%10.2f" % val[CHAN_READ][chan]).rjust(12),
      print >> outf, ("%10.2f" % val[CHAN_WRITE][chan]).rjust(12),

    print >> outf, ("%10.2f" % val[NODE_READ]).rjust(12),
    print >> outf, ("%10.2f" % val[NODE_WRITE]).rjust(12),
    print >> outf, ("%10.2f" % val[NODE_MEMORY]).rjust(12),

    if extra:
      print >> outf, ("%10.2f" % val[SYSTEM_READ]).rjust(12),
      print >> outf, ("%10.2f" % val[SYSTEM_WRITE]).rjust(12),
      print >> outf, ("%10.2f" % val[SYSTEM_MEMORY]).rjust(12),
    print >> outf, ""

def pcmInfoReport(bench, cfg, iter, node, extra=False, quiet=False):

  prf = try_open_write(pcmReportFile(bench, cfg, iter))
  if quiet:
    outf = prf
  else:
    outf = TeeFile(sys.stdout,prf)

  pcmvals = pcmInfoVals(bench, cfg, iter, node)

  print >> outf, "PCM Report for Node: %d\n" % (node)

  chans = range(0,4)
  print >> outf, "".ljust(2),
  print >> outf, "val".ljust(5),
  print >> outf, "IPC".rjust(9),
  print >> outf, "L3_MISS (KB)".rjust(14),
  print >> outf, "L2_MISS (KB)".rjust(14),
  print >> outf, "L3_HIT".rjust(9),
  print >> outf, "L2_HIT".rjust(9),
  print >> outf, "L3_CLK".rjust(9),
  print >> outf, "L2_CLK".rjust(9),
  print >> outf, "READ".rjust(9),
  print >> outf, "WRITE".rjust(9),
  print >> outf, "SOCKJ".rjust(9),
  print >> outf, "DRAMJ".rjust(9),
  print >> outf, ""

  for i,val in enumerate(pcmvals):

    print >> outf, "".ljust(2),
    print >> outf, ("%d" % i).ljust(5),

    print >> outf, ("%8.2f" % val[IPC]).rjust(9),
    print >> outf, ("%d" % val[L3_MISS]).rjust(14),
    print >> outf, ("%d" % val[L2_MISS]).rjust(14),
    print >> outf, ("%8.2f" % val[L3_HIT]).rjust(9),
    print >> outf, ("%8.2f" % val[L2_HIT]).rjust(9),
    print >> outf, ("%8.2f" % val[L3_CLK]).rjust(9),
    print >> outf, ("%8.2f" % val[L2_CLK]).rjust(9),
    print >> outf, ("%8.2f" % val[READ]).rjust(9),
    print >> outf, ("%8.2f" % val[WRITE]).rjust(9),
    print >> outf, ("%8.2f" % val[SOCK_JOULES]).rjust(9),
    print >> outf, ("%8.2f" % val[DRAM_JOULES]).rjust(9),
    print >> outf, ""


def allocPointInfoReport(bench, cfg, iter=0, apdict=None, quiet=False,
  sortkey=SIZE, cutoff=None, shorten=True, colored_refs=False, do_kaps=False,
  refcut=0.10, full=False, style=REFS, extra=False):

  if not apdict:
    apdict = allocPointDict(bench, cfg, iter)

  nvals = (getEndval(bench, cfg, iter, quiet=True) + 1)
  try:
    if sortkey==REFS_PER_VAL:
      apkeys = [ x[0] for x in \
                 sorted( [ (key, (apdict[key][REFS] / nvals)) \
                            for key in apdict.keys() ], \
                         key=itemgetter(1), reverse=True ) ]
    else:
      apkeys = [ x[0] for x in \
               sorted( [ (key,apdict[key][sortkey]) for key in apdict.keys() ], \
                         key=itemgetter(1), reverse=True ) ]
    if cutoff:
      apkeys = apkeys[:cutoff]
  except KeyError:
    print "bad sort key: %s" % sortkey
    raise

  if do_kaps:
    if style == REFS:
      kaps = knapsackRefAllocPoints(bench, cfg, cutoff=refcut)
    elif style == SIZE:
      kaps = knapsackSizeAllocPoints(bench, cfg, cutoff=refcut)
    elif style == REFS_PER_VAL:
      kaps = refsPerValAllocPoints(bench, cfg, cutoff=refcut)
    else:
      raise SystemExit(1)
    for apkey in apdict.keys():
      if apkey in kaps:
        apdict[apkey][KAP] = True
      else:
        apdict[apkey][KAP] = False

  all_size = sum ( [ apdict[key][SIZE] for key in apdict.keys() ] )
  all_refs = sum ( [ apdict[key][REFS] for key in apdict.keys() ] )

  apif = try_open_write(apiReportFile(bench, cfg, iter))
  if quiet:
    outf = apif
  else:
    outf = TeeFile(sys.stdout,apif)

  print >> outf, "".ljust(2),
  print >> outf, "rank".ljust(5),
  if shorten:
    print >> outf, "allocation point".ljust(33),
    print >> outf, "klass".ljust(33),
  else:
    print >> outf, "allocation point".ljust(60),
    print >> outf, "klass".ljust(60),
  print >> outf, "size (KB)".rjust(12),
  print >> outf, "size %%".rjust(12),
  print >> outf, "objects".rjust(12),
  if colored_refs:
    print >> outf, "red refs".rjust(12),
    print >> outf, "blue refs".rjust(12),
  print >> outf, "total refs".rjust(12),
  print >> outf, "ref %%".rjust(12),
  if extra:
    print >> outf, "refs/val".rjust(12),
    print >> outf, "life/obj".rjust(12),
  if do_kaps:
    print >> outf, "KAP?".rjust(12),
  print >> outf, ""

  for i,apkey in enumerate(apkeys):
    apinfo     = apdict[apkey]

    if full:
      print apinfo[ALLOC_POINT]

    method     = apinfo[ALLOC_POINT].split()[0].split('/')[-1]
    bci        = apinfo[ALLOC_POINT].split()[2]

    kbsize     = apinfo[SIZE] / 1024
    size_pct   = float(apinfo[SIZE]) / float(all_size)
    if colored_refs:
      red_refs   = apinfo[RED_REFS] 
      blue_refs  = apinfo[BLUE_REFS] 
    total_refs   = apinfo[REFS] 
    ref_pct      = float(total_refs) / float(all_refs)
    #refs_per_val = float(total_refs) / apinfo[LIVE_VALS]

    #start,end   = apinfo[ACTIVE_VALS]
    #active_span = (end-start)+1

    refs_per_val = (apinfo[REFS] / nvals)
    vals_per_obj = float(apinfo[LIVE_VALS]) / apinfo[OBJECTS]

    print >> outf, "".ljust(2),
    print >> outf, ("%d" % i).ljust(5),
    if shorten:
      shap = ("%s, %s" % (method, bci))[:30]
      #shk  = ("%s" % apinfo[KLASS])[:30]
      klasses = list(apinfo[KLASS])
      if len(klasses) > 1:
        shk  = ("%d klasses [" % len(klasses))
        for klass in klasses[:-1]:
          shk += klass
          shk += ", "
        shk += klasses[-1]
        shk = shk[:30]
      else:
        shk  = ("%s" % klasses[0])[:30]

      print >> outf, ("%s" % shap).ljust(33),
      print >> outf, ("%s" % shk).ljust(33),
    else:
      print >> outf, ("%s, %s" % (method, bci)).ljust(60),

      klasses = list(apinfo[KLASS])
      if len(klasses) > 1:
        shk  = ("%d klasses [" % len(klasses))
        for klass in klasses[:-1]:
          shk += klass
          shk += ", "
        shk += klasses[-1]
        shk = shk[:57]
      else:
        shk  = ("%s" % klasses[0])[:57]
      print >> outf, ("%s" % shk).ljust(60),
      #print >> outf, ("%s" % apinfo[KLASS]).ljust(60),
    print >> outf, ("%d" % kbsize).rjust(12),
    print >> outf, ("%5.4f" % size_pct).rjust(12),
    print >> outf, ("%d" % apinfo[OBJECTS]).rjust(12),
    if colored_refs:
      print >> outf, ("%d" % red_refs).rjust(12),
      print >> outf, ("%d" % blue_refs).rjust(12),
    print >> outf, ("%d" % total_refs).rjust(12),
    print >> outf, ("%5.4f" % ref_pct).rjust(12),
    if extra:
      print >> outf, ("%d" % refs_per_val).rjust(12),
      print >> outf, ("%9.3f" % vals_per_obj).rjust(12),
    if do_kaps:
      print >> outf, ("%s" % ('-' if apinfo[KAP] else '+')).rjust(12),
    print >> outf, ""

def allocPointSpaceReport(apsinfo=None, benches=dacapo_defs, cfgs=[OBJ_INFO_GUIDED], \
  iter=0, color=BLUE, style=REFS, cutoff=0.10, outf=sys.stdout):

  if not apsinfo:
    apsinfo = allocPointSpaceInfo(benches=benches, cfgs=cfgs, iter=iter, \
                                  style=style, cutoff=cutoff)

  print >> outf, "Allocation Point Space Report for %s objects\n" % color

  print >> outf, "".ljust(2),
  print >> outf, "cfg".ljust(20),
  print >> outf, "bench".ljust(30),
  print >> outf, "size (KB)".rjust(12),
  print >> outf, "size %%".rjust(12),
  print >> outf, "objects".rjust(12),
  print >> outf, "objects %%".rjust(12),
  print >> outf, "refs".rjust(12),
  print >> outf, "refs %%".rjust(12),
  print >> outf, ""

  #allavgs = {}
  for cfg in cfgs:
    #cfgavgs = {}
    for bench in benches:

      bcinfo = apsinfo[bench][cfg]

      total_size    = bcinfo[RED][SIZE]    + bcinfo[BLUE][SIZE]
      total_objects = bcinfo[RED][OBJECTS] + bcinfo[BLUE][OBJECTS]
      total_refs    = bcinfo[RED][REFS]    + bcinfo[BLUE][REFS]

      color_size    = bcinfo[color][SIZE]
      color_objects = bcinfo[color][OBJECTS]
      color_refs    = bcinfo[color][REFS]

      size_rat    = float(color_size)    / total_size
      objects_rat = float(color_objects) / total_objects
      refs_rat    = float(color_refs)    / total_refs

      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, ("%s"%bench).ljust(30),

      print >> outf, ("%d"    % total_size).rjust(12),
      print >> outf, ("%5.4f" % size_rat).rjust(12),
      print >> outf, ("%d"    % total_objects).rjust(12),
      print >> outf, ("%5.4f" % objects_rat).rjust(12),
      print >> outf, ("%d"    % total_refs).rjust(12),
      print >> outf, ("%5.4f" % refs_rat).rjust(12),
      print >> outf, ""


def allocPointCoverReport(benches=jvm2008s, cfgs=[OBJ_INFO_DEFAULT], iter=0,
  apcov=None, cutkey=REFS, compkey=SIZE, outf=sys.stdout):

  if not apcov:
    apcov = allocPointCoverInfo(benches=benches, cfgs=cfgs, iter=iter)

  print >> outf, "Allocation Point Cover Report\n"

  print >> outf, "".ljust(2),
  print >> outf, "cfg".ljust(20),
  print >> outf, "bench".ljust(30),
  for rat in coverrats[cutkey]:
    print >> outf, ("%4.1f" % (100.0*rat)).rjust(18),
  print >> outf, ""
  print >> outf, "".ljust(54),
  for rat in range(len(coverrats[cutkey])):
    print >> outf, "(".rjust(3), ("%s,"%cutkey).rjust(7), ("%s)"%compkey).rjust(6),
    #print >> outf, ("(%s, %s)" % (cutkey,compkey)).rjust(16),
  print >> outf, ""
  
  allavgs = {}
  for cfg in cfgs:
    cfgavgs = {}
    for bench in benches:

      if not apcov.has_key(bench):
        continue
      if not apcov[bench].has_key(cfg):
        continue

      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, ("%s"%bench).ljust(30),

      covrec = apcov[bench][cfg][cutkey]
      for rat in coverrats[cutkey]:
        cutval  = 100.0 * covrec[rat][cutkey][2]
        compval = 100.0 * covrec[rat][compkey][2]

        if not cfgavgs.has_key(rat):
          cfgavgs[rat] = [] 
        if not allavgs.has_key(rat):
          allavgs[rat] = [] 
        cfgavgs[rat].append((cutval, compval))
        allavgs[rat].append((cutval, compval))

        valstr  = "%6.3f  %6.3f" % (cutval, compval)
        print >> outf, ("%s" % valstr).rjust(18),
      print >> outf, "" 
    if cfgavgs:
      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, "*".ljust(30),
      for rat in coverrats[cutkey]:
        cutavg  = geomean( [ x[0] for x in cfgavgs[rat] ] )
        compavg = geomean( [ x[1] for x in cfgavgs[rat] ] )
        valstr  = "%6.3f  %6.3f" % (cutavg, compavg)
        print >> outf, ("%s" % valstr).rjust(18),
      print >> outf, "" 

  print >> outf, "".ljust(2),
  print >> outf, "*".ljust(20),
  print >> outf, "*".ljust(30),
  for rat in coverrats[cutkey]:
    cutavg  = geomean( [ x[0] for x in allavgs[rat] ] )
    compavg = geomean( [ x[1] for x in allavgs[rat] ] )
    valstr  = "%6.3f  %6.3f" % (cutavg, compavg)
    print >> outf, ("%s" % valstr).rjust(18),
  print >> outf, "" 

def gcSpaceReport(bench, cfg, iter=0, res=None, pos=HEAP_BEFORE, \
                  used=True, colored=True, ceden=True, quiet=False):

  if not res:
    res = getGCSpaceInfo(bench, cfg, iter, colored=colored, ceden=ceden)

  csgcrf = try_open_write(csgcReportFile(bench, cfg, iter))
  if quiet:
    outf = csgcrf
  else:
    outf = TeeFile(sys.stdout,csgcrf)

  UC = 0
  if not used:
    UC = 1

  print >> outf, "GC Space Report"

  print >> outf, "".ljust(2),
  print >> outf, "val".ljust(5),
  print >> outf, "time (MS)".ljust(9),
  if colored and ceden:
    print >> outf, "eden-red".rjust(13),
    print >> outf, "eden-blue".rjust(13),
  else:
    print >> outf, "eden".rjust(13),
  if colored:
    print >> outf, "surv-red".rjust(13),
    print >> outf, "surv-blue".rjust(13),
  else:
    print >> outf, "survivor".rjust(13),
  if colored:
    print >> outf, "ten-red".rjust(13),
    print >> outf, "ten-blue".rjust(13),
  else:
    print >> outf, "tenured".rjust(13),
  if colored:
    print >> outf, "red-rat".rjust(8),
    print >> outf, "blue-rat".rjust(8),
  else:
    print >> outf, "surv-rat".rjust(13),
    print >> outf, "new-rat".rjust(13),
  print >> outf, ""

  for i,val in enumerate(res):
    rec = val[pos]

    print >> outf, "".ljust(2),
    print >> outf, ("%d" % i).ljust(5),
    print >> outf, ("%d" % rec[REL_TIME]).ljust(9),

    if colored and ceden:
      eden_red  = rec[EDEN][RED][UC]
      eden_blue = rec[EDEN][BLUE][UC]
    else:
      eden = rec[EDEN][UC]

    if colored:
      surv_red  = rec[SURVIVOR][RED][UC]
      surv_blue = rec[SURVIVOR][BLUE][UC]
      ten_red   = rec[TENURED][RED][UC]
      ten_blue  = rec[TENURED][BLUE][UC]

      if ceden:
        red_tot   = eden_red  + surv_red  + ten_red
        blue_tot  = eden_blue + surv_blue + ten_blue
        total     = red_tot + blue_tot
      else:
        red_tot   = eden + surv_red + ten_red
        blue_tot  = surv_blue + ten_blue
        total     = red_tot + blue_tot

      red_rat   = float(red_tot)  / float(total)
      blue_rat  = float(blue_tot) / float(total)
    else:
      survivor = rec[SURVIVOR][UC]
      tenured  = rec[TENURED][UC]

      if not used:
        surv_rat = float(eden)/(float(survivor)/2)
        new_rat  = float(eden+survivor)/tenured

    if colored and ceden:
      print >> outf, ("%d" % eden_red).rjust(13),
      print >> outf, ("%d" % eden_blue).rjust(13),
    else:
      print >> outf, ("%d" % eden).rjust(13),
    if colored:
      print >> outf, ("%d" % surv_red).rjust(13),
      print >> outf, ("%d" % surv_blue).rjust(13),
    else:
      print >> outf, ("%d" % survivor).rjust(13),
    if colored:
      print >> outf, ("%d" % ten_red).rjust(13),
      print >> outf, ("%d" % ten_blue).rjust(13),
    else:
      print >> outf, ("%d" % tenured).rjust(13),
    if colored:
      print >> outf, ("%2.4f" % red_rat).rjust(8),
      print >> outf, ("%2.4f" % blue_rat).rjust(8),
    else:
      if not used:
        print >> outf, ("%2.4f" % surv_rat).rjust(13),
        print >> outf, ("%2.4f" % new_rat).rjust(13),
    print >> outf, ""


def allocPointKnapsackReport(benches=jvm2008s, cfgs=[OBJ_INFO_DEFAULT], iter=0,
  apds=None, kaps=None, style=REFS, cutoff=0.10, outf=sys.stdout):

  if not apds:
    apds = getAllocPointDicts(benches=benches, cfgs=cfgs, iter=iter)
  if not kaps:
    kaps = getKAPs(benches=benches, cfgs=cfgs, iter=iter, style=style, \
                   cutoff=cutoff)

  print >> outf, "Allocation Point Knapsack Report (style=%s, cutoff=%4.3f)\n" % \
                  (style, cutoff)

  print >> outf, "".ljust(2),
  print >> outf, "cfg".ljust(20),
  print >> outf, "bench".ljust(30),
  print >> outf, "refs".rjust(13),
  print >> outf, "kap refs".rjust(13),
  print >> outf, "size".rjust(13),
  print >> outf, "kap size".rjust(13),
  print >> outf, "refs/val".rjust(13),
  print >> outf, "kap refs/val".rjust(13),
  print >> outf, ""
  
  allavgs = []
  for cfg in cfgs:
    cfgavgs = []
    for bench in benches:

      if not apds.has_key(bench) or not kaps.has_key(bench):
        continue
      if not apds[bench].has_key(cfg) or not kaps[bench].has_key(cfg):
        continue

      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, ("%s"%bench).ljust(30),

      apd   = apds[bench][cfg]
      kap   = kaps[bench][cfg]
      nvals = (getEndval(bench, cfg, 0, quiet=True) + 1)

      total_refs = sum ( [ apd[x][REFS] for x in apd.keys() ] )
      kap_refs   = sum ( [ apd[x][REFS] for x in apd.keys() if x in kap ] )
      ref_rat    = float(kap_refs) / total_refs

      total_size = sum ( [ apd[x][SIZE] for x in apd.keys() ] )
      kap_size   = sum ( [ apd[x][SIZE] for x in apd.keys() if x in kap ] )
      size_rat   = float(kap_size) / total_size

      total_rpv  = sum ([ apd[x][REFS] / nvals for x in apd.keys() ])
      kap_rpv    = sum ([ apd[x][REFS] / nvals for x in apd.keys() if x in kap ])

      print >> outf, ("%d"%total_refs).rjust(13),
      print >> outf, ("%6.6f"%ref_rat).rjust(13),
      print >> outf, ("%d"%(total_size/1024)).rjust(13),
      print >> outf, ("%6.6f"%size_rat).rjust(13),
      print >> outf, ("%d"%total_rpv).rjust(13),
      print >> outf, ("%d"%kap_rpv).rjust(13),
      print >> outf, ""

      cfgavgs.append((total_refs,ref_rat,total_size,size_rat, kap_rpv, total_rpv))
      allavgs.append((total_refs,ref_rat,total_size,size_rat, kap_rpv, total_rpv))

    if cfgavgs:
      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, "*".ljust(30),

      total_refs = average( [ x[0] for x in cfgavgs ] )
      ref_rat    = average( [ x[1] for x in cfgavgs ] )
      total_size = average( [ x[2] for x in cfgavgs ] )
      size_rat   = average( [ x[3] for x in cfgavgs ] )
      kap_rpv    = average( [ x[4] for x in cfgavgs ] )
      total_rpv  = average( [ x[5] for x in cfgavgs ] )

      print >> outf, ("%d"%total_refs).rjust(13),
      print >> outf, ("%6.6f"%ref_rat).rjust(13),
      print >> outf, ("%d"%(total_size/1024)).rjust(13),
      print >> outf, ("%6.6f"%size_rat).rjust(13),
      print >> outf, ("%d"%total_rpv).rjust(13),
      print >> outf, ("%d"%kap_rpv).rjust(13),
      print >> outf, ""

  print >> outf, "".ljust(2),
  print >> outf, "*".ljust(20),
  print >> outf, "*".ljust(30),

  total_refs = average( [ x[0] for x in allavgs ] )
  ref_rat    = average( [ x[1] for x in allavgs ] )
  total_size = average( [ x[2] for x in allavgs ] )
  size_rat   = average( [ x[3] for x in allavgs ] )
  kap_rpv    = average( [ x[4] for x in allavgs ] )
  total_rpv  = average( [ x[5] for x in allavgs ] )

  print >> outf, ("%d"%total_refs).rjust(13),
  print >> outf, ("%6.6f"%ref_rat).rjust(13),
  print >> outf, ("%d"%(total_size/1024)).rjust(13),
  print >> outf, ("%6.6f"%size_rat).rjust(13),
  print >> outf, ("%d"%total_rpv).rjust(13),
  print >> outf, ("%d"%kap_rpv).rjust(13),
  print >> outf, ""

def printAndAppendDRStat(stat, avgrec, fmt, length, outf=sys.stdout):
  if stat == None:
    print >> outf, ("N/A").rjust(length),
  else:
    print >> outf, (fmt % stat).rjust(length),
  avgrec.append(stat)

def filterInfoVals(vals, start, end):

  fvals  = []
  curtime = 0
  for elapsed,val in vals:
    curtime += elapsed

    scale = 1.0
    if curtime > start:
      if (curtime - start) < elapsed:
        scale = (float(curtime - start) / elapsed)
      if curtime < end:
        val[SCALE] = scale
        fvals.append(val)
      else:
        val_start = curtime - elapsed
        if val_start < end:
          scale = (float(end - val_start) / elapsed)
          val[SCALE] = scale
          fvals.append(val)

  return fvals

def getDimmReportInfo(bench, cfg, iters, miniters, metrics, dimms, node, \
  per_second=True):

  drinfo = {}
  utimes = []
  for i in range(iters):
    utime = getUnixTime(bench, cfg, i, rawtype=PCM_SELF_REFRESH)[0]
    if utime:
      utimes.append((utime, i))

  my_iters = [ x[1] for x in utimes ]

  bwvals  = []
  srvals  = []
  bwtimes = []
  srtimes = []
  for i in my_iters:

    bwvals_i = []
    bws      = pcmMemoryInfoVals(bench, cfg, i, node)
    for val in bws:
      bwvals_i.append((val[TIME_ELAPSED],val))

    tvs   = None
    filter_start = filter_end = start = end = None
    if bench in membenches:
      tvs = getMemBenchTimeVals(bench, cfg, i, rawtype=PCM_MEMORY)
    elif bench in dacapos:
      tvs = getDacapoBenchTimeVals(bench, cfg, i, rawtype=PCM_MEMORY)
    elif bench in jvm2008s:
      tvs = getjvm2008BenchTimeVals(bench, cfg, i, rawtype=PCM_MEMORY)

    if bench in membenches:
      if tvs:
        filter_start = tvs[MB_THREAD_START] - tvs[MB_START]
        filter_end   = tvs[MB_END]          - tvs[MB_START]
        start = filter_start
        end   = filter_end
    else:
      if tvs:
        pcm_start = pcmStart(bench, cfg, i, type=PCM_MEMORY)
        if not pcm_start:
          pcm_end   = tvs[BENCH_END]
          pcm_start = pcm_end - sum([x[0] for x in bwvals_i])
        filter_start = tvs[BENCH_START] - pcm_start
        filter_end   = tvs[BENCH_END]   - pcm_start
        start        = tvs[BENCH_START] - tvs[VM_START]
        end          = tvs[BENCH_END]   - tvs[VM_START]

    bwtimes.append( ( (float(end-start)) / 1000 ) )

    #bwvals_i = [ x[1] for x in bwvals_i if x[0] > start and x[0] < end ]
    bwvals_i = filterInfoVals(bwvals_i, filter_start, filter_end)
    bwvals.append( bwvals_i )
#    print ""
#    for bval in bwvals_i:
#      for dimm in dimms:
#        print "  dimm: %d read: %8.2f write: %8.2f scale: %3.2f" % \
#          (dimm, bval[CHAN_READ][dimm], bval[CHAN_WRITE][dimm], bval[SCALE])
#      print "  tots:   read: %8.2f write: %8.2f" % \
#        ( sum( [ bval[CHAN_READ][x]  for x in dimms ] ),
#          sum( [ bval[CHAN_WRITE][x] for x in dimms ] ) )
#      print ""

    srvals_i = []
    srs      = pcmSRInfoVals(bench, cfg, i, node)
    for val in srs:
      srvals_i.append((val[TIME_ELAPSED],val))

    tvs   = None
    filter_start = filter_end = start = end = None
    if bench in membenches:
      tvs = getMemBenchTimeVals(bench, cfg, i, rawtype=PCM_SELF_REFRESH)
    elif bench in dacapos:
      tvs = getDacapoBenchTimeVals(bench, cfg, i, rawtype=PCM_SELF_REFRESH)
    elif bench in jvm2008s:
      tvs = getjvm2008BenchTimeVals(bench, cfg, i, rawtype=PCM_SELF_REFRESH)

    if bench in membenches:
      if tvs:
        filter_start = tvs[MB_THREAD_START] - tvs[MB_START]
        filter_end   = tvs[MB_END]          - tvs[MB_START]
        start = filter_start
        end   = filter_end
    else:
      if tvs:
        pcm_start = pcmStart(bench, cfg, i, type=PCM_SELF_REFRESH)
        if not pcm_start:
          pcm_end   = tvs[BENCH_END]
          pcm_start = pcm_end - sum([x[0] for x in srvals_i])
        filter_start = tvs[BENCH_START] - pcm_start
        filter_end   = tvs[BENCH_END]   - pcm_start
        start        = tvs[BENCH_START] - tvs[VM_START]
        end          = tvs[BENCH_END]   - tvs[VM_START]

    srtimes.append( ( (float(end-start)) / 1000 ) )
    srvals_i   = filterInfoVals(srvals_i, filter_start, filter_end)
    srvals.append( srvals_i )

  drinfo[NODE]  = {}
  drinfo[DIMMS] = []
  for dimm in dimms:

    drec = {}
    for met in metrics:
      drec[met] = []

    for i,ix in enumerate(my_iters):

      #return bwvals
      # calculate the read/write operations per sample
      rd_samples = [ (val[CHAN_READ][dimm] * (val[SCALE] * (float(val[TIME_ELAPSED])/1000))) \
                     for val in bwvals[i] ]
#      rd_samps,outs = rmoutliers(rd_samples)
#      for out in outs:
#        print "%s-%s-i%d: removed rd_samples outlier: %5.2f" \
#          % (bench, cfg, iter, out)
#        rd_samps.append(average(rd_samples))

      wr_samples = [ (val[CHAN_WRITE][dimm] * (val[SCALE] * (float(val[TIME_ELAPSED])/1000))) \
                     for val in bwvals[i] ]
#      wr_samps,outs = rmoutliers(wr_samples)
#      for out in outs:
#        print "%s-%s-i%d: removed wr_samples outlier: %5.2f" \
#          % (bench, cfg, iter, out)
#        wr_samps.append(average(wr_samples))

#      if dimm == 2:
#        for val in bwvals[i]:
#          print "scale: %3.f elapsed: %d" % (val[SCALE], val[TIME_ELAPSED])
#          for rval,samp in zip(bwvals[i], rd_samples):
#            print "  val: %8.2f rd_sample: %8.2f" % (rval[CHAN_READ][dimm], samp)

      rd_gb     = (float(sum(rd_samples))/1024)
      wr_gb     = (float(sum(wr_samples))/1024)
      mem_gb    = rd_gb + wr_gb

      rd_joules = rd_gb * (RD_JOULES*OPS_PER_GB)
      wr_joules = wr_gb * (WR_JOULES*OPS_PER_GB)
      op_joules = rd_joules + wr_joules

      # calculate the time spent in each bg mode (in seconds)
      sr_samples = [ ( (float(val[SR_CYCLES][dimm]) / val[DRAM_CLOCKS][dimm]) \
                       * (float(val[TIME_ELAPSED])/1000) * val[SCALE]) for val in srvals[i] ]
#      sr_samps,outs = rmoutliers(sr_samples)
#      for out in outs:
#        print "%s-%s-i%d: removed sr_samples outlier: %5.2f" \
#          % (bench, cfg, iter, out)
#        sr_samps.append(average(sr_samples))

      ppd_samples = [ ( (float(val[PPD_CYCLES][dimm]) / val[DRAM_CLOCKS][dimm]) \
                       * (float(val[TIME_ELAPSED])/1000) * val[SCALE]) for val in srvals[i] ]
#      ppd_samps,outs = rmoutliers(ppd_samples)
#      for out in outs:
#        print "%s-%s-i%d: removed ppd_samples outlier: %5.2f" \
#          % (bench, cfg, iter, out)
#        ppd_samps.append(average(ppd_samples))

      ps_samples = [ ( (float(val[PS_CYCLES][dimm]) / val[DRAM_CLOCKS][dimm]) \
                       * (float(val[TIME_ELAPSED])/1000) * val[SCALE]) for val in srvals[i] ]
#      ps_samps,outs = rmoutliers(ps_samples)
#      for out in outs:
#        print "%s-%s-i%d: removed ps_samples outlier: %5.2f" \
#          % (bench, cfg, iter, out)
#        ps_samps.append(average(ps_samples))

      sr_joules  = (SR_WATTS  * sum( sr_samples  ))
      ppd_joules = (PPD_WATTS * sum( ppd_samples ))
      ps_joules  = (PS_WATTS  * sum( ps_samples  ))

      bg_joules  = (sr_joules + ppd_joules + ps_joules)

      iter_joules = op_joules + bg_joules
      iter_rds    = rd_gb
      iter_wrs    = wr_gb
      iter_gbs    = mem_gb

      if per_second: 
        iter_joules /= srtimes[ix]
        iter_rds    /= srtimes[ix]
        iter_wrs    /= srtimes[ix]
        iter_gbs    /= srtimes[ix]

      drec[MEM_READ_BW].append(iter_rds)
      drec[MEM_WRITE_BW].append(iter_wrs)
      drec[MEM_TOTAL_BW].append(iter_gbs)
      drec[MODEL_MEM_ENERGY].append(iter_joules)

    drinfo[DIMMS].append(drec)

  if len(my_iters) >= miniters:
    drinfo[RUNTIME] = average(srtimes)
    #drinfo[RUNTIME] = average(xtimes)
  else:
    drinfo[RUNTIME] = None

  for metric in metrics:
    drinfo[NODE][metric] = []
    for i in my_iters:
      drinfo[NODE][metric].append( sum ([ drinfo[DIMMS][x][metric][i] for x in dimms ]) )

    if len(my_iters) >= miniters:
      drinfo[NODE][metric] = average(drinfo[NODE][metric])
    else:
      drinfo[NODE][metric] = None

    for d in dimms:
      if len(my_iters) >= miniters:
        drinfo[DIMMS][d][metric] = average(drinfo[DIMMS][d][metric])
      else:
        drinfo[DIMMS][d][metric] = None

  return drinfo

def dimmReport(benches=dacapo_larges, cfgs=[HDKD_ALLOC_TUNE], iters=10,
  node=0, miniters=8, metric=MODEL_MEM_ENERGY, per_second=True, outf=sys.stdout):

  metrics = [ MEM_READ_BW, MEM_WRITE_BW, MEM_TOTAL_BW, MODEL_MEM_ENERGY ]

  if not metric in metrics:
    print "invalid metric: %s" % metric
    return

  print >> outf, "%s DIMM Report\n" % metric

  dimms = range(0,4)
  print >> outf, "".ljust(2),
  print >> outf, "cfg".ljust(20),
  print >> outf, "bench".ljust(30),
  print >> outf, "runtime".rjust(13),
  for dimm in dimms:
    print >> outf, ("DIMM%d" % dimm).rjust(15),
  print >> outf, ("NODE%d" % node).rjust(15),
  print >> outf, ""

  allavgs = []
  for cfg in cfgs:
    cfgavgs = []
    for bench in benches:

      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, ("%s"%bench).ljust(30),

      avgrec = []
      drinfo = getDimmReportInfo(bench, cfg, iters, miniters, metrics, dimms, node, \
                                 per_second=per_second)

      printAndAppendDRStat(drinfo[RUNTIME], avgrec, "%8.2f", 13, outf)

      for dimm in dimms:
        printAndAppendDRStat(drinfo[DIMMS][dimm][metric], avgrec, "%10.2f", 15, outf)

      printAndAppendDRStat(drinfo[NODE][metric], avgrec, "%10.2f", 15, outf)

      cfgavgs.append(avgrec)
      allavgs.append(avgrec)
      print  >> outf, ""

    if cfgavgs:
      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, "*".ljust(30),

      cfgtimes = [ x[0] for x in cfgavgs if not x[0]==None ]
      try:
        print >> outf, ("%8.2f" % average(cfgtimes)).rjust(13),
      except:
        print >> outf, ("N/A").rjust(13),

      for d in dimms:
        idx = d+1
        cfgdimms = [ x[idx] for x in cfgavgs if not x[idx]==None ]
        try:
          print >> outf, ("%10.2f" % average(cfgdimms)).rjust(15),
        except:
          print >> outf, ("N/A").rjust(15),

      idx = len(dimms) + 1
      cfgnodes = [ x[idx] for x in cfgavgs if not x[idx]==None ]
      try:
        print >> outf, ("%10.2f" % average(cfgnodes)).rjust(15),
      except:
        print >> outf, ("N/A").rjust(15),
      print >> outf, "\n"

  print >> outf, "".ljust(2),
  print >> outf, "*".ljust(20),
  print >> outf, "*".ljust(30),

  alltimes = [ x[0] for x in allavgs if not x[0]==None ]
  try:
    print >> outf, ("%8.2f" % average(alltimes)).rjust(13),
  except:
    print >> outf, ("N/A").rjust(13),

  for d in dimms:
    idx = d+1
    alldimms = [ x[idx] for x in allavgs if not x[idx]==None ]
    try:
      print >> outf, ("%10.2f" % average(alldimms)).rjust(15),
    except:
      print >> outf, ("N/A").rjust(15),

  idx = len(dimms) + 1
  allnodes = [ x[idx] for x in allavgs if not x[idx]==None ]
  try:
    print >> outf, ("%10.2f" % average(allnodes)).rjust(15),
  except:
    print >> outf, ("N/A").rjust(15),
  print >> outf, ""

def aggCKEOffReport(benches=dacapo_larges, cfgs=[DEFAULT_ALLOCATIONS], iters=10,
  node=0, miniters=8, agg=AVERAGE, outf=sys.stdout):

  aggfuncs = {
    AVERAGE:average,
    GEOMEAN:geomean,
    MIN:min,
    MAX:max,
    MEDIAN:median
  }

  agg = agg.lower()
  if not aggfuncs.has_key(agg):
    print "invalid agg: %s" % agg
    return

  aggfunc = aggfuncs[agg]

  print >> outf, "Average CKE Off Report (agg=%s)\n" % agg

  chans = range(0,4)
  ranks = range(0,2)
  print >> outf, "".ljust(2),
  print >> outf, "cfg".ljust(20),
  print >> outf, "bench".ljust(30),
  print >> outf, "runtime".rjust(13),
  for chan in chans:
    for rank in ranks:
      print >> outf, ("CH%dR%d" % (chan, rank)).rjust(9),
  print >> outf, "DRAMW".rjust(9),
  print >> outf, "DRAMJ".rjust(9),
  print >> outf, ""

  allavgs = []
  for cfg in cfgs:
    cfgavgs = []
    for bench in benches:

      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, ("%s"%bench).ljust(30),

      avgrec = []

      tstats = getHarnessTimeStats(bench, cfg, iters=iters, \
                                   miniters=miniters, quiet=True)
      if tstats:
        print >> outf, ("%8.2f" % tstats[MEAN]).rjust(13),
        avgrec.append(tstats[MEAN])
      else:
        print >> outf, ("N/A").rjust(13),
        avgrec.append(None)

      utimes = []
      for i in range(iters):
        utime = getUnixTime(bench, cfg, i)[0]
        if utime:
          utimes.append((utime, i))

      my_iters = [ x[1] for x in utimes ]
#      my_times = [ x[0] for x in utimes ]
#
#      if len(my_iters) >= miniters:
#        utime = average(my_times)
#        print >> outf, ("%8.2f" % utime).rjust(13),
#        avgrec.append(utime)
#      else:
#        print >> outf, ("N/A").rjust(13),
#        avgrec.append(None)

      pcmvals = []
      for i in my_iters:
        pvtime = 0
        vals   = []
        tvs    = None
        pvs    = pcmPowerInfoVals(bench, cfg, i, node)
        for val in pvs:
          pvtime += val[TIME_ELAPSED]
          vals.append((pvtime,val))

        start = end = None
        if filter:
          if bench in membenches:
            tvs = getMemBenchTimeVals(bench, cfg, i)
            if tvs:
              start = tvs[MB_THREAD_START] - tvs[MB_START]
              end   = tvs[MB_END]          - tvs[MB_START]
          elif bench in dacapos:
            tvs = getDacapoBenchTimeVals(bench, cfg, i)
            if tvs:
              start = (tvs[BENCH_START] - tvs[VM_START])
              end   = (tvs[BENCH_END]   - tvs[VM_START])
          elif bench in jvm2008s:
            tvs = getjvm2008BenchTimeVals(bench, cfg, i)
            if tvs:
              start = (tvs[BENCH_START] - tvs[VM_START])
              end   = (tvs[BENCH_END]   - tvs[VM_START])

          if start:
            vals = [ x[1] for x in vals if x[0] > start and x[0] < end ]
          else:
            vals = [ x[1] for x in vals ]

        else:
          vals = [ x[1] for x in vals ]

        pcmvals.append( vals )

      for chan in chans:
        for rank in ranks:
          aggvals = []
          for i,ix in enumerate(my_iters):
            samples = [ val[CKEOFF][chan][rank] for val in pcmvals[i] ]
            aggvals.append( aggfunc ( rmoutliers(samples)[0] ) )
          if len(my_iters) >= miniters:
            ckeoff = average(aggvals)
            print >> outf, ("%5.2f" % ckeoff).rjust(9),
            avgrec.append(ckeoff)
          else:
            print >> outf, ("N/A").rjust(9),
            avgrec.append(None)

      my_aggfunc = aggfunc
      if agg == GEOMEAN:
        my_aggfunc = aggfuncs[AVERAGE]

      aggvals  = []
      memstats = getMemPowerStats(bench, cfg, iters=iters, \
                                  miniters=miniters, node=node, \
                                  filter=True, quiet=True)
      dramw = memstats[0][MEAN]
      dramj = memstats[1][MEAN]
      #print dramw, dramj
      if dramw:
        print >> outf, ("%5.2f" % dramw).rjust(9),
        avgrec.append(dramw)
      else:
        print >> outf, ("N/A").rjust(9),
        avgrec.append(None)
      if dramj:
        print  >> outf, ("%5.2f" % dramj).rjust(9),
        avgrec.append(dramj)
      else:
        print >> outf, ("N/A").rjust(9),
        avgrec.append(None)

#      for i,ix in enumerate(my_iters):
#        samples = [ val[DRAM_WATTS]  for val in pcmvals[i] ]
#        aggvals.append( my_aggfunc ( rmoutliers(samples)[0] ) )
#      if len(my_iters) >= miniters:
#        dramw = average(aggvals)
#        print  >> outf, ("%5.2f" % dramw).rjust(9),
#        avgrec.append(dramw)
#      else:
#        print >> outf, ("N/A").rjust(9),
#        avgrec.append(None)
#
#      aggvals = []
#      for i,ix in enumerate(my_iters):
#        samples = [ val[DRAM_JOULES]  for val in pcmvals[i] ]
#        aggvals.append( my_aggfunc ( rmoutliers(samples)[0] ) )
#      if len(my_iters) >= miniters:
#        dramj = average(aggvals)
#        print  >> outf, ("%5.2f" % dramj).rjust(9),
#        avgrec.append(dramj)
#      else:
#        print >> outf, ("N/A").rjust(9),
#        avgrec.append(None)

      cfgavgs.append(avgrec)
      allavgs.append(avgrec)
      print  >> outf, ""

    if cfgavgs:
      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, "*".ljust(30),

      cfgtimes = [ x[0] for x in cfgavgs if not x[0]==None ]
      try:
        print >> outf, ("%8.2f" % average(cfgtimes)).rjust(13),
      except:
        print >> outf, ("N/A").rjust(13),

      for i,chan in enumerate(chans):
        for j,rank in enumerate(ranks):
          idx = (i*len(ranks) + j) + 1
          cfgoffs = [ x[idx] for x in cfgavgs if not x[idx]==None ]
          try:
            print >> outf, ("%5.2f" % average(cfgoffs)).rjust(9),
          except:
            print >> outf, ("N/A").rjust(9),

      idx = (len(chans) * len(ranks))+1
      cfgdramws = [ x[idx]   for x in cfgavgs if not x[idx]==None ]
      try:
        print >> outf, ("%5.2f" % average(cfgdramws)).rjust(9),
      except:
        print >> outf, ("N/A").rjust(9),

      cfgdramjs = [ x[idx+1] for x in cfgavgs if not x[idx+1]==None ]
      try:
        print >> outf, ("%5.2f" % average(cfgdramjs)).rjust(9),
      except:
        print >> outf, ("N/A").rjust(9),
      print >> outf, "\n"

  print >> outf, "".ljust(2),
  print >> outf, "*".ljust(20),
  print >> outf, "*".ljust(30),

  alltimes = [ x[0] for x in allavgs if not x[0]==None ]
  try:
    print >> outf, ("%8.2f" % average(alltimes)).rjust(13),
  except:
    print >> outf, ("N/A").rjust(13),

  for i,chan in enumerate(chans):
    for j,rank in enumerate(ranks):
      idx = (i*len(ranks) + j)+1
      alloffs = [ x[idx] for x in allavgs if not x[idx]==None]
      try:
        print >> outf, ("%5.2f" % average(alloffs)).rjust(9),
      except:
        print >> outf, ("N/A").rjust(9),

  idx = (len(chans) * len(ranks))+1
  alldramws = [ x[idx] for x in allavgs if not x[idx]==None ]
  try:
    print >> outf, ("%5.2f" % average(alldramws)).rjust(9),
  except:
    print >> outf, ("N/A").rjust(9),

  alldramjs = [ x[idx+1] for x in allavgs if not x[idx+1]==None ]
  try:
    print >> outf, ("%5.2f" % average(alldramjs)).rjust(9),
  except:
    print >> outf, ("N/A").rjust(9),
  print >> outf, ""

def aggMemBWReport(benches=dacapo_larges, cfgs=[DEFAULT_ALLOCATIONS], iters=10,
  node=0, miniters=8, agg=AVERAGE, outf=sys.stdout):

  aggfuncs = {
    AVERAGE:average,
    GEOMEAN:geomean,
    MIN:min,
    MAX:max,
    MEDIAN:median
  }

  agg = agg.lower()
  if not aggfuncs.has_key(agg):
    print "invalid agg: %s" % agg
    return

  aggfunc = aggfuncs[agg]

  print >> outf, "Average CKE Off Report (agg=%s)\n" % agg

  chans = range(0,4)
  ranks = range(0,2)
  print >> outf, "".ljust(2),
  print >> outf, "cfg".ljust(20),
  print >> outf, "bench".ljust(20),
  print >> outf, "runtime".rjust(13),
  for chan in chans:
    print >> outf, ("CH%dRD" % chan).rjust(9),
    print >> outf, ("CH%dWR" % chan).rjust(9),
  print >> outf, "NODERD".rjust(12),
  print >> outf, "NODEWR".rjust(12),
  print >> outf, "NODEMEM".rjust(12),
  print >> outf, ""

  allavgs = []
  for cfg in cfgs:
    cfgavgs = []
    for bench in benches:

      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, ("%s"%bench).ljust(20),

      avgrec = []

      utimes = []
      for i in range(iters):
        utime = getUnixTime(bench, cfg, i)[0]
        if utime:
          utimes.append((utime, i))

      my_times = [ x[0] for x in utimes ]
      my_iters = [ x[1] for x in utimes ]

      if len(my_iters) >= miniters:
        utime = average(my_times)
        print >> outf, ("%8.2f" % utime).rjust(13),
        avgrec.append(utime)
      else:
        print >> outf, ("N/A").rjust(13),
        avgrec.append(None)

      pcmvals = []
      for i in my_iters:
        pvtime = 0
        vals   = []
        tvs    = None
        pvs    = pcmMemoryInfoVals(bench, cfg, i, node)
        for val in pvs:
          pvtime += val[TIME_ELAPSED]
          vals.append((pvtime,val))

        start = end = None
        if filter:
          if bench in membenches:
            tvs = getMemBenchTimeVals(bench, cfg, i)
            if tvs:
              start = tvs[MB_THREAD_START] - tvs[MB_START]
              end   = tvs[MB_END]          - tvs[MB_START]
          elif bench in dacapos:
            tvs = getDacapoBenchTimeVals(bench, cfg, i)
            if tvs:
              start = (tvs[BENCH_START] - tvs[VM_START])
              end   = (tvs[BENCH_END]   - tvs[VM_START])
          elif bench in jvm2008s:
            tvs = getjvm2008BenchTimeVals(bench, cfg, i)
            if tvs:
              start = (tvs[BENCH_START] - tvs[VM_START])
              end   = (tvs[BENCH_END]   - tvs[VM_START])

          if start:
            vals = [ x[1] for x in vals if x[0] > start and x[0] < end ]
          else:
            vals = [ x[1] for x in vals ]

        else:
          vals = [ x[1] for x in vals ]

        pcmvals.append( vals )

      for chan in chans:
        if len(my_iters) >= miniters:
          rdvals = []
          wrvals = []
          for i,ix in enumerate(my_iters):
            rds = [ val[CHAN_READ][chan]  for val in pcmvals[i] ]
            wrs = [ val[CHAN_WRITE][chan] for val in pcmvals[i] ]
            rdvals.append( aggfunc ( rds ) )
            wrvals.append( aggfunc ( wrs ) )
          chanrd = average(rdvals)
          chanwr = average(wrvals)
          print >> outf, ("%5.2f" % chanrd).rjust(9),
          print >> outf, ("%5.2f" % chanwr).rjust(9),
          avgrec.append(chanrd)
          avgrec.append(chanwr)
        else:
          print >> outf, ("N/A").rjust(9),
          print >> outf, ("N/A").rjust(9),
          avgrec.append(None)
          avgrec.append(None)

      my_aggfunc = aggfunc
      if agg == GEOMEAN:
        my_aggfunc = aggfuncs[AVERAGE]

      aggvals = []
      for i,ix in enumerate(my_iters):
        samples = [ val[NODE_READ]  for val in pcmvals[i] ]
        aggvals.append( my_aggfunc ( samples ) )
      if len(my_iters) >= miniters:
        noderd = average(aggvals)
        print  >> outf, ("%5.2f" % noderd).rjust(12),
        avgrec.append(noderd)
      else:
        print >> outf, ("N/A").rjust(12),
        avgrec.append(None)

      aggvals = []
      for i,ix in enumerate(my_iters):
        samples = [ val[NODE_WRITE]  for val in pcmvals[i] ]
        aggvals.append( my_aggfunc ( samples ) )
      if len(my_iters) >= miniters:
        nodewr = average(aggvals)
        print  >> outf, ("%5.2f" % nodewr).rjust(12),
        avgrec.append(nodewr)
      else:
        print >> outf, ("N/A").rjust(12),
        avgrec.append(None)

      aggvals = []
      for i,ix in enumerate(my_iters):
        samples = [ val[NODE_MEMORY]  for val in pcmvals[i] ]
        aggvals.append( my_aggfunc ( samples ) )
      if len(my_iters) >= miniters:
        nodemem = average(aggvals)
        print  >> outf, ("%5.2f" % nodemem).rjust(12),
        avgrec.append(nodemem)
      else:
        print >> outf, ("N/A").rjust(12),
        avgrec.append(None)

      cfgavgs.append(avgrec)
      allavgs.append(avgrec)
      print  >> outf, ""

    if cfgavgs:
      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, "*".ljust(20),

      cfgtimes = [ x[0] for x in cfgavgs if not x[0]==None ]
      try:
        print >> outf, ("%8.2f" % average(cfgtimes)).rjust(13),
      except:
        print >> outf, ("N/A").rjust(13),

      for i,chan in enumerate(chans):
        rdidx = (i*2) + 1
        wridx = (i*2) + 2
        cfgrds = [ x[rdidx] for x in cfgavgs if not x[rdidx]==None ]
        cfgwrs = [ x[wridx] for x in cfgavgs if not x[wridx]==None ]
        try:
          print >> outf, ("%5.2f" % average(cfgrds)).rjust(9),
          print >> outf, ("%5.2f" % average(cfgwrs)).rjust(9),
        except:
          print >> outf, ("N/A").rjust(9),
          print >> outf, ("N/A").rjust(9),

      idx = (len(chans) * 2)+1
      noderds = [ x[idx]   for x in cfgavgs if not x[idx]==None ]
      try:
        print >> outf, ("%5.2f" % average(noderds)).rjust(12),
      except:
        print >> outf, ("N/A").rjust(12),

      nodewrs = [ x[idx+1] for x in cfgavgs if not x[idx+1]==None ]
      try:
        print >> outf, ("%5.2f" % average(nodewrs)).rjust(12),
      except:
        print >> outf, ("N/A").rjust(12),

      nodemems = [ x[idx+2] for x in cfgavgs if not x[idx+2]==None ]
      try:
        print >> outf, ("%5.2f" % average(nodemems)).rjust(12),
      except:
        print >> outf, ("N/A").rjust(12),
      print >> outf, "\n"

  print >> outf, "".ljust(2),
  print >> outf, "*".ljust(20),
  print >> outf, "*".ljust(20),

  alltimes = [ x[0] for x in allavgs if not x[0]==None ]
  try:
    print >> outf, ("%8.2f" % average(alltimes)).rjust(13),
  except:
    print >> outf, ("N/A").rjust(13),

  for i,chan in enumerate(chans):
    rdidx = (i*2)+1
    wridx = (i*2)+1
    allrds = [ x[rdidx] for x in allavgs if not x[rdidx]==None]
    allwrs = [ x[wridx] for x in allavgs if not x[wridx]==None]
    try:
      print >> outf, ("%5.2f" % average(allrds)).rjust(9),
      print >> outf, ("%5.2f" % average(allwrs)).rjust(9),
    except:
      print >> outf, ("N/A").rjust(9),
      print >> outf, ("N/A").rjust(9),

  idx = (len(chans) * 2)+1
  noderds = [ x[idx] for x in allavgs if not x[idx]==None ]
  try:
    print >> outf, ("%5.2f" % average(noderds)).rjust(12),
  except:
    print >> outf, ("N/A").rjust(12),

  nodewrs = [ x[idx+1] for x in allavgs if not x[idx+1]==None ]
  try:
    print >> outf, ("%5.2f" % average(nodewrs)).rjust(12),
  except:
    print >> outf, ("N/A").rjust(12),

  nodemems = [ x[idx+2] for x in allavgs if not x[idx+2]==None ]
  try:
    print >> outf, ("%5.2f" % average(nodemems)).rjust(12),
  except:
    print >> outf, ("N/A").rjust(12),
  print >> outf, ""


def printACGCAvgVal(avgvals, idx, format, align, outf=sys.stdout):
  vals = [ x[idx] for x in avgvals if not x[idx]==None ]
  if vals:
    try:
      if align[0] == RIGHT_ALIGN:
        print >> outf, (format % average(vals)).rjust(align[1]),
      elif align[0] == LEFT_ALIGN:
        print >> outf, (format % average(vals)).ljust(align[1]),
      else:
        raise ValueError
    except:
      print >> outf, (format % average(vals))
  else:
    print >> outf, ("N/A").align[0](align[1]),

def aggColoredSpaceGCReport(benches=dacapo_larges, cfgs=[DEFAULT_ALLOCATIONS],
  iters=10, miniters=8, agg=AVERAGE, colored_eden=True, pos=HEAP_BEFORE,
  outf=sys.stdout):

  aggfuncs = {
    AVERAGE:average,
    GEOMEAN:geomean,
    MIN:min,
    MAX:max,
    MEDIAN:median
  }

  agg = agg.lower()
  if not aggfuncs.has_key(agg):
    print "invalid agg: %s" % agg
    return

  aggfunc = aggfuncs[agg]

  print >> outf, "Average CKE Off Report (agg=%s)\n" % agg

  chans = range(0,4)
  ranks = range(0,2)
  print >> outf, "".ljust(2),
  print >> outf, "cfg".ljust(20),
  print >> outf, "bench".ljust(30),
  print >> outf, "runtime".rjust(13),
  if colored_eden:
    print >> outf, "eden-red".rjust(13),
    print >> outf, "eden-blue".rjust(13),
  else:
    print >> outf, "eden".rjust(13),
  print >> outf, "surv-red".rjust(13),
  print >> outf, "surv-blue".rjust(13),
  print >> outf, "ten-red".rjust(13),
  print >> outf, "ten-blue".rjust(13),
  print >> outf, "red-rat".rjust(8),
  print >> outf, "blue-rat".rjust(8),
  print >> outf, ""

  allavgs = []
  for cfg in cfgs:
    cfgavgs = []
    for bench in benches:

      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, ("%s"%bench).ljust(30),

      avgrec = []

      utimes = []
      for i in range(iters):
        utime = getUnixTime(bench, cfg, i)[0]
        if utime:
          utimes.append((utime, i))

      my_times = [ x[0] for x in utimes ]
      my_iters = [ x[1] for x in utimes ]

      if len(my_iters) >= miniters:
        utime = average(my_times)
        print >> outf, ("%8.2f" % utime).rjust(13),
        avgrec.append(utime)
      else:
        print >> outf, ("N/A").rjust(13),
        avgrec.append(None)

      gcvals = []
      for i in my_iters:
        gcvals.append( getGCSpaceInfo(bench, cfg, i, colored=True,
                                      ceden=colored_eden) )

      # do the eden space
      if colored_eden:
        eden_red_vals  = []
        eden_red_sum   = []
        eden_blue_vals = []
        eden_blue_sum  = []
        for i,ix in enumerate(my_iters):
          eden_red_samples  =   [ val[pos][EDEN][RED][0]  for val in gcvals[i] ]
          eden_red_vals.append  ( aggfunc(eden_red_samples) )
          eden_red_sum.append   ( sum(eden_red_samples) )
          eden_blue_samples =   [ val[pos][EDEN][BLUE][0] for val in gcvals[i] ]
          eden_blue_vals.append ( aggfunc(eden_blue_samples) )
          eden_blue_sum.append  ( sum(eden_blue_samples) )
        if len(my_iters) >= miniters:
          eden_red_agg_avg  = average(eden_red_vals)
          eden_red_sum_avg  = average(eden_red_sum)
          eden_blue_agg_avg = average(eden_blue_vals)
          eden_blue_sum_avg = average(eden_blue_sum)
          avgrec.append(eden_red_agg_avg)
          #avgrec.append(eden_red_sum_avg)
          avgrec.append(eden_blue_agg_avg)
          #avgrec.append(eden_blue_sum_avg)
          print >> outf, ("%d" % eden_red_agg_avg).rjust(13),
          print >> outf, ("%d" % eden_blue_sum_avg).rjust(13),
        else:
          eden_red_agg_avg = eden_blue_agg_avg = None
          eden_red_sum_avg = eden_blue_sum_avg = None
          avgrec.append(None)
          avgrec.append(None)
          #avgrec.append(None)
          #avgrec.append(None)
          print >> outf, ("N/A").rjust(13),
          print >> outf, ("N/A").rjust(13),
      else:
        eden_vals  = []
        for i,ix in enumerate(my_iters):
          eden_samples  =   [ val[pos][EDEN][0]  for val in gcvals[i] ]
          eden_vals.append  ( aggfunc(eden_samples) )
        if len(my_iters) >= miniters:
          eden = average(eden_vals)
          avgrec.append(eden)
          print >> outf, ("%d" % eden).rjust(13),
        else:
          eden = None
          avgrec.append(None)
          print >> outf, ("N/A").rjust(13),

      # survivor and tenured spaces
      surv_red_vals  = []
      surv_blue_vals = []
      ten_red_vals   = []
      ten_blue_vals  = []
      for i,ix in enumerate(my_iters):

        surv_red_samples  =   [ val[pos][SURVIVOR][RED][0]  for val in gcvals[i] ]
        surv_red_vals.append  ( aggfunc(surv_red_samples)  )
        surv_blue_samples =   [ val[pos][SURVIVOR][BLUE][0] for val in gcvals[i] ]
        surv_blue_vals.append ( aggfunc(surv_blue_samples) )
        ten_red_samples   =   [ val[pos][TENURED][RED][0]   for val in gcvals[i] ]
        ten_red_vals.append   ( aggfunc(ten_red_samples)   )
        ten_blue_samples  =   [ val[pos][TENURED][BLUE][0]  for val in gcvals[i] ]
        ten_blue_vals.append  ( aggfunc(ten_blue_samples)  )

      if len(my_iters) >= miniters:

        surv_red  = average(surv_red_vals)
        surv_blue = average(surv_blue_vals)
        avgrec.append(surv_red)
        avgrec.append(surv_blue)
        print >> outf, ("%d" % surv_red).rjust(13),
        print >> outf, ("%d" % surv_blue).rjust(13),

        ten_red  = average(ten_red_vals)
        ten_blue = average(ten_blue_vals)
        avgrec.append(ten_red)
        avgrec.append(ten_blue)
        print >> outf, ("%d" % ten_red).rjust(13),
        print >> outf, ("%d" % ten_blue).rjust(13),

      else:
        surv_red = surv_blue = None
        avgrec.append(None)
        avgrec.append(None)
        print >> outf, ("N/A").rjust(13),
        print >> outf, ("N/A").rjust(13),

        ten_red = ten_blue = None
        avgrec.append(None)
        avgrec.append(None)
        print >> outf, ("N/A").rjust(13),
        print >> outf, ("N/A").rjust(13),


      # ratios
      if len(my_iters) >= miniters:
        if colored_eden:
          #red_tot   = eden_red  + surv_red  + ten_red
          #blue_tot  = eden_blue + surv_blue + ten_blue

          red_tot   = eden_red_sum_avg
          blue_tot  = eden_blue_sum_avg
          total     = red_tot + blue_tot
        else:
          red_tot   = eden + surv_red + ten_red
          blue_tot  = surv_blue + ten_blue
          total     = red_tot + blue_tot

        red_rat   = float(red_tot)  / float(total)
        blue_rat  = float(blue_tot) / float(total)

        avgrec.append(red_rat)
        avgrec.append(blue_rat)
        print >> outf, ("%2.4f" % red_rat).rjust(8),
        print >> outf, ("%2.4f" % blue_rat).rjust(8),
      else:
        avgrec.append(None)
        avgrec.append(None)
        print >> outf, ("N/A").rjust(8),
        print >> outf, ("N/A").rjust(8),

      cfgavgs.append(avgrec)
      allavgs.append(avgrec)
      print  >> outf, ""

    if cfgavgs:
      print >> outf, "".ljust(2),
      print >> outf, ("%s"%cfg).ljust(20),
      print >> outf, "*".ljust(30),

      printACGCAvgVal(cfgavgs, 0, "%8.2f", (RIGHT_ALIGN, 13), outf=outf)
      nvals = 6 if colored_eden else 5

      for i in  range(1,nvals+1):
        printACGCAvgVal(cfgavgs, i, "%5.2f", (RIGHT_ALIGN, 13), outf=outf) 
      printACGCAvgVal(cfgavgs, i+1, "%2.4f", (RIGHT_ALIGN, 8), outf=outf) 
      printACGCAvgVal(cfgavgs, i+2, "%2.4f", (RIGHT_ALIGN, 8), outf=outf) 
      print >> outf, "\n"

  print >> outf, "".ljust(2),
  print >> outf, "*".ljust(20),
  print >> outf, "*".ljust(30),

  printACGCAvgVal(allavgs, 0, "%8.2f", (RIGHT_ALIGN, 13), outf=outf)
  nvals = 6 if colored_eden else 5

  for i in  range(1,nvals+1):
    printACGCAvgVal(allavgs, i, "%5.2f", (RIGHT_ALIGN, 13), outf=outf) 
  printACGCAvgVal(allavgs, i+1, "%2.4f", (RIGHT_ALIGN, 8),  outf=outf) 
  printACGCAvgVal(allavgs, i+2, "%2.4f", (RIGHT_ALIGN, 8),  outf=outf) 

  print >> outf, ""

#def printKSARLine(items, outf=sys.stdout):
#  print >> outf, ("%s"    % items[0]).ljust(30),
#  print >> outf, ("%d"    % items[1]).rjust(5),
#  print >> outf, ("%d"    % items[2]).rjust(12),
#  print >> outf, ("%5.4f" % items[3]).rjust(9),
#  print >> outf, ("%d"    % items[4]).rjust(8),
#  print >> outf, ("%5.4f" % items[5]).rjust(9),
#  print >> outf, ("%d"    % items[6]).rjust(8),
#  print >> outf, ("%5.4f" % items[7]).rjust(9),
#  print >> outf, ("%d"    % items[8]).rjust(12),
#  print >> outf, ("%5.4f" % items[9]).rjust(9),
#  print >> outf, ("%d"    % items[10]).rjust(8),
#  print >> outf, ("%5.4f" % items[11]).rjust(9),
#  print >> outf, ("%d"    % items[12]).rjust(8),
#  print >> outf, ("%5.4f" % items[13]).rjust(9),
#  print >> outf, ""
#
#def knapsackSimAggReport(kss=None, benches=dacapo_defs, cfg=OBJ_INFO_INTERVAL,
#  iter=0, clean=True, cutoff=0.02, sigfigs=5, aggtype=AGG_STATIC_SAME_PPV,
#  rattype=AGG_RATIO, sztype=SIZE, outf=sys.stdout):
#
#  if not kss:
#    kss = getKnapsackSims(benches=benches, cfg=cfg, iter=iter, \
#                          clean=clean, cutoff=cutoff, sigfigs=sigfigs)
#
#  print >> outf, "Knapsack Sim Aggregates Report for %s\n" % aggtype.lower()
#
#  print >> outf, "bench".ljust(30),
#  print >> outf, "nvals".rjust(5),
#  if sztype == OBJECTS:
#    print >> outf, "objects".rjust(12),
#    print >> outf, "obj %".rjust(9),
#    print >> outf, "vmiss_lo".rjust(8),
#    print >> outf, "omiss_lo".rjust(9),
#    print >> outf, "vmiss_hi".rjust(8),
#    print >> outf, "omiss_hi".rjust(9),
#  else:
#    print >> outf, "size (KB)".rjust(12),
#    print >> outf, "size %".rjust(9),
#    print >> outf, "vmiss_lo".rjust(8),
#    print >> outf, "smiss_lo".rjust(9),
#    print >> outf, "vmiss_hi".rjust(8),
#    print >> outf, "smiss_hi".rjust(9),
#  print >> outf, "refs".rjust(12),
#  print >> outf, "refs %".rjust(9),
#  print >> outf, "vmiss_lo".rjust(8),
#  print >> outf, "rmiss_lo".rjust(9),
#  print >> outf, "vmiss_hi".rjust(8),
#  print >> outf, "rmiss_hi".rjust(9),
#  #print >> outf, "xref-miss".rjust(9),
#  #print >> outf, "max miss".rjust(9),
#  #print >> outf, "refs std".rjust(9),
#  print >> outf, ""
#
#  if sztype == OBJECTS:
#    sz_key = OBJECTS
#    sz_misses_key = OBJECTS_MISSES
#  else:
#    sz_key = SIZE
#    sz_misses_key = SIZE_MISSES
#
#  avgs = []
#  for bench in benches:
#
#    rec = kss[bench][cfg][aggtype]
#
#    nvals              = rec[NVALS]
#
#    tot_size           = rec[sz_key][TOTAL]
#    rat_size           = rec[sz_key][rattype]
#    sval_miss_lo       = rec[sz_misses_key][0][NVALS]
#    size_miss_lo       = rec[sz_misses_key][0][rattype]
#    sval_miss_hi       = rec[sz_misses_key][1][NVALS]
#    size_miss_hi       = rec[sz_misses_key][1][rattype]
#    #rat_size_misses    = rec[SIZE_MISSES][rattype]
#    #max_size_misses    = rec[SIZE_MISSES][MAX_RATIO]
#    #std_size           = rec[SIZE][STD_RATIO]
#
#    tot_refs           = rec[REFS][TOTAL]
#    rat_refs           = rec[REFS][rattype]
#    rval_miss_lo       = rec[REFS_MISSES][0][NVALS]
#    refs_miss_lo       = rec[REFS_MISSES][0][rattype]
#    rval_miss_hi       = rec[REFS_MISSES][1][NVALS]
#    refs_miss_hi       = rec[REFS_MISSES][1][rattype]
#    #rat_refs_misses_x  = rec[REFS_MISSES][AGG_RATIO_X]
#    #max_refs_misses    = rec[REFS_MISSES][MAX_RATIO]
#    #std_refs           = rec[REFS][STD_RATIO]
#
##    print tot_size
##    print rat_size
##    print sval_miss_lo
##    print size_miss_lo
##    print sval_miss_hi
##    print size_miss_hi
#    printKSARLine((bench, nvals, tot_size, rat_size, sval_miss_lo,
#                   size_miss_lo, sval_miss_hi, size_miss_hi,
#                   tot_refs, rat_refs, rval_miss_lo, refs_miss_lo,
#                   rval_miss_hi, refs_miss_hi), outf=outf)
#
#    avgs.append((nvals, tot_size, rat_size, sval_miss_lo, size_miss_lo,
#                 sval_miss_hi, size_miss_hi, tot_refs, rat_refs, rval_miss_lo,
#                 refs_miss_lo, rval_miss_hi, refs_miss_hi))
#
##    printKSARLine((bench, nvals,
##      tot_objects, rat_objects, rat_objects_misses, std_objects,
##      tot_size, rat_size, rat_size_misses, std_size,
##      tot_refs, rat_refs, rat_refs_misses, std_refs), outf=outf)
##
##    avgs.append((nvals, tot_objects, rat_objects, rat_objects_misses,
##                 std_objects, tot_size, rat_size, rat_size_misses,
##                 std_size, tot_refs, rat_refs, rat_refs_misses,
##                 std_refs))
#
##    printKSARLine((bench, nvals,
##      tot_objects, rat_objects, rat_objects_misses, max_objects_misses,
##      tot_size, rat_size, rat_size_misses, max_size_misses,
##      tot_refs, rat_refs, rat_refs_misses, max_refs_misses), outf=outf)
##
##    avgs.append((nvals, tot_objects, rat_objects, rat_objects_misses,
##                 max_objects_misses, tot_size, rat_size, rat_size_misses,
##                 max_size_misses, tot_refs, rat_refs, rat_refs_misses,
##                 max_refs_misses))
#
#  if avgs:
#    nvals              = average( [ x[0]  for x in avgs ] )
#    tot_size           = average( [ x[1]  for x in avgs ] )
#    rat_size           = average( [ x[2]  for x in avgs ] )
#    sval_miss_lo       = average( [ x[3]  for x in avgs ] )
#    size_miss_lo       = average( [ x[4]  for x in avgs ] )
#    sval_miss_hi       = average( [ x[5]  for x in avgs ] )
#    size_miss_hi       = average( [ x[6]  for x in avgs ] )
#    tot_refs           = average( [ x[7]  for x in avgs ] )
#    rat_refs           = average( [ x[8]  for x in avgs ] )
#    rval_miss_lo       = average( [ x[9]  for x in avgs ] )
#    refs_miss_lo       = average( [ x[10] for x in avgs ] )
#    rval_miss_hi       = average( [ x[11] for x in avgs ] )
#    refs_miss_hi       = average( [ x[12] for x in avgs ] )
#
#    printKSARLine(("*", nvals, tot_size, rat_size, sval_miss_lo, size_miss_lo,
#      sval_miss_hi, size_miss_hi, tot_refs, rat_refs, rval_miss_lo, refs_miss_lo,
#      rval_miss_hi, refs_miss_hi), outf=outf)
#
##    printKSARLine(("*", nvals,
##      tot_objects, rat_objects, rat_objects_misses, std_objects,
##      tot_size, rat_size, rat_size_misses, std_size,
##      tot_refs, rat_refs, rat_refs_misses, std_refs), outf=outf)
#
##    printKSARLine(("*", nvals,
##      tot_objects, rat_objects, rat_objects_misses, max_objects_misses,
##      tot_size, rat_size, rat_size_misses, max_size_misses,
##      tot_refs, rat_refs, rat_refs_misses, max_refs_misses), outf=outf)
#
#def printKSTRLine(items, outf=sys.stdout):
#  print >> outf, "".ljust(2),
#  print >> outf, ("%s"%items[0]).ljust(20),
#  print >> outf, ("%s"%items[1]).ljust(30),
#  print >> outf, ("%d"    % items[2]).rjust(12),
#  print >> outf, ("%5.4f" % items[3]).rjust(12),
#  print >> outf, ("%d"    % items[4]).rjust(12),
#  print >> outf, ("%5.4f" % items[5]).rjust(12),
#  print >> outf, ("%d"    % items[6]).rjust(12),
#  print >> outf, ("%5.4f" % items[7]).rjust(12),
#  print >> outf, ""
#
#def knapsackSimTotalsReport(kss=None, benches=dacapo_defs, cfg=OBJ_INFO_SIM,
#  iter=0, clean=True, cutoff=0.02, sigfigs=5, ktype=STATIC_SAME_KS, outf=sys.stdout):
#
#  if not kss:
#    kss = getKnapsackSims(benches=benches, cfg=cfg, iter=iter, \
#                          clean=clean, cutoff=cutoff, sigfigs=sigfigs)
#
#  print >> outf, "Knapsack Sim Totals Report for %s\n" % ktype.lower()
#
#  print >> outf, "".ljust(2),
#  print >> outf, "cfg".ljust(20),
#  print >> outf, "bench".ljust(30),
#  print >> outf, "objects".rjust(12),
#  print >> outf, "objects %%".rjust(12),
#  print >> outf, "size (KB)".rjust(12),
#  print >> outf, "size %%".rjust(12),
#  print >> outf, "refs".rjust(12),
#  print >> outf, "refs %%".rjust(12),
#  print >> outf, ""
#
#  objects_idx = size_idx = refs_idx = None
#  if ktype == STATIC_SAME_KS:
#    objects_idx = FULL_OBJECTS_IDX
#    size_idx    = FULL_SIZE_IDX
#    refs_idx    = STATIC_SAME_IDX
#    rec_idx     = STATIC_SAME_KS_TOTALS
#  elif ktype == GUIDE_REFS_KS:
#    objects_idx = FULL_OBJECTS_IDX
#    size_idx    = FULL_SIZE_IDX
#    refs_idx    = STATIC_SAME_IDX
#    rec_idx     = GUIDED_KS_REFS_TOTALS
#  else:
#    raise SystemExit(1)
#
#  avgs = []
#  for bench in benches:
#
#    rec = kss[bench][cfg][rec_idx]
#
#    tot_size    = rec[ALL_APS][size_idx]
#    rat_size    = rec[KS_RATIO][size_idx]
#    tot_objects = rec[ALL_APS][objects_idx]
#    rat_objects = rec[KS_RATIO][objects_idx]
#    tot_refs    = rec[ALL_APS][refs_idx]
#    rat_refs    = rec[KS_RATIO][refs_idx]
#
#    printKSTRLine((cfg, bench, tot_objects, rat_objects, tot_size, rat_size,
#                   tot_refs, rat_refs), outf=outf)
#
#    avgs.append((tot_objects,rat_objects,tot_size,rat_size,tot_refs,rat_refs))
#
#  if avgs:
#    tot_objects = average( [ x[0] for x in avgs ] )
#    rat_objects = average( [ x[1] for x in avgs ] )
#    tot_size    = average( [ x[2] for x in avgs ] )
#    rat_size    = average( [ x[3] for x in avgs ] )
#    tot_refs    = average( [ x[4] for x in avgs ] )
#    rat_refs    = average( [ x[5] for x in avgs ] )
#
#    printKSTRLine((cfg, "*", tot_objects, rat_objects, tot_size, rat_size,
#                   tot_refs, rat_refs), outf=outf)
#
#def printKSDRLine(items, outf=sys.stdout):
#  print >> outf, ("%s"    % items[0]).ljust(30),
#  print >> outf, ("%d"    % items[1]).rjust(5),
#  print >> outf, ("%d"    % items[2]).rjust(12),
#  print >> outf, ("%5.4f" % items[3]).rjust(9),
#  print >> outf, ("%d"    % items[4]).rjust(12),
#  print >> outf, ("%5.4f" % items[5]).rjust(9),
#  print >> outf, ("%d"    % items[6]).rjust(12),
#  print >> outf, ("%5.4f" % items[7]).rjust(9),
#  print >> outf, ""
#
#def knapsackSimDiffReport(kss=None, benches=dacapo_defs, cfg=OBJ_INFO_INTERVAL,
#  iter=0, clean=True, cutoff=0.02, sigfigs=5, aggtype=AGG_STATIC_DIFF,
#  outf=sys.stdout):
#
#  if not kss:
#    kss = getKnapsackSims(benches=benches, cfg=cfg, iter=iter, \
#                          clean=clean, cutoff=cutoff, sigfigs=sigfigs)
#
#  print >> outf, "Knapsack Sim Aggregates Report for %s\n" % aggtype.lower()
#
#  print >> outf, "bench".ljust(30),
#  print >> outf, "nvals".rjust(5),
#  print >> outf, "objects".rjust(12),
#  print >> outf, "obj-diff".rjust(9),
#  print >> outf, "size (KB)".rjust(12),
#  print >> outf, "sz-diff".rjust(9),
#  print >> outf, "refs".rjust(12),
#  print >> outf, "ref-diff".rjust(9),
#  print >> outf, ""
#
#  avgs = []
#  for bench in benches:
#
#    rec = kss[bench][cfg][aggtype]
#
#    nvals         = rec[NVALS]
#    tot_objects   = rec[OBJECTS][0]
#    tot_size      = rec[SIZE][0]
#    tot_refs      = rec[REFS][0]
#    rat_objects   = rec[OBJECTS][2]
#    rat_size      = rec[SIZE][2]
#    rat_refs      = rec[REFS][2]
#
#    printKSDRLine((bench, nvals, tot_objects, rat_objects,
#                   tot_size, rat_size, tot_refs, rat_refs), outf=outf)
#
#    avgs.append((nvals, tot_objects, rat_objects, tot_size, rat_size,
#                 tot_refs, rat_refs))
#
#  if avgs:
#    nvals         = average( [ x[0] for x in avgs ] )
#    tot_objects   = average( [ x[1] for x in avgs ] )
#    rat_objects   = average( [ x[2] for x in avgs ] )
#    tot_size      = average( [ x[3] for x in avgs ] )
#    rat_size      = average( [ x[4] for x in avgs ] )
#    tot_refs      = average( [ x[5] for x in avgs ] )
#    rat_refs      = average( [ x[6] for x in avgs ] )
#
#    printKSDRLine(("*", nvals, tot_objects, rat_objects,
#                   tot_size, rat_size, tot_refs, rat_refs), outf=outf)


#def printKVRLine(items, outf=sys.stdout):
#  print >> outf, "".ljust(2),
#  print >> outf, ("%s"    % items[0]).ljust(5),
#  print >> outf, ("%s"    % items[1]).ljust(9),
#  print >> outf, ("%d"    % items[2]).rjust(12),
#  print >> outf, ("%5.4f" % items[3]).rjust(9),
#  print >> outf, ("%5.4f" % items[4]).rjust(9),
#  print >> outf, ("%5.4f" % items[5]).rjust(9),
#  print >> outf, ("%d"    % items[6]).rjust(12),
#  print >> outf, ("%5.4f" % items[7]).rjust(9),
#  print >> outf, ("%5.4f" % items[8]).rjust(9),
#  print >> outf, ("%5.4f" % items[9]).rjust(9),
#  print >> outf, ("%d"    % items[10]).rjust(12),
#  print >> outf, ("%5.4f" % items[11]).rjust(9),
#  print >> outf, ("%5.4f" % items[12]).rjust(9),
#  print >> outf, ("%5.4f" % items[13]).rjust(9),
#  print >> outf, ""

#def printKVRAggLine(avgs, label, aggfn):
#  agg_tot_objects     = aggfn( [ x[0]  for x in avgs ] )
#  agg_rat_objects     = aggfn( [ x[1]  for x in avgs ] )
#  agg_miss_objects_lo = aggfn( [ x[2]  for x in avgs ] )
#  agg_miss_objects_hi = aggfn( [ x[3]  for x in avgs ] )
#  agg_tot_size        = aggfn( [ x[4]  for x in avgs ] )
#  agg_rat_size        = aggfn( [ x[5]  for x in avgs ] )
#  agg_miss_size_lo    = aggfn( [ x[6]  for x in avgs ] )
#  agg_miss_size_hi    = aggfn( [ x[7]  for x in avgs ] )
#  agg_tot_refs        = aggfn( [ x[8]  for x in avgs ] )
#  agg_rat_refs        = aggfn( [ x[9]  for x in avgs ] )
#  agg_miss_refs_lo    = aggfn( [ x[10] for x in avgs ] )
#  agg_miss_refs_hi    = aggfn( [ x[11] for x in avgs ] )
#
#  printKVRLine((label, "-", agg_tot_objects, agg_rat_objects,
#                agg_miss_objects_lo, agg_miss_objects_hi, agg_tot_size,
#                agg_rat_size, agg_miss_size_lo, agg_miss_size_hi,
#                agg_tot_refs, agg_rat_refs, agg_miss_refs_lo,
#                agg_miss_refs_hi), outf=outf)
#
#def knapsackValReport(ksinfo=None, bench=None, cfg=OBJ_INFO_INTERVAL, \
#  iter=0, clean=True, ktype=STATIC_SAME_PPV, cutoff=0.02, sigfigs=5, sidx=0, \
#  outf=sys.stdout):
#
#  if bench is None:
#    print "need to specify a bench"
#    return
#
#  if not ksinfo:
#    ksinfo = knapsackSim(bench, cfg, iter=iter, clean=clean, cutoff=cutoff,
#                         sigfigs=sigfigs)
#
#  objects_idx = VAL_ALIVE_OBJECTS_IDX
#  size_idx    = VAL_ALIVE_SIZE_IDX
#  refs_idx    = VAL_REFS_IDX
#
#  print >> outf, "Knapsack Val Report for %s-%s" % (bench, cfg)
#  print >> outf, "ktype: %s (cutoff=%3.2f, sigfigs=%d)" % (ktype, cutoff, sigfigs)
#
#  print >> outf, "".ljust(2),
#  print >> outf, "val".ljust(5),
#  print >> outf, "time (MS)".ljust(9),
#  print >> outf, "objects".rjust(12),
#  print >> outf, "obj %".rjust(9),
#  print >> outf, "lo miss".rjust(9),
#  print >> outf, "hi miss".rjust(9),
#  print >> outf, "size (KB)".rjust(12),
#  print >> outf, "size %".rjust(9),
#  print >> outf, "lo miss".rjust(9),
#  print >> outf, "hi miss".rjust(9),
#  print >> outf, "refs".rjust(12),
#  print >> outf, "refs %".rjust(9),
#  print >> outf, "lo miss".rjust(9),
#  print >> outf, "hi miss".rjust(9),
#  print >> outf, ""
#
#  avgs = []
#  for i,val in enumerate(ksinfo[VALS]):
#    if i < sidx:
#      continue
#
#    if val[REASON] in ['pre-major-gc','post-minor-gc', 'post-major-gc']:
#      continue
#
#    rec = val[ktype]
#    if not rec:
#      print >> outf, "".ljust(2),
#      print >> outf, ("%d"    % i).ljust(5),
#      print >> outf, ("%d"    % val[DURATION]).ljust(9),
#      print >> outf, "None".rjust(12),
#      print >> outf, "None".rjust(9),
#      print >> outf, "None".rjust(9),
#      print >> outf, "None".rjust(9),
#      print >> outf, "None".rjust(12),
#      print >> outf, "None".rjust(9),
#      print >> outf, "None".rjust(9),
#      print >> outf, "None".rjust(9),
#      print >> outf, "None".rjust(12),
#      print >> outf, "None".rjust(9),
#      print >> outf, "None".rjust(9),
#      print >> outf, "None".rjust(9),
#      print >> outf, ""
#      continue
#
#    tot_objects     = rec[ALL_APS][objects_idx]
#    rat_objects     = rec[RATIO][objects_idx]
#    miss_objects_lo = get_objects_misses(rec, val[IDEAL_KS_PPV])[0][1]
#    miss_objects_hi = get_objects_misses(rec, val[IDEAL_KS_PPV])[1][1]
#
#    tot_size     = rec[ALL_APS][size_idx]
#    rat_size     = rec[RATIO][size_idx]
#    miss_size_lo = get_size_misses(rec, val[IDEAL_KS_PPV])[0][1]
#    miss_size_hi = get_size_misses(rec, val[IDEAL_KS_PPV])[1][1]
#
#    tot_refs     = rec[ALL_APS][refs_idx]
#    rat_refs     = rec[RATIO][refs_idx]
#    miss_refs_lo = get_refs_misses(rec, val[IDEAL_KS_PPV], \
#                                   target_rat=cutoff)[0][1]
#    miss_refs_hi = get_refs_misses(rec, val[IDEAL_KS_PPV], \
#                                   target_rat=cutoff)[1][1]
#
#    printKVRLine((str(i), str(val[DURATION]), tot_objects, rat_objects,
#                 miss_objects_lo, miss_objects_hi, tot_size, rat_size,
#                 miss_size_lo, miss_size_hi, tot_refs, rat_refs, miss_refs_lo,
#                 miss_refs_hi), outf=outf)
#
#    avgs.append((tot_objects, rat_objects, miss_objects_lo, miss_objects_hi,
#                 tot_size, rat_size, miss_size_lo, miss_size_hi, tot_refs,
#                 rat_refs, miss_refs_lo, miss_refs_hi))
#
#  printKVRAggLine(avgs, "average", average)
#  printKVRAggLine(avgs, "max",     max    )
#  printKVRAggLine(avgs, "min",     min    )

def print_ks_header(item, outf=sys.stdout):
  val, width, align = item[:3]
  print >> outf, align(val, width),

def print_ks_item(scfg, val, outf=sys.stdout):
  width, align, fmt = scfg[1:4]
  if val != None:
    print >> outf, align((fmt % val), width),
  else:
    print >> outf, align("N/A", width),

def print_ks_agg_item(scfg, val, outf=sys.stdout):
  width, align, fmt = scfg[1:4]
  if scfg[6]:
    if val != None:
      print >> outf, align((fmt % val), width),
    else:
      print >> outf, align("N/A", width),
  else:
    print >> outf, align("-", width),

def print_ks_line(items, agg=False, outf=sys.stdout):
  for scfg,val in items:
    if agg:
      print_ks_agg_item(scfg, val, outf=outf),
    else:
      print_ks_item(scfg, val, outf=outf),
  print >> outf, ""

def print_kvr_line(head, items, agg=False, outf=sys.stdout):
  print >> outf, ("%s" % head).ljust(5),
  print_ks_line(items, agg=agg, outf=outf)

def print_ksir_line(bench, items, agg=False, outf=sys.stdout):
  print >> outf, ("%s" % bench).ljust(22),
  print_ks_line(items, agg=agg, outf=outf)

def get_agg_val_item(rec, keys, fn, aggfn, dur_cutoff=50):
  agg  = None
  vals = []
  for i,val in enumerate(rec[VALS]):
    if val[REASON] in ['pre-major-gc','post-minor-gc', 'post-major-gc']:
      continue
    if val[DURATION] < dur_cutoff:
      continue
    if sum ( [ val[APD][k][VAL_REFS_IDX] for k in val[APD].keys() ] ) == 0:
      #print "warning: val has no refs. duration: ", val[DURATION]
      continue

    item = get_ks_item(val, keys, fn)
    #print_kvr_line(str(i), zip(my_statcfg, items), outf=outf)
    vals.append(item)

  if vals:
    #print vals
    myvs = [ float(x) for x in vals if not x == None ]
    agg  = aggfn(myvs)
  return agg

def get_ks_item(val, keys, fn):

  item = None
  if type(keys[0]) is list and keys[0][0] == AGG_VAL:
    item = get_agg_val_item(val, keys[1:], fn, keys[0][1])
  elif len(keys) == 1:
    key = keys[0]
    if type(key) is list:
      if fn in misses_fns:
        # misses fn
        arg0, arg1, arg2, arg3, arg4, arg5 = key
        if val[arg0] != None:
          item = fn(val[arg0], val[arg1], arg2, arg3, arg4, arg5)
      elif fn == get_over_under_ratio:
        arg0, arg1 = key
        if val[arg0] != None:
          item = fn(val[arg0], arg1)
    else:
      if val[key] != None:
        item = fn(val[key]) if fn else val[key]
  elif len(keys) == 2:
    k0, k1 = keys
    if val[k0] != None:
      item = fn(val[k0][k1]) if fn else val[k0][k1]
  elif len(keys) == 3:
    k0, k1, k2 = keys
    if val[k0] != None:
      item = fn(val[k0][k1][k2]) if fn else val[k0][k1][k2]
  elif len(keys) == 4:
    k0, k1, k2, k3 = keys
    if val[k0] != None:
      item = fn(val[k0][k1][k2][k3]) if fn else val[k0][k1][k2][k3]
  else:
    print "malformed statcfg: ", sc
    raise SystemExit(1)

  return item

def get_ks_items(val, statcfg):

  items = []
  for sc in statcfg:
    keys, fn = sc[4:6]
    items.append(get_ks_item(val, keys, fn))

  return items

def get_agg_ks_items(vals, aggfn, statcfg):
  
  items = []
  for i,sc in enumerate(statcfg):
    if sc[6]:
      myvs = [ float(x[i]) for x in vals if not x[i] == None ]
      items.append(aggfn(myvs))
    else:
      items.append(None)

  return items

def copysc(statcfg):
  newsc = []
  for sc in statcfg:
    mycopy = deepcopy(sc[4])
    newsc.append((sc[0], sc[1], sc[2], sc[3], mycopy, sc[5], sc[6]))
  return newsc

def replace_ksc_args(statcfg, args):
  for sc in statcfg:
    keys = sc[4]
    for i,key in enumerate(keys):
      if type(key) is list:
        for j,kt in enumerate(key):
          if kt in args.keys():
            key[j] = args[kt]
      else:
        if key in args.keys():
          keys[i] = args[key]

def knapsackValReport(ksinfo, statcfg=kvcfg, kstype=STATIC_SAME_PPV,
  dur_cutoff=50, outf=sys.stdout):

  print >> outf, "Knapsack Val Report"
  print >> outf, "bench".ljust(5),

  my_statcfg = copysc(statcfg)
  replace_ksc_args(my_statcfg, {"__arg_kstype" : kstype})

  for item in my_statcfg:
    print_ks_header(item, outf)
  print >> outf, ""

  vals = []
  for i,val in enumerate(ksinfo[VALS]):
    if val[REASON] in ['pre-major-gc','post-minor-gc', 'post-major-gc']:
      continue
    if val[DURATION] < dur_cutoff:
      continue
    if sum ( [ val[APD][k][VAL_REFS_IDX] for k in val[APD].keys() ] ) == 0:
      print "warning: val has no refs. duration: ", val[DURATION]
      continue

    items = get_ks_items(val, my_statcfg)
    print_kvr_line(str(i), zip(my_statcfg, items), outf=outf)
    vals.append(items)

  if vals:
    avgs  = get_agg_ks_items(vals, average, my_statcfg)
    maxs  = get_agg_ks_items(vals, max,     my_statcfg)
    mins  = get_agg_ks_items(vals, min,     my_statcfg)

    print_kvr_line("max", zip(my_statcfg, maxs), agg=True, outf=outf)
    print_kvr_line("min", zip(my_statcfg, mins), agg=True, outf=outf)
    print_kvr_line("avg", zip(my_statcfg, avgs), agg=True, outf=outf)

def benchInfoReport(x, dur_cutoff=50, outf=sys.stdout):

  print >> outf, "Bench Info Report"
  print >> outf, "bench".ljust(22),

  print >> outf, "nvals".ljust(5),
  print >> outf, "avg KB".ljust(14),
  print >> outf, "max KB".ljust(14),
  print >> outf, "all KB".ljust(14),
  print >> outf, "avg refs".ljust(14),
  print >> outf, "max refs".ljust(14),
  print >> outf, "all refs".ljust(14),
  print >> outf, ""

  benches = x.keys()
  cfg     = x[x.keys()[0]].keys()[0]

  for bench in benches:
    ksinfo = x[bench][cfg]
    vals = []

    for i,val in enumerate(ksinfo[VALS]):
      if val[REASON] in ['pre-major-gc','post-minor-gc', 'post-major-gc']:
        continue
      if val[DURATION] < dur_cutoff:
        continue
      if sum ( [ val[APD][k][VAL_REFS_IDX] for k in val[APD].keys() ] ) == 0:
        #print "warning: val has no refs. duration: ", val[DURATION]
        continue

      alive_bytes = val[IDEAL_KS_PPV][ALL_STATS][VAL_ALIVE_SIZE_IDX]
      new_bytes   = val[IDEAL_KS_PPV][ALL_STATS][VAL_NEW_SIZE_IDX]
      refs        = val[IDEAL_KS_PPV][ALL_STATS][VAL_REFS_IDX]
      vals.append((alive_bytes, new_bytes, refs))

    if vals:
      nvals    = len(vals)
      avg_size = int(average( [ v[0] for v in vals ] ) / 1024)
      max_size = int(max    ( [ v[0] for v in vals ] ) / 1024)
      all_size = int(sum    ( [ v[1] for v in vals ] ) / 1024)
      avg_refs = average( [ v[2] for v in vals ] )
      max_refs = max    ( [ v[2] for v in vals ] )
      all_refs = int(sum    ( [ v[2] for v in vals ] ))

      print >> outf, ("%s"   % bench).ljust(22),
      print >> outf, ("%d"   % nvals).ljust(5),
      print >> outf, ("%d"   % avg_size).ljust(14),
      print >> outf, ("%d"   % max_size).ljust(14),
      print >> outf, ("%d"   % all_size).ljust(14),
      print >> outf, ("%d" % avg_refs).ljust(14),
      print >> outf, ("%d" % max_refs).ljust(14),
      print >> outf, ("%d" % all_refs).ljust(14),
      print >> outf, ""

def knapsackSimInfoReport(kss, benches=dacapo_defs, cfg=OBJ_INFO_XSIM,
  statcfg=ksicfg, kstype=AGG_STATIC_SAME_PPV, vkstype=STATIC_SAME_PPV,
  aggtype=PER_VAL, outf=sys.stdout):

  print >> outf, "Knapsack Sim Info Report"
  print >> outf, "bench".ljust(22),

  my_statcfg = copysc(statcfg)
  replace_ksc_args(my_statcfg, {"__arg_kstype"  : kstype,
                                "__arg_vkstype" : vkstype,
                                "__arg_aggtype" : aggtype}
  )

  for item in my_statcfg:
    print_ks_header(item, outf)
  print >> outf, ""

  bnvals = []
  for bench in benches:
    rec   = kss[bench][cfg]
    items = get_ks_items(rec, my_statcfg)
    print_ksir_line(bench, zip(my_statcfg, items), outf=outf)
    bnvals.append(items)

  if bnvals:
    avgs  = get_agg_ks_items(bnvals, average, my_statcfg)
    maxs  = get_agg_ks_items(bnvals, max,     my_statcfg)
    mins  = get_agg_ks_items(bnvals, min,     my_statcfg)

    #print_ksir_line("max", zip(my_statcfg, maxs), agg=True, outf=outf)
    #print_ksir_line("min", zip(my_statcfg, mins), agg=True, outf=outf)
    print_ksir_line("avg", zip(my_statcfg, avgs), agg=True, outf=outf)

def knapsackSimExcelReport(kss, benches=dacapo_defs, cfg=OBJ_INFO_XSIM,
  statcfg=ksicfg, kstypes=defkstypes, aggtype=PER_VAL, style=REFS,
  outf=sys.stdout):

  for bench in benches:
    rec = kss[bench][cfg]
    for i, kt in enumerate(kstypes):
      kstype, vkstype = kt
      outstr = cStringIO.StringIO()
      my_statcfg = copysc(statcfg)
      replace_ksc_args(my_statcfg, {"__arg_kstype"  : kstype,
                                    "__arg_vkstype" : vkstype,
                                    "__arg_aggtype" : aggtype}
      )
      items = get_ks_items(rec, my_statcfg)
      print_ksir_line(bench, zip(my_statcfg, items), outf=outstr)
      vals = outstr.getvalue()
      outstr.close()

      pts = vals.split()
      if style == SIZE:
        over  = (100.0  * float(pts[5]))
        under = (-100.0 * float(pts[6]))
      elif style == REFS:
        over  = (100.0  * float(pts[11]))
        under = (-100.0 * float(pts[12]))
      else:
        raise SystemExit(1)

      for j in range(i):
        print >> outf, " , ,",
      print >> outf, " %s, %s" % (under, over)

def print_kssr_line(label, items, outf=sys.stdout):
  print >> outf, ("%s" % label).ljust(22),
  print_ks_line(items, outf=outf)

def print_excel_kssr_line(i, label, items, style=REFS, outf=sys.stdout):
  outstr = cStringIO.StringIO()
  print_ks_line(items, outf=outstr)
  vals = outstr.getvalue()
  outstr.close()

  pts = vals.split()
  if style in [SIZE,REFS]:
    if style == SIZE:
      over  = (100.0  * float(pts[1]))
      under = (-100.0 * float(pts[2]))
    elif style == REFS:
      over  = (100.0  * float(pts[5]))
      under = (-100.0 * float(pts[6]))
    for j in range(i-1):
      print >> outf, " , ,",
    print >> outf, " %s, %s" % (under, over)
  elif style == FIXED_SIZE_REFS:
    val = (100.0 * float(pts[5]))
    for j in range(i-1):
      print >> outf, " , ",
    print >> outf, "%s" % val
  else:
    raise SystemExit(1)

def get_ks_strat_items(kss, statcfg, benches, cfg):
  st_items = None
  bnavgs   = []
  for bench in benches:
    rec = kss[bench][cfg]
    items = get_ks_items(rec, statcfg)
    bnavgs.append(items)
  if bnavgs:
    st_items = get_agg_ks_items(bnavgs, average, statcfg)
  return st_items 

def knapsackSimStrategyReport(kss, benches=dacapo_defs, cfg=OBJ_INFO_XSIM,
  strats=ksstrats, statcfg=ksstratcfg, aggtype=PER_VAL, excel=False,
  style=REFS, outf=sys.stdout):

  if not excel:
    print >> outf, "Knapsack Sim Agg Report"
    print >> outf, "strategy".ljust(22),

    for item in statcfg:
      print_ks_header(item, outf)
    print >> outf, ""

  for i,strip in enumerate(strats):
    strat, vstrat, label = strip

    if excel and strat == AGG_IDEAL_KS_PPV:
      continue

    my_statcfg = copysc(statcfg)
    replace_ksc_args(my_statcfg, {"__arg_kstype"  : strat,
                                  "__arg_vkstype" : vstrat,
                                  "__arg_aggtype" : aggtype}
    )

    items = get_ks_strat_items(kss, my_statcfg, benches, cfg)

    if excel:
      print_excel_kssr_line(i, label, zip(my_statcfg, items), style=style, \
                            outf=outf)
    else:
      print_kssr_line(label, zip(my_statcfg, items), outf=outf)

def print_oat_header(item, outf=sys.stdout):
  val, width, align = item[:3]
  print >> outf, align(val, width),

def print_oat_item(scfg, val, outf=sys.stdout):
  width, align, fmt = scfg[1:4]
  print >> outf, align((fmt % val), width),

def print_oat_agg_item(scfg, val, outf=sys.stdout):
  width, align, fmt = scfg[1:4]
  if scfg[6]:
    if val != None:
      print >> outf, align((fmt % val), width),
    else:
      print >> outf, align("N/A", width),
  else:
    print >> outf, align("-", width),

def get_oai_vals(statcfg, rec):

  vals = []
  rawvals = []
  for item in statcfg:
    keys, fn, avgkey = item[4:7]
    if keys == None:
      if item[0] == "nvals":
        vals.append(len(rec[VALS]))
        rawvals.append(len(rec[VALS]))
      else:
        print "invalid item in statcfg: ", item
        raise SystemExit(1)
    else:
      rec_avg = rec[avgkey]
      if len(keys) == 1:
        vals.append(fn(rec_avg[keys[0]]) if fn else rec_avg[keys[0]])
        rawvals.append(rec_avg[keys[0]])
      elif keys[0] in [VAL_HISTORY, VAL_FUTURE, GRP_FUTURE]:
        k0, k1, k2 = keys
        vals.append(fn(rec_avg[k0][k1][k2]) if fn else rec_avg[k0][k1][k2])
        rawvals.append(rec_avg[k0][k1][k2])

  return vals,rawvals

def print_oair_line(bench, items, outf=sys.stdout):
  print >> outf, ("%s" % bench).ljust(30),
  for scfg,val in items:
    print_oat_item(scfg, val),
  print >> outf, ""

def print_oavr_line(head, items, agg=False, outf=sys.stdout):
  print >> outf, ("%s" % head).ljust(5),
  for scfg,val in items:
    if agg:
      print_oat_agg_item(scfg, val),
    else:
      print_oat_item(scfg, val),
  print >> outf, ""

def get_oav_items(val, statcfg):

  items = []
  for sc in statcfg:
    keys, fn = sc[4:6]
    if len(keys) == 1:
      items.append(fn(val[keys[0]]) if fn else val[keys[0]])
    elif keys[0] in [VAL_HISTORY, VAL_FUTURE, GRP_FUTURE]:
      k0, k1, k2 = keys
      items.append(fn(val[k0][k1][k2]) if fn else val[k0][k1][k2])
    else:
      print "malformed statcfg: ", sc
      raise SystemExit(1)

  return items

def get_agg_oav_items(oainfo, aggkey, statcfg):

  items = []
  for sc in statcfg:
    keys, fn, doagg = sc[4:7]
    if doagg:
      if len(keys) == 1:
        try:
          items.append((fn(oainfo[aggkey][keys[0]]) \
                        if fn else oainfo[aggkey][keys[0]]))
        except KeyError:
          items.append(None)
      elif keys[0] in [VAL_HISTORY, VAL_FUTURE, GRP_FUTURE]:
        k0, k1, k2 = keys
        try:
          items.append((fn(oainfo[aggkey][k0][k1][k2]) \
                        if fn else oainfo[aggkey][k0][k1][k2]))
        except KeyError:
          items.append(None)
      else:
        print "malformed statcfg: ", sc
        raise SystemExit(1)
    else:
      items.append(None)

  return items

def objAddrValReport(oainfo, statcfg=oavrcfg, outf=sys.stdout):

  print >> outf, "Object Address Info Val Report"
  print >> outf, "val".ljust(5),

  for item in statcfg:
    print_oat_header(item, outf)
  print >> outf, ""

  vals = []
  for i,val in enumerate(oainfo[VALS]):
    items = get_oav_items(val, statcfg)
    print_oavr_line(str(i), zip(statcfg, items), outf=outf)
    vals.append(items)

  if vals:
    avgs  = get_agg_oav_items(oainfo, AVERAGE, statcfg)
    maxs  = get_agg_oav_items(oainfo, MAX,     statcfg)
    mins  = get_agg_oav_items(oainfo, MIN,     statcfg)
    wgts  = get_agg_oav_items(oainfo, WGT_AVG, statcfg)
    sums  = get_agg_oav_items(oainfo, SUM_AGG, statcfg)

    print_oavr_line("avg", zip(statcfg, avgs), agg=True, outf=outf)
    print_oavr_line("max", zip(statcfg, maxs), agg=True, outf=outf)
    print_oavr_line("min", zip(statcfg, mins), agg=True, outf=outf)
    print_oavr_line("wgt", zip(statcfg, wgts), agg=True, outf=outf)
    print_oavr_line("sum", zip(statcfg, sums), agg=True, outf=outf)

def objAddrInfoReport(oais, benches=dacapo_defs, cfg=ADDR_INFO_INTERVAL,
  iter=0, statcfg=oihotcfg, outf=sys.stdout):

  print >> outf, "Object Address Info Report"
  print >> outf, "bench".ljust(30),

  for item in statcfg:
    print_oat_header(item, outf)
  print >> outf, ""

  rawavgs = []
  for bench in benches:
    rec = oais[bench][cfg]
    vals, rawvals = get_oai_vals(statcfg, rec)
    print_oair_line(bench, zip(statcfg, vals), outf=outf)
    rawavgs.append(rawvals)

  if rawavgs:
    avgs = [] 
    for i,sc in enumerate(statcfg):
      raw = average( [x[i] for x in rawavgs] )
      avg = sc[5](raw) if sc[5] else raw
      avgs.append(avg)
    print_oair_line("*", zip(statcfg, avgs), outf=outf)

def pri(item, fmt, width, align=LEFT_ALIGN, outf=sys.stdout):
  if not align in [LEFT_ALIGN, RIGHT_ALIGN]:
    print "bad align: ", align
    raise SystemExit(1)

  if item == None:
    if align == LEFT_ALIGN:
      print >> outf, ("-").ljust(width),
    else:
      print >> outf, ("-").rjust(width),
  else:
    if align == LEFT_ALIGN:
      print >> outf, (fmt % item).ljust(width),
    else:
      print >> outf, (fmt % item).rjust(width),

def printKFRLine(items, outf=sys.stdout):
  print >> outf, ("%s"    % items[0]).ljust(40),  # klass
  print >> outf, ("%d"    % items[1]).ljust(7),   # bpi
  print >> outf, ("%5.4f" % items[2]).rjust(12),  # pack-size
  print >> outf, ("%5.4f" % items[3]).rjust(12),  # pack-align
  print >> outf, ("%d"    % items[4]).rjust(12),  # objects
  print >> outf, ("%d"    % items[5]).rjust(12),  # size(bytes)
  print >> outf, ("%5.4f" % items[6]).rjust(12),  # size(ratio)
  print >> outf, ("%d"    % items[7]).rjust(12),  # waste(bytes)
  print >> outf, ("%5.4f" % items[8]).rjust(12),  # waste(ratio)
  print >> outf, ("%d"    % items[9]).rjust(12),  # walgn(bytes)
  print >> outf, ("%5.4f" % items[10]).rjust(12), # walgn(ratio)

def printKFRLine(items, outf=sys.stdout):
  pri(items[0],  "%s",    40, align=LEFT_ALIGN,  outf=outf) # klass
  pri(items[1],  "%d",    7,  align=LEFT_ALIGN,  outf=outf) # bpi
  pri(items[2],  "%5.4f", 12, align=RIGHT_ALIGN, outf=outf) # pack-size
  pri(items[3],  "%5.4f", 12, align=RIGHT_ALIGN, outf=outf) # pack-align
  pri(items[4],  "%d",    12, align=RIGHT_ALIGN, outf=outf) # objects
  pri(items[5],  "%d",    12, align=RIGHT_ALIGN, outf=outf) # size(bytes)
  pri(items[6],  "%5.4f", 12, align=RIGHT_ALIGN, outf=outf) # size(ratio)
  pri(items[7],  "%d",    12, align=RIGHT_ALIGN, outf=outf) # waste(bytes)
  pri(items[8],  "%5.4f", 12, align=RIGHT_ALIGN, outf=outf) # waste(ratio)
  pri(items[9],  "%d",    12, align=RIGHT_ALIGN, outf=outf) # walgn(bytes)
  pri(items[10], "%5.4f", 12, align=RIGHT_ALIGN, outf=outf) # walgn(ratio)
  print >> outf, ""

def klassFieldReport(ofinfo=None, bench=None, cfg=FIELD_TLAB_INTERVAL, \
  iter=0, clean=False, cutoff=30, vrange=None, per_inst=False, outf=sys.stdout):

  if bench is None:
    print "need to specify a bench"
    return

  if not ofinfo:
    ofinfo = getObjFieldInfo(bench=bench, cfg=cfg, iter=iter, clean=clean)

  xrange = range(vrange) if not vrange == None else range(len(ofinfo[VALS]))
  aggrec = getAggFieldInfo(ofinfo, xrange)
  duration = sum ([ofinfo[VALS][i][DURATION] for i in xrange])

  print >> outf, ("Klass Field Report for %s-%s, " % (bench, cfg))
  print >> outf, "Aggregated over ",
  print >> outf, ("vrange: {%d, %d}, " % vrange) if vrange else "entire run, ",
  print >> outf, "duration: %d ms" % duration
  print >> outf, ""

  print >> outf, "klass".ljust(40),
  print >> outf, "bpi".ljust(7),
  print >> outf, "pack-size".rjust(12),
  print >> outf, "pack-align".rjust(12),
  print >> outf, "objects".rjust(12),
  print >> outf, "size(bytes)".rjust(12),
  print >> outf, "size(ratio)".rjust(12),
  print >> outf, "waste(bytes)".rjust(12),
  print >> outf, "waste(ratio)".rjust(12),
  print >> outf, "walgn(bytes)".rjust(12),
  print >> outf, "walgn(ratio)".rjust(12),
  #print >> outf, "inst-ratio".rjust(12),
  #print >> outf, "ref-ratio".rjust(12),
  #print >> outf, "fref-ratio".rjust(12),
  print >> outf, ""

  sorted_keys = sorted( [ (k, aggrec[k][BYTES_WASTED_ALIGN]) for k in aggrec.keys()\
                          if not k == ALL_KLASSES and (aggrec[k][INSTANCE_SIZE] > 0) ],
                        key=itemgetter(1), reverse=True )
  keys = [x[0] for x in sorted_keys]

  base_size = aggrec[ALL_KLASSES][INS_SIZE] if per_inst else aggrec[ALL_KLASSES][TOTAL_SIZE]
                  
  aggs = []
  for i,key in enumerate(keys):
    klass_rec = aggrec[key]

    pack_size  = float(klass_rec[PACKED_SIZE])       / klass_rec[INSTANCE_SIZE]
    pack_align = float(klass_rec[PACKED_SIZE_ALIGN]) / klass_rec[INSTANCE_SIZE]

    size_ratio  = waste_ratio = walgn_ratio = 0.
    if base_size > 0.0:
      size_ratio  = float(klass_rec[TOTAL_SIZE])         / base_size
      waste_ratio = float(klass_rec[BYTES_WASTED])       / base_size
      walgn_ratio = float(klass_rec[BYTES_WASTED_ALIGN]) / base_size

    total_ratio = float(klass_rec[TOTAL_SIZE]) / aggrec[ALL_KLASSES][TOTAL_SIZE]

    if i < cutoff:
      printKFRLine((klass_rec[KLASS_NAME][:38], klass_rec[INSTANCE_SIZE], pack_size,
                    pack_align, klass_rec[TOTAL_OBJECTS], klass_rec[TOTAL_SIZE],
                    size_ratio, klass_rec[BYTES_WASTED], waste_ratio,
                    klass_rec[BYTES_WASTED_ALIGN], walgn_ratio))

    aggs.append((klass_rec[INSTANCE_SIZE], pack_size, pack_align,
                 klass_rec[TOTAL_OBJECTS], klass_rec[TOTAL_SIZE], size_ratio,
                 klass_rec[BYTES_WASTED], waste_ratio,
                 klass_rec[BYTES_WASTED_ALIGN], walgn_ratio))


  avg_ins_size      = average ( [ kr[0] for kr in aggs ] )
  avg_pack_size     = average ( [ kr[1] for kr in aggs ] )
  avg_pack_align    = average ( [ kr[2] for kr in aggs ] )
  avg_total_objects = average ( [ kr[3] for kr in aggs ] )
  avg_total_size    = average ( [ kr[4] for kr in aggs ] )
  avg_size_ratio    = average ( [ kr[5] for kr in aggs ] )

  printKFRLine(("per-klass-avg", avg_ins_size, avg_pack_size, avg_pack_align,
                avg_total_objects, avg_total_size, avg_size_ratio, None, None,
                None, None))

  total_objects     = sum ( [ kr[3] for kr in aggs ] )

  total_size        = sum ( [ kr[4] for kr in aggs ] )
  total_size_ratio  = float(total_size)  / base_size

  total_waste       = sum ( [ kr[6] for kr in aggs ] )
  total_waste_ratio = float(total_waste) / base_size

  total_walgn       = sum ( [ kr[8] for kr in aggs ] )
  total_walgn_ratio = float(total_walgn) / base_size

  printKFRLine(("totals", None, None, None, total_objects, total_size,
                total_size_ratio, total_waste, total_waste_ratio, total_walgn,
                total_walgn_ratio))

def printKISLine(items, outf=sys.stdout):
  #print >> outf, "".ljust(2),
  print >> outf, ("%s"    % items[0]).ljust(30),  # bench
  print >> outf, ("%d"    % items[1]).rjust(12),  # size (KB)
  print >> outf, ("%d"    % items[2]).rjust(12),  # ins  (KB)
  print >> outf, ("%5.4f" % items[3]).rjust(12),  # ins  ratio
  print >> outf, ("%d"    % items[4]).rjust(12),  # arr  (KB)
  print >> outf, ("%5.4f" % items[5]).rjust(12),  # arr  ratio
  print >> outf, ("%d"    % items[6]).rjust(12),  # vm   (KB)
  print >> outf, ("%5.4f" % items[7]).rjust(12),  # vm   ratio
  if items[8] > 0 or items[9] > 0:
    print >> outf, ("%d"    % items[8]).rjust(12),  # fill (KB)
    print >> outf, ("%5.4f" % items[9]).rjust(12),  # fill ratio
#  print >> outf, ("%d"    % items[10]).rjust(12), # hot-ins (KB)
#  print >> outf, ("%5.4f" % items[11]).rjust(12), # hot-ins ratio
#  print >> outf, ("%d"    % items[12]).rjust(12), # hot-arr (KB)
#  print >> outf, ("%5.4f" % items[13]).rjust(12), # hot-arr ratio
  print >> outf, ""

def klassInfoSummary(ofis=None, benches=dacapo_defs, cfg=FIELD_TLAB_INTERVAL,
  iter=0, clean=False, filler=False, outf=sys.stdout):

  if not ofis:
    ofis = getOFIs(benches=benches, cfg=cfg, iter=iter, clean=clean)

  print >> outf, "Klass Info Summary"

  #print >> outf, "".ljust(2),
  print >> outf, "bench".ljust(30),
  print >> outf, "size (KB)".rjust(12),
  print >> outf, "ins  (KB)".rjust(12),
  print >> outf, "ins  ratio".rjust(12),
  print >> outf, "arr  (KB)".rjust(12),
  print >> outf, "arr  ratio".rjust(12),
  print >> outf, "vm   (KB)".rjust(12),
  print >> outf, "vm   ratio".rjust(12),
  if filler:
    print >> outf, "fill (KB)".rjust(12),
    print >> outf, "fill ratio".rjust(12),
#  print >> outf, "hot-ins (KB)".rjust(12),
#  print >> outf, "hot-ins-rat".rjust(12),
#  print >> outf, "hot-arr (KB)".rjust(12),
#  print >> outf, "hot-arr-rat".rjust(12),
  print >> outf, ""

  avgs = []
  for bench in benches:

    rec = ofis[bench][cfg][AGGREGATE]
    
    total_size   = (rec[ALL_KLASSES][TOTAL_SIZE] >> 10) if filler else \
                   (rec[ALL_KLASSES][NONFILL_SIZE] >> 10)

    ins_size     = (rec[KT_TOTALS][KT_APP_INSTANCE][TOTAL_SIZE] >> 10)
    ins_ratio    = 0. if total_size == 0 else (float(ins_size) / total_size)

    arr_size     = (rec[KT_TOTALS][KT_APP_ARRAY][TOTAL_SIZE] >> 10)
    arr_ratio    = 0. if total_size == 0 else (float(arr_size) / total_size)

    filler_size  = -1
    filler_ratio = -1
    if filler:
      filler_size  = (rec[KT_TOTALS][KT_VM_FILLER][TOTAL_SIZE] >> 10)
      filler_ratio = 0. if total_size == 0 else (float(filler_size) / total_size)

    vm_size     = ( (rec[KT_TOTALS][KT_VM_INSTANCE][TOTAL_SIZE] + \
                     rec[KT_TOTALS][KT_VM_ARRAY][TOTAL_SIZE]    + \
                     rec[KT_TOTALS][KT_VM_OTHER][TOTAL_SIZE]) >> 10)
    vm_ratio    = 0. if total_size == 0 else (float(vm_size) / total_size)

    printKISLine((bench, total_size, ins_size, ins_ratio, arr_size, arr_ratio,
                  vm_size, vm_ratio, filler_size, filler_ratio), outf=outf)
    avgs.append((total_size, ins_size, ins_ratio, arr_size, arr_ratio,
                 vm_size, vm_ratio, filler_size, filler_ratio))

  if avgs:

    total_size   = average( [ x[0] for x in avgs ] )
    ins_size     = average( [ x[1] for x in avgs ] )
    ins_ratio    = average( [ x[2] for x in avgs ] )
    arr_size     = average( [ x[3] for x in avgs ] )
    arr_ratio    = average( [ x[4] for x in avgs ] )
    vm_size      = average( [ x[5] for x in avgs ] )
    vm_ratio     = average( [ x[6] for x in avgs ] )
    filler_size  = average( [ x[7] for x in avgs ] )
    filler_ratio = average( [ x[8] for x in avgs ] )

    printKISLine(("*", total_size, ins_size, ins_ratio, arr_size, arr_ratio,
                  vm_size, vm_ratio, filler_size, filler_ratio), outf=outf)


# not really sure how useful this report is -- I'm taking it out now because I
# think it may be incorrect / misleading.
#
#def printOFIRLine(items, outf=sys.stdout):
#  #print >> outf, "".ljust(2),
#  print >> outf, ("%s"    % items[0]).ljust(30),  # bench
#  print >> outf, ("%d"    % items[1]).ljust(9),   # nvals
#  print >> outf, ("%d"    % items[2]).ljust(12),  # dur (ms)
#  print >> outf, ("%d"    % items[3]).rjust(12),  # size (KB)
#  print >> outf, ("%d"    % items[4]).rjust(12),  # hot (KB)
#  print >> outf, ("%5.4f" % items[5]).rjust(12),  # hot ratio
#  print >> outf, ("%d"    % items[6]).rjust(12),  # ins (KB)
#  print >> outf, ("%5.4f" % items[7]).rjust(12),  # ins ratio
#  print >> outf, ("%d"    % items[8]).rjust(12),  # arr (KB)
#  print >> outf, ("%5.4f" % items[9]).rjust(12),  # arr ratio
##  print >> outf, ("%d"    % items[10]).rjust(12), # hot-ins (KB)
##  print >> outf, ("%5.4f" % items[11]).rjust(12), # hot-ins ratio
##  print >> outf, ("%d"    % items[12]).rjust(12), # hot-arr (KB)
##  print >> outf, ("%5.4f" % items[13]).rjust(12), # hot-arr ratio
#  print >> outf, ""
#
#def instanceInfoReport(ofis=None, benches=dacapo_defs, cfg=FIELD_TLAB_INTERVAL,
#  iter=0, clean=False, wgt=False, outf=sys.stdout):
#
#  if not ofis:
#    ofis = getOFIs(benches=benches, cfg=cfg, iter=iter, clean=clean)
#
#  print >> outf, "Object Field Totals Report"
#
#  #print >> outf, "".ljust(2),
#  print >> outf, "bench".ljust(30),
#  print >> outf, "nvals".ljust(9),
#  print >> outf, "dur (ms)".ljust(12),
#  print >> outf, "size (KB)".rjust(12),
#  print >> outf, "hot (KB)".rjust(12),
#  print >> outf, "hot ratio".rjust(12),
#  print >> outf, "ins (KB)".rjust(12),
#  print >> outf, "ins ratio".rjust(12),
#  print >> outf, "arr (KB)".rjust(12),
#  print >> outf, "arr ratio".rjust(12),
##  print >> outf, "hot-ins (KB)".rjust(12),
##  print >> outf, "hot-ins-rat".rjust(12),
##  print >> outf, "hot-arr (KB)".rjust(12),
##  print >> outf, "hot-arr-rat".rjust(12),
#  print >> outf, ""
#
#  avgs = []
#  for bench in benches:
#
#    rec = ofis[bench][cfg]
#
#    if wgt:
#      rec_avgs = rec[WGT_AVG]
#    else:
#      rec_avgs = rec[AVERAGE]
#
#    nvals     = len(rec[VALS])
#    dur       = rec_avgs[DURATION]
#    live_size = (int(rec_avgs[LIVE_SIZE]) >> 10)
#    hot_size  = (int(rec_avgs[HOT_SIZE]) >> 10)
#    hot_ratio = rec_avgs[HOT_SIZE_RATIO]
#    ins_size  = (int(rec_avgs[INS_SIZE]) >> 10)
#    ins_ratio = rec_avgs[INS_SIZE_RATIO]
#    arr_size  = (int(rec_avgs[ARR_SIZE]) >> 10)
#    arr_ratio = rec_avgs[ARR_SIZE_RATIO]
#
#    hot_ins_size  = (int(rec_avgs[HOT_INS_SIZE]) >> 10)
#    hot_ins_ratio = rec_avgs[HOT_INS_SIZE_RATIO]
#    hot_arr_size  = (int(rec_avgs[HOT_ARR_SIZE]) >> 10)
#    hot_arr_ratio = rec_avgs[HOT_ARR_SIZE_RATIO]
#
#    printOFIRLine((bench, nvals, dur, live_size, hot_size, hot_ratio,
#                   ins_size, ins_ratio, arr_size, arr_ratio), outf=outf)
##    printOFIRLine((bench, nvals, dur, live_size, hot_size, hot_ratio,
##                   ins_size, ins_ratio, arr_size, arr_ratio, hot_ins_size,
##                   hot_ins_ratio, hot_arr_size, hot_arr_ratio), outf=outf)
#
#    avgs.append((nvals, dur, live_size, hot_size, hot_ratio, ins_size,
#                 ins_ratio, arr_size, arr_ratio, hot_ins_size,
#                 hot_ins_ratio, hot_arr_size, hot_arr_ratio))
#
#  if avgs:
#
#    nvals     = average( [ x[0] for x in avgs ] )
#    dur       = average( [ x[1] for x in avgs ] )
#
#    live_size = average( [ x[2] for x in avgs ] )
#    hot_size  = average( [ x[3] for x in avgs ] )
#    hot_ratio = average( [ x[4] for x in avgs ] )
#    ins_size  = average( [ x[5] for x in avgs ] )
#    ins_ratio = average( [ x[6] for x in avgs ] )
#    arr_size  = average( [ x[7] for x in avgs ] )
#    arr_ratio = average( [ x[8] for x in avgs ] )
#
#    hot_ins_size  = average( [ x[9] for x in avgs ] )
#    hot_ins_ratio = average( [ x[10] for x in avgs ] )
#    hot_arr_size  = average( [ x[11] for x in avgs ] )
#    hot_arr_ratio = average( [ x[12] for x in avgs ] )
#
#    printOFIRLine(("*", nvals, dur, live_size, hot_size, hot_ratio,
#                   ins_size, ins_ratio, arr_size, arr_ratio), outf=outf)
##    printOFIRLine(("*", nvals, dur, live_size, hot_size, hot_ratio,
##                   ins_size, ins_ratio, arr_size, arr_ratio, hot_ins_size,
##                   hot_ins_ratio, hot_arr_size, hot_arr_ratio), outf=outf)

