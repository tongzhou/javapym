#############################################################################
# pyzephyr utility functions
#############################################################################

import os
import errno
import sys
from lib.globals import *
from subprocess import Popen, STDOUT, call, PIPE
from numpy import average, std, median, float64, empty, array
from pickle import load,dump
from shutil import rmtree,copyfile,copytree,move
from operator import itemgetter
from math import fabs, floor, ceil
from copy import deepcopy
from time import strptime, mktime, ctime
from struct import pack, unpack, calcsize
from itertools import tee, izip
from multiprocessing import Pool, Manager
from random import Random
import tarfile
import shelve
import gc
import cStringIO

def getcpu():
  p = Popen ("uname -a", shell=True, stdout=PIPE)
  os.waitpid(p.pid,0)
  return p.stdout.read().split()[1]
  
def mkdir_p(path):
  try:
    os.makedirs(path)
  except OSError as exc:
    if exc.errno == errno.EEXIST:
      pass
    else: raise

def rm_f(path):
  try:
    os.remove(path)
  except OSError as exc:
    if exc.errno == errno.EEXIST:
      pass
    else: raise

def rm_rf(path):
  try:
    rmtree(path)
  except OSError as exc:
    if exc.errno == errno.ENOENT:
      pass
    else: raise

def getdir(dir, create=False, clean=False):
  if clean:
    os.system("rm -rf %s" % dir)
  if create:
    mkdir_p(dir)
  return dir

def needRootAccess(cmd):
  if cmd.startswith('sudo') \
    or cmd.startswith('numactl') \
    or cmd.startswith('mcolorctl') \
    or cmd.startswith('mscramble'):
    return True
  return False

def execmd(cmd, outfile=None, wait=True, cutoff=None, env=None, \
           shell=False, dotime=False, verbose=V1):

  if needRootAccess(cmd) and not rootAccess:
    if verbose > V1:
      print "warning: no root access, ignoring \"%s\"" % cmd
    return None

  if verbose > V1:
    print cmd 

  if shell:
    if dotime:
      cmd = (unixtimecmd + " -f " + unixtimefmt + " " + cmd)
    p = Popen(cmd, env=env, shell=True)

  else:
    cmdpts = []
    if dotime:
      cmdpts += ([unixtimecmd] + ["-f"] + [unixtimefmt])
    cmdpts += cmd.split()

    if outfile:
      with open(outfile,'w') as outf:
        p = Popen(cmdpts, stdout=outf, stderr=STDOUT, env=env, \
                  shell=False)
    else:
      p = Popen(cmdpts, env=env, shell=False)

#  if shell:
#    p = Popen(cmd, env=env, shell=True)
#  elif not outfile:
#    p = Popen(cmd.split(), env=env, shell=shell)
#  else:
#    with open(outfile,'w') as outf:
#      p = Popen(cmd.split(), stdout=outf, stderr=STDOUT, env=env, \
#                shell=shell)

  if wait:
    if cutoff:
      wait_cutoff(p, cutoff)
    else:
      os.waitpid(p.pid,0)
  else:
    bgprocs.append(p)

  if bgprocs:
    clearBGProcs()

  return p

def geomean(nums):
  #print " nums: ", nums
  #print ""
  mynums = [ (n+1.0) for n in nums \
             if isinstance(n, (int, long, float, complex)) ]
  #print " mynums: ", mynums
  #return (reduce(lambda x, y: x*y, mynums))**(1.0/len(nums))
  gm = (reduce(lambda x, y: x*y, mynums))**(1.0/len(mynums))
  #print gm
  return (gm-1.0)

def expiter(benches=jvm2008s, cfgs=defcfgs):
  for bench in benches:
    for cfg in cfgs:
      yield (bench, cfg)

def expiteriter(benches=jvm2008s, cfgs=defcfgs, iters=defiters):
  for bench in benches:
    for cfg in cfgs:
      for i in range(iters):
        yield (bench, cfg, i)

def rmoutliers(samples):

  mysamples = deepcopy(samples)
  outs = []
  for i,isample in enumerate(samples):
    # this is a hack -- should never have valid values over 1000.0
    if isample > 1000.0:
      mysamples.remove(isample)
      outs.append(isample)
  return (mysamples, outs)
#  savg = average(samples)
#  sstd = std(samples)
#
#  mysamples = samples
#  outs = []
#  for i,isample in enumerate(mysamples):
#    zi = fabs(  ((isample-savg) / sstd) )
#    if zi > 2.95:
#      mysamples.remove(isample)
#      outs.append(isample)
#  return (mysamples, outs)

def getInvocationCounts(bench, cfg, iter=0):

  try:
    rawfile = rawoutfile(bench, cfg, iter)
    rawf    = open(rawfile)
  except IOError:
    print "no output file: ", rawfile
    return []
 
  methods = []

  # for the whole file
  for line in rawf:

    # scan to start of histogram
    if not histoHeaderRE.match(line):
      continue

    for line in rawf:
  
      # a newline marks the end of the histogram
      if line == '\n':
        break
  
      # some methods are qualified - get rid of the qualifier
      for pref in mprefs:
        if line.startswith(pref):
          line = line[len(pref):]
  
      method = line.strip() 

      # read method lines
      icnt = bcnt = None
      for mline in rawf:
        if icntRE.match(mline):
          icnt = long(mline.split()[1])
        if bcntRE.match(mline):
          bcnt = long(mline.split()[1])
        if compRE.match(mline):
          break

      methods.append((method,icnt,bcnt))
  return methods

def getUnixTime(bench, cfg, ix, rawtype=None):

  #print bench, cfg, ix
  try:
    rawfile = rawoutfile(bench, cfg, ix, type=rawtype)
    rawf    = open(rawfile)
  except IOError:
    #print "no output file: ", rawfile
    return (None, None)

  if not bench in customs and not bench in allowFails:
    if hadFatalError(bench,cfg,ix,rawtype=rawtype) is True:
      return (None, None)

  for line in rawf:
    if unixTimeRE.match(line):
      elapsed = float(line.split()[1])
      # bug in GNU time makes times from /usr/bin/time four times too large
      rss     = int(float(line.split()[3]) / 4.0)
      return (elapsed,rss)

  return (None,None)

def getUnixTimes(bench, cfg, iters=defiters):
  return [ x for x in [ getUnixTime(bench, cfg, i) for i in range(iters) ] \
           if type(x[0]) is float and type(x[1]) is int ]

def getUnixTimeStats(bench, cfg, iters=defiters, miniters=defminiters):
  times = getUnixTimes(bench, cfg, iters=iters)
  elapseds = [ x[0] for x in times ]
  rsss     = [ x[1] for x in times ]
  if len(times) >= miniters:
    estats = {}
    estats[MEAN]  = average(elapseds)
    estats[STDEV] = std(elapseds)

    mstats = {}
    mstats[MEAN]  = average(rsss)
    mstats[STDEV] = std(rsss)

    return (estats, mstats)
  return None

def hadFatalError(bench, cfg, iter, rawtype=None):
  try:
    rawfile = rawoutfile(bench, cfg, iter, type=rawtype)
    rawf    = open(rawfile)
  except IOError:
    print "no output file: ", rawfile
    return None

  for line in rawf:
    if fatalErrorRE.match(line):
      return True
  return False

def getHarnessTime(bench, cfg, iter, benchiters=startiters, quiet=False):
  try:
    rawfile = rawoutfile(bench, cfg, iter)
    rawf    = open(rawfile)
  except IOError:
    if not quiet:
      print "no output file: ", rawfile
    return None

  if bench in bw_benches or bench in simplebb_benches:
    for line in rawf:
      if bwbIterRE.match(line):
        try:
          dur   = line.split()[3]
          iters = line.split()[5]
          #return ((float(dur)/1000) / float(iters))
          return ((float(dur)) / float(iters))
        except ValueError:
          return " ".join(line.split()[3:])

  if bench in jvm2008s:
    for line in rawf:
      if jvm2008TimeRE.match(line):
        try:
          return (60.0 / float(line.split()[3]))
        except ValueError:
          return " ".join(line.split()[3:])
  elif bench in dacapos:
    vals  = []
    start = None
    for line in rawf:
#      if bench.startswith('h2'):
#        if dacapoIterStartRE.match(line):
#          start   = int(line.split()[3])
#        elif dacapoIterEndRE.match(line):
#          end     = int(line.split()[3])
#          elapsed = (float(end - start) / 1000.0)
#          vals.append(elapsed)
#          start   = None
#      else:
      if dacapoTimeRE.match(line):
        try:
          vals.append(((float(line.split()[8])) / 1000.0))
        except ValueError:
          print "error parsing %s" % rawfile
          print "bad result line: %s" % line
          raise SystemExit(1)
      elif dacapoPassedRE.match(line):
        try:
          vals.append(((float(line.split()[6])) / 1000.0))
        except ValueError:
          print "error parsing %s" % rawfile
          print "bad result line: %s" % line
          raise SystemExit(1)

    myvals = []
    for bi in benchiters:
      try:
        myvals.append(vals[bi-1])
      except:
        return None
    return float(median(myvals)) if vals else None
    #return float(min(myvals)) if vals else None
    # uncomment this if you want to use a warmup run
    #return float(median(vals[1])) if vals else None
    #return float(median(vals[2:])) if vals else None
    #return float(median(vals)) if vals else None
  elif bench in jgf_singles or bench in jgf_multis:
    for line in rawf:
      if jgfTimeRE.match(line):
        pts    = line.split()
        ncalls = int(pts[2])
        time   = float(pts[4])
        return (time/ncalls)

  return None

def getHarnessTimes(bench, cfg, iters=defiters, benchiters=startiters, quiet=False):
  return [ getHarnessTime(bench, cfg, i, benchiters=benchiters, quiet=quiet) \
           for i in range(iters) if type(getHarnessTime(bench, cfg, i, \
                                         benchiters=benchiters, quiet=quiet)) is float ]

def getHarnessTimeStats(bench, cfg, iters=defiters, miniters=defminiters, \
                        benchiters=startiters, quiet=False):
  times = [ x for x in getHarnessTimes(bench, cfg, iters=iters,
                                       benchiters=benchiters, quiet=quiet) \
           if type(x) is float ]
  if len(times) >= miniters:
    stats = {}
    stats[MEAN]  = average(times)
    stats[STDEV] = std(times)
    return stats
  return None

def getHarnessTimePar(cfg, iter):
  runs     = parcfgs[cfg]
  runtimes = []
  for run,(bench,runcfg,profcfg) in enumerate(runs):
    try:
      rawfile = rawoutfilepar(cfg, iter, run)
      rawf    = open(rawfile)
    except IOError:
      print "no output file: ", rawfile
      return None

    if bench in jvm2008s:
      for line in rawf:
        if jvm2008TimeRE.match(line):
          try:
            runtimes.append(60.0 / float(line.split()[3]))
          except ValueError:
            return " ".join(line.split()[3:])
    elif bench in dacapos:
      for line in rawf:
        if dacapoTimeRE.match(line):
          try:
            runtimes.append((float(line.split()[6])) / 1000.0)
          except ValueError:
            print "error parsing %s" % rawfile
            print "bad result line: %s" % line
            raise SystemExit(1)

  return float(average(runtimes))

def getHarnessTimesPar(cfg, iters=defiters):
  return [ getHarnessTimePar(cfg, i) for i in range(iters) \
           if type(getHarnessTimePar(cfg, i)) is float ]

def getHarnessTimeStatsPar(cfg, iters=defiters, miniters=defminiters):
  times = [ x for x in getHarnessTimesPar(cfg, iters=iters) if type(x) is float ]
  if len(times) >= miniters:
    stats = {}
    stats[MEAN]  = average(getHarnessTimesPar(cfg, iters=iters))
    stats[STDEV] = std(getHarnessTimesPar(cfg, iters=iters))
    return stats
  return None

def getThreadTimes(bench, cfg, iter):

  try:
    rawfile = rawoutfile(bench, cfg, iter)
    rawf    = open(rawfile)
  except IOError:
    print "no output file: ", rawfile
    return None

  ttimes  = {}
  curiter = 0
  for line in rawf:

    if preIterRE.match(line):
      curiter += 1

      for tline in rawf:
        if threadTimeRE.match(tline):
          pts = tline.split()
          tid = int(pts[1])
          if not ttimes.has_key(tid):
            ttimes[tid] = {}
            ttimes[tid][TYPE]  = pts[3]
            ttimes[tid][ITERS] = [ None for x in range(curiter-1) ]
          ttimes[tid][ITERS].append(None)
          ttimes[tid][ITERS][-1] = int(pts[6])
        else:
          break

    elif postIterRE.match(line):

      for tline in rawf:
        if threadTimeRE.match(tline):
          pts = tline.split()
          tid = int(pts[1])
          if not ttimes.has_key(tid):
            ttimes[tid] = {}
            ttimes[tid][TYPE]  = pts[3]
            ttimes[tid][ITERS] = [ None for x in range(curiter) ]
          if ttimes[tid][ITERS][curiter-1] == None:
            ttimes[tid][ITERS][curiter-1] = int(pts[6])
          else:
            start_time = ttimes[tid][ITERS][curiter-1]
            ttimes[tid][ITERS][curiter-1] = (int(pts[6]) - start_time)
        else:
          break
  return ttimes

def getThreadTypesTime(bench, cfg, iter, benchiters, ttypes):
  ttimes = getThreadTimes(bench, cfg, iter)

  time = 0
  tids = [ x for x in ttimes.keys() if ttimes[x][TYPE] in ttypes ]
  for tid in tids:
    for bi in benchiters:
      tt = ttimes[tid][ITERS][bi-1]
      if tt != None:
        time += tt
  return time

def getAppTime(bench, cfg, iter, benchiters):
  return float(getThreadTypesTime(bench, cfg, iter, benchiters, [JavaThread, VMThread]))

def getAppTimeMillis(bench, cfg, iter, benchiters):
  return getAppTime(bench, cfg, iter, benchiters) / 1000000

def getAppThreadTimes(bench, cfg, iters=defiters, benchiters=startiters):
  return [ getAppTimeMillis(bench, cfg, i, benchiters) for i in range(iters) \
           if type(getAppTimeMillis(bench, cfg, i, benchiters)) is float ]

def getAppThreadTimeStats(bench, cfg, iters=defiters, miniters=defminiters, \
                          benchiters=startiters):
  times = [ x for x in getAppThreadTimes(bench, cfg, iters=iters, \
                                         benchiters=benchiters) \
           if type(x) is float ]
  if len(times) >= miniters:
    stats = {}
    stats[MEAN]  = average(times)
    stats[STDEV] = std(times)
    return stats
  return None

def getCompTime(bench, cfg, iter, benchiters):
  return float(getThreadTypesTime(bench, cfg, iter, benchiters, [CompilerThread]))

def getCompTimeMillis(bench, cfg, iter, benchiters):
  return getCompTime(bench, cfg, iter, benchiters) / 1000000

def getCompileThreadTimes(bench, cfg, iters=defiters, benchiters=startiters):
  return [ getCompTimeMillis(bench, cfg, i, benchiters) for i in range(iters) \
           if type(getCompTimeMillis(bench, cfg, i, benchiters)) is float ]

def getCompileThreadTimeStats(bench, cfg, iters=defiters, miniters=defminiters, \
                              benchiters=startiters):
  times = [ x for x in getCompileThreadTimes(bench, cfg, iters=iters, \
                                             benchiters=benchiters) \
           if type(x) is float ]
  if len(times) >= miniters:
    stats = {}
    stats[MEAN]  = average(times)
    stats[STDEV] = std(times)
    return stats
  return None

# currently we just get the aggregate value for the summary view - we can and
# probably should extend this in the near future
#
def getEmonMetric(bench, cfg, iter, metric):
  try:
    esvfile = emonSummaryViewFile(bench, cfg, iter)
    esvf    = open(esvfile)
  except IOError:
    print "no emon file: ", esvfile
    return None

  metricre = edprocMetricREs[metric]
  for line in esvf:
    if re.match(metricre, line):
      return float(line.split(',')[1])

  return None

def getEmonMetrics(bench, cfg, metric, iters=defiters):
  return [ getEmonMetric(bench, cfg, i, metric) for i in range(iters) \
           if type(getEmonMetric(bench, cfg, i, metric)) is float ]

def getEmonMetricStats(bench, cfg, metric, iters=defiters, miniters=defminiters):
  times = [ x for x in getEmonMetrics(bench, cfg, metric, iters=iters) \
            if type(x) is float ]
  if len(times) >= miniters:
    stats = {}
    stats[MEAN]  = average(getEmonMetrics(bench, cfg, metric, iters=iters))
    stats[STDEV] = std(getEmonMetrics(bench, cfg, metric, iters=iters))
    return stats
  return None

def getPGovMetricVals(bench, cfg, iter, metric, pkg=1, rate=100):
  try:
    pgovfile = pgovRawFile(bench, cfg, iter)
    pgovf    = open(pgovfile)
  except IOError:
    print "no pgov file: ", pgovfile
    return None

  # get column corresponding to metric and pkg
  match  = "%s_%d" % (metric.split("_")[1], pkg)
  header = pgovf.readline()
  col    = None
  for i,m in enumerate(header.split()):
    if match == m:
      col = i
      break

  if col is None:
    print "invalid (metric,pkg): %s_%d" % (metric, pkg)
    return None

  vals = []
  prev_time = None
  for line in pgovf:
    # last line can fail
    try:
#      cur_time = long(line.split()[0])
#      if not prev_time is None and ((cur_time - prev_time) > (1.5*rate)):
#        prev_time = cur_time
#        continue
#      vals.append(float(line.split()[col]))
#      prev_time = cur_time
      cur_time = long(line.split()[0])
      val      = float(line.split()[col])
      if not prev_time is None and ((cur_time - prev_time) > (1.9*rate)):
        len  = (cur_time - prev_time) / rate
        xval = val / float(len)
        for i in range(len):
          vals.append(xval)
      else:
        vals.append(float(line.split()[col]))
      prev_time = cur_time
    except:
      break
  return vals

def getPGovMetricMinMax(bench, cfg, iter, metric, pkg=1, fslice=20, bslice=20):

  vals = getPGovMetricVals(bench, cfg, iter, metric, pkg=pkg)
  vals = vals[fslice:][:-bslice]
  return (min(vals),max(vals))

def getPGovMetric(bench, cfg, iter, metric, pkg=1):

  vals = getPGovMetricVals(bench, cfg, iter, metric, pkg=pkg)
  return float(average(vals))

def getPGovMetrics(bench, cfg, metric, pkg=1, iters=defiters):
  return [ getPGovMetric(bench, cfg, i, metric) for i in range(iters) \
           if type(getPGovMetric(bench, cfg, i, metric)) is float ]

def getPGovMetricStats(bench, cfg, metric, pkg=1, iters=defiters, miniters=defminiters):
  vals = [ x for x in getPGovMetrics(bench, cfg, metric, pkg=pkg, iters=iters) \
            if type(x) is float ]
  if len(vals) >= miniters:
    stats = {}
    stats[MEAN]  = geomean(getPGovMetrics(bench, cfg, metric, pkg=pkg, iters=iters))
    stats[STDEV] = std(getPGovMetrics(bench, cfg, metric, pkg=pkg, iters=iters))
    return stats
  return None

def getEmonMetricPar(cfg, iter, metric):
  try:
    esvfile = emonSummaryViewFilePar(cfg, iter, 0)
    esvf    = open(esvfile)
  except IOError:
    print "no emon file: ", esvfile
    return None

  metricre = edprocMetricREs[metric]
  for line in esvf:
    if re.match(metricre, line):
      return float(line.split(',')[1])

  return None

def getEmonMetricsPar(cfg, metric, iters=defiters):
  return [ getEmonMetricPar(cfg, i, metric) for i in range(iters) \
           if type(getEmonMetricPar(cfg, i, metric)) is float ]

def getEmonMetricStatsPar(cfg, metric, iters=defiters, miniters=defminiters):
  times = [ x for x in getEmonMetricsPar(cfg, metric, iters=iters) \
            if type(x) is float ]
  if len(times) >= miniters:
    stats = {}
    stats[MEAN]  = average(getEmonMetricsPar(cfg, metric, iters=iters))
    stats[STDEV] = std(getEmonMetricsPar(cfg, metric, iters=iters))
    return stats
  return None

def getPGovMetricValsPar(cfg, iter, metric, pkg=1, rate=100):
  try:
    pgovfile = pgovRawFilePar(cfg, iter, 0)
    pgovf    = open(pgovfile)
  except IOError:
    print "no pgov file: ", pgovfile
    return None

  # get column corresponding to metric and pkg
  match  = "%s_%d" % (metric.lstrip("PGOV_"), pkg)
  header = pgovf.readline()
  col    = None
  for i,m in enumerate(header.split()):
    if match == m:
      col = i
      break

  if col is None:
    print "invalid (metric,pkg): %s_%d" % (metric, pkg)
    return None


  # sometimes, power_gov prints out lines with values aggregated for longer
  # intervals. this code attempts to correct that
  #
  vals = []
  prev_time = None
  for line in pgovf:
    try:
      cur_time = long(line.split()[0])
      if not prev_time is None and ((cur_time - prev_time) > (1.5*rate)):
        continue
      vals.append(float(line.split()[col]))
      prev_time = cur_time
#      cur_time = long(line.split()[0])
#      val      = float(line.split()[col])
#      if not prev_time is None and ((cur_time - prev_time) > (1.9*rate)):
#        len  = (cur_time - prev_time) / rate
#        xval = val / float(len)
#        for i in range(len):
#          vals.append(xval)
#      else:
#        vals.append(float(line.split()[col]))
#      prev_time = cur_time
    except:
      break
  return vals

def getPGovMetricPar(cfg, iter, metric, pkg=1, rate=defpowrate):
  vals = getPGovMetricValsPar(cfg, iter, metric, pkg=pkg, rate=rate)
  return float(average(vals))

def getPGovMetricsPar(cfg, metric, pkg=1, iters=defiters):
  return [ getPGovMetricPar(cfg, i, metric) for i in range(iters) \
           if type(getPGovMetricPar(cfg, i, metric)) is float ]

def getPGovMetricStatsPar(cfg, metric, pkg=1, iters=defiters, miniters=defminiters):
  vals = [ x for x in getPGovMetricsPar(cfg, metric, pkg=pkg, iters=iters) \
            if type(x) is float ]
  if len(vals) >= miniters:
    stats = {}
    stats[MEAN]  = geomean(getPGovMetricsPar(cfg, metric, pkg=pkg, iters=iters))
    stats[STDEV] = std(getPGovMetricsPar(cfg, metric, pkg=pkg, iters=iters))
    return stats
  return None

def newPcmPowerInfoRecord(chans, ranks):

  pcmrec = {}
  pcmrec[CKEOFF] = {}
  for chan in chans:
    if not pcmrec[CKEOFF].has_key(chan):
      pcmrec[CKEOFF][chan] = {}
    for rank in ranks:
      if not pcmrec[CKEOFF][chan].has_key(rank):
        pcmrec[CKEOFF][chan][rank] = None
  return pcmrec

def pcmPowerInfoVals(bench, cfg, iter, node, quiet=False):

  pcmvals = []
  chans   = range(0,4)
  ranks   = range(0,2)

  try:
    pcmf = try_open_read(pcmPowerFile(bench, cfg, iter), quiet=True)
  except IOError:
    return None

  # discard the first newval marker
  for line in pcmf:
    if powValRE.match(line):
      break

  currec = newPcmPowerInfoRecord(chans, ranks)
  for line in pcmf:

    if powValRE.match(line):
      pcmvals.append(currec)
      currec = newPcmPowerInfoRecord(chans, ranks)

    elif timeElapsedRE.match(line):
      currec[TIME_ELAPSED] = int(line.split()[2])

    elif chanInfoRE.match(line):
      pts = line.split()
      mynode = int(pts[0].split('CH')[0].strip('S'))
      if mynode == node:
        chan = int(pts[0].split('CH')[1].rstrip(';'))
        rank = int(pts[3].split('Rank')[1])
        res  = float(pts[7].rstrip(';%'))
        currec[CKEOFF][chan][rank] = res

    elif skInfoRE.match(line):
      pts = line.split()
      mynode = int(pts[0].lstrip('S').rstrip(';'))
      if mynode == node:
        currec[SOCK_WATTS] = float(pts[9].rstrip(';'))

    elif dramInfoRE.match(line):
      pts = line.split()
      mynode = int(pts[0].lstrip('S').rstrip(';'))
      if mynode == node:
        currec[DRAM_WATTS]  = float(pts[12])
        currec[DRAM_JOULES] = float(pts[9].rstrip(';'))

  return pcmvals

def newPcmSRInfoRecord(chans):

  pcmrec = {}

  pcmrec[DRAM_CLOCKS] = {}
  pcmrec[SR_CYCLES]   = {}
  pcmrec[PPD_CYCLES]  = {}
  pcmrec[PS_CYCLES]   = {}
  for chan in chans:
    pcmrec[DRAM_CLOCKS][chan] = 0
    pcmrec[SR_CYCLES][chan]   = 0
    pcmrec[PPD_CYCLES][chan]  = 0
    pcmrec[PS_CYCLES][chan]   = 0
  return pcmrec

def pcmSRInfoVals(bench, cfg, iter, node):

  pcmvals = []
  chans   = range(0,4)

  try:
    pcmf = try_open_read(pcmSelfRefreshFile(bench, cfg, iter), quiet=True)
  except IOError:
    return None

  # discard the first newval marker
  for line in pcmf:
    if powValRE.match(line):
      break

  currec = newPcmSRInfoRecord(chans)
  for line in pcmf:

    if powValRE.match(line):
      pcmvals.append(currec)
      currec = newPcmSRInfoRecord(chans)

    elif timeElapsedRE.match(line):
      currec[TIME_ELAPSED] = int(line.split()[2])

    elif chanInfoRE.match(line):
      pts = line.split()
      mynode = int(pts[0].split('CH')[0].strip('S'))
      if mynode == node:
        chan = int(pts[0].split('CH')[1].rstrip(';'))

        currec[DRAM_CLOCKS][chan] = int(pts[2].rstrip(';'))
        currec[SR_CYCLES][chan]   = int(pts[5].rstrip(';'))

        ppd_cycles                = int(pts[11].rstrip(';'))
        currec[PPD_CYCLES][chan]  = ppd_cycles  - currec[SR_CYCLES][chan]
        currec[PS_CYCLES][chan]   = currec[DRAM_CLOCKS][chan] - ppd_cycles

    elif sockInfoRE.match(line):
      pts = line.split()
      currec[SOCK_JOULES] = float(pts[7].rstrip(';'))
      currec[SOCK_WATTS]  = float(pts[9].rstrip(';'))

  return pcmvals

def newPcmMemoryRecord(chans):

  pcmrec = {}
  pcmrec[CHAN_READ]     = {}
  pcmrec[CHAN_WRITE]    = {}
  pcmrec[NODE_READ]     = 0.0
  pcmrec[NODE_WRITE]    = 0.0
  pcmrec[NODE_PWRITE]   = 0.0
  pcmrec[NODE_MEMORY]   = 0.0
  pcmrec[SYSTEM_READ]   = 0.0
  pcmrec[SYSTEM_WRITE]  = 0.0
  pcmrec[SYSTEM_MEMORY] = 0.0
  for chan in chans:
    if not pcmrec[CHAN_READ].has_key(chan):
      pcmrec[CHAN_READ][chan]  = 0.0
    if not pcmrec[CHAN_WRITE].has_key(chan):
      pcmrec[CHAN_WRITE][chan] = 0.0
  return pcmrec

def pcmMemoryInfoVals(bench, cfg, iter, node):

  pcmvals = []
  chans   = range(0,4)

  pcmf = try_open_read(pcmMemoryFile(bench, cfg, iter))

  cursocks = None
  curchan  = None
  currec   = newPcmMemoryRecord(chans)

  single_mode = False
  for line in pcmf:
    if sockGroupRE.match(line):
      pts = line.split()
      if len(pts) > 4:
        single_mode = False
        cursocks = ( int(pts[2]), int(pts[5]) )
      else:
        single_mode = True
        cursocks = [int(pts[2])]

    elif timeElapsedRE.match(line):
      currec[TIME_ELAPSED] = int(line.split()[2])

    elif chanReadRE.match(line):
      pts = line.split(':')
      if node in cursocks:
        if single_mode:
          curchan = int(pts[0].split()[-1])
          val = float(pts[2].split()[0])
          currec[CHAN_READ][curchan] = (val if val > 0.0 else 0.0)
        else:
          if node in (0,2):
            curchan = int(pts[0].split()[-1])
            val = float(pts[2].split()[0])
            currec[CHAN_READ][curchan] = (val if val > 0.0 else 0.0)
          else:
            curchan = int(pts[0].split()[-1])
            val = float(pts[-1].split()[0])
            currec[CHAN_READ][curchan] = (val if val > 0.0 else 0.0)

    elif chanWriteRE.match(line):
      pts = line.split(':')
      if node in cursocks:
        if single_mode:
          val = float(pts[1].split()[0])
          currec[CHAN_WRITE][curchan] = (val if val > 0.0 else 0.0)
          curchan = None
        else:
          if node in (0,2):
            val = float(pts[1].split()[0])
            currec[CHAN_WRITE][curchan] = (val if val > 0.0 else 0.0)
            curchan = None
          else:
            val = float(pts[-1].split()[0])
            currec[CHAN_WRITE][curchan] = (val if val > 0.0 else 0.0)
            curchan = None

    elif nodeReadRE.match(line):
      pts = line.split(':')
      if node in cursocks:
        if single_mode:
          currec[NODE_READ] = float(pts[1].split()[0])
        else:
          if node in (0,2):
            currec[NODE_READ] = float(pts[1].split()[0])
          else:
            currec[NODE_READ] = float(pts[-1].split()[0])

    elif nodeWriteRE.match(line):
      pts = line.split(':')
      if node in cursocks:
        if single_mode:
          currec[NODE_WRITE] = float(pts[1].split()[0])
        else:
          if node in (0,2):
            currec[NODE_WRITE] = float(pts[1].split()[0])
          else:
            currec[NODE_WRITE] = float(pts[-1].split()[0])

    elif nodePWriteRE.match(line):
      pts = line.split(':')
      if node in cursocks:
        if single_mode:
          currec[NODE_PWRITE] = float(pts[1].split()[0])
        else:
          if node in (0,2):
            currec[NODE_PWRITE] = int(pts[1].split()[0])
          else:
            currec[NODE_PWRITE] = int(pts[-1].split()[0])

    elif nodeMemoryRE.match(line):
      pts = line.split(':')
      if node in cursocks:
        if single_mode:
          currec[NODE_MEMORY] = float(pts[1].split()[0])
        else:
          if node in (0,2):
            currec[NODE_MEMORY] = float(pts[1].split()[0])
          else:
            currec[NODE_MEMORY] = float(pts[-1].split()[0])

    elif systemReadRE.match(line):
      pts = line.split(':')
      currec[SYSTEM_READ] = float(pts[1].split()[0])

    elif systemWriteRE.match(line):
      pts = line.split(':')
      currec[SYSTEM_WRITE] = float(pts[1].split()[0])

    elif systemMemoryRE.match(line):
      pts = line.split(':')
      currec[SYSTEM_MEMORY] = float(pts[1].split()[0])

      pcmvals.append(currec)
      currec = newPcmMemoryRecord(chans)

  return pcmvals

def toKB(miss, qual):

  if qual == None:
    return (0 if miss < 512 else 1)
  elif qual == "K":
    return miss
  elif qual == "M":
    return (miss*(2**10))
  elif qual == "G":
    return (miss*(2**20))

def newPcmRecord():

  pcmrec = {}
  pcmrec[IPC]          = None
  pcmrec[L3_MISS]      = None
  pcmrec[L2_MISS]      = None
  pcmrec[L3_HIT]       = None
  pcmrec[L2_HIT]       = None
  pcmrec[L3_CLK]       = None
  pcmrec[L2_CLK]       = None
  pcmrec[READ]         = None
  pcmrec[WRITE]        = None
  pcmrec[DRAM_JOULES]  = None
  pcmrec[SOCK_JOULES]  = None
  return pcmrec

def pcmInfoVals(bench, cfg, iter, node):

  pcmvals = []
  pcmf = try_open_read(pcmFile(bench, cfg, iter))

  currec = newPcmRecord()
  for line in pcmf:
    if skInfoRE.match(line):
      pts = line.split()
      if node == int(pts[1]): 
        currec[IPC] = float(pts[3])

        # have to handle qualifier on L3 and L2 misses
        adj    = 0
        qual   = None
        l3miss = int(pts[6])
        if not pts[7].isdigit():
          qual = pts[7]
          adj += 1
        currec[L3_MISS] = toKB(l3miss,qual)

        qual   = None
        l2miss = int(pts[7+adj])
        if not pts[8+adj].isdigit():
          qual = pts[8+adj]
          adj += 1
        currec[L2_MISS] = toKB(l2miss,qual)

        currec[L3_HIT]  = float(pts[8+adj])
        currec[L2_HIT]  = float(pts[9+adj])
        currec[L3_CLK]  = float(pts[10+adj])
        currec[L2_CLK]  = float(pts[11+adj])
        currec[READ]    = float(pts[12+adj])
        currec[WRITE]   = float(pts[13+adj])

    elif skJoulesRE.match(line):
      pts = line.split()
      if node == int(pts[1]): 
        currec[SOCK_JOULES] = float(pts[4])

    elif dramJoulesRE.match(line):
      pts = line.split() 
      if node == int(pts[1]): 
        currec[DRAM_JOULES] = float(pts[4])
        pcmvals.append(currec)
        currec = newPcmRecord()
  return pcmvals


def getMemBenchTimeVals(bench, cfg, iter, rawtype=PCM_SELF_REFRESH):
  try:
    rawfile = rawoutfile(bench, cfg, iter, type=rawtype)
    rawf    = open(rawfile)
  except IOError:
    print "no raw file: ", rawfile
    return None

  vals = {}
  for line in rawf:
    if mbStartRE.match(line):
      vals[MB_START]        = (int(line.split()[-1]) / 1000000)
    elif mbAllocStartRE.match(line):
      vals[MB_ALLOC_START]  = (int(line.split()[-1]) / 1000000)
    elif mbAllocEndRE.match(line):
      vals[MB_ALLOC_END]    = (int(line.split()[-1]) / 1000000)
    elif mbThreadStartRE.match(line):
      vals[MB_THREAD_START] = (int(line.split()[-1]) / 1000000)
    elif mbEndRE.match(line):
      vals[MB_END]          = (int(line.split()[-1]) / 1000000)
  return vals

def getDacapoBenchTimeVals(bench, cfg, iter, rawtype=None):
  try:
    rawfile = rawoutfile(bench, cfg, iter, type=rawtype)
    rawf    = open(rawfile)
  except IOError:
    print "no raw file: ", rawfile
    return None

  vals = {}
  for line in rawf:
    if vmStartRE.match(line):
      vals[VM_START]    = int(line.split()[-1])
    elif dacapoStartRE.match(line):
      vals[BENCH_START] = int(line.split()[-1])
    elif dacapoEndRE.match(line):
      vals[BENCH_END]   = int(line.split()[-1])
    #elif unixTimeRE.match(line):
    #  ends,endms = [int(x) for x in line.split()[1].split('.')]
    #  vals[BENCH_END]   = ((ends*1000)+endms) + vals[VM_START]
  return vals

def jvmTimeToSeconds(jvmtime):
  day,month,date,clock,tz,year = jvmtime.split()
  ts = strptime(("%s %s %s %s" % (month, date, year, clock)), \
                      "%b %d %Y %H:%M:%S")
  return int(mktime(ts))

def getjvm2008BenchTimeVals(bench, cfg, iter, rawtype=None):
  try:
    rawfile = rawoutfile(bench, cfg, iter, type=rawtype)
    rawf    = open(rawfile)
  except IOError:
    print "no raw file: ", rawfile
    return None

  vals = {}
  for line in rawf:
    if vmStartRE.match(line):
      vals[VM_START] = int(line.split()[-1])
    if jvmStartRE.match(line):
      vals[BENCH_START] = (jvmTimeToSeconds(" ".join(line.split()[4:])) * 1000)
    elif jvmEndRE.match(line):
      vals[BENCH_END]   = (jvmTimeToSeconds(" ".join(line.split()[4:])) * 1000)
    elif jvm2008TimeRE.match(line):
      vals[SCORE] = float(line.split()[3])
  return vals

def getJGFBenchTimeVals(bench, cfg, iter, rawtype=None):
  try:
    rawfile = rawoutfile(bench, cfg, iter, type=rawtype)
    rawf    = open(rawfile)
  except IOError:
    print "no raw file: ", rawfile
    return None

  vals = {}
  for line in rawf:
    if vmStartRE.match(line):
      vals[VM_START] = int(line.split()[-1])
    if jgfStartRE.match(line):
      vals[BENCH_START] = int(line.split()[2])
    elif jgfEndRE.match(line):
      vals[BENCH_END]   = int(line.split()[2])
    elif jgfTimeRE.match(line):
      vals[SCORE] = int(line.split()[2])
  return vals

def getMemBenchAllocTime(bench, cfg, iter):
  tvs = getMemBenchTimeVals(bench, cfg, iter)
  return (tvs[MB_ALLOC_END] - tvs[MB_ALLOC_START])

def getMemBenchThreadTime(bench, cfg, iter):
  tvs = getMemBenchTimeVals(bench, cfg, iter)
  return (tvs[MB_END] - tvs[MB_THREAD_START])

def getMemBenchAllocTimes(bench, cfg, iters=defiters):
  return [ getMemBenchAllocTime(bench, cfg, i) for i in range(iters) \
           if type(getMemBenchAllocTime(bench, cfg, i)) is int ]

def getMemBenchThreadTimes(bench, cfg, iters=defiters):
  return [ getMemBenchThreadTime(bench, cfg, i) for i in range(iters) \
           if type(getMemBenchThreadTime(bench, cfg, i)) is int ]

def getMemBenchAllocTimeStats(bench, cfg, iters=defiters, miniters=defminiters):
  atimes = getMemBenchAllocTimes(bench, cfg, iters=iters)
  vals = [ x for x in atimes if type(x) is int ]
  if len(vals) >= miniters:
    stats = {}
    stats[MEAN]  = average(atimes)
    stats[STDEV] = std(atimes)
    return stats
  return None

def getMemBenchThreadTimeStats(bench, cfg, iters=defiters, miniters=defminiters):
  ttimes = getMemBenchThreadTimes(bench, cfg, iters=iters)
  vals = [ x for x in ttimes if type(x) is int ]
  if len(vals) >= miniters:
    stats = {}
    stats[MEAN]  = average(ttimes)
    stats[STDEV] = std(ttimes)
    return stats
  return None

def filterVals(vals, start, end):

  fvals   = []
  curtime = 0
  for elapsed,val in vals:
    curtime += elapsed

    scale = 1.0
    if curtime > start:
      if (curtime - start) < elapsed:
        scale = (float(curtime - start) / elapsed)
      if curtime < end:
        if type(val) is list:
          fvals.append([scale*v for v in val])
        else:
          fvals.append((scale*val))
      else:
        val_start = curtime - elapsed
        if val_start < end:
          scale = (float(end - val_start) / elapsed)
          if type(val) is list:
            fvals.append([scale*v for v in val])
          else:
            fvals.append((scale*val))

  return fvals

def pcmStart(bench, cfg, iter, type=PCM_MEMORY):

  if type == PCM_MEMORY:
    pcmf = try_open_read(pcmMemoryFile(bench, cfg, iter))
  elif type == PCM_SELF_REFRESH:
    pcmf = try_open_read(pcmSelfRefreshFile(bench, cfg, iter))
  else:
    return None

  pcm_start = None
  for line in pcmf:
    if pcmStartRE.match(line):
      return int(line.split()[2])
  return None

def filterIdleVals(vals, vnum=None):

  start   = 0
  etimes  = [ x[0] for x in vals ]
  end     = sum ( etimes )
  fvals   = [ x[1] for x in vals ]

  if vnum != None:
    if vnum > len(fvals):
      print "warning: vnum > len(fvals)"
    else:
      div, rem = divmod((len(fvals) - vnum), 2)
      start = 0
      end   = sum ( [ etimes[i] for i in range(div+rem, len(fvals)-div) ] )
      fvals = fvals[(div+rem):][:-div]

  return (start, end, fvals)

def filterPCMVals(vals, bench, cfg, iter, rawtype=PCM_SELF_REFRESH, \
  vnum=None):
  start = end = None
  if bench in membenches:
    tvs = getMemBenchTimeVals(bench, cfg, iter, rawtype=rawtype)
  elif bench in dacapos:
    tvs = getDacapoBenchTimeVals(bench, cfg, iter, rawtype=rawtype)
  elif bench in jvm2008s:
    tvs = getjvm2008BenchTimeVals(bench, cfg, iter, rawtype=rawtype)
  elif bench in jgf_singles or bench in jgf_multis:
    tvs = getJGFBenchTimeVals(bench, cfg, iter, rawtype=rawtype)
  else:
    print "invalid bench: %s" % bench
    raise SystemExit(1)

  if bench in membenches:
    if tvs:
      start = tvs[MB_THREAD_START] - tvs[MB_START]
      end   = tvs[MB_END]          - tvs[MB_START]
  else:
    if tvs:
      pcm_start = pcmStart(bench, cfg, iter, type=rawtype)
      if not pcm_start:
        pcm_end   = tvs[BENCH_END]
        pcm_start = pcm_end - sum([x[0] for x in vals])
      start = tvs[BENCH_START] - pcm_start
      end   = tvs[BENCH_END]   - pcm_start

  if not start:
    print "filterVals: bad start and end time vals (%s-%s-i%d)" \
          % (bench, cfg, iter)
    return None

  etimes = [x[0] for x in vals]
  fvals  = filterVals(vals, start, end)
  if vnum != None:
    if vnum > len(fvals):
      print "warning: vnum > len(fvals)"
    else:
      div, rem = divmod((len(fvals) - vnum), 2)
      start = 0
      end   = sum ( [ etimes[i] for i in range(div+rem, len(fvals)-div) ] )
      fvals = fvals[(div+rem):][:-div]
  return (start, end, fvals)

def getChanSRInfo(bench, cfg, iter, node=0, chan=0, vnum=defvnum, quiet=True):

  if not chan in range(0,4):
    print "bad chan: %d" % chan

  if not node in range(0,2):
    print "bad node: %d" % node

  # power state vals
  psvals = pcmSRInfoVals(bench, cfg, iter, node)
  if not psvals:
    return (None, None, None, None)

  srivals = []
  for i,val in enumerate(psvals):
    srivals.append( ( val[TIME_ELAPSED], \
                      [ val[DRAM_CLOCKS][chan], val[SR_CYCLES][chan],
                        val[PPD_CYCLES][chan],  val[PS_CYCLES][chan]
                      ]
                  ) )

  start = end = None
  if bench in idles:
    start, end, srivals = filterIdleVals(srivals, vnum=vnum)
  else:
    # use start and end from PCM_SELF_REFRESH to match our performance results
    start, end, srivals = filterPCMVals(srivals, bench, cfg, iter, \
                                        rawtype=PCM_SELF_REFRESH, vnum=vnum)

  tot_clocks   = sum ( [ x[0] for x in srivals ] )
  sr_cyc_rat   = float( sum ( [ x[1] for x in srivals ] ) ) / tot_clocks
  ppd_cyc_rat  = float( sum ( [ x[2] for x in srivals ] ) ) / tot_clocks
  ps_cyc_rat   = float( sum ( [ x[3] for x in srivals ] ) ) / tot_clocks

  elapsed      = (float(end - start)/1000)
  sr_seconds   = elapsed * sr_cyc_rat
  ppd_seconds  = elapsed * ppd_cyc_rat
  ps_seconds   = elapsed * ps_cyc_rat

  sr_joules    = (SR_WATTS  * sr_seconds)
  ppd_joules   = (PPD_WATTS * ppd_seconds)
  ps_joules    = (PS_WATTS  * ps_seconds)

  return ((sr_cyc_rat, ppd_cyc_rat, ps_cyc_rat),
          (sr_joules,  ppd_joules,  ps_joules),
          (start, end))

def getChanSRInfos(bench, cfg, iters=defiters, miniters=defminiters,
  node=0, chan=0, vnum=defvnum):
  sris = []
  for i in range(iters):
    rats,js,tvs = getChanSRInfo(bench, cfg, i, node, chan, vnum=vnum)
    if type(rats[0]) is float and type(js[0]) is float:
      sris.append((rats,js,tvs))
  return sris

def getChanSRInfoStats(bench, cfg, iters=defiters, miniters=defminiters,
                       node=0, chan=0, idle_adj=False, vnum=defvnum):

  tsris = getChanSRInfos(bench, cfg, iters=iters, miniters=miniters, \
                         node=node, chan=chan, vnum=vnum)

  ratvals   = []
  joulevals = []
  for rs,js,tvs in tsris:
    if type(rs[0]) is float:
      ratvals.append(rs)
    if type(js[0]) is float:
      joulevals.append(js)

  if len(ratvals) >= miniters and len(joulevals) >= miniters:
    rstats = {}
    jstats = {}

    if idle_adj and bench not in jvm2008s:
      pass
    else:
      rstats[MEAN] = ( average( [ x[0] for x in ratvals ] ),
                       average( [ x[1] for x in ratvals ] ),
                       average( [ x[2] for x in ratvals ] )
                     )
      jstats[MEAN] = ( average( [ x[0] for x in joulevals ] ),
                       average( [ x[1] for x in joulevals ] ),
                       average( [ x[2] for x in joulevals ] )
                     )

    rstats[STDEV] = ( stdev( [ x[0] for x in ratvals ] ),
                      stdev( [ x[1] for x in ratvals ] ),
                      stdev( [ x[2] for x in ratvals ] )
                    )
    jstats[STDEV] = ( stdev( [ x[0] for x in joulevals ] ),
                      stdev( [ x[1] for x in joulevals ] ),
                      stdev( [ x[2] for x in joulevals ] )
                    )

    return (rstats, jstats)
  return None

def getModelChanPower(bench, cfg, iter, node=0, chan=0, vnum=defvnum, \
  quiet=True):

  if not chan in range(0,4):
    print "bad chan: %d" % chan

  if not node in range(0,2):
    print "bad node: %d" % node

  # power state vals
  psvals = pcmSRInfoVals(bench, cfg, iter, node)
  if not psvals:
    return (None, None, None, None)

  # bandwidth vals
  bwvals = pcmMemoryInfoVals(bench, cfg, iter, node)
  if not bwvals:
    return (None, None, None, None)

  bgjvals = []
  for i,val in enumerate(psvals):

    bg_joules      = 0.0
    sr_cycle_rat   = (float(val[SR_CYCLES][chan])  / val[DRAM_CLOCKS][chan])
    ppd_cycle_rat  = (float(val[PPD_CYCLES][chan]) / val[DRAM_CLOCKS][chan])
    ps_cycle_rat   = (float(val[PS_CYCLES][chan])  / val[DRAM_CLOCKS][chan])

    sr_chan_watts  = (SR_WATTS  * sr_cycle_rat)
    ppd_chan_watts = (PPD_WATTS * ppd_cycle_rat)
    ps_chan_watts  = (PS_WATTS  * ps_cycle_rat)

    chan_bg_watts = (sr_chan_watts + ppd_chan_watts + ps_chan_watts)
    bg_joules    += (float(val[TIME_ELAPSED]) / 1000) * chan_bg_watts

    bgjvals.append((val[TIME_ELAPSED],bg_joules))

  opjvals = []
  for i,val in enumerate(bwvals):
    rd_gbs    = ( (float(val[CHAN_READ][chan])  / 1024) * \
                  (float(val[TIME_ELAPSED]) / 1000) )
    wr_gbs    = ( (float(val[CHAN_WRITE][chan]) / 1024) * \
                  (float(val[TIME_ELAPSED]) / 1000) )
    rd_joules = rd_gbs * (RD_JOULES*OPS_PER_GB)
    wr_joules = wr_gbs * (WR_JOULES*OPS_PER_GB)
    op_joules = rd_joules + wr_joules

    opjvals.append((val[TIME_ELAPSED], op_joules))

  start = end = None
  if bench in idles:
    _,       _, bgjvals = filterIdleVals(bgjvals, vnum=vnum)
    start, end, opjvals = filterIdleVals(opjvals, vnum=vnum)
  else:
    # use start and end from PCM_SELF_REFRESH to match our performance results
    _,       _, opjvals = filterPCMVals(opjvals, bench, cfg, iter, \
                                        rawtype=PCM_MEMORY, vnum=vnum)
    start, end, bgjvals = filterPCMVals(bgjvals, bench, cfg, iter, \
                                        rawtype=PCM_SELF_REFRESH, vnum=vnum)

  elapsed      = (float(end - start)/1000)

  bg_energy    = float(sum(bgjvals))
  op_energy    = float(sum(opjvals))
  total_energy = bg_energy + op_energy

  bg_watts     = (bg_energy    / elapsed)
  op_watts     = (op_energy    / elapsed)
  total_watts  = (total_energy / elapsed)

  # convert energy to joules / operation
  if bench in jvm2008s:
    tvs = getjvm2008BenchTimeVals(bench, cfg, iter, rawtype=PCM_SELF_REFRESH)
    runtime       = (tvs[BENCH_END] - tvs[BENCH_START])
    total_minutes = (float(runtime) / 60000)
    total_ops     = (tvs[SCORE] * total_minutes)

    bg_jpo        = bg_energy    / total_ops
    op_jpo        = op_energy    / total_ops
    total_jpo     = total_energy / total_ops

    return ((total_watts, total_jpo), (bg_watts, bg_jpo),
            (op_watts, op_jpo), (start, end))

  elif bench in jgf_multis or bench in jgf_singles:
    # score is the number of calls
    bg_jpo    = bg_energy    / tvs[SCORE]
    op_jpo    = op_energy    / tvs[SCORE]
    total_jpo = total_energy / tvs[SCORE]

    return ((total_watts, total_jpo), (bg_watts, bg_jpo),
            (op_watts, op_jpo), (start, end))

  return ((total_watts, total_energy), (bg_watts, bg_energy),
          (op_watts, op_energy), (start, end))

def getModelChanPowers(bench, cfg, iters=defiters, miniters=defminiters,
  node=0, chan=0, vnum=defvnum):
  mbtps = []
  for i in range(iters):
    tots,bgs,ops,tvs = getModelChanPower(bench, cfg, i, node, chan, vnum=vnum)
    if type(tots[0]) is float:
      mbtps.append((tots,bgs,ops,tvs))
  return mbtps

def getModelChanPowerStats(bench, cfg, iters=defiters, miniters=defminiters,
                           node=0, chan=0, idle_adj=False, vnum=defvnum):

  tpows = getModelChanPowers(bench, cfg, iters=iters, miniters=miniters, \
                            node=node, chan=chan, vnum=vnum)
  totws = [ x[0][0] for x in tpows if type(x[0][0]) is float ]
  totjs = [ x[0][1] for x in tpows if type(x[0][1]) is float ]
  bgws  = [ x[1][0] for x in tpows if type(x[1][0]) is float ]
  bgjs  = [ x[1][1] for x in tpows if type(x[1][1]) is float ]
  opws  = [ x[2][0] for x in tpows if type(x[2][0]) is float ]
  opjs  = [ x[2][1] for x in tpows if type(x[2][1]) is float ]

  if len(totws) >= miniters:
    totwstats = {}
    totjstats = {}
    bgwstats  = {}
    bgjstats  = {}
    opwstats  = {}
    opjstats  = {}

    if idle_adj and bench not in jvm2008s:
      pass

#      cfgset = mmpcfgs
#      if bench in storebenches:
#        cfgset = stmmpcfgs
#
#      longest = getLongestSP(bench, cfgset, iters=iters,
#                              miniters=miniters, node=node)
#      elapsed = average([ (tvs[1]-tvs[0]) for ws,js,tvs in tpows ])
#      rem     = (longest - elapsed)
#
#      idlecfg = HDKD_ALLOC_TUNE if cfg == HDKD_ALLOC_TUNE else HDKM_ALLOC_TUNE_V2
#      idlews, idlejs, _  = getModelMemPower('idle-100', idlecfg, 0)
#
#      #print "elapsed: %3.2f longest: %3.2f rem: %3.2f idlews: %3.2f idlejs: %3.2f" % \
#      #      (elapsed, longest, rem, idlews, idlejs)
#
#      jsadj = (float(rem)/1000) * idlews
#      jstats[MEAN] = average(joulevals) + jsadj
#      wstats[MEAN] = (jstats[MEAN] / (elapsed/1000))
    else:
      totwstats[MEAN]  = average(totws)
      totjstats[MEAN]  = average(totjs)
      bgwstats[MEAN]   = average(bgws)
      bgjstats[MEAN]   = average(bgjs)
      opwstats[MEAN]   = average(opws)
      opjstats[MEAN]   = average(opjs)

    totwstats[STDEV] = std(totws)
    totjstats[STDEV] = std(totjs)
    bgwstats[STDEV]  = std(bgws)
    bgjstats[STDEV]  = std(bgjs)
    opwstats[STDEV]  = std(opws)
    opjstats[STDEV]  = std(opjs)
    return ((totwstats,totjstats),(bgwstats,bgjstats),(opwstats,opjstats))

  return None

def getModelMemPower(bench, cfg, iter, node=0, vnum=defvnum, quiet=True):

  chans = range(0,4)

  # power state vals
  psvals = pcmSRInfoVals(bench, cfg, iter, node)
  if not psvals:
    return (None, None, None)

  # bandwidth vals
  bwvals = pcmMemoryInfoVals(bench, cfg, iter, node)
  if not bwvals:
    return (None, None, None)

  bgjvals = []
  for i,val in enumerate(psvals):

    bg_joules = 0.0
    for chan in chans:

      sr_cycle_rat   = (float(val[SR_CYCLES][chan])  / val[DRAM_CLOCKS][chan])
      ppd_cycle_rat  = (float(val[PPD_CYCLES][chan]) / val[DRAM_CLOCKS][chan])
      ps_cycle_rat   = (float(val[PS_CYCLES][chan])  / val[DRAM_CLOCKS][chan])

      sr_chan_watts  = (SR_WATTS  * sr_cycle_rat)
      ppd_chan_watts = (PPD_WATTS * ppd_cycle_rat)
      ps_chan_watts  = (PS_WATTS  * ps_cycle_rat)

      chan_bg_watts = (sr_chan_watts + ppd_chan_watts + ps_chan_watts)
      bg_joules    += (float(val[TIME_ELAPSED]) / 1000) * chan_bg_watts

    bgjvals.append((val[TIME_ELAPSED],bg_joules))

  opjvals = []
  for i,val in enumerate(bwvals):
    rd_gbs    = ( (float(val[NODE_READ])  / 1024) * (float(val[TIME_ELAPSED]) / 1000) )
    wr_gbs    = ( (float(val[NODE_WRITE]) / 1024) * (float(val[TIME_ELAPSED]) / 1000) )
    rd_joules = rd_gbs * (RD_JOULES*OPS_PER_GB)
    wr_joules = wr_gbs * (WR_JOULES*OPS_PER_GB)
    op_joules = rd_joules + wr_joules

    opjvals.append((val[TIME_ELAPSED], op_joules))

  start = end = None
  if bench in idles:
    _,       _, bgjvals = filterIdleVals(bgjvals, vnum=vnum)
    start, end, opjvals = filterIdleVals(opjvals, vnum=vnum)
  else:
    # use start and end from PCM_SELF_REFRESH to match our performance results
    _,       _, opjvals = filterPCMVals(opjvals, bench, cfg, iter, \
                                        rawtype=PCM_MEMORY, vnum=vnum)
    start, end, bgjvals = filterPCMVals(bgjvals, bench, cfg, iter, \
                                        rawtype=PCM_SELF_REFRESH, vnum=vnum)


  elapsed      = (float(end - start)/1000)

  bg_energy    = float(sum(bgjvals))
  op_energy    = float(sum(opjvals))
  total_energy = bg_energy + op_energy

  bg_watts     = (bg_energy    / elapsed)
  op_watts     = (op_energy    / elapsed)
  total_watts  = (total_energy / elapsed)

  # convert energy to joules / operation
  if bench in jvm2008s:
    tvs = getjvm2008BenchTimeVals(bench, cfg, iter, rawtype=PCM_SELF_REFRESH)
    runtime       = (tvs[BENCH_END] - tvs[BENCH_START])
    total_minutes = (float(runtime) / 60000)
    total_ops     = (tvs[SCORE] * total_minutes)
    bg_jpo        = bg_energy    / total_ops
    op_jpo        = op_energy    / total_ops
    total_jpo     = total_energy / total_ops

    return ((total_watts, total_jpo), (bg_watts, bg_jpo),
            (op_watts, op_jpo), (start, end))

  elif bench in jgf_multis or bench in jgf_singles:
    # score is the number of calls
    bg_jpo    = bg_energy    / tvs[SCORE]
    op_jpo    = op_energy    / tvs[SCORE]
    total_jpo = total_energy / tvs[SCORE]

    return ((total_watts, total_jpo), (bg_watts, bg_jpo),
            (op_watts, op_jpo), (start, end))

  return ((total_watts, total_energy), (bg_watts, bg_energy),
          (op_watts, op_energy), (start, end))

def getModelMemPowers(bench, cfg, iters=defiters, miniters=defminiters, node=0, \
  vnum=defvnum):
  mbtps = []
  for i in range(iters):
    tots,bgs,ops,tvs = getModelMemPower(bench, cfg, i, node, vnum=vnum)
    if type(tots[0]) is float:
      mbtps.append((tots,bgs,ops,tvs))
  return mbtps

def getLongestMMP(bench, cfgset, iters=5, miniters=5, node=0):
  
  evs = []
  for cfg in cfgset:
    tpows = getModelMemPowers(bench, cfg, iters=iters, miniters=miniters, \
                              node=node)
    elapsed = average([ (tvs[1]-tvs[0]) for ws,js,tvs in tpows ])
    evs.append(elapsed)
  return max(evs)

def getModelMemPowerStats(bench, cfg, iters=defiters, miniters=defminiters,
                          node=0, vnum=defvnum, idle_adj=False):

  tpows = getModelMemPowers(bench, cfg, iters=iters, miniters=miniters, \
                            node=node, vnum=vnum)
  totws = [ x[0][0] for x in tpows if type(x[0][0]) is float ]
  totjs = [ x[0][1] for x in tpows if type(x[0][1]) is float ]
  bgws  = [ x[1][0] for x in tpows if type(x[1][0]) is float ]
  bgjs  = [ x[1][1] for x in tpows if type(x[1][1]) is float ]
  opws  = [ x[2][0] for x in tpows if type(x[2][0]) is float ]
  opjs  = [ x[2][1] for x in tpows if type(x[2][1]) is float ]

  if len(totws) >= miniters:
    totwstats = {}
    totjstats = {}
    bgwstats  = {}
    bgjstats  = {}
    opwstats  = {}
    opjstats  = {}

    if idle_adj and bench not in jvm2008s:
      pass
      # need to update this to have (tot, bg, op, tv) format

#      cfgset = mmpcfgs
#      if bench in storebenches:
#        cfgset = stmmpcfgs
#
#      longest = getLongestSP(bench, cfgset, iters=iters,
#                              miniters=miniters, node=node)
#      elapsed = average([ (tvs[1]-tvs[0]) for ws,js,tvs in tpows ])
#      rem     = (longest - elapsed)
#
#      idlecfg = HDKD_ALLOC_TUNE if cfg == HDKD_ALLOC_TUNE else HDKM_ALLOC_TUNE_V2
#      idlews, idlejs, _  = getModelMemPower('idle-100', idlecfg, 0)
#
#      #print "elapsed: %3.2f longest: %3.2f rem: %3.2f idlews: %3.2f idlejs: %3.2f" % \
#      #      (elapsed, longest, rem, idlews, idlejs)
#
#      jsadj = (float(rem)/1000) * idlews
#      jstats[MEAN] = average(joulevals) + jsadj
#      wstats[MEAN] = (jstats[MEAN] / (elapsed/1000))

    else:
      totwstats[MEAN]  = average(totws)
      totjstats[MEAN]  = average(totjs)
      bgwstats[MEAN]   = average(bgws)
      bgjstats[MEAN]   = average(bgjs)
      opwstats[MEAN]   = average(opws)
      opjstats[MEAN]   = average(opjs)

    totwstats[STDEV] = std(totws)
    totjstats[STDEV] = std(totjs)
    bgwstats[STDEV]  = std(bgws)
    bgjstats[STDEV]  = std(bgjs)
    opwstats[STDEV]  = std(opws)
    opjstats[STDEV]  = std(opjs)
    return ((totwstats,totjstats),(bgwstats,bgjstats),(opwstats,opjstats))

  return None

# socket power is actually just power for the CPU
# 
def getSockPower(bench, cfg, iter, node=0, vnum=defvnum, quiet=True):

  # power state vals
  srvals = pcmSRInfoVals(bench, cfg, iter, node)
  if not srvals:
    return (None, None, None)

  sjvals = []
  for i,val in enumerate(srvals):
    sjvals.append((val[TIME_ELAPSED],val[SOCK_JOULES]))

  start = end = None
  if bench in idles:
    start  = 0
    end    = sum ( [ x[0] for x in sjvals ] )
    sjvals = [ x[1] for x in sjvals ]
  else:
    start, end, sjvals = filterPCMVals(sjvals, bench, cfg, iter, \
                                       vnum=vnum, rawtype=PCM_SELF_REFRESH)

  total_energy = float(sum(sjvals))
  avgwvals     = (total_energy / (float(end - start)/1000))

  # convert energy to joules / operation
  if bench in jvm2008s:
    tvs = getjvm2008BenchTimeVals(bench, cfg, iter, rawtype=PCM_SELF_REFRESH)
    runtime       = (tvs[BENCH_END] - tvs[BENCH_START])
    total_minutes = (float(runtime) / 60000)
    total_ops     = (tvs[SCORE] * total_minutes)
    joules_per_op = total_energy / total_ops

    return (avgwvals, joules_per_op, (start, end))

  elif bench in jgf_multis or bench in jgf_singles:
    # score is the number of calls
    joules_per_op = total_energy / tvs[SCORE]

    return (avgwvals, joules_per_op, (start, end))

  return (avgwvals, total_energy, (start, end))

def getSockPowers(bench, cfg, iters=defiters, miniters=defminiters, node=0,\
  vnum=defvnum):
  spows = []
  for i in range(iters):
    ws,js,tvs = getSockPower(bench, cfg, i, node, vnum)
    if type(ws) is float and type(js) is float:
      spows.append((ws,js,tvs))
  return spows

def getLongestSP(bench, cfgset, iters=5, miniters=5, node=0):
  
  evs = []
  for cfg in cfgset:
    spows = getSockPowers(bench, cfg, iters=iters, miniters=miniters, \
                          node=node)
    elapsed = average([ (tvs[1]-tvs[0]) for ws,js,tvs in spows ])
    evs.append(elapsed)
  return max(evs)

def getSockPowerStats(bench, cfg, iters=defiters, miniters=defminiters,
                     node=0, vnum=defvnum, idle_adj=False):
  spows = getSockPowers(bench, cfg, iters=iters, miniters=miniters, \
                        node=node, vnum=vnum)
  wattvals  = [ x[0] for x in spows if type(x[0]) is float ]
  joulevals = [ x[1] for x in spows if type(x[1]) is float ]
  if len(wattvals) >= miniters and len(joulevals) >= miniters:
    wstats = {}
    jstats = {}

    if idle_adj and bench not in jvm2008s:

      cfgset = mmpcfgs
      if bench in storebenches:
        cfgset = stmmpcfgs

      longest = getLongestSP(bench, cfgset, iters=iters,
                              miniters=miniters, node=node)
      elapsed = average([ (tvs[1]-tvs[0]) for ws,js,tvs in spows ])
      rem     = (longest - elapsed)

      idlecfg = HDKD_ALLOC_TUNE if cfg == HDKD_ALLOC_TUNE else HDKM_ALLOC_TUNE_V2
      idlews, idlejs, _  = getSockPower('idle-100', idlecfg, 0)

      jsadj = (float(rem)/1000) * idlews
      jstats[MEAN] = average(joulevals) + jsadj
      wstats[MEAN] = (jstats[MEAN] / (elapsed/1000))

    else:

      wstats[MEAN]  = average(wattvals)
      jstats[MEAN]  = average(joulevals)

    wstats[STDEV] = std(wattvals)
    jstats[STDEV] = std(joulevals)
    return (wstats,jstats)
  return None

def getModelTotalPowers(bench, cfg, iters=defiters, miniters=defminiters, \
                        node=0, vnum=defvnum):
  spows = getSockPowers(bench, cfg, iters=iters, miniters=miniters, \
                        node=node, vnum=vnum)
  mpows = getModelMemPowers(bench, cfg, iters=iters, miniters=miniters, \
                            node=node, vnum=vnum)
  tpows = [ ((s[0]+m[0][0]),(s[1]+m[0][1]), s[2], m[3]) for s,m in zip(spows,mpows) ]
  return tpows

def getModelTotalPowerStats(bench, cfg, iters=defiters, miniters=defminiters,
                            node=0, vnum=defvnum, idle_adj=False):
  tpows = getModelTotalPowers(bench, cfg, iters=iters, miniters=miniters, \
                              node=node, vnum=vnum)
  wattvals  = [ x[0] for x in tpows if type(x[0]) is float ]
  joulevals = [ x[1] for x in tpows if type(x[1]) is float ]
  if len(wattvals) >= miniters and len(joulevals) >= miniters:
    wstats = {}
    jstats = {}

    if idle_adj and bench not in jvm2008s:

      cfgset = mmpcfgs
      if bench in storebenches:
        cfgset = stmmpcfgs

      longest = getLongestSP(bench, cfgset, iters=iters,
                              miniters=miniters, node=node)
      elapsed = average([ (stvs[1]-stvs[0]) for ws,js,stvs,mtvs in tpows ])
      rem     = (longest - elapsed)
      idlecfg = HDKD_ALLOC_TUNE if cfg == HDKD_ALLOC_TUNE else HDKM_ALLOC_TUNE_V2
      idlews, idlejs, _  = getSockPower('idle-100', idlecfg, 0)
      sjsadj  = (float(rem)/1000) * idlews

      longest = getLongestMMP(bench, cfgset, iters=iters,
                              miniters=miniters, node=node)
      elapsed = average([ (mtvs[1]-mtvs[0]) for ws,js,stvs,mtvs in tpows ])
      rem     = (longest - elapsed)
      idlecfg = HDKD_ALLOC_TUNE if cfg == HDKD_ALLOC_TUNE else HDKM_ALLOC_TUNE_V2
      idlews, idlejs, _  = getModelMemPower('idle-100', idlecfg, 0)
      mjsadj  = (float(rem)/1000) * idlews

      #print average(joulevals), sjsadj, mjsadj
      jstats[MEAN] = average(joulevals) + sjsadj + mjsadj
      wstats[MEAN] = (jstats[MEAN] / (elapsed/1000))

    else:

      wstats[MEAN]  = average(wattvals)
      jstats[MEAN]  = average(joulevals)

    wstats[STDEV] = std(wattvals)
    jstats[STDEV] = std(joulevals)
    return (wstats,jstats)
  return None

def getMemPower(bench, cfg, iter, node=0, filter=True, quiet=True):

  pvtime = 0
  twvals = []
  tjvals = []
  tvs    = None

  pvs = pcmPowerInfoVals(bench, cfg, iter, node, quiet=quiet)
  if not pvs:
    return (None, None)

  for i,val in enumerate(pvs):
    pvtime += val[TIME_ELAPSED]
    twvals.append((pvtime,val[DRAM_WATTS]))
    tjvals.append((pvtime,val[DRAM_JOULES]))

  start = end = None
  if filter:
    if bench in membenches:
      tvs = getMemBenchTimeVals(bench, cfg, iter)
      if tvs:
        start = tvs[MB_THREAD_START] - tvs[MB_START]
        end   = tvs[MB_END]          - tvs[MB_START]
    elif bench in dacapos:
      tvs = getDacapoBenchTimeVals(bench, cfg, iter)
      if tvs:
        start = (tvs[BENCH_START] - tvs[VM_START])
        end   = (tvs[BENCH_END]   - tvs[VM_START])
    elif bench in jvm2008s:
      tvs = getjvm2008BenchTimeVals(bench, cfg, iter)
      if tvs:
        start = (tvs[BENCH_START] - tvs[VM_START])
        end   = (tvs[BENCH_END]   - tvs[VM_START])
    elif bench in jgf_singles or bench in jgf_multis:
      tvs = getJGFBenchTimeVals(bench, cfg, iter)
      if tvs:
        start = (tvs[BENCH_START] - tvs[VM_START])
        end   = (tvs[BENCH_END]   - tvs[VM_START])

    if start:
      twvals = [ x[1] for x in twvals if x[0] > start and x[0] < end ]
      tjvals = [ x[1] for x in tjvals if x[0] > start and x[0] < end ]
    else:
      twvals = [ x[1] for x in twvals ]
      tjvals = [ x[1] for x in tjvals ]

  else:
    twvals = [ x[1] for x in twvals ]
    tjvals = [ x[1] for x in tjvals ]

  twvals,outs = rmoutliers(twvals)
  if not quiet:
    for out in outs:
      print "%s-%s-i%d: removed DRAM_WATTS outlier: %5.2f" \
            % (bench, cfg, iter, out)

  tjvals,outs = rmoutliers(tjvals)
  for out in outs:
    if not quiet:
      print "%s-%s-i%d: removed DRAM_JOULES outlier: %5.2f" \
            % (bench, cfg, iter, out)
    tjvals.append(average(tjvals))

  # convert energy to joules / operation
  if filter:
    if bench in jvm2008s:
      runtime       = (tvs[BENCH_END] - tvs[BENCH_START])
      total_minutes = (float(runtime) / 60000)
      total_ops     = (tvs[SCORE] * total_minutes)

      total_energy  = float(sum(tjvals))
      joules_per_op = total_energy / total_ops

      return (float(average(twvals)), joules_per_op)
    elif bench in jgf_multis or bench in jgf_singles:
      # score is the number of calls
      total_energy  = float(sum(tjvals))
      joules_per_op = total_energy / tvs[SCORE]

      return (float(average(twvals)), joules_per_op)

  return (float(average(twvals)), float(sum(tjvals)))

def getMemPowers(bench, cfg, iters=defiters, miniters=defminiters, node=0, \
                 filter=True, quiet=True):
  mbtps = []
  for i in range(iters):
    val = getMemPower(bench, cfg, i, node, filter=filter, quiet=quiet)
    if type(val[0]) is float and type(val[1]) is float:
      mbtps.append(val)
  return mbtps

def getMemPowerStats(bench, cfg, iters=defiters, miniters=defminiters,
                     node=0, filter=True, quiet=True):
  tpows = getMemPowers(bench, cfg, iters=iters, miniters=miniters, \
                       node=node, filter=filter, quiet=quiet)
  wattvals  = [ x[0] for x in tpows if type(x[0]) is float ]
  joulevals = [ x[1] for x in tpows if type(x[1]) is float ]
  if len(wattvals) >= miniters and len(joulevals) >= miniters:
    wstats = {}
    wstats[MEAN]  = average(wattvals)
    wstats[STDEV] = std(wattvals)

    jstats = {}
    jstats[MEAN]  = average(joulevals)
    jstats[STDEV] = std(joulevals)
    return (wstats,jstats)
  return None

def getChanBandwidth(bench, cfg, iter, node=0, chan=0, \
  vnum=defvnum, per_second=False):

  rdvals  = []
  wrvals  = []
  memvals = []
  itimes  = []
  pvs     = pcmMemoryInfoVals(bench, cfg, iter, node)
  for i,val in enumerate(pvs):

    rd_scale  = ( (val[CHAN_READ][chan]/1024)   * \
                  (float(val[TIME_ELAPSED])/1000))
    wr_scale  = ( (val[CHAN_WRITE][chan]/1024)  * \
                  (float(val[TIME_ELAPSED])/1000))

    rdvals.append((val[TIME_ELAPSED],rd_scale))
    wrvals.append((val[TIME_ELAPSED],wr_scale))

  start = end = None
  if bench in idles:
    start  = 0
    end    = sum ( [ x[0] for x in wrvals ] )
    rdvals = [ x[1] for x in rdvals ]
    wrvals = [ x[1] for x in wrvals ]
  else:
    _, _, rdvals       = filterPCMVals(rdvals, bench, cfg, iter, \
                                       rawtype=PCM_MEMORY, vnum=vnum)
    start, end, wrvals = filterPCMVals(wrvals, bench, cfg, iter, \
                                       rawtype=PCM_MEMORY, vnum=vnum)

#  if filter:
#    tvs = None
#    if bench in membenches:
#      tvs = getMemBenchTimeVals(bench, cfg, iter, rawtype=PCM_MEMORY)
#    elif bench in dacapos:
#      tvs = getDacapoBenchTimeVals(bench, cfg, iter, rawtype=PCM_MEMORY)
#    elif bench in jvm2008s:
#      tvs = getjvm2008BenchTimeVals(bench, cfg, iter, rawtype=PCM_MEMORY)
#    elif bench in jgf_singles or bench in jgf_multis:
#      tvs = getJGFBenchTimeVals(bench, cfg, iter, rawtype=PCM_MEMORY)
#
#    if bench in membenches:
#      if tvs:
#        start = tvs[MB_THREAD_START] - tvs[MB_START]
#        end   = tvs[MB_END]          - tvs[MB_START]
#    else:
#      if tvs:
#
#        pcm_start = pcmStart(bench, cfg, iter, type=PCM_MEMORY)
#        if not pcm_start:
#          pcm_end   = tvs[BENCH_END]
#          pcm_start = pcm_end - sum([x[0] for x in memvals])
#        start = tvs[BENCH_START] - pcm_start
#        end   = tvs[BENCH_END]   - pcm_start
#
#    itimes.append( (float(end-start)/1000) )
#
#    if start:
#      rdvals  = filterVals(rdvals,  start, end)
#      wrvals  = filterVals(wrvals,  start, end)
#    else:
#      rdvals  = [ x[1] for x in rdvals  ]
#      wrvals  = [ x[1] for x in wrvals  ]
#  else:
#    rdvals  = [ x[1] for x in rdvals  ]
#    wrvals  = [ x[1] for x in wrvals  ]

  total_rds  = float(sum(rdvals))
  total_wrs  = float(sum(wrvals))
  total_bw   = total_rds + total_wrs

  if per_second:
    runtime = (float(end-start)/1000)
    return ( (total_rds/runtime), (total_wrs/runtime), (total_bw/runtime) )

  return (total_rds, total_wrs, total_bw)

def getChanBandwidths(bench, cfg, iters=defiters, node=0, chan=0, \
  per_second=False, vnum=defvnum):
  mbtbws = []
  for i in range(iters):
    val = getChanBandwidth(bench, cfg, i, node=node, chan=chan, \
                           vnum=vnum, per_second=per_second)
    if type(val[0]) is float and type(val[1]) is float:
      mbtbws.append(val)
  return mbtbws

def getChanBandwidthStats(bench, cfg, iters=defiters, miniters=defminiters,
                          node=0, chan=0, vnum=defvnum, per_second=False):
  tbws    = getChanBandwidths(bench, cfg, iters=iters, node=node, \
                              chan=chan, vnum=vnum, per_second=per_second)
  rdvals  = [ x[0] for x in tbws if type(x[0]) is float ]
  wrvals  = [ x[1] for x in tbws if type(x[1]) is float ]
  totvals = [ x[2] for x in tbws if type(x[2]) is float ]
  if len(rdvals) >= miniters and len(wrvals) >= miniters:
    rdstats = {}
    rdstats[MEAN]  = average(rdvals)
    rdstats[STDEV] = std(rdvals)

    wrstats = {}
    wrstats[MEAN]  = average(wrvals)
    wrstats[STDEV] = std(wrvals)

    totstats = {}
    totstats[MEAN]  = average(totvals)
    totstats[STDEV] = std(totvals)

    return (rdstats,wrstats,totstats)
  return None

def getMemBandwidth(bench, cfg, iter, node=0, filter=True, vnum=defvnum, \
  per_second=False):

  rdvals  = []
  wrvals  = []
  memvals = []
  itimes  = []
  pvs     = pcmMemoryInfoVals(bench, cfg, iter, node)
  for i,val in enumerate(pvs):

    rd_scale  = ( (val[NODE_READ]/1024)   * (float(val[TIME_ELAPSED])/1000))
    wr_scale  = ( (val[NODE_WRITE]/1024)  * (float(val[TIME_ELAPSED])/1000))
    mem_scale = ( (val[NODE_MEMORY]/1024) * (float(val[TIME_ELAPSED])/1000))

    rdvals.append((val[TIME_ELAPSED],rd_scale))
    wrvals.append((val[TIME_ELAPSED],wr_scale))
    memvals.append((val[TIME_ELAPSED],mem_scale))

  start = end = None
  if bench in idles:
    start   = 0
    end     = sum ( [ x[0] for x in wrvals ] )
    rdvals  = [ x[1] for x in rdvals  ]
    wrvals  = [ x[1] for x in wrvals  ]
    memvals = [ x[1] for x in memvals ]
  else:
    _, _, rdvals        = filterPCMVals(rdvals, bench, cfg, iter, \
                                        rawtype=PCM_MEMORY, vnum=vnum)
    start, end, wrvals  = filterPCMVals(wrvals, bench, cfg, iter, \
                                        rawtype=PCM_MEMORY, vnum=vnum)
    start, end, memvals = filterPCMVals(memvals, bench, cfg, iter, \
                                        rawtype=PCM_MEMORY, vnum=vnum)

  total_rds  = float(sum(rdvals))
  total_wrs  = float(sum(wrvals))
  total_mems = float(sum(memvals))

  if per_second:
    runtime = (float(end-start)/1000)
    return ( (total_rds/runtime), (total_wrs/runtime), (total_mems/runtime) )

  return (total_rds, total_wrs, total_mems)

def getMemBandwidths(bench, cfg, iters=defiters, node=0, filter=True, \
  vnum=defvnum, per_second=False):
  mbtbws = []
  for i in range(iters):
    val = getMemBandwidth(bench, cfg, i, node, filter=filter, \
                          vnum=vnum, per_second=per_second)
    if type(val[0]) is float and type(val[1]) is float and type(val[2]) is float:
      mbtbws.append(val)
  return mbtbws

def getMemBandwidthStats(bench, cfg, iters=defiters, miniters=defminiters,
                         node=0, filter=True, vnum=defvnum, per_second=False):
  tbws    = getMemBandwidths(bench, cfg, iters=iters, node=node, \
                             filter=filter, vnum=vnum, per_second=per_second)
  rdvals  = [ x[0] for x in tbws if type(x[0]) is float ]
  wrvals  = [ x[1] for x in tbws if type(x[1]) is float ]
  memvals = [ x[2] for x in tbws if type(x[2]) is float ]
  if len(rdvals) >= miniters and len(wrvals) >= miniters and len(memvals) >= miniters:
    rdstats = {}
    rdstats[MEAN]  = average(rdvals)
    rdstats[STDEV] = std(rdvals)

    wrstats = {}
    wrstats[MEAN]  = average(wrvals)
    wrstats[STDEV] = std(wrvals)

    memstats = {}
    memstats[MEAN]  = average(memvals)
    memstats[STDEV] = std(memvals)
    return (rdstats,wrstats,memstats)
  return None

def newCSSInfo(inv, full, colored=True, ceden=True):

  cssinfo = {}
  cssinfo[HEAP_BEFORE] = {}
  cssinfo[HEAP_AFTER]  = {}
  cssinfo[INVOCATION]  = inv
  cssinfo[MAJOR_GC]    = full

  if colored:
    if ceden:
      cssinfo[HEAP_BEFORE][EDEN]   = {}
      cssinfo[HEAP_AFTER][EDEN]    = {}

    cssinfo[HEAP_BEFORE][SURVIVOR] = {}
    cssinfo[HEAP_BEFORE][TENURED]  = {}
    cssinfo[HEAP_AFTER][SURVIVOR]  = {}
    cssinfo[HEAP_AFTER][TENURED]   = {}
  return cssinfo

def getUsedCap(line, colored=True):

  used = cap = None
  pts = line.split()
  if colored:
    cap  = int(pts[3].strip(',K'))
    used = int(pts[6].strip('(K)'))
  else:
    cap  = int(pts[2].strip(',K'))
    used = int(pts[5].strip('(K)'))
  return (used, cap)

def parseGCInfo(rawf, cssinfo, pos, colored=True, ceden=True):

  line = rawf.next()

  # read the used from the eden space
  while not edenSpaceRE.search(line):
    line = rawf.next()

  if colored and ceden:

    while not colorRedSpaceRE.search(line):
      line = rawf.next()
    cssinfo[pos][EDEN][RED] = (getUsedCap(line))

    while not colorBlueSpaceRE.search(line):
      line = rawf.next()
    cssinfo[pos][EDEN][BLUE] = (getUsedCap(line))

  else:
    cssinfo[pos][EDEN] = (getUsedCap(line,False))


  # survivor space used
  while not fromSpaceRE.search(line):
    line = rawf.next()

  if colored:
    while not colorRedSpaceRE.search(line):
      line = rawf.next()
    cssinfo[pos][SURVIVOR][RED] = (getUsedCap(line))

    while not colorBlueSpaceRE.search(line):
      line = rawf.next()
    cssinfo[pos][SURVIVOR][BLUE] = (getUsedCap(line))

  else:
    cssinfo[pos][SURVIVOR] = (getUsedCap(line,False))


  while not toSpaceRE.search(line):
    line = rawf.next()

  if colored:
    while not colorRedSpaceRE.search(line):
      line = rawf.next()
    used, cap = getUsedCap(line)
    from_used, from_cap = cssinfo[pos][SURVIVOR][RED]
    cssinfo[pos][SURVIVOR][RED] = (used+from_used,cap+from_cap)

    while not colorBlueSpaceRE.search(line):
      line = rawf.next()
    used, cap = getUsedCap(line)
    from_used, from_cap = cssinfo[pos][SURVIVOR][BLUE]
    cssinfo[pos][SURVIVOR][BLUE] = (used+from_used,cap+from_cap)
  else:
    used, cap = getUsedCap(line,False)
    from_used, from_cap = cssinfo[pos][SURVIVOR]
    cssinfo[pos][SURVIVOR] = (used+from_used,cap+from_cap)


  # tenured space used
  while not objectSpaceRE.search(line):
    line = rawf.next()

  if colored:
    while not colorRedSpaceRE.search(line):
      line = rawf.next()
    cssinfo[pos][TENURED][RED] = (getUsedCap(line))

    while not colorBlueSpaceRE.search(line):
      line = rawf.next()
    cssinfo[pos][TENURED][BLUE] = (getUsedCap(line))
  else:
    cssinfo[pos][TENURED] = (getUsedCap(line,False))

  # perm space used
#  line = rawf.next()
#  while not objectSpaceRE.search(line):
#    line = rawf.next()
#
#  cssinfo[pos][PERM] = (getUsedCap(line,False))

def getGCSpaceInfo(bench, cfg, iter, colored=True, ceden=True):
  try:
    rawfile = rawoutfile(bench, cfg, iter)
    rawf    = open(rawfile)
  except IOError:
    print "no raw file: ", rawfile
    return None

  cur_finv = 0
  start_time = None

  vals = []
  for line in rawf:
    if vmStartRE.match(line):

      start_time = int(line.split()[2])

    elif heapBeforeRE.search(line):
      line     = heapBeforeRE.split(line)
      pts      = line[1].split()
      inv      = int(pts[1].split('=')[-1])
      finv     = int(pts[3].strip('):'))
      time     = int(pts[4].strip('[]'))
      full     = True if finv > cur_finv else False
      cur_finv = finv

      curinfo  = newCSSInfo(inv, full, colored=colored, ceden=ceden)

      curinfo[HEAP_BEFORE][REL_TIME] = time - start_time
      parseGCInfo(rawf, curinfo, HEAP_BEFORE, colored=colored, ceden=ceden)

    elif heapAfterRE.search(line):
      line = heapAfterRE.split(line)
      pts  = line[1].split()
      time = int(pts[4].strip('[]'))

      curinfo[HEAP_AFTER][REL_TIME] = time - start_time
      parseGCInfo(rawf, curinfo, HEAP_AFTER, colored=colored, ceden=ceden)
      vals.append(curinfo)

    elif heapEndOfRunRE.search(line):
      time     = int(line.split()[1].strip('[]'))
      curinfo  = newCSSInfo(-1, -1, colored=colored, ceden=ceden)

      curinfo[HEAP_BEFORE][REL_TIME] = time - start_time
      parseGCInfo(rawf, curinfo, HEAP_BEFORE, colored=colored, ceden=ceden)
      vals.append(curinfo)

  return vals

def getGCRuntimeVals(bench, cfg, iter):
  try:
    rawfile = rawoutfile(bench, cfg, iter)
    rawf    = open(rawfile)
  except IOError:
    print "no raw file: ", rawfile
    return None

  vals = []
  for line in rawf:
    if minorGCRE.match(line):
      gctype = MINOR_GC

      # sometimes GC lines get broken up in the output
      while not GCTimeRE.search(line):
        line = rawf.next()

      duration = float(line.split()[-2].split('=')[1])
      vals.append((gctype,duration))
    elif majorGCRE.match(line):
      gctype = MAJOR_GC

      # sometimes GC lines get broken up in the output
      while not GCTimeRE.search(line):
        line = rawf.next()

      duration = float(line.split()[-2].split('=')[1])
      vals.append((gctype,duration))
  return vals

def getGCRuntime(bench, cfg, iter, gctype=None):
  vals = getGCRuntimeVals(bench, cfg, iter)
  if gctype == MINOR_GC:
    return sum ( [ x[1] for x in vals if x[0] == MINOR_GC ] )
  elif gctype == MAJOR_GC:
    return sum ( [ x[1] for x in vals if x[0] == MINOR_GC ] )
  return sum ( [ x[1] for x in vals ] )

def getNumGC(bench, cfg, iter, gctype=None):
  vals = getGCRuntimeVals(bench, cfg, iter)
  if gctype == MINOR_GC:
    return len ( [ x for x in vals if x[0] == MINOR_GC ] )
  elif gctype == MAJOR_GC:
    return len ( [ x for x in vals if x[0] == MINOR_GC ] )
  return len(vals)

def getGCRuntimes(bench, cfg, iters=defiters, gctype=None):
  return [ getGCRuntime(bench, cfg, i, gctype=gctype) for i in range(iters) \
           if type(getGCRuntime(bench, cfg, i, gctype=gctype)) is float ]

def getNumGCs(bench, cfg, iters=defiters, gctype=None):
  return [ getNumGC(bench, cfg, i, gctype=gctype) for i in range(iters) \
           if type(getNumGC(bench, cfg, i, gctype=gctype)) is int ]

def getGCRuntimeStats(bench, cfg, gctype=None, iters=defiters, \
                      miniters=defminiters):
  gcruntimes = getGCRuntimes(bench, cfg, iters=iters, gctype=gctype)
  vals = [ x for x in gcruntimes if type(x) is float ]
  if len(vals) >= miniters:
    stats = {}
    stats[MEAN]  = average(gcruntimes)
    stats[STDEV] = std(gcruntimes)
    return stats
  return None

def getNumGCStats(bench, cfg, gctype=MINOR_GC, iters=defiters, \
                  miniters=defminiters):
  numgcs = getNumGCs(bench, cfg, iters=iters, gctype=gctype)
  vals = [ x for x in numgcs if type(x) is int ]
  if len(vals) >= miniters:
    stats = {}
    stats[MEAN]  = average(numgcs)
    stats[STDEV] = std(numgcs)
    return stats
  return None

def getGCPowerVals(bench, cfg, iter):
  try:
    rawfile = rawoutfile(bench, cfg, iter)
    rawf    = open(rawfile)
  except IOError:
    print "no raw file: ", rawfile
    return None

  vals = []
  for line in rawf:
    if gcpowRE.match(line):
      elapsed = float(line.split()[4])
      delta   = float(line.split()[6])
      power   = float(line.split()[8])
      vals.append((elapsed,delta,power))
#      if elapsed > 0.1:
#        vals.append((elapsed,delta,power))
  return vals

def getGCPower(bench, cfg, iter, metric=GC_POWER):

  vals = getGCPowerVals(bench, cfg, iter)
  if not vals:
    return None

  tsum = float(sum([x[0] for x in vals]))
  jsum = float(sum([x[1] for x in vals]))
  pavg = float(average([x[2] for x in vals]))
  if metric == GC_POWER:
    return (jsum / tsum)
  elif metric == GC_JOULES:
    return jsum
  elif metric == GC_TIME:
    return tsum
  elif metric == GC_POWER_AVG:
    return pavg
  return None

def getGCPowers(bench, cfg, iters=defiters, metric=GC_POWER):
  return [ getGCPower(bench, cfg, i, metric=metric) for i in range(iters) \
           if type(getGCPower(bench, cfg, i, metric=metric)) is float ]

def getGCPowerStats(bench, cfg, metric=GC_POWER, iters=defiters, miniters=defminiters):
  vals = [ x for x in getGCPowers(bench, cfg, iters=iters, metric=metric) \
           if type(x) is float ]
  if len(vals) >= miniters:
    stats = {}
    stats[MEAN]  = geomean(getGCPowers(bench, cfg, iters=iters, metric=metric))
    stats[STDEV] = std(getGCPowers(bench, cfg, iters=iters, metric=metric))
    return stats
  return None

def edprocess(benches=jvm2008s, cfgs=defcfgs, iters=defiters, verbose=V1):

  for bench, cfg, iter in expiteriter(benches, cfgs, iters):
    cmdpts = []
    cmdpts.append(edproc)
    cmdpts.append(bench)
    cmdpts.append(cfg)
    cmdpts.append(str(iter))

    print "processing emon data for %s-%s-%d" % (bench, cfg, iter)
    execmd(" ".join(cmdpts), wait=False, verbose=verbose)

def sortObjectAllocationLog(bench, cfg, iter, wait=True, verbose=False):

  cmdpts = []
  cmdpts.append(sortobjexe)
  cmdpts.append(objAllocLog(bench,cfg,iter))

  print "sorting object allocation log"
  execmd(" ".join(cmdpts), wait=wait, verbose=verbose)

def buildRefMapLog(bench, cfg, iter, wait=True, verbose=False):

  cmdpts = []
  cmdpts.append(buildrefmapexe)
  cmdpts.append(bench)
  cmdpts.append(cfg)
  cmdpts.append(str(iter))

  print "building refmap log"
  execmd(" ".join(cmdpts), wait=wait, verbose=verbose)

def clusterObjects(bench, cfg, iter, clusters=3, passes=20, wait=True, \
                   verbose=False):

  cmdpts = []
  cmdpts.append(objclusterexe)
  cmdpts.append(bench)
  cmdpts.append(cfg)
  cmdpts.append(str(iter))
  cmdpts.append("--clusters=%d"%clusters)
  cmdpts.append("--passes=%d"%passes)

  print "clustering objects"
  execmd(" ".join(cmdpts), wait=wait, verbose=verbose)

def parClusterObjects(bench, cfg, iter, clusters=[2,3,4,5,6], passes=20, \
                      verbose=False):

  for c in clusters:
    cmdpts = []
    cmdpts.append(objclusterexe)
    cmdpts.append(bench)
    cmdpts.append(cfg)
    cmdpts.append(str(iter))
    cmdpts.append("--clusters=%d"%c)
    cmdpts.append("--passes=%d"%passes)

    print "clustering objects (%d clusters)" % c
    execmd(" ".join(cmdpts), wait=False, verbose=verbose)

def drawHeatMap(bench, cfg, iter, cluster=3, sample=None, wait=True, \
                verbose=False):

  cmdpts = []
  cmdpts.append(drawheatexe)
  cmdpts.append(bench)
  cmdpts.append(cfg)
  cmdpts.append(str(iter))
  cmdpts.append("--clusters=%d"%cluster)
  if sample:
    cmdpts.append("--sample=%d"%sample)

  print "drawing heat map"
  execmd(" ".join(cmdpts), wait=wait, verbose=verbose)

def parDrawHeatMap(bench, cfg, iter, clusters=[2,3,4,5,6], samples=None, \
                   verbose=False):

  for c in clusters:
    cmdpts = []
    cmdpts.append(drawheatexe)
    cmdpts.append(bench)
    cmdpts.append(cfg)
    cmdpts.append(str(iter))
    cmdpts.append("--clusters=%d"%c)
    if samples:
      for s in samples:
        cmdpts.append("--sample=%d"%s)

        print "drawing heat map (c=%d, s=%d)" % (c, s)
        execmd(" ".join(cmdpts), wait=False, verbose=verbose)
    else:
      print "drawing heat map (c=%d, all objects)" % (c)
      execmd(" ".join(cmdpts), wait=False, verbose=verbose)

def klassClusterInfo(bench, cfg, iter, cluster=3, sample=None, wait=True, \
                     verbose=False):

  cmdpts = []
  cmdpts.append(kcinfoexe)
  cmdpts.append(bench)
  cmdpts.append(cfg)
  cmdpts.append(str(iter))
  cmdpts.append("--clusters=%d"%cluster)
  if sample:
    cmdpts.append("--sample=%d"%sample)

  print "klass cluster info (%d clusters)" % cluster
  execmd(" ".join(cmdpts), wait=wait, verbose=verbose)

def parKlassClusterInfo(bench, cfg, iter, clusters=[2,3,4,5,6], samples=None, \
                        verbose=False):

  for c in clusters:
    cmdpts = []
    cmdpts.append(kcinfoexe)
    cmdpts.append(bench)
    cmdpts.append(cfg)
    cmdpts.append(str(iter))
    cmdpts.append("--clusters=%d"%c)
    if samples:
      for s in samples:
        cmdpts.append("--sample=%d"%s)

        print "klass cluster info (c=%d, s=%d)" % (c, s)
        execmd(" ".join(cmdpts), wait=False, verbose=verbose)
    else:
      print "klass cluster info (c=%d, all objects)" % (c)
      execmd(" ".join(cmdpts), wait=False, verbose=verbose)

def dynamicHotSets(bench, cfg, iter, threshold=100, ratio=0.1, \
                    dumpreds=False, red_verbose=False, wait=True, \
                    verbose=False):

  cmdpts = []
  cmdpts.append(hotsetsexe)
  cmdpts.append(bench)
  cmdpts.append(cfg)
  cmdpts.append(str(iter))
  cmdpts.append("--hot_threshold=%d"%threshold)
  cmdpts.append("--hot_ratio=%2.3f"%ratio)
  if dumpreds:
    cmdpts.append("--dumpreds=%s"%redObjInfo(bench, cfg, iter))
    if red_verbose:
      cmdpts.append("--red_verbose")

  print "dynamic object ref sets"
  execmd(" ".join(cmdpts), wait=wait, verbose=verbose)

def getDynamicHotSets(bench, cfg, iter, rebuild=False, threshold=100, \
                       ratio=0.1, verbose=False):

  if rebuild:
    dynamicHotSets(bench, cfg, iter, threshold=threshold, ratio=ratio)

  return (load(try_open_read(hotSetPkl(bench, cfg, iter, threshold, ratio))))

def dynamicColdSets(bench, cfg, iter, threshold=1, delay=1, memory=1, \
                    quiet=True, ratio=None, size=None, clean_objd=False, \
                    clean_lives=False, max_procs=50, wait=True, verbose=False):

  cmdpts = []
  cmdpts.append(coldsetsexe)
  cmdpts.append(bench)
  cmdpts.append(cfg)
  cmdpts.append(str(iter))
  cmdpts.append("--cold_threshold=%d"%threshold)
  cmdpts.append("--val_memory=%d"%memory)
  cmdpts.append("--val_delay=%d"%delay)
  cmdpts.append("--max_procs=%d"%max_procs)
  if quiet:
    cmdpts.append("--quiet")
  if clean_objd:
    cmdpts.append("--clean_objd")
  if clean_lives:
    cmdpts.append("--clean_lives")
  if ratio:
    cmdpts.append("--hot_ratio=%2.4f"%ratio)
  if size:
    cmdpts.append("--hot_size=%d"%size)

  print "dynamic cold sets"
  execmd(" ".join(cmdpts), wait=wait, verbose=verbose)

def clean_shm(bench, cfg, iter):
  rm_rf(shmdir(bench,cfg,iter))

def clean_shmroot():
  rm_rf(shmroot)

def getDynamicColdSets(bench, cfg, iter, rebuild=False, threshold=100, \
                       verbose=False):

  if rebuild:
    dynamicColdSets(bench, cfg, iter, threshold=threshold)

  return (load(try_open_read(coldSetPkl(bench, cfg, iter, threshold))))

def dynamicHeatMap(bench, cfg, iter, wait=True, verbose=False):

  cmdpts = []
  cmdpts.append(dynamicheatexe)
  cmdpts.append(bench)
  cmdpts.append(cfg)
  cmdpts.append(str(iter))

  print "dynamic heat map"
  execmd(" ".join(cmdpts), wait=wait, verbose=verbose)

def try_open_read(fname, bin=False, quiet=False):
  try:
    if bin:
      openf = open(fname,'rb')
    else:
      openf = open(fname)
  except IOError, exc:
    if not quiet:
      print ("file: %s does not exist!" % fname)
    raise
  return openf

def try_open_write(fname, bin=False, quiet=False):
  try:
    if bin:
      openf = open(fname, 'wb')
    else:
      openf = open(fname, 'w')
  except IOError, exc:
    if not quiet:
      print ("could not open %s for writing!" % fname)
    raise
  return openf

# strange things if you start processes in ipython that you don't wait to wait
# on - use this to clear them out
#
def clearBGProcs():

  try:
    for p in bgprocs:
      pid,ret = os.waitpid(p.pid,os.WNOHANG)
      if pid != 0:
        bgprocs.remove(p)
        #print "process %d exited with code %d" % (pid,ret)
  except OSError:
    return

def customAPC(cfg):
  return (cfg in custom_apcs)

def getKnapsackAPs(bench, cfg, iter, style, cutoff):

  kaps = None
  if style == REFS:
    kaps = knapsackRefAllocPoints(bench, cfg, iter, cutoff=cutoff)
  elif style == SIZE:
    kaps = knapsackSizeAllocPoints(bench, cfg, iter, cutoff=cutoff)
  elif style == REFS_PER_VAL:
    kaps = refsPerValAllocPoints(bench, cfg, iter, cutoff=cutoff)
  elif style == LONG_LIVED:
    kaps = longLivedAllocPoints(bench, cfg, iter, cutoff=cutoff)
  return kaps

def getAPBench(bench, cfg):
  apbench = bench

  if cfg in caxrefs:
    if bench in jgfs:
      if bench in jgf_singles:
        bname, size = bench.split('-')
        apbench = "%s-L"
      elif bench in jgf_multis:
        if bench in jgfmultibts:
          bname, _, _ = bench.split('-')
        else:
          bname, _ = bench.split('-')
        apbench = ("%s-L-bt1" % bname)
    elif bench in scilarges:
      apbench = ("%s-bt4" % bench)
    elif bench in jvm2008_defs or bench == 'scimark.monte_carlo':
      apbench = "%s-bt4" % bench
    elif bench in jvm2008bts:
      apbench = bench.split('-')[0]
      apbench = ("%s-bt1" % ".".join(apbench.split('.')[:2]))
  else:
    if bench in dacapos:
      bname, size = bench.split('-')
      apbench = ("%s-small" % bname)
    elif bench in jvm2008_defs or bench == 'scimark.monte_carlo':
      apbench = "%s-bt1" % bench
    elif bench in scilarges:
      apbench = ("%s-bt1" % ".".join(apbench.split('.')[:2]+["small"]))
    elif bench in scimarkbts:
      apbench = bench.split('-')[0]
      if apbench == "scimark.monte_carlo":
        apbench = ("%s-bt1" % ".".join(apbench.split('.')[:2]))
      else:
        apbench = ("%s-bt1" % ".".join(apbench.split('.')[:2]+["small"]))
    elif bench in storebenches:
      apbench = 'StoreBench-tiny'
    elif bench in jgfs:
      if bench in jgf_singles:
        bname, size = bench.split('-')
        apbench = ("%s-A" % bname)
      elif bench in jgf_multis:
        if bench in jgfmultibts:
          bname, size, bts = bench.split('-')
          apbench = ("%s-A-bt1" % bname)
        else:
          bname, size = bench.split('-')
          apbench = ("%s-A-bt1" % bname)
  return apbench


def getObjInfos(benches=jvm2008s, cfgs=[OBJ_RED_PROFILE], iter=0):

  oinfo = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not oinfo.has_key(bench):
      oinfo[bench] = {}

    oinfo[bench][cfg] = objInfoVals(bench, cfg, iter)
  return oinfo

def objInfoVals(bench, cfg, iter=0, clean_objd=False):

  objinfof = try_open_read(objInfoLog(bench, cfg, iter))

  #makeObjDict(bench, cfg, iter=iter, clean_objd=clean_objd)
  #objd = shelve.open(shmObjDict(bench,cfg,iter))

  objinfo = []
  curinfo = curpos = curgen = curspace = curtype = None
  for line in objinfof:

    # pre-org
    if preOrgRE.match(line):
      if not curinfo is None:
        objinfo.append(curinfo)

      curinfo = {}
      curinfo[VAL]      = int(line.split()[2])
      curinfo[DURATION] = int(line.split()[7])

      for pkey in [PRE,POST]:
        curinfo[pkey] = {}
        for gkey in genKeys:
          curinfo[pkey][gkey] = {}
          for skey in spaceKeys:
            curinfo[pkey][gkey][skey] = {}
            for okey in objInfoKeys:
              curinfo[pkey][gkey][skey][okey] = 0

      curinfo[YCS] = {}
      for gkey in [YOUNG, OLD]:
        curinfo[YCS][gkey] = {}
        for skey in ycsSpaceKeys:
          curinfo[YCS][gkey][skey] = {}
          for okey in objInfoKeys:
            curinfo[YCS][gkey][skey][okey] = 0

      curpos = PRE

    # young collection stats
    elif ycsRE.match(line):
      curpos = YCS

    # post-org
    elif postOrgRE.match(line):
      curpos = POST

    # live stats
    elif livesRE.match(line):
      pts = line.split()
      curgen,curspace = [ x.upper() for x in [pts[0],pts[1]] ]
      nobjs,size,refs = [ int(x)    for x in [pts[3],pts[5],pts[7]] ]

      curinfo[curpos][curgen][curspace][LIVE_OBJECTS] = nobjs
      curinfo[curpos][curgen][curspace][LIVE_SIZE]    = size
      curinfo[curpos][curgen][curspace][LIVE_REFS]    = refs

    # hot stats
    elif hotsRE.match(line):
      pts = line.split()
      curgen,curspace = [ x.upper() for x in [pts[0],pts[1]] ]
      nobjs,size,refs = [ int(x)    for x in [pts[3],pts[5],pts[7]] ]

      curinfo[curpos][curgen][curspace][HOT_OBJECTS] = nobjs
      curinfo[curpos][curgen][curspace][HOT_SIZE]    = size
      curinfo[curpos][curgen][curspace][HOT_REFS]    = refs

    elif ycsLivesRE.match(line):
      curgen = (line.split()[2].strip(')').upper())
      curtype = LIVE

    elif ycsHotsRE.match(line):
      curgen = (line.split()[2].strip(')').upper())
      curtype = HOT

    elif space2spaceRE.match(line):

      pts = line.split()
      curspace = pts[0].strip(':').upper()
      nobjs,size,refs = [ int(x) for x in [pts[1],pts[3],pts[5]] ]

      if curtype == LIVE:
        curinfo[YCS][curgen][curspace][LIVE_OBJECTS] = nobjs
        curinfo[YCS][curgen][curspace][LIVE_SIZE]    = size
        curinfo[YCS][curgen][curspace][LIVE_REFS]    = refs
      elif curtype == HOT:
        curinfo[YCS][curgen][curspace][HOT_OBJECTS]  = nobjs
        curinfo[YCS][curgen][curspace][HOT_SIZE]     = size
        curinfo[YCS][curgen][curspace][HOT_REFS]     = refs
      else:
        print "error"
        raise SystemExit(1)

#    else:
#      line = line.strip(' *')
#
#      # ref line
#      if objrefRE.match(line):
#        curinfo[curpos][curgen][curspace][REFS] += int(line.split()[1])
#        #obj,ref = line.split()
#        if objd[obj][1][1] == curinfo[VAL]:
#          curinfo[DYING_REFS][refspace] += int(ref)
#        if curinfo[VAL] > 3:
#          raise SystemExit(1)
      
  if not curinfo is None:
    objinfo.append(curinfo)

  return objinfo

def getHotObjInfos(benches=dacapo_defs, cfgs=[OBJ_ORG_PROFILE], iter=0):

  hinfo = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not hinfo.has_key(bench):
      hinfo[bench] = {}

    hinfo[bench][cfg] = objInfoVals(bench, cfg, iter)
  return hinfo

def hotObjInfo(bench, cfg, iter=0, clean_hotd=False):

  makeHotDict(bench, cfg, iter=iter, clean_hotd=clean_hotd)
  hotd = shelve.open(shmHotDict(bench, cfg, iter))

  TOTIDX = 0
  HOTIDX = 1

  i = 0
  res = {}
  res[KLASSES] = [0,0]
  res[OBJECTS] = [0,0]
  res[SIZE]    = [0,0]
  for klass in hotd.keys():
    khot = False
    klassd = hotd[klass]
    for obj in hotd[klass].keys():
      objd = klassd[obj]
      res[OBJECTS][TOTIDX] += 1
      res[SIZE][TOTIDX]    += objd[1]
      if not objd[2] is None:
        res[OBJECTS][HOTIDX] += 1
        res[SIZE][HOTIDX]    += objd[1]
        khot = True
      if (i % 100000 == 0):
        print "here: %d" % i
      i+=1
    res[KLASSES][TOTIDX] += 1
    if khot:
      res[KLASSES][HOTIDX] += 1
  return res 

def newHKIRecord():
  hki = {}
  hki[TOTAL_OBJECTS] = 0
  hki[TOTAL_SIZE]    = 0
  hki[HOT_OBJECTS]   = 0
  hki[HOT_SIZE]      = 0
  return hki

def hotKlassInfo(bench, cfg, iter=0, clean_hotd=False):

  makeHotDict(bench, cfg, iter=iter, clean_hotd=clean_hotd)
  hotd = shelve.open(shmHotDict(bench, cfg, iter))

  TOTIDX = 0
  HOTIDX = 1

  i = 0
  res = {}
  res[AGGREGATE] = newHKIRecord()
  for klass in hotd.keys():
    res[klass] = newHKIRecord()
    for objid,objinfo in hotd[klass].items():
      res[klass][TOTAL_OBJECTS]     += 1
      res[klass][TOTAL_SIZE]        += objinfo[1]
      res[AGGREGATE][TOTAL_OBJECTS] += 1
      res[AGGREGATE][TOTAL_SIZE]    += objinfo[1]
      if not objinfo[2] is None:
        res[klass][HOT_OBJECTS]     += 1
        res[klass][HOT_SIZE]        += objinfo[1]
        res[AGGREGATE][HOT_OBJECTS] += 1
        res[AGGREGATE][HOT_SIZE]    += objinfo[1]
      if (i % 100000 == 0):
        print "here: %d" % i
      i+=1
  return res 

def ycsFromSpaces(spaces):
  return [ x for l in [ ycsFrom[s] for s in spaces ] for x in l]

def ycsToSpaces(spaces):
  return [ x for l in [ ycsTo[s] for s in spaces ] for x in l]

def getval(valinfo, pos, gens, spaces, metric):
#  tot = 0
#  for gen in gens:
#    for space in spaces:
#      print valinfo[pos][gen][space][metric]
#      tot += valinfo[pos][gen][space][metric]
#  print tot
  return sum ([ valinfo[pos][gen][space][metric] for gen in gens \
                                                 for space in spaces ] )

def getrat(valinfo, num_pos, num_gens, num_spaces, num_metric, \
           den_pos, den_gens, den_spaces, den_metric):
  den = getval(valinfo, den_pos, den_gens, den_spaces, den_metric)
  if den > 0:
    return (float(getval(valinfo, num_pos, num_gens, num_spaces, num_metric)) / den)
  return None

def getSurvRat(valinfo, gens, spaces, metric):
#  return getrat(valinfo, YCS, gens, ycsFromSpaces(spaces), metric, \
#                         PRE, gens, spaces,                metric)
  return getrat(valinfo, POST, gens, spaces, metric, \
                         PRE,  gens, spaces, metric)

def asinfo_val_agg(func, bcinfo, pos, gens, spaces, metric):
  return func( \
          [ getval(vi, pos, gens, spaces, metric) \
            for vi in bcinfo ] )

def cold_agg(func, bcinfo, gens, spaces, liverss, hotrss):
#  print [ (getval(vi, PRE, gens, spaces, liverss) - \
#            getval(vi, PRE, gens, spaces, hotrss)) for vi in bcinfo ]
  return func ( \
         [ (getval(vi, PRE, gens, spaces, liverss) - \
            getval(vi, PRE, gens, spaces, hotrss)) for vi in bcinfo ] )

#def asinfo_val_average(bcinfo, pos, gens, spaces, metric):
#  return asinfo_val_agg(average, bcinfo, pos, gens, spaces, metric)
#
#def asinfo_val_median(bcinfo, pos, gens, spaces, metric):
#  return asinfo_val_agg(median, bcinfo, pos, gens, spaces, metric)
#
#def asinfo_val_sum(bcinfo, pos, gens, spaces, metric):
#  return asinfo_val_agg(sum, bcinfo, pos, gens, spaces, metric)

#def asinfo_rat_agg(aggfunc, bcinfo, pos, gens, spaces, metric, \
#                   den_pos, den_gens, den_spaces, den_metric, \
#                   ratfunc=getrat):
#  if aggfunc in [average, median, geomean]:
#    return asinfo_agg_rat(aggfunc, bcinfo, pos, gens, spaces, metric, \
#                          den_pos, den_gens, den_spaces, den_metric, \
#                          ratfunc=ratfunc)
#  elif aggfunc == SUM_RAT:
#    return asinfo_sum_rat(bcinfo, pos, gens, spaces, metric, \
#                          den_pos, den_gens, den_spaces, den_metric)
#  else:
#    raise SystemExit(1)

def asinfo_rat_agg(aggfunc, bcinfo, pos, gens, spaces, metric, \
                   den_pos, den_gens, den_spaces, den_metric):
    return aggfunc( \
      [ getrat(vi, pos,         gens,     spaces,     metric, \
                   den_pos, den_gens, den_spaces, den_metric ) \
        for vi in bcinfo ] )

def asinfo_sum_rat(bcinfo, pos, gens, spaces, metric, \
                   den_pos, den_gens, den_spaces, den_metric):
  num = asinfo_val_agg(sum, bcinfo,     pos,     gens,     spaces,     metric)
  den = asinfo_val_agg(sum, bcinfo, den_pos, den_gens, den_spaces, den_metric)
  if den > 0:
    return float(num) / den
  return None

def asinfo_surv_rat_agg(aggfunc, bcinfo, gens, spaces, metric):
    return aggfunc( \
      [ getSurvRat(vi, gens, spaces, metric) \
        for vi in bcinfo ] )

def asinfo_surv_sum_rat(bcinfo, gens, spaces, metric):
  #num = asinfo_val_agg(sum, bcinfo, YCS, gens, ycsFromSpaces(spaces), metric)
  num = asinfo_val_agg(sum, bcinfo, POST, gens, spaces, metric)
  den = asinfo_val_agg(sum, bcinfo, PRE, gens, spaces, metric)
  if den > 0:
    return float(num) / den
  return None

#def liveObjs(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  return sumvals(valinfo, pos, gens, spaces, LIVE_OBJECTS)
#
#def liveSize(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  return sumvals(valinfo, pos, gens, spaces, LIVE_SIZE)
#
#def liveRefs(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  return sumvals ( valinfo, pos, gens, spaces, LIVE_REFS )
#
#def hotObjs(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  return sumvals(valinfo, pos, gens, spaces, HOT_OBJECTS)
#
#def hotSize(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  return sumvals(valinfo, pos, gens, spaces, HOT_SIZE)
#
#def hotRefs(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  return sumvals ( valinfo, pos, gens, spaces, HOT_REFS )

#def coldObjs(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  live_objs = liveObjs(valinfo, pos=pos, gens=gens, spaces=spaces)
#  hot_objs  = hotObjs (valinfo, pos=pos, gens=gens, spaces=spaces)
#  
#  return (live_objs - hot_objs)
#
#def coldSize(valinfo, pos=PRE, gens=genKeys, spaces=coloredSpaceKeys):
#  live_size = liveSize(valinfo, pos=pos, gens=gens, spaces=spaces)
#  hot_size  = hotSize (valinfo, pos=pos, gens=gens, spaces=spaces)
#  
#  return (live_size - hot_size)
#
#def coldObjsRat(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  den = liveObjs(valinfo, pos=pos, gens=gens, spaces=spaces)
#  if den > 0:
#    return float(coldObjs(valinfo, pos=pos, gens=gens, spaces=spaces)) / den
#  return None
#
#def coldSizeRat(valinfo, pos=PRE, gens=genKeys, spaces=spaceKeys):
#  den = liveSize(valinfo, pos=pos, gens=gens, spaces=spaces)
#  if den > 0:
#    return float(coldSize(valinfo, pos=pos, gens=gens, spaces=spaces)) / den
#  return None

#def survivingLiveObjs(valinfo, gens=coloredGenKeys, spaces=ycsSpaceKeys):
#  return sumvals(valinfo, YCS, gens, spaces, LIVE_OBJECTS)
#
#def survivingLiveSize(valinfo, gens=coloredGenKeys, spaces=ycsSpaceKeys):
#  return sumvals(valinfo, YCS, gens, spaces, LIVE_SIZE)
#
#def survivingLiveRefs(valinfo, gens=coloredGenKeys, spaces=ycsSpaceKeys):
#  return sumvals(valinfo, YCS, gens, spaces, LIVE_REFS)
#
#def survivingHotObjs(valinfo, gens=coloredGenKeys, spaces=ycsSpaceKeys):
#  return sumvals(valinfo, YCS, gens, spaces, HOT_OBJECTS)
#
#def survivingHotSize(valinfo, gens=coloredGenKeys, spaces=ycsSpaceKeys):
#  return sumvals(valinfo, YCS, gens, spaces, HOT_SIZE)
#
#def survivingHotRefs(valinfo, gens=coloredGenKeys, spaces=ycsSpaceKeys):
#  return sumvals(valinfo, YCS, gens, spaces, HOT_REFS)

#def survivingLiveObjsRat(valinfo, gens=coloredGenKeys, spaces=coloredSpaceKeys):
#  return getrat(valinfo, YCS, gens, ycsFromSpaces(spaces), LIVE_OBJECTS, \
#                         PRE, gens, spaces,                LIVE_OBJECTS)
#
#def survivingLiveSizeRat(valinfo, gens=coloredGenKeys, spaces=coloredSpaceKeys):
#  return getrat(valinfo, YCS, gens, ycsFromSpaces(spaces), LIVE_SIZE, \
#                         PRE, gens, spaces,                LIVE_SIZE)
#
#def survivingHotObjsRat(valinfo, gens=coloredGenKeys, spaces=coloredSpaceKeys):
#  return getrat(valinfo, YCS, gens, ycsFromSpaces(spaces), HOT_OBJECTS, \
#                         PRE, gens, spaces,                HOT_OBJECTS)
#
#def survivingHotSizeRat(valinfo, gens=coloredGenKeys, spaces=coloredSpaceKeys):
#  return getrat(valinfo, YCS, gens, ycsFromSpaces(spaces), HOT_SIZE, \
#                         PRE, gens, spaces,                HOT_SIZE)
#
#def survivingLiveRefsRat(valinfo, gens=coloredGenKeys, spaces=coloredSpaceKeys):
#  return getrat(valinfo, YCS, gens, ycsFromSpaces(spaces), LIVE_REFS, \
#                         PRE, gens, spaces,                LIVE_REFS)
#
#def survivingHotRefsRat(valinfo, gens=coloredGenKeys, spaces=coloredSpaceKeys):
#  return getrat(valinfo, YCS, gens, ycsFromSpaces(spaces), HOT_REFS, \
#                         PRE, gens, spaces,                HOT_REFS)

#
#def deadRefs(valinfo, gens=genKeys, spaces=spaceKeys):
#  pre_live_refs  = liveRefs(valinfo, pos=PRE,  gens=gens, spaces=spaces)
#  post_live_refs = liveRefs(valinfo, pos=POST, gens=gens, spaces=spaces)
#  
#  return (pre_live_refs - post_live_refs)
#
#def deadRefRat(valinfo, gens=genKeys, spaces=spaceKeys):
#  den = liveRefs(valinfo, pos=PRE, gens=gens, spaces=spaces)
#  if den > 0:
#    return (float(deadRefs(valinfo, gens=gens, spaces=spaces)) / den)
#  return None

#def deadLiveObjs(valinfo, gens=genKeys, spaces=spaceKeys):
#  pre_live_objs  = liveObjs(valinfo, pos=PRE,  gens=gens, spaces=spaces)
#  post_live_objs = liveObjs(valinfo, pos=POST, gens=gens, spaces=spaces)
#  
#  return (pre_live_objs - post_live_objs)
#
#def deadLiveObjsRat(valinfo, gens=genKeys, spaces=spaceKeys):
#  den = liveObjs(valinfo, pos=PRE, gens=gens, spaces=spaces)
#  if den > 0:
#    return (float(deadLiveObjs(valinfo, gens=gens, spaces=spaces)) / den)
#  return None
#
#def deadLiveSize(valinfo, gens=genKeys, spaces=spaceKeys):
#  pre_live_size  = liveSize(valinfo, pos=PRE,  gens=gens, spaces=spaces)
#  post_live_size = liveSize(valinfo, pos=POST, gens=gens, spaces=spaces)
#  
#  return (pre_live_size - post_live_size)
#
#def deadLiveSizeRat(valinfo, gens=genKeys, spaces=spaceKeys):
#  den = liveSize(valinfo, pos=PRE, gens=gens, spaces=spaces)
#  if den > 0:
#    return (float(deadLiveSize(valinfo, gens=gens, spaces=spaces)) / den)
#  return None
#
#def deadHotObjs(valinfo, gens=genKeys, spaces=spaceKeys):
#  pre_hot_objs  = hotObjs(valinfo, pos=PRE,  gens=gens, spaces=spaces)
#  post_hot_objs = hotObjs(valinfo, pos=POST, gens=gens, spaces=spaces)
#  
#  return (pre_hot_objs - post_hot_objs)
#
#def deadHotObjsRat(valinfo, gens=genKeys, spaces=spaceKeys):
#  den = hotObjs(valinfo, pos=PRE, gens=gens, spaces=spaces)
#  if den > 0:
#    return (float(deadHotObjs(valinfo, gens=gens, spaces=spaces)) / den)
#  return None
#
#def deadHotSize(valinfo, gens=genKeys, spaces=spaceKeys):
#  pre_hot_size  = hotSize(valinfo, pos=PRE,  gens=gens, spaces=spaces)
#  post_hot_size = hotSize(valinfo, pos=POST, gens=gens, spaces=spaces)
#  
#  return (pre_hot_size - post_hot_size)
#
#def deadHotSizeRat(valinfo, gens=genKeys, spaces=spaceKeys):
#  den = hotSize(valinfo, pos=PRE, gens=gens, spaces=spaces)
#  if den > 0:
#    return (float(deadHotSize(valinfo, gens=gens, spaces=spaces)) / den)
#  return None
#
#def deadRefs(valinfo, gens=genKeys, spaces=spaceKeys):
#  pre_live_refs  = liveRefs(valinfo, pos=PRE,  gens=gens, spaces=spaces)
#  post_live_refs = liveRefs(valinfo, pos=POST, gens=gens, spaces=spaces)
#  
#  return (pre_live_refs - post_live_refs)
#
#def deadRefRat(valinfo, gens=genKeys, spaces=spaceKeys):
#  den = liveRefs(valinfo, pos=PRE, gens=gens, spaces=spaces)
#  if den > 0:
#    return (float(deadRefs(valinfo, gens=gens, spaces=spaces)) / den)
#  return None

#
#def liveObjsRat(valinfo, num_pos=YCS, num_gens=coloredGenKeys, \
#  num_spaces=coloredSpaceKeys, den_pos=PRE, den_gens=coloredGenKeys, \
#  den_spaces=coloredSpaceKeys):
#  den = liveObjs(valinfo, pos=den_pos, gens=den_gens, spaces=den_spaces)
#  if den > 0:
#    return (float(liveObjs(valinfo, pos=num_pos, gens=num_gens, spaces=num_spaces)) / den)
#  return None
#
#def liveSizeRat(valinfo, num_pos=YCS, num_gens=coloredGenKeys, \
#  num_spaces=coloredSpaceKeys, den_pos=PRE, den_gens=coloredGenKeys, \
#  den_spaces=coloredSpaceKeys):
#  den = liveSize(valinfo, pos=den_pos, gens=den_gens, spaces=den_spaces)
#  if den > 0:
#    return (float(liveSize(valinfo, pos=num_pos, gens=num_gens, spaces=num_spaces)) / den)
#  return None
#
#def coloredRefRat(valinfo, num_pos=PRE, num_gens=coloredGenKeys, \
#  num_spaces=coloredSpaceKeys, den_pos=PRE, den_gens=coloredGenKeys, \
#  den_gens=[YOUNG], den_spaces=coloredSpaceKeys):
#  den = liveRefs(valinfo, pos=pos, gens=den_gens, spaces=den_spaces)
#  if den > 0:
#    return (float(liveRefs(valinfo, pos=pos, gens=num_gens, spaces=num_spaces)) / den)
#  return None

def makeObjDict(bench, cfg, iter=0, clean_objd=False, wklass=False, wap=False):

  # create the objd if the pkl file does not exist or the rawfile is newer
  # than the pickled objd
  #
  shmobjd = shmObjDict(bench,cfg,iter)
  rawfile = rawoutfile(bench,cfg,iter)

  if os.path.exists(shmobjd):
    if clean_objd or ( os.path.getmtime(shmobjd) < os.path.getmtime(rawfile) ):
      rm_f(shmobjd)

  if not os.path.exists(shmobjd):

    print "getting deads info"
    deadsf = try_open_read(deadObjLog(bench, cfg, iter))

    deads = {}
    cur_val = 0
    for line in deadsf:
      if deadsHeaderRE.match(line):
        cur_val = int(line.split()[-1])
      elif deadsDoneRE.match(line):
        cur_val = -1
      else:
        objid = int(line.strip())
        if deads.has_key(objid) and deads[objid] != cur_val:
          print "warning: obj %d died twice in different intervals (%d and %d)" % \
                (objid, deads[objid], cur_val)
        deads[int(line.strip())] = cur_val
    deadsf.close()

    print "getting object info"
    allocf = try_open_read(objAllocLog(bench, cfg, iter))

    for line in allocf:
      if headerEndRE.match(line):
        break

    mkdir_p (shmdir(bench,cfg,iter))
    objd = shelve.open(shmobjd)
    for i,line in enumerate(allocf):
      if objrefRE.match(line):
        pts = line.split()
        id,cval,size = [ int(x) for x in pts[:3] ]
        dval = deads[id] if deads.has_key(id) else -1
        if wklass:
          objd[str(id)] = (klass, size, (cval, dval))
        else:
          objd[str(id)] = (size, (cval, dval))
      elif klassRE.match(line):
        klass = line.split()[1]
      if i%100000 == 0:
        print i
    allocf.close()
    objd.close()

def makeHotDict(bench, cfg, iter=0, clean_hotd=False):

  # create the objd if the pkl file does not exist or the rawfile is newer
  # than the pickled objd
  #
  shmhotd = shmHotDict(bench,cfg,iter)
  rawfile = rawoutfile(bench,cfg,iter)

  if os.path.exists(shmhotd):
    if clean_hotd or ( os.path.getmtime(shmhotd) < os.path.getmtime(rawfile) ):
      rm_f(shmhotd)

  if not os.path.exists(shmhotd):

    print "getting deads info"
    deadsf = try_open_read(deadObjLog(bench, cfg, iter))

    deads = {}
    cur_val = 0
    for line in deadsf:
      if deadsHeaderRE.match(line):
        cur_val = int(line.split()[-1])
      elif deadsDoneRE.match(line):
        cur_val = -1
      else:
        deads[int(line.strip())] = cur_val
    deadsf.close()

    print "getting object info"
    allocf = try_open_read(objAllocLog(bench, cfg, iter))

    for line in allocf:
      if headerEndRE.match(line):
        break

    mkdir_p (shmdir(bench,cfg,iter))
    hotd = shelve.open(shmhotd, writeback=True)
    for i,line in enumerate(allocf):
      if objrefRE.match(line):
        id,cval,size = [ int(x) for x in line.split()[:3] ]
        dval = deads[id] if deads.has_key(id) else -1
        if hotd[klass].has_key(str(id)):
          print "duplicate id! %d" % id
        hotd[klass][str(id)] = ((cval,dval), size, None)
      elif klassRE.match(line):
        klass = line.split()[1]
        if not hotd.has_key(klass):
          hotd[klass] = {}
      if i%100000 == 0:
        print i
    allocf.close()


    objinfof = try_open_read(objInfoLog(bench, cfg, iter))
    val = None
    for line in objinfof:
      if preOrgRE.match(line):  
        val = int(line.split()[2])
      elif hotKlassRE.match(line):
        curklass = line.split()[1]
      else:
        line = line.strip(' *')

        # ref line
        if objrefRE.match(line):
          objstr,refsstr = line.split()

          obj  = int(objstr)
          refs = int(refsstr)

          oldinfo = hotd[curklass][objstr]
          if oldinfo[2] is None:
            newrefs = [(val, refs)]
          else:
            newrefs = (oldinfo[2] + [(val, refs)])
          hotd[curklass][objstr] = (oldinfo[0], oldinfo[1], newrefs)

    objinfof.close()
    hotd.close()

def getMaxRun(vals):

  if not vals:
    return 0

  maxrun = 1
  tmpmax = 1
  for i,(v,r) in enumerate(vals[:-1]):
    if (v+1) == vals[i+1][0]:
      tmpmax += 1
      if tmpmax > maxrun:
        maxrun = tmpmax
    else:
      tmpmax = 1
      
  return maxrun

def getMaxHole(vals):

  if not vals:
    return 0

  maxhole = 0
  tmpmax  = 0
  for i,(v,r) in enumerate(vals[:-1]):
    tmpmax = ((vals[i+1][0] - v) - 1)
    if tmpmax > maxhole:
      maxhole = tmpmax
      
  return maxhole

def getAccessInfoDicts(benches=dacapo_smalls, cfgs=[OBJ_INFO_INTERVAL], iter=0, \
                       clean=False):

  aidict = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not aidict.has_key(bench):
      aidict[bench] = {}

    aidict[bench][cfg] = accessInfoDict(bench, cfg, iter, clean=clean)
  return aidict


def accessInfoDict(bench, cfg, iter=0, clean=False, dodups=False):

  # create the objd if the pkl file does not exist or the rawfile is newer
  # than the pickled objd
  #
  rawfile = rawoutfile(bench,cfg,iter)

  makeObjDict(bench, cfg, iter, clean_objd=clean)
  objd = shelve.open(shmObjDict(bench,cfg,iter))

  endval = 0
  objinfof = try_open_read(objInfoLog(bench, cfg, iter))
  for line in objinfof:
    if endRunRE.match(line):
      endval = (int(line.split()[2])+1)
      break
  objinfof.seek(0,0)

  accessd             = {}
  accessd[OBJECTS]    = {}
  accessd[TOTAL_VALS] = endval

  val  = None
  if dodups:
    dups = {}
  for line in objinfof:
    if oicValRE.match(line):  
      if dodups and dups:
        print "dups in val: %d" % val
        for obj in dups.keys():
          print "  obj: %8d  refs: %8d  dups: %d" % \
          (obj, dups[obj][1], dups[obj][0])
      val = int(line.split()[2])
      print "val: %5d" % val

      dups = {}
    else:
      line = line.strip(' *')

      # ref line
      if objrefRE.match(line):
        objstr,refsstr = line.split()

        obj  = int(objstr)
        refs = int(refsstr)

        oinfo   = None
        try:
          tmpinfo = objd[objstr]
        except:
          print "ref to unknown object: %s" % objstr
          continue

        # mark objects that never die with endval
        if tmpinfo[1][1] == -1:
          oinfo = (tmpinfo[0], (tmpinfo[1][0], endval))
        else:
          oinfo = tmpinfo

        if not accessd[OBJECTS].has_key(obj):
          newrec                = {}
          newrec[SIZE]          = oinfo[0]
          newrec[LIVE_VALS]     = oinfo[1]
          newrec[HOT_VALS]      = []
          accessd[OBJECTS][obj] = newrec

        objrec = accessd[OBJECTS][obj]
        if objrec[HOT_VALS]:
          if objrec[HOT_VALS][-1][0] == val:
            if dodups:
              if not dups.has_key(obj):
                dups[obj] = (0,refs)
              cur       = dups[obj]
              dups[obj] = (cur[0]+1,cur[1])
            #print "warning: skipping duplicate ref for obj: %5d  " % obj,
            #print "(val=%d, refs=%d)" % (val,refs)
            continue
        objrec[HOT_VALS].append((val,refs))

  objinfof.close()
  if dodups and dups:
    print "dups in val: %d" % val
    for obj in dups.keys():
      print "  obj: %8d, refs: %8d, dups: %d" % \
      (obj, dups[obj][1], dups[obj][0])


#  for obj in accessd.keys():
#    #accessd[obj][MAX_HOT]  = getMaxRun(accessd[obj][HOT_VALS])
#    accessd[obj][MAX_COLD] = getMaxHole(accessd[obj][HOT_VALS])
#    birth    = accessd[obj][LIVE_VALS][0]
#    death    = accessd[obj][LIVE_VALS][1]
#    hot_time = len(accessd[obj][HOT_VALS])
#    try:
#      life_time = (death-birth+1)
#      if life_time <= 0:
#        raise ValueError
#      accessd[obj][HOT_RATE] = (float(hot_time) / life_time)
#    except ValueError as e:
#      print "error: lifetime is zero! obj=%d, (%d, %d)" % \
#            (obj, birth, death)
#
#  accessd[AGGREGATE] = {}
#  accessd[AGGREGATE][TOTAL_TIME] = endval
#  accessd[AGGREGATE][LIVE_TIME]  = []
#  accessd[AGGREGATE][HOT_TIME]   = []
#  objkeys = [ x for x in accessd.keys() if not x == AGGREGATE ]
#  for obj in objkeys:
#    birth    = accessd[obj][LIVE_VALS][0]
#    death    = accessd[obj][LIVE_VALS][1]
#    hot_time = len(accessd[obj][HOT_VALS])
#    accessd[AGGREGATE][LIVE_TIME].append((death-birth))
#    accessd[AGGREGATE][HOT_TIME].append(hot_time)
#
#  accessd[AGGREGATE][LIVE_TIME] = average(accessd[AGGREGATE][LIVE_TIME])
#  accessd[AGGREGATE][HOT_TIME]  = average(accessd[AGGREGATE][HOT_TIME])

  return accessd

def hrhBucket(hrd):
  bucket = int(floor(hrd[HOT_RATE]*10))
  if bucket < 0:
    return 0
  elif bucket > 9:
    return 9
  return bucket

def hotRateInfo(benches=dacapo_defs, cfgs=[OBJ_INFO_DEFAULT], iter=0, \
  clean_objd=False):

  hri = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not hri.has_key(bench):
      hri[bench] = {}

    hri[bench][cfg] = {}
    makeObjDict(bench, cfg, iter, clean_objd=clean_objd)
    objd = shelve.open(shmObjDict(bench,cfg,iter))
    hrd  = hotRateDict(bench, cfg, iter)

    hrhisto = {}
    for obj in hrd.keys():
      bucket = hrhBucket(hrd[obj])
      if not hrhisto.has_key(bucket):
        hrhisto[bucket] = 0
      hrhisto[bucket] += 1

    hri[bench][cfg][HOT_RATE_HISTO] = {}
    for bucket in hrhisto.keys():
      hri[bench][cfg][HOT_RATE_HISTO][bucket] = \
        (hrhisto[bucket], float(hrhisto[bucket]) / hri[bench][cfg][HOT_OBJECTS])
  return hri

def apCoverRecord(apdict, allinfo, cutkey):

  try:
    apkeys = [ x[0] for x in \
             sorted( [ (key,apdict[key][REFS]) for key in apdict.keys() ], \
                       key=itemgetter(1), reverse=True ) ]
  except KeyError:
    print "bad sort key: %s" % sortkey
    raise

  covrec = {}
  for myrat in coverrats[cutkey]:
    covrec[myrat] = {}

    tots = {}
    tots[SIZE]    = 0
    tots[OBJECTS] = 0
    tots[REFS]    = 0

    for i,apkey in enumerate(apkeys):
      apinfo      = apdict[apkey]

      tots[SIZE]    += (apinfo[SIZE] / 1024)
      tots[OBJECTS] += apinfo[OBJECTS]
      tots[REFS]    += apinfo[REFS]

      running_rat = float(tots[cutkey]) / float(allinfo[cutkey])
      if running_rat > myrat:
        break

    rats = {}
    rats[SIZE]    = float(tots[SIZE])    / float(allinfo[SIZE])
    rats[OBJECTS] = float(tots[OBJECTS]) / float(allinfo[OBJECTS])
    rats[REFS]    = float(tots[REFS])    / float(allinfo[REFS])
    rats[APS]     = float(i) / float(len(apkeys))

    covrec[myrat] = {}
    covrec[myrat][SIZE]    = (tots[SIZE],    allinfo[SIZE],    rats[SIZE])
    covrec[myrat][OBJECTS] = (tots[OBJECTS], allinfo[OBJECTS], rats[OBJECTS])
    covrec[myrat][REFS]    = (tots[REFS],    allinfo[REFS],    rats[REFS])
    covrec[myrat][APS]     = (i,             len(apkeys),      rats[APS])

  return covrec

def allocPointCoverInfo(res=None, benches=jvm2008s, cfgs=[OBJ_INFO_DEFAULT], \
  iter=0):

  if not res:
    res = getAllocPointDicts(benches=benches, cfgs=cfgs, iter=iter)

  apcinfo = {}
  for bench,cfg in expiter(benches, cfgs):
    bcres = res[bench][cfg]

    if not apcinfo.has_key(bench):
      apcinfo[bench] = {}

    if not apcinfo[bench].has_key(cfg):
      apcinfo[bench][cfg] = {}

    allinfo = {}
    allinfo[SIZE]    = sum ( [ (bcres[key][SIZE]/1024) for key in bcres.keys() ] )
    allinfo[OBJECTS] = sum ( [ bcres[key][OBJECTS] for key in bcres.keys() ] )
    allinfo[REFS]    = sum ( [ bcres[key][REFS]    for key in bcres.keys() ] )

    if any( [ True if allinfo[key] == 0 else False for key in allinfo.keys() ] ):
      print "invalid zero value in all_info:"
      print all_info
      raise SystemExit(1)

    apcinfo[bench][cfg][SIZE]    = apCoverRecord(bcres, allinfo, SIZE)
    apcinfo[bench][cfg][OBJECTS] = apCoverRecord(bcres, allinfo, OBJECTS)
    apcinfo[bench][cfg][REFS]    = apCoverRecord(bcres, allinfo, REFS)

  return apcinfo

def allocPointSpaceInfo(res=None, benches=jvm2008s, cfgs=[OBJ_INFO_GUIDED], \
  iter=0, style=REFS, cutoff=0.10):

  if not res:
    res = getAllocPointDicts(benches=benches, cfgs=cfgs, iter=iter, \
                             icolor=True, style=style, cutoff=cutoff)

  # for each allocation point, get its classification
  # report size, number of objects in different spaces
  # references to objects in different spaces
  apsinfo = {}
  for bench,cfg in expiter(benches, cfgs):
    bcres = res[bench][cfg]

    if not apsinfo.has_key(bench):
      apsinfo[bench] = {}

    if not apsinfo[bench].has_key(cfg):
      apsinfo[bench][cfg] = {}

    spaceinfo = {}

    spaceinfo[RED]           = {}
    spaceinfo[RED][SIZE]     = 0
    spaceinfo[RED][OBJECTS]  = 0
    spaceinfo[RED][REFS]     = 0

    spaceinfo[BLUE]          = {}
    spaceinfo[BLUE][SIZE]    = 0
    spaceinfo[BLUE][OBJECTS] = 0
    spaceinfo[BLUE][REFS]    = 0

    for rec in bcres:
      spaceinfo[bcres[rec][INIT_COLOR]][SIZE]    += bcres[rec][SIZE]
      spaceinfo[bcres[rec][INIT_COLOR]][OBJECTS] += bcres[rec][OBJECTS]
      spaceinfo[bcres[rec][INIT_COLOR]][REFS]    += bcres[rec][REFS]

    apsinfo[bench][cfg] = spaceinfo

  return apsinfo

def getEndval(bench, cfg=OBJ_INFO_INTERVAL, iter=0, quiet=False):
  endval = 0
  try:
    endvalf = try_open_read(endValFile(bench, cfg, iter))
    endval = int(endvalf.read().strip())
    endvalf.close()
  except IOError:
    if not quiet:
      print "finding endval ... ",
    sys.stdout.flush()
    objinfof = try_open_read(objInfoLog(bench, cfg, iter))
    for line in objinfof:
      if oicValRE.match(line):
        endval = (int(line.split()[2])+1)
    endvalf = try_open_write(endValFile(bench, cfg, iter))
    endvalf.write('%d\n'%endval)
    endvalf.close()
    objinfof.close()
  if not quiet:
    print "%s-%s-i%d endval=%d" % (bench, cfg, iter, endval)
  return endval

def getOverflows(bench, cfg, iter):
  try:
    rawfile = rawoutfile(bench, cfg, iter)
    rawf    = open(rawfile)
  except IOError:
    print "no output file: ", rawfile
    return []

  ovfs = []
  for line in rawf:
    if overflowRE.match(line):
      oid = line.split()[-1]
      print "warning: %s overflowed" % oid
      ovfs.append(oid)
  return ovfs

def getAllocPointDicts(benches=jvm2008s, cfgs=[OBJ_INFO_DEFAULT], iter=0, \
                       icolor=False, style=REFS, cutoff=0.10, clean=False):

  apdict = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not apdict.has_key(bench):
      apdict[bench] = {}

    apdict[bench][cfg] = allocPointDict(bench, cfg, iter, clean=clean, \
                                        icolor=icolor, style=style, \
                                        cutoff=cutoff)

  return apdict

def allocPointMap(bench, cfg, iter=0):
  apmap = {}
  apmf  = try_open_read(apMapLog(bench, cfg, iter)) 
  apmf.next()
  for line in apmf:
    if line[0].isdigit():
      pts = line.split()
      apmap[int(pts[0])] = {}
      if pts[-1] == "unknown-AP":
        apmap[int(pts[0])][NAME]  = pts[-1]
        apmap[int(pts[0])][KLASS] = -1
      else:
        apmap[int(pts[0])][NAME]  = " ".join(pts[-3:])
        apmap[int(pts[0])][KLASS] = int(pts[1])
  apmf.close()
  return apmap

def allocPointDict(bench, cfg, iter=0, clean=False, app_only=False):

  if not clean:
    try:
      apdict = load(open(allocDictPkl(bench, cfg, iter)))
      return apdict    
    except:
      print "could not open apd pkl for %s-%s-i%d, regenerating ..." % \
            (bench, cfg, iter)

  print "getting alloc point info"
  apinfo = allocPointInfo(bench, cfg, iter, clean=clean, app_only=app_only)
  dump(apinfo[FULL][APD], open(allocDictPkl(bench, cfg, iter), 'w'))
  return apinfo[FULL][APD]

#def allocPointDict(bench, cfg, iter=0, clean=False, icolor=False, \
#                   style=REFS, cutoff=0.10):
#
#  if not clean:
#    try:
#      apdict = load(open(allocDictPkl(bench, cfg, iter)))
#      return apdict    
#    except:
#      print "could not open apd pkl for %s-%s-i%d, regenerating ..." % \
#            (bench, cfg, iter)
#
#  print "getting alloc point map"
#  apmap = allocPointMap(bench, cfg, iter)
#
#  print "getting obj dict"
#  makeObjDict(bench, cfg, iter, clean_objd=clean)
#  objd = shelve.open(shmObjDict(bench,cfg,iter))    
#
#  endval = getEndval(bench, cfg, iter)
#
#  if icolor:
#    print "getting initial coloring info"
#    if not apcfgs.has_key(cfgs[cfg][BC_RUNCFG]):
#      print "no initial colors for config: %s" % cfg
#      return
#
#    icbench = getAPBench(bench, cfg)
#    iccfg   = apcfgs[cfgs[cfg][BC_RUNCFG]]
#    icaps   = allocPointDict(icbench, iccfg)
#
#    if customAPC(iccfg):
#      cutoff = float("0.%s" % cfg.split('_')[-1])
#      style  = REFS if cfg.split('_')[-2] == 'ref' else SIZE
#
#    print icbench, iccfg, style, cutoff
#    blue_keys = getKnapsackAPs(icbench, iccfg, 0, style, cutoff)
#    blues = [ icaps[k][ALLOC_POINT] for k in blue_keys ]
#
#  print "getting alloc point info"
#  allocf = try_open_read(objAllocLog(bench, cfg, iter))
#
#  for line in allocf:
#    if headerEndRE.match(line):
#      break
#
#  ovfs = getOverflows(bench,cfg,iter)
#
#  mkdir_p (shmdir(bench,cfg,iter))
#  apdict = {}
#  cnt = 0
#  foo=0
#  for i,line in enumerate(allocf):
#    if cnt > 1000:
#      raise SystemExit(1)
#    if objrefRE.match(line):
#      pts = line.split()
#      start,end = objd[pts[0]][1]
#      if end == -1:
#        end = endval
#      lifespan = (end-start)+1
#      if lifespan == 0:
#        print "obj %s has 0 lifespan! (end=%d, start=%d)" % (pts[0],end,start)
#        continue
#      size,redref,blueref = [ int(x) for x in pts[2:5] ]
#      apkey = int(pts[5])
#
#      for oid in ovfs:
#        if pts[0] == oid:
#          redref += (2**31)
#
#      if not apdict.has_key(apkey):
#        if not apmap.has_key(apkey):
#          print "unknown AP method: %d for obj: %d (size=%d, refs=%d)" % \
#                (apkey, int(pts[0]), size, (redref+blueref))
#          cnt+=1
#          continue
#        apdict[apkey] = {}
#        apdict[apkey][ALLOC_POINT]  = apmap[apkey]
#        apdict[apkey][KLASS]        = set([klass])
#        apdict[apkey][RED_REFS]     = 0
#        apdict[apkey][BLUE_REFS]    = 0
#        apdict[apkey][REFS]         = 0
#        apdict[apkey][LIVE_VALS]    = 0
#        apdict[apkey][OBJECTS]      = 0
#        apdict[apkey][SIZE]         = 0
#
#        if icolor:
#          if apmap[apkey] in blues: 
#            apdict[apkey][INIT_COLOR] = BLUE
#          else:
#            apdict[apkey][INIT_COLOR] = RED
#
#      if not apdict.has_key(apkey) and not apmap.has_key(apkey[0]):
#        print "unknown AP method: %d for obj: %d (size=%d, refs=%d)" % \
#              (apkey, int(pts[0]), size, (redref+blueref))
#        cnt+=1
#        continue
#
#      #cur_rpv    = apdict[apkey][REFS_PER_VAL]
#      #cur_vals   = apdict[apkey][LIVE_VALS]
#      #rpv        = float((cur_rpv  * cur_vals) + (total_refs)) / \
#      #                   (cur_vals + lifespan)
#
#      total_refs = redref+blueref
##      old_start, old_end = apdict[apkey][ACTIVE_VALS]
##      new_start, new_end = old_start, old_end
##      if start < old_start:
##        new_start = start
##      if end > old_end:
##        new_end = end
#
#      apdict[apkey][KLASS].add(klass)
#      apdict[apkey][RED_REFS]   += redref
#      apdict[apkey][BLUE_REFS]  += blueref
#      apdict[apkey][OBJECTS]    += 1
#      apdict[apkey][SIZE]       += size
#      apdict[apkey][REFS]       += total_refs
#      apdict[apkey][LIVE_VALS]  += lifespan
#      #apdict[apkey][ACTIVE_VALS]   = (new_start,new_end)
#      #apdict[apkey][REFS_PER_VAL]  = rpv
#
#    elif klassRE.match(line):
#      klass = line.split()[1]
#    if i%100000 == 0:
#      print i
#  allocf.close()
#  dump(apdict, open(allocDictPkl(bench, cfg, iter), 'w'))
#
#  return apdict

def getColdKeys(bench, cfg):

  apcf = try_open_read(apcCmdsFile(bench, cfg))
  apcf.next()
  cold_keys = []
  for line in apcf:
    if int(line.split()[-1]) == 1:
      cold_keys.append(" ".join(line.split()[1:4]))
  return cold_keys

def writeCutoffApcCmdsFile(bench, cfg, apdict=None, cutoff=20, quiet=True, short=True):

  if not apdict:
    apdict = allocPointDict(bench, cfg)

  mkdir_p(expdir(bench, cfg))
  apcf = try_open_write(apcCmdsFile(bench, cfg))
  if quiet:
    outf = apcf
  else:
    outf = TeeFile(sys.stdout,apcf)

  aps = [ x[0] for x in sorted ( [ (key, apdict[key][FULL_REFS_IDX]) for key in apdict.keys() ],
                                    key=itemgetter(1), reverse=True) ]

  if short:
    for i,ap in enumerate(aps):
      if i < cutoff:
        #print >> outf, ("%s %s %d 0" % (apccmd, apdict[ap][METHOD_NAME], ap[1]))
        print >> outf, ("%s %s 0" % (apccmd, apdict[ap][ALLOC_POINT]))
  else:
    for i,ap in enumerate(aps):
      print >> outf, ("%s %s" % (apccmd, apdict[ap][ALLOC_POINT])),
      if i < cutoff:
        print >> outf, " 0"
      else:
        print >> outf, " 1"

def writeKnapsackApcCmdsFile(bench, cfg, cutoff=0.10, quiet=True, \
  style=REFS, short=False):

  apmap = allocPointMap(bench, cfg)
  mkdir_p(expdir(bench, cfg))
  apcf = try_open_write(apcCmdsFile(bench, cfg))
  if quiet:
    outf = apcf
  else:
    outf = TeeFile(sys.stdout,apcf)

  cold_keys = getKnapsackAPs(bench, cfg, 0, style, cutoff)
  print >> outf, "quiet"
  if short:
    for i,key in enumerate(apmap.keys()):
      if "unknown-AP" == apmap[key]:
        continue
      if not key in cold_keys:
        print >> outf, ("%s %s 0" % (apccmd, apmap[key][NAME]))
  else:
    for i,key in enumerate(apmap.keys()):
      if "unknown-AP" == apmap[key]:
        continue
      print >> outf, ("%s %s" % (apccmd, apmap[key][NAME])),
      if not key in cold_keys:
        print >> outf, " 0"
      else:
        print >> outf, " 1"

#def knapsackSizeAllocPoints(bench, cfg=OBJ_INFO_DEFAULT, iter=0, cutoff=0.10):
#
#  apd = allocPointDict(bench, cfg, iter)
#
#  total_refs = sum( [ apd[key][SIZE] for key in apd.keys() ] )
#  wgts = [ (key, (float(apd[key][SIZE]) / total_refs)) \
#          for key in apd.keys() ]
#
#  wgt_incs = [ x for x in [ (float(i)/10000) for i in range(10000) ] if x <= cutoff ]
#
#  m = []
#  m.append([])
#  for j in range(len(wgt_incs)):
#    #print "j: %d" % j
#    m[0].append(0)
#
#  for i,wgt in enumerate(wgts,start=1):
#    m.append([])
#    for j,inc in enumerate(wgt_incs):
#      #print "i: %4d, j: %4d, wgt[1]: %4.4f, inc: %4.4f" % (i,j, wgt[1], inc),
#      if wgt[1] <= inc:
#        x = int (float('%s' % float('%.3g' % (inc-wgt[1]))) * 10000)
#        #print "  p1: x: %4d" % x,
#        #print "  p1: a: %13d, b: %13d" % (m[i-1][j], m[i-1][x] + apd[wgt[0]][REFS])
#        m[i].append(max(m[i-1][j], m[i-1][x] + apd[wgt[0]][REFS]))
#      else:
#        #print "  p2: m[i-1][j]: %13d" % (m[i-1][j])
#        m[i].append(m[i-1][j])
##    if i>20:
##      return
##    if wgt[1] > 0.1:
##      print "wgt[1]: %4.4f, m[%d][%d] = %d" % (wgt[1],i,j,m[i][j])
##    if i % 100 == 0:
##      print "m[%d][%d] = %d" % (i,j,m[i][j])
#
#  #print "soln: m[%d][%d] = %d" % (len(m)-1, len(m[-1])-1, m[-1][-1])
#
#  rec = []
#  i = len(wgts)
#  j = (len(wgt_incs)-1)
#  while i > 0:
##    print "i: %d, j: %d, len(m): %d, len(m[i]): %d" %\
##          (i, j, len(m), len(m[i]))
#    if m[i][j] != m[i-1][j]:
#      rec.append(wgts[i-1][0])
#      j -= int (float('%s' % float('%.3g' % (wgts[i-1][1]))) * 10000)
#    i -= 1
#
##  res = {}
##  for key in rec:
##    res[key] = apd[key]
##
##  hot_size   = sum ([ res[key][SIZE] for key in res.keys() ])
##  hot_refs   = sum ([ res[key][REFS] for key in res.keys() ])
##  total_size = sum ([ apd[key][SIZE] for key in apd.keys() ])
##  total_refs = sum ([ apd[key][REFS] for key in apd.keys() ])
##  size_rat   = float(hot_size) / total_size
##  ref_rat    = float(hot_refs) / total_refs
##
##  print ""
##  print "hot size:   ", ("%d"%hot_size).rjust(13),    "  hot refs:   ", ("%d"%hot_refs).rjust(13)
##  print "total size: ", ("%d"%total_size).rjust(13),  "  total refs: ", ("%d"%total_refs).rjust(13)
##  print "size ratio: ", ("%6.6f"%size_rat).rjust(13), "  ref ratio:  ", ("%6.6f"%ref_rat).rjust(13)
#
#  return rec
#
#def knapsackRefAllocPoints(bench, cfg=OBJ_INFO_DEFAULT, iter=0, cutoff=0.10):
#
#  apd = allocPointDict(bench, cfg, iter)
#
#  total_refs = sum( [ apd[key][REFS] for key in apd.keys() ] )
#  wgts = [ (key, (float(apd[key][REFS]) / total_refs)) \
#          for key in apd.keys() ]
#
#  wgt_incs = [ x for x in [ (float(i)/10000) for i in range(10000) ] if x <= cutoff ]
#
#  m = []
#  m.append([])
#  for j in range(len(wgt_incs)):
#    #print "j: %d" % j
#    m[0].append(0)
#
#  for i,wgt in enumerate(wgts,start=1):
#    m.append([])
#    for j,inc in enumerate(wgt_incs):
#      print "i: %4d, j: %4d, wgt[1]: %4.4f, inc: %4.4f" % (i,j, wgt[1], inc),
#      if wgt[1] <= inc:
#        x = int (float('%s' % float('%.3g' % (inc-wgt[1]))) * 10000)
#        print "  p1: x: %4d" % x,
#        print "  p1: a: %13d, b: %13d" % (m[i-1][j], m[i-1][x] + apd[wgt[0]][SIZE])
#        m[i].append(max(m[i-1][j], m[i-1][x] + apd[wgt[0]][SIZE]))
#      else:
#        #print "  p2: m[i-1][j]: %13d" % (m[i-1][j])
#        m[i].append(m[i-1][j])
##    if i>20:
##      return
##    if wgt[1] > 0.1:
##      print "wgt[1]: %4.4f, m[%d][%d] = %d" % (wgt[1],i,j,m[i][j])
##    if i % 100 == 0:
##      print "m[%d][%d] = %d" % (i,j,m[i][j])
#
#  #print "soln: m[%d][%d] = %d" % (len(m)-1, len(m[-1])-1, m[-1][-1])
#
#  rec = []
#  i = len(wgts)
#  j = (len(wgt_incs)-1)
#  while i > 0:
##    print "i: %d, j: %d, len(m): %d, len(m[i]): %d" %\
##          (i, j, len(m), len(m[i]))
#    if m[i][j] != m[i-1][j]:
#      rec.append(wgts[i-1][0])
#      j -= int (float('%s' % float('%.3g' % (wgts[i-1][1]))) * 10000)
#    i -= 1
#
#  return rec

# use knapsack problem algorithm to select alloc points that are hot and don't
# take up a lot of space
#
def knapsackSizeAllocPoints(bench, cfg=OBJ_INFO_DEFAULT, iter=0, cutoff=0.10, sigfigs=5):

  # first get the alloc point data and scale for significant figures
  wincs = (10**sigfigs)
  apd = allocPointDict(bench, cfg, iter)
  total_size = sum( [ apd[key][FULL_SIZE_IDX] for key in apd.keys() ] )
  wgts = [ (key, int((float(apd[key][FULL_SIZE_IDX]) / total_size) * wincs)) \
           for key in apd.keys() ]

  sorted_wgts = sorted(wgts, key=itemgetter(1), reverse=True)

  sig_wgts = [ x for x in sorted_wgts if x[1] > 0 ]
  insig_wgts = sorted_wgts[len(sig_wgts):]
  
  wgt_incs = range(int(cutoff*wincs))

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[1] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[1]] + apd[wgt[0]][FULL_REFS_IDX]))
      else:
        m[i].append(m[i-1][j])
    #if i % 10 == 0:
      #print "m[%d][%d] = %d" % (i,j,m[i][j])

  hots = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      hots.append(sig_wgts[i-1][0])
      j -= sig_wgts[i-1][1]
    i -= 1

  for wgt in insig_wgts:
    hots.append(wgt[0])

  # hots is the hot keys -- need to return the cold_keys
  rec = []
  res = {}
  for key in apd.keys():
    if not key in hots:
      rec.append(key)
      res[key] = apd[key]

  cold_size  = sum ([ res[key][FULL_SIZE_IDX] for key in res.keys() ])
  cold_refs  = sum ([ res[key][FULL_REFS_IDX] for key in res.keys() ])
  total_size = sum ([ apd[key][FULL_SIZE_IDX] for key in apd.keys() ])
  total_refs = sum ([ apd[key][FULL_REFS_IDX] for key in apd.keys() ])
  size_rat   = float(cold_size) / total_size
  ref_rat    = float(cold_refs) / total_refs

  print ""
  print "cold size:  ", ("%d"%cold_size).rjust(13),   "  cold refs:  ", ("%d"%cold_refs).rjust(13)
  print "total size: ", ("%d"%total_size).rjust(13),  "  total refs: ", ("%d"%total_refs).rjust(13)
  print "size ratio: ", ("%6.6f"%size_rat).rjust(13), "  ref ratio:  ", ("%6.6f"%ref_rat).rjust(13)

  return rec

# use knapsack problem algorithm to select alloc points that are cold and take
# up a lot of space
#
def knapsackRefAllocPoints(bench, cfg=OBJ_INFO_DEFAULT, iter=0, cutoff=0.10,\
  sigfigs=5):

  # first get the alloc point data and scale for significant figures
  wincs = (10**sigfigs)
  print bench, cfg, iter
  apd = allocPointDict(bench, cfg, iter, clean=False, app_only=False)
  total_size = sum( [ apd[key][FULL_REFS_IDX] for key in apd.keys() ] )

  wgts = [ (key, int((float(apd[key][FULL_REFS_IDX]) / total_size) * wincs)) \
           for key in apd.keys() ]

  sorted_wgts = sorted(wgts, key=itemgetter(1), reverse=True)

  sig_wgts = [ x for x in sorted_wgts if x[1] > 0 ]
  insig_wgts = sorted_wgts[len(sig_wgts):]
  
  wgt_incs = range(int(cutoff*wincs))

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[1] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[1]] + apd[wgt[0]][FULL_SIZE_IDX]))
      else:
        m[i].append(m[i-1][j])
    #if i % 10 == 0:
      #print "m[%d][%d] = %d" % (i,j,m[i][j])

  rec = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      rec.append(sig_wgts[i-1][0])
      j -= sig_wgts[i-1][1]
    i -= 1

  for wgt in insig_wgts:
    rec.append(wgt[0])

#  # TESTING
  #rec_sorted = [ k[0] for k in \
  #                sorted( [ (x,apd[x][SIZE]) for x in rec ], \
  #                          key=itemgetter(1), reverse=True ) ]
  #rec_sorted = [ k[0] for k in \
  #                sorted( [ (x,apd[x][REFS]) for x in rec ], \
  #                          key=itemgetter(1), reverse=True )  ]
  #for i in range(1):
  #  rec.remove(rec_sorted[i])
  #rec = [rec_sorted[0]]
  #del rec_sorted[0]
  #del rec_sorted[2]
  #del rec_sorted[3]
  #rec = rec_sorted 
  #rec = [rec_sorted[0]] + rec_sorted[3:]
  #for r in rec[:5]:
  #  print apd[r]
#  # END TESTING

  res = {}
  for key in rec:
    res[key] = apd[key]

  cold_size  = sum ([ res[key][FULL_SIZE_IDX] for key in res.keys() ])
  cold_refs  = sum ([ res[key][FULL_REFS_IDX] for key in res.keys() ])
  total_size = sum ([ apd[key][FULL_SIZE_IDX] for key in apd.keys() ])
  total_refs = sum ([ apd[key][FULL_REFS_IDX] for key in apd.keys() ])
  size_rat   = float(cold_size) / total_size
  ref_rat    = float(cold_refs) / total_refs

  print ""
  print "cold size:  ", ("%d"%cold_size).rjust(13),   "  cold refs:  ", ("%d"%cold_refs).rjust(13)
  print "total size: ", ("%d"%total_size).rjust(13),  "  total refs: ", ("%d"%total_refs).rjust(13)
  print "size ratio: ", ("%6.6f"%size_rat).rjust(13), "  ref ratio:  ", ("%6.6f"%ref_rat).rjust(13)

  return rec

def refsPerValAllocPoints(bench, cfg=OBJ_INFO_INTERVAL, iter=0, \
                          cutoff=200000):

  if cutoff < 400000:
    winc = 10
  elif cutoff >= 400000 and cutoff < 4000000:
    winc = 100
  else:
    winc = 1000

  # first get the alloc point data and scale for significant figures
  apd = allocPointDict(bench, cfg, iter)
  wgts = []
  nvals = (getEndval(bench, cfg, iter)+1)
  wgts = [ (key, (apd[key][FULL_REFS_IDX] / nvals)) for key in apd.keys() ]

  sorted_wgts = sorted(wgts, key=itemgetter(1), reverse=True)

  sig_wgts = [ x for x in sorted_wgts if x[1] > 1 ]
  insig_wgts = sorted_wgts[len(sig_wgts):]
  print len(sig_wgts)
  
  while True:
    wgt_incs = range(winc,cutoff,winc)
    if not cutoff in wgt_incs:
      wgt_incs += [cutoff]

    m = []
    m.append([])
    for j in wgt_incs:
      m[0].append(0)

    for i,wgt in enumerate(sig_wgts,start=1):
      m.append([])
      for j,inc in enumerate(wgt_incs):
        if wgt[1] <= inc:
          m[i].append(max(m[i-1][j], m[i-1][(inc-wgt[1])/winc] + apd[wgt[0]][FULL_SIZE_IDX]))
        else:
          m[i].append(m[i-1][j])
      if i%100 == 0:
        print i
      #if i > (len(sig_wgts)-10):
        #print "m[%d][%d] = %d" % (i,j,m[i][j])

    #for i,j in enumerate(m[-1]):
      #print "m[-1][%d] = %d" % (i,j)

    rec = []
    i = len(sig_wgts)
    j = (len(wgt_incs)-1)
    while i > 0:
      if m[i][j] != m[i-1][j]:
        rec.append(sig_wgts[i-1][0])
        j -= (sig_wgts[i-1][1]/winc)
      i -= 1

    for wgt in insig_wgts:
      rec.append(wgt[0])

    cold_rpv = sum ([ apd[key][FULL_REFS_IDX] / nvals for key in rec ])
    if ((cold_rpv - cutoff) < (cutoff*0.2)):
      print "finished with winc=%d" % winc
      break
    elif winc == 1:
      print "bad knapsack even with winc=1" 
      break
    print "cutoff: %d  cold_rpv: %d" %  (cutoff, cold_rpv)
    print "bad knapsack with winc=%d, trying with %d" % (winc, winc/10)
    winc /= 10

  res = {}
  for key in rec:
    res[key] = apd[key]

#  cold_rpvs = []
#  for key in res.keys():
#    refs        = res[key][REFS]
#    start,end   = res[key][ACTIVE_VALS]
#    active_span = ((end-start)+1)
#    cold_rpvs.append((refs / active_span))
#  cold_rpv = sum (cold_rpvs)

#  total_rpvs = []
#  for key in apd.keys():
#    refs        = apd[key][REFS]
#    start,end   = apd[key][ACTIVE_VALS]
#    active_span = ((end-start)+1)
#    total_rpvs.append((refs / active_span))
#  total_rpv = sum (total_rpvs)

  cold_rpv   = sum ([ res[key][FULL_REFS_IDX] / nvals for key in res.keys() ])
  cold_size  = sum ([ res[key][FULL_SIZE_IDX]         for key in res.keys() ])
  cold_refs  = sum ([ res[key][FULL_REFS_IDX]         for key in res.keys() ])
  total_size = sum ([ apd[key][FULL_SIZE_IDX]         for key in apd.keys() ])
  total_refs = sum ([ apd[key][FULL_REFS_IDX]         for key in apd.keys() ])
  total_rpv  = sum ([ apd[key][FULL_REFS_IDX] / nvals for key in apd.keys() ])
  size_rat   = float(cold_size) / total_size
  ref_rat    = float(cold_refs) / total_refs

  print ""
  print "cold size:      ", ("%d"%cold_size).rjust(13),   "  ",
  print "cold refs:      ", ("%d"%cold_refs).rjust(13)
  print "total size:     ", ("%d"%total_size).rjust(13),  "  ",
  print "total refs:     ", ("%d"%total_refs).rjust(13)
  print "size ratio:     ", ("%6.6f"%size_rat).rjust(13), "  ",
  print "ref ratio:      ", ("%6.6f"%ref_rat).rjust(13)
  print "cold refs/val:  ", ("%d"%cold_rpv).rjust(13)
  print "total refs/val: ", ("%d"%total_rpv).rjust(13)

  return rec

#def longLivedAllocPoints(bench, cfg=OBJ_INFO_INTERVAL, iter=0, cutoff=1.5):
#
#  # first get the alloc point data and scale for significant figures
#  apd = allocPointDict(bench, cfg, iter)
#  ll_keys = []
#  for key in apd.keys():
#    objects      = apd[key][REFS]
#    live_vals    = apd[key][LIVE_VALS]
#    vals_per_obj = (float(live_vals) / objects)
#    if (vals_per_obj > cutoff):
#      ll_keys.append((key, vals_per_obj))
#  ll_keys = [ x[0] for x in sorted ( ll_keys, key=itemgetter(1), reverse=True) ]
#
#  res = {}
#  for key in ll_keys:
#    res[key] = apd[key]
#
#  ll_size    = sum ([ res[key][SIZE] for key in res.keys() ])
#  ll_refs    = sum ([ res[key][REFS] for key in res.keys() ])
#  total_size = sum ([ apd[key][SIZE] for key in apd.keys() ])
#  total_refs = sum ([ apd[key][REFS] for key in apd.keys() ])
#  size_rat   = float(ll_size) / total_size
#  ref_rat    = float(ll_refs) / total_refs
#
#  print ""
#  print "ll size:      ", ("%d"%ll_size).rjust(13),     "  ",
#  print "ll refs:      ", ("%d"%ll_refs).rjust(13)
#  print "total size:   ", ("%d"%total_size).rjust(13),  "  ",
#  print "total refs:   ", ("%d"%total_refs).rjust(13)
#  print "size ratio:   ", ("%6.6f"%size_rat).rjust(13), "  ",
#  print "ref ratio:    ", ("%6.6f"%ref_rat).rjust(13)
#
#  return ll_keys

def getKAPs(benches=jvm2008s, cfgs=[OBJ_INFO_DEFAULT], iter=0, style=REFS, \
            cutoff=0.10):

  kaps = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not kaps.has_key(bench):
      kaps[bench] = {}

    kaps[bench][cfg] = getKnapsackAPs(bench, cfg, iter, style, cutoff)
  return kaps

def get_binrec(binf, rec_size, fmt):
  rec_str = binf.read(rec_size)
  return unpack(fmt, rec_str) 

def setupValInfo(bench, cfg, iter, apinfo):
  apinfo[VALS] = []

  addrinfof = try_open_read(objAddrInfoLog(bench, cfg, iter))
  for line in addrinfof:
    if oaicValRE.match(line):  
      val     = int(line.split()[2])
      dur     = int(line.split()[7])
      reason  = addrinfof.next().split()[1].strip('()')
      curinfo = {}
      curinfo[VAL]         = val
      curinfo[DURATION]    = dur
      curinfo[REASON]      = reason
      curinfo[APD]         = {}
      curinfo[HKD]         = {}
      curinfo[HOT_KLASSES] = {}
      curinfo[HOT_KLASSES][PER_10]  = []
      curinfo[HOT_KLASSES][PER_50]  = []
      curinfo[HOT_KLASSES][PER_250] = []
      apinfo[VALS].append(curinfo)
  return apinfo

def getValAllocPointInfo(bench, cfg, iter, apinfo, app_only=False):

  if app_only:
    unkkey = get_unknown_key(apinfo)

  apinfof = try_open_read(apInfoBin(bench, cfg, iter), bin=True)

  apinfof.seek(0,2)
  apf_size = apinfof.tell()

  fmt = 'iqqqqqqq'
  rec_size = calcsize(fmt)

  apinfof.seek(0)

  try:

    val = 0
    api_rec = get_binrec(apinfof, rec_size, fmt)
    while ((apf_size - apinfof.tell()) >= rec_size):
      curapd = apinfo[VALS][val][APD]

      if api_rec[0] != APP_TOTAL_REC:
        raise
      if not app_only:
        curapd[APP_TOTAL_REC] = api_rec[1:]

      api_rec = get_binrec(apinfof, rec_size, fmt)
      if api_rec[0] != UNKNOWN_REC:
        raise
      if not app_only:
        curapd[UNKNOWN_REC] = api_rec[1:]

      api_rec = get_binrec(apinfof, rec_size, fmt)
      if api_rec[0] != SUM_TOTAL_REC:
        raise
      if not app_only:
        curapd[SUM_TOTAL_REC] = api_rec[1:]

      api_rec = get_binrec(apinfof, rec_size, fmt)
      apkey   = api_rec[0]
      while apkey >= 0:
        if not app_only or apkey != unkkey:
          curapd[apkey] = api_rec[1:]

        rec_str = apinfof.read(rec_size)
        if not rec_str:
          break
        api_rec = unpack(fmt, rec_str)
        apkey = api_rec[0]
      val += 1

  except:
    print "error reading apinfo table"
    return None

  print "  getting full alloc point info (names: %d, vals: %d)" % \
    (len(apinfo[AP_MAP].keys()), len(apinfo[VALS]))

  # MRJ: The old way of aggregating the profile data was producing some
  # really poor results with the PER_VAL reports. The issue is that just
  # summing up all the objects/refs over all intervals might skew the profile
  # to fit a small number of intervals with many objects/refs. Then, in the
  # other intervals (where the benchmark spends most of its time), the
  # predictions made by the aggregated profile are very poor.
  #
  # To reduce this effect, we scale the profile data at each interval as we're
  # aggregating the profile. Object/ref counts for each AP are divided by the
  # total number of objects/refs in the interval. Then, (since the knapsack
  # requires integer values), we multiply this ratio by the total number of
  # objects/refs across all AP's and all intervals.
  #
  tot_objects = tot_size = tot_refs = 0
  val_totals  = {}
  if app_only:
    for val in apinfo[VALS]:
      val_objects  = sum ([ val[APD][ap][VAL_ALIVE_OBJECTS_IDX] \
                            for ap in val[APD].keys() if ap != unkkey ])
      val_size     = sum ([ val[APD][ap][VAL_ALIVE_SIZE_IDX]    \
                           for ap in val[APD].keys() if ap != unkkey ])
      val_refs     = sum ([ val[APD][ap][VAL_REFS_IDX]          \
                            for ap in val[APD].keys() if ap != unkkey ])
      tot_objects += val_objects
      tot_size    += val_size
      tot_refs    += val_refs
      val_totals[val[VAL]] = (val_objects, val_size, val_refs)
  else:
    for val in apinfo[VALS]:
      val_objects  = val[APD][APP_TOTAL_REC][VAL_ALIVE_OBJECTS_IDX]
      val_size     = val[APD][APP_TOTAL_REC][VAL_ALIVE_SIZE_IDX]
      val_refs     = val[APD][APP_TOTAL_REC][VAL_REFS_IDX]
      tot_objects += val_objects
      tot_size    += val_size
      tot_refs    += val_refs
      val_totals[val[VAL]] = (val_objects, val_size, val_refs)
    #tot_objects = sum ([ val[APD][APP_TOTAL_REC][VAL_ALIVE_OBJECTS_IDX] for val in apinfo[VALS] ])
    #tot_size    = sum ([ val[APD][APP_TOTAL_REC][VAL_ALIVE_SIZE_IDX]    for val in apinfo[VALS] ])
    #tot_refs    = sum ([ val[APD][APP_TOTAL_REC][VAL_REFS_IDX]          for val in apinfo[VALS] ])

  apinfo[FULL] = {}
  apinfo[FULL][APD] = {}
  for i,ap in enumerate(apinfo[AP_MAP].keys()):
    if not app_only or ap != unkkey:
      full_objects = full_size = full_refs = 0
      for val in apinfo[VALS]:
        apd = val[APD]
        val_objects, val_size, val_refs = val_totals[val[VAL]]
        if apd.has_key(ap):
          try:
            rat_objects   = float(apd[ap][VAL_ALIVE_OBJECTS_IDX]) / val_objects
            full_objects += int ( rat_objects * tot_objects )
          except ZeroDivisionError:
            pass
          try:
            rat_size   = float(apd[ap][VAL_ALIVE_SIZE_IDX]) / val_size
            full_size += int ( rat_size * tot_size )
          except ZeroDivisionError:
            pass
          try:
            rat_refs   = float(apd[ap][VAL_REFS_IDX]) / val_refs
            full_refs += int ( rat_refs * tot_refs )
          except ZeroDivisionError:
            pass
      apinfo[FULL][APD][ap] = (full_objects, full_size, full_refs)
#
#  apinfo[FULL] = {}
#  apinfo[FULL][APD] = {}
#  for i,ap in enumerate(apinfo[AP_MAP].keys()):
#    if not app_only or ap != unkkey:
#      full_objects = full_size = full_refs = 0
#      for val in apinfo[VALS]:
#        apd = val[APD]
#        if apd.has_key(ap):
#          full_objects += apd[ap][VAL_ALIVE_OBJECTS_IDX]
#          full_size    += apd[ap][VAL_ALIVE_SIZE_IDX]
#          full_refs    += apd[ap][VAL_REFS_IDX]
#      apinfo[FULL][APD][ap] = (full_objects, full_size, full_refs)

def getKlassRecIdx(kr, idx):
  return [ kr[i+idx] for i in range(0,27,3) ]

def getValKlassInfo(bench, cfg, iter, apinfo, app_only=False):
  if app_only:
    items   = apinfo[KLASS_MAP].items()
    unkkeys = [x[0][NAME] for x in items if x[1] == '(null)']

  kinfof = try_open_read(klassRecordInfoBin(bench, cfg, iter), bin=True)

  kinfof.seek(0,2)
  apf_size = kinfof.tell()

  fmt = 'i' + ''.join(['q' for _ in range(27)])
  rec_size = calcsize(fmt)

  kinfof.seek(0)

  try:

    val = 0
    krec = get_binrec(kinfof, rec_size, fmt)
    while ((apf_size - kinfof.tell()) >= rec_size):
      curhkd = apinfo[VALS][val][HKD]

      # APP_TOTAL_REC marks the beginning of an interval. The last nine values
      # are the sum total (all objects) -- and the first nine values are the
      # totals only for the application objects
      #
      if krec[0] != APP_TOTAL_REC:
        raise
      if not app_only:
        curhkd[APP_TOTAL_REC] = getKlassRecIdx(krec[1:], 2)

      krec = get_binrec(kinfof, rec_size, fmt)
      kid  = krec[0]
      while kid >= 0:
        if app_only:
          if not kid in unkkeys:
            curhkd[kid] = getKlassRecIdx(krec[1:], 1)
        else:
          curhkd[kid] = getKlassRecIdx(krec[1:], 1)

        rec_str = kinfof.read(rec_size)
        if not rec_str:
          break
        krec = unpack(fmt, rec_str)
        kid = krec[0]
      val += 1

  except:
    print "error reading apinfo table"
    return None

def allocPointInfo(bench, cfg, iter=0, clean=False, app_only=False,
  getssi=True, getvki=False):

  apinfo = {}
  if not clean:
    try:
      apinfo = load(open(allocInfoPkl(bench, cfg, iter, app_only)))
      return apinfo
    except:
      print "could not open apd pkl for %s-%s-i%d, regenerating ..." % \
            (bench, cfg, iter)

  print "  getting alloc point map"
  apinfo[AP_MAP] = allocPointMap(bench, cfg, iter)

  print "  getting klass map"
  apinfo[KLASS_MAP] = klassMap(bench, cfg, iter, apinfo[AP_MAP])

  print "  getting klass access lists"
  apinfo[KALS] = getKALs(bench, cfg)

  print "  setting up val info"
  setupValInfo(bench, cfg, iter, apinfo)

  print "  getting val alloc point info"
  getValAllocPointInfo(bench, cfg, iter, apinfo, app_only=app_only)

  if (getssi):
    print "  getting stack sample info"
    getStackSampleInfo(bench, cfg, iter, apinfo)

  if (getvki):
    print "  getting val klass info"
    getValKlassInfo(bench, cfg, iter, apinfo, app_only=app_only)

  dump(apinfo, open(allocInfoPkl(bench, cfg, iter, app_only), 'w'))
  return apinfo

def hotKlassAPs(hotks, kapmap):
  aps = []
  for kid in hotks:
    aps += kapmap[kid][APS]
  return aps

#def allocPointInfo(bench, cfg, iter=0, clean=True):
#
#  apinfo = {}
#  if not clean:
#    try:
#      apinfo = load(open(allocInfoPkl(bench, cfg, iter)))
#      return apinfo    
#    except:
#      print "could not open apd pkl for %s-%s-i%d, regenerating ..." % \
#            (bench, cfg, iter)
#
#  print "  getting alloc point map"
#  apinfo[NAMES] = allocPointMap(bench, cfg, iter)
#
#  print "  getting val alloc point info"
#  apinfo[VALS]  = []
#  apinfof = try_open_read(apInfoLog(bench, cfg, iter))
#  for line in apinfof:
#    if aicValRE.match(line):
#      pts = line.split()
#      curinfo = {}
#      curinfo[REASON]       = pts[0]
#      curinfo[TIME_ELAPSED] = int(pts[7])
#
#      curinfo[PRE]       = {}
#      curinfo[POST]      = {}
#      curinfo[PRE][APS]  = {}
#      curinfo[POST][APS] = {}
#
#      apinfof.next() # skip the pre-GC line
#      apinfof.next() # skip the headers
#      for line in apinfof:
#        if not line[0].isdigit() and not line.startswith("total"):
#          break
#        pts   = line.split()
#        apkey = pts[0]
#        vinfo = [ int(x) for x in pts[1:] if x.isdigit() ]
#        if apkey == 'total':
#          curinfo[PRE][TOTAL] = vinfo
#        else:
#          curinfo[PRE][APS][int(apkey)] = vinfo
#
#      apinfof.next() # skip the post-GC line
#      apinfof.next() # skip the headers
#      for line in apinfof:
#        if not line[0].isdigit() and not line.startswith("total"):
#          break
#        pts = line.split()
#        apkey = pts[0]
#        vinfo = [ int(x) for x in pts[1:] if x.isdigit() ]
#        if apkey == 'total':
#          curinfo[POST][TOTAL] = vinfo
#        else:
#          curinfo[POST][APS][int(apkey)] = vinfo
#      if (len(apinfo[VALS])%10)==0:
#        print len(apinfo[VALS])
#      apinfo[VALS].append(curinfo)
#
#  print "  getting full alloc point info (names: %d, vals: %d)" % \
#    (len(apinfo[NAMES].keys()), len(apinfo[VALS]))
#  apinfo[FULL] = {}
#  for i,ap in enumerate(apinfo[NAMES].keys()):
#    full_objects = full_size = full_refs = 0
#    for val in apinfo[VALS]:
#      rec = val[PRE][APS]
#      if rec.has_key(ap):
#        full_objects += rec[ap][VAL_ALIVE_OBJECTS_IDX]
#        full_size    += rec[ap][VAL_ALIVE_SIZE_IDX]
#        full_refs    += rec[ap][VAL_REFS_IDX]
#    apinfo[FULL][ap] = (full_objects, full_size, full_refs)
#
#  dump(apinfo, open(allocInfoPkl(bench, cfg, iter), 'w'))
#  return apinfo

def klassMap(bench, cfg, iter, apmap):
  kmapf = try_open_read(klassMapLog(bench, cfg, iter))
  kmap  = {}
  for line in kmapf:
    kid,name = line.split()[:2]
    kmap[int(kid)] = {}
    kmap[int(kid)][NAME] = name
    kmap[int(kid)][APS]  = []

  for ap in apmap.keys():
    kid = apmap[ap][KLASS]
    if kid >= 0:
      kmap[kid][APS] += [ap]

  return kmap

def getKlassIDMap(bench, cfg, iter=0):
  kmapf = try_open_read(klassMapLog(bench, cfg, iter))
  kmap  = {}
  for line in kmapf:
    kid,klass   = line.split()[:2]
    kmap[klass] = int(kid)
  return kmap

#def getIDKlassMap(bench, cfg, iter=0):
#  kmapf = try_open_read(klassMapLog(bench, cfg, iter))
#  kmap  = {}
#  for line in kmapf:
#    kid,klass      = line.split()[:2]
#    kmap[int(kid)] = klass
#  return kmap

#def getKlassAPMap(apmap):
#  kapmap = {}
#  for ap in apmap.keys():
#    klass = apmap[ap][KLASS]
#    if not kapmap.has_key(klass):
#      kapmap[klass] = []
#    kapmap[klass] += [ap]
#  return kapmap

def getKALs(bench, cfg, iter=0, mkalcfg=METHOD_KAL_INFO):
  kmap   = getKlassIDMap(bench, cfg, iter=iter)

  #small_only = [OBJ_INFO_SHSIM, OBJ_INFO_XSHSIM] + \
  #             [(OBJ_INFO_VXSIM%x) for x in vrates]
  small_only = [OBJ_INFO_SHSIM, OBJ_INFO_XSHSIM]
  mkx_only   = small_only + [(OBJ_INFO_VXSIM%x) for x in vrates]
  mknx_only  = [(OBJ_INFO_NVXSIM%x) for x in vrates]

  if cfg in mkx_only:
    mkalcfg = METHOD_KAL_INFOX
  elif cfg in mknx_only:
    mkalcfg = METHOD_KAL_INFONX

  mkalsf = try_open_read(mkalsLog(bench, mkalcfg, 0))
  mkals  = {}
  for line in mkalsf:
    if mkalRE.match(line):
      method = " ".join(line.split()[1:])
      mkals[method] = []
      for kline in mkalsf:
        if kline == '\n':
          break
        klass = kline.strip()
        if klass != "<none>" and kmap.has_key(klass):
          mkals[method].append(kmap[klass])
  return mkals

def getStackSampleInfo(bench, cfg, iter, apinfo):

  stackf  = try_open_read(stackSampleLog(bench, cfg, iter))
  cur_idx = 0
  cur_val = apinfo[VALS][cur_idx]
  cur_hms = []
  for line in stackf:
    if hmsValRE.match(line):
      # reverse the hot methods so the latest samples are included in PER_50
      # and PER_250
      #
      for i,hm in enumerate(reversed(cur_hms)):
        if not apinfo[KALS].has_key(hm):
          print "no kals for method: ", hm
          continue
        for kid in apinfo[KALS][hm]:
          cur_val[HOT_KLASSES][PER_10].append(kid)
          if ((i%5)==0):
            cur_val[HOT_KLASSES][PER_50].append(kid)
          if i == 0:
            cur_val[HOT_KLASSES][PER_250].append(kid)

      cur_val[HOT_KLASSES][PER_10]  = set(cur_val[HOT_KLASSES][PER_10])
      cur_val[HOT_KLASSES][PER_50]  = set(cur_val[HOT_KLASSES][PER_50])
      cur_val[HOT_KLASSES][PER_250] = set(cur_val[HOT_KLASSES][PER_250])
      cur_idx += 1
      if cur_idx == len(apinfo[VALS]):
        break
      cur_val = apinfo[VALS][cur_idx]
      cur_hms = []

    elif line != '\n':
      cur_hms.append(line.strip())

#      for hm in cur_val[HOT_METHODS]:
#        if not apinfo[KALS].has_key(hm):
#          print "no kals for method: ", hm
#          continue
#        for kid in apinfo[KALS][hm]:
#          cur_val[HOT_KLASSES].append(kid)
#
#      cur_val[HOT_METHODS] = set(cur_val[HOT_METHODS])
#      cur_val[HOT_KLASSES] = set(cur_val[HOT_KLASSES])
#      cur_idx += 1
#      if cur_idx == len(apinfo[VALS]):
#        break
#      cur_val = apinfo[VALS][cur_idx]
#
#    elif line != '\n':
#      cur_val[HOT_METHODS].append(line.strip())

def getOnlineHKs(apinfo, vals, style=PER_10):
  hks = []
  for i in vals:
    val = apinfo[VALS][i]
    for hk in val[HOT_KLASSES][style]:
      hks.append(hk)
  return set(hks)

def find_key(name, api):
  for key in api[AP_MAP].keys():
    if api[AP_MAP][key][NAME] == name:
      return key
  return None

def remap_ks(orig_ks, orig_api, map_api):

  mapped_ks = []
  for okey in orig_ks:
    mkey = find_key(orig_api[AP_MAP][okey][NAME], map_api)
    if mkey is not None:
      mapped_ks.append(mkey)
  return mapped_ks


def compute_full_knapsack_totals(ks_keys, apd):

  ks = {}
  for key in ks_keys:
    ks[key] = apd[key]

  apd_objects = sum ([ apd[key][FULL_OBJECTS_IDX] for key in apd.keys() ])
  apd_size    = sum ([ apd[key][FULL_SIZE_IDX]    for key in apd.keys() ])
  apd_refs    = sum ([ apd[key][FULL_REFS_IDX]    for key in apd.keys() ])

  ks_objects  = sum ([ ks[key][FULL_OBJECTS_IDX]  for key in ks.keys()  ])
  ks_size     = sum ([ ks[key][FULL_SIZE_IDX]     for key in ks.keys()  ])
  ks_refs     = sum ([ ks[key][FULL_REFS_IDX]     for key in ks.keys()  ])

  rat_objects = float(ks_objects)  / apd_objects
  rat_size    = float(ks_size)     / apd_size
  rat_refs    = float(ks_refs)     / apd_refs

  ksts = {}
  ksts[ALL_STATS] = ( apd_objects, apd_size, apd_refs )
  ksts[KS_STATS]  = ( ks_objects,  ks_size,  ks_refs  )
  ksts[KS_RATIO]  = ( rat_objects, rat_size, rat_refs )

  return ksts

def add_to_stats(stats, info):
  for i,stat in enumerate(stats):
    stats[i] += info[i]

def get_stat_rat(num_stats, den_stats):
  rats = []
  for num,den in zip(num_stats, den_stats):
    rats.append(0. if num == 0 else float(num) / den)
  return rats

def compute_vals_knapsack_totals(ks_keys, ideal_keys, apd):

  if ks_keys == None:
    return

#  ks = {}
#  for key in ks_keys:
#    if apd.has_key(key):
#      ks[key] = apd[key]

#  ks_keys    = ks.keys()
#  ideal_keys = [ x for x in ideal_keys if x in apd.keys()     ]
#  over_keys  = [ x for x in ks_keys    if not x in ideal_keys ]
#  under_keys = [ x for x in ideal_keys if not x in ks_keys    ]

  all_stats   = [0, 0, 0, 0, 0, 0, 0]
  ks_stats    = [0, 0, 0, 0, 0, 0, 0]
  over_stats  = [0, 0, 0, 0, 0, 0, 0]
  under_stats = [0, 0, 0, 0, 0, 0, 0]

  #apd_keys    = [ k for k in apd.keys() if k > 0 ]

  total_aps   = len(apd.keys())
  #total_aps   = len(apd_keys)
  total_ideal = len(ideal_keys)
  total_ks    = 0
  total_over  = 0
  total_under = 0
  for key in apd.keys():
  #for key in apd_keys:
    ap_info = apd[key]
    add_to_stats(all_stats, ap_info)

    if key in ks_keys:
      add_to_stats(ks_stats, ap_info)
      total_ks += 1

      if not key in ideal_keys:
        add_to_stats(over_stats, ap_info)
        total_over += 1
    else:
      if key in ideal_keys:
        add_to_stats(under_stats, ap_info)
        total_under += 1

  ks_rats    = get_stat_rat(ks_stats,    all_stats)
  over_rats  = get_stat_rat(over_stats,  all_stats)
  under_rats = get_stat_rat(under_stats, all_stats)

  ks_ap_rat    = 0. if total_ks    == 0 else float(total_ks)    / total_aps
  over_ap_rat  = 0. if total_over  == 0 else float(total_over)  / total_aps
  under_ap_rat = 0. if total_under == 0 else float(total_under) / total_aps

  ksts              = {}
  ksts[ALL_STATS]   = tuple(all_stats)
  ksts[KS_STATS]    = tuple(ks_stats)
  ksts[OVER_STATS]  = tuple(over_stats)
  ksts[UNDER_STATS] = tuple(under_stats)
  ksts[KS_RATIO]    = tuple(ks_rats)
  ksts[OVER_RATIO]  = tuple(over_rats)
  ksts[UNDER_RATIO] = tuple(under_rats)
  ksts[AP_STATS]    = ( total_aps,
                        (total_ks,    ks_ap_rat    ),
                        (total_over,  over_ap_rat  ),
                        (total_under, under_ap_rat )
                      )

  return ksts

#  apd_total_objects = sum ([ apd[key][VAL_ALIVE_OBJECTS_IDX] for key in apd.keys() ])
#  apd_total_size    = sum ([ apd[key][VAL_ALIVE_SIZE_IDX]    for key in apd.keys() ])
#  apd_hot_objects   = sum ([ apd[key][VAL_HOT_OBJECTS_IDX]   for key in apd.keys() ])
#  apd_hot_size      = sum ([ apd[key][VAL_HOT_SIZE_IDX]      for key in apd.keys() ])
#  apd_new_objects   = sum ([ apd[key][VAL_NEW_OBJECTS_IDX]   for key in apd.keys() ])
#  apd_new_size      = sum ([ apd[key][VAL_NEW_SIZE_IDX]      for key in apd.keys() ])
#  apd_refs          = sum ([ apd[key][VAL_REFS_IDX]          for key in apd.keys() ])
#
#  ks_total_objects  = sum ([ ks[key][VAL_ALIVE_OBJECTS_IDX]  for key in ks_keys    ])
#  ks_total_size     = sum ([ ks[key][VAL_ALIVE_SIZE_IDX]     for key in ks_keys    ])
#  ks_hot_objects    = sum ([ ks[key][VAL_HOT_OBJECTS_IDX]    for key in ks_keys    ])
#  ks_hot_size       = sum ([ ks[key][VAL_HOT_SIZE_IDX]       for key in ks_keys    ])
#  ks_new_objects    = sum ([ ks[key][VAL_NEW_OBJECTS_IDX]    for key in ks_keys    ])
#  ks_new_size       = sum ([ ks[key][VAL_NEW_SIZE_IDX]       for key in ks_keys    ])
#  ks_refs           = sum ([ ks[key][VAL_REFS_IDX]           for key in ks_keys    ])

#  rat_total_objects = 0. if ks_total_objects == 0 else float(ks_total_objects) / apd_total_objects
#  rat_total_size    = 0. if ks_total_size    == 0 else float(ks_total_size)    / apd_total_size
#  rat_hot_objects   = 0. if ks_hot_objects   == 0 else float(ks_hot_objects)   / apd_hot_objects
#  rat_hot_size      = 0. if ks_hot_size      == 0 else float(ks_hot_size)      / apd_hot_size
#  rat_new_objects   = 0. if ks_new_objects   == 0 else float(ks_new_objects)   / apd_new_objects
#  rat_new_size      = 0. if ks_new_size      == 0 else float(ks_new_size)      / apd_new_size
#  rat_refs          = 0. if ks_refs          == 0 else float(ks_refs)          / apd_refs

#  over_total_objects  = sum ([ apd[key][VAL_ALIVE_OBJECTS_IDX]  for key in over_keys   ])
#  over_total_size     = sum ([ apd[key][VAL_ALIVE_SIZE_IDX]     for key in over_keys   ])
#  over_hot_objects    = sum ([ apd[key][VAL_HOT_OBJECTS_IDX]    for key in over_keys   ])
#  over_hot_size       = sum ([ apd[key][VAL_HOT_SIZE_IDX]       for key in over_keys   ])
#  over_new_objects    = sum ([ apd[key][VAL_NEW_OBJECTS_IDX]    for key in over_keys   ])
#  over_new_size       = sum ([ apd[key][VAL_NEW_SIZE_IDX]       for key in over_keys   ])
#  over_refs           = sum ([ apd[key][VAL_REFS_IDX]           for key in over_keys   ])

#  over_rat_total_objects = 0. if over_total_objects == 0 else float(over_total_objects) / apd_total_objects
#  over_rat_total_size    = 0. if over_total_size    == 0 else float(over_total_size)    / apd_total_size
#  over_rat_hot_objects   = 0. if over_hot_objects   == 0 else float(over_hot_objects)   / apd_hot_objects
#  over_rat_hot_size      = 0. if over_hot_size      == 0 else float(over_hot_size)      / apd_hot_size
#  over_rat_new_objects   = 0. if over_new_objects   == 0 else float(over_new_objects)   / apd_new_objects
#  over_rat_new_size      = 0. if over_new_size      == 0 else float(over_new_size)      / apd_new_size
#  over_rat_refs          = 0. if over_refs          == 0 else float(over_refs)          / apd_refs

#  under_total_objects = sum ([ apd[key][VAL_ALIVE_OBJECTS_IDX]  for key in under_keys  ])
#  under_total_size    = sum ([ apd[key][VAL_ALIVE_SIZE_IDX]     for key in under_keys  ])
#  under_hot_objects   = sum ([ apd[key][VAL_HOT_OBJECTS_IDX]    for key in under_keys  ])
#  under_hot_size      = sum ([ apd[key][VAL_HOT_SIZE_IDX]       for key in under_keys  ])
#  under_new_objects   = sum ([ apd[key][VAL_NEW_OBJECTS_IDX]    for key in under_keys  ])
#  under_new_size      = sum ([ apd[key][VAL_NEW_SIZE_IDX]       for key in under_keys  ])
#  under_refs          = sum ([ apd[key][VAL_REFS_IDX]           for key in under_keys  ])

#  under_rat_total_objects = 0. if under_total_objects == 0 else float(under_total_objects) / apd_total_objects
#  under_rat_total_size    = 0. if under_total_size    == 0 else float(under_total_size)    / apd_total_size
#  under_rat_hot_objects   = 0. if under_hot_objects   == 0 else float(under_hot_objects)   / apd_hot_objects
#  under_rat_hot_size      = 0. if under_hot_size      == 0 else float(under_hot_size)      / apd_hot_size
#  under_rat_new_objects   = 0. if under_new_objects   == 0 else float(under_new_objects)   / apd_new_objects
#  under_rat_new_size      = 0. if under_new_size      == 0 else float(under_new_size)      / apd_new_size
#  under_rat_refs          = 0. if under_refs          == 0 else float(under_refs)          / apd_refs

#  ksts = {}
#  ksts[ALL_STATS]   = ( apd_total_objects,       apd_total_size,
#                        apd_hot_objects,         apd_hot_size,
#                        apd_new_objects,         apd_new_size, 
#                        apd_refs )
#  ksts[KS_STATS]    = ( ks_total_objects,        ks_total_size,
#                        ks_hot_objects,          ks_hot_size,
#                        ks_new_objects,          ks_new_size, 
#                        ks_refs  )
#  ksts[KS_RATIO]       = ( rat_total_objects,       rat_total_size,
#                        rat_hot_objects,         rat_hot_size,
#                        rat_new_objects,         rat_new_size, 
#                        rat_refs )
#  ksts[OVER_STATS]  = ( over_total_objects,      over_total_size,
#                        over_hot_objects,        over_hot_size,
#                        over_new_objects,        over_new_size, 
#                        over_refs      )
#  ksts[OVER_RATIO]  = ( over_rat_total_objects,  over_rat_total_size,
#                        over_rat_hot_objects,    over_rat_hot_size,
#                        over_rat_new_objects,    over_rat_new_size, 
#                        over_rat_refs  )
#  ksts[UNDER_STATS] = ( under_total_objects,     under_total_size,
#                        under_hot_objects,       under_hot_size,
#                        under_new_objects,       under_new_size, 
#                        under_refs     )
#  ksts[UNDER_RATIO] = ( under_rat_total_objects, under_rat_total_size,
#                        under_rat_hot_objects,   under_rat_hot_size,
#                        under_rat_new_objects,   under_rat_new_size, 
#                        under_rat_refs )

def compute_vals_hks_totals(hotks, kapmap, ideal_keys, apd):

  if hotks == None:
    return

  hot_aps  = []
  for klass in hotks:
    hot_aps += kapmap[klass][APS]

  cold_aps = []
  for key in apd.keys():
    if not key in hot_aps:
      cold_aps.append(key)

  return compute_vals_knapsack_totals(cold_aps, ideal_keys, apd)

def compute_vals_size_hks_totals(hotks, kapmap, ideal_keys, apd, cutoff):

  if hotks == None:
    return

  hot_aps  = []
  for klass in hotks:
    hot_aps += kapmap[klass][APS]

  cold_aps = []
  for key in apd.keys():
    if not key in hot_aps:
      cold_aps.append(key)

  return compute_vals_size_ks_totals(cold_aps, ideal_keys, apd, cutoff=cutoff)

def compute_vals_size_hks_totals_v2(hotks, kapmap, ideal_keys, apd, cutoff):

  if hotks == None:
    return

  hot_aps  = []
  for klass in hotks:
    hot_aps += kapmap[klass][APS]
  hot_aps = [x for x in hot_aps if x in apd.keys()]

  cold_aps = []
  for key in apd.keys():
    if not key in hot_aps:
      cold_aps.append(key)

  total_size  = sum([apd[key][VAL_ALIVE_SIZE_IDX] for key in apd.keys()])
  hot_size    = sum([apd[key][VAL_ALIVE_SIZE_IDX] for key in hot_aps])
  target_size = int((total_size * cutoff))

  if hot_size < target_size:
    rnd = Random()
    rnd.seed(0)
    while True:
      next_hot = rnd.choice(cold_aps)
      tmp_size = hot_size + apd[next_hot][VAL_ALIVE_SIZE_IDX]
      if tmp_size > target_size:
        break
      cold_aps.remove(next_hot)
      hot_aps.append(next_hot)
      hot_size += apd[next_hot][VAL_ALIVE_SIZE_IDX]

  elif hot_size > target_size:
    rnd = Random()
    rnd.seed(0)
    while True:
      next_cold = rnd.choice(hot_aps)
      hot_aps.remove(next_cold)
      cold_aps.append(next_cold)
      hot_size -= apd[next_cold][VAL_ALIVE_SIZE_IDX]
      if hot_size < target_size:
        break

  return compute_vals_size_ks_totals(cold_aps, ideal_keys, apd, cutoff=cutoff)

#def compute_vals_hks_totals(hotks, hkd):
#
#  ideal_total_objs  = ideal_stats[VAL_ALIVE_OBJECTS_IDX]
#  ideal_total_size  = ideal_stats[VAL_ALIVE_SIZE_IDX]
#
#  hkd_total_objects = sum ([ hkd[key][HKD_VAL_ALIVE_OBJECTS_IDX] for key in hkd.keys() ])
#  hkd_total_size    = sum ([ hkd[key][HKD_VAL_ALIVE_SIZE_IDX]    for key in hkd.keys() ])
#  hkd_hot_objects   = sum ([ hkd[key][HKD_VAL_HOT_OBJECTS_IDX]   for key in hkd.keys() ])
#  hkd_hot_size      = sum ([ hkd[key][HKD_VAL_HOT_SIZE_IDX]      for key in hkd.keys() ])
#  hkd_new_objects   = sum ([ hkd[key][HKD_VAL_NEW_OBJECTS_IDX]   for key in hkd.keys() ])
#  hkd_new_size      = sum ([ hkd[key][HKD_VAL_NEW_SIZE_IDX]      for key in hkd.keys() ])
#  hkd_refs          = sum ([ hkd[key][HKD_VAL_REFS_IDX]          for key in hkd.keys() ])
#
#  ks_total_objects  = sum ([ hkd[key][HKD_VAL_ALIVE_OBJECTS_IDX]  for key in coldks ])
#  ks_total_size     = sum ([ hkd[key][HKD_VAL_ALIVE_SIZE_IDX]     for key in coldks ])
#  ks_hot_objects    = sum ([ hkd[key][HKD_VAL_HOT_OBJECTS_IDX]    for key in coldks ])
#  ks_hot_size       = sum ([ hkd[key][HKD_VAL_HOT_SIZE_IDX]       for key in coldks ])
#  ks_new_objects    = sum ([ hkd[key][HKD_VAL_NEW_OBJECTS_IDX]    for key in coldks ])
#  ks_new_size       = sum ([ hkd[key][HKD_VAL_NEW_SIZE_IDX]       for key in coldks ])
#  ks_refs           = sum ([ hkd[key][HKD_VAL_REFS_IDX]           for key in coldks ])
#
#  rat_total_objects = 0. if ks_total_objects == 0 else float(ks_total_objects) / hkd_total_objects
#  rat_total_size    = 0. if ks_total_size    == 0 else float(ks_total_size)    / hkd_total_size
#  rat_hot_objects   = 0. if ks_hot_objects   == 0 else float(ks_hot_objects)   / hkd_hot_objects
#  rat_hot_size      = 0. if ks_hot_size      == 0 else float(ks_hot_size)      / hkd_hot_size
#  rat_new_objects   = 0. if ks_new_objects   == 0 else float(ks_new_objects)   / hkd_new_objects
#  rat_new_size      = 0. if ks_new_size      == 0 else float(ks_new_size)      / hkd_new_size
#  rat_refs          = 0. if ks_refs          == 0 else float(ks_refs)          / hkd_refs
#
#  ksts = {}
#  ksts[ALL_STATS]    = ( hkd_total_objects, hkd_total_size,
#                         hkd_hot_objects,   hkd_hot_size,
#                         hkd_new_objects,   hkd_new_size, 
#                         hkd_refs )
#  ksts[KS_STATS]     = ( ks_total_objects,  ks_total_size,
#                         ks_hot_objects,    ks_hot_size,
#                         ks_new_objects,    ks_new_size, 
#                         ks_refs )
#  ksts[KS_RATIO]        = ( rat_total_objects, rat_total_size,
#                         rat_hot_objects,   rat_hot_size,
#                         rat_new_objects,   rat_new_size, 
#                         rat_refs )
#  return ksts

def compute_vals_size_ks_totals(ks_keys, ideal_keys, apd, adj=False, \
  cutoff=None):

  if ks_keys == None:
    return

#  ks = {}
#  for key in ks_keys:
#    if apd.has_key(key):
#      ks[key] = apd[key]

#  ks_keys    = ks.keys()
#  ideal_keys = [ x for x in ideal_keys if x in apd.keys()     ]
#  over_keys  = [ x for x in ks_keys    if not x in ideal_keys ]
#  under_keys = [ x for x in ideal_keys if not x in ks_keys    ]

  all_stats   = [0, 0, 0, 0, 0, 0, 0]
  ks_stats    = [0, 0, 0, 0, 0, 0, 0]
  out_stats   = [0, 0, 0, 0, 0, 0, 0]
  #adj_stats   = [0, 0, 0, 0, 0, 0, 0]

#  for x in ks_keys:
#    if not x in apd.keys():
#      print x

  total_aps   = len(apd.keys())
  total_ideal = len(ideal_keys)
  total_ks    = 0
  total_out   = 0

  for key in apd.keys():
    ap_info = apd[key]
    add_to_stats(all_stats, ap_info)

    if key in ks_keys:
      add_to_stats(ks_stats, ap_info)
      total_ks += 1
    else:
      add_to_stats(out_stats, ap_info)
      total_out += 1

  ks_rats   = get_stat_rat(ks_stats,  all_stats)
  out_rats  = get_stat_rat(out_stats, all_stats)

#  ks_adj_stats  = deepcopy(ks_stats)
#  ks_adj_rats   = deepcopy(ks_rats)
#  out_adj_stats = deepcopy(out_stats)
#  out_adj_rats  = deepcopy(out_rats)
#
#  if adj:
#    total_size  = sum ([ apd[x][VAL_ALIVE_SIZE_IDX] for x in apd.keys() ])
#    ideal_size  = sum ([ apd[x][VAL_ALIVE_SIZE_IDX] for x in ideal_keys ])
#    cutoff_size = min ( [((1.0-cutoff)*total_size), ideal_size] )
#
#    ks_size = ks_stats[VAL_ALIVE_SIZE_IDX]
#    if ks_size < cutoff_size:
#      ks_objs   = sum ([ apd[x][VAL_ALIVE_OBJECTS_IDX] for x in ks_keys if apd.has_key(x) ])
#      ks_refs   = sum ([ apd[x][VAL_REFS_IDX]          for x in ks_keys if apd.has_key(x) ])
#      ks_rpb    = float(ks_refs) / ks_size
#      ks_opb    = float(ks_objs) / ks_size
#
#      short    = cutoff_size - ks_size
#      total_refs = sum ([ apd[x][VAL_REFS_IDX] for x in apd.keys() ])
#      #print total_size, ks_size, cutoff_size, short, total_refs, ks_refs, ks_rpb
#
#      obj_adj   = ceil(short * ks_opb)
#      ref_adj   = ceil(short * ks_rpb)
#
#      ks_adj_stats[VAL_ALIVE_OBJECTS_IDX]  += obj_adj
#      ks_adj_stats[VAL_ALIVE_SIZE_IDX]     += short
#      ks_adj_stats[VAL_REFS_IDX]           += ref_adj
#      ks_adj_rats = get_stat_rat(ks_adj_stats, all_stats)
#
#      out_adj_stats[VAL_ALIVE_OBJECTS_IDX] -= obj_adj
#      out_adj_stats[VAL_ALIVE_SIZE_IDX]    -= short
#      out_adj_stats[VAL_REFS_IDX]          -= ref_adj
#      out_adj_rats = get_stat_rat(out_adj_stats, all_stats)

  ksts                = {}
  ksts[ALL_STATS]     = tuple(all_stats)
  ksts[KS_STATS]      = tuple(ks_stats)
  ksts[KS_RATIO]      = tuple(ks_rats)
#  ksts[KS_ADJ_STATS]  = tuple(ks_adj_stats)
#  ksts[KS_ADJ_RATIO]  = tuple(ks_adj_rats)
  ksts[OUT_STATS]     = tuple(out_stats)
  ksts[OUT_RATIO]     = tuple(out_rats)
#  ksts[OUT_ADJ_STATS] = tuple(out_adj_stats)
#  ksts[OUT_ADJ_RATIO] = tuple(out_adj_rats)

  return ksts

def genSizeKnapsack(refs_apd, size_apd, style=FULL_REFS, cutoff=0.02, \
  sigfigs=5, pp=False):

  apd        = {}
  auto_hot   = []
  for key in size_apd.keys():
    if not refs_apd.has_key(key):
      auto_hot.append(key)
#      objs = size_apd[key][VAL_ALIVE_OBJECTS_IDX]
#      size = size_apd[key][VAL_ALIVE_SIZE_IDX]
#      apd[key] = (objs, size, 0)

  for key in refs_apd.keys():
    objs = size_apd[key][VAL_ALIVE_OBJECTS_IDX] if size_apd.has_key(key) else 0
    size = size_apd[key][VAL_ALIVE_SIZE_IDX]    if size_apd.has_key(key) else 0
    apd[key] = (objs, size, refs_apd[key][FULL_REFS_IDX])

#  for key in size_apd.keys():
#    refs = refs_apd[key][FULL_REFS_IDX] if refs_apd.has_key(key) else 0
#    apd[key] = (size_apd[key][VAL_ALIVE_OBJECTS_IDX],
#                size_apd[key][VAL_ALIVE_SIZE_IDX], refs)

#  if pp:
#    print "hot predict"
#    tmp1 = sorted([ (key,apd[key][2]) for key in apd.keys() ],
#                  key=itemgetter(1), reverse=True)
#    for key,refs in tmp1[:20]:
#      print ("%5d" % key), " : (", apd[key][1], apd[key][2], ")"
#
#    print "hot val"
#    tmp2 = sorted([ (key,size_apd[key][VAL_REFS_IDX]) for key in size_apd.keys() ],
#                  key=itemgetter(1), reverse=True)
#    for key,refs in tmp2[:20]:
#      print ("%5d" % key), " : (", size_apd[key][1], size_apd[key][6], \
#            ")   [ (",apd[key][1],apd[key][2],") ]"

  total_size    = sum ([size_apd[key][VAL_ALIVE_SIZE_IDX] for key in size_apd.keys()])
  auto_hot_size = sum ([size_apd[key][1] for key in auto_hot])
  target_size   = total_size * cutoff
  target_size  -= auto_hot_size
  if target_size > 0:
    apd_size      = sum ([apd[key][VAL_ALIVE_SIZE_IDX] for key in apd.keys()])
    target_cutoff = target_size / apd_size
    #print style
    ks_hot        = genKnapsack(apd, style=style, cutoff=target_cutoff, sigfigs=sigfigs, pp=pp)
    #if pp:
    #  ks_size       = sum ([apd[key][VAL_ALIVE_SIZE_IDX] for key in ks_hot])
    #  print "auto_hot_size: %d ks_size: %d total_size: %d apd_size: %d cutoff: %4.4f, target_cutoff: %4.4f" % \
    #        (auto_hot_size, ks_size, total_size, apd_size, cutoff, target_cutoff)
    return [x for x in apd.keys() if not x in (auto_hot + ks_hot)]
  else:
    print "warning: auto_hot exceeds total: auto_hot_size: %d total_size: %d cutoff: %4.4f" % \
          (auto_hot_size, total_size, cutoff)
    return [x for x in apd.keys() if not x in auto_hot]

def genKnapsack(apd, style=FULL_REFS, cutoff=0.02, sigfigs=5, pp=False):
  if style==FULL_REFS:
    return __genKnapsack(apd, FULL_REFS_IDX, FULL_SIZE_IDX, cutoff=cutoff,
                         sigfigs=sigfigs, pp=pp)
  elif style==FULL_SIZE:
    return __genKnapsack(apd, FULL_SIZE_IDX, FULL_REFS_IDX, cutoff=cutoff,
                         sigfigs=sigfigs, pp=pp)
  elif style==VAL_REFS:
    return __genKnapsack(apd, VAL_REFS_IDX, VAL_ALIVE_SIZE_IDX,
                         cutoff=cutoff, sigfigs=sigfigs, pp=pp)
  elif style==VAL_SIZE:
    return __genKnapsack(apd, VAL_ALIVE_SIZE_IDX, VAL_REFS_IDX,
                         cutoff=cutoff, sigfigs=sigfigs, pp=pp)

  print "genKnapsack: invalid style: %s", style
  return None

def __genKnapsack(apd, wgt_idx, value_idx, cutoff=0.02, sigfigs=5, pp=False):

  #print "apd[45]: ", apd[45]
  #print "apd[48]: ", apd[48]
  wincs = (10**sigfigs)
  total_size = sum( [ apd[key][wgt_idx] for key in apd.keys() ] )

  #print "__total_size: ", total_size
  if total_size == 0:
    return None

  wgts = [ (key, int((float(apd[key][wgt_idx]) / total_size) * wincs)) \
           for key in apd.keys() ]

  sorted_wgts = sorted(wgts, key=itemgetter(1), reverse=True)

  sig_wgts = [ x for x in sorted_wgts if x[1] > 0 ]
  insig_wgts = sorted_wgts[len(sig_wgts):] 

  wgt_incs = range(int(cutoff*wincs))
  if not wgt_incs:
    return []

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[1] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[1]] + apd[wgt[0]][value_idx]))
      else:
        m[i].append(m[i-1][j])
    #if i % 10 == 0:
      #print "m[%d][%d] = %d" % (i,j,m[i][j])

  rec = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      rec.append(sig_wgts[i-1][0])
      j -= sig_wgts[i-1][1]
    i -= 1

  for wgt in insig_wgts:
    rec.append(wgt[0])

  res = {}
  for key in rec:
    res[key] = apd[key]

  ks_value    = sum ([ res[key][value_idx] for key in res.keys() ])
  ks_wgt      = sum ([ res[key][wgt_idx]   for key in res.keys() ])
  total_value = sum ([ apd[key][value_idx] for key in apd.keys() ])
  total_wgt   = sum ([ apd[key][wgt_idx]   for key in apd.keys() ])
  value_rat   = float(ks_value) / total_value
  wgt_rat     = float(ks_wgt)   / total_wgt

  if pp:
    print ""
    print "ks    value: ", ("%d"%ks_value).rjust(13),     "  ks wgt:    ", ("%d"%ks_wgt).rjust(13)
    print "total value: ", ("%d"%total_value).rjust(13),  "  total wgt: ", ("%d"%total_wgt).rjust(13)
    print "value ratio: ", ("%6.6f"%value_rat).rjust(13), "  wgt ratio: ", ("%6.6f"%wgt_rat).rjust(13)

  return rec
  #return ks_keys

def get_unknown_key(apinfo):
  items  = apinfo[AP_MAP].items()
  return items[[ x[1][NAME] for x in items].index("unknown-AP")][0]

def window_apd(apinfo, vals):

  wapd   = {}
  vals   = [ x for x in apinfo[VALS] if x[VAL] in vals ]
  unkkey = get_unknown_key(apinfo)

#  if type(history) is int:
#    vals = apinfo[VALS][max([0,start-history]):start]
#  elif history == NMAX:
#    vals = apinfo[VALS][0:start]
#  else:
#    print "window_apd: invalid history: ", history
#    raise SystemExit(1)

  # MRJ: scale the profile data during aggregation - see comment in
  # getValAllocPointInfo
  #
  tot_objects = tot_size = tot_refs = 0
  val_totals  = {}
  for val in vals:
    val_objects  = sum ([ val[APD][ap][VAL_ALIVE_OBJECTS_IDX] \
                          for ap in val[APD].keys() if ap != unkkey ])
    val_size     = sum ([ val[APD][ap][VAL_ALIVE_SIZE_IDX]    \
                         for ap in val[APD].keys() if ap != unkkey ])
    val_refs     = sum ([ val[APD][ap][VAL_REFS_IDX]          \
                          for ap in val[APD].keys() if ap != unkkey ])
    tot_objects += val_objects
    tot_size    += val_size
    tot_refs    += val_refs
    val_totals[val[VAL]] = (val_objects, val_size, val_refs)

  for val in vals:
    vaps = val[APD]
    val_objects, val_size, val_refs = val_totals[val[VAL]]
    for ap in vaps.keys():
      if not wapd.has_key(ap):
        wapd[ap] = [0, 0, 0]

      try:
        rat_objects = float(vaps[ap][VAL_ALIVE_OBJECTS_IDX]) / val_objects
        inc_objects = int ( rat_objects * tot_objects )
      except ZeroDivisionError:
        inc_objects = 0
      try:
        rat_size = float(vaps[ap][VAL_ALIVE_SIZE_IDX]) / val_size
        inc_size = int ( rat_size * tot_size )
      except ZeroDivisionError:
        inc_size = 0
      try:
        rat_refs = float(vaps[ap][VAL_REFS_IDX]) / val_refs
        inc_refs = int ( rat_refs * tot_refs )
        #if ap == 5577:
        #  print rat_refs, tot_refs, inc_refs,
      except ZeroDivisionError:
        inc_refs = 0

      wapd[ap][FULL_OBJECTS_IDX] += inc_objects
      wapd[ap][FULL_SIZE_IDX]    += inc_size
      wapd[ap][FULL_REFS_IDX]    += inc_refs
      #if ap == 5577:
      #  print " ", wapd[ap][FULL_REFS_IDX]
  return wapd
 
def get_objects_misses(rec, target, adj, out):
  stat_key = KS_ADJ_STATS if adj else KS_STATS
  rat_key  = KS_ADJ_RATIO if adj else KS_RATIO
  objects_target      = target[stat_key][VAL_ALIVE_OBJECTS_IDX]
  objects_target_rat  = target[rat_key][VAL_ALIVE_OBJECTS_IDX]

  rec_all_objects     = rec[ALL_STATS][VAL_ALIVE_OBJECTS_IDX]
  rec_ks_objects      = rec[stat_key][VAL_ALIVE_OBJECTS_IDX]
  rec_objects_rat     = 0. if rec_ks_objects == 0 else rec_ks_objects / float(rec_all_objects)

  missed_objects_hi = missed_objects_lo = 0
  if rec_ks_objects > objects_target:
    missed_objects_hi = (rec_ks_objects - objects_target)
  elif rec_ks_objects < objects_target:
    missed_objects_lo = (objects_target - rec_ks_objects)

  missed_objects_hi_rat = missed_objects_lo_rat = 0.0
  if rec_ks_objects > objects_target:
    missed_objects_hi_rat = (rec_objects_rat - objects_target_rat)
  elif rec_ks_objects < objects_target:
    missed_objects_lo_rat = (objects_target_rat - rec_objects_rat)

  return ((missed_objects_lo, missed_objects_lo_rat),
          (missed_objects_hi, missed_objects_hi_rat))

def get_objects_misses_idx(rec, target, hilo=HIGH, rat=True, adj=False, out=False):
  ratidx = 1 if rat else 0
  return get_objects_misses(rec, target, adj, out)[hilo][ratidx]

def get_size_misses(rec, target, adj, out):
  stat_key = KS_ADJ_STATS if adj else KS_STATS
  rat_key  = KS_ADJ_RATIO if adj else KS_RATIO
  size_target      = target[stat_key][VAL_ALIVE_SIZE_IDX]
  size_target_rat  = target[rat_key][VAL_ALIVE_SIZE_IDX]

  rec_all_size     = rec[ALL_STATS][VAL_ALIVE_SIZE_IDX]
  rec_ks_size      = rec[stat_key][VAL_ALIVE_SIZE_IDX]
  rec_size_rat     = 0. if rec_ks_size == 0 else rec_ks_size / float(rec_all_size)

  missed_size_hi = missed_size_lo = 0
  if rec_ks_size > size_target:
    missed_size_hi = (rec_ks_size - size_target)
  elif rec_ks_size < size_target:
    missed_size_lo = (size_target - rec_ks_size)

  missed_size_hi_rat = missed_size_lo_rat = 0.0
  if rec_ks_size > size_target:
    missed_size_hi_rat = (rec_size_rat - size_target_rat)
  elif rec_ks_size < size_target:
    missed_size_lo_rat = (size_target_rat - rec_size_rat)

  return ((missed_size_lo, missed_size_lo_rat),
          (missed_size_hi, missed_size_hi_rat))

def get_size_misses_idx(rec, target, hilo=HIGH, rat=True, adj=False, out=False):
  ratidx = 1 if rat else 0
  return get_size_misses(rec, target, adj, out)[hilo][ratidx]

def get_refs_misses(rec, target, adj, out):
  if out:
    stat_key = OUT_ADJ_STATS if adj else OUT_STATS
    rat_key  = OUT_ADJ_RATIO if adj else OUT_RATIO
  else:
    stat_key = KS_ADJ_STATS if adj else KS_STATS
    rat_key  = KS_ADJ_RATIO if adj else KS_RATIO
  refs_target      = target[stat_key][VAL_REFS_IDX]
  refs_target_rat  = target[rat_key][VAL_REFS_IDX]

  rec_all_refs     = rec[ALL_STATS][VAL_REFS_IDX]
  rec_ks_refs      = rec[stat_key][VAL_REFS_IDX]
  rec_refs_rat     = 0. if rec_ks_refs == 0 else rec_ks_refs / float(rec_all_refs)

  missed_refs_hi = missed_refs_lo = 0
  if rec_ks_refs > refs_target:
    missed_refs_hi = (rec_ks_refs - refs_target)
  elif rec_ks_refs < refs_target:
    missed_refs_lo = (refs_target - rec_ks_refs)

  missed_refs_hi_rat = missed_refs_lo_rat = 0.0
  if rec_ks_refs > refs_target:
    missed_refs_hi_rat = (rec_refs_rat - refs_target_rat)
  elif rec_ks_refs < refs_target:
    missed_refs_lo_rat = (refs_target_rat - rec_refs_rat)

  return ((missed_refs_lo, missed_refs_lo_rat),
          (missed_refs_hi, missed_refs_hi_rat))

#  refs_target_rat  = target[rat_key][VAL_REFS_IDX]
#
#  refs_lo    = refs_hi = None
#  refs_ktarg = target[stat_key][VAL_REFS_IDX]
#  refs_rtarg = (target[ALL_STATS][VAL_REFS_IDX] * target_rat)
#  refs_lo    = refs_ktarg if (refs_ktarg <  refs_rtarg) else refs_rtarg
#  refs_hi    = refs_ktarg if (refs_ktarg >= refs_rtarg) else refs_rtarg
#
#  missed_refs_hi = missed_refs_lo = 0
#  if val_ks_refs > refs_hi:
#    missed_refs_hi = (val_ks_refs - refs_hi)
#  elif val_ks_refs < refs_lo:
#    missed_refs_lo = (refs_lo - val_ks_refs)
#
#
#  refs_lo_rat    = refs_hi_rat = None
#  refs_ktarg_rat = target[rat_key][VAL_REFS_IDX]
#  refs_rtarg_rat = target_rat
#  refs_lo_rat    = refs_ktarg_rat if (refs_ktarg_rat <  refs_rtarg_rat) \
#                                  else refs_rtarg_rat
#  refs_hi_rat    = refs_ktarg_rat if (refs_ktarg_rat >= refs_rtarg_rat) \
#                                  else refs_rtarg_rat
#
#  missed_refs_hi_rat = missed_refs_lo_rat = 0.0
#  if val_refs_rat > refs_hi_rat:
#    missed_refs_hi_rat = (val_refs_rat - refs_hi_rat)
#  elif val_refs_rat < refs_lo_rat:
#    missed_refs_lo_rat = (refs_lo_rat - val_refs_rat)
#
#  return ((missed_refs_lo, missed_refs_lo_rat),
#          (missed_refs_hi, missed_refs_hi_rat))

def get_refs_misses_idx(rec, target, hilo=HIGH, rat=True, adj=False, out=False):
  ratidx = 1 if rat else 0
  return get_refs_misses(rec, target, adj, out)[hilo][ratidx]

def get_over_under_ratio(rec, idx):
  return rec[OVER_RATIO][idx] + rec[UNDER_RATIO][idx]

def get_ap_over_under_ratio(rec):
  return rec[AP_STATS][3][1] + rec[AP_STATS][4][1]

def get_agg_over_under_ratio(rec):
  return rec[OVER_RATIO] + rec[UNDER_RATIO]

def agg_diff_vals(vals, key):
  akvs   = {} 
  myvals = [ v for v in vals if v.has_key(key) ]

  akvs[NVALS] = len(myvals)

  all_objects  = sum     ( [ x[key][TOTALS][0] for x in myvals ] )
  diff_objects = sum     ( [ x[key][DIFFS][0]  for x in myvals ] )
  rat_objects  = average ( [ x[key][KS_RATIO][0]  for x in myvals ] )

  all_size     = sum     ( [ x[key][TOTALS][1] for x in myvals ] )
  diff_size    = sum     ( [ x[key][DIFFS][1]  for x in myvals ] )
  rat_size     = average ( [ x[key][KS_RATIO][1]  for x in myvals ] )

  all_refs     = sum     ( [ x[key][TOTALS][2] for x in myvals ] )
  diff_refs    = sum     ( [ x[key][DIFFS][2]  for x in myvals ] )
  rat_refs     = average ( [ x[key][KS_RATIO][2]  for x in myvals ] )

  akvs[OBJECTS] = (all_objects, diff_objects, rat_objects)
  akvs[SIZE]    = (all_size,    diff_size,    rat_size)
  akvs[REFS]    = (all_refs,    diff_refs,    rat_refs)

  return akvs

def agg_ks_vals(vals, key, target_rat=0.02, mindur=50):
  akvs   = {}
  myvals = [ v for v in vals \
             if  not v[REASON] in ['pre-major-gc','post-minor-gc','post-major-gc'] \
             and not v[DURATION] < mindur \
             and sum ( [ v[APD][k][VAL_REFS_IDX] for k in v[APD].keys() ] ) > 0
             and not v[IDEAL_KS_PPV] == None \
             and not v[key] == None \
           ]
#  myvals = [ v for v in vals \
#             if  not v[REASON] in ['pre-major-gc','post-minor-gc','post-major-gc'] \
#             and not v[DURATION] < mindur \
#             and sum ( [ v[APD][k][VAL_REFS_IDX] for k in v[APD].keys() ] ) > 0
#             and not v[key] == None \
#           ]

  akvs[NVALS] = len(myvals)

  akv_stats   = [ ALL_STATS, KS_STATS, OVER_STATS, UNDER_STATS ]
  akv_ratios  = [ (KS_RATIO,    KS_STATS,    ALL_STATS),
                  (OVER_RATIO,  OVER_STATS,  ALL_STATS),
                  (UNDER_RATIO, UNDER_STATS, ALL_STATS)
                ]

  av_osrs     = [ (OBJECTS, VAL_NEW_OBJECTS_IDX),
                  (SIZE,    VAL_NEW_SIZE_IDX),
                  (REFS,    VAL_REFS_IDX)
                ]

  akvs[ALL_VALS] = {}
  for osr,idx in av_osrs:
    akvs[ALL_VALS][osr] = {}
    for at in akv_stats:
      akvs[ALL_VALS][osr][at] = sum ( [ x[key][at][idx] for x in myvals ] )
    for rkey,nkey,dkey in akv_ratios:
      num = akvs[ALL_VALS][osr][nkey]
      den = akvs[ALL_VALS][osr][dkey]
      akvs[ALL_VALS][osr][rkey] = 0. if num == 0 else num / float(den)


  pv_osrs   = [ (OBJECTS, VAL_ALIVE_OBJECTS_IDX),
                (SIZE,    VAL_ALIVE_SIZE_IDX),
                (REFS,    VAL_REFS_IDX)
              ]

  akvs[PER_VAL] = {}
  for osr,idx in pv_osrs:
    akvs[PER_VAL][osr] = {}
    for at in akv_stats:
      akvs[PER_VAL][osr][at] = average ( [ x[key][at][idx] for x in myvals ] )
    for rkey,_,_ in akv_ratios:
      akvs[PER_VAL][osr][rkey] = average( [ x[key][rkey][idx] for x in myvals ] )

  akvs[PER_VAL][APS] = {}
  akvs[PER_VAL][APS][ALL_STATS]   = average( [ x[key][AP_STATS][0]    for x in myvals ] )
  akvs[PER_VAL][APS][KS_STATS]    = average( [ x[key][AP_STATS][1][0] for x in myvals ] )
  akvs[PER_VAL][APS][OVER_STATS]  = average( [ x[key][AP_STATS][2][0] for x in myvals ] )
  akvs[PER_VAL][APS][UNDER_STATS] = average( [ x[key][AP_STATS][3][0] for x in myvals ] )
  akvs[PER_VAL][APS][KS_RATIO]    = average( [ x[key][AP_STATS][1][1] for x in myvals ] )
  akvs[PER_VAL][APS][OVER_RATIO]  = average( [ x[key][AP_STATS][2][1] for x in myvals ] )
  akvs[PER_VAL][APS][UNDER_RATIO] = average( [ x[key][AP_STATS][3][1] for x in myvals ] )

  return akvs

#  all_aps               = sum     ( [ x[key][AP_STATS][0]    for x in myvals ] )
#  ideal_aps             = sum     ( [ x[key][AP_STATS][1][0] for x in myvals ] )
#  ideal_aps_rat         = average ( [ x[key][AP_STATS][1][1] for x in myvals ] )
#  ks_aps                = sum     ( [ x[key][AP_STATS][2][0] for x in myvals ] )
#  ks_aps_rat            = average ( [ x[key][AP_STATS][2][1] for x in myvals ] )
#  over_aps              = sum     ( [ x[key][AP_STATS][3][0] for x in myvals ] )
#  over_aps_rat          = average ( [ x[key][AP_STATS][3][1] for x in myvals ] )
#  under_aps             = sum     ( [ x[key][AP_STATS][4][0] for x in myvals ] )
#  under_aps_rat         = average ( [ x[key][AP_STATS][4][1] for x in myvals ] )
#
#  akvs[APS]                   = {}
#  akvs[APS][TOTAL]            = all_aps
#  akvs[APS][IDEAL_TOTAL]      = ideal_aps
#  akvs[APS][IDEAL_RATIO]      = ideal_aps_rat
#  akvs[APS][KS_TOTAL]         = ks_aps
#  akvs[APS][KS_RATIO]         = ks_aps_rat
#  akvs[APS][OVER_TOTAL]       = over_aps
#  akvs[APS][OVER_RATIO]       = over_aps_rat
#  akvs[APS][UNDER_TOTAL]      = under_aps
#  akvs[APS][UNDER_RATIO]      = under_aps_rat

#  tot_objects_new       = sum ( [ x[key][ALL_STATS][VAL_NEW_OBJECTS_IDX]           for x in myvals ] )
#  ks_objects_new        = sum ( [ x[key][KS_STATS][VAL_NEW_OBJECTS_IDX]            for x in myvals ] )
#  over_objects_new      = sum ( [ x[key][OVER_STATS][VAL_NEW_OBJECTS_IDX]          for x in myvals ] )
#  under_objects_new     = sum ( [ x[key][UNDER_STATS][VAL_NEW_OBJECTS_IDX]         for x in myvals ] )
#  rat_objects_new       = 0. if ks_objects_new    == 0 else ks_objects_new    / float(tot_objects_new)
#  rat_over_objects_new  = 0. if over_objects_new  == 0 else over_objects_new  / float(tot_objects_new)
#  rat_under_objects_new = 0. if under_objects_new == 0 else under_objects_new / float(tot_objects_new)
#
#  tot_size_new          = sum ( [ x[key][ALL_STATS][VAL_NEW_SIZE_IDX]              for x in myvals ] )
#  ks_size_new           = sum ( [ x[key][KS_STATS][VAL_NEW_SIZE_IDX]               for x in myvals ] )
#  over_size_new         = sum ( [ x[key][OVER_STATS][VAL_NEW_SIZE_IDX]             for x in myvals ] )
#  under_size_new        = sum ( [ x[key][UNDER_STATS][VAL_NEW_SIZE_IDX]            for x in myvals ] )
#  rat_size_new          = 0. if ks_size_new == 0 else ks_size_new / float(tot_size_new)
#  rat_over_size_new     = 0. if over_size_new  == 0 else over_size_new  / float(tot_size_new)
#  rat_under_size_new    = 0. if under_size_new == 0 else under_size_new / float(tot_size_new)
#
#  tot_refs              = sum ( [ x[key][ALL_STATS][VAL_REFS_IDX]                  for x in myvals ] )
#  ks_refs               = sum ( [ x[key][KS_STATS][VAL_REFS_IDX]                   for x in myvals ] )
#  over_refs             = sum ( [ x[key][OVER_STATS][VAL_REFS_IDX]                 for x in myvals ] )
#  under_refs            = sum ( [ x[key][UNDER_STATS][VAL_REFS_IDX]                for x in myvals ] )
#  rat_refs              = 0. if ks_refs == 0 else ks_refs / float(tot_refs)
#  rat_over_refs         = 0. if over_refs  == 0 else over_refs  / float(tot_refs)
#  rat_under_refs        = 0. if under_refs == 0 else under_refs / float(tot_refs)

#  tot_objects_pv        = average ( sum ( [ x[key][ALL_STATS][VAL_ALIVE_OBJECTS_IDX]   for x in myvals ] ) )
#  ks_objects_pv         = average ( sum ( [ x[key][KS_STATS][VAL_ALIVE_OBJECTS_IDX]    for x in myvals ] ) )
#  over_objects_pv       = average ( sum ( [ x[key][OVER_STATS][VAL_ALIVE_OBJECTS_IDX]  for x in myvals ] ) )
#  under_objects_pv      = average ( sum ( [ x[key][UNDER_STATS][VAL_ALIVE_OBJECTS_IDX] for x in myvals ] ) )
#  rat_objects_pv        = average ( [ x[key][KS_RATIO][VAL_ALIVE_OBJECTS_IDX]             for x in myvals ] )
#  rat_over_objects_pv   = average ( [ x[key][OVER_RATIO][VAL_ALIVE_OBJECTS_IDX]        for x in myvals ] )
#  rat_under_objects_pv  = average ( [ x[key][UNDER_RATIO][VAL_ALIVE_OBJECTS_IDX]       for x in myvals ] )
#
#  tot_size_pv           = average ( sum ( [ x[key][ALL_STATS][VAL_ALIVE_SIZE_IDX]      for x in myvals ] ) )
#  ks_size_pv            = average ( sum ( [ x[key][KS_STATS][VAL_ALIVE_SIZE_IDX]       for x in myvals ] ) )
#  over_size_pv          = average ( sum ( [ x[key][OVER_STATS][VAL_ALIVE_SIZE_IDX]     for x in myvals ] ) )
#  under_size_pv         = average ( sum ( [ x[key][UNDER_STATS][VAL_ALIVE_SIZE_IDX]    for x in myvals ] ) )
#  rat_size_pv           = average ( [ x[key][KS_RATIO][VAL_ALIVE_SIZE_IDX]                for x in myvals ] )
#  rat_over_size_pv      = average ( [ x[key][OVER_RATIO][VAL_ALIVE_SIZE_IDX]           for x in myvals ] )
#  rat_under_size_pv     = average ( [ x[key][UNDER_RATIO][VAL_ALIVE_SIZE_IDX]          for x in myvals ] )
#
#  tot_refs_pv           = average ( sum ( [ x[key][ALL_STATS][VAL_REFS_IDX]            for x in myvals ] ) )
#  ks_refs_pv            = average ( sum ( [ x[key][KS_STATS][VAL_REFS_IDX]             for x in myvals ] ) )
#  over_refs_pv          = average ( sum ( [ x[key][OVER_STATS][VAL_REFS_IDX]           for x in myvals ] ) )
#  under_refs_pv         = average ( sum ( [ x[key][UNDER_STATS][VAL_REFS_IDX]          for x in myvals ] ) )
#  rat_refs_pv           = average ( [ x[key][KS_RATIO][VAL_REFS_IDX]                      for x in myvals ] )
#  rat_over_refs_pv      = average ( [ x[key][OVER_RATIO][VAL_REFS_IDX]                 for x in myvals ] )
#  rat_under_refs_pv     = average ( [ x[key][UNDER_RATIO][VAL_REFS_IDX]                for x in myvals ] )


  # summing with 'new objects' or 'new size' gives the total objects / size
  # created during the run
  #
#  akvs[OBJECTS]               = {}
#  akvs[OBJECTS][TOTAL_NEW]    = new_objects
#  akvs[OBJECTS][KNAPSACK_NEW] = new_ks_objects
#  akvs[OBJECTS][SUM_RATIO]    = 0. if ks_objects == 0 else ks_objects / float(all_objects)
#  akvs[OBJECTS][AGG_RATIO]    = rat_objects
#  akvs[OBJECTS][STD_RATIO]    = std_objects
#  akvs[OBJECTS][OVER_TOTAL]   = over_objects
#  akvs[OBJECTS][OVER_RATIO]   = over_objects_rat
#  akvs[OBJECTS][UNDER_TOTAL]  = under_objects
#  akvs[OBJECTS][UNDER_RATIO]  = under_objects_rat
#
#  akvs[SIZE]                  = {}
#  akvs[SIZE][TOTAL_NEW]           = new_size
#  akvs[SIZE][KNAPSACK_NEW]        = new_ks_size
#  akvs[SIZE][SUM_RATIO]       = 0. if new_ks_size == 0 else new_ks_size / float(new_size)
#  akvs[SIZE][AGG_RATIO]       = rat_size
#  akvs[SIZE][STD_RATIO]       = std_size
#  akvs[SIZE][OVER_TOTAL]      = over_size
#  akvs[SIZE][OVER_RATIO]      = over_size_rat
#  akvs[SIZE][UNDER_TOTAL]     = under_size
#  akvs[SIZE][UNDER_RATIO]     = under_size_rat
#
#  akvs[REFS]                  = {}
#  akvs[REFS][TOTAL]           = all_refs
#  akvs[REFS][KNAPSACK]        = ks_refs
#  akvs[REFS][SUM_RATIO]       = 0. if ks_refs == 0 else ks_refs / float(all_refs)
#  akvs[REFS][AGG_RATIO]       = rat_refs
#  akvs[REFS][STD_RATIO]       = std_refs
#  akvs[REFS][OVER_TOTAL]      = over_refs
#  akvs[REFS][OVER_RATIO]      = over_refs_rat
#  akvs[REFS][UNDER_TOTAL]     = under_refs
#  akvs[REFS][UNDER_RATIO]     = under_refs_rat

#  new_rat_objects   = average ( [ x[key][KS_RATIO][VAL_NEW_OBJECTS_IDX]         for x in myvals ] )
#  new_std_objects   = std     ( [ x[key][KS_RATIO][VAL_NEW_OBJECTS_IDX]         for x in myvals ] )
#  new_rat_size      = average ( [ x[key][KS_RATIO][VAL_NEW_SIZE_IDX]            for x in myvals ] )
#  new_std_size      = std     ( [ x[key][KS_RATIO][VAL_NEW_SIZE_IDX]            for x in myvals ] )
#  std_objects       = std     ( [ x[key][KS_RATIO][VAL_ALIVE_OBJECTS_IDX]       for x in myvals ] )
#  std_size          = std     ( [ x[key][KS_RATIO][VAL_ALIVE_SIZE_IDX]          for x in myvals ] )
#  std_refs          = std     ( [ x[key][KS_RATIO][VAL_REFS_IDX]                for x in myvals ] )

#  objects_misses_lo       = []
#  objects_misses_lo_rat   = []
#  objects_misses_lo_rat_x = []
#  objects_misses_hi       = []
#  objects_misses_hi_rat   = []
#  objects_misses_hi_rat_x = []
#  size_misses_lo          = []
#  size_misses_lo_rat      = []
#  size_misses_lo_rat_x    = []
#  size_misses_hi          = []
#  size_misses_hi_rat      = []
#  size_misses_hi_rat_x    = []
#  refs_misses_lo          = []
#  refs_misses_lo_rat      = []
#  refs_misses_lo_rat_x    = []
#  refs_misses_hi          = []
#  refs_misses_hi_rat      = []
#  refs_misses_hi_rat_x    = []
#  for val in myvals:
#    missed_objects_info = get_objects_misses(val[key], val[IDEAL_KS_PPV])
#    objects_misses_lo.append(missed_objects_info[0][0])
#    objects_misses_lo_rat.append(missed_objects_info[0][1])
#    objects_misses_hi.append(missed_objects_info[1][0])
#    objects_misses_hi_rat.append(missed_objects_info[1][1])
#    if missed_objects_info[0][1] > 0.000005:
#      objects_misses_lo_rat_x.append(missed_objects_info[0][1])
#    if missed_objects_info[1][1] > 0.000005:
#      objects_misses_hi_rat_x.append(missed_objects_info[0][1])
#
#    missed_size_info = get_size_misses(val[key], val[IDEAL_KS_PPV])
#    size_misses_lo.append(missed_size_info[0][0])
#    size_misses_lo_rat.append(missed_size_info[0][1])
#    size_misses_hi.append(missed_size_info[1][0])
#    size_misses_hi_rat.append(missed_size_info[1][1])
#    if missed_size_info[0][1] > 0.000005:
#      size_misses_lo_rat_x.append(missed_size_info[0][1])
#    if missed_size_info[1][1] > 0.000005:
#      size_misses_hi_rat_x.append(missed_size_info[0][1])
#
#    missed_refs_info = get_refs_misses(val[key], val[IDEAL_KS_PPV], \
#                                       target_rat=target_rat)
#    refs_misses_lo.append(missed_refs_info[0][0])
#    refs_misses_lo_rat.append(missed_refs_info[0][1])
#    refs_misses_hi.append(missed_refs_info[1][0])
#    refs_misses_hi_rat.append(missed_refs_info[1][1])
#    if missed_refs_info[0][1] > 0.000005:
#      refs_misses_lo_rat_x.append(missed_refs_info[0][1])
#    if missed_refs_info[1][1] > 0.000005:
#      refs_misses_hi_rat_x.append(missed_refs_info[0][1])
#
#  total_objects_misses_lo   = sum(objects_misses_lo)
#  sum_objects_misses_lo_rat = 0. if total_objects_misses_lo == 0 else total_objects_misses_lo / float(all_objects)
#  agg_objects_misses_lo_rat = average(objects_misses_lo_rat)
#  nvals_objects_misses_lo   = len(objects_misses_lo_rat_x)
#  max_objects_misses_lo_rat = max(objects_misses_lo_rat)
#  min_objects_misses_lo_rat = min(objects_misses_lo_rat)
#  agg_objects_misses_lo     = average(objects_misses_lo)
#
#  total_objects_misses_hi   = sum(objects_misses_hi)
#  sum_objects_misses_hi_rat = 0. if total_objects_misses_hi == 0 else total_objects_misses_hi / float(all_objects)
#  agg_objects_misses_hi_rat = average(objects_misses_hi_rat)
#  nvals_objects_misses_hi   = len(objects_misses_hi_rat_x)
#  max_objects_misses_hi_rat = max(objects_misses_hi_rat)
#  min_objects_misses_hi_rat = min(objects_misses_hi_rat)
#  agg_objects_misses_hi     = average(objects_misses_hi)
#
#  total_size_misses_lo      = sum(size_misses_lo)
#  sum_size_misses_lo_rat    = 0. if total_size_misses_lo == 0 else total_size_misses_lo / float(all_size)
#  agg_size_misses_lo_rat    = average(size_misses_lo_rat)
#  nvals_size_misses_lo      = len(size_misses_lo_rat_x)
#  max_size_misses_lo_rat    = max(size_misses_lo_rat)
#  min_size_misses_lo_rat    = min(size_misses_lo_rat)
#  agg_size_misses_lo        = average(size_misses_lo)
#
#  total_size_misses_hi      = sum(size_misses_hi)
#  sum_size_misses_hi_rat    = 0. if total_size_misses_hi == 0 else total_size_misses_hi / float(all_size)
#  agg_size_misses_hi_rat    = average(size_misses_hi_rat)
#  nvals_size_misses_hi      = len(size_misses_hi_rat_x)
#  max_size_misses_hi_rat    = max(size_misses_hi_rat)
#  min_size_misses_hi_rat    = min(size_misses_hi_rat)
#  agg_size_misses_hi        = average(size_misses_hi)
#
#  total_refs_misses_lo      = sum(refs_misses_lo)
#  sum_refs_misses_lo_rat    = 0. if total_refs_misses_lo == 0 else total_refs_misses_lo / float(all_refs)
#  agg_refs_misses_lo_rat    = average(refs_misses_lo_rat)
#  nvals_refs_misses_lo      = len(refs_misses_lo_rat_x)
#  max_refs_misses_lo_rat    = max(refs_misses_lo_rat)
#  min_refs_misses_lo_rat    = min(refs_misses_lo_rat)
#  agg_refs_misses_lo        = average(refs_misses_lo)
#
#  total_refs_misses_hi      = sum(refs_misses_hi)
#  sum_refs_misses_hi_rat    = 0. if total_refs_misses_hi == 0 else total_refs_misses_hi / float(all_refs)
#  agg_refs_misses_hi_rat    = average(refs_misses_hi_rat)
#  nvals_refs_misses_hi      = len(refs_misses_hi_rat_x)
#  max_refs_misses_hi_rat    = max(refs_misses_hi_rat)
#  min_refs_misses_hi_rat    = min(refs_misses_hi_rat)
#  agg_refs_misses_hi        = average(refs_misses_hi)
#
#  akvs[OBJECTS_MISSES]                 = ({}, {})
#  akvs[OBJECTS_MISSES][0][TOTAL]       = total_objects_misses_lo
#  akvs[OBJECTS_MISSES][0][AGG_RATIO]   = agg_objects_misses_lo_rat
#  akvs[OBJECTS_MISSES][0][SUM_RATIO]   = sum_objects_misses_lo_rat
#  akvs[OBJECTS_MISSES][0][MAX_RATIO]   = max_objects_misses_lo_rat
#  akvs[OBJECTS_MISSES][0][MIN_RATIO]   = min_objects_misses_lo_rat
#  akvs[OBJECTS_MISSES][0][AGG_PER_VAL] = agg_objects_misses_lo
#  akvs[OBJECTS_MISSES][0][NVALS]       = nvals_objects_misses_lo
#
#  akvs[OBJECTS_MISSES][1][TOTAL]       = total_objects_misses_hi
#  akvs[OBJECTS_MISSES][1][AGG_RATIO]   = agg_objects_misses_hi_rat
#  akvs[OBJECTS_MISSES][1][SUM_RATIO]   = sum_objects_misses_hi_rat
#  akvs[OBJECTS_MISSES][1][MAX_RATIO]   = max_objects_misses_hi_rat
#  akvs[OBJECTS_MISSES][1][MIN_RATIO]   = min_objects_misses_hi_rat
#  akvs[OBJECTS_MISSES][1][AGG_PER_VAL] = agg_objects_misses_hi
#  akvs[OBJECTS_MISSES][1][NVALS]       = nvals_objects_misses_hi
#
#  akvs[SIZE_MISSES]                 = ({}, {})
#  akvs[SIZE_MISSES][0][TOTAL]       = total_size_misses_lo
#  akvs[SIZE_MISSES][0][AGG_RATIO]   = agg_size_misses_lo_rat
#  akvs[SIZE_MISSES][0][SUM_RATIO]   = sum_size_misses_lo_rat
#  akvs[SIZE_MISSES][0][MAX_RATIO]   = max_size_misses_lo_rat
#  akvs[SIZE_MISSES][0][MIN_RATIO]   = min_size_misses_lo_rat
#  akvs[SIZE_MISSES][0][AGG_PER_VAL] = agg_size_misses_lo
#  akvs[SIZE_MISSES][0][NVALS]       = nvals_size_misses_lo
#
#  akvs[SIZE_MISSES][1][TOTAL]       = total_size_misses_hi
#  akvs[SIZE_MISSES][1][AGG_RATIO]   = agg_size_misses_hi_rat
#  akvs[SIZE_MISSES][1][SUM_RATIO]   = sum_size_misses_hi_rat
#  akvs[SIZE_MISSES][1][MAX_RATIO]   = max_size_misses_hi_rat
#  akvs[SIZE_MISSES][1][MIN_RATIO]   = min_size_misses_hi_rat
#  akvs[SIZE_MISSES][1][AGG_PER_VAL] = agg_size_misses_hi
#  akvs[SIZE_MISSES][1][NVALS]       = nvals_size_misses_hi
#
#  akvs[REFS_MISSES]                 = ({}, {})
#  akvs[REFS_MISSES][0][TOTAL]       = total_refs_misses_lo
#  akvs[REFS_MISSES][0][AGG_RATIO]   = agg_refs_misses_lo_rat
#  akvs[REFS_MISSES][0][SUM_RATIO]   = sum_refs_misses_lo_rat
#  akvs[REFS_MISSES][0][MAX_RATIO]   = max_refs_misses_lo_rat
#  akvs[REFS_MISSES][0][MIN_RATIO]   = min_refs_misses_lo_rat
#  akvs[REFS_MISSES][0][AGG_PER_VAL] = agg_refs_misses_lo
#  akvs[REFS_MISSES][0][NVALS]       = nvals_refs_misses_lo
#
#  akvs[REFS_MISSES][1][TOTAL]       = total_refs_misses_hi
#  akvs[REFS_MISSES][1][AGG_RATIO]   = agg_refs_misses_hi_rat
#  akvs[REFS_MISSES][1][SUM_RATIO]   = sum_refs_misses_hi_rat
#  akvs[REFS_MISSES][1][MAX_RATIO]   = max_refs_misses_hi_rat
#  akvs[REFS_MISSES][1][MIN_RATIO]   = min_refs_misses_hi_rat
#  akvs[REFS_MISSES][1][AGG_PER_VAL] = agg_refs_misses_hi
#  akvs[REFS_MISSES][1][NVALS]       = nvals_refs_misses_hi

def compute_agg_ks_vals(apinfo, cutoff, mindur=50):
  apinfo[AGG_STATIC_SAME_PPV]          = agg_ks_vals(apinfo[VALS], STATIC_SAME_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_STATIC_DIFF_PPV]          = agg_ks_vals(apinfo[VALS], STATIC_DIFF_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_N1_PPV]         = agg_ks_vals(apinfo[VALS], ONLINE_KS_N1_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_N2_PPV]         = agg_ks_vals(apinfo[VALS], ONLINE_KS_N2_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_N4_PPV]         = agg_ks_vals(apinfo[VALS], ONLINE_KS_N4_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_N8_PPV]         = agg_ks_vals(apinfo[VALS], ONLINE_KS_N8_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_NMAX_PPV]       = agg_ks_vals(apinfo[VALS], ONLINE_KS_NMAX_PPV, \
                                         target_rat=cutoff, mindur=mindur )

  apinfo[AGG_ONLINE_FAST_N1_10_PPV]    = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N1_10_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N2_10_PPV]    = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N2_10_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N4_10_PPV]    = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N4_10_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N8_10_PPV]    = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N8_10_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_NMAX_10_PPV]  = agg_ks_vals(apinfo[VALS], ONLINE_FAST_NMAX_10_PPV,  \
                                         target_rat=cutoff, mindur=mindur )

  apinfo[AGG_ONLINE_FAST_N1_50_PPV]    = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N1_50_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N2_50_PPV]    = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N2_50_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N4_50_PPV]    = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N4_50_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N8_50_PPV]    = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N8_50_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_NMAX_50_PPV]  = agg_ks_vals(apinfo[VALS], ONLINE_FAST_NMAX_50_PPV,  
                                         target_rat=cutoff, mindur=mindur )

  apinfo[AGG_ONLINE_FAST_N1_250_PPV]   = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N1_250_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N2_250_PPV]   = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N2_250_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N4_250_PPV]   = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N4_250_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N8_250_PPV]   = agg_ks_vals(apinfo[VALS], ONLINE_FAST_N8_250_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_NMAX_250_PPV] = agg_ks_vals(apinfo[VALS], ONLINE_FAST_NMAX_250_PPV, \
                                         target_rat=cutoff, mindur=mindur )

  apinfo[AGG_IDEAL_KS_PPV]  = agg_ks_vals(apinfo[VALS], IDEAL_KS_PPV, \
                              target_rat=cutoff, mindur=mindur )
  return apinfo

def agg_size_ks_vals(vals, key, target_rat=0.02, mindur=50):
  akvs   = {} 
  myvals = [ v for v in vals \
             if  not v[REASON] in ['pre-major-gc','post-minor-gc','post-major-gc'] \
             and not v[DURATION] < mindur \
             and sum ( [ v[APD][k][VAL_REFS_IDX] for k in v[APD].keys() ] ) > 0
             and not v[IDEAL_KS_PPV] == None \
             and not v[key] == None \
           ]

  akvs[NVALS] = len(myvals)

#  akv_stats   = [ ALL_STATS,  KS_STATS, OUT_STATS, KS_ADJ_STATS, OUT_ADJ_STATS ]
#  akv_ratios  = [ (KS_RATIO,      KS_STATS,      ALL_STATS),
#                  (OUT_RATIO,     OUT_STATS,     ALL_STATS),
#                  (KS_ADJ_RATIO,  KS_ADJ_STATS,  ALL_STATS),
#                  (OUT_ADJ_RATIO, OUT_ADJ_STATS, ALL_STATS),
#                ]
  akv_stats   = [ ALL_STATS,  KS_STATS, OUT_STATS ]
  akv_ratios  = [ (KS_RATIO,      KS_STATS,      ALL_STATS),
                  (OUT_RATIO,     OUT_STATS,     ALL_STATS),
                ]

  av_osrs     = [ (OBJECTS, VAL_NEW_OBJECTS_IDX),
                  (SIZE,    VAL_NEW_SIZE_IDX),
                  (REFS,    VAL_REFS_IDX)
                ]

  akvs[ALL_VALS] = {}
  for osr,idx in av_osrs:
    akvs[ALL_VALS][osr] = {}
    for at in akv_stats:
      akvs[ALL_VALS][osr][at] = sum ( [ x[key][at][idx] for x in myvals ] )
    for rkey,nkey,dkey in akv_ratios:
      num = akvs[ALL_VALS][osr][nkey]
      den = akvs[ALL_VALS][osr][dkey]
      akvs[ALL_VALS][osr][rkey] = 0. if num == 0 else num / float(den)


  pv_osrs   = [ (OBJECTS, VAL_ALIVE_OBJECTS_IDX),
                (SIZE,    VAL_ALIVE_SIZE_IDX),
                (REFS,    VAL_REFS_IDX)
              ]

  akvs[PER_VAL] = {}
  for osr,idx in pv_osrs:
    akvs[PER_VAL][osr] = {}
    for at in akv_stats:
      akvs[PER_VAL][osr][at] = average ( [ x[key][at][idx] for x in myvals ] )
    for rkey,_,_ in akv_ratios:
      akvs[PER_VAL][osr][rkey] = average( [ x[key][rkey][idx] for x in myvals ] )

  return akvs


def compute_agg_size_ks_vals(apinfo, cutoff, mindur=50):
  apinfo[AGG_STATIC_SAME_PPV]          = agg_size_ks_vals(apinfo[VALS], STATIC_SAME_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_STATIC_DIFF_PPV]          = agg_size_ks_vals(apinfo[VALS], STATIC_DIFF_PPV,    \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_N1_PPV]         = agg_size_ks_vals(apinfo[VALS], ONLINE_KS_N1_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_N2_PPV]         = agg_size_ks_vals(apinfo[VALS], ONLINE_KS_N2_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_N4_PPV]         = agg_size_ks_vals(apinfo[VALS], ONLINE_KS_N4_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_N8_PPV]         = agg_size_ks_vals(apinfo[VALS], ONLINE_KS_N8_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_KS_NMAX_PPV]       = agg_size_ks_vals(apinfo[VALS], ONLINE_KS_NMAX_PPV, \
                                         target_rat=cutoff, mindur=mindur )

  apinfo[AGG_ONLINE_FAST_N1_10_PPV]    = agg_size_ks_vals(apinfo[VALS], ONLINE_FAST_N1_10_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N2_10_PPV]    = agg_size_ks_vals(apinfo[VALS], ONLINE_FAST_N2_10_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N4_10_PPV]    = agg_size_ks_vals(apinfo[VALS], ONLINE_FAST_N4_10_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_N8_10_PPV]    = agg_size_ks_vals(apinfo[VALS], ONLINE_FAST_N8_10_PPV,   \
                                         target_rat=cutoff, mindur=mindur )
  apinfo[AGG_ONLINE_FAST_NMAX_10_PPV]  = agg_size_ks_vals(apinfo[VALS], ONLINE_FAST_NMAX_10_PPV, \
                                         target_rat=cutoff, mindur=mindur )

  apinfo[AGG_IDEAL_KS_PPV]  = agg_size_ks_vals(apinfo[VALS], IDEAL_KS_PPV, \
                              target_rat=cutoff, mindur=mindur )
  return apinfo

def getKSGuide(bench, cfg):
  guide_bench = None

  #small_only = [OBJ_INFO_SHSIM, OBJ_INFO_XSHSIM] + \
  #             [(OBJ_INFO_VXSIM%x) for x in vrates]
  small_only = [OBJ_INFO_SHSIM, OBJ_INFO_XSHSIM]

  if bench in dacapos:
    if cfg in small_only:
      guide_bench = "-".join([bench.split('-')[0], SMALL])
    else:
      guide_size  = SMALL if bench.split('-')[1] == DEFAULT else DEFAULT
      guide_bench = "-".join([bench.split('-')[0], guide_size])
  else:
    guide_bench = getAPBench(bench,cfg)
  guide_cfg = cfg

  return (guide_bench, guide_cfg)

def knapsackSimSlim(bench, cfg, iter=0, clean=False, cutoff=0.02,
  sigfigs=5, clean_sim=False, deep_clean=False, reagg=True, mindur=50, \
  nthreads=5):

  (guide_bench, guide_cfg) = getKSGuide(bench, cfg)
#  if not clean:
#    ksslim = load(open(knapsackSimSlimPkl(bench, cfg, iter, guide_bench, cutoff)))
#    if reagg:
#      ksslim = compute_agg_ks_vals(ksslim, cutoff, mindur=mindur)
#    return ksslim
  if not clean:
    try:
      ksslim = load(open(knapsackSimSlimPkl(bench, cfg, iter, guide_bench, cutoff)))
      if reagg:
        ksslim = compute_agg_ks_vals(ksslim, cutoff, mindur=mindur)
      return ksslim
    except:
      print "could not open knapsack sim slim pkl for %s-%s-i%d-%s-%3.2f, regenerating ..." % \
            (bench, cfg, iter, guide_bench, cutoff)

  val_black_keys = [ONLINE_KS_N1, ONLINE_KS_N2, ONLINE_KS_N4,
                    ONLINE_KS_N8, ONLINE_KS_NMAX, IDEAL_KS,
                    ONLINE_FAST_N1_PER_10, ONLINE_FAST_N2_PER_10,
                    ONLINE_FAST_N4_PER_10, ONLINE_FAST_N8_PER_10,
                    ONLINE_FAST_NMAX_PER_10, ONLINE_FAST_N1_PER_50,
                    ONLINE_FAST_N2_PER_50, ONLINE_FAST_N4_PER_50,
                    ONLINE_FAST_N8_PER_50, ONLINE_FAST_NMAX_PER_50,
                    ONLINE_FAST_N1_PER_250, ONLINE_FAST_N2_PER_250,
                    ONLINE_FAST_N4_PER_250, ONLINE_FAST_N8_PER_250,
                    ONLINE_FAST_NMAX_PER_250
                   ]

  base_black_keys = [FULL, VALS, KALS, AP_MAP, KLASS_MAP,
                     STATIC_DIFF_KS, STATIC_SAME_KS]

  ksslim = {}
  apinfo = knapsackSim(bench, cfg, iter=iter, clean=clean_sim, cutoff=cutoff,
                       sigfigs=sigfigs, deep_clean=deep_clean, reagg=reagg,
                       mindur=mindur, nthreads=nthreads)

  for key in [ k for k in apinfo.keys() if not k in base_black_keys ]:
    ksslim[key] = deepcopy(apinfo[key])

  ksslim[VALS] = []
  for apv in apinfo[VALS]:
    curval = {}
    for key in [ k for k in apv.keys() if not k in val_black_keys ]:
      curval[key] = deepcopy(apv[key])

    ksslim[VALS].append(curval)

  dump(ksslim, open(knapsackSimSlimPkl(bench, cfg, iter, guide_bench, cutoff), 'w'))
  return ksslim

def kszSimSlim(bench, cfg, iter=0, clean=False, cutoff=0.02, sigfigs=5,
  clean_sim=False, deep_clean=False, reagg=True, mindur=50, nthreads=5):

  (guide_bench, guide_cfg) = getKSGuide(bench, cfg)
  if not clean:
#    kfile = kszSimSlimPkl(bench, cfg, iter, guide_bench, cutoff)
#    ksslim = load(open(kszSimSlimPkl(bench, cfg, iter, guide_bench, cutoff)))
#    if reagg:
#      ksslim = compute_agg_size_ks_vals(ksslim, cutoff, mindur=mindur)
#    return ksslim
    try:
      kfile = kszSimSlimPkl(bench, cfg, iter, guide_bench, cutoff)
      ksslim = load(open(kszSimSlimPkl(bench, cfg, iter, guide_bench, cutoff)))
      if reagg:
        ksslim = compute_agg_size_ks_vals(ksslim, cutoff, mindur=mindur)
      return ksslim
    except:
      print "could not open knapsack sim slim pkl for %s-%s-i%d-%s-%3.2f, regenerating ..." % \
            (bench, cfg, iter, guide_bench, cutoff)

  val_black_keys = [ONLINE_KS_N1, ONLINE_KS_N2, ONLINE_KS_N4,
                    ONLINE_KS_N8, ONLINE_KS_NMAX, IDEAL_KS,
                    ONLINE_FAST_N1_PER_10, ONLINE_FAST_N2_PER_10,
                    ONLINE_FAST_N4_PER_10, ONLINE_FAST_N8_PER_10,
                    ONLINE_FAST_NMAX_PER_10, ONLINE_FAST_N1_PER_50,
                    ONLINE_FAST_N2_PER_50, ONLINE_FAST_N4_PER_50,
                    ONLINE_FAST_N8_PER_50, ONLINE_FAST_NMAX_PER_50,
                    ONLINE_FAST_N1_PER_250, ONLINE_FAST_N2_PER_250,
                    ONLINE_FAST_N4_PER_250, ONLINE_FAST_N8_PER_250,
                    ONLINE_FAST_NMAX_PER_250
                   ]

  base_black_keys = [FULL, VALS, KALS, AP_MAP, KLASS_MAP,
                     STATIC_DIFF_KS, STATIC_SAME_KS]

  ksslim = {}
  apinfo = kszSim(bench, cfg, iter=iter, clean=clean_sim, cutoff=cutoff,
                  sigfigs=sigfigs, deep_clean=deep_clean, reagg=reagg,
                  mindur=mindur, nthreads=nthreads)

  for key in [ k for k in apinfo.keys() if not k in base_black_keys ]:
    ksslim[key] = deepcopy(apinfo[key])

  ksslim[VALS] = []
  for apv in apinfo[VALS]:
    curval = {}
    for key in [ k for k in apv.keys() if not k in val_black_keys ]:
      curval[key] = deepcopy(apv[key])

    ksslim[VALS].append(curval)

  dump(ksslim, open(kszSimSlimPkl(bench, cfg, iter, guide_bench, cutoff), 'w'))
  return ksslim

def printAPD(apdict, apnames, ks=None, sortkey=REFS, style=FULL, cutoff=30, outf=sys.stdout):

  if style == FULL:
    refs_idx = FULL_REFS_IDX
    size_idx = FULL_SIZE_IDX
  else:
    refs_idx = VAL_ALIVE_SIZE_IDX
    size_idx = VAL_REFS_IDX

  #refs_idx = VAL_ALIVE_SIZE_IDX
  #size_idx = VAL_REFS_IDX
  sortkey = refs_idx if sortkey == REFS else size_idx

  apkeys = [ x[0] for x in \
           sorted( [ (key,apdict[key][sortkey]) for key in apdict.keys() ], \
                     key=itemgetter(1), reverse=True ) ]

  print >> outf, "".ljust(2),
  print >> outf, "rank".ljust(5),
  print >> outf, "allocation point".ljust(33),
  print >> outf, "size (KB)".rjust(12),
  print >> outf, "size %%".rjust(12),
  print >> outf, "total refs".rjust(12),
  print >> outf, "ref %%".rjust(12),
  if ks:
    print >> outf, "KAP?".rjust(12),
  print >> outf, ""

  all_size = sum ( [ apdict[key][size_idx] for key in apdict.keys() ] )
  all_refs = sum ( [ apdict[key][refs_idx] for key in apdict.keys() ] )

  for i,apkey in enumerate(apkeys):
    if i > cutoff:
      break

    apinfo     = apdict[apkey]
    name       = apnames[apkey]
    kbsize     = apinfo[size_idx] / 1024
    size_pct   = float(apinfo[size_idx]) / float(all_size)
    total_refs = apinfo[refs_idx] 
    ref_pct    = float(total_refs) / float(all_refs)

    print >> outf, "".ljust(2),
    print >> outf, ("%d" % i).ljust(5),
    print >> outf, ("%s" % name[:30]).ljust(33),
    print >> outf, ("%d" % kbsize).rjust(12),
    print >> outf, ("%5.4f" % size_pct).rjust(12),
    print >> outf, ("%d" % total_refs).rjust(12),
    print >> outf, ("%5.4f" % ref_pct).rjust(12),
    if ks:
      print >> outf, ("%s" % ('+' if apkey in ks else '-')).rjust(12),
    print >> outf, ""

  if ks:
    # XXX: needs fixed
    kap_totals = compute_vals_knapsack_totals(ks, apdict)
    print >> outf, "".ljust(2),
    print >> outf, "KAP".ljust(5),
    print >> outf, "totals".ljust(33),
    print >> outf, ("%d" % (kap_totals[KS_STATS][size_idx]/1024)).rjust(12),
    print >> outf, ("%5.4f" % kap_totals[KS_RATIO][size_idx]).rjust(12),
    print >> outf, ("%d" % kap_totals[KS_STATS][refs_idx]).rjust(12),
    print >> outf, ("%5.4f" % kap_totals[KS_RATIO][refs_idx]).rjust(12),
    print >> outf, ""

def knapsackSim(bench, cfg, iter=0, clean=False, cutoff=0.02, sigfigs=5,
  deep_clean=False, reagg=True, nthreads=5, mindur=10):

  (guide_bench, guide_cfg) = getKSGuide(bench, cfg)
  if not clean:
    if reagg:
      try:

        apinfo = load(open(knapsackSimPkl(bench, cfg, iter, guide_bench, cutoff)))
        apinfo = compute_agg_ks_vals(apinfo, cutoff, mindur=mindur)
        return apinfo
      except:
        print "could not open knapsack sim pkl for %s-%s-i%d-%s-%3.2f, regenerating ..." % \
              (bench, cfg, iter, guide_bench, cutoff)
    else:
      try:

        apinfo = load(open(knapsackSimPkl(bench, cfg, iter, guide_bench, cutoff)))
        return apinfo    
      except:
        print "could not open knapsack sim pkl for %s-%s-i%d-%s-%3.2f, regenerating ..." % \
              (bench, cfg, iter, guide_bench, cutoff)

  print ("getting API (%s-%s) ... " % (bench, cfg)),
  apinfo = allocPointInfo(bench, cfg, iter=iter, clean=deep_clean, app_only=True)
  print "done"

  apinfo[STATIC_SAME_KS]        = None
  apinfo[STATIC_SAME_KS_TOTALS] = None
  apinfo[STATIC_DIFF_KS]        = None
  apinfo[STATIC_DIFF_KS_TOTALS] = None

  if guide_bench and guide_cfg:

    print ("getting guide API (%s-%s) ... " % (bench, cfg)),
    guide_apinfo     = allocPointInfo(guide_bench, guide_cfg, iter=iter,
                                      clean=deep_clean, app_only=True)
    print "done"
    guide_ks         = genKnapsack(guide_apinfo[FULL][APD], style=FULL_REFS,
                                   cutoff=cutoff, sigfigs=sigfigs)
    if guide_ks:
      apinfo[STATIC_DIFF_KS] = remap_ks(guide_ks, guide_apinfo, apinfo)

  apinfo[STATIC_SAME_KS] = genKnapsack(apinfo[FULL][APD], style=FULL_REFS,
                                       cutoff=cutoff, sigfigs=sigfigs)

  if apinfo[STATIC_SAME_KS]:
    apinfo[STATIC_SAME_KS_TOTALS] = compute_full_knapsack_totals(apinfo[STATIC_SAME_KS],
                                    apinfo[FULL][APD])

  if apinfo[STATIC_DIFF_KS]:
    apinfo[STATIC_DIFF_KS_TOTALS] = compute_full_knapsack_totals(apinfo[STATIC_DIFF_KS],
                                    apinfo[FULL][APD])

  prev = []
  next_prev = []
  for i,val in enumerate(apinfo[VALS]):
    if val[REASON] in ['pre-major-gc', 'post-minor-gc', 'post-major-gc']:
      continue
    if val[DURATION] < mindur:
      continue
    if sum ( [ val[APD][k][VAL_REFS_IDX] for k in val[APD].keys() ] ) == 0:
      print "warning: val has no refs. duration: ", val[DURATION]
      continue
    print "  val: ", ("%d" % i).rjust(4)

    for style in AP_STYLES+HK_STYLES:
      val[style] = None

    # the ideal knapsack
    val[IDEAL_KS] = genKnapsack(val[APD], VAL_REFS, cutoff, sigfigs, pp=False)

    next_prev.append(i)

    while True:
      try:
        pool = Pool(nthreads)
      except:
        print "could not allocate thread pool!"
        sleep (10)
        continue
      else:
        break

    if prev:
      ppv_results = [None for _ in range(20)]
      ppv_results[0] = pool.apply_async( compute_vals_knapsack_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_N1], val[IDEAL_KS], val[APD]) )
      ppv_results[1] = pool.apply_async( compute_vals_knapsack_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_N2], val[IDEAL_KS], val[APD]) )
      ppv_results[2] = pool.apply_async( compute_vals_knapsack_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_N4], val[IDEAL_KS], val[APD]) )
      ppv_results[3] = pool.apply_async( compute_vals_knapsack_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_N8], val[IDEAL_KS], val[APD]) )
      ppv_results[4] = pool.apply_async( compute_vals_knapsack_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_NMAX], val[IDEAL_KS], val[APD]) )

      ppv_results[5] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N1_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[6] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N2_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[7] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N4_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[8] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N8_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[9] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_NMAX_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )

      ppv_results[10] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N1_PER_50], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[11] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N2_PER_50], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[12] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N4_PER_50], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[13] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N8_PER_50], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[14] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_NMAX_PER_50], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )

      ppv_results[15] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N1_PER_250], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[16] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N2_PER_250], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[17] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N4_PER_250], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[18] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N8_PER_250], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )
      ppv_results[19] = pool.apply_async( compute_vals_hks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_NMAX_PER_250], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD]) )

    full_ppv  = pool.apply_async( compute_vals_knapsack_totals,
                 (apinfo[STATIC_SAME_KS],  val[IDEAL_KS], val[APD]) )
    guide_ppv = pool.apply_async( compute_vals_knapsack_totals,
                 (apinfo[STATIC_DIFF_KS], val[IDEAL_KS], val[APD]) )

    if val[IDEAL_KS]:
      ideal_ppv = pool.apply_async( compute_vals_knapsack_totals,
                    (val[IDEAL_KS], val[IDEAL_KS], val[APD]) )

    ks_results = [None for _ in range(5)]
    ks_results[0] = pool.apply_async( genKnapsack,
      (window_apd(apinfo,next_prev[-1:]), FULL_REFS, cutoff, sigfigs) )
    ks_results[1] = pool.apply_async( genKnapsack,
      (window_apd(apinfo,next_prev[-2:]), FULL_REFS, cutoff, sigfigs) )
    ks_results[2] = pool.apply_async( genKnapsack,
      (window_apd(apinfo,next_prev[-4:]), FULL_REFS, cutoff, sigfigs) )
    ks_results[3] = pool.apply_async( genKnapsack,
      (window_apd(apinfo,next_prev[-8:]), FULL_REFS, cutoff, sigfigs) )
    ks_results[4] = pool.apply_async( genKnapsack,
      (window_apd(apinfo,next_prev), FULL_REFS, cutoff, sigfigs) )

    if prev:
      val[ONLINE_KS_N1_PPV]         = ppv_results[0].get()
      val[ONLINE_KS_N2_PPV]         = ppv_results[1].get()
      val[ONLINE_KS_N4_PPV]         = ppv_results[2].get()
      val[ONLINE_KS_N8_PPV]         = ppv_results[3].get()
      val[ONLINE_KS_NMAX_PPV]       = ppv_results[4].get()
      val[ONLINE_FAST_N1_10_PPV]    = ppv_results[5].get()
      val[ONLINE_FAST_N2_10_PPV]    = ppv_results[6].get()
      val[ONLINE_FAST_N4_10_PPV]    = ppv_results[7].get()
      val[ONLINE_FAST_N8_10_PPV]    = ppv_results[8].get()
      val[ONLINE_FAST_NMAX_10_PPV]  = ppv_results[9].get()
      val[ONLINE_FAST_N1_50_PPV]    = ppv_results[10].get()
      val[ONLINE_FAST_N2_50_PPV]    = ppv_results[11].get()
      val[ONLINE_FAST_N4_50_PPV]    = ppv_results[12].get()
      val[ONLINE_FAST_N8_50_PPV]    = ppv_results[13].get()
      val[ONLINE_FAST_NMAX_50_PPV]  = ppv_results[14].get()
      val[ONLINE_FAST_N1_250_PPV]   = ppv_results[15].get()
      val[ONLINE_FAST_N2_250_PPV]   = ppv_results[16].get()
      val[ONLINE_FAST_N4_250_PPV]   = ppv_results[17].get()
      val[ONLINE_FAST_N8_250_PPV]   = ppv_results[18].get()
      val[ONLINE_FAST_NMAX_250_PPV] = ppv_results[19].get()

    val[STATIC_SAME_PPV] = full_ppv.get()
    val[STATIC_DIFF_PPV] = guide_ppv.get()
    if val[IDEAL_KS]:
      val[IDEAL_KS_PPV]  = ideal_ppv.get()

    val[ONLINE_KS_N1]    = ks_results[0].get()
    val[ONLINE_KS_N2]    = ks_results[1].get()
    val[ONLINE_KS_N4]    = ks_results[2].get()
    val[ONLINE_KS_N8]    = ks_results[3].get()
    val[ONLINE_KS_NMAX]  = ks_results[4].get()

    prev.append(i)

    val[ONLINE_FAST_N1_PER_10]    = getOnlineHKs(apinfo, prev[-1:], PER_10)
    val[ONLINE_FAST_N2_PER_10]    = getOnlineHKs(apinfo, prev[-2:], PER_10)
    val[ONLINE_FAST_N4_PER_10]    = getOnlineHKs(apinfo, prev[-4:], PER_10)
    val[ONLINE_FAST_N8_PER_10]    = getOnlineHKs(apinfo, prev[-8:], PER_10)
    val[ONLINE_FAST_NMAX_PER_10]  = getOnlineHKs(apinfo, prev, PER_10)
    val[ONLINE_FAST_N1_PER_50]    = getOnlineHKs(apinfo, prev[-1:], PER_50)
    val[ONLINE_FAST_N2_PER_50]    = getOnlineHKs(apinfo, prev[-2:], PER_50)
    val[ONLINE_FAST_N4_PER_50]    = getOnlineHKs(apinfo, prev[-4:], PER_50)
    val[ONLINE_FAST_N8_PER_50]    = getOnlineHKs(apinfo, prev[-8:], PER_50)
    val[ONLINE_FAST_NMAX_PER_50]  = getOnlineHKs(apinfo, prev, PER_50)
    val[ONLINE_FAST_N1_PER_250]   = getOnlineHKs(apinfo, prev[-1:], PER_250)
    val[ONLINE_FAST_N2_PER_250]   = getOnlineHKs(apinfo, prev[-2:], PER_250)
    val[ONLINE_FAST_N4_PER_250]   = getOnlineHKs(apinfo, prev[-4:], PER_250)
    val[ONLINE_FAST_N8_PER_250]   = getOnlineHKs(apinfo, prev[-8:], PER_250)
    val[ONLINE_FAST_NMAX_PER_250] = getOnlineHKs(apinfo, prev, PER_250)

    if not val[IDEAL_KS]:
      print "warning: could not compute knapsack for val: %d for %s-%s" % \
            (i, bench, cfg)
    pool.close()
    del pool
    if ((i % 10) == 0):
      gc.collect()

  apinfo = compute_agg_ks_vals(apinfo, cutoff, mindur=mindur)
  dump(apinfo, open(knapsackSimPkl(bench, cfg, iter, guide_bench, cutoff), 'w'))

  return apinfo

def getKnapsackSims(benches=dacapo_defs, cfg=OBJ_INFO_SIM, iter=0,
  clean=False, reagg=True, deep_clean=True, cutoff=0.02, sigfigs=5,
  mindur=50, nthreads=5):

  kss = {}
  for bench,cfg in expiter(benches, cfgs=[cfg]):
    ksimstr = "  %s-%s-%3.2f" % (bench,cfg,cutoff)
    if not clean:
      (guide_bench, guide_cfg) = getKSGuide(bench, cfg)
      ksimpkl = knapsackSimPkl(bench, cfg, iter, getKSGuide(bench,cfg)[0], cutoff)
      if os.path.exists(ksimpkl):
        print "%-40s [ %s ]" % (ksimstr, ctime(os.path.getmtime(ksimpkl)))
      else:
        print "%-40s [ %s ]" % (ksimstr, "ksim pkl does not exist")
    else:
      print "%-40s" % (ksimstr)
    if not kss.has_key(bench):
      kss[bench] = {}

    kss[bench][cfg] = knapsackSim(bench, cfg, iter=iter, clean=clean,
                                  reagg=reagg, deep_clean=deep_clean,
                                  cutoff=cutoff, sigfigs=sigfigs,
                                  mindur=mindur, nthreads=nthreads)

  return kss

def getKnapsackSimSlims(benches=dacapo_defs, cfg=OBJ_INFO_SIM, iter=0,
  clean=False, clean_sim=False, deep_clean=False, reagg=True, cutoff=0.02,
  sigfigs=5, mindur=50, nthreads=5):

  kss = {}
  for bench,cfg in expiter(benches, cfgs=[cfg]):
    slimstr = "  %s-%s-%3.2f" % (bench,cfg,cutoff)
    if not clean:
      (guide_bench, guide_cfg) = getKSGuide(bench, cfg)
      slimpkl = knapsackSimSlimPkl(bench, cfg, iter, getKSGuide(bench,cfg)[0], cutoff)
      if os.path.exists(slimpkl):
        print "%-40s [ %s ]" % (slimstr, ctime(os.path.getmtime(slimpkl)))
      else:
        print "%-40s [ %s ]" % (slimstr, "slim pkl does not exist")
    else:
      print "%-40s" % (slimstr)
    if not kss.has_key(bench):
      kss[bench] = {}

    kss[bench][cfg] = knapsackSimSlim(bench, cfg, iter=iter, clean=clean,
                                      clean_sim=clean_sim,
                                      deep_clean=deep_clean, reagg=reagg,
                                      cutoff=cutoff, sigfigs=sigfigs,
                                      mindur=mindur, nthreads=nthreads)

  return kss

def getKszSims(benches=dacapo_defs, cfg=OBJ_INFO_SIM, iter=0,
  clean=False, reagg=True, deep_clean=True, cutoff=0.02, sigfigs=5,
  mindur=50, nthreads=5):

  kss = {}
  for bench,cfg in expiter(benches, cfgs=[cfg]):
    ksimstr = "  %s-%s-%3.2f" % (bench,cfg,cutoff)
    if not clean:
      (guide_bench, guide_cfg) = getKSGuide(bench, cfg)
      ksimpkl = kszSimPkl(bench, cfg, iter, getKSGuide(bench,cfg)[0], cutoff)
      if os.path.exists(ksimpkl):
        print "%-40s [ %s ]" % (ksimstr, ctime(os.path.getmtime(ksimpkl)))
      else:
        print "%-40s [ %s ]" % (ksimstr, "ksim pkl does not exist")
    else:
      print "%-40s" % (ksimstr)
    if not kss.has_key(bench):
      kss[bench] = {}

    kss[bench][cfg] = kszSim(bench, cfg, iter=iter, clean=clean,
                             reagg=reagg, deep_clean=deep_clean,
                             cutoff=cutoff, sigfigs=sigfigs,
                             mindur=mindur, nthreads=nthreads)

  return kss

def getKszSimSlims(benches=dacapo_defs, cfg=OBJ_INFO_SIM, iter=0,
  clean=False, clean_sim=False, deep_clean=False, reagg=True, cutoff=0.02,
  sigfigs=5, mindur=50, nthreads=5):

  kss = {}
  for bench,cfg in expiter(benches, cfgs=[cfg]):
    slimstr = "  %s-%s-%3.2f" % (bench,cfg,cutoff)
    if not clean:
      (guide_bench, guide_cfg) = getKSGuide(bench, cfg)
      slimpkl = kszSimSlimPkl(bench, cfg, iter, getKSGuide(bench,cfg)[0], cutoff)
      if os.path.exists(slimpkl):
        print "%-40s [ %s ]" % (slimstr, ctime(os.path.getmtime(slimpkl)))
      else:
        print "%-40s [ %s ]" % (slimstr, "slim pkl does not exist")
    else:
      print "%-40s" % (slimstr)
    if not kss.has_key(bench):
      kss[bench] = {}

    kss[bench][cfg] = kszSimSlim(bench, cfg, iter=iter, clean=clean,
                                 clean_sim=clean_sim,
                                 deep_clean=deep_clean, reagg=reagg,
                                 cutoff=cutoff, mindur=mindur,
                                 sigfigs=sigfigs)

  return kss

def kszSim(bench, cfg, iter=0, clean=False, cutoff=0.02, sigfigs=5,
  deep_clean=False, reagg=True, nthreads=5, mindur=50):

  (guide_bench, guide_cfg) = getKSGuide(bench, cfg)
  if not clean:
    if reagg:
      try:
        apinfo = load(open(kszSimPkl(bench, cfg, iter, guide_bench, cutoff)))
        apinfo = compute_agg_size_ks_vals(apinfo, cutoff, mindur=mindur)
        return apinfo
      except:
        print "could not open knapsack sim pkl for %s-%s-i%d-%s-%3.2f, regenerating ..." % \
              (bench, cfg, iter, guide_bench, cutoff)
    else:
      try:
        apinfo = load(open(kszSimPkl(bench, cfg, iter, guide_bench, cutoff)))
        return apinfo    
      except:
        print "could not open knapsack sim pkl for %s-%s-i%d-%s-%3.2f, regenerating ..." % \
              (bench, cfg, iter, guide_bench, cutoff)

  print ("getting API (%s-%s) ... " % (bench, cfg)),
  apinfo = allocPointInfo(bench, cfg, iter=iter, clean=deep_clean, app_only=True)
  print "done"

  apinfo[STATIC_DIFF_KS]        = None
  apinfo[STATIC_SAME_KS]        = None
  apinfo[STATIC_SAME_KS_TOTALS] = None
  apinfo[STATIC_DIFF_KS_TOTALS] = None

  if guide_bench and guide_cfg:

    print ("getting guide API (%s-%s) ... " % (bench, cfg)),
    guide_apinfo     = allocPointInfo(guide_bench, guide_cfg, iter=iter,
                                      clean=deep_clean, app_only=True)
    print "done"

  prev = []
  for i,val in enumerate(apinfo[VALS]):
    if val[REASON] in ['pre-major-gc', 'post-minor-gc', 'post-major-gc']:
      continue
    if val[DURATION] < mindur:
      continue
    if sum ( [ val[APD][k][VAL_REFS_IDX] for k in val[APD].keys() ] ) == 0:
      print "warning: val has no refs. duration: ", val[DURATION]
      continue
    print "  val: ", ("%d" % i).rjust(4)

    for style in AP_STYLES+HK_STYLES:
      val[style] = None

    pool = Pool(nthreads)

    # the ideal knapsack
    val[IDEAL_KS]     = genKnapsack(val[APD], VAL_REFS, cutoff, sigfigs)
    val[IDEAL_KS_PPV] = compute_vals_size_ks_totals(val[IDEAL_KS], val[IDEAL_KS],
                                                    val[APD], False, cutoff)
    sz_cutoff = (1.0 - val[IDEAL_KS_PPV][KS_RATIO][VAL_ALIVE_SIZE_IDX])

    static_same_ks = pool.apply_async( genSizeKnapsack,
      (apinfo[FULL][APD], val[APD], FULL_SIZE, sz_cutoff, sigfigs) )
    static_diff_ks = pool.apply_async( genSizeKnapsack,
      (guide_apinfo[FULL][APD], val[APD], FULL_SIZE, sz_cutoff, sigfigs) )

    if prev:
      online_ks = [None for _ in range(5)]
      online_ks[0] = pool.apply_async( genSizeKnapsack,
        (window_apd(apinfo,prev[-1:]), val[APD], FULL_SIZE, sz_cutoff, sigfigs) )
      online_ks[1] = pool.apply_async( genSizeKnapsack,
        (window_apd(apinfo,prev[-2:]), val[APD], FULL_SIZE, sz_cutoff, sigfigs) )
      online_ks[2] = pool.apply_async( genSizeKnapsack,
        (window_apd(apinfo,prev[-4:]), val[APD], FULL_SIZE, sz_cutoff, sigfigs) )
      online_ks[3] = pool.apply_async( genSizeKnapsack,
        (window_apd(apinfo,prev[-8:]), val[APD], FULL_SIZE, sz_cutoff, sigfigs) )
      online_ks[4] = pool.apply_async( genSizeKnapsack,
        (window_apd(apinfo,prev), val[APD], FULL_SIZE, sz_cutoff, sigfigs) )

#      ppv_results[10] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_N1_PER_50], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#      ppv_results[11] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_N2_PER_50], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#      ppv_results[12] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_N4_PER_50], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#      ppv_results[13] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_N8_PER_50], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#      ppv_results[14] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_NMAX_PER_50], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#
#      ppv_results[15] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_N1_PER_250], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#      ppv_results[16] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_N2_PER_250], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#      ppv_results[17] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_N4_PER_250], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#      ppv_results[18] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_N8_PER_250], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#      ppv_results[19] = pool.apply_async( compute_vals_size_hks_totals,
#        (apinfo[VALS][prev[-1]][ONLINE_FAST_NMAX_PER_250], apinfo[KLASS_MAP],
#         val[IDEAL_KS], val[APD]) )
#

#    if i == 1:
#      val[STATIC_SAME_KS] = genSizeKnapsack(apinfo[FULL][APD], val[APD], FULL_SIZE,
#                                            sz_cutoff, sigfigs)
#    else:
#      val[STATIC_SAME_KS] = genSizeKnapsack(apinfo[FULL][APD], val[APD], FULL_SIZE,
#                                            sz_cutoff, sigfigs)
    val[STATIC_SAME_KS] = static_same_ks.get()
    val[STATIC_DIFF_KS] = static_diff_ks.get()
    if prev:
      apinfo[VALS][prev[-1]][ONLINE_KS_N1]   = online_ks[0].get()
      apinfo[VALS][prev[-1]][ONLINE_KS_N2]   = online_ks[1].get()
      apinfo[VALS][prev[-1]][ONLINE_KS_N4]   = online_ks[2].get()
      apinfo[VALS][prev[-1]][ONLINE_KS_N8]   = online_ks[3].get()
      apinfo[VALS][prev[-1]][ONLINE_KS_NMAX] = online_ks[4].get()
      apinfo[VALS][prev[-1]][ONLINE_FAST_N1_PER_10]   = getOnlineHKs(apinfo, prev[-1:], PER_10)
      apinfo[VALS][prev[-1]][ONLINE_FAST_N2_PER_10]   = getOnlineHKs(apinfo, prev[-2:], PER_10)
      apinfo[VALS][prev[-1]][ONLINE_FAST_N4_PER_10]   = getOnlineHKs(apinfo, prev[-4:], PER_10)
      apinfo[VALS][prev[-1]][ONLINE_FAST_N8_PER_10]   = getOnlineHKs(apinfo, prev[-8:], PER_10)
      apinfo[VALS][prev[-1]][ONLINE_FAST_NMAX_PER_10] = getOnlineHKs(apinfo, prev, PER_10)

    full_ppv  = pool.apply_async( compute_vals_size_ks_totals,
                 (val[STATIC_SAME_KS], val[IDEAL_KS], val[APD], True, sz_cutoff) )
    guide_ppv = pool.apply_async( compute_vals_size_ks_totals,
                 (val[STATIC_DIFF_KS], val[IDEAL_KS], val[APD], True, sz_cutoff) )

    if prev:
      ppv_results = [None for _ in range(10)]
      ppv_results[0] = pool.apply_async( compute_vals_size_ks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_N1], val[IDEAL_KS], val[APD], True, sz_cutoff) )
      ppv_results[1] = pool.apply_async( compute_vals_size_ks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_N2], val[IDEAL_KS], val[APD], True, sz_cutoff) )
      ppv_results[2] = pool.apply_async( compute_vals_size_ks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_N4], val[IDEAL_KS], val[APD], True, sz_cutoff) )
      ppv_results[3] = pool.apply_async( compute_vals_size_ks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_N8], val[IDEAL_KS], val[APD], True, sz_cutoff) )
      ppv_results[4] = pool.apply_async( compute_vals_size_ks_totals,
        (apinfo[VALS][prev[-1]][ONLINE_KS_NMAX], val[IDEAL_KS], val[APD], True, sz_cutoff) )
      ppv_results[5] = pool.apply_async( compute_vals_size_hks_totals_v2,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N1_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD], sz_cutoff) )
      ppv_results[6] = pool.apply_async( compute_vals_size_hks_totals_v2,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N2_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD], sz_cutoff) )
      ppv_results[7] = pool.apply_async( compute_vals_size_hks_totals_v2,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N4_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD], sz_cutoff) )
      ppv_results[8] = pool.apply_async( compute_vals_size_hks_totals_v2,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_N8_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD], sz_cutoff) )
      ppv_results[9] = pool.apply_async( compute_vals_size_hks_totals_v2,
        (apinfo[VALS][prev[-1]][ONLINE_FAST_NMAX_PER_10], apinfo[KLASS_MAP],
         val[IDEAL_KS], val[APD], sz_cutoff) )

#    if val[IDEAL_KS]:
#      ideal_ppv = pool.apply_async( compute_vals_size_ks_totals,
#                    (val[IDEAL_KS], val[IDEAL_KS], val[APD], cutoff) )

    val[STATIC_SAME_PPV] = full_ppv.get()
    val[STATIC_DIFF_PPV] = guide_ppv.get()
    if prev:
      val[ONLINE_KS_N1_PPV]         = ppv_results[0].get()
      val[ONLINE_KS_N2_PPV]         = ppv_results[1].get()
      val[ONLINE_KS_N4_PPV]         = ppv_results[2].get()
      val[ONLINE_KS_N8_PPV]         = ppv_results[3].get()
      val[ONLINE_KS_NMAX_PPV]       = ppv_results[4].get()
      val[ONLINE_FAST_N1_10_PPV]    = ppv_results[5].get()
      val[ONLINE_FAST_N2_10_PPV]    = ppv_results[6].get()
      val[ONLINE_FAST_N4_10_PPV]    = ppv_results[7].get()
      val[ONLINE_FAST_N8_10_PPV]    = ppv_results[8].get()
      val[ONLINE_FAST_NMAX_10_PPV]  = ppv_results[9].get()
#      val[ONLINE_FAST_N1_10_PPV]    = compute_vals_size_hks_totals_v2(
#        apinfo[VALS][prev[-1]][ONLINE_FAST_N1_PER_10], apinfo[KLASS_MAP],
#        val[IDEAL_KS], val[APD], sz_cutoff )

#      val[ONLINE_FAST_N1_50_PPV]    = ppv_results[10].get()
#      val[ONLINE_FAST_N2_50_PPV]    = ppv_results[11].get()
#      val[ONLINE_FAST_N4_50_PPV]    = ppv_results[12].get()
#      val[ONLINE_FAST_N8_50_PPV]    = ppv_results[13].get()
#      val[ONLINE_FAST_NMAX_50_PPV]  = ppv_results[14].get()
#      val[ONLINE_FAST_N1_250_PPV]   = ppv_results[15].get()
#      val[ONLINE_FAST_N2_250_PPV]   = ppv_results[16].get()
#      val[ONLINE_FAST_N4_250_PPV]   = ppv_results[17].get()
#      val[ONLINE_FAST_N8_250_PPV]   = ppv_results[18].get()
#      val[ONLINE_FAST_NMAX_250_PPV] = ppv_results[19].get()
#
#    if val[IDEAL_KS]:
#      val[IDEAL_KS_PPV] = ideal_ppv.get()

    prev.append(i)

#    val[ONLINE_FAST_N1_PER_50]    = getOnlineHKs(apinfo, prev[-1:], PER_50)
#    val[ONLINE_FAST_N2_PER_50]    = getOnlineHKs(apinfo, prev[-2:], PER_50)
#    val[ONLINE_FAST_N4_PER_50]    = getOnlineHKs(apinfo, prev[-4:], PER_50)
#    val[ONLINE_FAST_N8_PER_50]    = getOnlineHKs(apinfo, prev[-8:], PER_50)
#    val[ONLINE_FAST_NMAX_PER_50]  = getOnlineHKs(apinfo, prev, PER_50)
#    val[ONLINE_FAST_N1_PER_250]   = getOnlineHKs(apinfo, prev[-1:], PER_250)
#    val[ONLINE_FAST_N2_PER_250]   = getOnlineHKs(apinfo, prev[-2:], PER_250)
#    val[ONLINE_FAST_N4_PER_250]   = getOnlineHKs(apinfo, prev[-4:], PER_250)
#    val[ONLINE_FAST_N8_PER_250]   = getOnlineHKs(apinfo, prev[-8:], PER_250)
#    val[ONLINE_FAST_NMAX_PER_250] = getOnlineHKs(apinfo, prev, PER_250)
#
    if not val[IDEAL_KS]:
      print "warning: could not compute knapsack for val: %d for %s-%s" % \
            (i, bench, cfg)

    pool.close()

  apinfo = compute_agg_size_ks_vals(apinfo, cutoff)
  kspklf = open(kszSimPkl(bench, cfg, iter, guide_bench, cutoff), 'w')
  dump(apinfo, kspklf)
  kspklf.close()

  return apinfo


def getSpace(addr, val):
  if addr >= val[EDEN][RANGE][0] and addr < val[EDEN][RANGE][1]:
    return EDEN
  elif addr >= val[SURVIVOR][RANGE][0] and addr < val[SURVIVOR][RANGE][1]:
    return SURVIVOR
  elif addr >= val[TENURED][RANGE][0] and addr < val[TENURED][RANGE][1]:
    return TENURED
  elif addr >= val[PERM][RANGE][0] and addr < val[PERM][RANGE][1]:
    return PERM
  else:
    return INVALID_SPACE

def assignRefs(pages, refs):

  ref_table = []

  refsPerPage, rem = divmod(refs, len(pages))
  for page in enumerate(pages):
    ref_table.append(refsPerPage)
  for i,_ in enumerate(range(rem)):
    ref_table[i] += 1

  return ref_table

def getPageSpan(addr, size):

  pages = []

  cur_addr  = addr
  rem       = size
  cur_page  = ((addr >> PAGE_SHIFT) << PAGE_SHIFT)
  next_page = (cur_page + PAGE_SIZE)

  # add in the current page
  pages.append(cur_page)

  # subtract the space left on the current page from the remaining size that
  # needs to be covered -- keep doing this until we have enough pages to cover
  # the size
  #
  rem -= (next_page - cur_addr)
  while rem > 0:
    cur_page = next_page
    next_page += PAGE_SIZE
    pages.append(cur_page)
    rem -= PAGE_SIZE

  return pages

def rangePages(low, high):
  pages = []
  cur_page = ((low >> PAGE_SHIFT) << PAGE_SHIFT)
  while cur_page < high:
    pages.append(cur_page)
    cur_page += PAGE_SIZE
  return pages

def parseSpaceInfo(addrinfof, space):
  spinfo = {}

  pts = addrinfof.next().split()
  used = int(pts[2])
  low  = int(pts[5],16)
  high = int(pts[6],16)

  spinfo[RANGE]          = (low, high)
  spinfo[USED_BYTES]     = used
  spinfo[TOTAL_PAGES]    = len(rangePages(low, high))

  pts = addrinfof.next().split()
  spinfo[LIVE_OBJECTS]   = int(pts[1])
  spinfo[LIVE_SIZE]      = int(pts[3])
  spinfo[LIVE_REFS]      = int(pts[5])

  pts = addrinfof.next().split()
  spinfo[HOT_OBJECTS]    = int(pts[1])
  spinfo[HOT_SIZE]       = int(pts[3])
  spinfo[HOT_REFS]       = int(pts[5])

  pts = addrinfof.next().split()
  spinfo[NEW_OBJECTS]    = int(pts[1])
  spinfo[NEW_SIZE]       = int(pts[3])
  spinfo[NEW_REFS]       = int(pts[5])

  pts = addrinfof.next().split()
  spinfo[VM_OBJECTS]     = int(pts[1])
  spinfo[VM_SIZE]        = int(pts[3])
  spinfo[VM_REFS]        = int(pts[5])

  pts = addrinfof.next().split()
  spinfo[FILLER_OBJECTS] = int(pts[1])
  spinfo[FILLER_SIZE]    = int(pts[3])
  spinfo[FILLER_REFS]    = int(pts[5])

  pts = addrinfof.next().split()
  spinfo[APP_OBJECTS]    = int(pts[1])
  spinfo[APP_SIZE]       = int(pts[3])
  spinfo[APP_REFS]       = int(pts[5])

  if space == PERM:
    # objects in perm space are not included in the log
    spinfo[HCO_OBJECTS]    = spinfo[HOT_OBJECTS]
    spinfo[HCO_SIZE]       = spinfo[HOT_SIZE]
    spinfo[HCO_REFS]       = spinfo[HOT_REFS]
  else:
    # get these later from the recs
    spinfo[HCO_OBJECTS]    = 0
    spinfo[HCO_SIZE]       = 0
    spinfo[HCO_REFS]       = 0

  # get these later from the recs
  spinfo[NEW_APP_OBJECTS]  = 0
  spinfo[NEW_APP_SIZE]     = 0
  spinfo[NEW_APP_REFS]     = 0

  # burn the next line
  line = addrinfof.next()

  return spinfo

def getOAIs(benches=dacapo_defs, cfgs=[ADDR_INFO_INTERVAL], iter=0, \
  spaces=ALL_SPACES, hot_cutoff=None, style=REF_CUTOFF, timer_cutoff=0, \
  history_vals=defhvals, future_vals=deffvals, future_grps=deffgrps, \
  clean=False):

  oais = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not oais.has_key(bench):
      oais[bench] = {}

    oais[bench][cfg] = getObjAddrInfo(bench=bench, cfg=cfg, spaces=spaces,
                                      hot_cutoff=hot_cutoff, style=style,
                                      timer_cutoff=timer_cutoff,
                                      history_vals=history_vals,
                                      future_vals=future_vals,
                                      future_grps=future_grps, clean=clean)
  return oais

def dumpOAI(oai):

  print "timer cutoff:  %d ms" % oai[TIMER_CUTOFF]
  print "hot cutoff:    %s"    % oai[HOT_CUTOFF]
  print "spaces:        ",       oai[SPACES]
  print "nvals:         %d"    % len(oai[VALS])

  print "averages:"
  print "  duration (ms): %d"    % oai[AVERAGE][DURATION]
  print "  used (KB):     %d"    % (int(oai[AVERAGE][USED_BYTES])   >> 10)
  print "  hot (KB):      %d"    % (int(oai[AVERAGE][HOT_SIZE])     >> 10)
  print "  hco (KB):      %d"    % (int(oai[AVERAGE][HCO_SIZE])     >> 10)
  print "  vm (KB):       %d"    % (int(oai[AVERAGE][VM_SIZE])      >> 10)
  print "  hot refs:      %5.4f" % oai[AVERAGE][HCO_REF_RATIO]
  print "  pages:         %d"    % oai[AVERAGE][TOTAL_PAGES]
  print "  hco pages:     %d"    % oai[AVERAGE][HCO_PAGES]
  print "  sim pages:     %d"    % oai[AVERAGE][SIM_PAGES]
  print "  hco ratio:     %5.4f" % oai[AVERAGE][HCO_PAGE_RATIO]
  print "  sim ratio:     %5.4f" % oai[AVERAGE][SIM_PAGE_RATIO]

  print "weighted averages:"
  print "  duration (ms): %d"    % oai[WGT_AVG][DURATION]
  print "  used (KB):     %d"    % (int(oai[WGT_AVG][USED_BYTES])   >> 10)
  print "  hot (KB):      %d"    % (int(oai[WGT_AVG][HOT_SIZE])     >> 10)
  print "  hco (KB):      %d"    % (int(oai[WGT_AVG][HCO_SIZE])     >> 10)
  print "  vm (KB):       %d"    % (int(oai[WGT_AVG][VM_SIZE])      >> 10)
  print "  hot refs:      %5.4f" % oai[WGT_AVG][HCO_REF_RATIO]
  print "  pages:         %d"    % oai[WGT_AVG][TOTAL_PAGES]
  print "  hco pages:     %d"    % oai[WGT_AVG][HCO_PAGES]
  print "  sim pages:     %d"    % oai[WGT_AVG][SIM_PAGES]
  print "  hco ratio:     %5.4f" % oai[WGT_AVG][HCO_PAGE_RATIO]
  print "  sim ratio:     %5.4f" % oai[WGT_AVG][SIM_PAGE_RATIO]

def getObjAddrInfo(bench, cfg=ADDR_INFO_INTERVAL, iter=0, \
  clean=False, spaces=ALL_SPACES, hot_cutoff=None, style=REF_CUTOFF, \
  timer_cutoff=0, history_vals=defhvals, future_vals=deffvals, \
  future_grps=deffgrps):

  oainfo = None
  if not clean:
    try:
      oainfo = load(open(objAddrInfoPkl(bench, cfg, iter, hot_cutoff,
                                        style=style)))
    except:
      oainfo = None
    if oainfo:
      if oainfo[TIMER_CUTOFF] != timer_cutoff or oainfo[HOT_CUTOFF] != hot_cutoff:
        oainfo = None

  if not oainfo:
    print "could not open object address info pkl for %s-%s-i%d, regenerating ..." % \
          (bench, cfg, iter)
    oainfo = parseObjAddrVals(bench, cfg, iter, hot_cutoff=hot_cutoff, \
                              style=style, timer_cutoff=timer_cutoff, \
                              history_vals=history_vals, \
                              future_vals=future_vals, future_grps=future_grps)

  oavals = oainfo[VALS]

  rec = {}
  rec[VALS] = []

  total_dur = sum( [ x[DURATION] for x in oavals ] )
  prev_minor_gc = prev_major_gc = False
  for i,val in enumerate(oavals):

    # skip 'pre-major-gc' vals because they throw off our new stats
    if val[REASON] == 'pre-major-gc':
      prev_minor_gc = False
      prev_major_gc = True
      continue

    cur_rec = {}
    cur_rec[VAL]          = val[VAL]
    cur_rec[DURATION]     = val[DURATION]
    cur_rec[DUR_RATIO]    = float(val[DURATION]) / total_dur # error if total_dur == 0
    cur_rec[REASON]       = val[REASON]
    cur_rec[USED_BYTES]   = sum ( [ val[x][USED_BYTES]   for x in spaces ] )

    # objects alive in this interval
    cur_rec[LIVE_OBJECTS] = sum ( [ val[x][LIVE_OBJECTS] for x in spaces ] )
    cur_rec[LIVE_SIZE]    = sum ( [ val[x][LIVE_SIZE]    for x in spaces ] )
    cur_rec[LIVE_REFS]    = sum ( [ val[x][LIVE_REFS]    for x in spaces ] )

    # objects new in this interval
    cur_rec[NEW_OBJECTS]  = sum ( [ val[x][NEW_OBJECTS]  for x in spaces ] )
    cur_rec[NEW_SIZE]     = sum ( [ val[x][NEW_SIZE]     for x in spaces ] )
    cur_rec[NEW_REFS]     = sum ( [ val[x][NEW_REFS]     for x in spaces ] )

    # objects referenced in this interval
    # XXX: just a temporary fix to handle future info -- we need to fix this
    gc_spaces = []
    if prev_minor_gc:
      gc_spaces = [SURVIVOR]
    elif prev_major_gc:
      gc_spaces = [SURVIVOR, TENURED]
    
    if gc_spaces:
      reg_spaces = [ x for x in spaces if not x in gc_spaces ]
      cur_rec[HOT_OBJECTS]  = sum ( [ val[x][HOT_OBJECTS]  for x in reg_spaces ] )
      cur_rec[HOT_SIZE]     = sum ( [ val[x][HOT_SIZE]     for x in reg_spaces ] )
      add_spaces = [ x for x in spaces if x in gc_spaces ]
      cur_rec[HOT_OBJECTS] += sum ( [ val[x][LIVE_OBJECTS]  for x in add_spaces ] )
      cur_rec[HOT_SIZE]    += sum ( [ val[x][LIVE_SIZE]     for x in add_spaces ] )
    else:
      cur_rec[HOT_OBJECTS]  = sum ( [ val[x][HOT_OBJECTS]  for x in spaces ] )
      cur_rec[HOT_SIZE]     = sum ( [ val[x][HOT_SIZE]     for x in spaces ] )

    # objects created by the VM that are alive in this interval (no reference
    # count info on these objects -- but assumed to be hot)
    #
    cur_rec[VM_OBJECTS]   = sum ( [ val[x][VM_OBJECTS]   for x in spaces ] )
    cur_rec[VM_SIZE]      = sum ( [ val[x][VM_SIZE]      for x in spaces ] )

    # objects created by the application alive at this interval
    cur_rec[APP_OBJECTS]  = sum ( [ val[x][APP_OBJECTS]  for x in spaces ] )
    cur_rec[APP_SIZE]     = sum ( [ val[x][APP_SIZE]     for x in spaces ] )
    cur_rec[APP_REFS]     = sum ( [ val[x][APP_REFS]     for x in spaces ] )

    # objects hot enough to pass the hot_cutoff ratio
    # (includes all VM objects)
    #
    cur_rec[HCO_OBJECTS]  = sum ( [ val[x][HCO_OBJECTS]  for x in spaces ] )
    cur_rec[HCO_SIZE]     = sum ( [ val[x][HCO_SIZE]     for x in spaces ] )
    cur_rec[HCO_REFS]     = sum ( [ val[x][HCO_REFS]     for x in spaces ] )

    # referenced objects - VM objects
    # this should exactly match the number of objects on the recs structure
    # after parseObjAddrVals (and we have found that it does in our current
    # framework -- as of 07/15)
    #
    cur_rec[HLVM_OBJECTS] = cur_rec[HOT_OBJECTS] - cur_rec[VM_OBJECTS]
    cur_rec[HLVM_SIZE]    = cur_rec[HOT_SIZE]    - cur_rec[VM_SIZE]

    # hot cutoff objects - VM objects
    cur_rec[CLVM_OBJECTS] = cur_rec[HCO_OBJECTS] - cur_rec[VM_OBJECTS]
    cur_rec[CLVM_SIZE]    = cur_rec[HCO_SIZE]    - cur_rec[VM_SIZE]

    cur_rec[TOTAL_PAGES]  = sum ( [ val[x][TOTAL_PAGES]  for x in spaces ] )
    cur_rec[HCO_PAGES]    = sum ( [ val[x][HCO_PAGES]    for x in spaces ] )
    cur_rec[SIM_PAGES]    = sum ( [ val[x][SIM_PAGES]    for x in spaces ] )

    # objects created by the application new at this interval
    cur_rec[NEW_APP_OBJECTS] = sum ( [ val[x][NEW_APP_OBJECTS]  for x in spaces ] )
    cur_rec[NEW_APP_SIZE]    = sum ( [ val[x][NEW_APP_SIZE]     for x in spaces ] )
    cur_rec[NEW_APP_REFS]    = sum ( [ val[x][NEW_APP_REFS]     for x in spaces ] )

    cur_rec[HLVM_OBJ_RATIO]  = -1.0 if cur_rec[APP_OBJECTS] == 0 else \
                                float(cur_rec[HLVM_OBJECTS]) / cur_rec[APP_OBJECTS]
    cur_rec[HLVM_SIZE_RATIO] = -1.0 if cur_rec[APP_SIZE] == 0 else \
                                float(cur_rec[HLVM_SIZE]) / cur_rec[APP_SIZE]

    cur_rec[CLVM_OBJ_RATIO]  = -1.0 if cur_rec[APP_OBJECTS] == 0 else \
                                float(cur_rec[CLVM_OBJECTS]) / cur_rec[APP_OBJECTS]
    cur_rec[CLVM_SIZE_RATIO] = -1.0 if cur_rec[APP_SIZE] == 0 else \
                                float(cur_rec[CLVM_SIZE]) / cur_rec[APP_SIZE]

    cur_rec[HCO_OBJ_RATIO]   = -1.0 if cur_rec[APP_OBJECTS] == 0 else \
                                float(cur_rec[HCO_OBJECTS]) / cur_rec[APP_OBJECTS]
    cur_rec[HCO_SIZE_RATIO]  = -1.0 if cur_rec[APP_SIZE] == 0 else \
                                float(cur_rec[HCO_SIZE]) / cur_rec[APP_SIZE]

    cur_rec[HCO_REF_RATIO]   = -1.0 if cur_rec[APP_REFS] == 0 else \
                                float(cur_rec[HCO_REFS]) / cur_rec[APP_REFS]
    cur_rec[HCO_PAGE_RATIO]  = -1.0 if cur_rec[TOTAL_PAGES] == 0 else \
                                float(cur_rec[HCO_PAGES]) / cur_rec[TOTAL_PAGES]
    cur_rec[SIM_PAGE_RATIO]  = -1.0 if cur_rec[TOTAL_PAGES] == 0 else \
                                float(cur_rec[SIM_PAGES]) / cur_rec[TOTAL_PAGES]

    cur_rec[NEW_APP_OBJ_RATIO] = -1.0 if cur_rec[APP_OBJECTS] == 0 else \
                                    float(cur_rec[NEW_APP_OBJECTS]) / cur_rec[APP_OBJECTS]
    cur_rec[NEW_APP_SIZE_RATIO]   = -1.0 if cur_rec[APP_SIZE] == 0 else \
                                    float(cur_rec[NEW_APP_SIZE]) / cur_rec[APP_SIZE]
    cur_rec[NEW_APP_REFS_RATIO]   = -1.0 if cur_rec[APP_REFS] == 0 else \
                                    float(cur_rec[NEW_APP_REFS]) / cur_rec[APP_REFS]


    # number (and size and refs) of hot application objects in this interval
    # that are 1) new (indexed by [NEW]), 2) accessed in the previous interval
    # or accessed at least once in some number of previous intervals (indexed
    # by the number of intervals or [MAX])
    #
    cur_rec[VAL_HISTORY] = newHistoryInfo(history_vals)
    vhi = cur_rec[VAL_HISTORY]
    for hval,hkey in history_vals:
      vhi[hkey][OBJECTS] = sum( [ val[x][VAL_HISTORY][hkey][OBJECTS] for x in spaces ] )
      vhi[hkey][SIZE]    = sum( [ val[x][VAL_HISTORY][hkey][SIZE]    for x in spaces ] )
      vhi[hkey][REFS]    = sum( [ val[x][VAL_HISTORY][hkey][REFS]    for x in spaces ] )

    # same numbers as above expressed as a ratio over all application objects
    # in this interval and over hot application objects in this interval
    #
    # TOTAL_*_RATIO is wrong because it only considers objects hot in this
    # interval in the numerator -- we should consider all live objects when
    # calculating the number of objects accessed in previous / subsequent
    # intervals
    #
    for hval,hkey in history_vals:
      vhi[hkey][TOTAL_OBJECT_RATIO] = -1.0 if cur_rec[APP_OBJECTS] == 0 else \
                                    float(vhi[hkey][OBJECTS]) / cur_rec[APP_OBJECTS]
      vhi[hkey][TOTAL_SIZE_RATIO] = -1.0 if cur_rec[APP_SIZE] == 0 else \
                                    float(vhi[hkey][SIZE]) / cur_rec[APP_SIZE]
      vhi[hkey][TOTAL_REFS_RATIO] = -1.0 if cur_rec[APP_REFS] == 0 else \
                                    float(vhi[hkey][REFS]) / cur_rec[APP_REFS]

      vhi[hkey][HOT_OBJECT_RATIO] = -1.0 if cur_rec[HLVM_OBJECTS] == 0 else \
                                    float(vhi[hkey][OBJECTS]) / cur_rec[HLVM_OBJECTS]
      vhi[hkey][HOT_SIZE_RATIO]   = -1.0 if cur_rec[HLVM_SIZE] == 0 else \
                                    float(vhi[hkey][SIZE]) / cur_rec[HLVM_SIZE]
      vhi[hkey][HOT_REFS_RATIO]   = -1.0 if cur_rec[APP_REFS] == 0 else \
                                    float(vhi[hkey][REFS]) / cur_rec[APP_REFS]

      vhi[hkey][NEW_APP_OBJ_RATIO]  = -1.0 if cur_rec[NEW_APP_OBJECTS] == 0 else \
                                      float(vhi[hkey][OBJECTS]) / cur_rec[NEW_APP_OBJECTS]
      vhi[hkey][NEW_APP_SIZE_RATIO] = -1.0 if cur_rec[NEW_APP_SIZE] == 0 else \
                                      float(vhi[hkey][SIZE]) / cur_rec[NEW_APP_SIZE]
      vhi[hkey][NEW_APP_REFS_RATIO] = -1.0 if cur_rec[NEW_APP_REFS] == 0 else \
                                      float(vhi[hkey][REFS]) / cur_rec[NEW_APP_REFS]

    # number (and size and refs) of hot application objects in this interval
    # that will 1) never be accessed again (indexed by [NEVER]), 2) will be
    # accessed in the next interval or accessed at least once in some
    # number of subsequent intervals (indexed by the number of intervals or
    # [MAX])
    #
    cur_rec[VAL_FUTURE] = newHistoryInfo(future_vals)
    vfi = cur_rec[VAL_FUTURE]
    for fval,fkey in future_vals:
      vfi[fkey][OBJECTS] = sum( [ val[x][VAL_FUTURE][fkey][OBJECTS] for x in spaces ] )
      vfi[fkey][SIZE]    = sum( [ val[x][VAL_FUTURE][fkey][SIZE]    for x in spaces ] )
      vfi[fkey][REFS]    = sum( [ val[x][VAL_FUTURE][fkey][REFS]    for x in spaces ] )

    # same numbers as above expressed as a ratio over all application objects
    # in this interval and over hot application objects in this interval
    #
    for fval,fkey in future_vals:
      vfi[fkey][TOTAL_OBJECT_RATIO] = -1.0 if cur_rec[APP_OBJECTS] == 0 else \
                                      float(vfi[fkey][OBJECTS]) / cur_rec[APP_OBJECTS]
      vfi[fkey][TOTAL_SIZE_RATIO]   = -1.0 if cur_rec[APP_SIZE] == 0 else \
                                      float(vfi[fkey][SIZE]) / cur_rec[APP_SIZE]
      vfi[fkey][TOTAL_REFS_RATIO]   = -1.0 if cur_rec[APP_REFS] == 0 else \
                                      float(vfi[fkey][REFS]) / cur_rec[APP_REFS]

      vfi[fkey][HOT_OBJECT_RATIO]   = -1.0 if cur_rec[HLVM_OBJECTS] == 0 else \
                                      float(vfi[fkey][OBJECTS]) / cur_rec[HLVM_OBJECTS]
      vfi[fkey][HOT_SIZE_RATIO]     = -1.0 if cur_rec[HLVM_SIZE] == 0 else \
                                      float(vfi[fkey][SIZE]) / cur_rec[HLVM_SIZE]
      vfi[fkey][HOT_REFS_RATIO]     = -1.0 if cur_rec[APP_REFS] == 0 else \
                                      float(vfi[fkey][REFS]) / cur_rec[APP_REFS]

      vfi[fkey][NEW_APP_OBJ_RATIO]  = -1.0 if cur_rec[NEW_APP_OBJECTS] == 0 else \
                                      float(vfi[fkey][OBJECTS]) / cur_rec[NEW_APP_OBJECTS]
      vfi[fkey][NEW_APP_SIZE_RATIO] = -1.0 if cur_rec[NEW_APP_SIZE] == 0 else \
                                      float(vfi[fkey][SIZE]) / cur_rec[NEW_APP_SIZE]
      vfi[fkey][NEW_APP_REFS_RATIO] = -1.0 if cur_rec[NEW_APP_REFS] == 0 else \
                                      float(vfi[fkey][REFS]) / cur_rec[NEW_APP_REFS]

    # number (and size and refs) of hot application objects in this interval
    # that will 1) never be accessed again (indexed by [NEVER]), 2) will be
    # accessed in the next interval or in a group of subsequent intervals.
    #
    cur_rec[GRP_FUTURE] = newHistoryInfo(future_grps)
    gfi = cur_rec[GRP_FUTURE]
    for fval,fkey in future_grps:
      gfi[fkey][OBJECTS] = sum( [ val[x][GRP_FUTURE][fkey][OBJECTS] for x in spaces ] )
      gfi[fkey][SIZE]    = sum( [ val[x][GRP_FUTURE][fkey][SIZE]    for x in spaces ] )
      gfi[fkey][REFS]    = sum( [ val[x][GRP_FUTURE][fkey][REFS]    for x in spaces ] )

    # same numbers as above expressed as a ratio over all application objects
    # in this interval and over hot application objects in this interval
    #
    for fval,fkey in future_grps:
      gfi[fkey][TOTAL_OBJECT_RATIO] = -1.0 if cur_rec[APP_OBJECTS] == 0 else \
                                      float(gfi[fkey][OBJECTS]) / cur_rec[APP_OBJECTS]
      gfi[fkey][TOTAL_SIZE_RATIO]   = -1.0 if cur_rec[APP_SIZE] == 0 else \
                                      float(gfi[fkey][SIZE]) / cur_rec[APP_SIZE]
      gfi[fkey][TOTAL_REFS_RATIO]   = -1.0 if cur_rec[APP_REFS] == 0 else \
                                      float(gfi[fkey][REFS]) / cur_rec[APP_REFS]

      gfi[fkey][HOT_OBJECT_RATIO] = -1.0 if cur_rec[HLVM_OBJECTS] == 0 else \
                                    float(gfi[fkey][OBJECTS]) / cur_rec[HLVM_OBJECTS]
      gfi[fkey][HOT_SIZE_RATIO]   = -1.0 if cur_rec[HLVM_SIZE] == 0 else \
                                    float(gfi[fkey][SIZE]) / cur_rec[HLVM_SIZE]
      gfi[fkey][HOT_REFS_RATIO]   = -1.0 if cur_rec[APP_REFS] == 0 else \
                                    float(gfi[fkey][REFS]) / cur_rec[APP_REFS]

      gfi[fkey][NEW_APP_OBJ_RATIO]  = -1.0 if cur_rec[NEW_APP_OBJECTS] == 0 else \
                                      float(gfi[fkey][OBJECTS]) / cur_rec[NEW_APP_OBJECTS]
      gfi[fkey][NEW_APP_SIZE_RATIO] = -1.0 if cur_rec[NEW_APP_SIZE] == 0 else \
                                      float(gfi[fkey][SIZE]) / cur_rec[NEW_APP_SIZE]
      gfi[fkey][NEW_APP_REFS_RATIO] = -1.0 if cur_rec[NEW_APP_REFS] == 0 else \
                                      float(gfi[fkey][REFS]) / cur_rec[NEW_APP_REFS]

    if val[REASON] == 'pre-minor-gc':
      prev_major_gc = False
      prev_minor_gc = True
    elif val[REASON] == 'pre-major-gc':
      prev_minor_gc = False
      prev_major_gc = True
    else:
      prev_minor_gc = prev_major_gc = False

    #print "val: %3d app: %10d hlvm: %10d hco: %10d hlvm_rat: %5.4f hco_rat: %5.4f" % \
    #      (cur_rec[VAL], cur_rec[APP_SIZE], cur_rec[HLVM_SIZE], cur_rec[CLVM_SIZE],
    #       cur_rec[HLVM_SIZE_RATIO], cur_rec[CLVM_SIZE_RATIO])
    rec[VALS].append(cur_rec)

  rec[TIMER_CUTOFF] = oainfo[TIMER_CUTOFF]
  rec[HOT_CUTOFF]   = oainfo[HOT_CUTOFF]
  rec[SPACES]       = spaces
  rec[AVERAGE]      = {}
  rec[MAX]          = {}
  rec[MIN]          = {}
  rec[WGT_AVG]      = {}
  rec[SUM_AGG]      = {}

  stats = [ DURATION, USED_BYTES, LIVE_OBJECTS, LIVE_SIZE, LIVE_REFS,\
            HOT_OBJECTS, HOT_SIZE, VM_OBJECTS, VM_SIZE, HLVM_OBJECTS,\
            HLVM_SIZE, APP_OBJECTS, APP_SIZE, APP_REFS, NEW_OBJECTS, NEW_SIZE,\
            NEW_REFS, HCO_OBJECTS, HCO_SIZE, HCO_REFS, TOTAL_PAGES, HCO_PAGES,\
            SIM_PAGES, HLVM_OBJ_RATIO, HLVM_SIZE_RATIO, CLVM_OBJ_RATIO,\
            CLVM_SIZE_RATIO, HCO_OBJ_RATIO, HCO_SIZE_RATIO, HCO_REF_RATIO,\
            HCO_PAGE_RATIO, SIM_PAGE_RATIO, NEW_APP_OBJECTS, NEW_APP_SIZE, \
            NEW_APP_REFS, NEW_APP_OBJ_RATIO, NEW_APP_SIZE_RATIO, \
            NEW_APP_REFS_RATIO ]

  sum_stats = [ NEW_APP_OBJECTS, NEW_APP_SIZE ]

  hf_stats  = [ OBJECTS, SIZE, REFS, TOTAL_OBJECT_RATIO, TOTAL_SIZE_RATIO, \
                TOTAL_REFS_RATIO, HOT_OBJECT_RATIO, HOT_SIZE_RATIO, \
                HOT_REFS_RATIO, NEW_APP_OBJ_RATIO, NEW_APP_SIZE_RATIO,
                NEW_APP_REFS_RATIO ]

  hf_sum_stats = [ OBJECTS, SIZE ]
  hf_srt_stats = { NEW_APP_OBJ_RATIO  : (OBJECTS, NEW_APP_OBJECTS), \
                   NEW_APP_SIZE_RATIO : (SIZE,    NEW_APP_SIZE) }

  rec[AVERAGE][VAL_HISTORY] = newHistoryInfo(history_vals)
  rec[MAX][VAL_HISTORY]     = newHistoryInfo(history_vals)
  rec[MIN][VAL_HISTORY]     = newHistoryInfo(history_vals)
  rec[WGT_AVG][VAL_HISTORY] = newHistoryInfo(history_vals)
  rec[SUM_AGG][VAL_HISTORY] = newHistoryInfo(history_vals)

  rec[AVERAGE][VAL_FUTURE]  = newHistoryInfo(future_vals)
  rec[MAX][VAL_FUTURE]      = newHistoryInfo(future_vals)
  rec[MIN][VAL_FUTURE]      = newHistoryInfo(future_vals)
  rec[WGT_AVG][VAL_FUTURE]  = newHistoryInfo(future_vals)
  rec[SUM_AGG][VAL_FUTURE]  = newHistoryInfo(future_vals)

  rec[AVERAGE][GRP_FUTURE]  = newHistoryInfo(future_grps)
  rec[MAX][GRP_FUTURE]      = newHistoryInfo(future_grps)
  rec[MIN][GRP_FUTURE]      = newHistoryInfo(future_grps)
  rec[WGT_AVG][GRP_FUTURE]  = newHistoryInfo(future_grps)
  rec[SUM_AGG][GRP_FUTURE]  = newHistoryInfo(future_grps)

  for stat in stats:
    vals  = rec[VALS]
    if stat.endswith("RATIO"):
      vals = [ x for x in rec[VALS] if x[stat] > -0.01 ]

    # there's a bug in python that causes 'average' to throw an exception on
    # really large values (which the REFS stats might have) ... just ignore
    # it for now
    #
    try:
      rec[AVERAGE][stat] = average( [ x[stat] for x in vals ] )
    except AttributeError:
      rec[AVERAGE][stat] = None

    rec[MAX][stat]     =     max( [ x[stat] for x in vals ] )
    rec[MIN][stat]     =     min( [ x[stat] for x in vals ] )

    xtdur = sum( [ x[DURATION] for x in vals ] )
    if xtdur == 0:
      rec[WGT_AVG][stat] = -1.0
    else:
      dvals = [ (x[stat], float(x[DURATION]) / xtdur) for x in vals ]
      try:
        rec[WGT_AVG][stat] = average( [ (val * (dur * len(dvals))) \
                                        for val,dur in dvals ] )
      except AttributeError:
        rec[WGT_AVG][stat] = None

    if stat in sum_stats:
      rec[SUM_AGG][stat] = sum( [ x[stat] for x in vals ] )

  for stat in hf_stats:

    for hval,hkey in history_vals:
      vals  = [ x for x in rec[VALS] ]
      if stat.endswith("RATIO"):
        vals  = [ x for x in rec[VALS] \
                  if x[VAL_HISTORY][hkey][stat] > -0.01 ]

      try:
        rec[AVERAGE][VAL_HISTORY][hkey][stat] = \
          average( [ x[VAL_HISTORY][hkey][stat] for x in vals ] )
      except AttributeError:
        rec[AVERAGE][VAL_HISTORY][hkey][stat] = None

      rec[MAX][VAL_HISTORY][hkey][stat] = \
        max( [ x[VAL_HISTORY][hkey][stat] for x in vals ] )
      rec[MIN][VAL_HISTORY][hkey][stat] = \
        min( [ x[VAL_HISTORY][hkey][stat] for x in vals ] )
      if stat in hf_sum_stats and "NEW" in hkey:
        rec[SUM_AGG][VAL_HISTORY][hkey][stat] = \
          sum( [ x[VAL_HISTORY][hkey][stat] for x in vals ] )

      xtdur = sum( [ x[DURATION] for x in vals ] )
      if xtdur == 0:
        rec[WGT_AVG][VAL_HISTORY][hkey][stat] = -1.0
      else:
        dvals = [ (x[VAL_HISTORY][hkey][stat], float(x[DURATION]) / xtdur) \
                  for x in vals ]
        try:
          rec[WGT_AVG][VAL_HISTORY][hkey][stat] = \
            average( [ (val * (dur * len(dvals))) for val,dur in dvals ] )
        except AttributeError:
          rec[WGT_AVG][VAL_HISTORY][hkey][stat] = None

    for fval,fkey in future_vals:
      vals  = [ x for x in rec[VALS] ]
      if stat.endswith("RATIO"):
        vals  = [ x for x in rec[VALS] \
                  if x[VAL_FUTURE][fkey][stat] > -0.01 ]

      try:
        rec[AVERAGE][VAL_FUTURE][fkey][stat] = \
          average( [ x[VAL_FUTURE][fkey][stat] for x in vals ] )
      except AttributeError:
        rec[AVERAGE][VAL_FUTURE][fkey][stat] = None

      rec[MAX][VAL_FUTURE][fkey][stat] = \
        max( [ x[VAL_FUTURE][fkey][stat] for x in vals ] )
      rec[MIN][VAL_FUTURE][fkey][stat] = \
        min( [ x[VAL_FUTURE][fkey][stat] for x in vals ] )
      if stat in hf_sum_stats and "NEW" in fkey:
        rec[SUM_AGG][VAL_FUTURE][fkey][stat] = \
          sum( [ x[VAL_FUTURE][fkey][stat] for x in vals ] )

      xtdur = sum( [ x[DURATION] for x in vals ] )
      if xtdur == 0:
        rec[WGT_AVG][VAL_FUTURE][fkey][stat] = -1.0
      else:
        dvals = [ (x[VAL_FUTURE][fkey][stat], float(x[DURATION]) / xtdur) \
                  for x in vals ]
        try:
          rec[WGT_AVG][VAL_FUTURE][fkey][stat] = \
            average( [ (val * (dur * len(dvals))) for val,dur in dvals ] )
        except AttributeError:
          rec[WGT_AVG][VAL_FUTURE][fkey][stat] = None

    for fgrp,fkey in future_grps:
      vals  = [ x for x in rec[VALS] ]
      if stat.endswith("RATIO"):
        vals  = [ x for x in rec[VALS] \
                  if x[GRP_FUTURE][fkey][stat] > -0.01 ]

      try:
        rec[AVERAGE][GRP_FUTURE][fkey][stat] = \
          average( [ x[GRP_FUTURE][fkey][stat] for x in vals ] )
      except AttributeError:
        rec[AVERAGE][GRP_FUTURE][fkey][stat] = None

      rec[MAX][GRP_FUTURE][fkey][stat] = \
        max( [ x[GRP_FUTURE][fkey][stat] for x in vals ] )
      rec[MIN][GRP_FUTURE][fkey][stat] = \
        min( [ x[GRP_FUTURE][fkey][stat] for x in vals ] )
      if stat in hf_sum_stats and "NEW" in fkey:
        rec[SUM_AGG][GRP_FUTURE][fkey][stat] = \
          sum( [ x[GRP_FUTURE][fkey][stat] for x in vals ] )

      xtdur = sum( [ x[DURATION] for x in vals ] )
      if xtdur == 0:
        rec[WGT_AVG][GRP_FUTURE][fkey][stat] = -1.0
      else:
        dvals = [ (x[GRP_FUTURE][fkey][stat], float(x[DURATION]) / xtdur) \
                  for x in vals ]
        try:
          rec[WGT_AVG][GRP_FUTURE][fkey][stat] = \
            average( [ (val * (dur * len(dvals))) for val,dur in dvals ] )
        except AttributeError:
          rec[WGT_AVG][GRP_FUTURE][fkey][stat] = None

  vals  = [ x for x in rec[VALS] if not x[REASON] == 'pre-major-gc']
  for stat in hf_srt_stats.keys():
    for hval,hkey in [ x for x in history_vals if "NEW" in x[1]]:
      vstat,bstat = hf_srt_stats[stat]
      val   = sum( [ x[VAL_HISTORY][hkey][vstat] for x in vals ] )
      base  = sum( [ x[bstat] for x in vals ] )
      rec[SUM_AGG][VAL_HISTORY][hkey][stat] = (float(val) / base)
    for fval,fkey in [ x for x in future_vals if "NEW" in x[1]]:
      vstat,bstat = hf_srt_stats[stat]
      val   = sum( [ x[VAL_FUTURE][fkey][vstat] for x in vals ] )
      base  = sum( [ x[bstat] for x in vals ] )
      rec[SUM_AGG][VAL_FUTURE][fkey][stat] = (float(val) / base)
    for fval,fkey in [ x for x in future_grps if "NEW" in x[1]]:
      vstat,bstat = hf_srt_stats[stat]
      val   = sum( [ x[GRP_FUTURE][fkey][vstat] for x in vals ] )
      base  = sum( [ x[bstat] for x in vals ] )
      rec[SUM_AGG][GRP_FUTURE][fkey][stat] = (float(val) / base)

  return rec

#  # take care of TOTAL_HOT's
#  for stat in [OBJECTS,SIZE,REFS]:
#    vals  = [ x for x in rec[VALS] ]
#
#    rec[AVERAGE][VAL_HISTORY][TOTAL_HOT][stat] = \
#      average( [ x[VAL_HISTORY][TOTAL_HOT][stat] for x in vals ] )
#    rec[AVERAGE][VAL_FUTURE][TOTAL_HOT][stat] = \
#      average( [ x[VAL_FUTURE][TOTAL_HOT][stat] for x in vals ] )
#    rec[MAX][VAL_HISTORY][TOTAL_HOT][stat] = \
#      max( [ x[VAL_HISTORY][TOTAL_HOT][stat] for x in vals ] )
#    rec[MAX][VAL_FUTURE][TOTAL_HOT][stat] = \
#      max( [ x[VAL_FUTURE][TOTAL_HOT][stat] for x in vals ] )
#    rec[MIN][VAL_HISTORY][TOTAL_HOT][stat] = \
#      min( [ x[VAL_HISTORY][TOTAL_HOT][stat] for x in vals ] )
#    rec[MIN][VAL_FUTURE][TOTAL_HOT][stat] = \
#      min( [ x[VAL_FUTURE][TOTAL_HOT][stat] for x in vals ] )
#
#    xtdur = sum( [ x[DURATION] for x in vals ] )
#    if xtdur == 0:
#      rec[WGT_AVG][VAL_HISTORY][TOTAL_HOT][stat] = -1.0
#      rec[WGT_AVG][VAL_FUTURE][TOTAL_HOT][stat]  = -1.0
#    else:
#      dvals = [ (x[VAL_HISTORY][TOTAL_HOT][stat], float(x[DURATION]) / xtdur) \
#                for x in vals ]
#      rec[WGT_AVG][VAL_HISTORY][TOTAL_HOT][stat] = \
#        average( [ (val * (dur * len(dvals))) for val,dur in dvals ] )
#
#      dvals = [ (x[VAL_FUTURE][TOTAL_HOT][stat], float(x[DURATION]) / xtdur) \
#                for x in vals ]
#      rec[WGT_AVG][VAL_FUTURE][TOTAL_HOT][stat] = \
#        average( [ (val * (dur * len(dvals))) for val,dur in dvals ] )

def addRec(statd, rec):
  statd[OBJECTS] += 1
  statd[SIZE]    += rec[1]
  statd[REFS]    += rec[2] 

def addBulkRec(statd, bulk_rec):
  statd[OBJECTS] += bulk_rec[0]
  statd[SIZE]    += bulk_rec[1]
  statd[REFS]    += bulk_rec[2] 

def subRec(statd, rec):
  statd[OBJECTS] -= 1
  statd[SIZE]    -= rec[1]
  statd[REFS]    -= rec[2] 

def updateRecordHistory(recs, rec_history, history_vals, space_val, reason):

  for i,rec in enumerate(recs):
    addr,size,refs,new = rec
    if refs >= 0:
      rec_history[addr] = VMAX

  for addr in rec_history.keys():
    rec_history[addr] -= 1

  if reason == 'pre-minor-gc':
    for addr in rec_history.keys():
      if getSpace(addr, space_val) in [EDEN, SURVIVOR]:
        del rec_history[addr]
  elif reason == 'pre-major-gc':
    for addr in rec_history.keys():
      if getSpace(addr, space_val) in [EDEN, SURVIVOR, TENURED]:
        del rec_history[addr]

def newHistoryInfo(history_vals):

  vhi = {}

  for hval,hkey in history_vals:
    vhi[hkey] = {}
    vhi[hkey][OBJECTS] = 0
    vhi[hkey][SIZE]    = 0
    vhi[hkey][REFS]    = 0

  return vhi

def postGCBuildHistoryInfo(cur_val, rec_history, history_vals, gc_spaces):

  __history_vals = [ x for x in history_vals if x[0] != None ]
  for space in gc_spaces:
    vhi      = cur_val[space][VAL_HISTORY]
    bulk_rec = ( cur_val[space][APP_OBJECTS], \
                 cur_val[space][APP_SIZE],    \
                 cur_val[space][APP_REFS] )

    if vhi.has_key(TOTAL_HOT):
      addBulkRec( vhi[TOTAL_HOT], bulk_rec )
    if vhi.has_key(NEW):
      addBulkRec( vhi[NEW], bulk_rec )

    cur_val[space][NEW_APP_OBJECTS] += cur_val[space][APP_OBJECTS]
    cur_val[space][NEW_APP_SIZE]    += cur_val[space][APP_SIZE]
    cur_val[space][NEW_APP_REFS]    += cur_val[space][APP_REFS]

  for addr in rec_history.keys():
    space = getSpace (addr, cur_val)
    if space in gc_spaces:
      vhi = cur_val[space][VAL_HISTORY]
      for hval,hkey in __history_vals:
        if rec_history[addr] >= (VMAX - hval):
          addRec(vhi[hkey], rec)

  return None

# MRJ - buildHistoryInfo is currently useful for finding the set of new
# objects, but the history_vals might not be correct or might not be very
# useful at this point
#
def buildHistoryInfo(cur_val, recs, rec_history, history_vals, prev_reason):

  for space in ALL_SPACES:
    cur_val[space][VAL_HISTORY] = newHistoryInfo(history_vals)

  gc_spaces = []
  if prev_reason in ['pre-minor-gc', 'pre-major-gc']:
    gc_spaces = [SURVIVOR] if prev_reason == 'pre-minor-gc' else [SURVIVOR, TENURED]
    postGCBuildHistoryInfo(cur_val, rec_history, history_vals, gc_spaces)
    #return None

  __history_vals = [ x for x in history_vals if x[0] != None ]
  for i,rec in enumerate(recs):
    addr,size,refs,new = rec
    if refs >= 0:

      space = getSpace(addr, cur_val)
      if space in gc_spaces:
        continue

      vhi = cur_val[space][VAL_HISTORY]
      if vhi.has_key(TOTAL_HOT):
        addRec(vhi[TOTAL_HOT], rec)

      if not rec_history.has_key(addr):
        if vhi.has_key(NEW):
          addRec(vhi[NEW], rec)

        rec[3] = True
        cur_val[space][NEW_APP_OBJECTS] += 1
        cur_val[space][NEW_APP_SIZE]    += size
        cur_val[space][NEW_APP_REFS]    += refs

      else:
        for hval,hkey in __history_vals:
          if rec_history[addr] >= (VMAX - hval):
            addRec(vhi[hkey], rec)

  return None

def gcRecordFuture(rec_future, reason, space_val):

  if reason == 'pre-minor-gc':
    for addr in rec_future.keys():
      if getSpace(addr, space_val) in [EDEN, SURVIVOR]:
        del rec_future[addr]
  elif reason == 'pre-major-gc':
    for addr in rec_future.keys():
      if getSpace(addr, space_val) in [EDEN, SURVIVOR, TENURED]:
        del rec_future[addr]

def updateRecordFuture(recs, rec_future):

  for addr in rec_future.keys():
    rec_future[addr][1][VMAX-1] += rec_future[addr][1][VMAX-2]
    for val in [x for x in reversed(range(VMAX-1))][:(VMAX-2)]:
      rec_future[addr][1][val] = rec_future[addr][1][val-1]
    rec_future[addr][1][0] = 0

  for i,rec in enumerate(recs):
    addr,size,refs,new = rec
    if refs >= 0:
      if not rec_future.has_key(addr):
        rec_future[addr] = (size, [ 0 for x in range(VMAX) ])
      assert (rec_future[addr][1][0] == 0), "bad future rec!"
      rec_future[addr][1][0] = refs

def preGCBuildFutureInfo(cur_val, recs, rec_future, future_vals, future_grps, \
  gc_spaces, prev_bottoms):

  __tot_future_vals = [ x for x in future_vals if x[0] != None and not "NEW" in x[1] ]
  __tot_future_grps = [ x for x in future_grps if x[0] != None and not "NEW" in x[1] ]
  __new_future_vals = [ x for x in future_vals if x[0] != None and "NEW" in x[1]     ]
  __new_future_grps = [ x for x in future_grps if x[0] != None and "NEW" in x[1]     ]

  for space in gc_spaces:
    vfi      = cur_val[space][VAL_FUTURE]
    gfi      = cur_val[space][GRP_FUTURE]
    bulk_rec = ( cur_val[space][APP_OBJECTS], \
                 cur_val[space][APP_SIZE],    \
                 cur_val[space][APP_REFS] )

    prev_bottom = prev_bottoms[0] if space == SURVIVOR else prev_bottoms[1]

    if vfi.has_key(TOTAL_HOT):
      addBulkRec(vfi[TOTAL_HOT], bulk_rec)
    if gfi.has_key(TOTAL_HOT):
      addBulkRec(gfi[TOTAL_HOT], bulk_rec)

    if vfi.has_key(TOTAL_NEW):
      addBulkRec(vfi[TOTAL_NEW], bulk_rec)
    if gfi.has_key(TOTAL_NEW):
      addBulkRec(gfi[TOTAL_NEW], bulk_rec)

    #cur_val[space][NEW_APP_OBJECTS] += cur_val[space][APP_OBJECTS]
    #cur_val[space][NEW_APP_SIZE]    += cur_val[space][APP_SIZE]
    #cur_val[space][NEW_APP_REFS]    += cur_val[space][APP_REFS]

    if vfi.has_key(NEVER):
      addBulkRec(vfi[NEVER], bulk_rec)
    if gfi.has_key(NEVER):
      addBulkRec(gfi[NEVER], bulk_rec)
    if vfi.has_key(NEVER_NEW):
      addBulkRec(vfi[NEVER_NEW], bulk_rec)
    if gfi.has_key(NEVER_NEW):
      addBulkRec(gfi[NEVER_NEW], bulk_rec)

    for addr in rec_future.keys():
      if space == getSpace(addr, cur_val):

        size = rec_future[addr][0]
        try:
          refs = recs[recs.index(addr)][2]
        except ValueError:
          refs = 0
        rec = (addr, size, refs)

        if vfi.has_key(NEVER):
          subRec(vfi[NEVER], rec)
        if gfi.has_key(NEVER):
          subRec(gfi[NEVER], rec)

        if prev_bottom == None or addr < prev_bottom:
          if vfi.has_key(NEVER_NEW):
            subRec(vfi[NEVER_NEW], rec)
          if gfi.has_key(NEVER_NEW):
            subRec(gfi[NEVER_NEW], rec)

        for fval,fkey in __tot_future_vals:
          refs = sum([rec_future[addr][1][i] for i in range(fval)])
          if refs > 0:
            rec = (addr, size, refs)
            addRec(vfi[fkey], rec)

        for fgrp,fkey in __tot_future_grps:
          refs = sum([rec_future[addr][1][i-1] for i in fgrp])
          if refs > 0:
            rec = (addr, size, refs)
            addRec(gfi[fkey], rec)

        if prev_bottom == None or addr < prev_bottom:
          for fval,fkey in __new_future_vals:
            refs = sum([rec_future[addr][1][i] for i in range(fval)])
            if refs > 0:
              rec = (addr, size, refs)
              addRec(vfi[fkey], rec)

          for fgrp,fkey in __new_future_grps:
            refs = sum([rec_future[addr][1][i-1] for i in fgrp])
            if refs > 0:
              rec = (addr, size, refs)
              addRec(gfi[fkey], rec)

  return None

def buildFutureInfo(cur_val, recs, rec_future, future_vals, future_grps, \
  next_reason, prev_bottoms):

  for space in ALL_SPACES:
    cur_val[space][VAL_FUTURE] = newHistoryInfo(future_vals)
    cur_val[space][GRP_FUTURE] = newHistoryInfo(future_grps)

  gc_spaces = []
  if next_reason in ['pre-minor-gc', 'pre-major-gc']:
    gc_spaces = [SURVIVOR] if next_reason == 'pre-minor-gc' else [SURVIVOR, TENURED]
    preGCBuildFutureInfo(cur_val, recs, rec_future, future_vals, future_grps, \
                         gc_spaces, prev_bottoms)
    #return None

  __tot_future_vals = [ x for x in future_vals if x[0] != None and not "NEW" in x[1] ]
  __tot_future_grps = [ x for x in future_grps if x[0] != None and not "NEW" in x[1] ]
  __new_future_vals = [ x for x in future_vals if x[0] != None and "NEW" in x[1]     ]
  __new_future_grps = [ x for x in future_grps if x[0] != None and "NEW" in x[1]     ]

  new_cnt     = 0
  surv_bottom = None
  ten_bottom  = None
  for i,rec in enumerate(recs):
    addr,size,refs,new = rec
    if refs >= 0:

      space = getSpace(addr, cur_val)
      if space in gc_spaces:
        continue

      vfi = cur_val[space][VAL_FUTURE]
      gfi = cur_val[space][GRP_FUTURE]

      if vfi.has_key(TOTAL_HOT):
        addRec(vfi[TOTAL_HOT], rec)
      if gfi.has_key(TOTAL_HOT):
        addRec(gfi[TOTAL_HOT], rec)

      if new:

        if space == SURVIVOR:
          if surv_bottom == None or addr < surv_bottom:
            surv_bottom = addr
        elif space == TENURED:
          if ten_bottom == None or addr < ten_bottom:
            ten_bottom = addr

        if vfi.has_key(TOTAL_NEW):
          addRec(vfi[TOTAL_NEW], rec)
        if gfi.has_key(TOTAL_NEW):
          addRec(gfi[TOTAL_NEW], rec)

      if not rec_future.has_key(addr):
        if vfi.has_key(NEVER):
          addRec(vfi[NEVER], rec)
        if gfi.has_key(NEVER):
          addRec(gfi[NEVER], rec)
        if new:
          if vfi.has_key(NEVER_NEW):
            addRec(vfi[NEVER_NEW], rec)
          if gfi.has_key(NEVER_NEW):
            addRec(gfi[NEVER_NEW], rec)
      else:
        for fval,fkey in __tot_future_vals:
          refs = sum([rec_future[addr][1][i] for i in range(fval)])
          if refs > 0:
            rec[2] = refs
            addRec(vfi[fkey], rec)

        for fgrp,fkey in __tot_future_grps:
          refs = sum([rec_future[addr][1][i-1] for i in fgrp])
          if refs > 0:
            rec[2] = refs
            addRec(gfi[fkey], rec)

#        for fval,fkey in __tot_future_vals:
#          if sum([rec_future[addr][i] for i in range(fval)]) > 0:
#            addRec(vfi[fkey], rec)
#
#        for fgrp,fkey in __tot_future_grps:
#          if sum([rec_future[addr][i-1] for i in fgrp]) > 0:
#            addRec(gfi[fkey], rec)

        if new:

          for fval,fkey in __new_future_vals:
            refs = sum([rec_future[addr][1][i] for i in range(fval)])
            if refs > 0:
              rec[2] = refs
              addRec(vfi[fkey], rec)

          for fgrp,fkey in __new_future_grps:
            refs = sum([rec_future[addr][1][i-1] for i in fgrp])
            if refs > 0:
              rec[2] = refs
              addRec(gfi[fkey], rec)

#          for fval,fkey in __new_future_vals:
#            if sum([rec_future[addr][i] for i in range(fval)]) > 0:
#              addRec(vfi[fkey], rec)
#
#          for fgrp,fkey in __new_future_grps:
#            if sum([rec_future[addr][i-1] for i in fgrp]) > 0:
#              addRec(gfi[fkey], rec)

  #print "new_cnt: %d" % new_cnt
  return (surv_bottom, ten_bottom)

#def updateRecordFuture(recs, rec_future, future_vals):
#
#  for i,rec in enumerate(recs):
#    addr,size,refs = rec
#    if refs >= 0:
#      rec_future[addr] = 0
#
#  for addr in rec_future.keys():
#    rec_future[addr] += 1
#
#def newFutureInfo(future_vals):
#
#  vhi = {}
#
#  for fval in future_vals + [NEVER,MAX,TOTAL_HOT]:
#    vhi[fval] = {}
#    vhi[fval][OBJECTS] = 0
#    vhi[fval][SIZE]    = 0
#    vhi[fval][REFS]    = 0
#
#  return vhi
#
#def buildValFutureInfo(cur_val, recs, rec_future, future_vals):
#
#  for space in ALL_SPACES:
#    cur_val[space][VAL_FUTURE] = newFutureInfo(future_vals)
#
#  for i,rec in enumerate(recs):
#    addr,size,refs = rec
#    if refs >= 0:
#      space = getSpace(addr, cur_val)
#      vfi   = cur_val[space][VAL_FUTURE]
#
#      vfi[TOTAL_HOT][OBJECTS] += 1
#      vfi[TOTAL_HOT][SIZE]    += size
#      vfi[TOTAL_HOT][REFS]    += refs
#
#      if not rec_future.has_key(addr):
#        vfi[NEVER][OBJECTS] += 1
#        vfi[NEVER][SIZE]    += size
#        vfi[NEVER][REFS]    += refs
#      else:
#        vfi[MAX][OBJECTS] += 1
#        vfi[MAX][SIZE]    += size
#        vfi[MAX][REFS]    += refs
#        for fval in future_vals:
#          if rec_future[addr] <= fval:
#            vfi[fval][OBJECTS] += 1
#            vfi[fval][SIZE]    += size
#            vfi[fval][REFS]    += refs
#
#  return None

def getRPBCutoffs(cur_val, recs, hot_cutoff):

  rcd = {}
  for space in ALL_SPACES:
    rcd[space] = 1

  if hot_cutoff:
    for space in ALL_SPACES:
      space_recs  = [ (rec, float(rec[2])/rec[1]) for rec in recs \
                      if getSpace(rec[0], cur_val) == space ]
      sorted_recs = sorted( space_recs, key=itemgetter(1), reverse=True)
      total_refs  = sum([x[0][2] for x in space_recs if x[0][2] > 0])
      tot_cutoff  = total_refs * hot_cutoff

      cur_refs    = 0
      rcd[space]  = 1
      for i,rec in enumerate(sorted_recs):
        cur_refs += rec[0][2]
        if cur_refs > tot_cutoff:
          rcd[space] = rec[1]
          break

  return rcd 

def getRecCutoffs(cur_val, recs, hot_cutoff):

  rcd = {}
  for space in ALL_SPACES:
    rcd[space] = 1

  if hot_cutoff:
    for space in ALL_SPACES:
      space_recs  = [ x for x in recs if getSpace(x[0], cur_val) == space ]
      sorted_recs = sorted( space_recs, key=itemgetter(2), reverse=True)
      total_refs  = sum([x[2] for x in space_recs if x[2] > 0])
      tot_cutoff  = total_refs * hot_cutoff

      cur_refs    = 0
      rcd[space]  = 1
      for i,rec in enumerate(sorted_recs):
        cur_refs += rec[2]
        if cur_refs > tot_cutoff:
          rcd[space] = rec[2]
          break

  return rcd 

def getHotPages(val, space):
  if space == PERM:
    hot_low  = val[space][RANGE][0]
    hot_high = hot_low + val[space][HCO_SIZE]
    return len(rangePages(hot_low, hot_high))

  return len(val[space][HCO_PAGE_HISTO])

def getSimPages(val, space):
  sim_low  = val[space][RANGE][0]
  sim_high = sim_low + val[space][HCO_SIZE]
  return len(rangePages(sim_low, sim_high))

def buildKSSizeHPH(cur_val, recs, cutoff, nsigs=1000):

  bad_recs = bad_size = 0
  vm_pages = []
  vm_pages_dict = {}
  for space in ALL_SPACES:
    vm_pages_dict[space] = []

  for i,rec in enumerate(recs):
    addr,size,refs,new = rec
    space = getSpace(addr, cur_val)
    if space == INVALID_SPACE:
      bad_recs += 1
      bad_size += size
      continue

    pages = getPageSpan(addr, size)
    if refs < 0:
      cur_val[space][HCO_OBJECTS] += 1
      cur_val[space][HCO_SIZE]    += size
      for page in pages:
        vm_pages.append(page)
        vm_pages_dict[getSpace(page, cur_val)].append(page)

  if bad_recs > 0:
    print "warning (val=%d): %d objects outside known heap, %d KB" % \
          (val, bad_recs, (bad_size >> 10))

  for page in vm_pages:
    space = getSpace(page, cur_val)
    histo = cur_val[space][HCO_PAGE_HISTO]
    if not histo.has_key(page):
      histo[page] = "vm-object"

  for space in ALL_SPACES:
    cur_val[space][HCO_PAGE_HISTO] = {}

  print "hola"
  wincs = (10**sigfigs)
  hot_recs = [ rec for rec in recs if rec[2] > 0 ]
  total_refs = sum( [ refs for addr,size,refs in hot_recs ] )

  wgts = [ (idx, int((float(refs) / total_refs) * wincs)) \
           for idx,(addr,size,refs) in enumerate(hot_recs) ]

  sorted_wgts = sorted(wgts, reverse=True)

  sig_wgts = [ x for x in sorted_wgts if x > 0 ]
  insig_wgts = sorted_wgts[len(sig_wgts):]
  
  wgt_incs = range(int(cutoff*wincs))

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  print "sig_wgts ", len(sig_wgts)
  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[1] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[1]] + hot_recs[wgt[0]][1]))
      else:
        m[i].append(m[i-1][j])
    #if i % 10 == 0:
      #print "m[%d][%d] = %d" % (i,j,m[i][j])

  ks = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      ks.append(sig_wgts[i-1][0])
      j -= sig_wgts[i-1][1]
    i -= 1

  for wgt in insig_wgts:
    ks.append(wgt[0])

  print "got ks ", len(ks)
  for idx in ks:
    addr,size,refs = hot_recs[idx]
    space = getSpace(addr, cur_val)
    histo = cur_val[space][HCO_PAGE_HISTO]
    pages = getPageSpan(addr, size)

    cur_val[space][HCO_OBJECTS] += 1
    cur_val[space][HCO_SIZE]    += size
    cur_val[space][HCO_REFS]    += refs
    ref_assign = assignRefs(pages, refs)
    for page,ref in zip(pages,ref_assign):
      if not histo.has_key(page):
        histo[page] = refs
      else:
        histo[page] += refs

  return None


def buildKSRefsHPH(cur_val, recs, cutoff, nsigs=1000):

  for space in ALL_SPACES:
    cur_val[space][HCO_PAGE_HISTO] = {}

  wincs      = (10**sigfigs)
  hot_recs   = [ rec for rec in recs if rec[2] > 0 ]
  total_refs = sum( [ refs for addr,size,refs in hot_recs ] )

  wgts = [ (idx, int((float(refs) / total_refs) * wincs)) \
           for idx,(addr,size,refs) in enumerate(hot_recs) ]

  sorted_wgts = sorted(wgts, key=itemgetter(1), reverse=True)

  #sig_wgts = [ x for x in sorted_wgts if x[1] > 0 ]
  sig_idxs  = array( [ x[0] for x in sorted_wgts if x[1] > 0 ] )
  sig_wgts  = array( [ x[1] for x in sorted_wgts if x[1] > 0 ] )
  insig_wgts = sorted_wgts[len(sig_wgts):]

  #print sig_wgts[0], sig_idxs[0], hot_recs[sig_idxs[0]]

  hot_sizes = array( [ hot_recs[x][1] for x in sig_idxs ] )

  print "hot_recs: %d sig_wgts: %d insig_wgts: %d" % \
        ( len(hot_recs), len(sig_wgts), len(insig_wgts) )

  refs       = sum ([ x[2] for x in hot_recs ])
  insig_refs = sum ([ hot_recs[x[0]][2] for x in insig_wgts ])
  sig_refs   = sum ([ hot_recs[x][2] for x in sig_idxs   ])
  print "refs: %10d sig: %10d insig: %10d" % (refs, sig_refs, insig_refs)
  
  wgt_incs = range(int(cutoff*wincs))

  m = empty( [len(sig_wgts)+1, len(wgt_incs)] )
  for i,j in enumerate(wgt_incs):
    m[0][i] = 0

#  if cur_val[VAL] == 4:
#    for i,sw in enumerate(sig_wgts):
#      if i%100 == 0:
#        print sw
  for i,wgt in enumerate((sig_wgts),start=1):
    #print "wgt_incs ", len(wgt_incs)
    for j,inc in enumerate(wgt_incs):
      if wgt <= inc:
        #print wgt, hot_sizes[i-1]
        m[i][j] = max(m[i-1][j], m[i-1][inc-wgt] + hot_sizes[i-1])
      else:
        m[i][j] = m[i-1][j]
    #if i % 10 == 0:
    #  print "m[%d][%d] = %d" % (i,j,m[i][j])

#  wincs = (10**sigfigs)
#  hot_recs = [ rec for rec in recs if rec[2] > 0 ]
#  total_refs = sum( [ refs for addr,size,refs in hot_recs ] )
#
#  wgts = [ (idx, int((float(refs) / total_refs) * wincs)) \
#           for idx,(addr,size,refs) in enumerate(hot_recs) ]
#
#  sorted_wgts = sorted(wgts, key=itemgetter(1), reverse=True)
#
#  sig_wgts = [ x for x in sorted_wgts if x[1] > 0 ]
#  insig_wgts = sorted_wgts[len(sig_wgts):]
#
#  print "hot_recs: %d sig_wgts: %d insig_wgts: %d" % \
#        ( len(hot_recs), len(sig_wgts), len(insig_wgts) )
#
#  refs       = sum ([ x[2] for x in hot_recs ])
#  insig_refs = sum ([ hot_recs[x[0]][2] for x in insig_wgts ])
#  sig_refs   = sum ([ hot_recs[x[0]][2] for x in sig_wgts   ])
#  print "refs: %10d sig: %10d insig: %10d" % (refs, sig_refs, insig_refs)
#  
#  wgt_incs = range(int(cutoff*wincs))

#  m = []
#  m.append([])
#  for j in wgt_incs:
#    m[0].append(0)
#
##  if cur_val[VAL] == 4:
##    for i,sw in enumerate(sig_wgts):
##      if i%100 == 0:
##        print sw
#  for i,wgt in enumerate(sig_wgts,start=1):
#    m.append([])
#    #print "wgt_incs ", len(wgt_incs)
#    for j,inc in enumerate(wgt_incs):
#      if wgt[1] <= inc:
#        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[1]] + hot_recs[wgt[0]][1]))
#      else:
#        m[i].append(m[i-1][j])
#    #if cur_val[VAL] == 4 and i % 10 == 0:
#    #  print "m[%d][%d] = %d" % (i,j,m[i][j])

  ks = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      ks.append(sig_idxs[i-1])
      j -= sig_wgts[i-1]
    i -= 1

  for wgt in insig_wgts:
    ks.append(wgt[0])

  hot_idxs = range(len(hot_recs))
  my_hots  = set(hot_idxs) - set(ks)
  print "got ks: %d my_hots: %d" % (len(ks), len(my_hots))
  for idx in my_hots:
    addr,size,refs = hot_recs[idx]
    space = getSpace(addr, cur_val)
    histo = cur_val[space][HCO_PAGE_HISTO]
    pages = getPageSpan(addr, size)

    cur_val[space][HCO_OBJECTS] += 1
    cur_val[space][HCO_SIZE]    += size
    cur_val[space][HCO_REFS]    += refs
    ref_assign = assignRefs(pages, refs)
    for page,ref in zip(pages,ref_assign):
      if not histo.has_key(page):
        histo[page] = refs
      else:
        histo[page] += refs

  hco_size = sum( [ cur_val[s][HCO_SIZE]   for s in APP_SPACES ] )
  hco_refs = sum( [ cur_val[s][HCO_REFS]   for s in APP_SPACES ] )
  tot_size = sum( [ cur_val[s][USED_BYTES] for s in APP_SPACES ] )
  tot_refs = sum( [ cur_val[s][LIVE_REFS]  for s in APP_SPACES ] )

  size_rat = 0 if tot_size == 0 else float(hco_size) / tot_size
  refs_rat = 0 if tot_refs == 0 else float(hco_refs) / tot_refs
  print "size : %10d %10d %10d %10d" % (hco_size, tot_size, hco_refs, tot_refs)
  print "size_rat: %1.3f ref_rat: %1.3f" % (size_rat, refs_rat)

  bad_recs = bad_size = 0
  vm_pages = []
  vm_pages_dict = {}
  for space in ALL_SPACES:
    vm_pages_dict[space] = []

  for i,rec in enumerate(recs):
    addr,size,refs,new = rec
    space = getSpace(addr, cur_val)
    if space == INVALID_SPACE:
      bad_recs += 1
      bad_size += size
      continue

    pages = getPageSpan(addr, size)
    if refs < 0:
      cur_val[space][HCO_OBJECTS] += 1
      cur_val[space][HCO_SIZE]    += size
      for page in pages:
        vm_pages.append(page)
        vm_pages_dict[getSpace(page, cur_val)].append(page)

  if bad_recs > 0:
    print "warning (val=%d): %d objects outside known heap, %d KB" % \
          (val, bad_recs, (bad_size >> 10))

  for page in vm_pages:
    space = getSpace(page, cur_val)
    histo = cur_val[space][HCO_PAGE_HISTO]
    if not histo.has_key(page):
      histo[page] = "vm-object"

  return None

def buildKSHotPageHisto(cur_val, recs, cutoff, nsigs=1000, style=REFS):
  if style == REFS:
    buildKSRefsHPH(cur_val, recs, cutoff, nsigs=nsigs)
  elif style == SIZE:
    buildKSSizeHPH(cur_val, recs, cutoff, nsigs=nsigs)
  else:
    print "invalid style: %s" % style
    raise SystemExit(1)

def buildRCHotPageHisto(cur_val, recs, hot_cutoff):

  bad_recs = bad_size = 0
  vm_pages = []
  vm_pages_dict = {}
  for space in ALL_SPACES:
    vm_pages_dict[space] = []
    cur_val[space][HCO_PAGE_HISTO] = {}

  rcd = getRecCutoffs(cur_val, recs, hot_cutoff)
  for space in ALL_SPACES:
    if rcd[space] < 1:
      print "error: rec_cutoff for %s is < 1: rc = %d" % (space, rcd[space])
      raise SystemExit(1)

#  eden_len = len ( [ x for x in recs if getSpace(x[0], cur_val) == EDEN     ] )
#  surv_len = len ( [ x for x in recs if getSpace(x[0], cur_val) == SURVIVOR ] )
#  ten_len  = len ( [ x for x in recs if getSpace(x[0], cur_val) == TENURED  ] )
#  perm_len = len ( [ x for x in recs if getSpace(x[0], cur_val) == PERM     ] )
#  print "%3d %10d %10d %10d %10d %10d" % \
#        (cur_val[VAL], len(recs), eden_len, surv_len, ten_len, perm_len)

  for i,rec in enumerate(recs):
    addr,size,refs,new = rec
    space = getSpace(addr, cur_val)
    if space == INVALID_SPACE:
#      print "rec:  ", hex(rec[0])
#      print "eden: ", (hex(cur_val[EDEN][RANGE][0]),hex(cur_val[EDEN][RANGE][1]))
#      print "surv: ", (hex(cur_val[SURVIVOR][RANGE][0]),hex(cur_val[SURVIVOR][RANGE][1]))
#      print "old:  ", (hex(cur_val[TENURED][RANGE][0]),hex(cur_val[TENURED][RANGE][1]))
#      return
      bad_recs += 1
      bad_size += size
      continue

    histo = cur_val[space][HCO_PAGE_HISTO]
    pages = getPageSpan(addr, size)
    if refs < 0:
      cur_val[space][HCO_OBJECTS] += 1
      cur_val[space][HCO_SIZE]    += size
      for page in pages:
        vm_pages.append(page)
        vm_pages_dict[getSpace(page, cur_val)].append(page)
    else:
      if refs >= rcd[space]:
        cur_val[space][HCO_OBJECTS] += 1
        cur_val[space][HCO_SIZE]    += size
        cur_val[space][HCO_REFS]    += refs
        ref_assign = assignRefs(pages, refs)
        for page,ref in zip(pages,ref_assign):
          if not histo.has_key(page):
            histo[page] = refs
          else:
            histo[page] += refs

  if bad_recs > 0:
    print "warning (val=%d): %d objects outside known heap, %d KB" % \
          (val, bad_recs, (bad_size >> 10))

  for page in vm_pages:
    space = getSpace(page, cur_val)
    histo = cur_val[space][HCO_PAGE_HISTO]
    if not histo.has_key(page):
      histo[page] = "vm-object"

  return None

def buildRPBHotPageHisto(cur_val, recs, hot_cutoff):

  vm_pages = []
  vm_pages_dict = {}
  for space in ALL_SPACES:
    vm_pages_dict[space] = []
    cur_val[space][HCO_PAGE_HISTO] = {}

  rcd = getRPBCutoffs(cur_val, recs, hot_cutoff)
  rpb_recs = [ (rec, float(rec[2])/rec[1]) for rec in recs ]
  for i,rec in enumerate(rpb_recs):
    ((addr,size,refs,new),rpb) = rec
    space = getSpace(addr, cur_val)

    histo = cur_val[space][HCO_PAGE_HISTO]
    pages = getPageSpan(addr, size)
    if refs < 0:
      cur_val[space][HCO_OBJECTS] += 1
      cur_val[space][HCO_SIZE]    += size
      for page in pages:
        vm_pages.append(page)
        vm_pages_dict[getSpace(page, cur_val)].append(page)
    else:
      if rpb >= rcd[space]:
        cur_val[space][HCO_OBJECTS] += 1
        cur_val[space][HCO_SIZE]    += size
        cur_val[space][HCO_REFS]    += refs
        ref_assign = assignRefs(pages, refs)
        for page,ref in zip(pages,ref_assign):
          if not histo.has_key(page):
            histo[page] = refs
          else:
            histo[page] += refs

  for page in vm_pages:
    space = getSpace(page, cur_val)
    histo = cur_val[space][HCO_PAGE_HISTO]
    if not histo.has_key(page):
      histo[page] = "vm-object"

  return None

def parseObjAddrVals(bench, cfg=ADDR_INFO_INTERVAL, iter=0, \
  hot_cutoff=None, style=REF_CUTOFF, timer_cutoff=0, history_vals=defhvals,
  future_vals=deffvals, future_grps=deffgrps):

  recs = getObjAddrTable(bench, cfg=cfg, iter=iter)
  print "  got object address table"

  addrinfof = try_open_read(objAddrInfoLog(bench, cfg, iter))

  oainfo = {}
  oainfo[TIMER_CUTOFF]   = timer_cutoff
  oainfo[HOT_CUTOFF]     = hot_cutoff
  oainfo[RECORD_HISTORY] = {}
  oainfo[RECORD_FUTURE]  = {}
  oainfo[VALS]           = []
  next_reason            = None
  prev_reason            = None
  for line in addrinfof:
    if oaicValRE.match(line):  
      val    = int(line.split()[2])
      dur    = int(line.split()[7])
      reason = addrinfof.next().split()[1].strip('()')

      if dur < timer_cutoff or reason in ['post-minor-gc', 'post-major-gc']:
        continue

      cur_val = {}
      cur_val[VAL]      = val
      cur_val[DURATION] = dur
      cur_val[REASON]   = reason
      cur_val[EDEN]     = parseSpaceInfo(addrinfof, EDEN)
      cur_val[SURVIVOR] = parseSpaceInfo(addrinfof, SURVIVOR)
      cur_val[TENURED]  = parseSpaceInfo(addrinfof, TENURED)
      cur_val[PERM]     = parseSpaceInfo(addrinfof, PERM)

      if style == REF_CUTOFF:
        buildRCHotPageHisto(cur_val, recs[val], hot_cutoff)
      elif style == RPB_CUTOFF:
        buildRPBHotPageHisto(cur_val, recs[val], hot_cutoff)
      elif style == KNAPSACK:
        buildKSHotPageHisto(cur_val, recs[val], hot_cutoff)

      for space in ALL_SPACES:
        cur_val[space][HCO_PAGES] = getHotPages(cur_val, space)
        cur_val[space][SIM_PAGES] = getSimPages(cur_val, space)

      if history_vals:
        buildHistoryInfo(cur_val, recs[val], oainfo[RECORD_HISTORY], \
                         history_vals, prev_reason)
        updateRecordHistory(recs[val], oainfo[RECORD_HISTORY], history_vals, \
                            cur_val, reason)
        prev_reason = reason

      print "hph: %d" % val
      oainfo[VALS].append(cur_val)

  next_reason  = None
  prev_bottoms = (None, None)
  if future_vals and future_grps:

    space_val = oainfo[VALS][-1]
    for cur_val in reversed(oainfo[VALS]):
      val    = cur_val[VAL]
      reason = cur_val[REASON]

      if reason in ['pre-minor-gc','pre-major-gc']:
        gcRecordFuture(oainfo[RECORD_FUTURE], reason, space_val)
        space_val = cur_val

      cur_idx = oainfo[VALS].index(cur_val)
      if cur_idx > 0:
        next_reason = oainfo[VALS][cur_idx-1][REASON]
      
      prev_bottoms = buildFutureInfo(cur_val, recs[val], oainfo[RECORD_FUTURE], \
                                     future_vals, future_grps, next_reason, \
                                     prev_bottoms)
      updateRecordFuture(recs[val], oainfo[RECORD_FUTURE])
      sbstr = hex(prev_bottoms[0]) if prev_bottoms[0] != None else str(None)
      tbstr = hex(prev_bottoms[1]) if prev_bottoms[1] != None else str(None)
      print "gfi: %5d surv_bottom: %s ten_bottom: %s" % (val, sbstr, tbstr)

  print "  dumping oavals"
  dump(oainfo, open(objAddrInfoPkl(bench, cfg, iter, hot_cutoff, style), 'w'))
  print "  done"

  return oainfo

def getObjAddrTable(bench, cfg=ADDR_INFO_INTERVAL, iter=0):

  addrtablef = try_open_read(objAddrTableLog(bench, cfg, iter), bin=True)

  addrtablef.seek(0,2)
  atf_size = addrtablef.tell()

  # format is padded with an extra integer because of the way structs are
  # aligned in the log file
  #
  fmt = 'Piq'
  rec_size = calcsize(fmt)

  recs = []
  addrtablef.seek(0)

  try:
    rec_str = addrtablef.read(rec_size)
    #cur_rec = unpack(fmt, rec_str)
    addr,size,refs = unpack(fmt, rec_str) 
    cur_rec = [addr, size, refs, False]

    while ((atf_size - addrtablef.tell()) >= rec_size):

      if cur_rec[0] != OAR_MARKER:
        raise

      val = cur_rec[1]

      recs.append([])
      cur_val = recs[-1]

      print "val: ", ("%d" % val).rjust(5)
      #if val % 10 == 0:
      #  print "val: ", ("%d" % val).rjust(5)
      rec_str = addrtablef.read(rec_size)
      #cur_rec = unpack(fmt, rec_str) 
      addr,size,refs = unpack(fmt, rec_str) 
      cur_rec = [addr, size, refs, False]
      while cur_rec[0] != OAR_MARKER:
        cur_val.append(cur_rec)
        rec_str = addrtablef.read(rec_size)

        if not rec_str:
          break

        #cur_rec = unpack(fmt, rec_str) 
        addr,size,refs = unpack(fmt, rec_str) 
        cur_rec = [addr, size, refs, False]


  except:
    print "error reading address table"
    return None

  return recs

def printObjAddrTable(bench, cfg=ADDR_INFO_INTERVAL, iter=0, nmax=None, outf=sys.stdout):

  addrtablef = try_open_read(objAddrTableLog(bench, cfg, iter), bin=True)

  addrtablef.seek(0,2)
  atf_size = addrtablef.tell()

  # format is padded with an extra integer because of the way structs are
  # aligned in the log file
  #
  fmt = 'Piiii'
  rec_size = calcsize(fmt)

  i = 0
  done = False

  recs = []
  addrtablef.seek(0)

  try:
    rec_str = addrtablef.read(rec_size)
    cur_rec = unpack(fmt, rec_str) 

    while ((atf_size - addrtablef.tell()) >= rec_size):

      if cur_rec[0] != OAR_MARKER:
        raise

      val = cur_rec[1]

      recs.append([])
      cur_val = recs[-1]

      print "%18s %16s %16s %4s (val=%d)" % ("address:", "size:", "refs:", "new:", val)

      rec_str = addrtablef.read(rec_size)
      cur_rec = unpack(fmt, rec_str) 
      while cur_rec[0] != OAR_MARKER:
        addr = cur_rec[0]
        size = cur_rec[1]
        refs = cur_rec[2]
        new  = cur_rec[3]
        print >> outf, ("0x%016x %16d %16d %4d" % (addr, size, refs, new))
        i += 1
        if nmax and i >= nmax:
          done = True
          break
        cur_val.append(cur_rec[:-1])
        rec_str = addrtablef.read(rec_size)

        if not rec_str:
          break

        cur_rec = unpack(fmt, rec_str) 
      if done:
        break

  except:
    print "error reading address table"
    return

  return 

def getOFIs(benches=dacapo_defs, cfgs=[FIELD_TLAB_INTERVAL], iter=0, \
  cutoff=1, clean=False):

  ofis = {}
  for bench,cfg in expiter(benches, cfgs):
    print "  %s-%s" % (bench,cfg)
    if not ofis.has_key(bench):
      ofis[bench] = {}

    ofis[bench][cfg] = getObjFieldInfo(bench=bench, cfg=cfg, clean=clean)
  return ofis

def pairwise(iterable):
  a, b = tee(iterable)
  next(b, None)
  return izip(a, b)

def getIntStats(line, pos):
  x = line.split()
  return [ int(x[i]) for i in pos ]

def getKlassRecords(teefile):

  klass_recs = []

  for line, nextline in teefile:
    if not klassRecRE.match(line):
      break

    cur_klass = {}

    pts = line.split()
    cur_klass[KLASS_ADDR]    = int(pts[2].strip('{}'), 16)
    cur_klass[INSTANCE_SIZE] = int(pts[3]) if pts[3].isdigit() else -1
    cur_klass[KLASS_TYPE]    = pts[5].upper()
    cur_klass[KLASS_NAME]    = pts[6]

    line, nextline = teefile.next()
    cur_klass[LIVE_OBJECTS] = tuple(getIntStats(line, (1,2,3)))

    line, nextline = teefile.next()
    cur_klass[HOT_OBJECTS]  = tuple(getIntStats(line, (1,2,3)))

    line, nextline = teefile.next()
    cur_klass[NEW_OBJECTS]  = tuple(getIntStats(line, (1,2,3)))

#    cur_klass[FIELDS] = []
#
#    if nextline.strip() == "fields:":
#      line, nextline = teefile.next()
#
#      while fieldRecRE.match(nextline):
#        line, nextline = teefile.next()
#        cur_klass[FIELDS].append(tuple(getIntStats(line, (0,1))))
#
#    if cur_klass[INSTANCE_SIZE] > 0:
#      # assume 12 bytes for the mark and klass words
#      header_bytes = OOP_HEADER_SIZE
#      covered  = set(range(vm_bytes))
#      for field in cur_klass[FIELDS]:
#        covered.update(set(range(field[0], field[0]+field[1])))
    
    if cur_klass[INSTANCE_SIZE] > 0:
      covered = []
      if nextline.strip() == "fields:":
        line, nextline = teefile.next()
        while not (klassRecRE.match(nextline) or nextline.strip() == ''):
          line, nextline = teefile.next()
          covered.append(getIntStats(line, [0])[0])

      cur_klass[COVERED_BYTES] = covered
      cur_klass[PACKED_SIZE]   = (len(covered) + OOP_HEADER_SIZE)

      align_bytes = divmod(cur_klass[PACKED_SIZE], WORD_SIZE)[1]
      padding     = 0 if align_bytes == 0 else (WORD_SIZE - align_bytes)

      cur_klass[PACKED_SIZE_ALIGN] = cur_klass[PACKED_SIZE] + padding

    #if cur_klass[KLASS_ADDR] == 218876704:
    #  print cur_klass
    #print "waka"
    klass_recs.append(cur_klass)


  return klass_recs

def getObjFieldInfo(bench, cfg=FIELD_TLAB_INTERVAL, iter=0, clean=False):

  finfo = None
  if not clean:
    try:
      finfo = load(open(objFieldInfoPkl(bench, cfg, iter)))
    except:
      print "could not open field info pkl, ",
      finfo = None

  if not finfo:
    print "collecting field info for %s-%s-i%d ..." % \
          (bench, cfg, iter)

  fieldinfof = try_open_read(objFieldInfoLog(bench, cfg, iter))

  finfo = {}
  finfo[VALS]    = []
  finfo[AVERAGE] = {}
  finfo[WGT_AVG] = {}

  cur_fieldinfof, next_fieldinfof = tee(fieldinfof)
  next_fieldinfof.next()
  teefile = izip(cur_fieldinfof, next_fieldinfof)

  for line, nextline in teefile:
    if not oaicValRE.match(line):
      break

    val, dur = getIntStats(line, (2,7))
    reason   = teefile.next()[0].split()[2].strip('()')

    cur_val = {}
    cur_val[VAL]       = val
    cur_val[DURATION]  = dur
    cur_val[REASON]    = reason

    teefile.next()
    cur_val[KLASSES] = getIntStats(teefile.next()[0], [2])
    cur_val[LIVE_OBJECTS], cur_val[LIVE_SIZE] = getIntStats(teefile.next()[0], (2, 5))
    cur_val[HOT_OBJECTS],  cur_val[HOT_SIZE]  = getIntStats(teefile.next()[0], (2, 5))
    cur_val[NEW_OBJECTS],  cur_val[NEW_SIZE]  = getIntStats(teefile.next()[0], (2, 5))
    #cur_val[INS_OBJECTS],  cur_val[INS_SIZE]  = getIntStats(teefile.next()[0], (2, 5))
    #cur_val[ARR_OBJECTS],  cur_val[ARR_SIZE]  = getIntStats(teefile.next()[0], (2, 5))

    # discard the blank line
    teefile.next()
    cur_val[KLASS_RECORDS] = getKlassRecords(teefile)

#    cur_val[HOT_INS_OBJECTS], cur_val[HOT_INS_SIZE] = \
#      getIntStats(teefile.next()[0], (2, 5))
#
#    cur_val[HOT_ARR_OBJECTS], cur_val[HOT_ARR_SIZE] = \
#      getIntStats(teefile.next()[0], (2, 5))
#
    cur_val[HOT_OBJ_RATIO]  = 0. if cur_val[LIVE_OBJECTS] == 0 else \
                              (float(cur_val[HOT_OBJECTS]) / cur_val[LIVE_OBJECTS])
    cur_val[HOT_SIZE_RATIO] = 0. if cur_val[LIVE_SIZE] == 0 else \
                             (float(cur_val[HOT_SIZE]) / cur_val[LIVE_SIZE])
#
#    cur_val[INS_OBJ_RATIO]  = 0. if cur_val[LIVE_OBJECTS] == 0 else \
#                              (float(cur_val[INS_OBJECTS]) / cur_val[LIVE_OBJECTS])
#    cur_val[INS_SIZE_RATIO] = 0. if cur_val[LIVE_SIZE] == 0 else \
#                             (float(cur_val[INS_SIZE]) / cur_val[LIVE_SIZE])
#
#    cur_val[ARR_OBJ_RATIO]  = 0. if cur_val[LIVE_OBJECTS] == 0 else \
#                              (float(cur_val[ARR_OBJECTS]) / cur_val[LIVE_OBJECTS])
#    cur_val[ARR_SIZE_RATIO] = 0. if cur_val[LIVE_SIZE] == 0 else \
#                             (float(cur_val[ARR_SIZE]) / cur_val[LIVE_SIZE])
#
#
#    cur_val[HOT_INS_OBJ_RATIO]  = 0. if cur_val[HOT_OBJECTS] == 0 else \
#                                (float(cur_val[HOT_INS_OBJECTS]) / cur_val[HOT_OBJECTS])
#    cur_val[HOT_INS_SIZE_RATIO] = 0. if cur_val[HOT_SIZE] == 0 else \
#                                (float(cur_val[HOT_INS_SIZE]) / cur_val[HOT_SIZE])
#
#    cur_val[HOT_ARR_OBJ_RATIO]  = 0. if cur_val[HOT_OBJECTS] == 0 else \
#                                (float(cur_val[HOT_ARR_OBJECTS]) / cur_val[HOT_OBJECTS])
#    cur_val[HOT_ARR_SIZE_RATIO] = 0. if cur_val[HOT_SIZE] == 0 else \
#                                (float(cur_val[HOT_ARR_SIZE]) / cur_val[HOT_SIZE])
                                
    finfo[VALS].append(cur_val)
 
  total_dur = sum ( [ x[DURATION] for x in finfo[VALS] ] )
  for val in finfo[VALS]:
    val[DUR_RATIO] = 0. if total_dur == 0 else float(val[DURATION]) / total_dur


#  avg_stats = [DURATION, LIVE_OBJECTS, LIVE_SIZE, HOT_OBJECTS, HOT_SIZE,
#               INS_OBJECTS, INS_SIZE, ARR_OBJECTS, ARR_SIZE, HOT_INS_OBJECTS,
#               HOT_INS_SIZE, HOT_ARR_OBJECTS, HOT_ARR_SIZE, HOT_OBJ_RATIO,
#               HOT_SIZE_RATIO, INS_OBJ_RATIO, INS_SIZE_RATIO, ARR_OBJ_RATIO,
#               ARR_SIZE_RATIO, HOT_INS_OBJ_RATIO, HOT_INS_SIZE_RATIO,
#               HOT_ARR_OBJ_RATIO, HOT_ARR_SIZE_RATIO]
  avg_stats = [DURATION, LIVE_OBJECTS, LIVE_SIZE, HOT_OBJECTS, HOT_SIZE,
               HOT_OBJ_RATIO, HOT_SIZE_RATIO, NEW_OBJECTS, NEW_SIZE]

  for stat in avg_stats:
    avg_vals = [ x[stat] for x in finfo[VALS] ]
    wgt_vals = [ x[stat]*(x[DUR_RATIO]*len(finfo[VALS])) for x in finfo[VALS] ]

    finfo[AVERAGE][stat] = average(avg_vals)
    finfo[WGT_AVG][stat] = average(wgt_vals)

  finfo[AGGREGATE] = getAggFieldInfo(finfo)

  dump(finfo, open(objFieldInfoPkl(bench, cfg, iter), 'w'))

  return finfo

def getAggFieldInfo(ofinfo, vrange=None):

  aggkrd = {}

  if vrange == None:
    vrange = range(len(ofinfo[VALS]))

  for i,v in enumerate(vrange):
    for klass in ofinfo[VALS][v][KLASS_RECORDS]:
      key = klass[KLASS_ADDR]
      cur_objects, cur_size, cur_refs = klass[LIVE_OBJECTS]
      new_objects, new_size, new_refs = klass[NEW_OBJECTS]
      cur_frefs = 0
      if klass[KLASS_TYPE] == KT_APP_INSTANCE:
        cur_frefs = (cur_refs - new_refs)

      # klass has not been seen before
      if not aggkrd.has_key(key):
        aggkrd[key] = {}
        aggkrd[key][KLASS_NAME]    = klass[KLASS_NAME]
        aggkrd[key][KLASS_TYPE]    = klass[KLASS_TYPE]
        aggkrd[key][INSTANCE_SIZE] = klass[INSTANCE_SIZE]
        aggkrd[key][TOTAL_OBJECTS] = (cur_objects, cur_objects)
        aggkrd[key][TOTAL_SIZE]    = (cur_size,    cur_size)
        aggkrd[key][TOTAL_REFS]    = cur_refs
        aggkrd[key][FIELD_REFS]    = cur_frefs

        if aggkrd[key][INSTANCE_SIZE] > 0:
          covered = klass[COVERED_BYTES] + range(OOP_HEADER_SIZE)
          aggkrd[key][COVERED_BYTES] = set(covered)
        
      else:
#        if ofinfo[VALS][v][REASON] in ["post-minor-gc", "post-major-gc"]:
#          new_objects = 0
#          new_size    = 0
#        else:
#          new_objects = cur_objects - aggkrd[key][TOTAL_OBJECTS][1]
#          new_size    = cur_size    - aggkrd[key][TOTAL_SIZE][1]

        total_objects = aggkrd[key][TOTAL_OBJECTS][0] + new_objects
        total_size    = aggkrd[key][TOTAL_SIZE][0]    + new_size
        total_refs    = aggkrd[key][TOTAL_REFS]       + cur_refs
        total_frefs   = aggkrd[key][FIELD_REFS]       + cur_frefs

        aggkrd[key][TOTAL_OBJECTS] = (total_objects, cur_objects)
        aggkrd[key][TOTAL_SIZE]    = (total_size,    cur_size)
        aggkrd[key][TOTAL_REFS]    = total_refs
        aggkrd[key][FIELD_REFS]    = total_frefs

        if aggkrd[key][INSTANCE_SIZE] > 0:
#          if not aggkrd[key].has_key(COVERED_BYTES):
#            print aggkrd[key]
#          if not klass.has_key(COVERED_BYTES):
#            print klass
#            print aggkrd[key]
          aggkrd[key][COVERED_BYTES].update(klass[COVERED_BYTES])

        prev_objects = cur_objects
        prev_size    = cur_size

  for klass in aggkrd.keys():
    aggkrd[klass][TOTAL_OBJECTS] = aggkrd[klass][TOTAL_OBJECTS][0]
    aggkrd[klass][TOTAL_SIZE]    = aggkrd[klass][TOTAL_SIZE][0]

    if aggkrd[klass][INSTANCE_SIZE] > 0:
      aggkrd[klass][PACKED_SIZE] = len(aggkrd[klass][COVERED_BYTES])
      align_bytes = divmod(aggkrd[klass][PACKED_SIZE], WORD_SIZE)[1]
      padding     = 0 if align_bytes == 0 else (WORD_SIZE - align_bytes)
      aggkrd[klass][PACKED_SIZE_ALIGN]  = aggkrd[klass][PACKED_SIZE] + padding

      aggkrd[klass][BYTES_WASTED]       = aggkrd[klass][TOTAL_OBJECTS] * \
        (aggkrd[klass][INSTANCE_SIZE] - aggkrd[klass][PACKED_SIZE])

      aggkrd[klass][BYTES_WASTED_ALIGN] = aggkrd[klass][TOTAL_OBJECTS] * \
        (aggkrd[klass][INSTANCE_SIZE] - aggkrd[klass][PACKED_SIZE_ALIGN])

  all_klasses = {}
  all_klasses[TOTAL_OBJECTS]   = all_klasses[TOTAL_SIZE]   = \
  all_klasses[TOTAL_REFS]      = all_klasses[FIELD_REFS]   = \
  all_klasses[NONFILL_OBJECTS] = all_klasses[NONFILL_SIZE] = 0
  #all_klasses[BYTES_WASTED]  = all_klasses[BYTES_WASTED_ALIGN] = 0

  for klass in aggkrd.keys():
    all_klasses[TOTAL_OBJECTS]   += aggkrd[klass][TOTAL_OBJECTS]
    all_klasses[TOTAL_SIZE]      += aggkrd[klass][TOTAL_SIZE]
    all_klasses[TOTAL_REFS]      += aggkrd[klass][TOTAL_REFS]
    all_klasses[FIELD_REFS]      += aggkrd[klass][FIELD_REFS]

    if aggkrd[klass][KLASS_TYPE] != KT_VM_FILLER:
      all_klasses[NONFILL_OBJECTS] += aggkrd[klass][TOTAL_OBJECTS]
      all_klasses[NONFILL_SIZE]    += aggkrd[klass][TOTAL_SIZE]

#    if aggkrd[klass][INSTANCE_SIZE] > 0:
#      all_klasses[INS_OBJECTS]        += aggkrd[klass][TOTAL_OBJECTS]
#      all_klasses[INS_SIZE]           += aggkrd[klass][TOTAL_SIZE]
#      all_klasses[BYTES_WASTED]       += aggkrd[klass][BYTES_WASTED]
#      all_klasses[BYTES_WASTED_ALIGN] += aggkrd[klass][BYTES_WASTED_ALIGN]

  aggkrd[ALL_KLASSES] = all_klasses

  klass_keys = [ k for k in aggkrd.keys() if not k == ALL_KLASSES ]

  kt_totals = {}
  for kt in klass_types:
    kt_totals[kt] = {}
    kt_totals[kt][TOTAL_OBJECTS] = kt_totals[kt][TOTAL_SIZE] = \
    kt_totals[kt][TOTAL_REFS]    = kt_totals[kt][FIELD_REFS] = 0
    for key in klass_keys:
      if not aggkrd[key].has_key(KLASS_TYPE):
        print aggkrd[key].keys()
        return
      if aggkrd[key][KLASS_TYPE] == kt:
        kt_totals[kt][TOTAL_OBJECTS] += aggkrd[key][TOTAL_OBJECTS]
        kt_totals[kt][TOTAL_SIZE]    += aggkrd[key][TOTAL_SIZE]
        kt_totals[kt][TOTAL_REFS]    += aggkrd[key][TOTAL_REFS]
        kt_totals[kt][FIELD_REFS]    += aggkrd[key][FIELD_REFS]

  aggkrd[KT_TOTALS] = kt_totals

  return aggkrd

def cpSRResults(benches=storebenches, cfgs=[HDKM_ALLOC_BASE], iters=5):
  for bench in benches:
    for cfg in cfgs:
      for iter in range(iters):
        cmd = ("cp %s %s" % (rawSelfRefreshFile(bench, cfg, iter), rawoutfile(bench, cfg, iter)))  
        #print cmd
        execmd(cmd)

def mvresults(benches=dacapo_defs, cfgs=[OBJ_ORG_PROFILE], dstdir=backupdir, \
  overwrite=False):

#  if os.path.exists(dstdir):
#    if overwrite:
#      rmtree(dstdir)
#    else:
#      print "%s already exists!" % dstdir
#      print "set overwrite to True to overwrite this directory"
#      return

#  print "mkdir %s" % dstdir
#  mkdir_p(dstdir)
  
  srcdir = "/home/mrjantz/projects/memcolor/hs6/tmp/results"
  dstdir = "/home/mrjantz/projects/memcolor/hs6/results"
  for bench in benches:
    cmd = ("mv %s %s" % ( (srcdir + ("/%s/obj_info_xsim" % bench)), \
                          (dstdir + ("/%s/" % bench)) ))
    print cmd
    os.system(cmd)

#  for bench in benches:
#    if not dstdir.endswith('/'):
#      dstdir = dstdir + '/'
#    bdir = dstdir + ("%s/" % (bench))
#    for cfg in cfgs:
#      bcdir = bdir + ("%s/" % (cfg))
#      #mkdir_p(bcdir)
#      cmd = ("mv %s %s" % (expdir(bench,cfg), bcdir))
#      print cmd
#      #os.system(cmd)
#      try:
#        move(expdir(bench,cfg),bcdir)
#      except (IOError, OSError) as exc:
#        if exc.errno == errno.ENOENT:
#          print "No result for: %s-%s" % (bench, cfg)
#          continue
#        else:
#          raise
#      print "moved %s-%s" % (bench,cfg)

def copyresults(benches=dacapo_defs, cfgs=[OBJ_ORG_PROFILE], dstdir=backupdir, \
  overwrite=False):

  if os.path.exists(dstdir):
    if overwrite:
      rmtree(dstdir)

  print "mkdir %s" % dstdir
  mkdir_p(dstdir)
  
  for bench in benches:
    if not dstdir.endswith('/'):
      dstdir = dstdir + '/'
    bdir = dstdir + ("%s/" % (bench))
    for cfg in cfgs:
      bcdir = bdir + ("%s/" % (cfg))
      try:
        copytree(expdir(bench,cfg),bcdir)
      except (IOError, OSError) as exc:
        if exc.errno == errno.ENOENT:
          print "No result for: %s-%s" % (bench, cfg)
          continue
        else:
          raise
      print "copied %s-%s" % (bench,cfg)

def tar(benches=dacapo_defs, cfgs=[OBJ_INFO_XSIM], tardir=deftardir, \
        dst=deftarfile, rmfiles=False):

  copyresults(benches, cfgs, tardir, overwrite=True)
  #mvresults(benches, cfgs, tardir, overwrite=True)
  
  #for bench,cfg in expiter(benches, cfgs):
    #bcdir = tardir + ("%s/%s/" % (bench,cfg))

    #print "adding %s-%s" % (bench,cfg)
    #print "mkdir %s" % bcdir
    #mkdir_p(bcdir)
    
    #print "cp %s %s" % (rawoutfile(bench, cfg, 0), bcdir+rawout)
    #copyfile(rawoutfile(bench, cfg, 0), bcdir+rawout)

    #print "cp %s %s" % (colorObjInfoValReport(bench, cfg, 0), bcdir+coivrfile)
    #copyfile(colorObjInfoValReport(bench, cfg, 0), bcdir+coivrfile)

    #print "cp %s %s" % (activeSetValReport(bench, cfg, 0), bcdir+coivrfile)
    #copyfile(activeSetValReport(bench, cfg, 0), bcdir+asvrfile)

  parentdir = "/".join(tardir.split("/")[:-2])
  reltardir = tardir.split("/")[-2]

  olddir = os.getcwd()

  print "cd %s" % parentdir
  os.chdir(parentdir)

  print "tar xvzf %s %s" % (dst, reltardir)
  mytar = tarfile.open(dst, 'w:gz')
  mytar.add(reltardir)
  mytar.close()

  print "cd %s" % olddir
  os.chdir(olddir)

def getAllTCLs(benches=dacapo_defs, cfg=[XINT_CCF], iter=0):
  tcls = {}
  for bench in benches:
    if not tcls.has_key(bench):
      tcls[bench] = {}
    for cfg in cfgs:
      tcls[bench][cfg] = getTieredCompilationLevels(bench, cfg)
  return tcls

def getTieredCompilationLevels(bench, cfg=XINT_CCF, iter=0):

  tcls  = {}
  tcls[C1_COMPILE] = []
  tcls[C2_COMPILE] = []
  mcnts = getInvocationCounts(bench, cfg, iter)
  for method,icnt,bcnt in mcnts:

    if icnt >= 2000 or (icnt >= 600 and icnt + bcnt >= 15000) or bcnt >= 40000:
      tcls[C2_COMPILE].append(method)
    elif icnt >= 200 or (icnt >= 100 and icnt + bcnt >= 2000) or bcnt >= 60000:
      tcls[C1_COMPILE].append(method)
  return tcls

def writeCorFile(bench, cfg=XINT_CCF, quiet=True):

  corf = try_open_write(corFile(bench, cfg))
  if quiet:
    outf = corf
  else:
    outf = TeeFile(sys.stdout,corf)

  tcls = getTieredCompilationLevels(bench, cfg)
  for method in tcls[C2_COMPILE]:
    print >> outf, ("compilec2 %s" % method)
  for method in tcls[C1_COMPILE]:
    print >> outf, ("compilec1 %s" % method)

  return

