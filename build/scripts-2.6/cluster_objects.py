#!/usr/bin/python

import sys
from lib.globals import *
from lib.utils import *

import numpy as np
from optparse import OptionParser
from scipy.cluster.vq import *
import Pycluster as pc
from pickle import dump

usage = "usage: %prog [options] bench cfg iter"
def cluster_objects(args):

  parser = OptionParser()
  parser.add_option("-c", "--clusters", dest="clusters",
                    help="number of clusters to use", default="3")
  parser.add_option("-p", "--passes", dest="passes",
                    help="number of passes in the EM algorithm", default="20")

  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  bench = args[0]
  cfg   = args[1]
  iter  = int(args[2])

  nclusters = int(opts.clusters)
  npasses   = int(opts.passes)

  reff = try_open_read(refMapLog(bench, cfg, iter))

  print "reading refmap"
  vals = [ int(x) for x in reff.readline().split() ]
  objs = {}
  for line in reff:
    id = int(line.split()[0])
    #refs = [ 1 if int(x) > 0 else 0 for x in line.split()[1:] ]
    refs = [ int(x) for x in line.split()[1:] ]
    assert not objs.has_key(id), ("duplicate id: %d" % id)
    objs[id] = refs

  print "doing clusters"
  #input_data = np.array([ x[1] for x in sorted (objs.items()) ])
  input_data = np.array([ [ 1 if y > 0 else 0 for y in x[1] ] \
                            for x in sorted (objs.items()) ])
  labels,error,nfound = pc.kcluster(input_data,nclusters,npass=npasses)

  print "got my clusters"
  c2o = {}
  o2c = {}
  for label,item in zip(labels,sorted(objs.items())):
    if not c2o.has_key(label):
      c2o[label]        = {}
      c2o[label][ITEMS] = []
      c2o[label][COLOR] = cluster_colors[label]
    c2o[label][ITEMS].append(item)
    assert not o2c.has_key(item[0]), ("duplicate item: %d" % item[0])
    o2c[item[0]] = c2o[label][COLOR]

  print "clusters"
  for ckey in c2o.keys():
    print "".ljust(2), "color = %-9s" % ("\'%s\'" % c2o[ckey][COLOR]), \
                       "len = %-10d"  % (len(c2o[ckey][ITEMS]))

  c2of = try_open_write(clusterToObjectFile(bench,cfg,iter,nclusters))
  dump(c2o, c2of) 
  c2of.close()

  o2cf = try_open_write(objectToClusterFile(bench,cfg,iter,nclusters))
  dump(o2c, o2cf) 
  o2cf.close()

if __name__ == '__main__':
  cluster_objects(sys.argv)

