#!/usr/bin/python

import sys
from lib.utils import *
from optparse import OptionParser
from operator import itemgetter
from lib.report import TeeFile

from cPickle import load, dump
from math import ceil
from multiprocessing import Process,Pool,Pipe,Manager

import shelve

newCollectionRE = re.compile("(\W)*(\S)+ ObjectInfoCollection:(\W)+(\d+)(\W)+\( time:(\W)+(\d+)(\W)+duration:(\W)+(\d+)")
objrefRE        = re.compile("(\W)*(\d+)(\W)+(\d+)")
deadsHeaderRE   = re.compile("(\S)+ -- dumping deads: val = (\d+)")
deadsDoneRE     = re.compile("done dumping deads")
startTimeRE     = re.compile("start time =(\W)+(\d+)")

headerEndRE = re.compile("----------------------------------------")
klassRE     = re.compile("(\W)*(\d+):")

#VALNUM = "valnum"
TIME   = "time"
LIVES  = "live"
RESULT = "result"

REDS   = "reds"
BLUES  = "blues"
TOTALS = "totals"

def run(func, args):
  result = func(*args)
  return result

def runstar(args):
  return run(*args)

def print_report(vals, results_queue, outfile, quiet):

  cvrf = try_open_write(outfile)
  if quiet:
    outf = cvrf
  else:
    outf = TeeFile(sys.stdout,cvrf)

  print >> outf, "VAL".ljust(6),
  print >> outf, "REASON".ljust(13),
  print >> outf, "TIME".ljust(13),
  print >> outf, "DUR".ljust(10),
  print >> outf, "RED".ljust(34),
  print >> outf, "BLUE".ljust(34),
  print >> outf, "TOTAL".ljust(34),
  print >> outf, ""

  print >> outf, "".ljust(45),
  for i in range(3):
    print >> outf, "ref".ljust(10), "obj".ljust(10), 
    if i == 2:
      print >> outf, "size (KB)".ljust(12),
    else:
      print >> outf, "size".ljust(12),
  print >> outf, ""

  finished_results = {}
  for val,reason,time,dur in vals:
    while not finished_results.has_key(val):
      v,res = results_queue.get()
      #print "report got v=%d" % v, res
      assert not finished_results.has_key(v), "duplicate results in queue"
      finished_results[v] = res

    result = finished_results[val]
    print >> outf, ("%-5d " % val),
    print >> outf, ("%-12s " % reason),
    print >> outf, ("%-12d " % time),
    print >> outf, ("%-9d " % dur),

    for rkey in [REDS, BLUES]:
      for i in range(3):
        if result[TOTALS][i] == 0:
          assert result[rkey][i] == 0, "zero total with non-zero value"
          print >> outf, ("%2.4f" % float(0)).ljust(10),
        else:
          print >> outf, ("%2.4f" % \
            (float(result[rkey][i]) / float(result[TOTALS][i]))).ljust(10),
      print >> outf,"".ljust(1),
    for i in range(3):
      print >> outf, ("%d" % result[TOTALS][i]).ljust(10),
    print >> outf,"".ljust(1), ""
    outf.flush()
  cvrf.close()

def get_threshold_coldsets(bench, cfg, iter, val, threshold, delay, memory):

  objd   = shelve.open(shmObjDict(bench,cfg,iter))
  lives  = shelve.open(shmLives(bench, cfg, iter, val))

#  if val == 6:
#    tmpf = try_open_write("tmp.out")

  results = {}
#  if val == 6:
#    print "doing 6"
#    treds = []
#    tblues = []

  total_objs = red_objs = blue_objs = 0
  total_refs = red_refs = blue_refs = 0
  total_size = red_size = blue_size = 0

  past_lives = [ (v, shelve.open(shmLives(bench, cfg, iter, v))) \
                 for v in range(val-memory,val) if v > 0 ]

  for i,obj in enumerate(lives.keys()):
#    if val == 6 and (i % 10000) == 0:
#      print "i=%d %s" % (i, obj)
#      for pl in past_lives:
#        if pl.has_key(obj):
#          print "pl: %d" % pl[obj]

    # delay is the leeway past creation time
    red = True
    cval = objd[obj][1][0]
    if (val - (cval+delay)) <= 0:
      pass
    else:
      my_past_lives = [ pl[1] for pl in past_lives if pl[0] > cval ]

      try:
        refs = sum ( [ life[obj] for life in my_past_lives ] )
      except:
        print "past life key error with obj=%s (val=%d)" % (obj,val)
      if refs < threshold:
        red = False

    osize = objd[obj][0]
    vrefs = lives[obj]

    if red:
#      if val == 6:
#        treds.append((obj,vrefs,osize))
      red_objs += 1
      red_refs += vrefs
      red_size += osize
    else:
#      if val == 6:
#        tblues.append((obj,vrefs,osize))
      blue_objs += 1
      blue_refs += vrefs
      blue_size += osize

    total_objs += 1
    total_refs += vrefs
    total_size += osize

#  if val == 6:
#    print "done with 6 -- sorting"
#    sys.stdout.flush()
#
#    trsort = sorted(treds, key=itemgetter(2), reverse=True)
#    print >> tmpf, "reds: val=%d" % val
#    for o,r,s in trsort:
#      print >> tmpf, "".ljust(4), "%-10s %-10d %-10d" % (o,r,s)
#
#    tbsort = sorted(tblues, key=itemgetter(2), reverse=True)
#    print >> tmpf, "blues: val=%d" % val
#    for o,r,s in tbsort:
#      print >> tmpf, "".ljust(4), "%-10s %-10d %-10d" % (o,r,s)

  results[TOTALS] = ( total_refs, total_objs, (total_size>>10) )
  results[REDS]   = ( red_refs,   red_objs,   (red_size>>10)   )
  results[BLUES]  = ( blue_refs,  blue_objs,  (blue_size>>10)  )

  for pl in [ x[1] for x in past_lives]:
    pl.close()

  objd.close()
  lives.close()

  return results

def get_size_coldsets(bench, cfg, iter, val, threshold, size, delay, memory):

  objd   = shelve.open(shmObjDict(bench,cfg,iter))
  lives  = shelve.open(shmLives(bench, cfg, iter, val))

  maxbytes = ((size << 10) << 10)

  results = {}
  print "get_size_coldsets: val=%d, maxbytes=%d" % (val, maxbytes)

  total_objs = red_objs = blue_objs = 0
  total_refs = red_refs = blue_refs = 0
  total_size = red_size = blue_size = 0

  past_lives = [ shelve.open(shmLives(bench, cfg, iter, v)) \
                 for v in range(val-memory,val) if v > 0 ]

  tmpreds = []
  for i,obj in enumerate(lives.keys()):

    red = True
    past_refs = sys.maxint

    alive_vals = [ True if life.has_key(obj) else False for life in past_lives ]
    if all( alive_vals ) and len(alive_vals) == memory:
      past_refs = sum ( [ life[obj] for life in past_lives ] )
      if past_refs < threshold:
        red = False

    osize = objd[obj][0]
    vrefs = lives[obj]

    if red:
      tmpreds.append(((obj,vrefs,osize),past_refs))
    else:
      blue_objs += 1
      blue_refs += vrefs
      blue_size += osize

    total_objs += 1
    total_refs += vrefs
    total_size += osize

  print "doing sorted reds: val=%d" % val
  sorted_reds = [ x[0] for x in sorted ( tmpreds, \
                  key=itemgetter(1), reverse=True ) ]

  i = 0
  for obj,vrefs,osize in sorted_reds:
    if not (red_size + osize) < maxbytes:
      break
    red_objs += 1
    red_refs += vrefs
    red_size += osize
    i += 1

  rest = sorted_reds[i:]
  for i,(obj,vrefs,osize) in enumerate(rest):
    blue_objs += 1
    blue_refs += vrefs
    blue_size += osize

  results[TOTALS] = ( total_refs, total_objs, (total_size>>10) )
  results[REDS]   = ( red_refs,   red_objs,   (red_size>>10)   )
  results[BLUES]  = ( blue_refs,  blue_objs,  (blue_size>>10)  )

  for pl in past_lives:
    pl.close()

  objd.close()
  lives.close()

  return results

def get_ratio_coldsets(bench, cfg, iter, val, threshold, ratio, delay, memory):

  objd   = shelve.open(shmObjDict(bench,cfg,iter))
  lives  = shelve.open(shmLives(bench, cfg, iter, val))

  results = {}
  print "get_ratio_coldsets: val=%d" % val

  total_objs = red_objs = blue_objs = 0
  total_refs = red_refs = blue_refs = 0
  total_size = red_size = blue_size = 0

  past_lives = [ shelve.open(shmLives(bench, cfg, iter, v)) \
                 for v in range(val-memory,val) if v > 0 ]

  nreds = int(ratio * float(len(lives)))
  tmpreds = []
  for i,obj in enumerate(lives.keys()):

    red = True
    past_refs = sys.maxint

    alive_vals = [ True if life.has_key(obj) else False for life in past_lives ]
    if all( alive_vals ) and len(alive_vals) == memory:
      past_refs = sum ( [ life[obj] for life in past_lives ] )
      if past_refs < threshold:
        red = False

    osize = objd[obj][0]
    vrefs = lives[obj]

    if red:
      tmpreds.append(((obj,vrefs,osize),past_refs))
    else:
      blue_objs += 1
      blue_refs += vrefs
      blue_size += osize

    total_objs += 1
    total_refs += vrefs
    total_size += osize

  print "doing sorted reds: nreds=%d val=%d" % (nreds,val)
  sorted_reds = [ x[0] for x in sorted ( tmpreds, \
                  key=itemgetter(1), reverse=True ) ]

  reds = sorted_reds[:nreds]
  for i,(obj,vrefs,osize) in enumerate(reds):
    red_objs += 1
    red_refs += vrefs
    red_size += osize

  rest = sorted_reds[nreds:]
  for i,(obj,vrefs,osize) in enumerate(rest):
    blue_objs += 1
    blue_refs += vrefs
    blue_size += osize

  results[TOTALS] = ( total_refs, total_objs, (total_size>>10) )
  results[REDS]   = ( red_refs,   red_objs,   (red_size>>10)   )
  results[BLUES]  = ( blue_refs,  blue_objs,  (blue_size>>10)  )

  for pl in past_lives:
    pl.close()

  objd.close()
  lives.close()

  return results

def do_coldsets(bench, cfg, iter, val, threshold, ratio, size, delay, \
                memory, queue):

  print "do_coldsets: %s %s %d (val=%d)" % (bench,cfg,iter,val)
  #objd = load(try_open_read(objDictPkl(bench,cfg,iter), bin=True))
  #lives = shelve.open(shmLives(val))
  #dyns   = {}

  results = {}
  if not ratio is None:
    results = get_ratio_coldsets(bench, cfg, iter, val, threshold, ratio, delay, memory)
  elif not size is None:
    results = get_size_coldsets(bench, cfg, iter, val, threshold, size, delay, memory)
  else:
    results = get_threshold_coldsets(bench, cfg, iter, val, threshold, delay, memory)

  print ("putting v=%d: " % val),
#  print result
  queue.put((val, results))
  return 0

#  if len(vals) < hist_val:
#    for obj in lives.keys():
#      dyns[obj] = RED
#    return dyns
#  else:


def get_lives(bench, cfg, iter, val):

  print "get_lives: %s %s %d (val=%d)" % (bench,cfg,iter,val)

  livesfile = shmLives(bench, cfg, iter, val)
  rawfile   = rawoutfile(bench,cfg,iter)
  if os.path.exists(livesfile):
    return

  objd  = shelve.open(shmObjDict(bench, cfg, iter))
  lives = shelve.open(livesfile)

  print "got objd %d (val=%d)" % (len(objd.keys()),val)
  for i,id in enumerate(objd.keys()):
    cval,dval =  objd[id][1]
    if val >= cval and (dval < 0 or val <= dval):
      lives[str(id)] = 0
    if (i % 1000000) == 0:
      print "i=%-10d val=%d" % (i,val)

  print "initial lives (val=%d)" % val
  valRE = re.compile(("(\W)*(\S)+ ObjectInfoCollection:(\W)+%d" % val))
  infof = try_open_read(objInfoLog(bench, cfg, iter))
  for line in infof:
    if valRE.match(line):
      infof.next()
      for oline in infof:
        if not objrefRE.match(oline):
          break
        xline = oline.lstrip(' *')
        objid,refs = [ x for x in xline.split() ]
        lives[objid] = long(refs)

  print "closing (val=%d)" % val
  objd.close()

  print "saving lives to %s" % livesfile
  lives.close()

  print "done adding lives (val=%d)" % val
  infof.close()
  return 0

def get_time(line):
  return long(line.split()[-4])

def get_reason(line):
  return line.split()[0]

def get_val(line):
  return long(line.split()[2])

def get_duration(line):
  return long(line.split()[-2])

def get_rat(num,den):
  if den == 0:
    assert num == 0, "zero total with non-zero value"
    return float(0)
  else:
    return (float(num) / float(den))

usage = "usage: %prog BENCH CFG ITER [OPTIONS]"
def dynamic_coldsets(args):

  parser = OptionParser()
  parser.add_option("-t", "--cold_threshold", dest="cold_threshold",
                    help="maximum number of refs for cold objects",
                    default="0")
  parser.add_option("-m", "--val_memory", dest="val_memory",
                    help="number of intervals to remember when calculating"
                    "cold sets", default="1")
  parser.add_option("-d", "--val_delay", dest="val_delay",
                    help="number of intervals to delay before calculating hot"
                    "and cold sets", default="1")
  parser.add_option("-r", "--hot_ratio", dest="hot_ratio",
                    help="maximum ratio of red objects", default=None)
  parser.add_option("-z", "--hot_size", dest="hot_size",
                    help="maximum size of red objects (in MB)", default=None)
  parser.add_option("-q", "--quiet", dest="quiet", action="store_true",
                    help="suppress printing report to stdout", default=False)
  parser.add_option("-l", "--clean_lives", dest="clean_lives", \
                    action="store_true", help="force reconstruction of lives", \
                    default=False)
  parser.add_option("-c", "--clean_objd", dest="clean_objd", \
                    action="store_true", help="force reconstruction of objd", \
                    default=False)
  parser.add_option("-x", "--max_procs", dest="max_procs",
                    help="max number of processes", default="50")
#  parser.add_option("-d", "--dumpreds", dest="dumpreds",
#                    help="dump red objects to file every interval",
#                    default=None)
#  parser.add_option("-v", "--red_verbose", dest="red_verbose",
#                    action="store_true", help="print red objects to dumpreds"
#                    "file", default=False)

  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  bench        = args[0]
  cfg          = args[1]
  iter         = int(args[2])
  threshold    = int(opts.cold_threshold)
  val_memory   = int(opts.val_memory)
  val_delay    = int(opts.val_delay)
  hot_ratio    = float(opts.hot_ratio) if opts.hot_ratio else None
  hot_size     = int(opts.hot_size) if opts.hot_size else None
  quiet        = opts.quiet
  clean_lives  = opts.clean_lives
  clean_objd   = opts.clean_objd
  max_procs    = int(opts.max_procs)
#  ratio        = float(opts.red_ratio)
#  red_verbose  = opts.red_verbose
#  dumpredsf    = try_open_write(opts.dumpreds) if opts.dumpreds else None

  # create the objd if the pkl file does not exist or the rawfile is newer
  # than the pickled objd
  #
  shmobjd = shmObjDict(bench,cfg,iter)
  rawfile = rawoutfile(bench,cfg,iter)

  if os.path.exists(shmobjd):
    if clean_objd or ( os.path.getmtime(shmobjd) < os.path.getmtime(rawfile) ):
      rm_f(shmobjd)

  if not os.path.exists(shmobjd):

    print "getting deads info"
    deadsf = try_open_read(deadObjLog(bench, cfg, iter))

    deads = {}
    cur_val = 0
    for line in deadsf:
      if deadsHeaderRE.match(line):
        cur_val = int(line.split()[-1])
      elif deadsDoneRE.match(line):
        cur_val = -1
      else:
        deads[int(line.strip())] = cur_val
    deadsf.close()

    print "getting object info"
    allocf = try_open_read(objAllocLog(bench, cfg, iter))

    for line in allocf:
      if headerEndRE.match(line):
        break

    mkdir_p (shmdir(bench,cfg,iter))
    objd = shelve.open(shmobjd)
    for i,line in enumerate(allocf):
      if objrefRE.match(line):
        id,cval,size = [ int(x) for x in line.split()[:3] ]
        dval = deads[id] if deads.has_key(id) else -1
        #objd[id] = (klass, size, (cval, dval))
        #objd[str(id)] = ((size >> 10), (cval, dval))
        objd[str(id)] = (size, (cval, dval))
        #objd[id] = (size, (cval, dval))
      elif klassRE.match(line):
        klass = line.split()[1]
      if i%100000 == 0:
        print i
    allocf.close()
    objd.close()

  print "verifying objinfo ...",
  infof = try_open_read(objInfoLog(bench, cfg, iter))
  first_line = infof.readline()
  if not startTimeRE.match(first_line):
    print "%s is not an objinfo file" % infofile
    raise SystemExit(1)

  print " ok\ngetting vals and times"
  start_time = long(first_line.split()[-1])
  vals = []
  infof.seek(0,0)
  for line in infof:
    if newCollectionRE.match(line):
      vals.append((get_val(line), get_reason(line), (get_time(line)-start_time), get_duration(line)))
  infof.close()

  lives_dir = shmLivesDir(bench,cfg,iter)
  if os.path.exists(lives_dir):
    if clean_lives or ( os.path.getmtime(lives_dir) < os.path.getmtime(rawfile) ):
      rm_rf(lives_dir)
  mkdir_p(lives_dir)

  mqueue  = Manager().Queue()
  outfile = coldsetValReport(bench, cfg, iter, thresh=threshold, \
                             delay=val_delay, mem=val_memory, \
                             ratio=hot_ratio, size=hot_size)
  report_thread = Process(target=print_report, \
                          args=(vals, mqueue, outfile, quiet))
  print "starting reporter thread"
  report_thread.start()

  # start threads to get live info
  lives_nprocs  = min(len(vals),int(float(max_procs) * 0.8))
  lives_pool    = Pool(processes=lives_nprocs)
  lives_results = {}
  print "opening lives pool (%d processes)" % lives_nprocs

  tasks = [ (get_lives, (bench,cfg,iter,val[0])) for val in vals ]
  for ((val,reason,time,dur),task) in zip(vals, tasks):
    lives_results[val] = lives_pool.apply_async(run, task)
  lives_pool.close()
 
  # start coldset threads -- but wait if live info is not ready
  coldset_nprocs = min(len(vals),(max_procs - lives_nprocs))
  coldset_pool   = Pool(processes=coldset_nprocs)
  print "opening coldset pool (%d processes)" % coldset_nprocs

  for val,reason,time,dur in vals:
    reqd_vals = [ x for x in range(val-val_memory,val) if x > 0 ] + [val]
    for reqv in reqd_vals:
      if not lives_results[reqv].ready():
        lives_results[reqv].wait()
        assert lives_results[reqv].successful(), \
               "get_lives error: val=%d" % reqv

    task = (do_coldsets, (bench, cfg, iter, val, threshold, hot_ratio, \
                          hot_size, val_delay, val_memory, mqueue) )
    coldset_pool.apply_async(run, task)

  coldset_pool.close()

  lives_pool.join()
  coldset_pool.join()
  report_thread.join()
  print "all done"

#  vals_lives = [ r.get() for r in results ]
#  print "got lives"
#
#  vals = []
#  for ((val,time),lives) in zip(vts,vals_lives):
#    valinfo = {}
#    valinfo[VALNUM] = val
#    valinfo[TIME]   = time
#    valinfo[LIVES]  = lives
#    vals.append(valinfo)
#
#  for val in vals:
#    print "VAL: %-8d, TIME: %d, LIVES: %d" % \
#          (val[VALNUM], val[TIME], len(val[LIVES]))

#  print "getting start time"
#  infof = try_open_read(objInfoLog(bench, cfg, iter))
#  first_line = infof.readline()
#  if not newCollectionRE.match(first_line):
#    print "%s is not an objinfo file" % infofile
#    raise SystemExit(1)
#
#  start_time = get_time(first_line)
#  cur_time_val = 0
#
#  vals = []
#  color_sets = [RED, BLUE]
#
#  print "building valsets"
#  #valsets    = {}
#  next_color = {}
#  infof.seek(0,0)
#  vals = []
#  for line in infof:
#    if newCollectionRE.match(line):
#      vals.append(get_val(line))
#      
#  for val in vals:
#
#  for line in infof:
#    if newCollectionRE.match(line):
#      cur_val = get_val(line)
#      cur_time_val = (get_time(line) - start_time)
#      print cur_val, cur_time_val
#      valinfo = {}
#      valinfo[VALNUM] = cur_val
#      valinfo[TIME]   = cur_time_val
#      valinfo[LIVES]  = getLiveObjects(bench, cfg, iter, cur_val)
#      valsets[cur_time_val] = {}
#      for color in color_sets:
#        valsets[cur_time_val][color]  = []
#
#      live_objs = []
#      for vline in infof:
#        xline = vline.lstrip(' *')
#        if objrefRE.match(xline):
#          objid,refs = [ long(x) for x in xline.split() ]
#          size  = objd[objid][1][2]
#          color = objd[objid][0]
#          valsets[cur_time_val][color].append((objid,refs,size))
#          live_objs.append((objid,refs,size))
#
#        else:
#          break
#
#      for objid in next_color.keys():
#        next_color[objid] = RED
#
#      nobjs = len(live_objs)
#      nrefs = sum ( [ x[1] for x in live_objs ] )
#      tsize = sum ( [ x[2] for x in live_objs ] )
#
##      nreds = ceil(float(nobjs) * ratio)
#      if opts.dumpreds:
#        redsd = {}
#
#      redobjs = redrefs = redsize = 0
#      for objid,refs,size in sorted(live_objs,key=itemgetter(1),reverse=True):
##        if nreds == 0:
##          break
# 
#        if refs <= thresh:
#          next_color[objid] = BLUE
##          nreds -= 1
#          if opts.dumpreds:
#            klass = allocd[objid][0]
#            if not redsd.has_key(klass):
#              redsd[klass] = []
#            redsd[klass].append((objid, refs, size))
#
#      if opts.dumpreds:
#        print >> dumpredsf, "val: %-8d (%d)" % (cur_val, cur_time_val)
#        klasses = [ x[0] for x in \
#                    sorted ( [ (klass, sum ( [ x[1] for x in redsd[klass] ] )) \
#                               for klass in redsd.keys() ],
#                             key=itemgetter(1),
#                             reverse=True) ]
#        for klass in klasses:
#
#          kobjs = len(redsd[klass])
#          krefs = sum ([ x[1] for x in redsd[klass] ])
#          ksize = sum ([ x[2] for x in redsd[klass] ])
#
#          if red_verbose:
#            print >> dumpredsf, "".ljust(2), ("%-50s" % klass), \
#                                ("objs = %-16d" % kobjs), \
#                                ("refs = %-16d" % krefs), \
#                                ("size = %-16d" % ksize)
#
#          redobjs += kobjs
#          redrefs += krefs
#          redsize += ksize
#
#          if red_verbose:
#            for objid, refs, size in redsd[klass]:
#              print >> dumpredsf, "".ljust(4), "%-13d" % objid,
#              print >> dumpredsf, "%-16d" % refs,
#              print >> dumpredsf, "%-16d" % size
#
#        print >> dumpredsf, "totals: objs = %2.4f %-30s" % \
#                 (get_rat(redobjs,nobjs), "(%d / %d)" % (redobjs, nobjs)),
#        print >> dumpredsf, "refs = %2.4f %-30s" % \
#                 (get_rat(redrefs,nrefs), "(%d / %d)" % (redrefs, nrefs)),
#        print >> dumpredsf, "size = %2.4f %-30s" % \
#                 (get_rat(redsize,tsize), "(%d / %d)" % (redsize, tsize))
#        dumpredsf.flush()
#
#  if opts.dumpreds:
#    dumpredsf.close()
#
#  infof.close()
#  print "finished building valsets -- dumping pkl"
#  #dump(valsets, try_open_write(valSetPkl(bench, cfg, iter, thresh, ratio)))
#  dump(valsets, try_open_write(coldSetPkl(bench, cfg, iter, thresh)))

if __name__ == '__main__':
  dynamic_coldsets(sys.argv)

