#!/usr/bin/python

import sys
from lib.utils import *

def edproc(args):

  bench = args[1]
  cfg   = args[2]
  iter  = int(args[3])

  mydir = rawDataDir(bench, cfg, iter)

  olddir = os.getcwd()

  try:
    os.chdir(mydir)
  except OSError:
    return -1

  # I really did not want to mess with changing these scripts - so I'm copying
  # them over for now
  #
  try:
    cmd = "cp %s %s %s" % (processmaster, edprbmaster, mydir)
    p = execmd(cmd)
  except:
    print "%s command exited with return code: %d" % (cmd, p.returncode)
    return p.returncode

  try:
    cmd = "sh process.sh"
    p = execmd(cmd, outfile=(mydir+edpout))
  except:
    print "%s command exited with return code: %d" % (cmd, p.returncode)
    return p.returncode

  os.chdir(olddir)
  return 0

if __name__ == '__main__':
  edproc(sys.argv)
