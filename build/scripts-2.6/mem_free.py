#!/usr/bin/python

import sys
import re
from optparse import OptionParser

usage = "usage: %prog [options]"
def mem_free(args):
  parser = OptionParser()
  parser.add_option("-n", "--node", dest="node",
                    help="node", default="0")
  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  meminfo_file = "/sys/devices/system/node/node%s/meminfo" % opts.node
  try:
    meminfof = open(meminfo_file)
  except:
    print "invalid meminfo_file: %s" % meminfo_file
    raise SystemExit(1)

  val = ""
  for line in meminfof.readlines():
    if re.match("Node \d+ MemFree:", line):
      val = line.split()[3]
      break

  print val
  return

if __name__ == '__main__':
  mem_free(sys.argv)
