#!/usr/bin/python

import sys
from lib.utils import *
from optparse import OptionParser
from operator import itemgetter

# MRJ -- this is obsolete -- I came up with a better way to log objects
# and klasses from HotSpot
#
usage = "usage: %prog OBJALLOC [OPTIONS]"
def sort_objalloc(args):

  parser = OptionParser()
  parser.add_option("-k", "--keep", dest="keep", action="store_true",
                    help="keep original objalloc file", default=False)

  try:
    (opts, args) = parser.parse_args()
  except:
    parser.print_usage()

  infile = args[0]
  try:
    inf = open(infile)
  except:
    print >> sys.stderr, "could not open objalloc file: %s" % infile
    raise SystemExit(1)

  outfile = "/".join( infile.split('/')[:-1] + \
                      [ infile.split('/')[-1] + ".shrink" ] )
  try:
    outf = open(outfile, 'w')
  except:
    print >> sys.stderr, "could not open shrink file: %s" % outfile
    raise SystemExit(1)

  # store the input file in a dictionary
  obj_dict = {}
  for line in inf:
    (id, addr, size, klass) = line.split()

    if not obj_dict.has_key(klass):
      obj_dict[klass] = []
    obj_dict[klass].append((int(id), addr, int(size)))
  inf.close()

  # sort objects in the klass by size
  for klass in obj_dict.keys():
    obj_dict[klass] = sorted( obj_dict[klass], key=itemgetter(2), reverse=True )

  # sort klasses by the number of instances
  lens = [ (k, len(obj_dict[k])) for k in obj_dict.keys() ]
  sorted_klasses = [ x[0] for x in sorted (lens, key=itemgetter(1), reverse=True) ]

  # dump to the output file
  tsize = tinst = 0
  for i,klass in enumerate(sorted_klasses):
    print >> outf, "  %d: %s" % (i, klass)

    ksize = 0
    kinst = len(obj_dict[klass])
    for (id, addr, size) in obj_dict[klass]:
      print >> outf, "  %13d: %s %13d" % (id, addr, size)
      ksize += int(size)

    print >> outf, "          total:", " ".ljust(20), "%13d (%d)" % (ksize, kinst)
    print >> outf, ""

    tsize += ksize
    tinst += kinst

  print >> outf,   "    All classes:", " ".ljust(20), "%13d (%d)" % (tsize, tinst)
  outf.close()

  if not opts.keep:
    os.system("rm -f %s" % infile)
    os.system("mv %s %s" % (outfile, infile))
  else:
    print "shrink is in %s" % outfile

if __name__ == '__main__':
  sort_objalloc(sys.argv)

