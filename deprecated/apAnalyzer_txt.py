#! /usr/bin/env python

import os
import sys
import re
import regex
import numpy as np
from scipy import stats as scistats
import pprint
from tabulate import tabulate
import traceback
import copy
import xlsxwriter


# sys.path.append('/home/tzhou/projects/mm/pymcolor/')
# os.environ["PYTHONPATH"] = '/home/tzhou/projects/mm/pymcolor/'
# pythonpath = os.environ.get("PYTHONPATH").split(':')

from lib.globals import *
from lib.utils import *
from lib.exp import *
from lib.report import *
print "loaded pymcolor globals, utils, exp, and report"

import json

pp = pprint.PrettyPrinter(indent=4)
dis_table = []
ct_dis_table = []
failed = []

# def ct_ri_analyze(benches):
#     global failed, dis_table
#     thresholds = [0.8, 0.9, 0.95]
#     for threshold in thresholds:
#         print 'threshold:', threshold
#         for bench in benches:
#             print 'do', benches
#             try:
#                 ri_analyze(bench, threshold)
#             except:
#                 failed.append(b)
#                 traceback.print_exc()
#         add_mean_row('geometric')
#         print_table(dis_table, ['unused', 'read-only', '80% read', '80% read sum', '90% read', '90% read sum', '95% read', '95% read sum'])
#         dis_table = []

def ct_ri_analyze(bench, thresholds=[0.8, 0.9, 0.95]):
    global dis_table
    print bench
    ap = allocPointInfo(bench,AP_TLAB_INTERVAL,getssi=False, getvki=False, clean=True)

    thre_num = len(thresholds)
    unused_ap_num = 0.0
    ri_ap_num = [0.0 for i in range(0, thre_num)]
    ro_ap_num = 0.0
    all_ap_num = 0.0
    
    unused_size = 0.0
    ri_size = [0.0 for i in range(0, thre_num)]
    ro_size = 0.0
    all_size = 0.0

    print 'apnum:', len(ap[FULL][APD])

    for apid in ap[FULL][APD]:
        if apid == 0:
            continue
        objs = ap[FULL][APD][apid]['objs']
        size = ap[FULL][APD][apid]['size']
        reads = ap[FULL][APD][apid]['reads']
        writes = ap[FULL][APD][apid]['writes']
        refs = ap[FULL][APD][apid]['refs']
        all_ap_num += 1
        all_size += size
        
        #print all_size, size

        if refs == 0:
            #print 'unused', apid
            unused_ap_num += 1
            unused_size += size
        elif reads == refs:
            ro_ap_num += 1
            ro_size += size
        else:
            pass
        
        for i, threshold in enumerate(thresholds):
            if int(refs)!=0 and reads/float(refs) > threshold and reads/float(refs) < 1:
                ri_ap_num[i] += 1
                ri_size[i] += size


    # unused_rate = unused_ap_num/all_ap_num
    # ri_ap_rate = ri_ap_num/all_ap_num
    # ro_ap_rate = ro_ap_num/all_ap_num

    unused_rate = unused_size/all_size
    ro_ap_rate = ro_size/all_size
    ri_ap_rate = []
    for i in range(0, thre_num):
        ri_ap_rate.append(ri_size[i]/all_size)
    
    #row = ['%s (%.2f)'%(bench,threshold), unused_rate, ri_ap_rate, ro_ap_rate, unused_rate+ro_ap_rate+ri_ap_rate]
    row = [bench, unused_rate, ro_ap_rate]
    for i in range(0, thre_num):
        row.append(ri_ap_rate[i])
        row.append(ri_ap_rate[i]+unused_rate+ro_ap_rate)
    dis_table.append(row)
    #print 'table', dis_table

        
        
def ri_analyze(bench, threshold=0.9):
    global dis_table
    print bench
    ap = allocPointInfo(bench,AP_TLAB_INTERVAL,getssi=False, getvki=False, clean=True)
    ri_ap_num = 0.0
    ro_ap_num = 0.0
    all_ap_num = 0.0
    unused_ap_num = 0.0

    ri_size = 0.0
    ro_size = 0.0
    all_size = 0.0
    unused_size = 0.0
    for apid in ap[FULL][APD]:
        objs = ap[FULL][APD][apid]['objs']
        size = ap[FULL][APD][apid]['size']
        reads = ap[FULL][APD][apid]['reads']
        writes = ap[FULL][APD][apid]['writes']
        refs = ap[FULL][APD][apid]['refs']

        all_ap_num += 1
        all_size += size
        if refs == 0:
            unused_ap_num += 1
            unused_size += size
        elif reads == refs:
            ro_ap_num += 1
            ro_size += size
        elif reads/float(refs) > threshold:
            ri_ap_num += 1
            ri_size += size
        else:
            pass

    # unused_rate = unused_ap_num/all_ap_num
    # ri_ap_rate = ri_ap_num/all_ap_num
    # ro_ap_rate = ro_ap_num/all_ap_num
    unused_rate = unused_size/all_size
    ri_ap_rate = ri_size/all_size
    ro_ap_rate = ro_size/all_size
    row = ['%s (%.2f)'%(bench,threshold), unused_rate, ri_ap_rate, ro_ap_rate, unused_rate+ro_ap_rate+ri_ap_rate]
    dis_table.append(row)


def per_byte_analyze(bench):
    global dis_table
    print bench
    ap = allocPointInfo(bench,AP_TLAB_INTERVAL,getssi=False, getvki=False, clean=True)
    ri_ap_num = 0.0
    ro_ap_num = 0.0
    all_ap_num = 0.0
    unused_ap_num = 0.0

    ri_size = 0.0
    ro_size = 0.0
    all_size = 0.0
    unused_size = 0.0
    for apid in ap[FULL][APD]:
        objs = ap[FULL][APD][apid]['objs']
        size = ap[FULL][APD][apid]['size']
        reads = ap[FULL][APD][apid]['reads']
        writes = ap[FULL][APD][apid]['writes']
        refs = ap[FULL][APD][apid]['refs']
        all_size += size
        write_per_byte = float(writes)/float(size)
        if write_per_byte < 0.01:
            ri_size += size
    dis_table.append([bench, ri_size/all_size]) 
    
def add_mean_row(mean="geometric"):
    global dis_table
    column_num = len(dis_table[0])
    row = ['geomean']
    for col in range(1, column_num):
        colx = map(lambda x: x[col], dis_table)
        row.append(geomean(colx))
    # unusedx = map(lambda x: x[1], dis_table)
    # rix = map(lambda x: x[2], dis_table)
    # rox = map(lambda x: x[3], dis_table)
    # sumx = map(lambda x: x[4], dis_table)

    
    # if mean == 'geometric':
    #     row.append(geomean(unusedx))
    #     row.append(geomean(rix))
    #     row.append(geomean(rox))
    #     row.append(geomean(sumx))
    dis_table.append(row)

    
def print_table(table, headers, fmt="grid", fp=None):
    formatted_table = []
    for row in table:
        newrow = []
        for item in row:
            if isinstance(item, float):
                newrow.append('{:.2%}'.format(item))
            else:
                newrow.append(item)
        formatted_table.append(newrow)
    print tabulate(formatted_table, headers, tablefmt=fmt)
    if fp is not None:
        print >> fp, tabulate(formatted_table, headers, tablefmt=fmt)
    
def get_apinfo(bench):
    ap = allocPointInfo(bench,AP_TLAB_INTERVAL,getssi=False, getvki=False, clean=True)

    #pp.pprint(ap['AP_MAP'])
    #print ap['VALS'][0].keys()
    #pp.pprint(ap['VALS'][0]['APD'][2019])

    
def reform_to_stacked_bar(table, size, fp=None):
    table_cp = copy.deepcopy(table)
    # remove sum colunm and bench name colunm
    for row in table_cp:
        del row[4]
        del row[5]
        del row[6]
        del row[0]
    # use first item in a row as index

    visual_table = []
    for row in table_cp:
        read_95 = row[4]
        read_90_delta = row[3] - row[4]
        read_80_delta = row[2] - row[3]
        vi_row = [row[0], row[1], read_95, read_90_delta, read_80_delta]
        visual_table.append(vi_row)
    zipped = zip(*visual_table)
    
    newtable = [
        {'name': '80% read', 'data': zipped[4], 'stack': size, 'color': 'rgba(149, 206, 255, 0.5)'},
        {'name': '90% read', 'data': zipped[3], 'stack': size, 'color': 'rgba(149, 206, 255, 0.75)'},
        {'name': '95% read', 'data': zipped[2], 'stack': size, 'color': 'rgba(149, 206, 255, 1.0)'},
        {'name': 'read-only', 'data': zipped[1], 'stack': size, 'color': 'rgb(169, 255, 150)'},
        {'name': 'unused', 'data': zipped[0], 'stack': size, 'color': 'rgb(92, 92, 97)'},
    ]
        
    json.dump(newtable, fp)

    
if __name__ == '__main__':
    #benches = ["lusearch-small", "pmd-small", "xalan-small", "luindex-small", "tradebeans-small", "fop-small", "sunflow-small", "eclipse-small", "jython-small", "batik-small", "h2-small", "avrora-small", "tomcat-small", "tradesoap-small"]
    #benches = ["lusearch-default", "pmd-default", "xalan-default", "luindex-default", "tradebeans-default", "fop-default", "sunflow-default", "eclipse-default", "jython-default", "avrora-default", "batik-default", "h2-default", "tomcat-default", "tradesoap-default"]
    
    bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "avrora", "batik", "h2", "tomcat", "tradesoap"]
    bench_prefix = ["lusearch", "pmd"]
    bench_size = 'small'
    #bench_size = 'default'

    if len(sys.argv) > 1:
        if sys.argv[1] == '-small':
            bench_size = 'small'
        elif sys.argv[1] == '-default':
            bench_size = 'default'

        if len(sys.argv) > 2:            
            if sys.argv[2] == 'all':
                bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "avrora", "batik", "h2", "tomcat", "tradesoap"]
            else:
                bench_prefix = []
                for i in range(2, len(sys.argv)):
                    bench_prefix.append(sys.argv[i])

    
    benches = [b+'-'+bench_size for b in bench_prefix]
    
    
    for b in benches:
        try:
            ct_ri_analyze(b)
        except:
            traceback.print_exc()
            failed.append(b)

    add_mean_row('geometric')
    f1 = open('apResults/table_'+bench_size+'.txt', 'w')
    print_table(dis_table, ['unused', 'read-only', '80% read', '80% read sum', '90% read', '90% read sum', '95% read', '95% read sum'], fp=f1)
    f2 = open('apResults/json_'+bench_size+'.txt', 'w')
    reform_to_stacked_bar(dis_table, bench_size, f2)

    f1.close()
    f2.close()
    print 'failed:', failed

    # add_mean_row('geometric')
    # print_table(dis_table, ['unused', 'read mostly', 'read only', 'sum'])
    # # calc_benches('default')
    # # draw_benches()
    
    # tradebeans, eclipse, h2 took up too much memory and slowed down the computer
    #draw_benches()
    #malaise('pmd-small')
    
    # ma = regex.match(r'((?P<num>\d)+(.+))', '123abc')
    # #ma = re.match(r'(?(?=\d)\d+|\w+)', '123abc')
    # print ma.group('num')
    # print ma.capturesdict()

    # malaise('pmd-small')
    #cold_analyze('pmd-small')
    #ri_analyze('xalan-small')
    #get_apinfo('pmd-small')
    #get_apinfo('pmd-small')
