#! /usr/bin/env python

import os
import sys
import re
import regex
import numpy as np
from scipy import stats as scistats
import pprint
from tabulate import tabulate

# sys.path.append('/home/tzhou/projects/mm/pymcolor/')
# os.environ["PYTHONPATH"] = '/home/tzhou/projects/mm/pymcolor/'
# pythonpath = os.environ.get("PYTHONPATH").split(':')

from lib.globals import *
from lib.utils import *
from lib.exp import *
from lib.report import *
print "loaded pymcolor globals, utils, exp, and report"

import json

pp = pprint.PrettyPrinter(indent=4)
dis_table = []
failed = []

def dyke():
    runexps(benches=["pmd-small"], cfgs=[FIELD_TLAB_INTERVAL], xjopts=['-XX:+PrintTextAPInfo'], objrate=1000, xhopts=[iteropt(10)],iters=1,verbose=V2)

def slur():
    ap = allocPointInfo('pmd-small',FIELD_TLAB_INTERVAL,getssi=False, getvki=True, clean=True)

def calc_benches(size='small'):
    # small_benches = ["lusearch-small", "pmd-small", "xalan-small", "luindex-small", "tradebeans-small", "fop-small", "sunflow-small", "eclipse-small", "jython-small", "batik-small", "h2-small", "avrora-small", "tomcat-small", "tradesoap-small"]
    # default_benches = ["lusearch-default", "pmd-default", "xalan-default", "luindex-default", "tradebeans-default", "fop-default", "sunflow-default", "eclipse-default", "jython-default", "batik-default", "h2-default", "avrora-default", "tomcat-default", "tradesoap-default"]

    benches_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "batik", "h2", "avrora", "tomcat", "tradesoap"]
    benches = map(lambda x: x+'-'+size, benches_prefix)
    data = map(lambda x: (x, malaise(x)), benches)
    json.dump(data, open(size+'_bench_data.txt', 'w'))

def ct_ri_analyze(benches):
    global failed, dis_table
    thresholds = [0.8, 0.9, 0.95]
    for threshold in thresholds:        
        print 'threshold:', threshold
        for bench in benches:
            print 'do', benches
            try:
                ri_analyze(bench, threshold)
            except:
                failed.append(b)
                traceback.print_exc()
        add_mean_row('geometric')
        print_table(dis_table, ['unused', 'read mostly', 'read only', 'sum'])
        dis_table = []

    
# def ct_ri_analyze(bench):
#     for threshold in [0.8, 0.9, 0.95]:
#         ri_analyze(bench, threshold)

    
def ri_analyze(bench, threshold = 0.9):
    global dis_table
    ap = allocPointInfo(bench,FIELD_TLAB_INTERVAL,getssi=False, getvki=True, clean=True)
    #valid_reason = ['pre-minor-gc', 'post-minor-gc', 'end-of-run']
    valid_reason = ['end-of-run']

    field_num = 0.0
    class_num = 0.0
    ri_field_num = 0.0
    ri_class_num = 0.0
    ri_rate = threshold

    ro_field_num = 0.0
    unused_field_num = 0.0

    klass_map = ap[KLASS_MAP]
    for colle in ap['VALS']:
        
        kr = colle['KLASSRECORD']

        if kr['reason'].strip() not in valid_reason:
            continue
        klass_reads = 0
        klass_writes = 0
        if kr['reason'].strip() in valid_reason:
            print 'klasses num in the colle:', len(kr['klasses'])
            for k in kr['klasses']:
                if 'app_instance' not in k['type']:
                    continue
                instance_size = int(k['instance_size'])
                fields_map = k['field_table']
                if not isinstance(fields_map, dict):
                    #print k['name']
                    continue

                tc_fields_num = len(fields_map.keys())
                tc_ri_fields_num = 0

                class_num += 1
                for offset, info in fields_map.iteritems():
                    reads = float(info['reads'])
                    writes = float(info['writes'])                        
                    klass_reads += reads
                    klass_writes += writes

                    field_num += 1
                    if reads+writes == 0:
                        unused_field_num += 1
                    elif writes == 0:
                        ro_field_num += 1
                    elif reads/(reads+writes) > ri_rate:
                        ri_field_num += 1
                        tc_ri_fields_num += 1
                if klass_reads/(klass_reads+klass_writes) > ri_rate:
                    ri_class_num += 1

        unused_rate = unused_field_num/field_num
        ri_rate = ri_field_num/field_num
        ro_rate = ro_field_num/field_num
        print 'field: %d, unused: %d, ri_field: %d, ro_field: %d' % (field_num, unused_field_num, ri_field_num, ro_field_num)
        #row = map(lambda x: '{:.2%}'.format(x), (unused_rate, ri_rate, ro_rate, unused_rate+ro_rate+ri_rate))
        row = ['%s (%.2f)'%(bench, threshold), unused_rate, ri_rate, ro_rate, unused_rate+ro_rate+ri_rate]
        dis_table.append(row)

def add_mean_row(mean="geometric"):
    global dis_table
    unusedx = map(lambda x: x[1], dis_table)
    rix = map(lambda x: x[2], dis_table)
    rox = map(lambda x: x[3], dis_table)
    sumx = map(lambda x: x[4], dis_table)

    row = ['geomean']
    if mean == 'geometric':
        row.append(geomean(unusedx))
        row.append(geomean(rix))
        row.append(geomean(rox))
        row.append(geomean(sumx))
    dis_table.append(row)
        

def print_table(table, headers, fmt="grid"):
    formatted_table = []
    for row in table:
        newrow = []
        for item in row:
            if isinstance(item, float):
                newrow.append('{:.2%}'.format(item))
            else:
                newrow.append(item)
        formatted_table.append(newrow)

    print tabulate(formatted_table, headers, tablefmt=fmt)

def cold_analyze(bench):
    # ap = {'VALS': {}}
    # print '%s:' % bench 
    # setupValInfo(bench=bench, cfg=FIELD_TLAB_INTERVAL, iter=0, apinfo=ap)
    # getValKlassInfoFromLog(bench=bench, cfg=FIELD_TLAB_INTERVAL, iter=0, apinfo=ap, app_only=False)
    ap = allocPointInfo(bench,FIELD_TLAB_INTERVAL,getssi=False, getvki=True, clean=True)
    #valid_reason = ['pre-minor-gc', 'signal', 'end-of-run']
    valid_reason = ['end-of-run']
    
    field_num = 0.0
    class_num = 0.0
    cold_field_num = 0.0
    normal_field_num = 0.0
    hot_field_num = 0.0

    all_fields_size = 0
    
    cold_field_size = 0
    normal_field_size = 0
    hot_field_size = 0

    all_refs = 0.0
    cold_refs = 0.0
    normal_refs = 0.0
    hot_refs = 0.0
    
    cold_threshold = 0.3
    hot_threshold = 0.9

    klass_map = ap[KLASS_MAP]
    for colle in ap['VALS']:
        #print colle.keys()
        kr = colle['KLASSRECORD']

        if kr['reason'].strip() not in valid_reason:
            # print 'invalid reason found, exit.'
            # exit(0)
            continue
            
        print 'klasses num in the colle:', len(kr['klasses'])
        for k in kr['klasses']:
            if 'app_instance' not in k['type']:
                continue
            instance_size = int(k['instance_size'])
            fields_map = k['field_table']
            klass_id = int(k['id'])
            fields_size = int(k['instance_size']) - 12

            instance_num = klass_map[klass_id][OBJECTS]
            instance_size *= instance_num

            print k['name']
            all_fields_size += instance_num * fields_size
            
            tcl_fields_num = 0
            tcl_cold_field_num = 0
            tcl_normal_field_num = 0
            tcl_hot_field_num = 0

            tcl_cold_field_size = 0
            tcl_normal_field_size = 0
            tcl_hot_field_size = 0

            print 'fields size', fields_size
            if fields_map:
                tcl_field_num = float(len(fields_map.keys()))
                field_num += tcl_field_num
                refs = map(lambda x: int(fields_map[x]['refs']), fields_map)
                tcl_ref_num = float(sum(refs))
                med_refs = np.median(refs)
                print refs
                print 'median', med_refs
                
                for offset, info in fields_map.iteritems():
                    refs = float(info['refs'])
                    size = float(info['size'])
                    print 'refs', refs, 'size', size
                    size *= instance_num
                    all_refs += refs
                    
                    if refs/med_refs < 0.25:                        
                        cold_field_num += 1
                        cold_field_size += size
                        tcl_cold_field_num += 1
                        tcl_cold_field_size += size
                        cold_refs += refs
                        print ' > cold'
                    elif refs/med_refs < 10:
                        normal_field_num += 1
                        normal_field_size += size
                        tcl_normal_field_num += 1
                        tcl_normal_field_size += size
                        normal_refs += refs
                        print ' > normal'
                    else:
                        hot_field_num += 1
                        hot_field_size += size
                        tcl_hot_field_num += 1
                        tcl_hot_field_size += size
                        hot_refs += refs
                        print ' > hot'

                    # if refs/tcl_ref_num < cold_threshold:                        
                    #     cold_field_num += 1
                    #     cold_field_size += size
                    #     tcl_cold_field_num += 1
                    #     tcl_cold_field_size += size
                    #     cold_refs += refs
                    # elif refs/tcl_ref_num < hot_threshold:
                    #     normal_field_num += 1
                    #     normal_field_size += size
                    #     tcl_normal_field_num += 1
                    #     tcl_normal_field_size += size
                    #     normal_refs += refs
                    # else:
                    #     hot_field_num += 1
                    #     hot_field_size += size
                    #     tcl_hot_field_num += 1
                    #     tcl_hot_field_size += size
                    #     hot_refs += refs

            print '\n\n'
            #print tcl_cold_fields_num/float(tcl_fields_num)
    #print field_num, cold_field_num
    print '========= space/refs percentage ========='
    print '%.3f fields are cold fields. They take up %.3f space and account for %.3f accesses' % (cold_field_num/field_num, cold_field_size/all_fields_size, cold_refs/all_refs)
    print '%.3f fields are normal fields. They take up %.3f space and account for %.3f accesses' % (normal_field_num/field_num, normal_field_size/all_fields_size, normal_refs/all_refs)
    print '%.3f fields are hot fields. They take up %.3f space and account for %.3f accesses' % (hot_field_num/field_num, hot_field_size/all_fields_size, hot_refs/all_refs)

    # print '========= instance size statistics =========='
    # print 'cold fields percentage', cold_field_size/all_fields_size
    # print 'normal fields percentage', normal_field_size/all_fields_size
    # print 'hot fields percentage', hot_field_size/all_fields_size

    print '========= ref per byte =========='
    # print 'cold fields', (cold_refs/all_refs)/(cold_field_size/all_fields_size)
    # print 'normal fields', (normal_refs/all_refs)/(normal_field_size/all_fields_size)
    # print 'hot fields', (hot_refs/all_refs)/(hot_field_size/all_fields_size)

    print 'cold fields', (cold_refs/cold_field_size)
    print 'normal fields', (normal_refs)/(normal_field_size)
    print 'hot fields', (hot_refs)/(hot_field_size)
    
def malaise(bench):
    ap = {'VALS': {}}
    print '%s:' % bench 
    setupValInfo(bench=bench, cfg=FIELD_TLAB_INTERVAL, iter=0, apinfo=ap)
    getValKlassInfoFromLog(bench=bench, cfg=FIELD_TLAB_INTERVAL, iter=0, apinfo=ap, app_only=False)
    #ap = allocPointInfo(bench,FIELD_TLAB_INTERVAL,getssi=False, getvki=True, clean=True)
    valid_reason = ['pre-minor-gc', 'signal', 'end-of-run']
    arr_numx = []
    instance_numx = []
    for colle in ap['VALS']:        
        if colle['reason'].strip() in valid_reason:
            print 'klasses num in the colle:', len(colle['klasses'])
            for k in colle['klasses']:
                print k['field_table']
                if 'app_instance' in k['type']:
                    instance_numx.append(int(k['klass_table']['new']['refs']))
                elif 'app_array' in k['type']:
                    arr_numx.append(int(k['klass_table']['new']['refs']))
                else:
                    pass
                    #print [k['type']], k['klass_table']['new']['refs']
            # klasses.append((k['name'], k['klass_table']['new']['refs']))
            # arr_klasses = filter(lambda x: x[0][0] == '[', klasses)
            # nonarr_klasses = filter(lambda x: x[0][0] != '[', klasses)
            # arr_num = sum(map(lambda x: int(x[1]), arr_klasses))
            # nonarr_num = sum(map(lambda x: int(x[1]), nonarr_klasses))
            # arr_numx.append(arr_num)
            # instance_numx.append(nonarr_num)
            # # print 'arr_num:', arr_num
            # # print 'instance_num:', nonarr_num
            # # print 'all:', arr_num + nonarr_num

    # if bench == 'lusearch-small':
    #     print arr_numx[1:40]
    #     print instance_numx[1:40]
    arr_sum = sum(arr_numx)
    instance_sum =  sum(instance_numx)
    if (instance_sum+arr_sum) == 0:
        return 0

    ratio = float(instance_sum)/(instance_sum+arr_sum)
    #print 'instance: %s, array: %s, ratio: %s' % (instance_sum, arr_sum, ratio)
    print ratio
    return ratio

if __name__ == '__main__':
    #benches = ["lusearch-small", "pmd-small", "xalan-small", "luindex-small", "tradebeans-small", "fop-small", "sunflow-small", "eclipse-small", "jython-small", "batik-small", "h2-small", "avrora-small", "tomcat-small", "tradesoap-small"]
    #benches = ["lusearch-default", "pmd-default", "xalan-default", "luindex-default", "tradebeans-default", "fop-default", "sunflow-default", "eclipse-default", "jython-default", "avrora-default", "batik-default", "h2-default", "tomcat-default", "tradesoap-default"]

    bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "batik", "h2", "avrora", "tomcat", "tradesoap"]
    #bench_prefix = ["lusearch"]
    bench_size = 'small'
    #bench_size = 'default'

    if len(sys.argv) > 1:
        if sys.argv[1] == '-small':
            bench_size = 'small'
        elif sys.argv[1] == '-default':
            bench_size = 'default'

        if len(sys.argv) > 2:
            if sys.argv[2] == 'all':
                bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "batik", "h2", "avrora", "tomcat", "tradesoap"]
            else:
                bench_prefix = []
                for i in range(2, len(sys.argv)):
                    bench_prefix.append(sys.argv[i])

    
    benches = [b+'-'+bench_size for b in bench_prefix]
    
    ct_ri_analyze(benches)
    # ri_analyze('pmd-small')
    # ri_analyze('fop-small')
    # #get_apinfo('pmd-small')
    # print_table(dis_table, ['unused', 'read mostly', 'read only', 'sum'])
