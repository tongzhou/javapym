#! /usr/bin/env python

import os
import sys
import re
import regex
import numpy as np
from scipy import stats as scistats
import pprint

# sys.path.append('/home/tzhou/projects/mm/pymcolor/')
# os.environ["PYTHONPATH"] = '/home/tzhou/projects/mm/pymcolor/'
# pythonpath = os.environ.get("PYTHONPATH").split(':')

from lib.globals import *
from lib.utils import *
from lib.exp import *
from lib.report import *
print "loaded pymcolor globals, utils, exp, and report"

import json

pp = pprint.PrettyPrinter(indent=4)

def runone(bench):
    runexps(benches=[bench], cfgs=[FIELD_TLAB_INTERVAL], xjopts=['-XX:LoadFieldInfo=/home/tzhou/obj_hotness/fieldinfo/%s.txt' % bench, '-XX:+PrintTextAPInfo', '-XX:-PrintObjectAddressInfoAtInterval'], xhopts=[iteropt(1)],iters=1,verbose=V2)

def get_benches():
    bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "avrora", "batik", "h2", "tomcat", "tradesoap"]
    #bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse"]
    bench_prefix = ["jython", "avrora", "batik"]
    bench_size = 'small'
    #bench_size = 'default'

    if len(sys.argv) > 1:
        if sys.argv[1] == '-small':
            bench_size = 'small'
        elif sys.argv[1] == '-default':
            bench_size = 'default'

        if len(sys.argv) > 2:            
            if sys.argv[2] == 'all':
                bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "avrora", "batik", "h2", "tomcat", "tradesoap"]
            else:
                bench_prefix = []
                for i in range(2, len(sys.argv)):
                    bench_prefix.append(sys.argv[i])
    benches = [b+'-'+bench_size for b in bench_prefix]
    return benches
    
if __name__ == '__main__':
    for b in get_benches():
        runone(b)
