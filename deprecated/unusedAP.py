#! /usr/bin/env python

from BaseAnalyzer import *

def ct_ri_analyze(bench, thresholds=[0.8, 0.9, 0.95]):
    global dis_table
    print bench
    ap = allocPointInfo(bench,AP_TLAB_INTERVAL,getssi=False, getvki=False, clean=True, sumvals=False)

    used_intervals = [-1, 0, 1] # meaning an ap is only used in the first interval
    used_inter_num = 3
    unused_ap_num = 0.0
    ri_ap_num = [0.0 for i in range(0, used_inter_num)]
    ro_ap_num = 0.0
    all_ap_num = 0.0
    
    unused_size = [0.0 for i in range(0, used_inter_num)]
    ri_size = [0.0 for i in range(0, used_inter_num)]
    ro_size = 0.0
    all_size = 0.0

    print 'apnum:', len(ap['APID_FULL'])
    for apid in ap['APID_FULL']:
        if apid == 0:
            continue

        cases = len(used_intervals)
        qualified = [True for i in range(0, cases)]
        this_ap_size = 0.0

        for i, info in enumerate(ap['APID_FULL'][apid]):
            objs = info['new_objs']
            new_size = int(info['new_size'])
            reads = info['reads']
            writes = info['writes']
            refs = int(info['refs'])

            if i == 0 and refs != 0:
                qualified[0] = False
            elif i == 1 and refs != 0:
                qualified[0] = False
                qualified[1] = False
            # elif i == 2 and refs != 0:
            #     qualified[0] = False
            #     qualified[1] = False
            #     qualified[2] = False
            elif i > 1 and refs != 0:
                qualified[0] = False
                qualified[1] = False
                qualified[2] = False

            all_size += new_size
            this_ap_size += new_size
        
        if qualified[0]:
            unused_size[0] += this_ap_size
        elif qualified[1]:
            unused_size[1] += this_ap_size
        elif qualified[2]:
            unused_size[2] += this_ap_size

    unused_rate = [0.0 for i in range(0, used_inter_num)]
    for i in range(0, used_inter_num):
        unused_rate[i] = unused_size[i]/all_size
    print unused_rate
    unused_rate.insert(0, bench)
    dis_table.append(unused_rate)
    # #row = ['%s (%.2f)'%(bench,threshold), unused_rate, ri_ap_rate, ro_ap_rate, unused_rate+ro_ap_rate+ri_ap_rate]
    # row = [bench, unused_rate, ro_ap_rate]
    # for i in range(0, used_inter_num):
    #     row.append(ri_ap_rate[i])
    #     row.append(ri_ap_rate[i]+unused_rate+ro_ap_rate)
    # dis_table.append(row)
    # #print 'table', dis_table

    
def add_mean_row(mean="geometric"):
    global dis_table
    print 'table', dis_table
    column_num = len(dis_table[0])
    row = ['geomean']
    for col in range(1, column_num):
        colx = map(lambda x: x[col], dis_table)
        row.append(geomean(colx))
    # unusedx = map(lambda x: x[1], dis_table)
    # rix = map(lambda x: x[2], dis_table)
    # rox = map(lambda x: x[3], dis_table)
    # sumx = map(lambda x: x[4], dis_table)

    
    # if mean == 'geometric':
    #     row.append(geomean(unusedx))
    #     row.append(geomean(rix))
    #     row.append(geomean(rox))
    #     row.append(geomean(sumx))
    dis_table.append(row)

    
def print_table(table, headers, fmt="grid", fp=None):
    formatted_table = []
    for row in table:
        newrow = []
        for item in row:
            if isinstance(item, float):
                newrow.append('{:.2%}'.format(item))
            else:
                newrow.append(item)
        formatted_table.append(newrow)
    print tabulate(formatted_table, headers, tablefmt=fmt)
    if fp is not None:
        print >> fp, tabulate(formatted_table, headers, tablefmt=fmt)
    
def get_apinfo(bench):
    ap = allocPointInfo(bench,AP_TLAB_INTERVAL,getssi=False, getvki=False, clean=True)

    #pp.pprint(ap['AP_MAP'])
    #print ap['VALS'][0].keys()
    #pp.pprint(ap['VALS'][0]['APD'][2019])

def write_line_to_xlsx(table, header, fn):
    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(fn)
    worksheet = workbook.add_worksheet()

    # Some data we want to write to the worksheet.
    expenses = (
            ['Rent', 1000],
            ['Gas',   100],
            ['Food',  300],
            ['Gym',    50],
        )

    row = 0
    col = 0

    for line in table:
        for col, field in enumerate(line):
            worksheet.write(row, col, field)
        row += 1
    workbook.close()

def reform_to_stacked_bar(table, size, fn):
    table_cp = copy.deepcopy(table)
    print table
    visual_table = table
    zipped = zip(*visual_table)
    
    workbook = xlsxwriter.Workbook(fn)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({'bold': 1})

    print zipped

    # Add the worksheet data that the charts will refer to.
    headings = ['Benchmark',  'unused']
    data = zipped

    worksheet.write_row('A1', headings, bold)
    worksheet.write_column('A2', data[0]) # bench names
    worksheet.write_column('B2', data[1]) # unused
    worksheet.write_column('C2', data[2]) # unused
    worksheet.write_column('D2', data[3]) # unused

    #######################################################################
    #
    # Create a percentage stacked chart sub-type.
    #
    chart3 = workbook.add_chart({'type': 'column', 'subtype': 'stacked'})

    series_num = 1
    bench_num = 15 #include geomean
    
    # unused
    chart3.add_series({
            'name':       '=Sheet1!$B$1',
            'categories': '=Sheet1!$A$2:$A$%d' % (bench_num+1),
            'values':     '=Sheet1!$B$2:$B$%d' % (bench_num+1),
        })

    # 
    chart3.add_series({
            'name':       '=Sheet1!$C$1',
            'categories': '=Sheet1!$A$2:$A$%d' % (bench_num+1),
            'values':     '=Sheet1!$C$2:$C$%d' % (bench_num+1),
        })

    # 
    chart3.add_series({
            'name':       '=Sheet1!$D$1',
            'categories': '=Sheet1!$A$2:$A$%d' % (bench_num+1),
            'values':     '=Sheet1!$D$2:$D$%d' % (bench_num+1),
        })

    
    chart3.set_title ({'name': 'Read-only Analysis'})
    chart3.set_x_axis({'name': 'DaCapo-9.12-bach'})
    chart3.set_y_axis({'name': 'Size Percentage', 'max': 1.0})

    # Set an Excel chart style.
    chart3.set_style(13)

    chart3.set_size({'x_scale': 2, 'y_scale': 2})
    # Insert the chart into the worksheet (with an offset).
    worksheet.insert_chart('D20', chart3, {'x_offset': 10, 'y_offset': 10})

    workbook.close()

    
    
if __name__ == '__main__':
    bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "avrora", "batik", "h2", "tomcat", "tradesoap"]
    bench_prefix = ["lusearch", "pmd"]
    bench_size = 'small'
    #bench_size = 'default'

    if len(sys.argv) > 1:
        if sys.argv[1] == '-small':
            bench_size = 'small'
        elif sys.argv[1] == '-default':
            bench_size = 'default'

        if len(sys.argv) > 2:            
            if sys.argv[2] == 'all':
                bench_prefix = ["lusearch", "pmd", "xalan", "luindex", "tradebeans", "fop", "sunflow", "eclipse", "jython", "avrora", "batik", "h2", "tomcat", "tradesoap"]
            else:
                bench_prefix = []
                for i in range(2, len(sys.argv)):
                    bench_prefix.append(sys.argv[i])

    else:
        print textwrap.dedent('''
            Usage: ./script [option]... [size] [bench]...
            Run the analysis script on the given benchmarks.
            Arguments.
              -s			Use a scratch directory to store results, which defaults to scriptname_results 				
            Example:
                ./script -small pmd
                ./script -small pmd fop
                ./script -small all
                ./script -default all
        ''')
        exit(0)
    
    benches = [b+'-'+bench_size for b in bench_prefix]
    
    
    for b in benches:
        try:
            ct_ri_analyze(b)
        except:
            traceback.print_exc()
            failed.append(b)

    add_mean_row('geometric')

    header = ['unused-0', 'unused-1', 'unused-2']
    write_line_to_xlsx(dis_table, header, 'unusedAP_scratch/table_'+bench_size+'.xlsx')
    reform_to_stacked_bar(dis_table, bench_size, 'unusedAP_scratch/chart_'+bench_size+'.xlsx')
    # write_line_to_xlsx(dis_table, header, 'apResults/table_'+bench_size+'.xlsx')
    # reform_to_stacked_bar(dis_table, bench_size, 'apResults/chart_'+bench_size+'.xlsx')

    print 'failed:', failed
