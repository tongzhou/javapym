#############################################################################
# pyzephyr globals
#############################################################################
import re
from platform import mcolordir
#############################################################################
# global flags
#############################################################################
PrintAPInfoParsing   = True
PrintProcessingSteps = False

# platform flags
COM1696              = True
BRAZILSERVER         = False

#############################################################################
# files and directories
#############################################################################

#mcolordir      = "/home/mrjantz/research/projects/memcolor/"
#mcolordir      = "/home/tzhou/projects/mm9/"
#if BRAZILSERVER:
 # mcolordir    = "/home/tzhou/obj_hotness/"


hotspotdir     = mcolordir   + "hotspot9/"
benchesdir     = mcolordir   + "benchmarks/"
analysisdir    = mcolordir   + "javapym/analysis/"

resultsdir     = hotspotdir  + "results/"
parresultsdir  = hotspotdir  + "par-results/"
ohotspot       = hotspotdir  + "build/linux/linux_amd64_compiler2/optimized/hotspot"
dhotspot       = hotspotdir  + "build/linux/linux_amd64_compiler2/debug/hotspot"
photspot       = hotspotdir  + "build/linux/linux_amd64_compiler2/objprofile2/hotspot"
ahotspot       = hotspotdir  + "build/linux/linux_amd64_compiler2/objaddr/hotspot"
ehotspot       = hotspotdir  + "build/linux/linux_amd64_compiler2/colorededen/hotspot"
thotspot       = hotspotdir  + "build/linux/linux_amd64_compiler2/coloredtlabs/hotspot"
defhotspot     = ohotspot
#ohotspot       = "/projects/zephyr/mjantz/dynzeph/hotspot/build/linux/linux_i486_compiler2/optimized/hotspot"
ghotspot       = hotspotdir  + "build/linux/linux_amd64_compiler2/jvmg/hotspot"
jvm2008sdir    = benchesdir  + "jvm2008/"
dacaposdir     = benchesdir  + "dacapo/"
customsdir     = benchesdir  + "custom/"
#origjvm2008jar = jvm2008sdir + "orig-build/release/SPECjvm2008/SPECjvm2008.jar"
origjvm2008jar = jvm2008sdir + "SPECjvm2008.jar"
hugejvm2008jar = jvm2008sdir + "huge-build/release/SPECjvm2008/SPECjvm2008.jar"
#jvm2008jar     = jvm2008sdir + "SPECjvm2008.jar"
dacapojar      = dacaposdir  + "dacapo-9.12-bach.jar"
#dacapojar      = dacaposdir  + "dacapo.jar"
jgfmultidir    = benchesdir + "jgf/threadv1.0"
jgfsingledir   = benchesdir + "jgf/v2.0"
jgfharness     = "harness.JGFHarness"
devshm         = "/dev/shm/"
shmroot        = devshm + "color-pp/"
rawout         = "raw.out"
cmdout         = ".cmd.out"
jvm2008results = jvm2008sdir + "results/"
iterd          = "i%d/"
rdatad         = "raw-data/"
scratchd       = ".scratch/"
emonevtsfile   = "emon-events.txt"
emonrawfile    = "emon.dat"
pgovrawfile    = "pgov.dat"
pcmpowerfile   = "pcm-power.dat"
pcmmemoryfile  = "pcm-memory.dat"
pcmsrfile      = "pcm-self-refresh.dat"
tmppcmfile     = "/tmp/pcm-power.dat"
pcmfile        = "pcm.dat"
rawsrfile      = "raw-self-refresh.out"
rawmemoryfile  = "raw-memory.out"
ptitsamplefile = "ptit-sample.dat"
mscramoutfile  = ".mscram.out"
emonmasterdir  = mcolordir  + "emon-master/"
edproc         = "edproc.py"
processmaster  = emonmasterdir + "process.sh"
edprbmaster    = emonmasterdir + "edp.rb"
snbmaster      = emonmasterdir + "snb-ep-events.txt"
nhmmaster      = emonmasterdir + "nhm-ep-events.txt"
edpout         = "edp.out"
esumviewfile   = "__edp_system_view_summary.csv"
esockviewfile  = "__edp_socket_view_summary.csv"
edashvfile     = "emon-v.dat"
edashMfile     = "emon-M.dat"
emoncmd        = "emon"
#killemoncmd    = "kill-emon-script.sh"
killemoncmd    = "killall -s SIGUSR1 counter"
killpgovcmd    = "killall power_gov"
killpcmpowcmd  = "killall pcm-power.x"
killpcmmemcmd  = "killall pcm-memory.x"
killpcmcmd     = "killall pcm.x"
killmscramcmd  = "killall mscramble"
ppemfile       = "%s.ppe.out"
numactlcmd     = "numactl"
mcolorctlcmd   = "mcolorctl"
mscramblecmd   = "mscramble"
trayctlcmd     = "trayctl"
powergovcmd    = "power_gov"
dropcachescmd  = "echo 3 | sudo tee /proc/sys/vm/drop_caches > /dev/null"
pcmpowercmd    = "mcolorctl --color=BLACK --bind=0 pcm-power.x %d -m 0"
pcmmemorycmd   = "mcolorctl --color=BLACK --bind=0 pcm-memory.x %d"
pcmsrcmd       = "mcolorctl --color=BLACK --bind=0 pcm-power.x %d -m 4"
pcmcmd         = "mcolorctl --color=BLACK --bind=0 pcm.x %d -m 0"
ptitsamplecmd  = "numactl --cpunodebind=0 --preferred=0 ptit-sample.py -n1 -d%d"
unixtimecmd    = "/usr/bin/time"
ecuador        = "ecuador"
colombia       = "colombia"
objinfolog     = "objinfo.log"
addrinfolog    = "addrinfo.log"
addrinfobin    = "addrinfo.bin"
krinfolog      = "krinfo.log"
krinfobin      = "krinfo.bin"
objalloclog    = "objalloc.log"
apmaplog       = "apmap.log"
klassmaplog    = "klassmap.log"
apinfolog      = "apinfo.log"
apinfobin      = "apinfo.bin"
deadobjlog     = "deadobj.log"
mkalslog       = "mkals.log"
stackslog      = "stacks.log"
endvalfile     = ".endval.txt"
refmaplog      = "refmap.log"
sortobjexe     = "sort_objalloc.py"
buildrefmapexe = "build_refmap.py"
objclusterexe  = "cluster_objects.py"
drawheatexe    = "draw_heatmap.py"
dynamicheatexe = "dynamic_heatmap.py"
kcinfoexe      = "klass_cluster_info.py"
refpdf         = "ref-c%d-all.pdf"
dynrefpdf      = "dynref.pdf"
refsmplpdf     = "ref-c%d-s%d.pdf"
obj2clustfile  = ".object2cluster-c%d.pkl"
clust2objfile  = ".cluster2object-c%d.pkl"
kcinfofile     = "kcinfo-c%d-all.txt"
kcsinfofile    = "kcinfo-c%d-s%d.txt"
hotsetpkl      = "hotsets-t%d-r%d.pkl"
coldsetpkl     = "coldsets-t%d.pkl"
hotsetsexe     = "dynamic_hotsets.py"
coldsetsexe    = "dynamic_coldsets.py"
hotvalreport   = "hotset_val_report-t%d-r%d.txt"
cvrratio       = "coldsets-t%d-r%d-d%d-m%d.txt"
cvrsize        = "coldsets-t%d-s%d-d%d-m%d.txt"
cvrthresh      = "coldsets-t%d-d%d-m%d.txt"
#cprevrfile     = "color-pre-val.txt"
#cpostvrfile    = "color-post-val.txt"
cvrfile        = "color-vals.txt"
powerrepfile   = "pcm-power-report.txt"
memoryrepfile  = "pcm-memory-report.txt"
pcmrepfile     = "pcm-report.txt"
apirepfile     = "api-report.txt"
apcrepfile     = "apc-report.txt"
apccmdsfile    = ".apc-cmds.txt"
corfile        = ".cor.txt"
allocdictpkl   = ".alloc-dict.pkl"
apiapppkl      = ".api-app.pkl"
apiallpkl      = ".api-all.pkl"
kspkl          = ".knapsack-sim-%s-%3.2f.pkl"
kszpkl         = ".ksz-sim-%s-%3.2f.pkl"
ksslimpkl      = ".ks-slim-%s-%3.2f.pkl"
kszslimpkl     = ".ksz-slim-%s-%3.2f.pkl"
oaipkl         = ".obj-addr-info-%s.pkl"
oairpbpkl      = ".obj-addr-info-bpr-%s.pkl"
oaikspkl       = ".obj-addr-info-ks-%s.pkl"
ofipkl         = ".obj-field-info.pkl"
csgcrepfile    = "csgc-report.txt"
asvrfile       = "active-set-vals.txt"
redobjinfo     = "reds.log"
shmobjdict     = "objd.dat"
shmhotdict     = "hotd.dat"
shmlivesdir    = "lives/"
shmlives       = "v%d_lives.dat"
deftarfile     = mcolordir+"mcolor.tar.gz"
deftardir      = "/tmp/mcolor/"
backupdir      = hotspotdir+"backup/"
apccmd         = "apcolor"
rdlfile        = "replay_data.log"
rdbfile        = "replay_data.bin"
rofile         = "replay_oops.log"

#############################################################################
# bench style results directories
#############################################################################
def benchdir(bench):
  return resultsdir + "%s/" % bench

def expdir(bench, cfg):
  return benchdir(bench) + "%s/" % cfg

def iterdir(bench, cfg, iter):
  return expdir(bench, cfg) + (iterd % iter)

def rawDataDir(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + rdatad

def emonEventsFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + emonevtsfile

def emonRawFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + emonrawfile

def pgovRawFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + pgovrawfile

def pcmPowerFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + pcmpowerfile

def pcmMemoryFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + pcmmemoryfile

def pcmSelfRefreshFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + pcmsrfile

def pcmFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + pcmfile

def ptitSampleFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + ptitsamplefile

def rawMemoryFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + rawmemoryfile

def rawSelfRefreshFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + rawsrfile

def methodCountersFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + mcfile

def replayDataLogFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + rdlfile

def replayDataBinFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + rdbfile

def replayOopsFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + rofile

def rawoutfile(bench, cfg, iter, type=None):
  if type == PCM_MEMORY:
    return rawMemoryFile(bench, cfg, iter)
  elif type == PCM_SELF_REFRESH:
    return rawSelfRefreshFile(bench, cfg, iter)
  else:
    return iterdir(bench, cfg, iter) + rawout

def cmdoutfile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + cmdout

def emonSummaryViewFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + esumviewfile

def emonSocketViewFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + esockviewfile

def emonDashvFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + edashvfile

def emonDashMFile(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + edashMfile

def ppEmonMetricFile(bench, cfg, iter, metric):
  return rawDataDir(bench, cfg, iter) + \
         (ppemfile % (metric.lower()))

def scratchdir(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + scratchd

def mscramOutFile(bench, cfg):
  return expdir(bench, cfg) + mscramoutfile

def objInfoLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + objinfolog

def objAddrInfoLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + addrinfolog

def objAddrInfoBin(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + addrinfobin

#def unknownPagesLog(bench, cfg, iter):
#  return rawDataDir(bench, cfg, iter) + addrupslog

def klassRecordInfoLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + krinfolog

def klassRecordInfoBin(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + krinfobin

def objAllocLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + objalloclog

def deadObjLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + deadobjlog

def apMapLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + apmaplog

def klassMapLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + klassmaplog

def apInfoLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + apinfolog

def apInfoBin(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + apinfobin

def mkalsLog(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + mkalslog

def stackSampleLog(bench, cfg, iter):
  return rawDataDir(bench, cfg, iter) + stackslog

def endValFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + endvalfile

def refMapLog(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + refmaplog

def refPDF(bench, cfg, iter, clusts, samples=None):
  if samples is None:
    return iterdir(bench, cfg, iter) + (refpdf % clusts)
  else:
    return iterdir(bench, cfg, iter) + (refsmplpdf % (clusts,samples))

def dynRefPDF(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + dynrefpdf

def clusterToObjectFile(bench, cfg, iter, clusts):
  return iterdir(bench, cfg, iter) + (clust2objfile % clusts)

def objectToClusterFile(bench, cfg, iter, clusts):
  return iterdir(bench, cfg, iter) + (obj2clustfile % clusts)

def klassClusterInfoFile(bench, cfg, iter, clusts, samps=None):
  if samps is None:
    return iterdir(bench, cfg, iter) + (kcinfofile % clusts)
  else:
    return iterdir(bench, cfg, iter) + (kcsinfofile % (clusts,samps))

def hotSetPkl(bench, cfg, iter, thresh, rat):
  return iterdir(bench, cfg, iter) + (hotsetpkl % (thresh, int(rat*100)))

def coldSetPkl(bench, cfg, iter, thresh):
  return iterdir(bench, cfg, iter) + (coldsetpkl % thresh)

def hotsetValReport(bench, cfg, iter, thresh, rat):
  return iterdir(bench, cfg, iter) + (hotvalreport % (thresh, int(rat*100)))

def coldsetValReport(bench, cfg, iter, thresh=0, delay=0, mem=1, ratio=None, \
                     size=None):
  if not ratio is None:
    return iterdir(bench, cfg, iter) + (cvrratio % (thresh, int(ratio*100.0), delay, mem))
  elif not size is None:
    return iterdir(bench, cfg, iter) + (cvrsize % (thresh, size, delay, mem))
  else:
    return iterdir(bench, cfg, iter) + (cvrthresh % (thresh, delay, mem))

def colorValReport(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + cvrfile

def pcmPowerReportFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + powerrepfile

def pcmMemoryReportFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + memoryrepfile

def pcmReportFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + pcmrepfile

def apiReportFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + apirepfile

def apcReportFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + apcrepfile

def apcCmdsFile(bench, cfg):
  return expdir(bench, cfg) + apccmdsfile

def bwColorAPCFile(bench):
  if bench in bw_benches:
    return customsdir + "BandwidthBench/" + apccmdsfile
  elif bench in simplebb_benches:
    return customsdir + "SimpleBB/" + apccmdsfile

def corFile(bench, cfg):
  return expdir(bench, cfg) + corfile

def allocDictPkl(bench, cfg, iter=0):
  return iterdir(bench, cfg, iter) + allocdictpkl

def allocInfoPkl(bench, cfg, iter=0, app_only=False):
  if app_only:
    return iterdir(bench, cfg, iter) + apiapppkl
  else:
    return iterdir(bench, cfg, iter) + apiallpkl

def knapsackSimPkl(bench, cfg, iter, guide_bench, cutoff):
  return iterdir(bench, cfg, iter) + (kspkl % (guide_bench, cutoff))

def kszSimPkl(bench, cfg, iter, guide_bench, cutoff):
  return iterdir(bench, cfg, iter) + (kszpkl % (guide_bench, cutoff))

def knapsackSimSlimPkl(bench, cfg, iter, guide_bench, cutoff):
  return iterdir(bench, cfg, iter) + (ksslimpkl % (guide_bench, cutoff))

def kszSimSlimPkl(bench, cfg, iter, guide_bench, cutoff):
  return iterdir(bench, cfg, iter) + (kszslimpkl % (guide_bench, cutoff))

def objAddrInfoPkl(bench, cfg, iter=0, hot_cutoff=None, style="REF_CUTOFF"):
  if style == "REF_CUTOFF":
    if hot_cutoff:
      return iterdir(bench, cfg, iter) + (oaipkl%("%1.2f"%hot_cutoff))
    return iterdir(bench, cfg, iter) + (oaipkl%"all")
  elif style == "RPB_CUTOFF":
    if hot_cutoff:
      return iterdir(bench, cfg, iter) + (oairpbpkl%("%1.2f"%hot_cutoff))
    return iterdir(bench, cfg, iter) + (oairpbpkl%"all")
  elif style == "KNAPSACK":
    return iterdir(bench, cfg, iter) + (oaikspkl%("%1.2f"%hot_cutoff))
  else:
    print "invalid style: %s" % style
    raise SystemExit(1)
  return None

def objFieldInfoPkl(bench, cfg, iter=0):
  return iterdir(bench, cfg, iter) + ofipkl

def csgcReportFile(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + csgcrepfile

def activeSetValReport(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + asvrfile

def redObjInfo(bench, cfg, iter):
  return iterdir(bench, cfg, iter) + redobjinfo

def shmbdir(bench):
  return shmroot + ("%s/" % bench)

def shmbcdir(bench, cfg):
  return shmbdir(bench) + ("%s/" % cfg)

def shmdir(bench, cfg, iter):
  return shmbcdir(bench,cfg) + (iterd % iter)

def shmObjDict(bench, cfg, iter):
  return shmdir(bench,cfg,iter) + shmobjdict

def shmHotDict(bench, cfg, iter):
  return shmdir(bench,cfg,iter) + shmhotdict

def shmLivesDir(bench, cfg, iter):
  return shmdir(bench, cfg, iter) + shmlivesdir

def shmLives(bench, cfg, iter, val):
  return shmLivesDir(bench, cfg, iter) + (shmlives % val)

#############################################################################
# parallel run style results directories
#############################################################################
def parcfgdir(pcfg):
  return parresultsdir + "%s/" % pcfg

def piterdir(pcfg, iter):
  return parcfgdir(pcfg) + (iterd % iter)

def pexpdir(pcfg, iter, run):
  bench = parcfgs[pcfg][run][0]
  jcfg  = parcfgs[pcfg][run][1]
  return piterdir(pcfg, iter) + "%s-%s-r%d/" % (bench, jcfg, run)

def rawDataDirPar(pcfg, iter, run):
  return pexpdir(pcfg, iter, run) + rdatad

def emonEventsFilePar(pcfg, iter, run):
  return rawDataDirPar(pcfg, iter, run) + emonevtsfile

def emonRawFilePar(pcfg, iter, run):
  return rawDataDirPar(pcfg, iter, run) + emonrawfile

def pgovRawFilePar(pcfg, iter, run):
  return rawDataDirPar(pcfg, iter, run) + pgovrawfile

def rawoutfilepar(pcfg, iter, run):
  return pexpdir(pcfg, iter, run) + rawout

def cmdoutfilepar(pcfg, iter, run):
  return pexpdir(pcfg, iter, run) + cmdout

def emonSummaryViewFilePar(pcfg, iter, run):
  return rawDataDirPar(pcfg, iter, run) + esumviewfile

def emonSocketViewFilePar(pcfg, iter, run):
  return rawDataDirPar(pcfg, iter, run) + esockviewfile

def emonDashvFilePar(pcfg, iter, run):
  return rawDataDirPar(pcfg, iter, run) + edashvfile

def emonDashMFilePar(pcfg, iter, run):
  return rawDataDirPar(pcfg, iter, run) + edashMfile

def pgovRawFilePar(pcfg, iter, run):
  return rawDataDirPar(pcfg, iter, run) + pgovrawfile

def ppEmonMetricFilePar(pcfg, iter, run, metric):
  return rawDataDirPar(pcfg, iter, run) + \
         (ppemfile % (metric.lower()))

def scratchdirpar(pcfg, iter, run):
  return pexpdir(pcfg, iter, run) + scratchd

def customDir(bench):
  return (customsdir + ("%s/" % bench))

#############################################################################
# miscellaneous values
#############################################################################
defiters            = 10
defminiters         = 8
defvnum             = 100
defrefcut           = 0.10
defcutoff           = 0.10
V0                  = 0
V1                  = 1
V2                  = 2
#gctrays             = "10,11"
gctrays             = "14,15"
gcnewsize           = str((10*(1024*1024*1024)))
#gcnewsize           = str((2*(1024*1024*1024)))
gcminheap           = "12g"
gcmaxheap           = "14g"
longiters           = 12
defobjrate          = 2000
#defobjrate          = 40000
defagethresh        = -1
defrefthresh        = -1
#defdaciters         = 20
defdaciters         = 1
steadyiters         = 11
xssiters            = 101
unixtimefmt         = "elapsed: %e rss: %M"
jgf_run_time        = 240
jgf_warmup_time     = 60
#jgf_run_time        = 30
#jgf_warmup_time     = 30
jvm2008_warmup_time = 60
bgprocs             = []
rootAccess          = True

APP_TOTAL_REC = -1
UNKNOWN_REC   = -2
SUM_TOTAL_REC = -3

OAR_MARKER   =  0xDEFECA7E
EMPTY_MARKER = -0xDEADBEEF
PAGE_SHIFT = 12
PAGE_SIZE  = 1 << PAGE_SHIFT

SMALL = "small"
LARGE = "large"

LOW  = 0
HIGH = 1

#############################################################################
# benchmarks
#############################################################################
jvm2008_startups = [
  'startup.helloworld',
  'startup.compiler.compiler',
  'startup.compiler.sunflow',
  'startup.compress',
  'startup.crypto.aes',
  'startup.crypto.rsa',
  'startup.crypto.signverify',
  'startup.mpegaudio',
  'startup.scimark.fft',
  'startup.scimark.lu',
  'startup.scimark.monte_carlo',
  'startup.scimark.sor',
  'startup.scimark.sparse',
  'startup.serial',
  'startup.sunflow',
  'startup.xml.transform',
  'startup.xml.validation'
]

jvm2008_defs = [
  'compiler.compiler',
  'compiler.sunflow',
  'compress',
  'crypto.aes',
  'crypto.rsa',
  'crypto.signverify',
  'serial',
  'derby',
  'sunflow',
  'mpegaudio',
  'xml.transform',
  'xml.validation'
]

scismalls = [ 'scimark.fft.small',
              'scimark.lu.small',
              'scimark.sor.small',
              'scimark.sparse.small',
              'scimark.monte_carlo'
            ]

scilarges = [ 'scimark.fft.large',
              'scimark.lu.large',
              'scimark.sor.large',
              'scimark.sparse.large',
            ]

scihuges =  [ 'scimark.fft.huge',
              'scimark.lu.huge',
              'scimark.sor.huge',
              'scimark.sparse.huge',
            ]

#scilargebt1s  = [ 'scimark.fft.large-bt1',
#                  'scimark.lu.large-bt1',
#                  'scimark.sor.large-bt1',
#                  'scimark.sparse.large-bt1',
#                  'scimark.monte_carlo-bt1'
#                ]
#
#scilargebt2s  = [ 'scimark.fft.large-bt2',
#                  'scimark.lu.large-bt2',
#                  'scimark.sor.large-bt2',
#                  'scimark.sparse.large-bt2',
#                  'scimark.monte_carlo-bt2'
#                ]
#
#scilargebt4s  = [ 'scimark.fft.large-bt4',
#                  'scimark.lu.large-bt4',
#                  'scimark.sor.large-bt4',
#                  'scimark.sparse.large-bt4',
#                  'scimark.monte_carlo-bt4'
#                ]
#
#scihugebt1s   = [ 'scimark.fft.huge-bt1',
#                  'scimark.lu.huge-bt1',
#                  'scimark.sor.huge-bt1',
#                  'scimark.sparse.huge-bt1',
#                ]
#
#scihugebt2s   = [ 'scimark.fft.huge-bt2',
#                  'scimark.lu.huge-bt2',
#                  'scimark.sor.huge-bt2',
#                  'scimark.sparse.huge-bt2',
#                ]
#
#scihugebt4s   = [ 'scimark.fft.huge-bt4',
#                  'scimark.lu.huge-bt4',
#                  'scimark.sor.huge-bt4',
#                  'scimark.sparse.huge-bt4',
#                ]

scimarks = scismalls + scilarges + scihuges
jvm2008s = jvm2008_startups + jvm2008_defs + scimarks

jgf_singles = [
  'euler-A',
  'euler-B',
  'euler-J',
  'heapsort-A',
  'heapsort-B',
  'heapsort-C',
  'heapsort-J',
  'search-A',
  'search-B',
  # no way to make search larger
]

jgf_multis = [
  'crypt-A',
  'crypt-B',
  'crypt-C',
  'crypt-J',
  'moldyn-A',
  'moldyn-B',
  'moldyn-J',
  'montecarlo-A',
  'montecarlo-B',
  # montecarlo is already big engough
  'raytracer-A',
  'raytracer-B',
  'raytracer-J',
  'raytracer-L',
  'series-A',
  'series-B',
  'series-C',
  # series takes too long even with size C
]

jgfSizes = {
  'A' : 0,
  'B' : 1,
  'C' : 2,
  'J' : 3,
  'K' : 90,
  'L' : 91,
}

jvm2008bts  = []
scimarkbts  = []
jgfmultibts = []
jvm2008btd  = {}
scimarkbtd  = {}
jgfmultibtd = {}

bts = [1, 2, 4, 8, 16, 32]
def btbench(bench, bt):
  return (bench + ('-bt%d'%bt))

for bt in bts:
  jvm2008btd[bt]  = []
  scimarkbtd[bt]  = []
  jgfmultibtd[bt] = []
  for bench in (jvm2008_defs+scimarks+jgf_multis):
    tbench = btbench(bench,bt)
    if bench in jvm2008s:
      jvm2008s.append(tbench)
      if bench in jvm2008_defs:
        jvm2008bts.append(tbench)
        jvm2008btd[bt].append(tbench)
      if bench in scimarks:
        scimarkbts.append(tbench)
        scimarkbtd[bt].append(tbench)
    elif bench in jgf_multis:
      jgfmultibts.append(tbench)
      jgfmultibtd[bt].append(tbench)

jgf_multis += jgfmultibts

jgfs = jgf_singles + jgf_multis

#candidate_jgfs = [
#  # single-threaded
#  'crypt-C-bt1',
#  'crypt-J-bt1',
#  'moldyn-B-bt1',
#  'moldyn-J-bt1',
#  'montecarlo-B-bt1',
#  'raytracer-B-bt1',
#  'raytracer-J-bt1',
#  'series-C-bt1',
#  'euler-B',
#  'euler-J',
#  'heapsort-C',
#  'heapsort-J',
#  'search-B',
#  # four threads
#  'crypt-C-bt4',
#  'crypt-J-bt4',
#  'moldyn-B-bt4',
#  'moldyn-J-bt4',
#  'montecarlo-B-bt4',
#  'raytracer-B-bt4',
#  'raytracer-J-bt4',
#  'series-C-bt4',
#  # sixteen threads
#  'crypt-C-bt16',
#  'crypt-J-bt16',
#  'moldyn-B-bt16',
#  'moldyn-J-bt16',
#  'montecarlo-B-bt16',
#  'raytracer-B-bt16',
#  'raytracer-J-bt16',
#  'series-C-bt16',
#]

#candidate_scis = [
#  'scimark.fft.large-bt1',
#  'scimark.lu.large-bt1',
#  'scimark.sor.large-bt1',
#  'scimark.sparse.large-bt1',
#  'scimark.monte_carlo-bt1',
#  'scimark.fft.large-bt4',
#  'scimark.lu.large-bt4',
#  'scimark.sor.large-bt4',
#  'scimark.sparse.large-bt4',
#  'scimark.monte_carlo-bt4',
#  'scimark.fft.large-bt16',
#  'scimark.lu.large-bt16',
#  'scimark.sor.large-bt16',
#  'scimark.sparse.large-bt16',
#  'scimark.monte_carlo-bt16',
#]

profile_benches = [
  'euler-A',
  #'euler-B',
  'heapsort-A',
  #'heapsort-B',
  #'heapsort-C',
  'search-A',
  'crypt-A-bt1',
  'moldyn-A-bt1',
  #'moldyn-B-bt1',
  #'moldyn-C-bt1',
  'raytracer-A-bt1',
  'series-A-bt1',
  'montecarlo-A-bt1',
  'scimark.monte_carlo-bt1'
] + [x for x in scimarkbtd[1] if 'small' in x] + jvm2008btd[1]

#candidate_benches = [
#  'euler-B',
#  'euler-J',
#  'heapsort-C',
#  'heapsort-J',
#  'search-B',
#  'crypt-C-bt16',
#  'crypt-J-bt16',
#  'moldyn-B-bt16',
#  'moldyn-J-bt16',
#  'raytracer-B-bt16',
#  'raytracer-J-bt16',
#  'series-C-bt16',
#  'montecarlo-B-bt16',
#  'scimark.monte_carlo',
#] + scilarges + jvm2008_defs

candidate_benches = [
  'scimark.monte_carlo',
] + scilarges + jvm2008_defs

#mybenches = [
#  'euler-J',
#  'heapsort-J',
#  'moldyn-J-bt16',
#  'compress',
#] + scilarges

badtunes = [ x for x in jgf_multis if 'montecarlo' in x ]
#badtunes = []

dacapo_smalls  = ['avrora-small',
                  'batik-small',
                  'fop-small',
                  'eclipse-small',
                  'h2-small',
                  'jython-small',
                  'luindex-small',
                  'lusearch-small',
                  'pmd-small',
                  'sunflow-small',
                  'tomcat-small',
                  'tradebeans-small',
                  'tradesoap-small',
                  'xalan-small',
                 ]

dacapo_defs  = [ 'avrora-default',
                 'batik-default',
                 'fop-default',
                 'eclipse-default',
                 'h2-default',
                 'jython-default',
                 'luindex-default',
                 'lusearch-default',
                 'pmd-default',
                 'sunflow-default',
                 'tomcat-default',
                 'tradebeans-default',
                 'tradesoap-default',
                 'xalan-default',
               ]

dacapo_larges  = [ 'avrora-large',
                   'batik-large',
                   #'fop-large',
                   'fop-default',
                   'eclipse-large',
                   'h2-large',
                   'jython-large',
                   #'luindex-large',
                   'luindex-default',
                   'lusearch-large',
                   'pmd-large',
                   'sunflow-large',
                   'tomcat-large',
                   'tradebeans-large',
                   'tradesoap-large',
                   'xalan-large',
                 ]

#storebenches = [
#  'StoreBench-tiny',
#  'StoreBench-t1-d0',
#  'StoreBench-t2-d0',
#  'StoreBench-t4-d0',
#  'StoreBench-t8-d0',
#  'StoreBench-t15-d0',
#  'StoreBench-t16-d0',
#  'StoreBench-t16-d50',
#  'StoreBench-t16-d100',
#  'StoreBench-t16-d150',
#  'StoreBench-t16-d175',
#  'StoreBench-t16-d200',
#  'StoreBench-t16-d225',
#  'StoreBench-t16-d250',
#  'StoreBench-t16-d300',
#  'StoreBench-t16-d500',
#  'StoreBench-t16-d1000',
#  'StoreBench-bw-d0',
#  'StoreBench-bw-d50',
#  'StoreBench-bw-d75',
#  'StoreBench-bw-d80',
#  'StoreBench-bw-d100',
#  'StoreBench-bw-d250',
#  'StoreBench-bw-d500',
#  'StoreBench-bw-d1000',
#  'StoreBench-bw-d2000',
#  'StoreBench-bw-d5000',
#  'StoreBench-bw-d10000',
#  'StoreBench-xx',
#  'StoreBench-xx-d0',
#  'StoreBench-xx-d50',
#  'StoreBench-xx-d60',
#  'StoreBench-xx-d70',
#  'StoreBench-xx-d75',
#  'StoreBench-xx-d80',
#  'StoreBench-xx-d90',
#  'StoreBench-xx-d100',
#  'StoreBench-xx-d150',
#  'StoreBench-xx-d175',
#  'StoreBench-xx-d200',
#  'StoreBench-xx-d225',
#  'StoreBench-xx-d250',
#  'StoreBench-xx-d300',
#  'StoreBench-xx-d500',
#  'StoreBench-xx-d1000',
#]
#
#customs = [ 'HelloWorld-default',
#            'MemBench-t1',
#            'MemBench-t2',
#            'MemBench-t4',
#            'MemBench-t8',
#            'MemBench-t15',
#            'MemBench-t16',
#            'MemBench-tiny',
#            'MemBench-small',
#            'MemBench-large',
#            'LoadBench-large',
#            'LoadBench-small',
#            'LoadBench-small-t1',
#            'LoadBench-small-t2',
#            'LoadBench-small-t4',
#            'LoadBench-small-t8',
#            'LoadBench-small-t16',
#            'LoadBench-small-test',
#            'LoadBench-large-test',
#            'LoadBench-large-t1',
#            'LoadBench-large-t2',
#            'LoadBench-large-t4',
#            'LoadBench-large-t8',
#            'LoadBench-large-t16',
#          ] + storebenches

storebenches = [
  'StoreBench-tiny',
  'StoreBench-dxx',
  'StoreBench-d0',
  'StoreBench-d100',
  'StoreBench-d150',
  'StoreBench-d200',
  'StoreBench-d250',
  'StoreBench-d300',
  'StoreBench-d350',
  'StoreBench-d400',
  'StoreBench-d500',
  'StoreBench-d750',
  'StoreBench-d1000',
  'StoreBench-d2000',
]

bwp_benches = [
  'BandwidthBench-p1.0',
  'BandwidthBench-p0.99',
  'BandwidthBench-p0.98',
  'BandwidthBench-p0.95',
  'BandwidthBench-p0.9',
  'BandwidthBench-p0.85',
  'BandwidthBench-p0.8',
  'BandwidthBench-p0.75',
  'BandwidthBench-p0.7',
  'BandwidthBench-p0.6',
  'BandwidthBench-p0.5',
]

bwxp_benches = [
  'BandwidthBench-xp1.0',
  'BandwidthBench-xp0.99',
  'BandwidthBench-xp0.98',
  'BandwidthBench-xp0.95',
  'BandwidthBench-xp0.9',
  'BandwidthBench-xp0.85',
  'BandwidthBench-xp0.8',
  'BandwidthBench-xp0.75',
  'BandwidthBench-xp0.7',
  'BandwidthBench-xp0.6',
  'BandwidthBench-xp0.5',
]

simplebb_benches = [
  'SimpleBB-tiny',
  'SimpleBB-p1.0',
  'SimpleBB-p0.99',
  'SimpleBB-p0.98',
  'SimpleBB-p0.95',
  'SimpleBB-p0.9',
  'SimpleBB-p0.85',
  'SimpleBB-p0.8',
  'SimpleBB-p0.75',
  'SimpleBB-p0.7',
  'SimpleBB-p0.6',
  'SimpleBB-p0.5',
]

bw_benches = [ 'BandwidthBench-x1.0'
             ] + bwp_benches + bwxp_benches

customs = [ 'HelloWorld-default',
          ] + storebenches + bw_benches + simplebb_benches

idles = [ 'idle-30', 'idle-60', 'idle-100' ]

allowFails = [ 'h2-default', 'h2-large' ]

dacapos = dacapo_smalls + dacapo_defs + dacapo_larges

allbenches = jvm2008s + dacapos + customs + idles + jgf_multis
defbenches = allbenches

#############################################################################
# java options
#############################################################################
UseNUMA                  = "UseNUMA"
UseNUMAInterleave        = "UseNUMAInterleave"
UseColoredSpaces         = "UseColoredSpaces"
ApplyColorToSpaces       = "ApplyColorToSpaces"
MColorColoredSpacePages  = "MColorColoredSpacePages"
RedMemoryTrays           = "RedMemoryTrays"
BlueMemoryTrays          = "BlueMemoryTrays"
UnknownObjectColor       = "UnknownObjectColor"
UnknownAPColor           = "UnknownAPColor"
EdenAlwaysRed            = "EdenAlwaysRed"
RedMemoryInterleave      = "RedMemoryInterleave"
BlueMemoryInterleave     = "BlueMemoryInterleave"
RandomHeapColors         = "RandomHeapColors"
RedObjectRatio           = "RedObjectRatio"
ParallelGCThreads        = "ParallelGCThreads"
ColoredSpaceStats        = "ColoredSpaceStats"
MColorNUMA               = "MColorNUMA"
HeapColor                = "HeapColor"
FreeNUMASpacePages       = "FreeNUMASpacePages"
MakeNUMASpaceLocal       = "MakeNUMASpaceLocal"
TenuredGenTrays          = "TenuredGenTrays"
PermGenTrays             = "PermGenTrays"
CodeCacheTrays           = "CodeCacheTrays"
PrintHeapAtGC            = "PrintHeapAtGC"
PrintGCDetails           = "PrintGCDetails"
TimeStampGC              = "TimeStampGC"
UseSerialGC              = "UseSerialGC"
NewSize                  = "NewSize"
OldSize                  = "OldSize"
PowerSampleGC            = "PowerSampleGC"
MaxGCTimeRatio           = "MaxGCTimeRatio"
MinSurvivorRatio         = "MinSurvivorRatio"
InitialSurvivorRatio     = "InitialSurvivorRatio"
RedHeap                  = "RedHeap"
FreeColoredSpacePages    = "FreeColoredSpacePages"

ProfileObjectInfo           = "ProfileObjectInfo"
PrintObjectInfoAtInterval   = "PrintObjectInfoAtInterval"
PrintAPInfoAtInterval       = "PrintAPInfoAtInterval"
PrintKRInfoAtInterval       = "PrintKRInfoAtInterval"
PrintStackSamples           = "PrintStackSamples"
StackHeapSampleRatio        = "StackHeapSampleRatio"
PrintThreadTimes            = "PrintThreadTimes"
TrimObjectInfo              = "TrimObjectInfo"
TimeObjectInfoPrinting      = "TimeObjectInfoPrinting"
ObjectInfoInterval          = "ObjectInfoInterval"
ObjectAddressInfoInterval   = "ObjectAddressInfoInterval"
UseCompressedOops           = "UseCompressedOops"
OnlyTenuredObjectInfo       = "OnlyTenuredObjectInfo"
ObjectInfoLog               = "ObjectInfoLog"
ObjectAllocationLog         = "ObjectAllocationLog"
DeadObjectLog               = "DeadObjectLog"
AllocPointMapLog            = "AllocPointMapLog"
AllocPointInfoLog           = "AllocPointInfoLog"
AllocPointInfoBin           = "AllocPointInfoBin"
ObjectAddressInfoLog        = "ObjectAddressInfoLog"
ObjectAddressInfoBin        = "ObjectAddressInfoBin"
StackSampleLog              = "StackSampleLog"
MethodKlassAccessListsLog   = "MethodKlassAccessListsLog"
KlassRecordInfoLog          = "KlassRecordInfoLog"
KlassRecordInfoBin          = "KlassRecordInfoBin"
KlassMapLog                 = "KlassMapLog"
CompileCommandFile          = "CompileCommandFile"
TotalRefCounts              = "TotalRefCounts"
PrintTextAPInfo             = "PrintTextAPInfo"
PrintTextKRInfo             = "PrintTextKRInfo"
CrashOnObjectInfoDump       = "CrashOnObjectInfoDump"
PrintObjectInfoBeforeFullGC = "PrintObjectInfoBeforeFullGC"
PrintObjectInfoAfterFullGC  = "PrintObjectInfoAfterFullGC"
OrganizeObjects             = "OrganizeObjects"
ColorObjectAllocations      = "ColorObjectAllocations"
ObjectLayoutInterval        = "ObjectLayoutInterval"
ColorAgeThreshold           = "ColorAgeThreshold"
ProfileAgeThreshold         = "ProfileAgeThreshold"
ColorRefThreshold           = "ColorRefThreshold"
SurvivorsAlwaysBlue         = "SurvivorsAlwaysBlue"
SurvivorsAlwaysRed          = "SurvivorsAlwaysRed"
HotObjectThreshold          = "HotObjectThreshold"
CountCompiledCalls          = "CountCompiledCalls"
MethodHistogramCutoff       = "MethodHistogramCutoff"
PrintClassHistogram         = "PrintClassHistogram"
CompileThreshold            = "CompileThreshold"
BackEdgeThreshold           = "BackEdgeThreshold"
CICompilerCount             = "CICompilerCount"
UseVMIndication             = "UseVMIndication"
VMIndicationThreshold       = "VMIndicationThreshold"
SpecifiedCompilationLevels  = "SpecifiedCompilationLevels"
LoadReplayCounters          = "LoadReplayCounters"
ResetMethodCounters         = "ResetMethodCounters"
UseOnStackReplacement       = "UseOnStackReplacement"
PrintCompilation            = "PrintCompilation"
PrintOracleDiff             = "PrintOracleDiff"
SubmitDualCompiles          = "SubmitDualCompiles"
TCStartupIteration          = "TCStartupIteration"
CompilationPolicyChoice     = "CompilationPolicyChoice"
UseCompiler                 = "UseCompiler"
NeverCompile                = "NeverCompile"
PrintThreadTimes            = "PrintThreadTimes"
BackgroundCompilation       = "BackgroundCompilation"
InterpretModeLoopCounts     = "InterpretModeLoopCounts"
InitialTenuringThreshold    = "InitialTenuringThreshold"
MaxTenuringThreshold        = "MaxTenuringThreshold"
PrintTenuringDistribution   = "PrintTenuringDistribution"
UseAdaptiveSizePolicy       = "UseAdaptiveSizePolicy"
DisableMajorGC              = "DisableMajorGC"
PreventTenuring             = "PreventTenuring"
PrintReferencedObjects      = "PrintReferencedObjects"
MemBenchOrganize            = "MemBenchOrganize"
NewRatio                    = "NewRatio"
SurvivorRatio               = "SurvivorRatio"
NewSize                     = "NewSize"
MaxNewSize                  = "MaxNewSize"
GCTimers                    = "GCTimers"
CompileSlowAllocations      = "CompileSlowAllocations"
SlowAllocations             = "SlowAllocations"
OnlyOrganizeGC              = "OnlyOrganizeGC"
OnlyIntervalGC              = "OnlyIntervalGC"
PrintKlassAccessLists       = "PrintKlassAccessLists"
JustDoIt                    = "JustDoIt"
TieredCompilation           = "TieredCompilation"
Inline                      = "Inline"
Tier4InvocationThreshold    = "Tier4InvocationThreshold"
Tier4MinInvocationThreshold = "Tier4MinInvocationThreshold"
Tier4CompileThreshold       = "Tier4CompileThreshold" 
Tier4BackEdgeThreshold      = "Tier4BackEdgeThreshold"
CompileTheWorld             = "CompileTheWorld"
UseParallelGC               = "UseParallelGC"

ProfileObjectAddressInfo           = "ProfileObjectAddressInfo"
ProfileObjectFieldInfo             = "ProfileObjectFieldInfo"
PrintObjectAddressInfoAtInterval   = "PrintObjectAddressInfoAtInterval"
PrintObjectAddressInfoAtGC         = "PrintObjectAddressInfoAtGC"
UseTLAB                            = "UseTLAB"
ResizeTLAB                         = "ResizeTLAB"
TLABSize                           = "TLABSize"

MethodSampleColors              = "MethodSampleColors"
MethodSamplerInterval           = "MethodSamplerInterval"
CoolDownInterval                = "CoolDownInterval"
HotKlassOrganize                = "HotKlassOrganize"
HotMethodAllocate               = "HotMethodAllocate"
HotKlassAllocate                = "HotKlassAllocate"
CoolDownFactor                  = "CoolDownFactor"
MaxMethodTemperature            = "MaxMethodTemperature"
MaxKlassTemperature             = "MaxKlassTemperature"
ScavengeAtRegularIntervals      = "ScavengeAtRegularIntervals"
ScavengeInterval                = "ScavengeInterval"
SampleCallStacksAtInterval      = "SampleCallStacksAtInterval"
SampleCallStacksContinuous      = "SampleCallStacksContinuous"
CallStackSampleInterval         = "CallStackSampleInterval"

MethodDataRecord   = "MethodDataRecord"
MethodDataReplay   = "MethodDataReplay"
NullReplayData     = "NullReplayData"
ReplayDataLogFile  = "ReplayDataLogFile"
ReplayDataBinFile  = "ReplayDataBinFile"
ReplayOopsFile     = "ReplayOopsFile"

Tier3InvocationThreshold    = "Tier3InvocationThreshold"
Tier3MinInvocationThreshold = "Tier3MinInvocationThreshold"
Tier3CompileThreshold       = "Tier3CompileThreshold"
Tier3BackEdgeThreshold      = "Tier3BackEdgeThreshold"
Tier4InvocationThreshold    = "Tier4InvocationThreshold"
Tier4MinInvocationThreshold = "Tier4MinInvocationThreshold"
Tier4CompileThreshold       = "Tier4CompileThreshold"
Tier4BackEdgeThreshold      = "Tier4BackEdgeThreshold"

Default_Tier3InvocationThreshold    = 200
Default_Tier3MinInvocationThreshold = 100
Default_Tier3CompileThreshold       = 2000
Default_Tier3BackEdgeThreshold      = 60000
Default_Tier4InvocationThreshold    = 5000
Default_Tier4MinInvocationThreshold = 600
Default_Tier4CompileThreshold       = 15000
Default_Tier4BackEdgeThreshold      = 40000

Default_CompileThreshold            = 10000
Default_BackEdgeThreshold           = 100000

def optionOn(opt):
  return "-XX:+%s" % opt

def optionOff(opt):
  return "-XX:-%s" % opt

def optionSet(opt,val):
  return "-XX:%s=%s" % (opt, val)

def optionGet(opt):
  matched = re.match(r'-XX:(.+)=(.+)', opt)
  if matched != None:
    return matched.groups()
  else:
    print 'opt %s has a bad format' %s
    exit(0)
  
def minHeapSize(val):
  return "-Xms%s" % val

def maxHeapSize(val):
  return "-Xmx%s" % val

def interpreterMode():
  return "-Xint"

def KB(n):
  return (n<<10)

def MB(n):
  return (n<<20)

def GB(n):
  return (n<<30)

def B2KB(n):
  return (int(n)>>10)

def ratstr(r):
  return ("N/A" if r < -0.01 else ("%5.4f" % r))

def to_bytes(str):
  bytes = None
  if str.lower().endswith('g'):
    bytes = GB(int(str.lower().strip('g')))
  elif str.lower().endswith('m'):
    bytes = MB(int(str.lower().strip('m')))
  elif str.lower().endswith('k'):
    bytes = KB(int(str.lower().strip('k')))
  if bytes == None:
    bytes = int(str)
  return bytes

#############################################################################
# jvm2008 parameter options
#############################################################################
specjvmHomeDir         = "specjvm.home.dir"
specjvmResDir          = "specjvm.result.dir"
specjvmRawDataDir      = "specjvm.raw.data.dir"
specjvmEmonEventsFile  = "specjvm.emon.events.file"
specjvmRunInitialCheck = "specjvm.run.initial.check"
specjvmRunEmon         = "specjvm.run.emon"
specjvmRunIostat       = "specjvm.run.iostat"
specjvmRunSar          = "specjvm.run.sar"
specjvmRunPowerGov     = "specjvm.run.power_gov"
specjvmPowerGovFreq    = "specjvm.power_gov.rate"

def specjvmOpt(opt, val):
  return "-D%s=%s" % (opt, val)

def specjvmOptTrue(opt):
  return "-D%s=True" % opt

def specjvmOptFalse(opt):
  return "-D%s=False" % opt

#############################################################################
# dacapo parameter options
#############################################################################
def scratchdiropt(dir):
  return "--scratch-directory=%s" % dir

def iteropt(n):
  return "-n %d" % n

#############################################################################
# miscellaneous options
#############################################################################
def opt(opt,arg):
  return "--%s=%s" % (opt,arg)

#############################################################################
# named configurations
# cfgs values are a tuples with keys for the run and profile cfgs
#############################################################################
DEFAULT             = "default"
DEFAULT_POWER       = "default_power"
NUMA                = "numa"
MCOLOR_NUMA         = "mcolor_numa"
MCOLOR_DEFAULT      = "mcolor_default"
COLORED_SPACES      = "colored_spaces"
COLORED_SPACES_X    = "colored_spaces_x"
ORGANIZE_OBJECTS    = "organize_objects"
COLOR_ALLOCATIONS   = "color_allocations"

DEFAULT_ALLOCATIONS = "default_allocations"
EDEN_ALWAYS_RED     = "eden_always_red"
EDEN_ALWAYS_RED_A   = "eden_always_red_a"
EDEN_ALWAYS_RED_X   = "eden_always_red_x"
EDEN_ALWAYS_RED_Y   = "eden_always_red_y"
EDEN_ALWAYS_RED_Z   = "eden_always_red_z"
RED_OBJECTS         = "red_objects"
BLUE_OBJECTS        = "blue_objects"
OBJ_ORG_PROFILE     = "obj_org_profile"
OBJ_ORG_ATP         = "oop_org_atp"
OOP_XXX_RT0         = "oop_xxx_rt0"
OOP_AT0_RT0         = "oop_at0_rt0"
OBJ_RED_PROFILE     = "obj_red_profile"
OBJ_BLUE_PROFILE    = "obj_blue_profile"
FULL_HPROF          = "full_hprof"
EMON_HPROF          = "emon_hprof"
POWER_GOV_HPROF     = "power_gov_hprof"
FULL_OPROF          = "full_oprof"
EMON_OPROF          = "emon_oprof"
POWER_GOV_OPROF     = "power_gov_oprof"
LONG_RUN            = "long_run"
DEFAULT_POWER_GOV   = "default_power_gov"
DEFAULT_FULL        = "default_full"
NUMA_NO_FREE        = "numa_no_free"
NUMA_NO_MAKE_LOCAL  = "numa_no_make_local"
MCOLOR_NUMA_NO_FREE = "mcolor_numa_no_free"
NUMANI              = "numani"
MCOLOR_NUMANI       = "mcolor_numani"
DEFAULT_EMON        = "default_emon"
OBJ_INFO_DEFAULT    = "obj_info_default"
OBJ_INFO_INTERVAL   = "obj_info_interval"
OBJ_INFO_SIM        = "obj_info_sim"
OBJ_INFO_XSIM       = "obj_info_xsim"
OBJ_INFO_SHSIM      = "obj_info_shsim"
OBJ_INFO_XSHSIM     = "obj_info_xshsim"
OBJ_INFO_VXSIM      = "obj_info_vxsim-%d"
OBJ_INFO_NVXSIM     = "obj_info_nvxsim-%d"
METHOD_KAL_INFO     = "method_kal_info"
METHOD_KAL_INFOX    = "method_kal_infox"
METHOD_KAL_INFONX   = "method_kal_infonx"
OBJ_INFO_ORGANIZE   = "obj_info_organize"
OBJ_INFO_GUIDED     = "obj_info_guided"
OBJ_INFO_AGE_THRESH = "obj_info_age_thresh"
OBJ_INFO_AT0_RT0    = "obj_info_at0_rt0"
OBJ_INFO_XXX_RT0    = "obj_info_xxx_rt0"
OBJ_INFO_TENURED    = "obj_info_tenured"
OBJ_INFO_FULL       = "obj_info_full"
OBJ_INFO_FULL_TEN   = "obj_info_full_ten"
OBJ_VALS_DEFAULT    = "obj_vals_default"
OBJ_VALS_TENURED    = "obj_vals_tenured"
OBJ_GCVALS_DEFAULT  = "obj_gcvals_default"
OBJ_GCVALS_TENURED  = "obj_gcvals_tenured"
XINT                = "xint"
XINT_WBEC           = "xint_wbec"
INTERPRET           = "interpret"
INTERPRET_WBEC      = "interpret_wbec"
XINT_CCF            = "xint_ccf"
TIERED_STEADY_BASE  = "tiered_steady_base"
SERVER_STEADY_BASE  = "server_steady_base"
TIERED_XSS_BASE     = "tiered_xss_base"
SERVER_XSS_BASE     = "server_xss_base"
STEADY_CCF          = "steady_ccf"
STEADY_CCF_REPLAY   = "steady_ccf_replay"
STEADY_REPLAY       = "steady_replay"
STEADY_REPLAY_LC    = "steady_replay_lc"
REPLAY_NO_RESET     = "replay_no_reset"
CCF_ORACLE_DIFF     = "ccf_oracle_diff"
PCM_POWER           = "pcm_power"
PCM_MEMORY          = "pcm_memory"
PCM_SELF_REFRESH    = "pcm_self_refresh"
PCM                 = "pcm"
PCM_ENERGY_MODEL    = "pcm_energy_model"

METHOD_DATA_RECORD  = "method_data_record"
METHOD_DATA_REPLAY  = "method_data_replay"
MD_CT_RECORD        = "md_ct_record_%3.2f"
MD_CT_REPLAY        = "md_ct_replay_%3.2f"
STEADY_MD_CT_RECORD = "steady_md_ct_record_%3.2f"
STEADY_MD_CT_REPLAY = "steady_md_ct_replay_%3.2f"
XSS_MD_CT_RECORD    = "xss_md_ct_record_%3.2f"
XSS_MD_CT_REPLAY    = "xss_md_ct_replay_%3.2f"

MD_RECORD           = "md_n%d_record"
MD_REPLAY           = "md_n%d_replay"

ADDR_TLAB_DEFAULT        = "addr_tlab_default"
ADDR_TLAB_INTERVAL       = "addr_tlab_interval"
ADDR_TLAB_PREGC          = "addr_tlab_pregc"
ADDR_TLAB_XVAL           = "addr_tlab_xval"
FIELD_INFO_INTERVAL      = "field_info_interval"
FIELD_TLAB_INTERVAL      = "field_tlab_interval"

AP_TLAB_INTERVAL         = "ap_tlab_interval"
MINI_AP_TLAB_INTERVAL    = "mini_ap_tlab_interval"
FIELD_TLAB_PREGC         = "field_tlab_pregc"
ADDR_INFO_DEFAULT        = "addr_info_default"
ADDR_INFO_INTERVAL       = "addr_info_interval"
#ADDR_HEAVY_INTERVAL      = "addr_heavy_interval"
MEMBENCH_DEFAULT         = "membench_default"
MEMBENCH_ORGANIZE        = "membench_organize"
MEMBENCH_ILV_ORGANIZE    = "membench_ilv_organize"
MEMBENCH_XILV            = "membench_xilv"
HDEFAULT_KDEFAULT        = "hdefault_kdefault"
HDEFAULT_KMCOLOR         = "hdefault_kmcolor"
HORGANIZE_KMCOLOR        = "horganize_kmcolor"
HORGANIZE_ILV_KMCOLOR    = "horganize_ilv_kmcolor"
HORGANIZE_XILV_KMCOLOR   = "horganize_xilv_kmcolor"
DEF_MEMBENCH_COLOR       = "membench_color_base"
MEMBENCH_COLOR           = "membench_color"
MEMBENCH_COLOR_V2        = "membench_color_v2"
BWBENCH_COLOR            = "bwbench_color"
BWBENCH_COLOR_ILV        = "bwbench_color_ilv"
BWBENCH_DEFAULT          = "bwbench_default"

HEAP_TRIAL               = "heap_trial"
HEAP_TRIAL_LAGOM         = "heap_trial_lagom"
COLOR_ALLOCATIONS_ILV    = "color_allocations_ilv"
DEF_ALLOC_BASE           = "def_alloc_base"
__DEF_ALLOC_BASE         = "__def_alloc_base"
HDKM_LOW_POWER           = "hdkm_low_power"
HDKM_ALLOC_BASE          = "hdkm_alloc_base"
HDKM_ALLOC_BASE_XX       = "hdkm_alloc_base_xx"
HDKM_ALLOC_BASE_V2       = "hdkm_alloc_base_v2"
HDKM_ALLOC_TUNE          = "hdkm_alloc_tune"
HDKM_ALLOC_TUNE_XX       = "hdkm_alloc_tune_xx"
HDKM_ALLOC_TUNE_V2       = "hdkm_alloc_tune_v2"
HDKD_ALLOC_BASE          = "hdkd_alloc_base"
HDKD_ALLOC_BASE_V2       = "hdkd_alloc_base_v2"
HDKD_ALLOC_TUNE          = "hdkd_alloc_tune"
HDKD_ALLOC_TUNE_XX       = "hdkd_alloc_tune_xx"
HDKD_CHAN_ILV            = "hdkd_chan_ilv"
ONLINE_COLOR_TEST        = "online_color_test"
ONLINE_COLOR_TUNE        = "online_color_tune"
ONLINE_COLOR_TUNE_HKO    = "online_color_tune_hko"
ONLINE_COLOR_TUNE_HKA    = "online_color_tune_hka"
OCT_HKA_MS10_CDF10       = "oct_hka_ms10_cdf10"
OCT_MS10_CD100           = "oct_ms10_cd100"
OCT_MS100_CD250          = "oct_ms100_cd250"
OCT_MS50_CD500           = "oct_ms50_cd500"
OCT_HKO_MS10_CD100       = "oct_hko_ms10_cd100"
OCT_HKO_MS100_CD250      = "oct_hko_ms100_cd250"
OCT_HKO_MS50_CD500       = "oct_hko_ms50_cd500"
OCT_HKA_MS10_CD100       = "oct_hka_ms10_cd100"
OCT_HKA_MS50_CD500       = "oct_hka_ms50_cd500"
OCT_HKA_SCIMARK1         = "oct_hka_scimark1"
OCT_HKA_SCIMARK2         = "oct_hka_scimark2"
OCT_HKA_SCIMARK3         = "oct_hka_scimark3"
#OCT_MS10_CD200           = "oct_ms10_cd200"
#OCT_HKO_MS10_CD50        = "oct_hko_ms10_cd50"
#OCT_HKO_MS10_CD200       = "oct_hko_ms10_cd200"
#OCT_MS50_CD50            = "oct_ms50_cd50"
#OCT_MS50_CD100           = "oct_ms50_cd100"
#OCT_MS50_CD200           = "oct_ms50_cd200"
#OCT_HKO_MS50_CD50        = "oct_hko_ms50_cd50"
#OCT_HKO_MS50_CD100       = "oct_hko_ms50_cd100"
#OCT_HKO_MS50_CD200       = "oct_hko_ms50_cd200"
HDKD_FAST_BASE           = "hdkd_fast_base"
HDKD_FAST_TUNE           = "hdkd_fast_tune"
HOKM_ALLOC_BASE          = "hokm_alloc_base"
HDKM_FAST_TUNE           = "hdkm_fast_tune"
HDKM_FAST_TUNE2          = "hdkm_fast_tune2"
COLOR_ILV_BASE           = "color_ilv_base"
COLOR_ILV_TUNE           = "color_ilv_tune"
COLOR_ILV_TUNE_X         = "color_ilv_tune_x"
COLOR_ILV_TLAB           = "color_ilv_tlab"
COLOR_ILV_TLAB_X         = "color_ilv_tlab_x"
DEFAULT_ALLOCATIONS_A    = "default_allocations_a"
DEFAULT_ALLOCATIONS_X    = "default_allocations_x"
DEFAULT_ALLOCATIONS_Y    = "default_allocations_y"
DEFAULT_ALLOCATIONS_Z    = "default_allocations_z"
HDKM_ALLOCATIONS_A       = "hdkm_allocations_a"
HDKM_ALLOCATIONS_B       = "hdkm_allocations_b"
HDKM_ALLOCATIONS_X       = "hdkm_allocations_x"
HDKM_ALLOCATIONS_Y       = "hdkm_allocations_y"
HDKM_ALLOCATIONS_Z       = "hdkm_allocations_z"
HDKD_ALLOCATIONS_A       = "hdkd_allocations_a"
COLOR_ALLOC_BASE         = "color_alloc_base"
TZ_COLOR_AP              = "tz_color_ap"
TZ_COLOR_AP_98           = "tz_color_ap_98"
TZ_COLOR_AP_95           = "tz_color_ap_95"
COLOR_ALLOC_BASE_X       = "color_alloc_base_x"
COLOR_ALLOC_BASE_V2      = "color_alloc_base_v2"
COLOR_ALLOC_TUNE         = "color_alloc_tune"
COLOR_ALLOC_TUNE_X       = "color_alloc_tune_x"
COLOR_ALLOC_TUNE_V2      = "color_alloc_tune_v2"
COLOR_ALLOCATIONS_A      = "color_allocations_a"
COLOR_ALLOCATIONS_B      = "color_allocations_b"
COLOR_ALLOCATIONS_C      = "color_allocations_c"
COLOR_ALLOCATIONS_X      = "color_allocations_x"
COLOR_ALLOCATIONS_Y      = "color_allocations_y"
COLOR_ALLOCATIONS_Z      = "color_allocations_z"
MEMBENCH_COLOR_ALLOC     = "membench_color_alloc"
SAMPLE_STACKS_VAL        = "sample_stacks_val"
SAMPLE_STACKS_CON        = "sample_stacks_con"
SAMPLE_STACKS_HPROF      = "sample_stacks_hprof"

# numa experiments - turn off numa_interleave
DEFAULT_HEMON                 = "default_hemon"
NUMA_HEMON                    = "numa_hemon"
NUMANI_HEMON                  = "numani_hemon"

DEFAULT_NOILV_HEMON           = "default_noilv_hemon"
NUMA_NOILV_HEMON              = "numa_noilv_hemon"
NUMANI_NOILV_HEMON            = "numani_noilv_hemon"

DEFAULT_HEMON_MCK             = "default_hemon_mck"
NUMA_HEMON_MCK                = "numa_hemon_mck"
NUMANI_HEMON_MCK              = "numani_hemon_mck"
MCOLOR_NUMA_HEMON_MCK         = "mcolor_numa_hemon_mck"
MCOLOR_NUMANI_HEMON_MCK       = "mcolor_numani_hemon_mck"

DEFAULT_NOILV_HEMON_MCK       = "default_noilv_hemon_mck"
NUMA_NOILV_HEMON_MCK          = "numa_noilv_hemon_mck"
NUMANI_NOILV_HEMON_MCK        = "numani_noilv_hemon_mck"
MCOLOR_NUMA_NOILV_HEMON_MCK   = "mcolor_numa_noilv_hemon_mck"
MCOLOR_NUMANI_NOILV_HEMON_MCK = "mcolor_numani_noilv_hemon_mck"

# older experiments
MCOLOR_NUMA_NOILV_EMON       = "mcolor_numa_noilv_emon"
NUMA_NOILV_EMON_MCK          = "numa_noilv_emon_mck"
MCOLOR_NUMA_NOILV_EMON_MCK   = "mcolor_numa_noilv_emon_mck"

DEFAULT_NOILV_PGOV           = "default_noilv_pgov"
NUMA_NOILV_PGOV              = "numa_noilv_pgov"
MCOLOR_NUMA_NOILV_PGOV       = "mcolor_numa_noilv_pgov"

# mscramble
DEFAULT_MSCRAMBLE_EMON_ILV = "default_mscramble_emon_ilv"
DEFAULT_MSCRAMBLE_PGOV_ILV = "default_mscramble_pgov_ilv"
DEFAULT_MSCRAMBLE_FULL_ILV = "default_mscramble_full_ilv"
DEFAULT_MSCRAMBLE_EMON_STK = "default_mscramble_emon_stk"
DEFAULT_MSCRAMBLE_PGOV_STK = "default_mscramble_pgov_stk"
DEFAULT_MSCRAMBLE_FULL_STK = "default_mscramble_full_stk"
DEFAULT_MSCRAMBLE_EMON     = "default_mscramble_emon"
DEFAULT_MSCRAMBLE_PGOV     = "default_mscramble_pgov"
DEFAULT_MSCRAMBLE_FULL     = "default_mscramble_full"
POWER_MSCRAMBLE_EMON       = "power_mscramble_emon"
POWER_MSCRAMBLE_PGOV       = "power_mscramble_pgov"
POWER_MSCRAMBLE_FULL       = "power_mscramble_full"

# other jazz
COLOMBIA_STOCK_DEFAULT   = "colombia_stock_default"
COLOMBIA_STOCK_EMON      = "colombia_stock_emon"
COLOMBIA_STOCK_POWER_GOV = "colombia_stock_power_gov"

COLOMBIA_MCKERN_DEFAULT   = "colombia_stock_default"
COLOMBIA_MCKERN_EMON      = "colombia_stock_emon"
COLOMBIA_MCKERN_POWER_GOV = "colombia_stock_power_gov"

COLOMBIA_NOILV_DEFAULT   = "colombia_noilv_default"
COLOMBIA_NOILV_EMON      = "colombia_noilv_emon"
COLOMBIA_NOILV_POWER_GOV = "colombia_noilv_power_gov"

MCOLOR_NODE1_CHN0   = "mcolor_n1c0"
MCOLOR_NODE1_CHN1   = "mcolor_n1c1"
MCOLOR_NODE1_CHN2   = "mcolor_n1c2"
MCOLOR_NODE1_CHN3   = "mcolor_n1c3"

MCOLOR_NODE1_CHN0_PGOV = "mcolor_n1c0_pgov"
MCOLOR_NODE1_CHN1_PGOV = "mcolor_n1c1_pgov"
MCOLOR_NODE1_CHN2_PGOV = "mcolor_n1c2_pgov"
MCOLOR_NODE1_CHN3_PGOV = "mcolor_n1c3_pgov"

MCOLOR_NODE1_CHN0_FULL = "mcolor_n1c0_full"
MCOLOR_NODE1_CHN1_FULL = "mcolor_n1c1_full"
MCOLOR_NODE1_CHN2_FULL = "mcolor_n1c2_full"
MCOLOR_NODE1_CHN3_FULL = "mcolor_n1c3_full"

TENURED_GEN_N1C3 = "tenured_gen_n1c3"

ECD_MCOLOR_N1C0 = "ecd_mcolor_n1c0"
ECD_MCOLOR_N1C1 = "ecd_mcolor_n1c1"
ECD_MCOLOR_N1C2 = "ecd_mcolor_n1c2"

ECD_MCOLOR_N1C0_EMON = "ecd_mcolor_n1c0_emon"
ECD_MCOLOR_N1C1_EMON = "ecd_mcolor_n1c1_emon"
ECD_MCOLOR_N1C2_EMON = "ecd_mcolor_n1c2_emon"
ECD_MCOLOR_N1C0_PGOV = "ecd_mcolor_n1c0_pgov"
ECD_MCOLOR_N1C1_PGOV = "ecd_mcolor_n1c1_pgov"
ECD_MCOLOR_N1C2_PGOV = "ecd_mcolor_n1c2_pgov"

# power sampling tenured gen experiment
DEFAULT_PSGC          = "default_psgc"
DEFAULT_PSGC_EMON     = "default_psgc_emon"
DEFAULT_PSGC_PGOV     = "default_psgc_pgov"
TEN_GEN_PSGC          = "ten_gen_psgc"
TEN_GEN_PSGC_EMON     = "ten_gen_psgc_emon"
TEN_GEN_PSGC_PGOV     = "ten_gen_psgc_pgov"

# unscrambled power sampling tenured gen experiment
UNSCRAM_PSGC              = "unscram_psgc"
UNSCRAM_PSGC_EMON         = "unscram_psgc_emon"
UNSCRAM_PSGC_PGOV         = "unscram_psgc_pgov"
UNSCRAM_TEN_GEN_PSGC      = "unscram_ten_gen_psgc"
UNSCRAM_TEN_GEN_PSGC_EMON = "unscram_ten_gen_psgc_emon"
UNSCRAM_TEN_GEN_PSGC_PGOV = "unscram_ten_gen_psgc_pgov"

# mscramble tenured gen experiment
MSCRAM_GC_DEFAULT          = "mscram_gc_default"
MSCRAM_GC_DEFAULT_EMON     = "mscram_gc_default_emon"
MSCRAM_GC_DEFAULT_PGOV     = "mscram_gc_default_pgov"
MSCRAM_GC_DEFAULT_MCK      = "mscram_gc_default_mck"
MSCRAM_GC_DEFAULT_EMON_MCK = "mscram_gc_default_emon_mck"
MSCRAM_GC_DEFAULT_PGOV_MCK = "mscram_gc_default_pgov_mck"
MSCRAM_GC_TEN_GEN          = "mscram_gc_ten_gen"
MSCRAM_GC_TEN_GEN_EMON     = "mscram_gc_ten_gen_emon"
MSCRAM_GC_TEN_GEN_PGOV     = "mscram_gc_ten_gen_pgov"

MSCRAM4_GC_DEFAULT          = "mscram4_gc_default"
MSCRAM4_GC_DEFAULT_EMON     = "mscram4_gc_default_emon"
MSCRAM4_GC_DEFAULT_PGOV     = "mscram4_gc_default_pgov"
MSCRAM4_GC_DEFAULT_MCK      = "mscram4_gc_default_mck"
MSCRAM4_GC_DEFAULT_EMON_MCK = "mscram4_gc_default_emon_mck"
MSCRAM4_GC_DEFAULT_PGOV_MCK = "mscram4_gc_default_pgov_mck"
MSCRAM4_GC_TEN_GEN          = "mscram4_gc_ten_gen"
MSCRAM4_GC_TEN_GEN_EMON     = "mscram4_gc_ten_gen_emon"
MSCRAM4_GC_TEN_GEN_PGOV     = "mscram4_gc_ten_gen_pgov"

# on ecuador
ECD_TENURED_GEN_N1C0 = "ecd_tenured_gen_n1c0"
ECD_TENURED_GEN_N1C1 = "ecd_tenured_gen_n1c1"
ECD_TENURED_GEN_N1C2 = "ecd_tenured_gen_n1c2"

# gc_gen_color
GC_GEN_COLOR             = "gc_gen_color"
GC_GEN_COLOR_CLEAN       = "gc_gen_color_clean"
GC_GEN_COLOR_EMON        = "gc_gen_color_emon"
GC_GEN_COLOR_CLEAN_EMON  = "gc_gen_color_clean_emon"
GC_GEN_COLOR_PGOV        = "gc_gen_color_pgov"
GC_GEN_COLOR_CLEAN_PGOV  = "gc_gen_color_clean_pgov"

gc_gen_cfgs = [
  GC_GEN_COLOR,
  GC_GEN_COLOR_CLEAN,
  GC_GEN_COLOR_EMON,
  GC_GEN_COLOR_CLEAN_EMON,
  GC_GEN_COLOR_PGOV,
  GC_GEN_COLOR_CLEAN_PGOV
]
  

BC_RUNCFG    = 0
BC_PROFCFG   = 1
BC_NUMACFG   = 2
BC_MCOLORCFG = 3
BC_MSCRAMCFG = 4
BC_TRAYCFG   = 5

# dacapo run strings
SHORT_RUN = "short_run"
ONE_ITER  = "one_iter"

# numactl strings
NODE0_BIND = "node0_bind"
NODE1_BIND = "node1_bind"

# mcolorctl strings
MCOLOR_BIND_N1C0 = "mcolor_bind_n1c0"
MCOLOR_BIND_N1C1 = "mcolor_bind_n1c1"
MCOLOR_BIND_N1C2 = "mcolor_bind_n1c2"
MCOLOR_BIND_N1C3 = "mcolor_bind_n1c3"

ECD_MCOLOR_BIND_N1C0 = "ecd_mcolor_bind_n1c0"
ECD_MCOLOR_BIND_N1C1 = "ecd_mcolor_bind_n1c1"
ECD_MCOLOR_BIND_N1C2 = "ecd_mcolor_bind_n1c2"

MCOLOR_BIND_SKIP_N1C0 = "mcolor_bind_skip_n1c0"
MCOLOR_BIND_SKIP_N1C1 = "mcolor_bind_skip_n1c1"
MCOLOR_BIND_SKIP_N1C2 = "mcolor_bind_skip_n1c2"
MCOLOR_BIND_SKIP_N1C3 = "mcolor_bind_skip_n1c3"

MCOLOR_ILV_N1 = "mcolor_ilv_n1"

# mscramble strings
SCRAMBLE_HOLD_11_OVER_1  = "scramble_hold_11_over_1"
SCRAMBLE_HOLD_15_OVER_1  = "scramble_hold_15_over_1"
SCRAMBLE_HOLD_11_OVER_4  = "scramble_hold_11_over_4"
SCRAMBLE_HOLD_11_OVER_8  = "scramble_hold_11_over_8"
SCRAMBLE_HOLD_11_OVER_9  = "scramble_hold_11_over_9"
SCRAMBLE_HOLD_11_OVER_11 = "scramble_hold_11_over_11"
SCRAMBLE_HOLD_15_OVER_8  = "scramble_hold_15_over_8"
SCRAMBLE_HOLD_15_OVER_9  = "scramble_hold_15_over_9"
SCRAMBLE_HOLD_15_OVER_11 = "scramble_hold_15_over_11"
SCRAMBLE_HOLD_30_OVER_2  = "scramble_hold_30_over_2"

# trayctl strings
RESTRICT_N1C0 = "restrict_n1c0"
RESTRICT_N1C1 = "restrict_n1c1"
RESTRICT_N1C2 = "restrict_n1c2"
RESTRICT_N1C3 = "restrict_n1c3"

# cfgs dict (for benchmark cfgs)
# indexed by name
# values are tuples with the following:
# (runcfg, profcfg, numactlcfg, mcolorctlcfg, mscramblecfg)


def bcfg(runcfg=DEFAULT, profcfg=DEFAULT, numacfg=None, mcolorcfg=None, \
         mscramcfg=None, traycfg=None):
  return (runcfg, profcfg, numacfg, mcolorcfg, mscramcfg, traycfg)

objinfo_cfgs  = [OBJ_INFO_DEFAULT, OBJ_INFO_INTERVAL, OBJ_INFO_ORGANIZE, \
                 OBJ_INFO_GUIDED, OBJ_INFO_AGE_THRESH, OBJ_INFO_AT0_RT0, \
                 OBJ_INFO_XXX_RT0, OBJ_INFO_SIM]

pcm_cfgs      = [PCM_POWER, PCM_MEMORY, PCM_SELF_REFRESH, PCM_ENERGY_MODEL]

cfgs = {}
cfgs[DEFAULT]                   = bcfg(DEFAULT)
cfgs[DEFAULT_POWER]             = bcfg(DEFAULT,profcfg=PCM_POWER)
cfgs[SAMPLE_STACKS_VAL]         = bcfg(SAMPLE_STACKS_VAL)
cfgs[SAMPLE_STACKS_CON]         = bcfg(SAMPLE_STACKS_CON)
cfgs[SAMPLE_STACKS_HPROF]       = bcfg(SAMPLE_STACKS_HPROF)
cfgs[XINT]                      = bcfg(XINT)
cfgs[XINT_WBEC]                 = bcfg(XINT_WBEC)
cfgs[INTERPRET]                 = bcfg(INTERPRET)
cfgs[INTERPRET_WBEC]            = bcfg(INTERPRET_WBEC)
cfgs[TIERED_STEADY_BASE]        = bcfg(TIERED_STEADY_BASE)
cfgs[SERVER_STEADY_BASE]        = bcfg(SERVER_STEADY_BASE)
cfgs[TIERED_XSS_BASE]           = bcfg(TIERED_XSS_BASE)
cfgs[SERVER_XSS_BASE]           = bcfg(SERVER_XSS_BASE)
cfgs[STEADY_CCF]                = bcfg(STEADY_CCF)
cfgs[STEADY_CCF_REPLAY]         = bcfg(STEADY_CCF_REPLAY)
cfgs[STEADY_REPLAY]             = bcfg(STEADY_REPLAY)
cfgs[STEADY_REPLAY_LC]          = bcfg(STEADY_REPLAY_LC)
cfgs[REPLAY_NO_RESET]           = bcfg(REPLAY_NO_RESET)
cfgs[CCF_ORACLE_DIFF]           = bcfg(CCF_ORACLE_DIFF)
cfgs[XINT_CCF]                  = bcfg(XINT_CCF)
cfgs[NUMA]                      = bcfg(NUMA)
cfgs[MCOLOR_DEFAULT]            = bcfg(MCOLOR_DEFAULT)
cfgs[MCOLOR_NUMA]               = bcfg(MCOLOR_NUMA)
cfgs[MCOLOR_NUMA_NO_FREE]       = bcfg(MCOLOR_NUMA_NO_FREE)
cfgs[NUMA_NO_FREE]              = bcfg(NUMA_NO_FREE)
cfgs[NUMA_NO_MAKE_LOCAL]        = bcfg(NUMA_NO_MAKE_LOCAL)
cfgs[OBJ_INFO_FULL]             = bcfg(profcfg=OBJ_INFO_FULL)
cfgs[OBJ_INFO_FULL_TEN]         = bcfg(profcfg=OBJ_INFO_FULL_TEN)
cfgs[OBJ_INFO_DEFAULT]          = bcfg(profcfg=OBJ_INFO_DEFAULT)
cfgs[OBJ_INFO_GUIDED]           = bcfg(OBJ_INFO_GUIDED, profcfg=OBJ_INFO_GUIDED)
cfgs[METHOD_DATA_RECORD]        = bcfg(METHOD_DATA_RECORD)
cfgs[METHOD_DATA_REPLAY]        = bcfg(METHOD_DATA_REPLAY)
cfgs[OBJ_INFO_INTERVAL]         = bcfg(runcfg=OBJ_INFO_INTERVAL,
                                       profcfg=OBJ_INFO_INTERVAL,
                                       numacfg=NODE0_BIND)
cfgs[OBJ_INFO_SIM]              = bcfg(runcfg=OBJ_INFO_INTERVAL,
                                       profcfg=OBJ_INFO_INTERVAL,
                                       numacfg=NODE0_BIND)
cfgs[OBJ_INFO_XSIM]             = bcfg(runcfg=OBJ_INFO_XSIM, profcfg=OBJ_INFO_XSIM,
                                       numacfg=NODE0_BIND)
cfgs[OBJ_INFO_SHSIM]            = bcfg(runcfg=OBJ_INFO_SHSIM, profcfg=OBJ_INFO_SHSIM,
                                       numacfg=NODE0_BIND)
cfgs[OBJ_INFO_XSHSIM]           = bcfg(runcfg=OBJ_INFO_XSHSIM, profcfg=OBJ_INFO_XSHSIM,
                                       numacfg=NODE0_BIND)
cfgs[METHOD_KAL_INFO]           = bcfg(runcfg=METHOD_KAL_INFO)
cfgs[METHOD_KAL_INFOX]          = bcfg(runcfg=METHOD_KAL_INFOX)
cfgs[METHOD_KAL_INFONX]         = bcfg(runcfg=METHOD_KAL_INFONX)
cfgs[OBJ_INFO_TENURED]          = bcfg(profcfg=OBJ_INFO_TENURED)
cfgs[OBJ_VALS_DEFAULT]          = bcfg(profcfg=OBJ_VALS_DEFAULT)
cfgs[OBJ_VALS_TENURED]          = bcfg(profcfg=OBJ_VALS_TENURED)
cfgs[ADDR_TLAB_DEFAULT]         = bcfg(profcfg=ADDR_TLAB_DEFAULT)
cfgs[ADDR_TLAB_INTERVAL]        = bcfg(profcfg=ADDR_TLAB_INTERVAL)
cfgs[ADDR_TLAB_XVAL]            = bcfg(profcfg=ADDR_TLAB_XVAL)
cfgs[ADDR_TLAB_PREGC]           = bcfg(profcfg=ADDR_TLAB_PREGC)
cfgs[FIELD_INFO_INTERVAL]       = bcfg(profcfg=FIELD_INFO_INTERVAL)
cfgs[FIELD_TLAB_INTERVAL]       = bcfg(profcfg=FIELD_TLAB_INTERVAL)
cfgs[TZ_COLOR_AP]               = bcfg(TZ_COLOR_AP)
cfgs[TZ_COLOR_AP_98]            = bcfg(TZ_COLOR_AP_98)
cfgs[TZ_COLOR_AP_95]            = bcfg(TZ_COLOR_AP_95)
cfgs[AP_TLAB_INTERVAL]          = bcfg(profcfg=AP_TLAB_INTERVAL)
cfgs[MINI_AP_TLAB_INTERVAL]     = bcfg(profcfg=MINI_AP_TLAB_INTERVAL)
cfgs[FIELD_TLAB_PREGC]          = bcfg(profcfg=FIELD_TLAB_PREGC)
cfgs[ADDR_INFO_DEFAULT]         = bcfg(profcfg=ADDR_INFO_DEFAULT)
cfgs[ADDR_INFO_INTERVAL]        = bcfg(profcfg=ADDR_INFO_INTERVAL)
#cfgs[ADDR_HEAVY_INTERVAL]       = bcfg(profcfg=ADDR_HEAVY_INTERVAL)
cfgs[OBJ_GCVALS_DEFAULT]        = bcfg(profcfg=OBJ_GCVALS_DEFAULT)
cfgs[OBJ_GCVALS_TENURED]        = bcfg(profcfg=OBJ_GCVALS_TENURED)
cfgs[COLORED_SPACES]            = bcfg(COLORED_SPACES, profcfg=PCM_POWER)
cfgs[ORGANIZE_OBJECTS]          = bcfg(ORGANIZE_OBJECTS)
cfgs[OBJ_ORG_PROFILE]           = bcfg(ORGANIZE_OBJECTS,profcfg=OBJ_INFO_ORGANIZE)
cfgs[OBJ_ORG_ATP]               = bcfg(ORGANIZE_OBJECTS,profcfg=OBJ_INFO_AGE_THRESH)
cfgs[OOP_AT0_RT0]               = bcfg(ORGANIZE_OBJECTS,profcfg=OBJ_INFO_AT0_RT0)
cfgs[OOP_XXX_RT0]               = bcfg(ORGANIZE_OBJECTS,profcfg=OBJ_INFO_XXX_RT0)
cfgs[COLOR_ALLOCATIONS]         = bcfg(COLOR_ALLOCATIONS, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[DEFAULT_ALLOCATIONS]       = bcfg(DEFAULT_ALLOCATIONS, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HEAP_TRIAL]                = bcfg(HEAP_TRIAL, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND)
cfgs[HEAP_TRIAL_LAGOM]          = bcfg(HEAP_TRIAL, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND)
cfgs[DEF_ALLOC_BASE]            = bcfg(DEF_ALLOC_BASE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_LOW_POWER]            = bcfg(HDKM_LOW_POWER,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[HDKM_ALLOC_BASE]           = bcfg(HDKM_ALLOC_BASE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_ALLOC_BASE_V2]        = bcfg(HDKM_ALLOC_BASE_V2,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[HDKM_ALLOC_TUNE]           = bcfg(HDKM_ALLOC_TUNE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_ALLOC_TUNE_XX]        = bcfg(HDKM_ALLOC_TUNE_V2,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[HDKM_ALLOC_TUNE_V2]        = bcfg(HDKM_ALLOC_TUNE_V2,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[HDKM_FAST_TUNE]            = bcfg(HDKM_FAST_TUNE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_FAST_TUNE2]           = bcfg(HDKM_FAST_TUNE2,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_ALLOC_BASE_XX]        = bcfg(HDKM_ALLOC_BASE_XX,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKD_ALLOC_BASE]           = bcfg(HDKD_ALLOC_BASE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKD_ALLOC_BASE_V2]        = bcfg(HDKD_ALLOC_BASE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKD_ALLOC_TUNE]           = bcfg(HDKD_ALLOC_TUNE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[HDKD_ALLOC_TUNE_XX]        = bcfg(HDKD_ALLOC_TUNE_XX,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[HDKD_CHAN_ILV]             = bcfg(HDKD_CHAN_ILV,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[ONLINE_COLOR_TEST]         = bcfg(ONLINE_COLOR_TEST,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[ONLINE_COLOR_TUNE]         = bcfg(ONLINE_COLOR_TUNE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[ONLINE_COLOR_TUNE_HKO]     = bcfg(ONLINE_COLOR_TUNE_HKO,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[OCT_HKA_MS10_CDF10]        = bcfg(OCT_HKA_MS10_CDF10,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
#cfgs[OCT_MS10_CD100]            = bcfg(OCT_MS10_CD100,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_MS100_CD250]           = bcfg(OCT_MS100_CD250,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS10_CD100]        = bcfg(OCT_HKO_MS10_CD100,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS100_CD250]       = bcfg(OCT_HKO_MS100_CD250,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS50_CD500]        = bcfg(OCT_HKO_MS50_CD500,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKA_MS10_CD100]        = bcfg(OCT_HKA_MS10_CD100,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKA_MS50_CD500]        = bcfg(OCT_HKA_MS50_CD500,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKA_SCIMARK1]          = bcfg(OCT_HKA_SCIMARK1,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKA_SCIMARK2]          = bcfg(OCT_HKA_SCIMARK2,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKA_SCIMARK3]          = bcfg(OCT_HKA_SCIMARK3,
#                                       profcfg=PCM_ENERGY_MODEL,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_MS10_CD50]                = bcfg(OCT_MS10_CD50, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_MS10_CD200]               = bcfg(OCT_MS10_CD200, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS10_CD50]            = bcfg(OCT_HKO_MS10_CD50, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS10_CD100]           = bcfg(OCT_HKO_MS10_CD100, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS10_CD200]           = bcfg(OCT_HKO_MS10_CD200, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_MS50_CD50]                = bcfg(OCT_MS50_CD50, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_MS50_CD100]               = bcfg(OCT_MS50_CD100, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_MS50_CD200]               = bcfg(OCT_MS50_CD200, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS50_CD50]            = bcfg(OCT_HKO_MS50_CD50, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS50_CD100]           = bcfg(OCT_HKO_MS50_CD100, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
#cfgs[OCT_HKO_MS50_CD200]           = bcfg(OCT_HKO_MS50_CD200, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[HDKD_FAST_BASE]            = bcfg(HDKD_FAST_BASE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HOKM_ALLOC_BASE]           = bcfg(HOKM_ALLOC_BASE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ILV_BASE]            = bcfg(COLOR_ILV_BASE, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ILV_TUNE]            = bcfg(COLOR_ILV_TUNE, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ILV_TUNE_X]          = bcfg(HDKM_ALLOC_TUNE, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ILV_TLAB]            = bcfg(COLOR_ILV_TUNE, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ILV_TLAB_X]          = bcfg(HDKM_ALLOC_TUNE, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[DEFAULT_ALLOCATIONS_A]     = bcfg(DEFAULT_ALLOCATIONS_A, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[DEFAULT_ALLOCATIONS_X]     = bcfg(DEFAULT_ALLOCATIONS_X, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[DEFAULT_ALLOCATIONS_Y]     = bcfg(DEFAULT_ALLOCATIONS_Y, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[DEFAULT_ALLOCATIONS_Z]     = bcfg(DEFAULT_ALLOCATIONS_Z, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_ALLOCATIONS_A]        = bcfg(HDKM_ALLOCATIONS_A, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_ALLOCATIONS_B]        = bcfg(HDKM_ALLOCATIONS_B, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKD_ALLOCATIONS_A]        = bcfg(HDKD_ALLOCATIONS_A, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_ALLOCATIONS_X]        = bcfg(HDKM_ALLOCATIONS_X, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_ALLOCATIONS_Y]        = bcfg(HDKM_ALLOCATIONS_Y, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDKM_ALLOCATIONS_Z]        = bcfg(HDKM_ALLOCATIONS_Z, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOC_BASE]          = bcfg(COLOR_ALLOC_BASE, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOC_TUNE]          = bcfg(COLOR_ALLOC_TUNE, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOC_TUNE_V2]       = bcfg(COLOR_ALLOC_TUNE_V2, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_30_OVER_2)
cfgs[COLOR_ALLOC_TUNE_X]        = bcfg(COLOR_ALLOC_TUNE_X, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOCATIONS_A]       = bcfg(COLOR_ALLOCATIONS_A, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOCATIONS_B]       = bcfg(COLOR_ALLOCATIONS_B, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOCATIONS_C]       = bcfg(COLOR_ALLOCATIONS_C, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOCATIONS_X]       = bcfg(COLOR_ALLOCATIONS_X, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOCATIONS_Y]       = bcfg(COLOR_ALLOCATIONS_Y, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOCATIONS_Z]       = bcfg(COLOR_ALLOCATIONS_Z, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLOR_ALLOCATIONS_ILV]     = bcfg(COLOR_ALLOCATIONS_ILV, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[EDEN_ALWAYS_RED]           = bcfg(EDEN_ALWAYS_RED, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[EDEN_ALWAYS_RED_A]         = bcfg(EDEN_ALWAYS_RED_A, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[EDEN_ALWAYS_RED_X]         = bcfg(EDEN_ALWAYS_RED_X, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[EDEN_ALWAYS_RED_Y]         = bcfg(EDEN_ALWAYS_RED_Y, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[EDEN_ALWAYS_RED_Z]         = bcfg(EDEN_ALWAYS_RED_Z, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[OBJ_RED_PROFILE]           = bcfg(RED_OBJECTS,profcfg=OBJ_INFO_INTERVAL)
cfgs[OBJ_BLUE_PROFILE]          = bcfg(BLUE_OBJECTS,profcfg=OBJ_INFO_INTERVAL)
#cfgs[HDEFAULT_KDEFAULT]         = bcfg(MEMBENCH_DEFAULT, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=None)
#cfgs[HDEFAULT_KMCOLOR]          = bcfg(MEMBENCH_DEFAULT, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=None)
#cfgs[HORGANIZE_KMCOLOR]         = bcfg(MEMBENCH_ORGANIZE, profcfg=PCM_POWER,
#                                       numacfg=NODE1_BIND,
#                                       mscramcfg=None)
cfgs[HDEFAULT_KDEFAULT]         = bcfg(MEMBENCH_DEFAULT,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HDEFAULT_KMCOLOR]          = bcfg(MEMBENCH_DEFAULT,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HORGANIZE_KMCOLOR]         = bcfg(MEMBENCH_ORGANIZE,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[MEMBENCH_COLOR]            = bcfg(MEMBENCH_COLOR_ALLOC,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[MEMBENCH_COLOR_V2]         = bcfg(MEMBENCH_COLOR_V2, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[BWBENCH_DEFAULT]           = bcfg(BWBENCH_DEFAULT,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[BWBENCH_COLOR]             = bcfg(BWBENCH_COLOR,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[BWBENCH_COLOR_ILV]         = bcfg(BWBENCH_COLOR_ILV,
                                       profcfg=PCM_ENERGY_MODEL,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HORGANIZE_ILV_KMCOLOR]     = bcfg(MEMBENCH_ILV_ORGANIZE, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[HORGANIZE_XILV_KMCOLOR]    = bcfg(MEMBENCH_XILV, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)
cfgs[COLORED_SPACES_X]          = bcfg(COLORED_SPACES_X, profcfg=PCM_POWER,
                                       numacfg=NODE1_BIND,
                                       mscramcfg=None)

cfgs[DEFAULT_EMON]              = bcfg(DEFAULT, profcfg=EMON_OPROF)

cfgs[MCOLOR_NODE1_CHN0]         = bcfg(profcfg=EMON_OPROF, mcolorcfg=MCOLOR_NODE1_CHN0)
cfgs[MCOLOR_NODE1_CHN1]         = bcfg(profcfg=EMON_OPROF, mcolorcfg=MCOLOR_NODE1_CHN1)
cfgs[MCOLOR_NODE1_CHN2]         = bcfg(profcfg=EMON_OPROF, mcolorcfg=MCOLOR_NODE1_CHN2)
cfgs[MCOLOR_NODE1_CHN3]         = bcfg(profcfg=EMON_OPROF, mcolorcfg=MCOLOR_NODE1_CHN3)

cfgs[MCOLOR_NODE1_CHN0_PGOV]    = bcfg(profcfg=POWER_GOV_OPROF, mcolorcfg=MCOLOR_NODE1_CHN0)
cfgs[MCOLOR_NODE1_CHN1_PGOV]    = bcfg(profcfg=POWER_GOV_OPROF, mcolorcfg=MCOLOR_NODE1_CHN1)
cfgs[MCOLOR_NODE1_CHN2_PGOV]    = bcfg(profcfg=POWER_GOV_OPROF, mcolorcfg=MCOLOR_NODE1_CHN2)
cfgs[MCOLOR_NODE1_CHN3_PGOV]    = bcfg(profcfg=POWER_GOV_OPROF, mcolorcfg=MCOLOR_NODE1_CHN3)

cfgs[MCOLOR_NODE1_CHN0_FULL]    = bcfg(profcfg=FULL_OPROF, mcolorcfg=MCOLOR_NODE1_CHN0)
cfgs[MCOLOR_NODE1_CHN1_FULL]    = bcfg(profcfg=FULL_OPROF, mcolorcfg=MCOLOR_NODE1_CHN1)
cfgs[MCOLOR_NODE1_CHN2_FULL]    = bcfg(profcfg=FULL_OPROF, mcolorcfg=MCOLOR_NODE1_CHN2)
cfgs[MCOLOR_NODE1_CHN3_FULL]    = bcfg(profcfg=FULL_OPROF, mcolorcfg=MCOLOR_NODE1_CHN3)

cfgs[COLOMBIA_STOCK_DEFAULT]    = bcfg(DEFAULT, DEFAULT)
cfgs[COLOMBIA_STOCK_EMON]       = bcfg(DEFAULT, EMON_OPROF)
cfgs[COLOMBIA_STOCK_POWER_GOV]  = bcfg(DEFAULT, POWER_GOV_OPROF)

cfgs[COLOMBIA_MCKERN_DEFAULT]   = bcfg(DEFAULT, DEFAULT)
cfgs[COLOMBIA_MCKERN_EMON]      = bcfg(DEFAULT, EMON_OPROF)
cfgs[COLOMBIA_MCKERN_POWER_GOV] = bcfg(DEFAULT, POWER_GOV_OPROF)

cfgs[COLOMBIA_NOILV_DEFAULT]    = bcfg(DEFAULT, DEFAULT)
cfgs[COLOMBIA_NOILV_EMON]       = bcfg(DEFAULT, EMON_OPROF)
cfgs[COLOMBIA_NOILV_POWER_GOV]  = bcfg(DEFAULT, POWER_GOV_OPROF)


cfgs[DEFAULT_PSGC]              = bcfg(DEFAULT_PSGC, numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_9)
cfgs[DEFAULT_PSGC_EMON]         = bcfg(DEFAULT_PSGC, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_9)
cfgs[DEFAULT_PSGC_PGOV]         = bcfg(DEFAULT_PSGC, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_9)

cfgs[TEN_GEN_PSGC]              = bcfg(TEN_GEN_PSGC, numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_9, \
                                       traycfg=RESTRICT_N1C3)
cfgs[TEN_GEN_PSGC_EMON]         = bcfg(TEN_GEN_PSGC, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_9, \
                                       traycfg=RESTRICT_N1C3)
cfgs[TEN_GEN_PSGC_PGOV]         = bcfg(TEN_GEN_PSGC, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_9, \
                                       traycfg=RESTRICT_N1C3)

cfgs[UNSCRAM_PSGC]              = bcfg(DEFAULT_PSGC, numacfg=NODE1_BIND)
cfgs[UNSCRAM_PSGC_EMON]         = bcfg(DEFAULT_PSGC, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND)
cfgs[UNSCRAM_PSGC_PGOV]         = bcfg(DEFAULT_PSGC, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND)

cfgs[UNSCRAM_TEN_GEN_PSGC]      = bcfg(TEN_GEN_PSGC, numacfg=NODE1_BIND,
                                       traycfg=RESTRICT_N1C3)
cfgs[UNSCRAM_TEN_GEN_PSGC_EMON] = bcfg(TEN_GEN_PSGC, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       traycfg=RESTRICT_N1C3)
cfgs[UNSCRAM_TEN_GEN_PSGC_PGOV] = bcfg(TEN_GEN_PSGC, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       traycfg=RESTRICT_N1C3)


cfgs[MSCRAM_GC_DEFAULT]          = bcfg(DEFAULT, numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_15_OVER_8)
cfgs[MSCRAM_GC_DEFAULT_EMON]     = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                        numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_15_OVER_8)
cfgs[MSCRAM_GC_DEFAULT_PGOV]     = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                        numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_15_OVER_8)

cfgs[MSCRAM_GC_DEFAULT_MCK]      = bcfg(DEFAULT, numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_11_OVER_8)
cfgs[MSCRAM_GC_DEFAULT_EMON_MCK] = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                        numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_11_OVER_8)
cfgs[MSCRAM_GC_DEFAULT_PGOV_MCK] = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                        numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_11_OVER_8)

cfgs[MSCRAM_GC_TEN_GEN]          = bcfg(TENURED_GEN_N1C3, numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_8, \
                                       traycfg=RESTRICT_N1C3)
cfgs[MSCRAM_GC_TEN_GEN_EMON]     = bcfg(TENURED_GEN_N1C3, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_8, \
                                       traycfg=RESTRICT_N1C3)
cfgs[MSCRAM_GC_TEN_GEN_PGOV]     = bcfg(TENURED_GEN_N1C3, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_8, \
                                       traycfg=RESTRICT_N1C3)

cfgs[MSCRAM4_GC_DEFAULT_MCK]      = bcfg(DEFAULT, numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_11_OVER_4)
cfgs[MSCRAM4_GC_DEFAULT_EMON_MCK] = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                        numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_11_OVER_4)
cfgs[MSCRAM4_GC_DEFAULT_PGOV_MCK] = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                        numacfg=NODE1_BIND, \
                                        mscramcfg=SCRAMBLE_HOLD_11_OVER_4)

cfgs[MSCRAM4_GC_TEN_GEN]          = bcfg(TENURED_GEN_N1C3, numacfg=NODE1_BIND,
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_4, \
                                       traycfg=RESTRICT_N1C3)
cfgs[MSCRAM4_GC_TEN_GEN_EMON]     = bcfg(TENURED_GEN_N1C3, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_4, \
                                       traycfg=RESTRICT_N1C3)
cfgs[MSCRAM4_GC_TEN_GEN_PGOV]     = bcfg(TENURED_GEN_N1C3, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_4, \
                                       traycfg=RESTRICT_N1C3)


cfgs[GC_GEN_COLOR]              = bcfg(GC_GEN_COLOR, numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_1, \
                                       traycfg=RESTRICT_N1C3)
cfgs[GC_GEN_COLOR_CLEAN]        = bcfg(GC_GEN_COLOR_CLEAN, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_1)

cfgs[GC_GEN_COLOR_EMON]         = bcfg(GC_GEN_COLOR, numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_1, \
                                       profcfg=EMON_HPROF, \
                                       traycfg=RESTRICT_N1C3)
cfgs[GC_GEN_COLOR_CLEAN_EMON]   = bcfg(GC_GEN_COLOR_CLEAN, numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_1, \
                                       profcfg=EMON_HPROF)

cfgs[GC_GEN_COLOR_PGOV]         = bcfg(GC_GEN_COLOR, numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_1, \
                                       profcfg=POWER_GOV_HPROF, \
                                       traycfg=RESTRICT_N1C3)
cfgs[GC_GEN_COLOR_CLEAN_PGOV]   = bcfg(GC_GEN_COLOR_CLEAN, numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_1, \
                                       profcfg=POWER_GOV_HPROF)

cfgs[ECD_TENURED_GEN_N1C0]      = bcfg(ECD_TENURED_GEN_N1C0, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND)
cfgs[ECD_TENURED_GEN_N1C1]      = bcfg(ECD_TENURED_GEN_N1C1, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND)
cfgs[ECD_TENURED_GEN_N1C2]      = bcfg(ECD_TENURED_GEN_N1C2, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND)

# numa experiments
#cfgs[DEFAULT_HEMON]                 = bcfg(DEFAULT, profcfg=EMON_HPROF, \
#                                           numacfg=NODE1_BIND)
#cfgs[NUMA_HEMON]                    = bcfg(NUMA, profcfg=EMON_HPROF, \
#                                           numacfg=NODE1_BIND)
cfgs[DEFAULT_HEMON]                 = bcfg(DEFAULT, profcfg=EMON_HPROF)
cfgs[NUMA_HEMON]                    = bcfg(NUMA, profcfg=EMON_HPROF)
cfgs[NUMANI_HEMON]                  = bcfg(NUMANI, profcfg=EMON_HPROF)

cfgs[DEFAULT_NOILV_HEMON]           = bcfg(DEFAULT, profcfg=EMON_HPROF)
cfgs[NUMA_NOILV_HEMON]              = bcfg(NUMA, profcfg=EMON_HPROF)
cfgs[NUMANI_NOILV_HEMON]            = bcfg(NUMANI, profcfg=EMON_HPROF)

cfgs[DEFAULT_HEMON_MCK]             = bcfg(DEFAULT, profcfg=EMON_HPROF)
cfgs[NUMA_HEMON_MCK]                = bcfg(NUMA, profcfg=EMON_HPROF)
cfgs[NUMANI_HEMON_MCK]              = bcfg(NUMANI, profcfg=EMON_HPROF)
cfgs[MCOLOR_NUMA_HEMON_MCK]         = bcfg(MCOLOR_NUMA, profcfg=EMON_HPROF)
cfgs[MCOLOR_NUMANI_HEMON_MCK]       = bcfg(MCOLOR_NUMANI, profcfg=EMON_HPROF)

cfgs[DEFAULT_NOILV_HEMON_MCK]       = bcfg(DEFAULT, profcfg=EMON_HPROF)
cfgs[NUMA_NOILV_HEMON_MCK]          = bcfg(NUMA, profcfg=EMON_HPROF)
cfgs[NUMANI_NOILV_HEMON_MCK]        = bcfg(NUMANI, profcfg=EMON_HPROF)
cfgs[MCOLOR_NUMA_NOILV_HEMON_MCK]   = bcfg(MCOLOR_NUMA, profcfg=EMON_HPROF)
cfgs[MCOLOR_NUMANI_NOILV_HEMON_MCK] = bcfg(MCOLOR_NUMANI, profcfg=EMON_HPROF)

# older runs
#cfgs[NUMA_NOILV_EMON]              = bcfg(NUMA, profcfg=EMON_HPROF)
#cfgs[NUMA_NOILV_EMON_MCK]          = bcfg(NUMA, profcfg=EMON_HPROF)
#cfgs[MCOLOR_NUMA_NOILV_EMON_MCK]   = bcfg(MCOLOR_NUMA, profcfg=EMON_HPROF)

cfgs[DEFAULT_MSCRAMBLE_EMON_ILV] = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)
cfgs[DEFAULT_MSCRAMBLE_PGOV_ILV] = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)
cfgs[DEFAULT_MSCRAMBLE_FULL_ILV] = bcfg(DEFAULT, profcfg=FULL_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)

cfgs[DEFAULT_MSCRAMBLE_EMON_STK] = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)
cfgs[DEFAULT_MSCRAMBLE_PGOV_STK] = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)
cfgs[DEFAULT_MSCRAMBLE_FULL_STK] = bcfg(DEFAULT, profcfg=FULL_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)

cfgs[DEFAULT_MSCRAMBLE_EMON]    = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)
cfgs[DEFAULT_MSCRAMBLE_PGOV]    = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)
cfgs[DEFAULT_MSCRAMBLE_FULL]    = bcfg(DEFAULT, profcfg=FULL_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mscramcfg=SCRAMBLE_HOLD_15_OVER_11)

cfgs[POWER_MSCRAMBLE_EMON]      = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mcolorcfg=MCOLOR_BIND_N1C0, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_11)
cfgs[POWER_MSCRAMBLE_PGOV]      = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mcolorcfg=MCOLOR_BIND_N1C0, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_11)
cfgs[POWER_MSCRAMBLE_FULL]      = bcfg(DEFAULT, profcfg=FULL_OPROF, \
                                       numacfg=NODE1_BIND, \
                                       mcolorcfg=MCOLOR_BIND_N1C0, \
                                       mscramcfg=SCRAMBLE_HOLD_11_OVER_11)

# cfgs specific to ecuador
cfgs[ECD_MCOLOR_N1C0_EMON]      = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                       mcolorcfg=ECD_MCOLOR_BIND_N1C0)
cfgs[ECD_MCOLOR_N1C1_EMON]      = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                       mcolorcfg=ECD_MCOLOR_BIND_N1C1)
cfgs[ECD_MCOLOR_N1C2_EMON]      = bcfg(DEFAULT, profcfg=EMON_OPROF, \
                                       mcolorcfg=ECD_MCOLOR_BIND_N1C2)

cfgs[ECD_MCOLOR_N1C0_PGOV]      = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                       mcolorcfg=ECD_MCOLOR_BIND_N1C0)
cfgs[ECD_MCOLOR_N1C1_PGOV]      = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                       mcolorcfg=ECD_MCOLOR_BIND_N1C1)
cfgs[ECD_MCOLOR_N1C2_PGOV]      = bcfg(DEFAULT, profcfg=POWER_GOV_OPROF, \
                                       mcolorcfg=ECD_MCOLOR_BIND_N1C2)

#md_mults = [ 0.01, 0.1, 0.25, 0.5, 0.75, 0.9, 1.0, 2.0, 10.0 ]
ctscale = [ 0, 0.01, 0.1, 0.5, 1.0, 2.0, 10.0 ]
md_ct_records = []
md_ct_replays = []
steady_md_ct_records = []
steady_md_ct_replays = []
xss_md_ct_records = []
xss_md_ct_replays = []
for ct in ctscale:
  md_ct_records.append((MD_CT_RECORD%ct))
  md_ct_replays.append((MD_CT_REPLAY%ct))
  steady_md_ct_records.append((STEADY_MD_CT_RECORD%ct))
  steady_md_ct_replays.append((STEADY_MD_CT_REPLAY%ct))
  xss_md_ct_records.append((XSS_MD_CT_RECORD%ct))
  xss_md_ct_replays.append((XSS_MD_CT_REPLAY%ct))

  cfgs[(MD_CT_RECORD%ct)]        = bcfg((MD_CT_RECORD%ct))
  cfgs[(MD_CT_REPLAY%ct)]        = bcfg((MD_CT_REPLAY%ct))
  cfgs[(STEADY_MD_CT_RECORD%ct)] = bcfg((STEADY_MD_CT_RECORD%ct))
  cfgs[(STEADY_MD_CT_REPLAY%ct)] = bcfg((STEADY_MD_CT_REPLAY%ct))
  cfgs[(XSS_MD_CT_RECORD%ct)]    = bcfg((XSS_MD_CT_RECORD%ct))
  cfgs[(XSS_MD_CT_REPLAY%ct)]    = bcfg((XSS_MD_CT_REPLAY%ct))

nscale = [0, 1, 10, 100]
md_records = []
md_replays = []
for n in nscale:
  if n != 0:
    md_records.append((MD_RECORD%n))
    cfgs[(MD_RECORD%n)] = bcfg((MD_RECORD%n))
  md_replays.append((MD_REPLAY%n))
  cfgs[(MD_REPLAY%n)] = bcfg((MD_REPLAY%n))

vrates = [50, 100, 250, 350, 500]
for vr in vrates:
  cfgs[(OBJ_INFO_VXSIM%vr)] = bcfg(runcfg=(OBJ_INFO_VXSIM%vr),
                                   profcfg=(OBJ_INFO_VXSIM%vr),
                                   numacfg=NODE0_BIND)
  cfgs[(OBJ_INFO_NVXSIM%vr)] = bcfg(runcfg=(OBJ_INFO_NVXSIM%vr),
                                   profcfg=(OBJ_INFO_NVXSIM%vr),
                                   numacfg=NODE0_BIND)

#############################################################################
# custom apc configs
#############################################################################
CA_REF_02    = "ca_ref_02"
CA_REF_05    = "ca_ref_05"
CA_REF_10    = "ca_ref_10"
CA_REF_20    = "ca_ref_20"
CA_REF_40    = "ca_ref_40"
CA_REF_V2_02 = "ca_ref_v2_02"
CA_REF_V2_05 = "ca_ref_v2_05"
CA_REF_V2_10 = "ca_ref_v2_10"
CA_REF_V2_20 = "ca_ref_v2_20"
CA_REF_V2_40 = "ca_ref_v2_40"
CA_REF_V3_02 = "ca_ref_v3_02"
CA_REF_V3_05 = "ca_ref_v3_05"
CA_REF_V3_10 = "ca_ref_v3_10"
CA_REF_V3_20 = "ca_ref_v3_20"
CA_REF_V3_40 = "ca_ref_v3_40"
CA_X_REF_02 = "ca_x_ref_02"
CA_X_REF_05 = "ca_x_ref_05"
CA_X_REF_10 = "ca_x_ref_10"
CA_X_REF_20 = "ca_x_ref_20"
CA_X_REF_40 = "ca_x_ref_40"
CA_Y_REF_02 = "ca_y_ref_02"
CA_Y_REF_05 = "ca_y_ref_05"
CA_Y_REF_10 = "ca_y_ref_10"
CA_Y_REF_20 = "ca_y_ref_20"
CA_Y_REF_40 = "ca_y_ref_40"
CA_A_REF_02 = "ca_a_ref_02"
CA_A_REF_05 = "ca_a_ref_05"
CA_A_REF_10 = "ca_a_ref_10"
CA_A_REF_20 = "ca_a_ref_20"
CA_A_REF_40 = "ca_a_ref_40"
CA_B_REF_02 = "ca_b_ref_02"
CA_B_REF_05 = "ca_b_ref_05"
CA_B_REF_10 = "ca_b_ref_10"
CA_B_REF_20 = "ca_b_ref_20"
CA_B_REF_40 = "ca_b_ref_40"
CA_C_REF_02 = "ca_c_ref_02"
CA_C_REF_05 = "ca_c_ref_05"
CA_C_REF_10 = "ca_c_ref_10"
CA_C_REF_20 = "ca_c_ref_20"
CA_C_REF_40 = "ca_c_ref_40"
CA_Z_REF_02 = "ca_z_ref_02"
CA_Z_REF_05 = "ca_z_ref_05"
CA_Z_REF_10 = "ca_z_ref_10"
CA_Z_REF_20 = "ca_z_ref_20"
CA_Z_REF_40 = "ca_z_ref_40"
CA_SIZE_02  = "ca_size_02"
CA_SIZE_05  = "ca_size_05"
CA_SIZE_10  = "ca_size_10"
EAR_REF_02  = "ear_ref_02"
EAR_REF_05  = "ear_ref_05"
EAR_REF_10  = "ear_ref_10"
EAR_REF_40  = "ear_ref_40"
EAR_A_REF_02  = "ear_a_ref_02"
EAR_A_REF_05  = "ear_a_ref_05"
EAR_A_REF_10  = "ear_a_ref_10"
EAR_A_REF_40  = "ear_a_ref_40"
EAR_X_REF_02  = "ear_x_ref_02"
EAR_X_REF_05  = "ear_x_ref_05"
EAR_X_REF_10  = "ear_x_ref_10"
EAR_X_REF_40  = "ear_x_ref_40"
EAR_Y_REF_02  = "ear_y_ref_02"
EAR_Y_REF_05  = "ear_y_ref_05"
EAR_Y_REF_10  = "ear_y_ref_10"
EAR_Y_REF_40  = "ear_y_ref_40"
EAR_Z_REF_02  = "ear_z_ref_02"
EAR_Z_REF_05  = "ear_z_ref_05"
EAR_Z_REF_10  = "ear_z_ref_10"
EAR_Z_REF_40  = "ear_z_ref_40"
EAR_SIZE_02 = "ear_size_02"
EAR_SIZE_05 = "ear_size_05"
EAR_SIZE_10 = "ear_size_10"

cfgs[CA_REF_02]   = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_REF_05]   = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_REF_10]   = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_REF_20]   = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_REF_40]   = cfgs[COLOR_ALLOC_TUNE]

cfgs[CA_REF_V2_02] = cfgs[COLOR_ALLOC_TUNE_V2]
cfgs[CA_REF_V2_05] = cfgs[COLOR_ALLOC_TUNE_V2]
cfgs[CA_REF_V2_10] = cfgs[COLOR_ALLOC_TUNE_V2]
cfgs[CA_REF_V2_20] = cfgs[COLOR_ALLOC_TUNE_V2]
cfgs[CA_REF_V2_40] = cfgs[COLOR_ALLOC_TUNE_V2]

cfgs[CA_REF_V3_02] = cfgs[COLOR_ALLOC_TUNE_V2]
cfgs[CA_REF_V3_05] = cfgs[COLOR_ALLOC_TUNE_V2]
cfgs[CA_REF_V3_10] = cfgs[COLOR_ALLOC_TUNE_V2]
cfgs[CA_REF_V3_20] = cfgs[COLOR_ALLOC_TUNE_V2]
cfgs[CA_REF_V3_40] = cfgs[COLOR_ALLOC_TUNE_V2]

cfgs[CA_X_REF_02] = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_X_REF_05] = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_X_REF_10] = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_X_REF_20] = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_X_REF_40] = cfgs[COLOR_ALLOC_TUNE]

cfgs[CA_Y_REF_02] = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_Y_REF_05] = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_Y_REF_10] = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_Y_REF_20] = cfgs[COLOR_ALLOC_TUNE]
cfgs[CA_Y_REF_40] = cfgs[COLOR_ALLOC_TUNE]

cfgs[CA_A_REF_02] = cfgs[COLOR_ALLOCATIONS_A]
cfgs[CA_A_REF_05] = cfgs[COLOR_ALLOCATIONS_A]
cfgs[CA_A_REF_10] = cfgs[COLOR_ALLOCATIONS_A]
cfgs[CA_A_REF_20] = cfgs[COLOR_ALLOCATIONS_A]
cfgs[CA_A_REF_40] = cfgs[COLOR_ALLOCATIONS_A]

cfgs[CA_B_REF_02] = cfgs[COLOR_ALLOCATIONS_B]
cfgs[CA_B_REF_05] = cfgs[COLOR_ALLOCATIONS_B]
cfgs[CA_B_REF_10] = cfgs[COLOR_ALLOCATIONS_B]
cfgs[CA_B_REF_20] = cfgs[COLOR_ALLOCATIONS_B]
cfgs[CA_B_REF_40] = cfgs[COLOR_ALLOCATIONS_B]

cfgs[CA_C_REF_02] = cfgs[COLOR_ALLOCATIONS_C]
cfgs[CA_C_REF_05] = cfgs[COLOR_ALLOCATIONS_C]
cfgs[CA_C_REF_10] = cfgs[COLOR_ALLOCATIONS_C]
cfgs[CA_C_REF_20] = cfgs[COLOR_ALLOCATIONS_C]
cfgs[CA_C_REF_40] = cfgs[COLOR_ALLOCATIONS_C]

cfgs[CA_Z_REF_02] = cfgs[COLOR_ALLOCATIONS_Z]
cfgs[CA_Z_REF_05] = cfgs[COLOR_ALLOCATIONS_Z]
cfgs[CA_Z_REF_10] = cfgs[COLOR_ALLOCATIONS_Z]
cfgs[CA_Z_REF_20] = cfgs[COLOR_ALLOCATIONS_Z]
cfgs[CA_Z_REF_40] = cfgs[COLOR_ALLOCATIONS_Z]

#cfgs[CA_SIZE_02]  = cfgs[COLOR_ALLOCATIONS]
#cfgs[CA_SIZE_05]  = cfgs[COLOR_ALLOCATIONS]
#cfgs[CA_SIZE_10]  = cfgs[COLOR_ALLOCATIONS]

cfgs[EAR_REF_02]  = cfgs[EDEN_ALWAYS_RED]
cfgs[EAR_REF_05]  = cfgs[EDEN_ALWAYS_RED]
cfgs[EAR_REF_10]  = cfgs[EDEN_ALWAYS_RED]
cfgs[EAR_REF_40]  = cfgs[EDEN_ALWAYS_RED]

cfgs[EAR_A_REF_02]  = cfgs[EDEN_ALWAYS_RED_A]
cfgs[EAR_A_REF_05]  = cfgs[EDEN_ALWAYS_RED_A]
cfgs[EAR_A_REF_10]  = cfgs[EDEN_ALWAYS_RED_A]
cfgs[EAR_A_REF_40]  = cfgs[EDEN_ALWAYS_RED_A]

cfgs[EAR_X_REF_02]  = cfgs[EDEN_ALWAYS_RED_X]
cfgs[EAR_X_REF_05]  = cfgs[EDEN_ALWAYS_RED_X]
cfgs[EAR_X_REF_10]  = cfgs[EDEN_ALWAYS_RED_X]
cfgs[EAR_X_REF_40]  = cfgs[EDEN_ALWAYS_RED_X]

cfgs[EAR_Y_REF_02]  = cfgs[EDEN_ALWAYS_RED_Y]
cfgs[EAR_Y_REF_05]  = cfgs[EDEN_ALWAYS_RED_Y]
cfgs[EAR_Y_REF_10]  = cfgs[EDEN_ALWAYS_RED_Y]
cfgs[EAR_Y_REF_40]  = cfgs[EDEN_ALWAYS_RED_Y]

cfgs[EAR_Z_REF_02]  = cfgs[EDEN_ALWAYS_RED_Z]
cfgs[EAR_Z_REF_05]  = cfgs[EDEN_ALWAYS_RED_Z]
cfgs[EAR_Z_REF_10]  = cfgs[EDEN_ALWAYS_RED_Z]
cfgs[EAR_Z_REF_40]  = cfgs[EDEN_ALWAYS_RED_Z]


cfgs[EAR_SIZE_02] = cfgs[EDEN_ALWAYS_RED]
cfgs[EAR_SIZE_05] = cfgs[EDEN_ALWAYS_RED]
cfgs[EAR_SIZE_10] = cfgs[EDEN_ALWAYS_RED]

custom_apcs = [ 
  CA_REF_02, CA_REF_05, CA_REF_10, CA_REF_20, CA_REF_40,
  CA_REF_V2_02, CA_REF_V2_05, CA_REF_V2_10, CA_REF_V2_20, CA_REF_V2_40,
  CA_REF_V3_02, CA_REF_V3_05, CA_REF_V3_10, CA_REF_V3_20, CA_REF_V3_40,
  CA_A_REF_02, CA_A_REF_05, CA_A_REF_10, CA_A_REF_20, CA_A_REF_40,
  CA_B_REF_02, CA_B_REF_05, CA_B_REF_10, CA_B_REF_20, CA_B_REF_40,
  CA_C_REF_02, CA_C_REF_05, CA_C_REF_10, CA_C_REF_20, CA_C_REF_40,
  CA_X_REF_02, CA_X_REF_05, CA_X_REF_10, CA_X_REF_20, CA_X_REF_40,
  CA_Y_REF_02, CA_Y_REF_05, CA_Y_REF_10, CA_Y_REF_20, CA_Y_REF_40,
  CA_Z_REF_02, CA_Z_REF_05, CA_Z_REF_10, CA_Z_REF_20, CA_Z_REF_40,
  CA_SIZE_02, CA_SIZE_05, CA_SIZE_10,
  EAR_REF_02, EAR_REF_05, EAR_REF_10, EAR_REF_40,
  EAR_A_REF_02, EAR_A_REF_05, EAR_A_REF_10, EAR_A_REF_40,
  EAR_X_REF_02, EAR_X_REF_05, EAR_X_REF_10, EAR_X_REF_40,
  EAR_Y_REF_02, EAR_Y_REF_05, EAR_Y_REF_10, EAR_Y_REF_40,
  EAR_Z_REF_02, EAR_Z_REF_05, EAR_Z_REF_10, EAR_Z_REF_40,
  EAR_SIZE_02, EAR_SIZE_05, EAR_SIZE_10,
]

caxrefs = [
  CA_X_REF_02, CA_X_REF_05, CA_X_REF_10, CA_X_REF_20, CA_X_REF_40,
  CA_Y_REF_02, CA_Y_REF_05, CA_Y_REF_10, CA_Y_REF_20, CA_Y_REF_40,
]

#############################################################################
# named parallel configurations
#############################################################################
AVRORA_DEFAULT_FOUR        = "avrora_default_four"
AVRORA_DEFAULT_FOUR_EMON   = "avrora_default_four_emon"
ECLIPSE_DEFAULT_FOUR       = "eclipse_default_four"
ECLIPSE_DEFAULT_FOUR_EMON  = "eclipse_default_four_emon"
ECLIPSE_DEFAULT_FOUR_PGOV  = "eclipse_default_four_pgov"
ECLIPSE_MCOLOR_FOUR        = "eclipse_mcolor_four"
ECLIPSE_MCOLOR_FOUR_EMON   = "eclipse_mcolor_four_emon"
ECLIPSE_MCOLOR_FOUR_PGOV   = "eclipse_mcolor_four_pgov"
H2_DEFAULT_FOUR            = "h2_default_four"
H2_DEFAULT_FOUR_EMON       = "h2_default_four_emon"
H2_DEFAULT_FOUR_PGOV       = "h2_default_four_pgov"
H2_MCOLOR_FOUR             = "h2_mcolor_four"
H2_MCOLOR_FOUR_EMON        = "h2_mcolor_four_emon"
H2_MCOLOR_FOUR_PGOV        = "h2_mcolor_four_pgov"

PC_BENCH     = 0
PC_RUNCFG    = 1
PC_PROFCFG   = 2
PC_NUMACFG   = 3
PC_MCOLORCFG = 4
PC_MSCRAMCFG = 5

def pcfg(bench, runcfg=DEFAULT, profcfg=DEFAULT, numacfg=None, \
         mcolorcfg=None, mscramcfg=None):
  return (bench, runcfg, profcfg, numacfg, mcolorcfg, mscramcfg)

def default_four_pcfg(bench):
  return [ pcfg(bench) for x in range(4) ] 

def default_four_emon_pcfg(bench):
  return [ pcfg(bench, profcfg=EMON_OPROF), 
           pcfg(bench),
           pcfg(bench),
           pcfg(bench),
         ]

def default_four_pgov_pcfg(bench):
  return [ pcfg(bench, profcfg=POWER_GOV_OPROF), 
           pcfg(bench),
           pcfg(bench),
           pcfg(bench),
         ]

def mcolor_four_pcfg(bench):
  return [ pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN0), 
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN1),
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN2),
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN3),
         ]

def mcolor_four_emon_pcfg(bench):
  return [ pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN0, profcfg=EMON_OPROF), 
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN1),
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN2),
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN3),
         ]

def mcolor_four_pgov_pcfg(bench):
  return [ pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN0, profcfg=POWER_GOV_OPROF), 
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN1),
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN2),
           pcfg(bench, mcolorcfg=MCOLOR_NODE1_CHN3),
         ]

parcfgs = {}
parcfgs[AVRORA_DEFAULT_FOUR]       = default_four_pcfg('avrora-default')
parcfgs[AVRORA_DEFAULT_FOUR_EMON]  = default_four_emon_pcfg('avrora-default')

parcfgs[ECLIPSE_DEFAULT_FOUR]      = default_four_pcfg('eclipse-default')
parcfgs[ECLIPSE_DEFAULT_FOUR_EMON] = default_four_emon_pcfg('eclipse-default')
parcfgs[ECLIPSE_DEFAULT_FOUR_PGOV] = default_four_pgov_pcfg('eclipse-default')

parcfgs[ECLIPSE_MCOLOR_FOUR]       = mcolor_four_pcfg('eclipse-default')
parcfgs[ECLIPSE_MCOLOR_FOUR_EMON]  = mcolor_four_emon_pcfg('eclipse-default')
parcfgs[ECLIPSE_MCOLOR_FOUR_PGOV]  = mcolor_four_pgov_pcfg('eclipse-default')

parcfgs[H2_DEFAULT_FOUR]       = default_four_pcfg('h2-default')
parcfgs[H2_DEFAULT_FOUR_EMON]  = default_four_emon_pcfg('h2-default')
parcfgs[H2_DEFAULT_FOUR_PGOV]  = default_four_pgov_pcfg('h2-default')

parcfgs[H2_MCOLOR_FOUR]       = mcolor_four_pcfg('h2-default')
parcfgs[H2_MCOLOR_FOUR_EMON]  = mcolor_four_emon_pcfg('h2-default')
parcfgs[H2_MCOLOR_FOUR_PGOV]  = mcolor_four_pgov_pcfg('h2-default')

defcfgs    = [DEFAULT]
defpcfgs   = [AVRORA_DEFAULT_FOUR]

#############################################################################
# allocation point configuration mapping
#############################################################################
apcfgs = {}
#apcfgs[COLOR_ALLOCATIONS_X] = OBJ_INFO_DEFAULT
#apcfgs[EDEN_ALWAYS_RED]     = OBJ_INFO_DEFAULT
apcfgs[OBJ_INFO_GUIDED]         = OBJ_INFO_INTERVAL
apcfgs[COLOR_ALLOCATIONS]       = OBJ_INFO_INTERVAL
apcfgs[COLOR_ALLOCATIONS_A]     = OBJ_INFO_INTERVAL
apcfgs[COLOR_ALLOCATIONS_B]     = OBJ_INFO_INTERVAL
apcfgs[COLOR_ALLOCATIONS_C]     = OBJ_INFO_INTERVAL
apcfgs[COLOR_ALLOCATIONS_X]     = OBJ_INFO_INTERVAL
apcfgs[COLOR_ALLOCATIONS_Y]     = OBJ_INFO_INTERVAL
apcfgs[COLOR_ALLOCATIONS_Z]     = OBJ_INFO_INTERVAL
apcfgs[EDEN_ALWAYS_RED]         = OBJ_INFO_INTERVAL
apcfgs[EDEN_ALWAYS_RED_A]       = OBJ_INFO_INTERVAL
apcfgs[EDEN_ALWAYS_RED_X]       = OBJ_INFO_INTERVAL
apcfgs[EDEN_ALWAYS_RED_Y]       = OBJ_INFO_INTERVAL
apcfgs[EDEN_ALWAYS_RED_Z]       = OBJ_INFO_INTERVAL
apcfgs[MEMBENCH_COLOR_ALLOC]    = OBJ_INFO_INTERVAL
apcfgs[MEMBENCH_COLOR_V2]       = OBJ_INFO_INTERVAL
apcfgs[HOKM_ALLOC_BASE]         = OBJ_INFO_INTERVAL

apcfgs[COLOR_ALLOC_TUNE]        = OBJ_INFO_XSIM
apcfgs[COLOR_ALLOC_TUNE_X]      = OBJ_INFO_XSIM
apcfgs[COLOR_ALLOC_TUNE_V2]     = OBJ_INFO_XSIM

#############################################################################
# objrated
#############################################################################
objrated = {}
objrated[ADDR_TLAB_XVAL]     = 200
objrated[ADDR_TLAB_INTERVAL] = 2000
objrated[OBJ_INFO_XSIM]      = 10
objrated[OBJ_INFO_SHSIM]     = 10
objrated[OBJ_INFO_XSHSIM]    = 10

for vr in vrates:
  objrated[(OBJ_INFO_VXSIM%vr)]   = 10
  objrated[(OBJ_INFO_NVXSIM%vr)]  = 10

#############################################################################
# allocation options
#############################################################################
alloc_opts = {
  "A" : [ #optionSet("NewRatio", "10"),
          #optionSet("NewRatio", "10"),
          optionSet("NewSize", "40g"),
          optionSet("SurvivorRatio", "1"),
          minHeapSize("56g"),
          maxHeapSize("56g"),
          #minHeapSize("56g"),
          #maxHeapSize("56g"),
        ],
  "B" : [ #optionSet("NewRatio", "50"),
          optionSet("NewSize", "7g"),
          #optionSet("NewRatio", ""),
          optionSet("SurvivorRatio", "2"),
          minHeapSize("30g"),
          maxHeapSize("30g"),
        ],
  "C" : [
          optionSet("NewRatio", "1"),
          optionSet("SurvivorRatio", "1"),
          minHeapSize("4g"),
          maxHeapSize("4g"),
        ],
  "X" : [ optionSet("NewSize", "50g"),
          optionSet("MaxNewSize", "50g"),
          optionSet("SurvivorRatio", "1"),
          minHeapSize("52g"),
          maxHeapSize("52g")
        ],
  "Y" : [ optionSet("NewSize", "350m"),
          optionSet("MaxNewSize", "350m"),
          minHeapSize("2g"),
          maxHeapSize("2g"),
        ],
  "Z" : [ optionSet("NewRatio", "6"),
          minHeapSize("4g"),
          maxHeapSize("4g"),
        ],
  "StoreBench" : [ 
    optionSet("NewSize",    "75161927680"),
    optionSet("MaxNewSize", "75161927680"),
    optionSet("SurvivorRatio", "20"),
    minHeapSize("80g"),
    maxHeapSize("80g"),
  ]
}

occfgs   = [ ONLINE_COLOR_TEST, ONLINE_COLOR_TUNE, ONLINE_COLOR_TUNE_HKO,
             OCT_HKA_MS10_CDF10,
#             OCT_MS10_CD100, OCT_MS100_CD250, OCT_HKO_MS10_CD100,
#             OCT_HKO_MS100_CD250, OCT_HKO_MS50_CD500, OCT_HKA_MS10_CD100,
#             OCT_HKA_MS50_CD500, OCT_HKA_SCIMARK1, OCT_HKA_SCIMARK2,
#             OCT_HKA_SCIMARK3
           ]
#             OCT_HKO_MS10_CD100, OCT_HKO_MS10_CD200, OCT_MS50_CD50,
#             OCT_MS50_CD100, OCT_MS50_CD200, OCT_HKO_MS50_CD50,
#             OCT_HKO_MS50_CD100, OCT_HKO_MS50_CD200

tunecfgs = [ HDKM_ALLOC_TUNE, HDKM_FAST_TUNE, HDKM_FAST_TUNE2,
             COLOR_ALLOC_TUNE, COLOR_ALLOC_TUNE_X, COLOR_ALLOC_TUNE_V2,
             COLOR_ILV_TUNE, HDKD_ALLOC_TUNE, HDKD_ALLOC_TUNE_XX,
             HDKM_ALLOC_TUNE_XX, HDKM_ALLOC_TUNE_V2, HDKM_LOW_POWER,
             HDKD_CHAN_ILV, ] + occfgs

mmpcfgs   = [ HDKD_ALLOC_TUNE, CA_REF_V2_02, CA_REF_V2_05, CA_REF_V2_10,
              OCT_HKA_MS50_CD500 ]
stmmpcfgs = [ HDKD_ALLOC_BASE, HDKM_ALLOC_BASE, MEMBENCH_COLOR ]

#############################################################################
# java configurations
#############################################################################
javaRunOpts = {}
javaRunOpts[DEFAULT]   = [ optionOn(UseParallelGC) ]

javaRunOpts[MEMBENCH_DEFAULT] = javaRunOpts[DEFAULT] + [
                                  #optionOff(UseAdaptiveSizePolicy),
                                  #optionSet(NewSize, "%d" % (GB(2))),
                                  #optionSet(MaxNewSize, "%d" % (GB(2)))
                                ]

javaRunOpts[XINT]           = [ interpreterMode(),
                                optionSet(MethodHistogramCutoff, "0"),
                                optionOn(CountCompiledCalls)
                              ]

javaRunOpts[XINT_WBEC]      = [ interpreterMode(),
                                optionSet(MethodHistogramCutoff, "0"),
                                optionOn(CountCompiledCalls),
                                optionOn(InterpretModeLoopCounts)
                              ]

javaRunOpts[INTERPRET]      = [ optionSet(CompileThreshold, "0"),
                                optionSet(MethodHistogramCutoff, "0"),
                                optionOn(CountCompiledCalls)
                              ]

javaRunOpts[INTERPRET_WBEC] = [ optionSet(CompileThreshold, "0"),
                                optionSet(MethodHistogramCutoff, "0"),
                                optionOn(CountCompiledCalls),
                                optionOn(InterpretModeLoopCounts)
                              ]

javaRunOpts[XINT_CCF]       = [ optionOn(NeverCompile),
                                optionOn(InterpretModeLoopCounts),
                                optionOn(CountCompiledCalls),
                                optionSet(TCStartupIteration, "2"),
                                optionSet(MethodHistogramCutoff, "0"),
                              ]

javaRunOpts[SERVER_STEADY_BASE]  = [ optionSet(CICompilerCount, "4"),
                                     "-XX:-TieredCompilation",
                                     optionSet(TCStartupIteration, "2"),
                                     optionOn(UseVMIndication),
                                     optionSet(VMIndicationThreshold, "2"),
                                     optionOn(ResetMethodCounters),
                                     optionOff(UseOnStackReplacement),
                                   ]

javaRunOpts[TIERED_STEADY_BASE]  = [ optionSet(CICompilerCount, "4"),
                                     optionSet(CompilationPolicyChoice, "2"),
                                     optionSet(TCStartupIteration, "2"),
                                     optionOn(UseVMIndication),
                                     optionSet(VMIndicationThreshold, "2"),
                                     optionOn(ResetMethodCounters),
                                     optionOff(UseOnStackReplacement),
                                     #optionOn(PrintThreadTimes),
                                     #optionOn("ProfileInterpreter"),
                                     #optionOn("PrintMethodData"),
                                     #optionOn(PrintCompilation),
                                     #optionOn("UnlockDiagnosticVMOptions"),
                                     #optionOn("PrintCompilation2"),
                                     #optionOn("LogCompilation"),
                                     #optionOn("PrintInlining"),
                                     #optionOn("Verbose"),
                                     #optionOff(BackgroundCompilation),
                                   ]

javaRunOpts[TIERED_XSS_BASE]     = javaRunOpts[TIERED_STEADY_BASE]
javaRunOpts[SERVER_XSS_BASE]     = javaRunOpts[SERVER_STEADY_BASE]

javaRunOpts[STEADY_CCF]          = javaRunOpts[TIERED_STEADY_BASE] + [ 
                                     optionOn(SpecifiedCompilationLevels),
                                     #optionOn(PrintCompilation),
                                     #optionOn(SubmitDualCompiles),
                                   ]

javaRunOpts[STEADY_CCF_REPLAY]   = javaRunOpts[TIERED_STEADY_BASE] + [ 
                                     optionOn(SpecifiedCompilationLevels),
                                     optionOn(MethodDataReplay),
                                   ]

javaRunOpts[STEADY_REPLAY]       = javaRunOpts[TIERED_STEADY_BASE] + [ 
                                     optionOn(MethodDataReplay),
                                   ]

javaRunOpts[STEADY_REPLAY_LC]    = javaRunOpts[STEADY_REPLAY] + [ 
                                     optionOn(LoadReplayCounters),
                                   ]

javaRunOpts[REPLAY_NO_RESET]     = [ optionSet(CICompilerCount, "4"),
                                     optionSet(CompilationPolicyChoice, "2"),
                                     optionSet(TCStartupIteration, "2"),
                                     optionOn(UseVMIndication),
                                     optionSet(VMIndicationThreshold, "2"),
                                     optionOff(ResetMethodCounters),
                                     optionOn(SpecifiedCompilationLevels),
                                     optionOn(MethodDataReplay),
                                     optionOff(UseOnStackReplacement),
                                   ]

javaRunOpts[CCF_ORACLE_DIFF]     = javaRunOpts[TIERED_STEADY_BASE] + [ 
                                     optionSet(TCStartupIteration, "2"),
                                     optionOn(PrintOracleDiff),
                                   ]

for ct in ctscale:
  tier3_it  = int((Default_Tier3InvocationThreshold    * ct))
  tier3_mit = int((Default_Tier3MinInvocationThreshold * ct))
  tier3_ct  = int((Default_Tier3CompileThreshold       * ct))
  tier3_bet = int((Default_Tier3BackEdgeThreshold      * ct))
  tier4_it  = int((Default_Tier4InvocationThreshold    * ct))
  tier4_mit = int((Default_Tier4MinInvocationThreshold * ct))
  tier4_ct  = int((Default_Tier4CompileThreshold       * ct))
  tier4_bet = int((Default_Tier4BackEdgeThreshold      * ct))

  server_ct  = int(Default_CompileThreshold  * ct)
  server_bet = int(Default_BackEdgeThreshold * ct)

  javaRunOpts[(MD_CT_RECORD%ct)] = javaRunOpts[DEFAULT] + [
    optionOn(MethodDataRecord),
    #optionSet(Tier3InvocationThreshold,    tier3_it),
    #optionSet(Tier3MinInvocationThreshold, tier3_mit),
    #optionSet(Tier3CompileThreshold,       tier3_ct),
    #optionSet(Tier3BackEdgeThreshold,      tier3_bet),
    #optionSet(Tier4InvocationThreshold,    tier4_it),
    #optionSet(Tier4MinInvocationThreshold, tier4_mit),
    #optionSet(Tier4CompileThreshold,       tier4_ct),
    #optionSet(Tier4BackEdgeThreshold,      tier4_bet),
    "-XX:-TieredCompilation",
    optionSet(CompileThreshold,            server_ct),
    optionSet(BackEdgeThreshold,           server_bet),
    optionSet(CICompilerCount, "1"),
    optionOff(BackgroundCompilation),
  ]

  javaRunOpts[(MD_CT_REPLAY%ct)] = javaRunOpts[SERVER_STEADY_BASE] + [
    optionOn(MethodDataReplay),
  ]

  if ct == 0:
    javaRunOpts[(MD_CT_REPLAY%ct)].append(optionOn(NullReplayData))

  javaRunOpts[(STEADY_MD_CT_RECORD%ct)] = javaRunOpts[(MD_CT_RECORD%ct)]
  javaRunOpts[(STEADY_MD_CT_REPLAY%ct)] = javaRunOpts[(MD_CT_REPLAY%ct)]
  javaRunOpts[(XSS_MD_CT_RECORD%ct)]    = javaRunOpts[(MD_CT_RECORD%ct)]
  javaRunOpts[(XSS_MD_CT_REPLAY%ct)]    = javaRunOpts[(MD_CT_REPLAY%ct)]

for n in nscale:
  if n != 0:
    javaRunOpts[(MD_RECORD%n)] = javaRunOpts[DEFAULT] + [
      optionOn(MethodDataRecord),
      optionSet(CICompilerCount, "1"),
      "-XX:-TieredCompilation",
      optionOff(BackgroundCompilation),
    ]
    javaRunOpts[(MD_REPLAY%n)] = javaRunOpts[SERVER_STEADY_BASE] + [
      optionOn(MethodDataReplay),
    ]
  else:
    javaRunOpts[(MD_REPLAY%n)] = javaRunOpts[SERVER_STEADY_BASE] + [
      optionOn(MethodDataReplay),
      optionOn(NullReplayData),
    ]

javaRunOpts[NUMA]        = [ optionOn(UseNUMA) ]
javaRunOpts[MCOLOR_NUMA] = [ optionOn(UseNUMA),
                             optionOn(MColorNUMA),
                           ]

javaRunOpts[NUMANI]        = [ optionOn(UseNUMA),
                               optionOff(UseNUMAInterleave) 
                             ]
javaRunOpts[MCOLOR_NUMANI] = [ optionOn(UseNUMA),
                               optionOff(UseNUMAInterleave),
                               optionOn(MColorNUMA),
                             ]

javaRunOpts[GC_GEN_COLOR]  = [ #minHeapSize(gcminheap),
                               #maxHeapSize(gcmaxheap),
                               optionOn(PowerSampleGC),
                               #optionOn(TimeStampGC),
                               #optionOn(UseSerialGC),
                               #optionSet(NewSize,gcnewsize),
                               optionSet(TenuredGenTrays,gctrays),
                               #optionOn(PrintHeapAtGC)
                              ]

javaRunOpts[GC_GEN_COLOR_CLEAN] = [ #minHeapSize(gcminheap),
                                    #maxHeapSize(gcmaxheap),
                                    optionOn(PowerSampleGC),
                                    #optionOn(UseSerialGC),
                                    #optionSet(NewSize,gcnewsize),
                                  ]

javaRunOpts[DEFAULT_PSGC]  = [ optionOn(PowerSampleGC) ]

javaRunOpts[TENURED_GEN_N1C3] = [ optionSet(TenuredGenTrays,"14,15") ]
javaRunOpts[TEN_GEN_PSGC]     = [ optionOn(PowerSampleGC), optionSet(TenuredGenTrays,"14,15") ]

javaRunOpts[ECD_TENURED_GEN_N1C0]  = [ optionSet(TenuredGenTrays,"10,11") ]
javaRunOpts[ECD_TENURED_GEN_N1C1]  = [ optionSet(TenuredGenTrays,"8,9") ]
javaRunOpts[ECD_TENURED_GEN_N1C2]  = [ optionSet(TenuredGenTrays,"6,7") ]

javaRunOpts[COLORED_SPACES]        = [ optionOn(UseColoredSpaces),
                                       optionSet(ParallelGCThreads, "1"),
                                     ]

javaRunOpts[COLORED_SPACES_X]      = [ optionOn(UseColoredSpaces),
                                       optionOff(ApplyColorToSpaces),
                                       optionSet(ParallelGCThreads, "1"),
                                     ]

javaRunOpts[MEMBENCH_ORGANIZE]     = javaRunOpts[COLORED_SPACES] + [
                                       optionOn(MemBenchOrganize),
                                       optionOff(UseAdaptiveSizePolicy),
                                       #optionOff(MColorColoredSpacePages),
                                       #optionSet(RedMemoryTrays, "8,9"),
                                       #optionSet(BlueMemoryTrays, "10-15"),
                                       optionSet(RedMemoryTrays, "0,1"),
                                       optionSet(BlueMemoryTrays, "2-7"),
                                       optionSet(NewSize, "%d" % (GB(2))),
                                       optionSet(MaxNewSize, "%d" % (GB(2)))
                                     ]

javaRunOpts[MEMBENCH_ILV_ORGANIZE] = javaRunOpts[COLORED_SPACES] + [
                                       optionOn(MemBenchOrganize),
                                       optionOff(UseAdaptiveSizePolicy),
                                       #optionOff(MColorColoredSpacePages),
                                       optionOn(RedMemoryInterleave),
                                       optionSet(RedMemoryTrays, "10,12"),
                                       optionOn(BlueMemoryInterleave),
                                       optionSet(BlueMemoryTrays, "8,9,11,13,14,15"),
                                       optionSet(NewSize, "%d" % (GB(2))),
                                       optionSet(MaxNewSize, "%d" % (GB(2)))
                                     ]

javaRunOpts[MEMBENCH_XILV]         = javaRunOpts[COLORED_SPACES] + [
                                       optionOn(MemBenchOrganize),
                                       optionOff(UseAdaptiveSizePolicy),
                                       #optionOff(MColorColoredSpacePages),
                                       optionOn(RedMemoryInterleave),
                                       optionSet(RedMemoryTrays, "8-15"),
                                       optionOn(BlueMemoryInterleave),
                                       optionSet(BlueMemoryTrays, "8-15"),
                                       optionSet(NewSize, "%d" % (GB(2))),
                                       optionSet(MaxNewSize, "%d" % (GB(2)))
                                     ]

javaRunOpts[ORGANIZE_OBJECTS]      = [ optionOn(UseColoredSpaces),
                                       optionOn(OrganizeObjects),
                                       optionSet(ParallelGCThreads, "1"),
                                       optionSet(InitialTenuringThreshold, "10000"),
                                       optionSet(MaxTenuringThreshold, "10000"),
                                       optionOff(UseAdaptiveSizePolicy),
                                       optionOn(PrintHeapAtGC),
                                       optionOn(PrintGCDetails),
                                       optionOn(SurvivorsAlwaysRed),
                                       #optionOn(PrintReferencedObjects)
                                       #optionOn(PrintTenuringDistribution)
                                     ]

javaRunOpts[DEFAULT_ALLOCATIONS]   = [ optionOff(UseCompressedOops),
                                       #optionSet(NewSize, "%d" % (GB(4))),
                                       #optionSet(MaxNewSize, "%d" % (GB(4))),
                                       #optionSet(NewSize, "%d" % (MB(200))),
                                       #optionSet(MaxNewSize, "%d" % (MB(200))),
                                       #optionSet(NewSize, "%d" % (MB(672))),
                                       #optionSet(MaxNewSize, "%d" % (MB(672))),
                                       #optionSet(NewSize, "%d" % (MB(50))),
                                       #optionSet(MaxNewSize, "%d" % (MB(50))),
                                       optionSet("NewRatio", "50"),
                                       optionOn(PrintHeapAtGC),
                                       optionOn(PrintGCDetails),
                                       optionSet(ParallelGCThreads, "1"),
                                       optionSet(InitialTenuringThreshold, "10000"),
                                       optionSet(MaxTenuringThreshold, "10000"),
                                       optionOff(UseAdaptiveSizePolicy),
                                       optionOn(GCTimers),
                                       optionOn(CompileSlowAllocations),
                                       #optionOff("UseTLAB"),
                                       optionOff("RewriteBytecodes"),
                                       optionOff("RewriteFrequentPairs"),
                                       optionOff("UseFastAccessorMethods"),
                                       #optionOn("UnlockDiagnosticVMOptions"),
                                       #optionSet("ScavengeRootsInCode", "0"),
                                       optionOn("AlwaysCountInvocations"),
                                       optionOn("DisableMajorGC")
                                       #optionOff(MColorColoredSpacePages),
                                       #interpreterMode()
                                     ]

javaRunOpts[COLOR_ALLOCATIONS]     = javaRunOpts[DEFAULT_ALLOCATIONS] + [
                                       optionOn(UseColoredSpaces),
                                       optionOn(ColorObjectAllocations),
                                       optionSet(RedMemoryTrays, "8,9"),
                                       optionSet(BlueMemoryTrays, "10-15"),
                                       #optionSet(BlueMemoryTrays, "10,11"),
                                       #optionOff(MColorColoredSpacePages),
                                       #interpreterMode()
                                     ]

javaRunOpts[HEAP_TRIAL]             = [ #optionOff(UseCompressedOops),
                                        optionOn(PrintHeapAtGC),
                                        optionOn(PrintGCDetails),
                                        optionOn(UseParallelGC),
                                        #optionOff(TieredCompilation),
                                        optionOn(PreventTenuring),
                                        #optionSet(ParallelGCThreads, "1"),
                                        #optionSet(InitialTenuringThreshold, "16"),
                                        #optionSet(MaxTenuringThreshold, "16"),
                                        #optionOff(UseAdaptiveSizePolicy),
                                        optionOn(CompileSlowAllocations),
                                        #optionOff("UseTLAB"),
                                        #optionOn("DisableMajorGC"),
                                        optionOn(GCTimers),
                                        #optionOff("RewriteBytecodes"),
                                        #optionOff("RewriteFrequentPairs"),
                                        #optionOff("UseFastAccessorMethods"),
                                        #interpreterMode()
                                      ]

# MRJ: this was the config before changing it to run simplebb_benches
#javaRunOpts[__DEF_ALLOC_BASE]       = [ optionOn(UseParallelGC),
#                                        #optionOff(TieredCompilation),
#                                        optionSet(ParallelGCThreads, "1"),
#                                        #optionOff(UseCompressedOops),
#                                        optionOn(PrintHeapAtGC),
#                                        optionOn(PrintGCDetails),
#                                        #optionSet(InitialTenuringThreshold, "10000"),
#                                        #optionSet(MaxTenuringThreshold, "10000"),
#                                        #optionSet(InitialTenuringThreshold, "16"),
#                                        #optionSet(MaxTenuringThreshold, "16"),
#                                        optionOff(UseAdaptiveSizePolicy),
#                                        optionOn(DisableMajorGC),
#                                        optionOn(PreventTenuring),
#                                        optionOn(GCTimers),
#                                      ]

javaRunOpts[__DEF_ALLOC_BASE]       = [ optionOn(UseParallelGC),
                                        #optionOff(TieredCompilation),
                                        optionSet(ParallelGCThreads, "1"),
                                        #optionOff(UseCompressedOops),
                                        optionOn(PrintHeapAtGC),
                                        optionOn(PrintGCDetails),
                                        optionSet(InitialTenuringThreshold, "10000"),
                                        optionSet(MaxTenuringThreshold, "10000"),
                                        #optionSet(InitialTenuringThreshold, "16"),
                                        #optionSet(MaxTenuringThreshold, "16"),
                                        optionOff(UseAdaptiveSizePolicy),
                                        #optionOn(DisableMajorGC),
                                        #optionOn(PreventTenuring),
                                        optionOn(GCTimers),
                                      ]

javaRunOpts[DEF_ALLOC_BASE]        = javaRunOpts[__DEF_ALLOC_BASE] + [
                                       optionOn(CompileSlowAllocations),
                                       optionOn(SlowAllocations),
                                       optionSet(TLABSize, str(KB(32))),
                                       optionOff(ResizeTLAB),
                                     ]

javaRunOpts[HDKM_ALLOC_BASE]       = javaRunOpts[DEF_ALLOC_BASE] + [
                                       optionOn(RedHeap),
                                       optionOff(FreeColoredSpacePages),
                                       optionSet(RedMemoryTrays, "4-7"),
                                       optionOn(RedMemoryInterleave),
                                     ]

javaRunOpts[HDKM_LOW_POWER]       = javaRunOpts[DEF_ALLOC_BASE] + [
                                       optionOn(RedHeap),
                                       optionOff(FreeColoredSpacePages),
                                       optionSet(RedMemoryTrays, "0-3"),
                                       #optionOn(RedMemoryInterleave),
                                       #optionOff(MColorColoredSpacePages),
                                     ]

javaRunOpts[HDKM_ALLOC_BASE_V2]    = javaRunOpts[DEF_ALLOC_BASE] + [
                                       optionOn(RedHeap),
                                       optionOff(FreeColoredSpacePages),
                                       optionSet(RedMemoryTrays, "4-7"),
                                       optionOn(RedMemoryInterleave),
                                       #optionOff(MColorColoredSpacePages),
                                     ]

javaRunOpts[HDKD_ALLOC_BASE]       = javaRunOpts[DEF_ALLOC_BASE]
javaRunOpts[HDKD_FAST_BASE]        = javaRunOpts[__DEF_ALLOC_BASE]
javaRunOpts[HDKD_ALLOC_TUNE]       = javaRunOpts[DEF_ALLOC_BASE]
javaRunOpts[HDKD_ALLOC_TUNE_XX]    = javaRunOpts[DEF_ALLOC_BASE]
javaRunOpts[HDKD_CHAN_ILV]         = javaRunOpts[DEF_ALLOC_BASE]
javaRunOpts[HDKM_ALLOC_BASE_XX]    = javaRunOpts[DEF_ALLOC_BASE]
javaRunOpts[HDKM_ALLOC_TUNE]       = javaRunOpts[HDKM_ALLOC_BASE]
javaRunOpts[HDKM_ALLOC_TUNE_V2]    = javaRunOpts[HDKM_ALLOC_BASE_V2]
javaRunOpts[HDKD_FAST_TUNE]        = javaRunOpts[__DEF_ALLOC_BASE]

javaRunOpts[HDKM_FAST_TUNE]        = javaRunOpts[__DEF_ALLOC_BASE] + [
                                       optionOn(RedHeap),
                                       optionOff(FreeColoredSpacePages),
                                       optionSet(RedMemoryTrays, "0-7"),
                                       optionOn(RedMemoryInterleave),
                                       #optionOff(MColorColoredSpacePages),
                                     ]

javaRunOpts[HDKM_FAST_TUNE2]       = javaRunOpts[HDKM_ALLOC_BASE]
#javaRunOpts[HDKM_FAST_TUNE2]       = javaRunOpts[__DEF_ALLOC_BASE] + [
#                                       optionOn(RedHeap),
#                                       optionOff(FreeColoredSpacePages),
#                                       optionSet(RedMemoryTrays, "8-15"),
#                                       optionOn(RedMemoryInterleave),
#                                       optionOn(CompileSlowAllocations),
#                                       #optionOff(MColorColoredSpacePages),
#                                     ]

javaRunOpts[COLOR_ILV_BASE]        = javaRunOpts[DEF_ALLOC_BASE] + [
                                       optionOn(UseColoredSpaces),
                                       optionOn(ColorObjectAllocations),
                                       optionSet(RedMemoryTrays, "8-15"),
                                       optionSet(BlueMemoryTrays, "8-15"),
                                       optionOn(RedMemoryInterleave),
                                       optionOn(BlueMemoryInterleave),
                                       optionOn(RandomHeapColors),
                                       optionSet(RedObjectRatio,"1.01"),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                       #optionOn("PrintTLAB"),
                                       #optionOn("TLABStats"),
                                       #optionOff(MColorColoredSpacePages),
                                     ]

javaRunOpts[COLOR_ILV_TUNE]        = javaRunOpts[COLOR_ILV_BASE]


javaRunOpts[HOKM_ALLOC_BASE]       = javaRunOpts[DEF_ALLOC_BASE] + [
                                       optionOn(UseColoredSpaces),
                                       optionOn(ColorObjectAllocations),
                                       optionSet(RedMemoryTrays, "8,9"),
                                       optionSet(BlueMemoryTrays, "10-15"),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                     ]

javaRunOpts[TZ_COLOR_AP]            = javaRunOpts[DEFAULT] + [
                                       optionOn(UseColoredSpaces),
                                       optionSet(ParallelGCThreads, 1),
                                       optionOn(PrintHeapAtGC),
                                       optionOn(PrintGCDetails),
                                       optionOn(UseAdaptiveSizePolicy),
                                       optionOn(DisableMajorGC),
                                       optionOn(PreventTenuring),
                                       optionOn(GCTimers),
                                       optionOn(CompileSlowAllocations),
                                       optionOn(SlowAllocations),
                                       optionOn(ColorObjectAllocations),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                       optionSet(TLABSize, 32768),
                                       optionOff(ResizeTLAB)
                                     ]

javaRunOpts[TZ_COLOR_AP_98]          = javaRunOpts[TZ_COLOR_AP]
javaRunOpts[TZ_COLOR_AP_95]          = javaRunOpts[TZ_COLOR_AP]


javaRunOpts[COLOR_ALLOC_BASE]      = javaRunOpts[DEF_ALLOC_BASE] + [
                                       #optionOff("TieredCompilation"),
                                       optionOn(UseColoredSpaces),
                                       optionOn(ColorObjectAllocations),
                                       optionSet(RedMemoryTrays, "4-5"),
                                       optionSet(BlueMemoryTrays, "6-7"),
                                       optionOn(RedMemoryInterleave),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                     ]

javaRunOpts[COLOR_ALLOC_BASE_V2]    = javaRunOpts[DEF_ALLOC_BASE] + [
                                       optionOn(UseColoredSpaces),
                                       optionOn(ColorObjectAllocations),
                                       optionSet(RedMemoryTrays, "0-1"),
                                       optionSet(BlueMemoryTrays, "2-3"),
                                       optionOn(RedMemoryInterleave),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                     ]

javaRunOpts[COLOR_ALLOC_BASE_X]    = javaRunOpts[DEF_ALLOC_BASE] + [
                                       #optionOn(OnlyOrganizeGC),
                                       #optionOn(OrganizeObjects),
                                       optionOn(UseColoredSpaces),
                                       optionOn(ColorObjectAllocations),
                                       optionSet(RedMemoryTrays, "0-3"),
                                       optionSet(BlueMemoryTrays, "4-7"),
                                       optionOn(RedMemoryInterleave),
                                       optionOn(BlueMemoryInterleave),
                                       #optionSet(RedMemoryTrays, "8-15"),
                                       #optionSet(BlueMemoryTrays, "8-15"),
                                       #optionOn(RedMemoryInterleave),
                                       #optionOn(BlueMemoryInterleave),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                       #optionSet(UnknownAPColor,"1"),
                                       #optionSet(UnknownObjectColor,"1"),
                                       #optionSet(BlueMemoryTrays, "10,11"),
                                       #optionOff(MColorColoredSpacePages),
                                       #interpreterMode()
                                     ]

javaRunOpts[COLOR_ALLOC_TUNE]      = javaRunOpts[COLOR_ALLOC_BASE]
javaRunOpts[COLOR_ALLOC_TUNE_V2]   = javaRunOpts[COLOR_ALLOC_BASE_V2]
javaRunOpts[COLOR_ALLOC_TUNE_X]    = javaRunOpts[COLOR_ALLOC_BASE_X]

javaRunOpts[DEF_MEMBENCH_COLOR]    = javaRunOpts[DEF_ALLOC_BASE] + [
                                       optionOn(UseColoredSpaces),
                                       optionOn(ColorObjectAllocations),
                                     ]

javaRunOpts[MEMBENCH_COLOR_ALLOC]  = javaRunOpts[DEF_MEMBENCH_COLOR] + [
                                       optionSet(RedMemoryTrays, "0,1"),
                                       optionSet(BlueMemoryTrays, "2-7"),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                     ]

javaRunOpts[MEMBENCH_COLOR_V2]     = javaRunOpts[DEF_MEMBENCH_COLOR] + [
                                       optionSet(RedMemoryTrays, "0"),
                                       optionSet(BlueMemoryTrays, "1-3"),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                     ]

javaRunOpts[BWBENCH_DEFAULT]        = javaRunOpts[DEF_ALLOC_BASE]
javaRunOpts[BWBENCH_COLOR]          = javaRunOpts[DEF_MEMBENCH_COLOR] + [
                                       optionSet(RedMemoryTrays, "8,9"),
                                       optionSet(BlueMemoryTrays, "10-15"),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                      ]
javaRunOpts[BWBENCH_COLOR_ILV]      = javaRunOpts[DEF_MEMBENCH_COLOR] + [
                                       optionSet(RedMemoryTrays, "8-15"),
                                       optionOn(RedMemoryInterleave),
                                       optionSet(BlueMemoryTrays, "8-15"),
                                       optionOn(BlueMemoryInterleave),
                                       optionSet(UnknownAPColor,"0"),
                                       optionSet(UnknownObjectColor,"0"),
                                      ]

javaRunOpts[ONLINE_COLOR_TUNE]     = javaRunOpts[COLOR_ALLOC_BASE] + [
                                       optionOn(SampleCallStacksAtInterval),
                                       #optionSet(MaxMethodTemperature, "4"),
                                       #optionSet(MaxKlassTemperature, "4"),
                                     ]

javaRunOpts[ONLINE_COLOR_TUNE_HKO] = javaRunOpts[ONLINE_COLOR_TUNE] + [
                                       optionOn(HotKlassOrganize),
                                     ]

javaRunOpts[ONLINE_COLOR_TUNE_HKA] = javaRunOpts[ONLINE_COLOR_TUNE] + [
                                       optionOn(HotKlassOrganize),
                                       optionOn(HotMethodAllocate),
                                       optionOn(HotKlassAllocate),
                                     ]

javaRunOpts[ONLINE_COLOR_TEST]     = javaRunOpts[HDKM_ALLOC_TUNE]
#javaRunOpts[ONLINE_COLOR_TEST]     = javaRunOpts[ONLINE_COLOR_TUNE_HKO] + [
#                                       #optionOn(ScavengeAtRegularIntervals),
#                                       #optionSet(ScavengeInterval, "5000"),
#                                       optionSet(MethodSamplerInterval, "1000"),
#                                       optionSet(CoolDownInterval, "10000"),
#                                       optionSet(MaxMethodTemperature, "2"),
#                                       optionSet(MaxKlassTemperature, "2"),
#                                     ]

javaRunOpts[OCT_HKA_MS10_CDF10] = javaRunOpts[ONLINE_COLOR_TUNE_HKA] + [
                                    optionSet(CallStackSampleInterval, "10"),
                                    optionSet(CoolDownFactor, "10"),
                                    optionSet(MaxMethodTemperature, "2"),
                                    optionSet(MaxKlassTemperature, "2"),
                                ]

#javaRunOpts[OCT_MS10_CD100]    = javaRunOpts[ONLINE_COLOR_TUNE] + [
#                                  optionSet(MethodSamplerInterval, "10"),
#                                  optionSet(CoolDownInterval, "100"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_MS100_CD250]    = javaRunOpts[ONLINE_COLOR_TUNE] + [
#                                  optionSet(MethodSamplerInterval, "100"),
#                                  optionSet(CoolDownInterval, "250"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKO_MS10_CD100] = javaRunOpts[ONLINE_COLOR_TUNE_HKO] + [
#                                  optionSet(MethodSamplerInterval, "10"),
#                                  optionSet(CoolDownInterval, "100"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKO_MS100_CD250] = javaRunOpts[ONLINE_COLOR_TUNE_HKO] + [
#                                     #optionOn(ScavengeAtRegularIntervals),
#                                     #optionSet(ScavengeInterval, "10000"),
#                                     optionSet(MethodSamplerInterval, "100"),
#                                     optionSet(CoolDownInterval, "250"),
#                                     optionSet(MaxMethodTemperature, "2"),
#                                     optionSet(MaxKlassTemperature, "2"),
#                                   ]
#
#javaRunOpts[OCT_HKO_MS50_CD500] = javaRunOpts[ONLINE_COLOR_TUNE_HKO] + [
#                                     #optionOn(ScavengeAtRegularIntervals),
#                                     #optionSet(ScavengeInterval, "10000"),
#                                     optionSet(MethodSamplerInterval, "50"),
#                                     optionSet(CoolDownInterval, "500"),
#                                     optionSet(MaxMethodTemperature, "2"),
#                                     optionSet(MaxKlassTemperature, "2"),
#                                   ]

#javaRunOpts[OCT_HKA_MS10_CD100] = javaRunOpts[ONLINE_COLOR_TUNE_HKA] + [
#                                  optionSet(MethodSamplerInterval, "10"),
#                                  optionSet(CoolDownInterval, "100"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKA_MS50_CD500] = javaRunOpts[ONLINE_COLOR_TUNE_HKA] + [
#                                  optionSet(MethodSamplerInterval, "50"),
#                                  optionSet(CoolDownInterval, "500"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKA_SCIMARK1]  = javaRunOpts[ONLINE_COLOR_TUNE_HKA] + [
#                                  optionSet(MethodSamplerInterval, "2000"),
#                                  optionSet(CoolDownInterval, "20000"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKA_SCIMARK2]  = javaRunOpts[ONLINE_COLOR_TUNE_HKA] + [
#                                  optionSet(MethodSamplerInterval, "10000"),
#                                  optionSet(CoolDownInterval, "20000"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKA_SCIMARK3]  = javaRunOpts[ONLINE_COLOR_TUNE_HKA] + [
#                                  optionSet(MethodSamplerInterval, "20000"),
#                                  optionSet(CoolDownInterval, "40000"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]

#javaRunOpts[OCT_MS10_CD50]     = javaRunOpts[ONLINE_COLOR_TUNE] + [
#                                  optionSet(MethodSamplerInterval, "10"),
#                                  optionSet(CoolDownInterval, "50"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                ]
#
#
#javaRunOpts[OCT_MS10_CD200]    = javaRunOpts[ONLINE_COLOR_TUNE] + [
#                                  optionSet(MethodSamplerInterval, "10"),
#                                  optionSet(CoolDownInterval, "200"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKO_MS10_CD50] = javaRunOpts[ONLINE_COLOR_TUNE_HKO] + [
#                                  optionSet(MethodSamplerInterval, "10"),
#                                  optionSet(CoolDownInterval, "50"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKO_MS10_CD200] = javaRunOpts[ONLINE_COLOR_TUNE_HKO] + [
#                                  optionSet(MethodSamplerInterval, "10"),
#                                  optionSet(CoolDownInterval, "200"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_MS50_CD50]     = javaRunOpts[ONLINE_COLOR_TUNE] + [
#                                  optionSet(MethodSamplerInterval, "50"),
#                                  optionSet(CoolDownInterval, "50"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_MS50_CD100]    = javaRunOpts[ONLINE_COLOR_TUNE] + [
#                                  optionSet(MethodSamplerInterval, "50"),
#                                  optionSet(CoolDownInterval, "100"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKO_MS50_CD50] = javaRunOpts[ONLINE_COLOR_TUNE_HKO] + [
#                                  optionSet(MethodSamplerInterval, "50"),
#                                  optionSet(CoolDownInterval, "50"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]
#
#javaRunOpts[OCT_HKO_MS50_CD100] = javaRunOpts[ONLINE_COLOR_TUNE_HKO] + [
#                                  optionSet(MethodSamplerInterval, "50"),
#                                  optionSet(CoolDownInterval, "100"),
#                                  optionSet(MaxMethodTemperature, "2"),
#                                  optionSet(MaxKlassTemperature, "2"),
#                                ]

javaRunOpts[DEFAULT_ALLOCATIONS_A]  = javaRunOpts[DEF_ALLOC_BASE]   + alloc_opts["A"]
javaRunOpts[HDKM_ALLOCATIONS_A]     = javaRunOpts[HDKM_ALLOC_BASE]  + alloc_opts["A"]
javaRunOpts[HDKM_ALLOCATIONS_B]     = javaRunOpts[HDKM_ALLOC_BASE]  + alloc_opts["B"]
javaRunOpts[HDKD_ALLOCATIONS_A]     = javaRunOpts[DEF_ALLOC_BASE]   + alloc_opts["A"]
javaRunOpts[COLOR_ALLOCATIONS_A]    = javaRunOpts[COLOR_ALLOC_BASE] + alloc_opts["A"]
javaRunOpts[COLOR_ALLOCATIONS_B]    = javaRunOpts[COLOR_ALLOC_BASE] + alloc_opts["B"]
javaRunOpts[COLOR_ALLOCATIONS_C]    = javaRunOpts[COLOR_ALLOC_BASE] + alloc_opts["B"] + [ optionOn(OrganizeObjects) ]
javaRunOpts[DEFAULT_ALLOCATIONS_X]  = javaRunOpts[DEF_ALLOC_BASE]   + alloc_opts["X"]
javaRunOpts[HDKM_ALLOCATIONS_X]     = javaRunOpts[DEF_ALLOC_BASE]   + alloc_opts["X"]
#javaRunOpts[COLOR_ALLOCATIONS_X]    = javaRunOpts[COLOR_ALLOC_BASE] + alloc_opts["X"] + [ optionOn(OrganizeObjects) ]
javaRunOpts[COLOR_ALLOCATIONS_X]    = javaRunOpts[COLOR_ALLOC_BASE] + alloc_opts["X"]
javaRunOpts[DEFAULT_ALLOCATIONS_Y]  = javaRunOpts[DEF_ALLOC_BASE]   + alloc_opts["Y"]
javaRunOpts[HDKM_ALLOCATIONS_Y]     = javaRunOpts[DEF_ALLOC_BASE]   + alloc_opts["Y"]
javaRunOpts[COLOR_ALLOCATIONS_Y]    = javaRunOpts[COLOR_ALLOC_BASE] + alloc_opts["Y"]
javaRunOpts[DEFAULT_ALLOCATIONS_Z]  = javaRunOpts[DEF_ALLOC_BASE]   + alloc_opts["Z"]
javaRunOpts[HDKM_ALLOCATIONS_Z]     = javaRunOpts[DEF_ALLOC_BASE]   + alloc_opts["Z"]
javaRunOpts[COLOR_ALLOCATIONS_Z]    = javaRunOpts[COLOR_ALLOC_BASE] + alloc_opts["Z"]

javaRunOpts[EDEN_ALWAYS_RED]        = javaRunOpts[COLOR_ALLOCATIONS]   + [ optionOn(EdenAlwaysRed) ]
javaRunOpts[EDEN_ALWAYS_RED_A]      = javaRunOpts[COLOR_ALLOCATIONS_A] + [ optionOn(EdenAlwaysRed) ]
javaRunOpts[EDEN_ALWAYS_RED_X]      = javaRunOpts[COLOR_ALLOCATIONS_X] + [ optionOn(EdenAlwaysRed) ]
javaRunOpts[EDEN_ALWAYS_RED_Y]      = javaRunOpts[COLOR_ALLOCATIONS_Y] + [ optionOn(EdenAlwaysRed) ]
javaRunOpts[EDEN_ALWAYS_RED_Z]      = javaRunOpts[COLOR_ALLOCATIONS_Z] + [ optionOn(EdenAlwaysRed) ]

#javaRunOpts[DEFAULT_ALLOCATIONS_X] = javaRunOpts[DEF_ALLOC_BASE] + alloc_opts["X"]
#                                     [
#                                       #optionSet("NewSize", "22g"),
#                                       #optionSet("MaxNewSize", "22g"),
#                                       #optionSet("SurvivorRatio", "100"),
#                                       #minHeapSize("28g"),
#                                       #maxHeapSize("28g")
#                                       optionSet("NewRatio", "50"),
#                                       minHeapSize("30g"),
#                                       maxHeapSize("30g")
#                                     ]
#
#javaRunOpts[COLOR_ALLOCATIONS_X]  = javaRunOpts[COLOR_ALLOC_BASE] + alloc_opts["X"]
#                                     [
#                                       optionSet("NewSize", "22g"),
#                                       optionSet("MaxNewSize", "22g"),
#                                       optionSet("SurvivorRatio", "100"),
#                                       minHeapSize("28g"),
#                                       maxHeapSize("28g")
#                                     ]
#
#javaRunOpts[DEFAULT_ALLOCATIONS_Y] = javaRunOpts[DEF_ALLOC_BASE] + [
#                                       optionSet("NewSize", "350m"),
#                                       optionSet("MaxNewSize", "350m"),
#                                       minHeapSize("2g"),
#                                       maxHeapSize("2g"),
#                                     ]
#
#javaRunOpts[COLOR_ALLOCATIONS_Y]  = javaRunOpts[COLOR_ALLOC_BASE] + [
#                                       optionSet("NewSize", "350m"),
#                                       optionSet("MaxNewSize", "350m"),
#                                       minHeapSize("2g"),
#                                       maxHeapSize("2g"),
#                                     ]
#
#javaRunOpts[DEFAULT_ALLOCATIONS_Z] = javaRunOpts[DEF_ALLOC_BASE] + [
#                                       #optionSet("NewSize", "650m"),
#                                       #optionSet("MaxNewSize", "650m"),
#                                       optionSet("NewRatio", "6"),
#                                       minHeapSize("4g"),
#                                       maxHeapSize("4g"),
#                                     ]
#
#javaRunOpts[COLOR_ALLOCATIONS_Z]   = javaRunOpts[COLOR_ALLOC_BASE] + [
#                                       #optionSet("NewSize", "650m"),
#                                       #optionSet("MaxNewSize", "650m"),
#                                       optionSet("NewRatio", "50"),
#                                       minHeapSize("30g"),
#                                       maxHeapSize("30g"),
#                                     ]

javaRunOpts[COLOR_ALLOCATIONS_ILV] = javaRunOpts[DEFAULT_ALLOCATIONS_X] + [
                                       #optionOn(OnlyOrganizeGC),
                                       #optionOn(OrganizeObjects),
                                       optionOn(UseColoredSpaces),
                                       optionOn(ColorObjectAllocations),
                                       optionSet(RedMemoryTrays, "0-7"),
                                       optionOn(RedMemoryInterleave),
                                       optionSet(BlueMemoryTrays, "0-7"),
                                       optionOn(BlueMemoryInterleave),
                                       #optionSet("NewRatio", "50"),
                                       #minHeapSize("30g"),
                                       #maxHeapSize("30g"),
                                       #optionSet(RedMemoryTrays, "8-11"),
                                       #optionOn(RedMemoryInterleave),
                                       #optionSet(BlueMemoryTrays, "12-15"),
                                     ]

#javaRunOpts[COLOR_ALLOCATIONS]     = [ optionOff(UseCompressedOops),
#                                       #optionSet(NewSize, "%d" % (GB(4))),
#                                       #optionSet(MaxNewSize, "%d" % (GB(4))),
#                                       optionSet(NewSize, "%d" % (MB(662))),
#                                       optionSet(MaxNewSize, "%d" % (MB(662))),
#                                       optionOn(PrintHeapAtGC),
#                                       optionOn(PrintGCDetails),
#                                       optionSet(ParallelGCThreads, "1"),
#                                       optionSet(InitialTenuringThreshold, "10000"),
#                                       optionSet(MaxTenuringThreshold, "10000"),
#                                       optionOff(UseAdaptiveSizePolicy),
#                                       optionOn(GCTimers),
#                                       optionOn(CompileSlowAllocations),
#                                       #optionOn(OrganizeObjects),
#                                       optionOff("UseTLAB"),
#                                       optionOff("RewriteBytecodes"),
#                                       optionOff("RewriteFrequentPairs"),
#                                       optionOff("UseFastAccessorMethods"),
#                                       optionOn("AlwaysCountInvocations"),
#                                       optionOn(UseColoredSpaces),
#                                       optionOn(ColorObjectAllocations),
#                                       optionSet(RedMemoryTrays, "8,9"),
#                                       optionSet(BlueMemoryTrays, "10-15"),
#                                       #optionOff(MColorColoredSpacePages),
#                                       #interpreterMode()
#                                     ]
#
#javaRunOpts[EDEN_ALWAYS_RED]       = javaRunOpts[COLOR_ALLOCATIONS] + \
#                                     [ optionOn(EdenAlwaysRed),
#                                     ]
#
#javaRunOpts[EDEN_ALWAYS_RED_X]     = javaRunOpts[COLOR_ALLOCATIONS_X] + \
#                                     [ optionOn(EdenAlwaysRed),
#                                     ]

javaRunOpts[RED_OBJECTS]           = javaRunOpts[ORGANIZE_OBJECTS] + [
                                       optionOn(SurvivorsAlwaysRed),
                                     ]

javaRunOpts[BLUE_OBJECTS]          = javaRunOpts[ORGANIZE_OBJECTS] + [
                                       optionOn(SurvivorsAlwaysBlue),
                                     ]

javaRunOpts[SAMPLE_STACKS_VAL]      = javaRunOpts[DEFAULT] + [
                                       optionOn(SampleCallStacksAtInterval),
                                       optionSet(CallStackSampleInterval, "10"),
                                     ]

javaRunOpts[SAMPLE_STACKS_CON]      = javaRunOpts[DEFAULT] + [
                                       optionOn(SampleCallStacksContinuous),
                                     ]

javaRunOpts[SAMPLE_STACKS_HPROF]    = javaRunOpts[DEFAULT] + [
                                       "-agentlib:hprof=cpu=samples,depth=1",
                                     ]

# the GC options and the special alloc_opts are necessary for eclipse and
# tradebeans/tradesoap (otherwise GC comes in and clears out some of the
# klasses before we can print them out)
#
javaRunOpts[METHOD_KAL_INFO]      = javaRunOpts[DEFAULT] + [
                                        optionOn(PrintKlassAccessLists),
                                        optionOff(BackgroundCompilation),
                                        #optionOn(JustDoIt),
                                        optionOff(Inline),
                                        #optionOn(CountCompiledCalls),
                                        #optionSet(MethodHistogramCutoff,"0"),
                                        #optionOn("PrintCompilation"),
                                        #optionOn("UnlockDiagnosticVMOptions"),
                                        #optionOn("PrintCompilation2"),
                                        optionOff(TieredCompilation),
                                        optionSet(CompileThreshold, "1"),
                                        #optionOn("DisableMajorGC"),
                                        #optionOn(OnlyIntervalGC),
                                     ]# + alloc_opts["A"]
javaRunOpts[METHOD_KAL_INFOX]      = javaRunOpts[METHOD_KAL_INFO]
javaRunOpts[METHOD_KAL_INFONX]     = javaRunOpts[METHOD_KAL_INFO]

javaRunOpts[METHOD_DATA_RECORD]    = javaRunOpts[DEFAULT] + [
                                       optionOn(MethodDataRecord),
                                     ]

javaRunOpts[METHOD_DATA_REPLAY]    = javaRunOpts[DEFAULT] + [
                                       optionOn(MethodDataReplay),
                                       optionSet(TCStartupIteration, "2"),
                                       optionOn(UseVMIndication),
                                       optionSet(VMIndicationThreshold, "2"),
                                       #optionOff(TieredCompilation),
                                       #optionSet("CICompilerCount", "1"),
                                     ]

javaProfOpts = {}
javaProfOpts[DEFAULT]           = []
javaProfOpts[PCM_POWER]         = javaProfOpts[DEFAULT]

javaProfOpts[OBJ_INFO_FULL]     = [ optionOn(ProfileObjectInfo),
                                    optionOn(PrintObjectInfoAtInterval),
                                    optionOn(TimeObjectInfoPrinting),
                                    optionOn(TotalRefCounts),
                                    optionOff(UseCompressedOops),
                                    interpreterMode()
                                  ]

javaProfOpts[OBJ_INFO_FULL_TEN] = [ optionOn(ProfileObjectInfo),
                                    optionOn(PrintObjectInfoAtInterval),
                                    optionOn(TimeObjectInfoPrinting),
                                    optionOn(TotalRefCounts),
                                    optionOff(UseCompressedOops),
                                    optionOn(OnlyTenuredObjectInfo),
                                    interpreterMode()
                                 ]

javaProfOpts[OBJ_INFO_ORGANIZE] = [ optionOn(ProfileObjectInfo),
                                    optionOn(TrimObjectInfo),
                                    optionOn(TimeObjectInfoPrinting),
                                    optionOn(TotalRefCounts),
                                    optionOff(UseCompressedOops),
                                    interpreterMode()
                                  ]

javaProfOpts[OBJ_INFO_DEFAULT]  = [ optionOn(ProfileObjectInfo),
                                    #optionSet(NewSize, "%d" % (MB(200))),
                                    #optionSet(MaxNewSize, "%d" % (MB(200))),
                                    #optionSet(NewSize, "%d" % (GB(2))),
                                    #optionSet(MaxNewSize, "%d" % (GB(2))),
                                    optionSet(ParallelGCThreads, "1"),
                                    optionOn(PrintHeapAtGC),
                                    optionOn(PrintGCDetails),
                                    optionOn(TrimObjectInfo),
                                    optionOn(TimeObjectInfoPrinting),
                                    optionOn(TotalRefCounts),
                                    optionOn(PrintTextAPInfo),
                                    optionOff(UseCompressedOops),
                                    #optionOff("UseBiasedLocking"),
                                    optionOff("RewriteBytecodes"),
                                    optionOff("RewriteFrequentPairs"),
                                    optionOff("UseFastAccessorMethods"),
                                    optionOn("AlwaysCountInvocations"),
                                    optionOn(CountCompiledCalls), # for the method histogram
                                    optionSet(MethodHistogramCutoff,"0"),
                                    #optionOff(UseAdaptiveSizePolicy),
                                    interpreterMode()
                                  ]

javaProfOpts[OBJ_INFO_INTERVAL] = javaProfOpts[OBJ_INFO_DEFAULT] + [
                                    #optionOn(PrintReferencedObjects),
                                    optionOn(PrintObjectInfoAtInterval),
                                    optionOn(PrintObjectInfoBeforeFullGC),
                                    optionOn(PrintObjectInfoAfterFullGC),
                                    optionOn(PrintAPInfoAtInterval),
                                    #optionSet(StackHeapSampleRatio, "5"),
                                    #optionOn(PrintStackSamples),
                                    optionOn(PrintThreadTimes),
                                    optionOn("DisableMajorGC"),
                                    optionOn(OnlyIntervalGC),
                                  ] + alloc_opts["A"]

javaProfOpts[OBJ_INFO_GUIDED]   = javaProfOpts[OBJ_INFO_DEFAULT] + [
                                    optionOn(UseColoredSpaces),
                                    optionOn(ColorObjectAllocations),
                                    optionOff(MColorColoredSpacePages),
                                    optionSet(ParallelGCThreads, "1"),
                                    optionSet(InitialTenuringThreshold, "10000"),
                                    optionSet(MaxTenuringThreshold, "10000"),
                                    optionOn(CompileSlowAllocations),
                                    optionOn(SlowAllocations),
                                    #optionOff("UseTLAB"),
                                    optionOn("DisableMajorGC"),
                                    optionOff(UseAdaptiveSizePolicy),
                                  ] + alloc_opts["C"]

javaProfOpts[OBJ_INFO_AGE_THRESH] = javaProfOpts[OBJ_INFO_ORGANIZE] + [
                                      optionSet(ProfileAgeThreshold,"0"),
                                    ]

javaProfOpts[OBJ_INFO_AT0_RT0]    = javaProfOpts[OBJ_INFO_AGE_THRESH] + [
                                      optionSet(ColorAgeThreshold,  "0"),
                                      optionSet(ColorRefThreshold,  "0"),
                                    ]

javaProfOpts[OBJ_INFO_XXX_RT0]    = javaProfOpts[OBJ_INFO_AGE_THRESH] + [
                                      optionSet(ColorAgeThreshold,  "-1"),
                                      optionSet(ColorRefThreshold,  "0")
                                    ]

javaProfOpts[OBJ_INFO_TENURED]  = [ optionOn(ProfileObjectInfo),
                                    optionOn(TrimObjectInfo),
                                    optionOn(TimeObjectInfoPrinting),
                                    #optionOn(PrintObjectInfoBeforeFullGC),
                                    #optionOn(PrintObjectInfoAfterFullGC),
                                    optionOn(TotalRefCounts),
                                    optionOn(OnlyTenuredObjectInfo),
                                    optionOff(UseCompressedOops),
                                    interpreterMode()
                                  ]

#javaProfOpts[OBJ_VALS_DEFAULT]  = [ optionOn(ProfileObjectInfo),
#                                    optionOn(TrimObjectInfo),
#                                    optionOn(TimeObjectInfoPrinting),
#                                    #optionOn(PrintObjectInfoBeforeFullGC),
#                                    #optionOn(PrintObjectInfoAfterFullGC),
#                                    optionOn(TotalRefCounts),
#                                    optionOff(UseCompressedOops),
#                                    interpreterMode()
#                                  ]
#
#javaProfOpts[OBJ_VALS_TENURED]  = [ optionOn(ProfileObjectInfo),
#                                    optionOn(TrimObjectInfo),
#                                    optionOn(TimeObjectInfoPrinting),
#                                    #optionOn(PrintObjectInfoBeforeFullGC),
#                                    #optionOn(PrintObjectInfoAfterFullGC),
#                                    optionOn(TotalRefCounts),
#                                    optionOn(OnlyTenuredObjectInfo),
#                                    optionOff(UseCompressedOops),
#                                    interpreterMode()
#                                  ]

javaProfOpts[OBJ_GCVALS_DEFAULT]  = [ optionOn(TrimObjectInfo),
                                      optionOn(TimeObjectInfoPrinting),
                                      #optionOn(PrintObjectInfoBeforeFullGC),
                                      optionOn(PrintObjectInfoAfterFullGC),
                                      optionOn(TotalRefCounts),
                                      optionOff(UseCompressedOops),
                                      interpreterMode()
                                    ]

javaProfOpts[OBJ_GCVALS_TENURED]  = [ optionOn(TrimObjectInfo),
                                      optionOn(TimeObjectInfoPrinting),
                                      #optionOn(PrintObjectInfoBeforeFullGC),
                                      optionOn(PrintObjectInfoAfterFullGC),
                                      optionOn(TotalRefCounts),
                                      optionOn(OnlyTenuredObjectInfo),
                                      optionOff(UseCompressedOops),
                                      interpreterMode()
                                  ]

javaProfOpts[ADDR_TLAB_DEFAULT]   = [ optionOn(ProfileObjectAddressInfo),
                                      optionOn(PrintHeapAtGC),
                                      optionOn(PrintGCDetails),
                                      #optionOn("PrintTextOAT"),
                                      interpreterMode()
                                    ]

javaProfOpts[ADDR_TLAB_INTERVAL]  = javaProfOpts[ADDR_TLAB_DEFAULT] + [
                                      optionOn(PrintObjectAddressInfoAtGC),
                                      optionOn(PrintObjectAddressInfoAtInterval),
                                    ]

javaProfOpts[ADDR_TLAB_XVAL]      = javaProfOpts[ADDR_TLAB_INTERVAL]

# the GC options and the special alloc_opts are necessary for eclipse and
# tradebeans/tradesoap (otherwise GC comes in and clears out some of the
# klasses before we can print them out
#
javaProfOpts[OBJ_INFO_XSIM]       = javaProfOpts[ADDR_TLAB_INTERVAL] + [
                                      optionOn(PrintAPInfoAtInterval),
                                      #optionOn(PrintTextAPInfo),
                                      optionOn(PrintKRInfoAtInterval),
                                      #optionOn(PrintTextKRInfo),
                                      optionSet(StackHeapSampleRatio, "25"),
                                      optionOn(PrintStackSamples),
                                      optionOn(PrintThreadTimes),
                                    ]
                                      #optionOn("DisableMajorGC"),
                                      #optionOn(OnlyIntervalGC),
                                    #] + alloc_opts["A"]
javaProfOpts[OBJ_INFO_SHSIM]      = javaProfOpts[ADDR_TLAB_INTERVAL] + [
                                      optionOn(PrintAPInfoAtInterval),
                                      #optionOn(PrintTextAPInfo),
                                      optionOn(PrintKRInfoAtInterval),
                                      #optionOn(PrintTextKRInfo),
                                      optionSet(StackHeapSampleRatio, "5"),
                                      optionOn(PrintStackSamples),
                                      optionOn(PrintThreadTimes),
                                    ]

for vr in vrates:
  xr = vr / 10
  javaProfOpts[(OBJ_INFO_VXSIM%vr)] = javaProfOpts[ADDR_TLAB_INTERVAL] + [
                                        optionOn(PrintAPInfoAtInterval),
                                        #optionOn(PrintTextAPInfo),
                                        optionOn(PrintKRInfoAtInterval),
                                        #optionOn(PrintTextKRInfo),
                                        optionSet(StackHeapSampleRatio, str((vr/10))),
                                        optionOn(PrintStackSamples),
                                        optionOn(PrintThreadTimes),
                                      ]

  javaProfOpts[(OBJ_INFO_NVXSIM%vr)] = javaProfOpts[ADDR_TLAB_INTERVAL] + [
                                        optionOn(PrintAPInfoAtInterval),
                                        #optionOn(PrintTextAPInfo),
                                        optionOn(PrintKRInfoAtInterval),
                                        #optionOn(PrintTextKRInfo),
                                        optionSet(StackHeapSampleRatio, str((vr/10))),
                                        optionOn(PrintStackSamples),
                                        optionOn(PrintThreadTimes),
                                      ]

javaProfOpts[OBJ_INFO_XSHSIM]     = javaProfOpts[ADDR_TLAB_INTERVAL] + [
                                      optionOn(PrintAPInfoAtInterval),
                                      #optionOn(PrintTextAPInfo),
                                      optionOn(PrintKRInfoAtInterval),
                                      #optionOn(PrintTextKRInfo),
                                      optionSet(StackHeapSampleRatio, "5"),
                                      optionOn(PrintStackSamples),
                                      optionOn(PrintThreadTimes),
                                      optionOn("CallStackSamplerThread"),
                                      optionSet("SamplerThreadDelay", "2000000"),
                                    ]


javaProfOpts[ADDR_TLAB_PREGC]     = javaProfOpts[ADDR_TLAB_DEFAULT] + [
                                      optionOn(PrintObjectAddressInfoAtGC),
                                    ]

javaProfOpts[ADDR_INFO_DEFAULT]   = javaProfOpts[ADDR_TLAB_DEFAULT] + [
                                      optionOff(UseTLAB),
                                    ]

javaProfOpts[ADDR_INFO_INTERVAL]  = javaProfOpts[ADDR_INFO_DEFAULT] + [
                                      optionOn(PrintObjectAddressInfoAtGC),
                                      optionOn(PrintObjectAddressInfoAtInterval),
                                    ]

javaProfOpts[FIELD_TLAB_INTERVAL] = javaProfOpts[ADDR_TLAB_INTERVAL] + [
                                      optionOn(ProfileObjectFieldInfo),
                                      optionOn(PrintTextKRInfo),
                                      optionOn(PrintKRInfoAtInterval),
                                      optionOn(PrintAPInfoAtInterval),
                                      #optionOff("UseCompilerSafepoints"),
                                      optionOff("RewriteBytecodes"),
                                      optionOff("RewriteFrequentPairs"),
                                      optionOff("UseFastAccessorMethods"),
                                    ]

javaProfOpts[AP_TLAB_INTERVAL]     = javaProfOpts[ADDR_TLAB_INTERVAL] + [
                                      optionOff(ProfileObjectFieldInfo),
                                      optionOn(PrintTextKRInfo),
                                      optionOn(PrintKRInfoAtInterval),
                                      optionOn(PrintAPInfoAtInterval),
                                      optionOn(PrintTextAPInfo),
                                      #optionOff("UseCompilerSafepoints"),
                                      optionOff("RewriteBytecodes"),
                                      optionOff("RewriteFrequentPairs"),
                                      optionOff("UseFastAccessorMethods"),
                                      #optionOn("SuppressPrintADDRInfo"),
                                      optionOff("UseCompressedOops"),
                                    ]

javaProfOpts[MINI_AP_TLAB_INTERVAL] = javaProfOpts[ADDR_TLAB_INTERVAL] + [
                                      optionOff(ProfileObjectFieldInfo),
                                      optionOff(PrintTextKRInfo),
                                      optionOff("PrintBinKRInfo"),
                                      optionOff("PrintBinADDRInfo"),
                                      optionOff("PrintBinAPInfo"),
                                      optionOn(PrintKRInfoAtInterval),
                                      optionOn(PrintAPInfoAtInterval),
                                      optionOn(PrintTextAPInfo),
                                      #optionOff("UseCompilerSafepoints"),
                                      optionOff("RewriteBytecodes"),
                                      optionOff("RewriteFrequentPairs"),
                                      optionOff("UseFastAccessorMethods"),
                                      optionOn("SuppressPrintADDRInfo"), 
                                    ]

javaProfOpts[FIELD_INFO_INTERVAL] = javaProfOpts[ADDR_INFO_INTERVAL] + [
                                      optionOn(ProfileObjectFieldInfo),
                                      #optionOff("UseCompilerSafepoints"),
                                      optionOff("RewriteBytecodes"),
                                      optionOff("RewriteFrequentPairs"),
                                      optionOff("UseFastAccessorMethods"),
                                    ]

javaProfOpts[FIELD_TLAB_PREGC]    = javaProfOpts[ADDR_TLAB_PREGC] + [
                                      optionOn(ProfileObjectFieldInfo),
                                    ]

#javaProfOpts[ADDR_HEAVY_INTERVAL] = javaProfOpts[ADDR_INFO_INTERVAL] + [
#                                      #optionOn("PrintTextOAT"),
#                                    ]

#############################################################################
# harness configurations
#############################################################################
IgnoreKitValidation  = "-ikv"

jvm2008RunOpts = {}

jvm2008ProfOpts = {}
jvm2008ProfOpts[DEFAULT]    = []
jvm2008ProfOpts[FULL_HPROF] = [ specjvmOptTrue(specjvmRunEmon),
                                specjvmOptTrue(specjvmRunIostat),
                                specjvmOptTrue(specjvmRunSar),
                                specjvmOptTrue(specjvmRunPowerGov),
                              ]

jvm2008ProfOpts[EMON_HPROF]      = [ specjvmOptTrue(specjvmRunEmon) ]
jvm2008ProfOpts[POWER_GOV_HPROF] = [ specjvmOptTrue(specjvmRunPowerGov) ]

dacapoRunOpts  = {}
dacapoRunOpts[DEFAULT]            = [ iteropt(defdaciters)   ]
dacapoRunOpts[METHOD_DATA_RECORD] = [ iteropt(defdaciters)   ]
dacapoRunOpts[COLORED_SPACES]     = [ iteropt(defdaciters)   ]
dacapoRunOpts[TIERED_STEADY_BASE] = [ iteropt(steadyiters+1) ]
dacapoRunOpts[SERVER_STEADY_BASE] = [ iteropt(steadyiters+1) ]
dacapoRunOpts[TIERED_XSS_BASE]    = [ iteropt(xssiters+1)    ]
dacapoRunOpts[SERVER_XSS_BASE]    = [ iteropt(xssiters+1)    ]
dacapoRunOpts[STEADY_CCF_REPLAY]  = [ iteropt(steadyiters)   ]
dacapoRunOpts[STEADY_REPLAY]      = [ iteropt(steadyiters)   ]
dacapoRunOpts[STEADY_REPLAY_LC]   = [ iteropt(steadyiters)   ]
dacapoRunOpts[REPLAY_NO_RESET]    = [ iteropt(steadyiters)   ]
dacapoRunOpts[STEADY_CCF]         = [ iteropt(steadyiters)   ]
dacapoRunOpts[OBJ_INFO_XSIM]      = [ iteropt(1)             ]
dacapoRunOpts[OBJ_INFO_SHSIM]     = [ iteropt(1)             ]
dacapoRunOpts[OBJ_INFO_XSHSIM]    = [ iteropt(1)             ]
dacapoRunOpts[CCF_ORACLE_DIFF]    = [ iteropt(steadyiters)   ]
dacapoRunOpts[XINT_CCF]           = [ iteropt(2)             ]
dacapoRunOpts[OBJ_INFO_INTERVAL]  = [ iteropt(1)             ]
dacapoRunOpts[SHORT_RUN]          = [ iteropt(3)             ]
dacapoRunOpts[ONE_ITER]           = [ iteropt(1)             ]

for md_cfg in steady_md_ct_records:
  dacapoRunOpts[md_cfg] = [ iteropt(steadyiters)   ]
for md_cfg in steady_md_ct_replays:
  dacapoRunOpts[md_cfg] = [ iteropt(steadyiters+1) ]
for md_cfg in xss_md_ct_records:
  dacapoRunOpts[md_cfg] = [ iteropt(xssiters)      ]
for md_cfg in xss_md_ct_replays:
  dacapoRunOpts[md_cfg] = [ iteropt(xssiters+1)    ]

for md_cfg in md_records:
  n = int(md_cfg.split('_')[1].strip('n'))
  dacapoRunOpts[md_cfg] = [ iteropt(n) ]
for md_cfg in md_replays:
  n = max(nscale)
  dacapoRunOpts[md_cfg] = [ iteropt(n+2) ]

for vr in vrates:
  dacapoRunOpts[(OBJ_INFO_VXSIM%vr)] = [ iteropt(1) ]
  dacapoRunOpts[(OBJ_INFO_NVXSIM%vr)] = [ iteropt(1) ]

dacapoProfOpts = {}

#############################################################################
# numactl configurations
#############################################################################
numaopts = {}
numaopts[DEFAULT]    = []
numaopts[NODE0_BIND] = [ opt("cpunodebind","0"), opt("preferred","0") ]
numaopts[NODE1_BIND] = [ opt("cpunodebind","1"), opt("preferred","1") ]

#############################################################################
# mcolorctl configurations
#############################################################################
mcoloropts = {}
mcoloropts[MCOLOR_BIND_N1C0] = [ opt("bind","8,9") ]
mcoloropts[MCOLOR_BIND_N1C1] = [ opt("bind","10,11") ]
mcoloropts[MCOLOR_BIND_N1C2] = [ opt("bind","12,13") ]
mcoloropts[MCOLOR_BIND_N1C3] = [ opt("bind","14,15") ]

mcoloropts[MCOLOR_BIND_SKIP_N1C0] = [ opt("bind","10-15") ]
mcoloropts[MCOLOR_BIND_SKIP_N1C1] = [ opt("bind","8-9,12-15") ]
mcoloropts[MCOLOR_BIND_SKIP_N1C2] = [ opt("bind","8-11,14-15") ]
mcoloropts[MCOLOR_BIND_SKIP_N1C3] = [ opt("bind","8-13") ]

mcoloropts[MCOLOR_ILV_N1] = [ opt("interleave","8-15") ]

mcoloropts[ECD_MCOLOR_BIND_N1C0] = [ opt("bind","10,11") ]
mcoloropts[ECD_MCOLOR_BIND_N1C1] = [ opt("bind","8,9")   ]
mcoloropts[ECD_MCOLOR_BIND_N1C2] = [ opt("bind","6,7")   ]

#############################################################################
# mscramble configurations
# (numacfg, mcolorcfg, sleeptime, args)
#############################################################################
mscramopts = {}
mscramopts[SCRAMBLE_HOLD_11_OVER_11] =  (NODE1_BIND, MCOLOR_BIND_SKIP_N1C0, 60, \
                                         ["11.8", "11.8"])

mscramopts[SCRAMBLE_HOLD_11_OVER_1]  =  (NODE1_BIND, MCOLOR_BIND_SKIP_N1C3, 60, ["11.8", "4.8"])
mscramopts[SCRAMBLE_HOLD_15_OVER_1]  =  (NODE1_BIND, None, 60, ["15.8", "4.8"])
mscramopts[SCRAMBLE_HOLD_11_OVER_4]  =  (NODE1_BIND, MCOLOR_BIND_SKIP_N1C3, 60, ["11.8", "4.8"])
mscramopts[SCRAMBLE_HOLD_11_OVER_8]  =  (NODE1_BIND, MCOLOR_BIND_SKIP_N1C3, 60, ["11.8", "8.8"])
mscramopts[SCRAMBLE_HOLD_15_OVER_8]  =  (NODE1_BIND, None, 60, ["15.8", "8.8"])

mscramopts[SCRAMBLE_HOLD_15_OVER_11] =  (NODE1_BIND, None, 60, ["15.8", "11.8"])
mscramopts[SCRAMBLE_HOLD_15_OVER_9]  =  (NODE1_BIND, None, 60, ["15.8", "9.8"])

mscramopts[SCRAMBLE_HOLD_30_OVER_2]  =  (NODE1_BIND, None, 120, ["30.0", "2.0"])

#############################################################################
# trayctl configurations
#############################################################################
trayopts = {}
trayopts[RESTRICT_N1C0] = ( ["--restrict=8,9"],   ["--allow=8,9"]   )
trayopts[RESTRICT_N1C1] = ( ["--restrict=10,11"], ["--allow=10,11"] )
trayopts[RESTRICT_N1C2] = ( ["--restrict=12,13"], ["--allow=12,13"] )
trayopts[RESTRICT_N1C3] = ( ["--restrict=14,15"], ["--allow=14,15"] )

#############################################################################
# regular expressions
#############################################################################
jvm2008TimeRE     = re.compile("Score on (\S)*:")
dacapoTimeRE      = re.compile("===== DaCapo 9.12 (\S)+ completed")
bwbIterRE         = re.compile("BWB complete!")
dacapoPassedRE    = re.compile("===== DaCapo 9.12 (\S)+ PASSED")
jgfTimeRE         = re.compile("(\S)*:Total")
fatalErrorRE      = re.compile("# A fatal error")
unixTimeRE        = re.compile("elapsed: [-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?(\W)+rss:")
emonStartRE       = re.compile("emon started:")
emonTimeRE        = re.compile("Loop number (\d+), monitoring time =")
emonCounterRE     = re.compile("Counter (\d+) (\S)*")
emonPkgRE         = re.compile("pkg (\d+):")
emonSeparatorRE   = re.compile("=====")
iterOptRE         = re.compile("-n (\d+)")
gcpowRE           = re.compile("elapsed (\S)+ GC time:")
headerEndRE       = re.compile("----------------------------------------")
klassRE           = re.compile("(\W)*(\d+):")
objRE             = re.compile("(\W)*(\d+)(\W)+(\d+)")
vmStartRE         = re.compile("vm start:")
pcmStartRE        = re.compile("pcm start_millis:")
dacapoIterStartRE = re.compile("iter (\d+) start:")
dacapoIterEndRE   = re.compile("iter (\d+) end:")
dacapoStartRE     = re.compile("iter 3 start:")
dacapoEndRE       = re.compile("iter 7 end:")
heapBeforeRE      = re.compile("\{Heap before")
heapAfterRE       = re.compile("Heap after")
heapEndOfRunRE    = re.compile("Heap \[(\d+)")
edenSpaceRE       = re.compile("(\s)*eden space")
fromSpaceRE       = re.compile("(\s)*from space")
toSpaceRE         = re.compile("(\s)*to   space")
objectSpaceRE     = re.compile("(\s)*object space")
colorRedSpaceRE   = re.compile("(\s)*color red space")
colorBlueSpaceRE  = re.compile("(\s)*color blue space")
minorGCRE         = re.compile("\[GC")
majorGCRE         = re.compile("\[Full GC")
GCTimeRE          = re.compile("\[Times:")
mbStartRE         = re.compile("Starting (\w)*Bench:")
mbAllocStartRE    = re.compile("allocation start:")
mbAllocEndRE      = re.compile("allocation complete:")
mbThreadStartRE   = re.compile("starting threads:")
mbEndRE           = re.compile("done:")
jvmStartRE        = re.compile("Iteration 1 (\S)* begins:")
jvmEndRE          = re.compile("Iteration 1 (\S)* ends:")
jgfStartRE        = re.compile("iterate begin:")
jgfEndRE          = re.compile("iterate end:")
overflowRE        = re.compile("warning: negative")
preIterRE         = re.compile("pre VM Indicator")
postIterRE        = re.compile("post VM Indicator")
threadTimeRE      = re.compile("Thread:(\W)*(\d+)")
#gcpowRE           = re.compile("elapsed \(minor\) GC time:")


#############################################################################
# benchmark metrics
#############################################################################
HARNESS_TIME = "Harness Time"

GC_POWER     = "GC Power"
GC_JOULES    = "GC Joules"
GC_TIME      = "GC Time"
GC_POWER_AVG = "GC Power Avg."

gcmets = [GC_POWER, GC_JOULES, GC_TIME, GC_POWER_AVG]

UNIX_TIME = "UNIX Time"
RSS       = "RSS"

timeMetrics = [UNIX_TIME, RSS]

MINOR_GC   = "minor_gc"
MAJOR_GC   = "major_gc"
GC_RUNTIME = "GC Runtime"
NUM_GC     = "# GC's"

MB_START         = "bench start"
MB_ALLOC_START   = "alloc start"
MB_ALLOC_END     = "alloc end"
MB_THREAD_START  = "thread start"
MB_END           = "bench end"

MB_ALLOC_TIME       = "alloc time"
MB_THREAD_TIME      = "thread time"
MB_THREAD_POWER     = "thread power"
MB_THREAD_ENERGY    = "thread energy"
MB_THREAD_READ_BW   = "thread read_bw"
MB_THREAD_WRITE_BW  = "thread write_bw"
MB_THREAD_MEMORY_BW = "thread mem_bw"
MEM_POWER           = "mem power"
MEM_ENERGY          = "mem energy"
MODEL_MEM_POWER     = "model_mem_power"
MODEL_MEM_ENERGY    = "model_mem_energy"
MODEL_TOTAL_ENERGY  = "model_total_energy"
MODEL_TOTAL_POWER   = "model_total_power"
MEM_READ_BW         = "mem read_bw"
MEM_WRITE_BW        = "mem write_bw"
MEM_TOTAL_BW        = "mem total_bw"
CPU_POWER           = "cpu power"
CPU_ENERGY          = "cpu energy"
CHAN_BANDWIDTH      = "chan_bandwidth"

MODEL_MEM_BG_POWER   = "model_mem_bg_power"
MODEL_MEM_BG_ENERGY  = "model_mem_bg_energy"
MODEL_MEM_OP_POWER   = "model_mem_op_power"
MODEL_MEM_OP_ENERGY  = "model_mem_op_energy"
MODEL_CHAN_POWER     = "model_chan_power"
MODEL_CHAN_ENERGY    = "model_chan_energy"
MODEL_CHAN_BG_POWER  = "model_chan_bg_power"
MODEL_CHAN_BG_ENERGY = "model_chan_bg_energy"
MODEL_CHAN_OP_POWER  = "model_chan_op_power"
MODEL_CHAN_OP_ENERGY = "model_chan_op_energy"

VM_START    = "vm_start"
BENCH_START = "bench_start"
BENCH_END   = "bench_end"
SCORE       = "score"

#############################################################################
# raw emon events (also metrics)
#############################################################################
UNC_M_ACT_COUNT                   = "UNC_M_ACT_COUNT"
UNC_M_CAS_COUNT_dot_ALL           = "UNC_M_CAS_COUNT.ALL"
UNC_M_POWER_SELF_REFRESH          = "UNC_M_POWER_SELF_REFRESH"
UNC_M_POWER_CKE_CYCLES_dot_RANK0  = "UNC_M_POWER_CKE_CYCLES.RANK0"
UNC_M_POWER_CKE_CYCLES_dot_RANK1  = "UNC_M_POWER_CKE_CYCLES.RANK1"

UNC_DRAM_READ_CAS_dot_CH0         = "UNC_DRAM_READ_CAS.CH0"
UNC_DRAM_READ_CAS_dot_CH1         = "UNC_DRAM_READ_CAS.CH1"
UNC_DRAM_READ_CAS_dot_CH2         = "UNC_DRAM_READ_CAS.CH2"
UNC_DRAM_WRITE_CAS_dot_CH0        = "UNC_DRAM_WRITE_CAS.CH0"
UNC_DRAM_WRITE_CAS_dot_CH1        = "UNC_DRAM_WRITE_CAS.CH1"
UNC_DRAM_WRITE_CAS_dot_CH2        = "UNC_DRAM_WRITE_CAS.CH2"

mem_events = [
  UNC_M_ACT_COUNT,
  UNC_M_CAS_COUNT_dot_ALL,
  UNC_M_POWER_SELF_REFRESH,
#  UNC_M_POWER_CKE_CYCLES_dot_RANK0,
#  UNC_M_POWER_CKE_CYCLES_dot_RANK1,
]

ecuador_mem_events = [
  UNC_DRAM_READ_CAS_dot_CH0,
  UNC_DRAM_READ_CAS_dot_CH1,
  UNC_DRAM_READ_CAS_dot_CH2,
  UNC_DRAM_WRITE_CAS_dot_CH0,
  UNC_DRAM_WRITE_CAS_dot_CH1,
  UNC_DRAM_WRITE_CAS_dot_CH2,
]

CAS_RD = "cas_rd"
CAS_WR = "cas_wr"
CAS_RW = "cas_rw"

cas_rd_evts = [ UNC_DRAM_READ_CAS_dot_CH0,
                UNC_DRAM_READ_CAS_dot_CH1,
                UNC_DRAM_READ_CAS_dot_CH2,
              ]

cas_wr_evts = [ UNC_DRAM_WRITE_CAS_dot_CH0,
                UNC_DRAM_WRITE_CAS_dot_CH1,
                UNC_DRAM_WRITE_CAS_dot_CH2,
              ]

cas_rw_evts = cas_rd_evts + cas_wr_evts

#############################################################################
# edproc metrics
#############################################################################
CPU_OPERATING_FREQUENCY = "CPU operating frequency (in GHz)"
CPU_UTILIZATION = "CPU utilization %"
CPI = "CPI"
LOADS_PER_INSTRUCTION = "loads per instruction"
STORES_PER_INSTRUCTION = "stores per instruction"
L1D_MPI = "L1D MPI (includes data+rfo w/ prefetches)"
L1D_PREFETCH_MPI = "L1D prefetch MPI"
L2_CODE_MPI = "L2 code MPI (demand misses)"
L2_DATA_READ_MPI = "L2 data read MPI"
L2_DATA_WRITE_MPI = "L2 data write MPI"
L2_MPI = "L2 MPI (code/data/rfo includes prefetches)"
L2_MPI_OLD = "L2 MPI (old version; broken)"
L2_PREFETCHES_PER_INSTRUCTION = "L2 prefetches per instruction"
L2_PREFETCH_MPI = "L2 prefetch MPI"
L2_ANY_LOCAL_REQUEST_THAT_HITM_IN_SIBLING = "L2 Any local request that HITM in a sibling core (per inst)"
LLC_CODE_MPI = "LLC code MPI (demand+prefetch)"
LLC_DATA_READ_MPI = "LLC data read MPI (demand+prefetch)"
LLC_DATA_WRITE_MPI = "LLC data write MPI (demand+prefetch)"
LLC_MPI = "LLC MPI (code+data+rfo; includes prefetches)"
LLC_WRITEBACKS_PER_INSTRUCTION = "LLC writebacks per instruction"
LLC_DATA_CODE_READS_THAT_HIT_E_F_LINES = "LLC Data/Code reads that hit E/F lines in remote socket (per instr)"
LLC_DATA_READS_THAT_HIT_MODIFIED_LINES = "LLC Data reads that hit Modified lines in remote socket (per instr)"
LLC_DATA_WRITES_THAT_HIT_MODIFIED_LINES = "LLC Data writes that hit Modified lines in remote socket (per instr)"
LLC_TOTAL_HITM = "LLC total HITM (per instr)"
DTLB_MPI = "DTLB MPI"
ITLB_MPI = "ITLB MPI"
PCT_NUMA_READS_SATISFIED_BY_LOCAL_DRAM = "% NUMA Reads satisfied by local DRAM"
PCT_NUMA_READS_SATISFIED_BY_REMOTE_DRAM = "% NUMA Reads satisfied by remote DRAM"
PCT_NUMA_READS_SATISFIED_BY_REMOTE_CACHE = "% NUMA Reads satisfied by remote cache"
NUMA_LOCALITY_PCT = "NUMA Locality %"
PCT_QPI_UTILIZATION_LINK_0_TRANSMIT = "QPI utilization link 0 transmit %"
PCT_QPI_UTILIZATION_LINK_1_TRANSMIT = "QPI utilization link 1 transmit %"
QPI_B_W_LINK_0_TRANSMIT = "QPI B/W link 0 transmit (MB/sec)"
QPI_B_W_LINK_1_TRANSMIT = "QPI B/W link 1 transmit (MB/sec)"
MEMORY_BANDWIDTH_READ = "memory bandwidth read (MB/sec)"
MEMORY_BANDWIDTH_WRITE = "memory bandwidth write (MB/sec)"
MEMORY_BANDWIDTH_TOTAL = "memory bandwidth total (MB/sec)"
MEMORY_LATENCY = "memory latency (in uncore clocks)"
IO_BANDWIDTH_WRITES = "IO bandwidth disk or network writes (MB/sec)"
IO_BANDWIDTH_READS = "IO bandwidth disk or network reads (MB/sec)"

edprocMetricREs = {
  CPU_OPERATING_FREQUENCY : "metric_CPU operating frequency (in GHz)",
  CPU_UTILIZATION : "metric_CPU_utilization_\%",
  CPI : "metric_CPI",
  LOADS_PER_INSTRUCTION : "metric_loads_per_instruction",
  STORES_PER_INSTRUCTION : "metric_stores_per_instruction",
  L1D_MPI : "metric_L1D_MPI (includes data+rfo w/ prefetches)",
  L1D_PREFETCH_MPI : "metric_L1D_prefetch_MPI",
  L2_CODE_MPI : "metric_L2_code_MPI (demand misses)",
  L2_DATA_READ_MPI : "metric_L2_data_read_MPI",
  L2_DATA_WRITE_MPI : "metric_L2_data_write_MPI",
  L2_MPI : "metric_L2_MPI (code/data/rfo includes prefetches)",
  L2_MPI_OLD : "metric_L2_MPI (old version; broken)",
  L2_PREFETCHES_PER_INSTRUCTION : "metric_L2_prefetches_per_instruction",
  L2_PREFETCH_MPI : "metric_L2_prefetch_MPI",
  L2_ANY_LOCAL_REQUEST_THAT_HITM_IN_SIBLING : "metric_L2_Any local request that HITM in a sibling core (per inst)",
  LLC_CODE_MPI : "metric_LLC_code_MPI (demand+prefetch)",
  LLC_DATA_READ_MPI : "metric_LLC_data_read_MPI (demand+prefetch)",
  LLC_DATA_WRITE_MPI : "metric_LLC_data_write_MPI (demand+prefetch)",
  LLC_MPI : "metric_LLC_MPI (code+data+rfo; includes prefetches)",
  LLC_WRITEBACKS_PER_INSTRUCTION : "metric_LLC_writebacks_per_instruction",
  LLC_DATA_CODE_READS_THAT_HIT_E_F_LINES : "metric_LLC_Data/Code reads that hit E/F lines in remote socket (per instr)",
  LLC_DATA_READS_THAT_HIT_MODIFIED_LINES : "metric_LLC_Data reads that hit Modified lines in remote socket (per instr)",
  LLC_DATA_WRITES_THAT_HIT_MODIFIED_LINES : "metric_LLC_Data writes that hit Modified lines in remote socket (per instr)",
  LLC_TOTAL_HITM : "metric_LLC_total_HITM (per instr)",
  DTLB_MPI : "metric_DTLB_MPI",
  ITLB_MPI : "metric_ITLB_MPI",
  PCT_NUMA_READS_SATISFIED_BY_LOCAL_DRAM : "metric_NUMA \%_Reads satisfied by local DRAM \(LLC prefetches excluded\)",
  PCT_NUMA_READS_SATISFIED_BY_REMOTE_DRAM : "metric_NUMA \%_Reads satisfied by remote DRAM \(LLC prefetches excluded\)",
  PCT_NUMA_READS_SATISFIED_BY_REMOTE_CACHE : "metric_NUMA \%_Reads satisfied by remote cache \(Hitm+HitF; LLC prefetches excluded\)",
  NUMA_LOCALITY_PCT : "metric_NUMA_NUMA-locality\%",
  PCT_QPI_UTILIZATION_LINK_0_TRANSMIT : "metric_QPI_utilization_\%_link_0_transmit",
  PCT_QPI_UTILIZATION_LINK_1_TRANSMIT : "metric_QPI_utilization_\%_link_1_transmit",
  QPI_B_W_LINK_0_TRANSMIT : "metric_QPI_B/W_link_0_transmit (MB/sec)",
  QPI_B_W_LINK_1_TRANSMIT : "metric_QPI_B/W_link_1_transmit (MB/sec)",
  MEMORY_BANDWIDTH_READ : "metric_memory_bandwidth_read (MB/sec)",
  MEMORY_BANDWIDTH_WRITE : "metric_memory_bandwidth_write (MB/sec)",
  MEMORY_BANDWIDTH_TOTAL : "metric_memory_bandwidth_total (MB/sec)",
  MEMORY_LATENCY : "metric_memory_latency (in uncore clocks)",
  IO_BANDWIDTH_WRITES : "metric_IO_bandwidth_disk_or_network_writes (MB/sec)",
  IO_BANDWIDTH_READS : "metric_IO_bandwidth_disk_or_network_reads (MB/sec)"
}

edprocMetrics = edprocMetricREs.keys()

#############################################################################
# stats - ways of describing metrics
#############################################################################
MEAN    = "mean"
STDEV   = "stdev"
GEOMEAN = "geomean"
MIN     = "min"
MAX     = "max"

#############################################################################
# power gov metrics
#############################################################################
PGOV_PKG    = "PGOV_PKG"
PGOV_CORE   = "PGOV_Core"
PGOV_UNCORE = "PGOV_Uncore"
PGOV_DRAM   = "PGOV_DRAM"

pgovMetrics = [
  PGOV_PKG,
  PGOV_CORE,
  PGOV_UNCORE,
  PGOV_DRAM,
]

#############################################################################
# emon and power_gov values
#############################################################################
deferate   = 0.1
defeloops  = 1000000
defpowrate = 1
defts      = 100

#############################################################################
# edd keys
#############################################################################
TIMEVAL = "timeval"
METRECS = "metrecs"
COUNTER = "counter"
DATA    = "data"

##############################################################################
# student t distribution
##############################################################################
student_t_95 = {
  1  : 12.71,
  2  : 4.303,
  3  : 3.182,
  4  : 2.776,
  5  : 2.571,
  6  : 2.447,
  7  : 2.998,
  8  : 2.306,
  9  : 2.262,
  10 : 2.228,
  11 : 2.201,
  12 : 2.179,
  13 : 2.160,
  14 : 2.145,
  15 : 2.131,
  16 : 2.120,
  17 : 2.110,
  18 : 2.101,
  19 : 2.093,
  20 : 2.086
}

#############################################################################
# cluster value keys
#############################################################################
ITEMS   = "items"
COLOR   = "color"

BLUE   = "blue"
RED    = "red"
GREEN  = "green"
BONE   = "bone"
ORANGE = "orange"
PURPLE = "purple"
GREY   = "grey"

cluster_colors = [ BLUE, RED, GREEN, BONE, ORANGE, PURPLE, GREY ]

colord = {
  BLUE   : 'Blues',
  RED    : 'Reds',
  GREEN  : 'Greens',
  BONE   : 'bone_r',
  ORANGE : 'Oranges',
  PURPLE : 'Purples',
  GREY   : 'Greys'
}

#############################################################################
# heap sizes
#############################################################################
heap_sizes = {
  "avrora-default"       : "30g",
  "avrora-large"         : "30g",
  "batik-default"        : "30g",
  "batik-large"          : "30g",
  "fop-default"          : "30g",
  "fop-large"            : "30g",
  "eclipse-default"      : "30g",
  "eclipse-large"        : "30g",
  "h2-default"           : "30g",
  "h2-large"             : "30g",
  "jython-default"       : "30g",
  "jython-large"         : "30g",
  "luindex-default"      : "30g",
  "luindex-large"        : "30g",
  "lusearch-default"     : "30g",
  "lusearch-large"       : "30g",
  "pmd-default"          : "30g",
  "pmd-large"            : "30g",
  "sunflow-default"      : "30g",
  "sunflow-large"        : "30g",
  "tomcat-default"       : "30g",
  "tomcat-large"         : "30g",
  "tradebeans-default"   : "30g",
  "tradebeans-large"     : "30g",
  "tradesoap-default"    : "30g",
  "tradesoap-large"      : "30g",
  "xalan-default"        : "30g",
  "xalan-large"          : "30g",
  "compiler.compiler"    : "30g",
  "compiler.sunflow"     : "30g",
  "compress"             : "30g",
  "crypto.aes"           : "30g",
  "crypto.rsa"           : "30g",
  "crypto.signverify"    : "30g",
  "serial"               : "30g",
  "derby"                : "30g",
  "sunflow"              : "30g",
  "mpegaudio"            : "30g",
  "xml.transform"        : "30g",
  "xml.validation"       : "30g",
  "scimark.fft.small"    : "4g",
  "scimark.fft.large"    : "16g",
  "scimark.lu.small"     : "4g",
  "scimark.lu.large"     : "16g",
  "scimark.sor.small"    : "4g",
  "scimark.sor.large"    : "16g",
  "scimark.sparse.small" : "4g",
  "scimark.sparse.large" : "16g",
  "scimark.monte_carlo"  : "16g"
}

#############################################################################
# invocation counts
#############################################################################
histoHeaderRE = re.compile("____Count_\(I\+C\)____Method________________________Module_________________")
icntRE  = re.compile("\s*invocation_counter")
bcntRE  = re.compile("\s*backedge_counter")
#icntRE  = re.compile("\s*our_invocation_counter")
#bcntRE  = re.compile("\s*our_backedge_counter")
compRE  = re.compile("\s*compiled_invocation_count")
apmRE   = re.compile("\s*ap_method:")
mprefs  = ["static ", "final ", "synchronized ", "native "]
compile_threshold = 10000

#############################################################################
# active set report
#############################################################################
DURATION  = "DURATION"
AGGREGATE = "aggregate"
AVERAGE   = "average"
MEDIAN    = "median"

LIVE    = "LIVE"
HOT     = "HOT"
OBJECTS = "OBJECTS"
SIZE    = "SIZE"
KLASSES = "KLASSES"
VAL     = "VAL"

LIFE    = "LIFE"
REFS    = "REFS"

# objInfoKeys
LIVE_OBJECTS       = "LIVE_OBJECTS"
LIVE_SIZE          = "LIVE_SIZE"
LIVE_REFS          = "LIVE_REFS"
HOT_OBJECTS        = "HOT_OBJECTS"
HOT_SIZE           = "HOT_SIZE"
HOT_REFS           = "HOT_REFS"
NEW_OBJECTS        = "NEW_OBJECTS"
NEW_SIZE           = "NEW_SIZE"
NEW_REFS           = "NEW_REFS"
VM_OBJECTS         = "VM_OBJECTS"
VM_SIZE            = "VM_SIZE"
VM_REFS            = "VM_REFS"
FILLER_OBJECTS     = "FILLER_OBJECTS"
FILLER_SIZE        = "FILLER_SIZE"
FILLER_REFS        = "FILLER_REFS"
APP_OBJECTS        = "APP_OBJECTS"
APP_SIZE           = "APP_SIZE"
APP_REFS           = "APP_REFS"
HCO_OBJECTS        = "HCO_OBJECTS"
HCO_SIZE           = "HCO_SIZE"
HCO_REFS           = "HCO_REFS"
HLVM_OBJECTS       = "HLVM_OBJECTS"
HLVM_SIZE          = "HLVM_SIZE"
CLVM_OBJECTS       = "CLVM_OBJECTS"
CLVM_SIZE          = "CLVM_SIZE"
TOTAL_OBJECT_RATIO = "TOTAL_OBJECT_RATIO"
TOTAL_SIZE_RATIO   = "TOTAL_SIZE_RATIO"
TOTAL_REFS_RATIO   = "TOTAL_REFS_RATIO"
HOT_OBJECT_RATIO   = "HOT_OBJECT_RATIO"
HOT_SIZE_RATIO     = "HOT_SIZE_RATIO"
HOT_REFS_RATIO     = "HOT_REFS_RATIO"
NEW_APP_OBJ_RATIO  = "NEW_APP_OBJ_RATIO"
NEW_APP_SIZE_RATIO = "NEW_APP_SIZE_RATIO"
NEW_APP_REFS_RATIO = "NEW_APP_REFS_RATIO"

FIXED_SIZE_REFS = "FIXED_SIZE_REFS"

objInfoKeys = [ LIVE_OBJECTS, LIVE_SIZE, LIVE_REFS,
                HOT_OBJECTS, HOT_SIZE, HOT_REFS ]

# posKeys
PRE  = "PRE"
YCS  = "YCS"
POST = "POST"

posKeys = [PRE, YCS, POST]

# genKeys
YOUNG = "YOUNG"
OLD   = "OLD"
PERM  = "PERM"
genKeys = [YOUNG, OLD, PERM]
coloredGenKeys = [YOUNG, OLD]

# spaces
UNCOLORED = "-"
RED       = "RED"
BLUE      = "BLUE"
coloredSpaceKeys = [RED, BLUE]
spaceKeys        = [UNCOLORED] + coloredSpaceKeys

## ycsGens
#YOUNG_TO_YOUNG = "YOUNG_TO_YOUNG"
#YOUNG_TO_OLD   = "YOUNG_TO_OLD"
#
#ycsGens = [ YOUNG_TO_YOUNG, YOUNG_TO_OLD ]

# ycsSpaces
RED_TO_RED   = "RED_TO_RED"
RED_TO_BLUE  = "RED_TO_BLUE"
BLUE_TO_RED  = "BLUE_TO_RED"
BLUE_TO_BLUE = "BLUE_TO_BLUE"

ycsSpaceKeys = [ RED_TO_RED, RED_TO_BLUE, BLUE_TO_RED, BLUE_TO_BLUE ]
ycsFrom = {
  RED  : [ RED_TO_RED,  RED_TO_BLUE  ],
  BLUE : [ BLUE_TO_RED, BLUE_TO_BLUE ]
}
ycsTo = {
  RED  : [ RED_TO_RED,  BLUE_TO_RED  ],
  BLUE : [ RED_TO_BLUE, BLUE_TO_BLUE ]
}

preOrgRE      = re.compile("\s*pre-org")
endRunRE      = re.compile("\s*end-of-run ObjectInfoCollection:")
ycsRE         = re.compile("\s*young collection stats")
postOrgRE     = re.compile("\s*post-org")
livesRE       = re.compile("\s*(\w)* [\-(\w)]*\s*lives:")
hotsRE        = re.compile("\s*(\w)* [\-(\w)]*\s*hots:")
hotKlassRE    = re.compile("\s*\* (\S)* :")
ycsLivesRE    = re.compile("\s*lives \(")
ycsHotsRE     = re.compile("\s*hots \(")
space2spaceRE = re.compile("\s*(\w)*_to_(\w)*")
objrefRE      = re.compile("(\W)*(\d+)(\W)+(\d+)")
deadsHeaderRE = re.compile("(\S)+ -- dumping deads: val = (\d+)")
deadsDoneRE   = re.compile("done dumping deads")
headerEndRE   = re.compile("----------------------------------------")
oicValRE      = re.compile("\s*(\S)*\s*ObjectInfoCollection:")
aicValRE      = re.compile("\s*(\S)*\s*APInfoCollection:")
oaicValRE     = re.compile("\s*(\S)*\s*ObjectAddressInfoCollection:")
klassRecRE    = re.compile("(\s)*(\d+) :(\s){0x(\S+)}")
hmsValRE      = re.compile("(\S)*\s*HotMethodSample:")
mkalRE        = re.compile("m(\d+):")
#fieldRecRE    = re.compile("(\s)+(\d+)(\s)+(\d+)(\s)+(\d+)")


# active set report
INFO_NVALS        = "INFO_NVALS"
INFO_LIVE_RSS     = "INFO_LIVE_RSS"
INFO_HOT_RSS      = "INFO_HOT_RSS"
INFO_REFS         = "INFO_REFS"
INFO_SURV_RSS     = "INFO_SURV_RSS"
INFO_HOT_SURV_RSS = "INFO_HOT_SURV_RSS"
INFO_SURV_REFS    = "INFO_SURV_REFS"
INFO_TO_REDS      = "INFO_TO_REDS"
INFO_TO_BLUES     = "INFO_TO_BLUES"
INFO_MIS_SPEC     = "INFO_MIS_SPEC"

ratinfos = [
  INFO_LIVE_RSS,
  INFO_HOT_RSS,
  INFO_REFS,
  INFO_SURV_RSS,
  INFO_HOT_SURV_RSS,
  INFO_SURV_REFS,
  INFO_TO_REDS,
  INFO_TO_BLUES,
  INFO_MIS_SPEC
]

definfos = [INFO_NVALS] + ratinfos
rssinfos = [INFO_LIVE_RSS, INFO_HOT_RSS, INFO_SURV_RSS, INFO_HOT_SURV_RSS]
stdinfos = [INFO_NVALS] + \
           [ INFO_LIVE_RSS, INFO_REFS, INFO_SURV_RSS, \
             INFO_TO_REDS, INFO_TO_BLUES ]
infos_a  = [INFO_NVALS] + \
           [ INFO_LIVE_RSS, INFO_REFS, INFO_MIS_SPEC ]

activeSetHeaders = {
  INFO_NVALS        : "nvals",
  INFO_LIVE_RSS     : "live %s",
  INFO_HOT_RSS      : "hot %s",
  INFO_REFS         : "refs",
  INFO_SURV_RSS     : "surv %s",
  INFO_HOT_SURV_RSS : "hot surv %s",
  INFO_SURV_REFS    : "surv refs",
  INFO_TO_REDS      : "to reds",
  INFO_TO_BLUES     : "to blues",
  INFO_MIS_SPEC     : "mis-spec"
}

orderedInfos = {
  INFO_NVALS        : 0,
  INFO_LIVE_RSS     : 1,
  INFO_HOT_RSS      : 2,
  INFO_REFS         : 3,
  INFO_SURV_RSS     : 4,
  INFO_HOT_SURV_RSS : 5,
  INFO_SURV_REFS    : 6,
  INFO_TO_REDS      : 7,
  INFO_TO_BLUES     : 8,
  INFO_MIS_SPEC     : 9
}

#############################################################################
# access info dictionary (accessd)
#############################################################################
OBJINFO        = "OBJINFO"
LIVE_VALS      = "LIVE_VALS"
ACTIVE_VALS    = "ACTIVE_VALS"
REFS_PER_VAL   = "REFS_PER_VAL"
LONG_LIVED     = "LONG_LIVED"
HOT_VALS       = "HOT_VALS"
LIVE_TIME      = "LIVE_TIME"
HOT_TIME       = "HOT_TIME"
TOTAL_TIME     = "TOTAL_TIME"
MAX_HOT        = "MAX_HOT"
MAX_COLD       = "MAX_COLD"
HOT_RATE       = "HOT_RATE"
TOTAL_OBJECTS  = "TOTAL_OBJECTS"
TOTAL_SIZE     = "TOTAL_SIZE"
TOTAL_REFS     = "TOTAL_REFS"
HOT_RATE_HISTO = "HOT_RATE_HISTO"

TOTAL_VALS     = "TOTAL_VALS"
LL_OBJECTS     = "LL_OBJECTS"
LL_SIZE        = "LL_SIZE"
LL_SPAN        = "LL_SPAN"
COLD_OBJECTS   = "COLD_OBJECTS"
COLD_SIZE      = "COLD_SIZE"
COLD_SPAN      = "COLD_SPAN"

#############################################################################
# allocation point dictionary (some keys reused)
#############################################################################
KLASS            = "KLASS"
EARLY_REFS       = "EARLY_REFS"
RED_REFS         = "RED_REFS"
BLUE_REFS        = "BLUE_REFS"
METHOD_NAME      = "METHOD_NAME"
ALLOC_POINT      = "ALLOC_POINT"
APS              = "APS"
APD              = "APD"
HKD              = "HKD"
TOTAL            = "TOTAL"
KAP              = "KAP"
INIT_COLOR       = "INIT_COLOR"
#APP_RECORD       = "APP_RECORD"
#UNKNOWN_RECORD   = "UNKNOWN_RECORD"
#SUM_TOTAL_RECORD = "SUM_TOTAL_RECORD"

#############################################################################
# allocation point report
#############################################################################
SIZE_SORT   = "SIZE_SORT"
OBJECT_SORT = "OBJECT_SORT"
REF_SORT    = "REF_SORT"

coverrats          = {}
coverrats[REFS]    = [0.5, 0.8, 0.9, 0.95, 0.99, 0.999]
coverrats[SIZE]    = [0.001, 0.01, 0.05, 0.1, 0.2, 0.5]
coverrats[OBJECTS] = [0.001, 0.01, 0.05, 0.1, 0.2, 0.5]

#############################################################################
# colored space size report
#############################################################################
HEAP_BEFORE   = "HEAP_BEFORE"
HEAP_AFTER    = "HEAP_AFTER"
EDEN          = "EDEN"
SURVIVOR      = "SURVIVOR"
TENURED       = "TENURED"
PERM          = "PERM"
INVALID_SPACE = "INVALID_SPACE"
INVOCATION    = "INVOCATION"
REL_TIME      = "REL_TIME"
REASON        = "REASON"

#############################################################################
# object address info report
#############################################################################
PRE_GC           = "PRE_GC"
TIMER_CUTOFF     = "TIMER_CUTOFF"
REF_CUTOFF       = "REF_CUTOFF"
RPB_CUTOFF       = "RPB_CUTOFF"
HOT_CUTOFF       = "HOT_CUTOFF"
RUN_VALS         = "RUN_VALS"
DUR_RATIO        = "DUR_RATIO"
USED_BYTES       = "USED_BYTES"
RANGE            = "RANGE"
SPACES           = "SPACES"
TOTAL_PAGES      = "TOTAL_PAGES"
UNKNOWN_SIZE     = "UNKNOWN_SIZE"
HCO_PAGES        = "HCO_PAGES"
HCO_PAGE_HISTO   = "HCO_PAGE_HISTO"
HCO_PAGE_RATIO   = "HCO_PAGE_RATIO"
SIM_PAGES        = "SIM_PAGES"
SIM_PAGE_HISTO   = "SIM_PAGE_HISTO"
SIM_PAGE_RATIO   = "SIM_PAGE_RATIO"
HLVM_OBJ_RATIO   = "HLVM_OBJ_RATIO"
HLVM_SIZE_RATIO  = "HLVM_SIZE_RATIO"
CLVM_OBJ_RATIO   = "CLVM_OBJ_RATIO"
CLVM_SIZE_RATIO  = "CLVM_SIZE_RATIO"
HCO_OBJ_RATIO    = "HCO_OBJ_RATIO"
HCO_SIZE_RATIO   = "HCO_SIZE_RATIO"
HOT_REF_RATIO    = "HOT_REF_RATIO"
HCO_REF_RATIO    = "HCO_REF_RATIO"
WGT_AVG          = "WGT_AVG"
SUM_AGG          = "SUM_AGG"
ALL_SPACES       = [EDEN, SURVIVOR, TENURED, PERM]
APP_SPACES       = [EDEN, SURVIVOR, TENURED]
SURV_SPACES      = [SURVIVOR, TENURED]

INS_OBJECTS      = "INS_OBJECTS"
INS_SIZE         = "INS_SIZE"
ARR_OBJECTS      = "ARR_OBJECTS"
ARR_SIZE         = "ARR_SIZE"

NEW_APP_OBJECTS  = "NEW_APP_OBJECTS"
NEW_APP_SIZE     = "NEW_APP_SIZE"
NEW_APP_REFS     = "NEW_APP_REFS"

HOT_INS_OBJECTS  = "HOT_INS_OBJECTS"
HOT_INS_SIZE     = "HOT_INS_SIZE"
HOT_ARR_OBJECTS  = "HOT_ARR_OBJECTS"
HOT_ARR_SIZE     = "HOT_ARR_SIZE"

HOT_OBJ_RATIO    = "HOT_OBJ_RATIO"
HOT_SIZE_RATIO   = "HOT_SIZE_RATIO"
INS_OBJ_RATIO    = "INS_OBJ_RATIO"
INS_SIZE_RATIO   = "INS_SIZE_RATIO"
ARR_OBJ_RATIO    = "ARR_OBJ_RATIO"
ARR_SIZE_RATIO   = "ARR_SIZE_RATIO"

HOT_INS_OBJ_RATIO  = "HOT_INS_OBJ_RATIO"
HOT_INS_SIZE_RATIO = "HOT_INS_SIZE_RATIO"
HOT_ARR_OBJ_RATIO  = "HOT_ARR_OBJ_RATIO"
HOT_ARR_SIZE_RATIO = "HOT_ARR_SIZE_RATIO"

COVERED_BYTES      = "COVERED_BYTES"
PACKED_SIZE        = "PACKED_SIZE"
PACKED_SIZE_ALIGN  = "PACKED_SIZE_ALIGN"
BYTES_WASTED       = "BYTES_WASTED"
BYTES_WASTED_ALIGN = "BYTES_WASTED_ALIGN"

KLASS_ADDR    = "KLASS_ADDR"
KLASS_NAME    = "KLASS_NAME"
KLASS_TYPE    = "KLASS_TYPE"
INSTANCE_SIZE = "INSTANCE_SIZE"
KLASS_RECORDS = "KLASS_RECORDS"
FIELDS        = "FIELDS"
FIELD_REFS    = "FIELD_REFS"
ALL_KLASSES   = "ALL_KLASSES"
KT_TOTALS     = "KT_TOTALS"

MARK_OOP_SIZE   = 8
KLASS_OOP_SIZE  = 4
OOP_HEADER_SIZE = (MARK_OOP_SIZE + KLASS_OOP_SIZE)
WORD_SIZE       = 8

NONFILL_OBJECTS = "NONFILL_OBJECTS"
NONFILL_SIZE    = "NONFILL_SIZE"

KT_UNSPECIFIED  = "UNSPECIFIED"
KT_INVALID      = "INVALID"
KT_VM_INSTANCE  = "VM_INSTANCE"
KT_VM_ARRAY     = "VM_ARRAY"
KT_VM_FILLER    = "VM_FILLER"
KT_VM_OTHER     = "VM_OTHER"
KT_APP_INSTANCE = "APP_INSTANCE"
KT_APP_ARRAY    = "APP_ARRAY"
KT_APP_FILLER   = "APP_FILLER"

klass_types = [
  KT_UNSPECIFIED,
  KT_INVALID,
  KT_VM_INSTANCE,
  KT_VM_ARRAY,
  KT_VM_FILLER,
  KT_VM_OTHER,
  KT_APP_INSTANCE,
  KT_APP_ARRAY,
  KT_APP_FILLER
]

RECORD_HISTORY = "RECORD_HISTORY"
VAL_HISTORY    = "VAL_HISTORY"
RECORD_FUTURE  = "RECORD_FUTURE"
VAL_FUTURE     = "VAL_FUTURE"
GRP_FUTURE     = "GRP_FUTURE"

TOTAL_HOT      = "TOTAL_HOT"
TOTAL_NEW      = "TOTAL_NEW"
NEVER_NEW      = "NEVER_NEW"
NEW            = "NEW"
NEVER          = "NEVER"
MAX            = "MAX"

VAL_1          = "VAL_1"
VAL_2          = "VAL_2"
VAL_4          = "VAL_4"
VAL_8          = "VAL_8"
VAL_16         = "VAL_16"
VAL_MAX        = "VAL_MAX"

NEWV_1         = "NEWV_1"
NEWV_2         = "NEWV_2"
NEWV_4         = "NEWV_4"
NEWV_8         = "NEWV_8"
NEWV_16        = "NEWV_16"
NEWV_MAX       = "NEWV_MAX"

GRP_1          = "GRP_1"
GRP_2          = "GRP_2"
GRP_3to4       = "GRP_3-4"
GRP_5to8       = "GRP_5-8"
GRP_9to16      = "GRP_9-16"
GRP_MAX        = "GRP_MAX"

NEWG_1         = "NEWG_1"
NEWG_2         = "NEWG_2"
NEWG_3to4      = "NEWG_3-4"
NEWG_5to8      = "NEWG_5-8"
NEWG_9to16     = "NEWG_9-16"
NEWG_MAX       = "NEWG_MAX"

VMAX           = 17

defhvals       = [ (None, TOTAL_HOT ), \
                   (None, NEW       ), \
                   (1,    VAL_1     ), \
                   (2,    VAL_2     ), \
                   (4,    VAL_4     ), \
                   (8,    VAL_8     ), \
                   (16,   VAL_16    ), \
                   (VMAX, VAL_MAX   )  ]

deffvals       = [ (None, TOTAL_HOT ), \
                   (None, NEVER     ), \
                   (None, TOTAL_NEW ), \
                   (None, NEVER_NEW ), \
                   (1,    VAL_1      ), \
                   (2,    VAL_2      ), \
                   (4,    VAL_4      ), \
                   (8,    VAL_8      ), \
                   (16,   VAL_16     ), \
                   (VMAX, VAL_MAX    ), \
                   (1,    NEWV_1     ), \
                   (2,    NEWV_2     ), \
                   (4,    NEWV_4     ), \
                   (8,    NEWV_8     ), \
                   (16,   NEWV_16    ), \
                   (VMAX, NEWV_MAX   )  ]

deffgrps       = [ (None, TOTAL_HOT       ), \
                   (None, NEVER           ), \
                   (None, TOTAL_NEW       ), \
                   (None, NEVER_NEW       ), \
                   ([1],       GRP_1      ), \
                   ([2],       GRP_2      ), \
                   ([3,4],     GRP_3to4   ), \
                   ([5,6,7,8], GRP_5to8   ), \
                   ([x for x in range(9,17)], GRP_9to16  ), \
                   ([VMAX],    GRP_MAX    ), \
                   ([1],       NEWG_1     ), \
                   ([2],       NEWG_2     ), \
                   ([3,4],     NEWG_3to4  ), \
                   ([5,6,7,8], NEWG_5to8  ), \
                   ([x for x in range(9,17)], NEWG_9to16  ), \
                   ([VMAX],    NEWG_MAX   )  ]

LEFT_ALIGN  = 0
RIGHT_ALIGN = 1

#############################################################################
# custom benches
#############################################################################
pct2delay = {
  1.0   : "8900",
  2.0   : "3750",
  5.0   : "1350",
  10.0  : "640",
  15.0  : "426",
  20.0  : "325",
  25.0  : "266",
  30.0  : "226",
  50.0  : "170",
  100.0 : "104", # use 200ns as our baseline
}

ns2delay = {
  0     : "0",  # 85ns
  100   : "20",
  150   : "63",
  200   : "104",
  250   : "150",
  300   : "183",
  350   : "220",
  400   : "264",
  500   : "346",
  750   : "553",
  1000  : "760",
  2000  : "1590",
  2500  : "2005",
  5000  : "4078",
  10000 : "8225",
  25000 : "20670",
  50000 : "41390",
  500000 : "415000",
}

brazil_delay = {
  200 : "302"
}

custbench_cfgs = {
  'StoreBench-tiny'  : ( [                                                  ], ["2",  "30",   ns2delay[0],    "0.01", "0.02" ]),
  'StoreBench-dxx'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "10",   ns2delay[0],    "2.3125", "16.1875" ]),
  'StoreBench-d0'    : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "1040", ns2delay[0],    "2.3125", "16.1875" ]),
  'StoreBench-d100'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "640",  ns2delay[100],  "2.3125", "16.1875" ]),
  'StoreBench-d150'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "280",  ns2delay[150],  "2.3125", "16.1875" ]),
  'StoreBench-d200'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "181",  ns2delay[200],  "2.3125", "16.1875" ]),
  'StoreBench-d250'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "130",  ns2delay[250],  "2.3125", "16.1875" ]),
  'StoreBench-d300'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "104",  ns2delay[300],  "2.3125", "16.1875" ]),
  'StoreBench-d350'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "88",   ns2delay[350],  "2.3125", "16.1875" ]),
  'StoreBench-d400'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "78",   ns2delay[400],  "2.3125", "16.1875" ]),
  'StoreBench-d500'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "53",   ns2delay[500],  "2.3125", "16.1875" ]),
  'StoreBench-d750'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "43",   ns2delay[750],  "2.3125", "16.1875" ]),
  'StoreBench-d1000' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "32",   ns2delay[1000], "2.3125", "16.1875" ]),
  'StoreBench-d2000' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "16",   ns2delay[2000], "2.3125", "16.1875" ]),

#  'BandwidthBench-d0'     : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "270", ns2delay[0],     "0.75", "0" ]),
#  'BandwidthBench-dxx'    : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["1",  "1", "0", "1.0", "0.01", "0.01" ]),
#  'BandwidthBench-d100'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "250", ns2delay[100],   "0.75", "0" ]),
#  'BandwidthBench-d150'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "230", ns2delay[150],   "0.75", "0" ]),
#  'BandwidthBench-d200'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "160", ns2delay[200],   "0.75", "0" ]),
#  'BandwidthBench-d250'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "120", ns2delay[250],   "0.75", "0" ]),
#  'BandwidthBench-d300'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "95",  ns2delay[300],   "0.75", "0" ]),
#  'BandwidthBench-d500'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "50",  ns2delay[500],   "0.75", "0" ]),
#  'BandwidthBench-d1000'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "23",  ns2delay[1000],  "0.75", "0" ]),
#  'BandwidthBench-d2000'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "12",  ns2delay[2000],  "0.75", "0" ]),
#  'BandwidthBench-d2500'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "9",   ns2delay[2500],  "0.75", "0" ]),
#  'BandwidthBench-d5000'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "4",   ns2delay[5000],  "0.75", "0" ]),
#  'BandwidthBench-d10000' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "2",   ns2delay[10000], "0.75", "0" ]),
#  'BandwidthBench-d25000' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "1",   ns2delay[25000], "0.75", "0" ]),
#  'BandwidthBench-d50000' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "1",   ns2delay[50000], "0.75", "0" ]),
#  'BandwidthBench-d500000': ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "1",   ns2delay[500000],"0.75", "0" ]),

  'BandwidthBench-x1.0'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "1.0",  "4", "16" ]),
  'BandwidthBench-p1.0'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "1.0",  "4", "16" ]),
  'BandwidthBench-p0.99'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.99", "4", "16" ]),
  'BandwidthBench-p0.98'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.98", "4", "16" ]),
  'BandwidthBench-p0.95'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.95", "4", "16" ]),
  'BandwidthBench-p0.9'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.9",  "4", "16" ]),
  'BandwidthBench-p0.85'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.85", "4", "16" ]),
  'BandwidthBench-p0.8'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.8",  "4", "16" ]),
  'BandwidthBench-p0.75'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.75", "4", "16" ]),
  'BandwidthBench-p0.7'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.7",  "4", "16" ]),
  'BandwidthBench-p0.6'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.6",  "4", "16" ]),
  'BandwidthBench-p0.5'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.5",  "4", "16" ]),
  'BandwidthBench-p0.4'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.4",  "4", "16" ]),
  'BandwidthBench-p0.3'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.3",  "4", "16" ]),
  'BandwidthBench-p0.2'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.2",  "4", "16" ]),
  'BandwidthBench-p0.1'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.1",  "4", "16" ]),
  'BandwidthBench-p0.0'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[200],  "0.0",  "4", "16" ]),

  'BandwidthBench-xp1.0'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "1.0",  "4", "16" ]),
  'BandwidthBench-xp0.99' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.99", "4", "16" ]),
  'BandwidthBench-xp0.98' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.98", "4", "16" ]),
  'BandwidthBench-xp0.95' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.95", "4", "16" ]),
  'BandwidthBench-xp0.9'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.9",  "4", "16" ]),
  'BandwidthBench-xp0.85' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.85", "4", "16" ]),
  'BandwidthBench-xp0.8'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.8",  "4", "16" ]),
  'BandwidthBench-xp0.75' : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.75", "4", "16" ]),
  'BandwidthBench-xp0.7'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.7",  "4", "16" ]),
  'BandwidthBench-xp0.6'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.6",  "4", "16" ]),
  'BandwidthBench-xp0.5'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", ns2delay[150],  "0.5",  "4", "16" ]),

  'SimpleBB-tiny'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "10",  brazil_delay[200],  "1.0",  "2", "8" ]),
  'SimpleBB-p1.0'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "1.0",  "200", "800" ]),
  'SimpleBB-p0.99'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.99", "200", "800" ]),
  'SimpleBB-p0.98'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.98", "200", "800" ]),
  'SimpleBB-p0.95'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.95", "200", "800" ]),
  'SimpleBB-p0.9'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.9",  "200", "800" ]),
  'SimpleBB-p0.85'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.85", "200", "800" ]),
  'SimpleBB-p0.8'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.8",  "200", "800" ]),
  'SimpleBB-p0.75'  : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.75", "200", "800" ]),
  'SimpleBB-p0.7'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.7",  "200", "800" ]),
  'SimpleBB-p0.6'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.6",  "200", "800" ]),
  'SimpleBB-p0.5'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.5",  "200", "800" ]),
  'SimpleBB-p0.4'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.4",  "200", "800" ]),
  'SimpleBB-p0.3'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.3",  "200", "800" ]),
  'SimpleBB-p0.2'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.2",  "200", "800" ]),
  'SimpleBB-p0.1'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.1",  "200", "800" ]),
  'SimpleBB-p0.0'   : ( [ "-XX:NewSize=66571993088", "-Xms90g",  "-Xmx90g" ], ["12", "105", brazil_delay[200],  "0.0",  "200", "800" ]),
}

#custbench_cfgs = {
#  'StoreBench-tiny'     : ( [ "-Ddelay=0"   ], ["2",  "100", "0",  "0.01", "0.02" ]),
#  #'StoreBench-tiny'     : ( [ "-Xms10g",  "-Xmx10g", "-Ddelay=0"   ], ["1",  "10",   "0.01", "0.02" ]),
#  'StoreBench-t1-d0'    : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=0"   ], ["1",  "50",   "0.25", "1.5" ]),
#  'StoreBench-t2-d0'    : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=0"   ], ["2",  "100",  "0.25", "1.5" ]),
#  'StoreBench-t4-d0'    : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=0"   ], ["4",  "200",  "0.25", "1.5" ]),
#  'StoreBench-t8-d0'    : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=0"   ], ["8",  "400",  "0.25", "1.5" ]),
#  'StoreBench-t15-d0'   : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=0"   ], ["15", "750",  "0.25", "1.5" ]),
#  'StoreBench-t16-d0'   : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=0"   ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d50'  : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=50"  ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d100' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=100" ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d150' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=150" ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d175' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=175" ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d200' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=200" ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d225' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=225" ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d250' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=250" ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d300' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=300" ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d500' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=500" ], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-t16-d1000': ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=1000"], ["16", "800",  "0.25", "1.5" ]),
#  'StoreBench-bw-d0'    : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=0"   ], ["2",  "500",  "0.05", "0.3" ]),
#  'StoreBench-bw-d50'   : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=50"  ], ["2",  "500",  "0.05", "0.3" ]),
#  'StoreBench-bw-d75'   : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=75"  ], ["2",  "500",  "0.05", "0.3" ]),
#  'StoreBench-bw-d80'   : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=80"  ], ["2",  "200",  "0.05", "0.3" ]),
#  'StoreBench-bw-d100'  : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=100" ], ["2",  "100",  "0.05", "0.3" ]),
#  'StoreBench-bw-d250'  : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=250" ], ["2",  "50",   "0.05", "0.3" ]),
#  'StoreBench-bw-d500'  : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=500" ], ["2",  "25",   "0.05", "0.3" ]),
#  'StoreBench-bw-d1000' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=1000"], ["2",  "10",   "0.05", "0.3" ]),
#  'StoreBench-bw-d2000' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=2000"], ["2",  "4",    "0.05", "0.3" ]),
#  'StoreBench-bw-d5000' : ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=5000"], ["2",  "2",    "0.05", "0.3" ]),
#  'StoreBench-bw-d10000': ( [ "-Xms60g",  "-Xmx60g", "-Ddelay=10000"],["2",  "1",    "0.05", "0.3" ]),
#  'StoreBench-xx'       : ( (alloc_opts["StoreBench"] + ["-Ddelay=0"   ]), ["16", "1",    "0.48", "2.72" ]),
#  'StoreBench-xx-d0'    : ( (alloc_opts["StoreBench"] + ["-Ddelay=0"   ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d50'   : ( (alloc_opts["StoreBench"] + ["-Ddelay=50"  ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d60'   : ( (alloc_opts["StoreBench"] + ["-Ddelay=60"  ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d70'   : ( (alloc_opts["StoreBench"] + ["-Ddelay=70"  ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d75'   : ( (alloc_opts["StoreBench"] + ["-Ddelay=75"  ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d80'   : ( (alloc_opts["StoreBench"] + ["-Ddelay=80"  ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d90'   : ( (alloc_opts["StoreBench"] + ["-Ddelay=90"  ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d100'  : ( (alloc_opts["StoreBench"] + ["-Ddelay=100" ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d150'  : ( (alloc_opts["StoreBench"] + ["-Ddelay=150" ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d200'  : ( (alloc_opts["StoreBench"] + ["-Ddelay=200" ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d225'  : ( (alloc_opts["StoreBench"] + ["-Ddelay=225" ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d250'  : ( (alloc_opts["StoreBench"] + ["-Ddelay=250" ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d300'  : ( (alloc_opts["StoreBench"] + ["-Ddelay=300" ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d500'  : ( (alloc_opts["StoreBench"] + ["-Ddelay=500" ]), ["16", "400",  "0.48", "2.72" ]),
#  'StoreBench-xx-d1000' : ( (alloc_opts["StoreBench"] + ["-Ddelay=1000"]), ["16", "400",  "0.48", "2.72" ]),
#  'MemBench-t1'         : ( [ "-Xms60g",  "-Xmx60g",  ], ["1",  "50",   "0.25", "1.5" ]),
#  'MemBench-t2'         : ( [ "-Xms60g",  "-Xmx60g",  ], ["2",  "100",  "0.25", "1.5" ]),
#  'MemBench-t4'         : ( [ "-Xms60g",  "-Xmx60g",  ], ["4",  "200",  "0.25", "1.5" ]),
#  'MemBench-t8'         : ( [ "-Xms60g",  "-Xmx60g",  ], ["8",  "400",  "0.25", "1.5" ]),
#  'MemBench-t15'        : ( [ "-Xms60g",  "-Xmx60g",  ], ["15", "750",  "0.25", "1.5" ]),
#  'MemBench-t16'        : ( [ "-Xms60g",  "-Xmx60g",  ], ["16", "800",  "0.25", "1.5" ]),
#  'MemBench-tiny'       : ( [ "-Xms60g",  "-Xmx60g",  ], ["1",  "50",   "0.25", "1.5" ]),
#  'MemBench-small'      : ( [ "-Xms60g",  "-Xmx60g",  ], ["8",  "400",  "0.25", "1.0" ]),
#  'MemBench-large'      : ( [ "-Xms60g",  "-Xmx60g",  ], ["15", "1000", "0.25", "1.0" ]),
#  'LoadBench-small-test': ( [ "-Xms60g",  "-Xmx60g",  ], ["16", "2000", "1.5",  "0.1" ]),
#  #'LoadBench-large'     : ( [ "-Xms60g",  "-Xmx60g",  ], ["16", "2000", "0.15", "1.35" ]),
#  'LoadBench-large'     : ( [ "-Xms60g",  "-Xmx60g",  ], ["16", "2000", "0.15", "1.35" ]),
#  'LoadBench-small'     : ( [ "-Xms60g",  "-Xmx60g",  ], ["16", "500",  "0.25", "1.5" ]),
#  'LoadBench-large-test': ( [ "-Xms60g",  "-Xmx60g",  ], ["16", "1000", "0.25", "1.5" ]),
#  'LoadBench-large-t1'  : ( [ "-Xms60g",  "-Xmx60g",  ], ["1",  "50",   "0.25", "1.5" ]),
#  'LoadBench-large-t2'  : ( [ "-Xms60g",  "-Xmx60g",  ], ["2",  "100",  "0.25", "1.5" ]),
#  'LoadBench-large-t4'  : ( [ "-Xms60g",  "-Xmx60g",  ], ["4",  "200",  "0.25", "1.5" ]),
#  'LoadBench-large-t8'  : ( [ "-Xms60g",  "-Xmx60g",  ], ["8",  "400",  "0.25", "1.5" ]),
#  'LoadBench-large-t16' : ( [ "-Xms60g",  "-Xmx60g",  ], ["16", "800",  "0.25", "1.5" ]),
#  'LoadBench-small-t1'  : ( [ "-Xms60g",  "-Xmx60g",  ], ["1",  "50",   "0.25", "1.0" ]),
#  'LoadBench-small-t2'  : ( [ "-Xms60g",  "-Xmx60g",  ], ["2",  "100",  "0.25", "1.0" ]),
#  'LoadBench-small-t4'  : ( [ "-Xms60g",  "-Xmx60g",  ], ["4",  "200",  "0.25", "1.0" ]),
#  'LoadBench-small-t8'  : ( [ "-Xms60g",  "-Xmx60g",  ], ["8",  "400",  "0.25", "1.0" ]),
#  'LoadBench-small-t16' : ( [ "-Xms60g",  "-Xmx60g",  ], ["16", "800",  "0.25", "1.0" ]),
#}

membenches = custbench_cfgs.keys()

#############################################################################
# pcm report
#############################################################################
TIME_ELAPSED = "TIME_ELAPSED"
CKEOFF       = "CKEOFF"
SOCK_WATTS   = "SOCK_WATTS"
SOCK_JOULES  = "SOCK_JOULES"
DRAM_WATTS   = "DRAM_WATTS"
DRAM_JOULES  = "DRAM_JOULES"

# pcm-power.x
sampleRateRE  = re.compile("Update every (\d+) seconds")
powValRE      = re.compile("-----------------------------------------------")
chanInfoRE    = re.compile("S(\d+)CH(\d+); DRAMClocks:")
timeElapsedRE = re.compile("Time elapsed: (\d+)")
sockInfoRE    = re.compile("S(\d+); Consumed energy")
dramInfoRE    = re.compile("S(\d+); Consumed DRAM energy")

CHAN_READ     = "CHAN_READ"
CHAN_WRITE    = "CHAN_WRITE"
NODE_READ     = "NODE_READ"
NODE_WRITE    = "NODE_WRITE"
NODE_PWRITE   = "NODE_PWRITE"
NODE_MEMORY   = "NODE_MEMORY"
SYSTEM_READ   = "SYSTEM_READ"
SYSTEM_WRITE  = "SYSTEM_WRITE"
SYSTEM_MEMORY = "SYSTEM_MEMORY"
NODE          = "NODE"
DIMMS         = "DIMMS"
RUNTIME       = "RUNTIME"
SCALE         = "SCALE"


DRAM_CLOCKS   = "DRAM_CLOCKS"
SR_CYCLES     = "SR_CYCLES"
PPD_CYCLES    = "PPD_CYCLES"
PS_CYCLES     = "PS_CYCLES"

# background power
SR_WATTS      = 0.92
PPD_WATTS     = 2.79
PS_WATTS      = 4.66

# operation power
RD_JOULES     = 0.000000056
WR_JOULES     = 0.000000061
OPS_PER_GB    = MB(16)
#RD_WATTS      = 0.939
#WR_WATTS      = 1.023

# pcm-memory.x
memValRE       = re.compile("\\|--------------------")
sockGroupRE    = re.compile("\\|--(\s+)Socket")
chanReadRE     = re.compile("\\|--(\s+)Mem Ch")
chanWriteRE    = re.compile("\\|--(\s+)Writes\(MB/s\):")
nodeReadRE     = re.compile("\\|--(\s+)NODE (\d+) Mem Read")
nodeWriteRE    = re.compile("\\|--(\s+)NODE (\d+) Mem Write")
nodePWriteRE   = re.compile("\\|--(\s+)NODE (\d+) P. Write")
nodeMemoryRE   = re.compile("\\|--(\s+)NODE (\d+) Memory")
systemReadRE   = re.compile("(\s+)\\|--(\s+)System Read")
systemWriteRE  = re.compile("(\s+)\\|--(\s+)System Write")
systemMemoryRE = re.compile("(\s+)\\|--(\s+)System Memory")
#memValRE       = re.compile("(\W+)--------------------")
#sockGroupRE    = re.compile("(\W+)--(\s+)Socket")
#chanReadRE     = re.compile("(\W*)--  Mem Ch")
#chanWriteRE    = re.compile("(\W*)--(\s+)Writes\(MB/s\):")
#nodeReadRE     = re.compile("(\W*)-- NODE(\d+) Mem Read")
#nodeWriteRE    = re.compile("(\W*)-- NODE(\d+) Mem Write")
#nodePWriteRE   = re.compile("(\W*)-- NODE(\d+) P. Write")
#nodeMemoryRE   = re.compile("(\W*)-- NODE(\d+) Memory")
#systemReadRE   = re.compile("(\W*)--(\s+)System Read")
#systemWriteRE  = re.compile("(\W*)--(\s+)System Write")
#systemMemoryRE = re.compile("(\W*)--(\s+)System Memory")

# pcm.x
skInfoRE       = re.compile("(\W+)SKT(\W+)(\d+)(\W+)(\d+)\.(\d+)")
skJoulesRE     = re.compile("(\W+)SKT(\W+)(\d+)(\W+)package")
dramJoulesRE   = re.compile("(\W+)SKT(\W+)(\d+)(\W+)DIMMs")

IPC         = "IPC"
L3_MISS     = "L3_MISS"
L2_MISS     = "L2_MISS"
L3_HIT      = "L3_HIT"
L2_HIT      = "L2_HIT"
L3_CLK      = "L3_CLK"
L2_CLK      = "L2_CLK"
READ        = "READ"
WRITE       = "WRITE"
DRAM_JOULES = "DRAM_JOULES"
SOCK_JOULES = "SOCK_JOULES"

UNKNOWN_ALLOC_METHOD = "unknown-alloc-method"

#############################################################################
# alloc point info with knapsacks
#############################################################################
VAL_ALIVE_OBJECTS_IDX = 0
VAL_ALIVE_SIZE_IDX    = 1
VAL_HOT_OBJECTS_IDX   = 2
VAL_HOT_SIZE_IDX      = 3
VAL_NEW_OBJECTS_IDX   = 4
VAL_NEW_SIZE_IDX      = 5
VAL_REFS_IDX          = 6

HKD_VAL_ALIVE_OBJECTS_IDX = 0
HKD_VAL_ALIVE_SIZE_IDX    = 1
HKD_VAL_HOT_OBJECTS_IDX   = 3
HKD_VAL_HOT_SIZE_IDX      = 4
HKD_VAL_NEW_OBJECTS_IDX   = 6
HKD_VAL_NEW_SIZE_IDX      = 7
HKD_VAL_REFS_IDX          = 2

FULL_OBJECTS_IDX = 0
FULL_SIZE_IDX    = 1
FULL_REFS_IDX    = 2

ALL_APS          = "ALL_APS"
KNAPSACK         = "KNAPSACK"
ALL_STATS        = "ALL_STATS"
KS_STATS         = "KS_STATS"
OUT_STATS        = "OUT_STATS"
RATIO            = "RATIO"
NAMES            = "NAMES"
VALS             = "VALS"
NVALS            = "NVALS"
FULL             = "FULL"
NMAX             = "NMAX"
TOTALS           = "TOTALS"
DIFFS            = "DIFFS"
OVER_TOTAL       = "OVER_TOTAL"
OVER_STATS       = "OVER_STATS"
OVER_RATIO       = "OVER_RATIO"
UNDER_TOTAL      = "UNDER_TOTAL"
UNDER_STATS      = "UNDER_STATS"
UNDER_RATIO      = "UNDER_RATIO"
IDEAL_TOTAL      = "IDEAL_TOTAL"
IDEAL_RATIO      = "IDEAL_RATIO"
KS_TOTAL         = "KS_TOTAL"
KS_RATIO         = "KS_RATIO"
OUT_RATIO        = "OUT_RATIO"
AP_STATS         = "AP_STATS"
ALL_VALS         = "ALL_VALS"
PER_VAL          = "PER_VAL"
AGG_VAL          = "AGG_VAL"
KS_ADJ_STATS     = "KS_ADJ_STATS"
KS_ADJ_RATIO     = "KS_ADJ_RATIO"
OUT_ADJ_STATS    = "OUT_ADJ_STATS"
OUT_ADJ_RATIO    = "OUT_ADJ_RATIO"

AGG_RATIO        = "AGG_RATIO"
AGG_RATIO_X      = "AGG_RATIO_X"
STD_RATIO        = "STD_RATIO"
MAX_RATIO        = "MAX_RATIO"
MIN_RATIO        = "MIN_RATIO"
SUM_RATIO        = "SUM_RATIO"
AGG_PER_VAL      = "AGG_PER_VAL"

OBJECTS_MISSES   = "OBJECTS_MISSES"
SIZE_MISSES      = "SIZE_MISSES"
REFS_MISSES      = "REFS_MISSES"

# knapsack styles
FULL_REFS = "FULL_REFS"
VAL_REFS  = "VAL_REFS"
FULL_SIZE = "FULL_SIZE"
VAL_SIZE  = "VAL_SIZE"

# knapsacks that we compute
STATIC_SAME_KS           = "STATIC_SAME_KS"
STATIC_DIFF_KS           = "STATIC_DIFF_KS"
IDEAL_KS                 = "IDEAL_KS"
ONLINE_KS_N1             = "ONLINE_KS_N1"
ONLINE_KS_N2             = "ONLINE_KS_N2"
ONLINE_KS_N4             = "ONLINE_KS_N4"
ONLINE_KS_N8             = "ONLINE_KS_N8"
ONLINE_KS_NMAX           = "ONLINE_KS_NMAX"
ONLINE_FAST_N1_PER_10    = "ONLINE_FAST_N1_PER_10"
ONLINE_FAST_N2_PER_10    = "ONLINE_FAST_N2_PER_10"
ONLINE_FAST_N4_PER_10    = "ONLINE_FAST_N4_PER_10"
ONLINE_FAST_N8_PER_10    = "ONLINE_FAST_N8_PER_10"
ONLINE_FAST_NMAX_PER_10  = "ONLINE_FAST_NMAX_PER_10"
ONLINE_FAST_N1_PER_50    = "ONLINE_FAST_N1_PER_50"
ONLINE_FAST_N2_PER_50    = "ONLINE_FAST_N2_PER_50"
ONLINE_FAST_N4_PER_50    = "ONLINE_FAST_N4_PER_50"
ONLINE_FAST_N8_PER_50    = "ONLINE_FAST_N8_PER_50"
ONLINE_FAST_NMAX_PER_50  = "ONLINE_FAST_NMAX_PER_50"
ONLINE_FAST_N1_PER_250   = "ONLINE_FAST_N1_PER_250"
ONLINE_FAST_N2_PER_250   = "ONLINE_FAST_N2_PER_250"
ONLINE_FAST_N4_PER_250   = "ONLINE_FAST_N4_PER_250"
ONLINE_FAST_N8_PER_250   = "ONLINE_FAST_N8_PER_250"
ONLINE_FAST_NMAX_PER_250 = "ONLINE_FAST_NMAX_PER_250"

# totals for a computed knapsack
IDEAL_KS_PPV          = "IDEAL_KS_PPV"
STATIC_SAME_KS_TOTALS = "STATIC_SAME_KS_TOTALS"
STATIC_DIFF_KS_TOTALS = "STATIC_DIFF_KS_TOTALS"

# knapsack totals predicted per value
STATIC_SAME_PPV          = "STATIC_SAME_PPV"
STATIC_DIFF_PPV          = "STATIC_DIFF_PPV"
ONLINE_KS_N1_PPV         = "ONLINE_KS_N1_PPV"
ONLINE_KS_N2_PPV         = "ONLINE_KS_N2_PPV"
ONLINE_KS_N4_PPV         = "ONLINE_KS_N4_PPV"
ONLINE_KS_N8_PPV         = "ONLINE_KS_N8_PPV"
ONLINE_KS_NMAX_PPV       = "ONLINE_KS_NMAX_PPV"
ONLINE_FAST_N1_10_PPV    = "ONLINE_FAST_N1_10_PPV"
ONLINE_FAST_N2_10_PPV    = "ONLINE_FAST_N2_10_PPV"
ONLINE_FAST_N4_10_PPV    = "ONLINE_FAST_N4_10_PPV"
ONLINE_FAST_N8_10_PPV    = "ONLINE_FAST_N8_10_PPV"
ONLINE_FAST_NMAX_10_PPV  = "ONLINE_FAST_NMAX_10_PPV"
ONLINE_FAST_N1_50_PPV    = "ONLINE_FAST_N1_50_PPV"
ONLINE_FAST_N2_50_PPV    = "ONLINE_FAST_N2_50_PPV"
ONLINE_FAST_N4_50_PPV    = "ONLINE_FAST_N4_50_PPV"
ONLINE_FAST_N8_50_PPV    = "ONLINE_FAST_N8_50_PPV"
ONLINE_FAST_NMAX_50_PPV  = "ONLINE_FAST_NMAX_50_PPV"
ONLINE_FAST_N1_250_PPV   = "ONLINE_FAST_N1_250_PPV"
ONLINE_FAST_N2_250_PPV   = "ONLINE_FAST_N2_250_PPV"
ONLINE_FAST_N4_250_PPV   = "ONLINE_FAST_N4_250_PPV"
ONLINE_FAST_N8_250_PPV   = "ONLINE_FAST_N8_250_PPV"
ONLINE_FAST_NMAX_250_PPV = "ONLINE_FAST_NMAX_250_PPV"

STATIC_DIFF         = "STATIC_DIFF"
REACTIVE_DIFF       = "REACTIVE_DIFF"

PER_10              = "PER_10"
PER_50              = "PER_50"
PER_250             = "PER_250"

# aggregate values
AGG_STATIC_SAME_PPV      = "AGG_STATIC_SAME_PPV"
AGG_STATIC_DIFF_PPV      = "AGG_STATIC_DIFF_PPV"
AGG_GUIDE_REFS_POST_PPV  = "AGG_GUIDE_REFS_POST_PPV"
AGG_IDEAL_KS_PPV         = "AGG_IDEAL_KS_PPV"
AGG_ONLINE_KS_N1_PPV     = "AGG_ONLINE_KS_N1_PPV"
AGG_ONLINE_KS_N2_PPV     = "AGG_ONLINE_KS_N2_PPV"
AGG_ONLINE_KS_N4_PPV     = "AGG_ONLINE_KS_N4_PPV"
AGG_ONLINE_KS_N8_PPV     = "AGG_ONLINE_KS_N8_PPV"
AGG_ONLINE_KS_NMAX_PPV   = "AGG_ONLINE_KS_NMAX_PPV"

AGG_ONLINE_FAST_N1_10_PPV     = "AGG_ONLINE_FAST_N1_10_PPV"
AGG_ONLINE_FAST_N2_10_PPV     = "AGG_ONLINE_FAST_N2_10_PPV"
AGG_ONLINE_FAST_N4_10_PPV     = "AGG_ONLINE_FAST_N4_10_PPV"
AGG_ONLINE_FAST_N8_10_PPV     = "AGG_ONLINE_FAST_N8_10_PPV"
AGG_ONLINE_FAST_NMAX_10_PPV   = "AGG_ONLINE_FAST_NMAX_10_PPV"
AGG_ONLINE_FAST_N1_50_PPV     = "AGG_ONLINE_FAST_N1_50_PPV"
AGG_ONLINE_FAST_N2_50_PPV     = "AGG_ONLINE_FAST_N2_50_PPV"
AGG_ONLINE_FAST_N4_50_PPV     = "AGG_ONLINE_FAST_N4_50_PPV"
AGG_ONLINE_FAST_N8_50_PPV     = "AGG_ONLINE_FAST_N8_50_PPV"
AGG_ONLINE_FAST_NMAX_50_PPV   = "AGG_ONLINE_FAST_NMAX_50_PPV"
AGG_ONLINE_FAST_N1_250_PPV    = "AGG_ONLINE_FAST_N1_250_PPV"
AGG_ONLINE_FAST_N2_250_PPV    = "AGG_ONLINE_FAST_N2_250_PPV"
AGG_ONLINE_FAST_N4_250_PPV    = "AGG_ONLINE_FAST_N4_250_PPV"
AGG_ONLINE_FAST_N8_250_PPV    = "AGG_ONLINE_FAST_N8_250_PPV"
AGG_ONLINE_FAST_NMAX_250_PPV  = "AGG_ONLINE_FAST_NMAX_250_PPV"

AGG_STATIC_DIFF          = "AGG_STATIC_DIFF"
AGG_REACTIVE_DIFF        = "AGG_REACTIVE_DIFF"

ksstrats = [
  (AGG_IDEAL_KS_PPV,            IDEAL_KS_PPV,            "ideal"),
  (AGG_STATIC_SAME_PPV,         STATIC_SAME_PPV,         "static-same"),
  (AGG_STATIC_DIFF_PPV,         STATIC_DIFF_PPV,         "static-diff"),
  (AGG_ONLINE_KS_N1_PPV,        ONLINE_KS_N1_PPV,        "online-ks-1"),
  (AGG_ONLINE_KS_N2_PPV,        ONLINE_KS_N2_PPV,        "online-ks-2"),
  (AGG_ONLINE_KS_N4_PPV,        ONLINE_KS_N4_PPV,        "online-ks-4"),
  (AGG_ONLINE_KS_N8_PPV,        ONLINE_KS_N8_PPV,        "online-ks-8"),
  (AGG_ONLINE_KS_NMAX_PPV,      ONLINE_KS_NMAX_PPV,      "online-ks-max"),
  (AGG_ONLINE_FAST_N1_10_PPV,   ONLINE_FAST_N1_10_PPV,   "online-fast-1-10"),
  (AGG_ONLINE_FAST_N2_10_PPV,   ONLINE_FAST_N2_10_PPV,   "online-fast-2-10"),
  (AGG_ONLINE_FAST_N4_10_PPV,   ONLINE_FAST_N4_10_PPV,   "online-fast-4-10"),
  (AGG_ONLINE_FAST_N8_10_PPV,   ONLINE_FAST_N8_10_PPV,   "online-fast-8-10"),
  (AGG_ONLINE_FAST_NMAX_10_PPV, ONLINE_FAST_NMAX_10_PPV, "online-fast-max-10"),
#  (AGG_ONLINE_FAST_N1_50_PPV,   ONLINE_FAST_N1_50_PPV,   "online-fast-1-50"),
#  (AGG_ONLINE_FAST_N2_50_PPV,   ONLINE_FAST_N2_50_PPV,   "online-fast-2-50"),
#  (AGG_ONLINE_FAST_N4_50_PPV,   ONLINE_FAST_N4_50_PPV,   "online-fast-4-50"),
#  (AGG_ONLINE_FAST_N8_50_PPV,   ONLINE_FAST_N8_50_PPV,   "online-fast-8-50"),
#  (AGG_ONLINE_FAST_NMAX_50_PPV, ONLINE_FAST_NMAX_50_PPV, "online-fast-max-50"),
#  (AGG_ONLINE_FAST_N1_250_PPV,  ONLINE_FAST_N1_250_PPV,  "online-fast-1-250"),
#  (AGG_ONLINE_FAST_N2_250_PPV,  ONLINE_FAST_N2_250_PPV,  "online-fast-2-250"),
#  (AGG_ONLINE_FAST_N4_250_PPV,  ONLINE_FAST_N4_250_PPV,  "online-fast-4-250"),
#  (AGG_ONLINE_FAST_N8_250_PPV,  ONLINE_FAST_N8_250_PPV,  "online-fast-8-250"),
#  (AGG_ONLINE_FAST_NMAX_250_PPV,ONLINE_FAST_NMAX_250_PPV,"online-fast-max-250")
]

defkstypes = [
  (AGG_STATIC_SAME_PPV,       STATIC_SAME_PPV),
  (AGG_STATIC_DIFF_PPV,       STATIC_DIFF_PPV),
  (AGG_ONLINE_KS_N1_PPV,      ONLINE_KS_N1_PPV),
  (AGG_ONLINE_FAST_N1_10_PPV, ONLINE_FAST_N1_PER_10),
]

AP_MAP       = "AP_MAP"
HOT_METHODS  = "HOT_METHODS"
HOT_KLASSES  = "HOT_KLASSES"
KALS         = "KALS"
ID_KLASS_MAP = "ID_KLASS_MAP"
KLASS_ID_MAP = "KLASS_ID_MAP"
KLASS_MAP    = "KLASS_MAP"
KLASS_AP_MAP = "KLASS_AP_MAP"
NAME         = "NAME"

AP_STYLES = [
  STATIC_SAME_PPV,
  STATIC_DIFF_PPV,
  IDEAL_KS_PPV,
  ONLINE_KS_N1_PPV,
  ONLINE_KS_N2_PPV,
  ONLINE_KS_N4_PPV,
  ONLINE_KS_N8_PPV,
  ONLINE_KS_NMAX_PPV
]

HK_STYLES = [
  ONLINE_FAST_N1_10_PPV,
  ONLINE_FAST_N2_10_PPV,
  ONLINE_FAST_N4_10_PPV,
  ONLINE_FAST_N8_10_PPV,
  ONLINE_FAST_NMAX_10_PPV,
  ONLINE_FAST_N1_50_PPV,
  ONLINE_FAST_N2_50_PPV,
  ONLINE_FAST_N4_50_PPV,
  ONLINE_FAST_N8_50_PPV,
  ONLINE_FAST_NMAX_50_PPV,
  ONLINE_FAST_N1_250_PPV,
  ONLINE_FAST_N2_250_PPV,
  ONLINE_FAST_N4_250_PPV,
  ONLINE_FAST_N8_250_PPV,
  ONLINE_FAST_NMAX_250_PPV
]

#############################################################################
# ahowars study on tiered compilation
#############################################################################
C1_COMPILE = "c1_compile"
C2_COMPILE = "c2_compile"

APP_THREAD_TIME     = "APP_THREAD_TIME"
COMPILE_THREAD_TIME = "COMPILE_THREAD_TIME"

corcfgs = {}
corcfgs[STEADY_CCF]        = XINT_CCF
corcfgs[STEADY_CCF_REPLAY] = XINT_CCF
corcfgs[REPLAY_NO_RESET]   = XINT_CCF
corcfgs[CCF_ORACLE_DIFF]   = XINT_CCF

TYPE  = "TYPE"
ITERS = "ITERS"

VMThread       = "VMThread"
ConcGCThread   = "ConcGCThread"
ParGCThread    = "ParGCThread"
JavaThread     = "JavaThread"
CompilerThread = "CompilerThread"
WatcherThread  = "WatcherThread"
UnknownThread  = "UnknownThread"

startiters  = [2]
steadyiters = [3, 4, 5, 6, 7]

