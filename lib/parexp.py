#############################################################################
# functions to build commands and run parallel style experiments
#############################################################################
from lib.globals import *
from lib.utils import *
from lib.parreport import printEmonMetricDataPar, printCASCountsPar, ppemetsPar
import time

def oprofEnabledPar(cfg, runs=[]):
  profcfgs = [ pcfg[PC_PROFCFG] for r,pcfg in enumerate(parcfgs[cfg]) \
               if r in runs ]
  oprofs   = [ FULL_OPROF, EMON_OPROF, POWER_GOV_OPROF ]
  return (any(x in profcfgs for x in oprofs))

def hprofEnabledPar(cfg, runs=[]):
  profcfgs = [ pcfg[PC_PROFCFG] for r,pcfg in enumerate(parcfgs[cfg]) \
               if r in runs ]
  hprofs   = [ FULL_HPROF, EMON_HPROF, POWER_GOV_HPROF ]
  return (any(x in profcfgs for x in hprofs))

def profilingEnabledPar(cfg, runs=[]):
  return (oprofEnabledPar(cfg, runs=runs) or hprofEnabledPar(cfg, runs=runs))

def oemonEnabledPar(cfg, runs=[]):
  profcfgs = [ pcfg[PC_PROFCFG] for r,pcfg in enumerate(parcfgs[cfg]) \
               if r in runs ]
  oprofs   = [ FULL_OPROF, EMON_OPROF ]
  return (any(x in profcfgs for x in oprofs))

def hemonEnabledPar(cfg, runs=[]):
  profcfgs = [ pcfg[PC_PROFCFG] for r,pcfg in enumerate(parcfgs[cfg]) \
               if r in runs ]
  hprofs   = [ FULL_HPROF, EMON_HPROF ]
  return (any(x in profcfgs for x in hprofs))

def emonEnabledPar(cfg, runs=[]):
  return (oemonEnabledPar(cfg, runs=runs) or hemonEnabledPar(cfg, runs=runs))

def opgovEnabledPar(cfg, runs=[]):
  profcfgs = [ pcfg[PC_PROFCFG] for r,pcfg in enumerate(parcfgs[cfg])
               if r in runs ]
  oprofs   = [ FULL_OPROF, POWER_GOV_OPROF ]
  return (any(x in profcfgs for x in oprofs))

def hpgovEnabledPar(cfg, runs=[]):
  profcfgs = [ pcfg[PC_PROFCFG] for r,pcfg in enumerate(parcfgs[cfg])
               if r in runs ]
  hprofs   = [ FULL_HPROF, POWER_GOV_HPROF ]
  return (any(x in profcfgs for x in hprofs))

def pgovEnabledPar(cfg, runs=[]):
  return (opgovEnabledPar(cfg, runs=runs) or hpgovEnabledPar(cfg, runs=runs))

def startEmonPar(cfg, iter, run, verbose=V1):

  edashvcmd = emoncmd + " -v"
  execmd(edashvcmd, emonDashvFilePar(cfg, iter, run), verbose=verbose)

  edashMcmd = emoncmd + " -M"
  execmd(edashMcmd, emonDashMFilePar(cfg, iter, run), verbose=verbose)

  start_time = time.time()
  ecmd   = emoncmd + " -i %s" % emonEventsFilePar(cfg, iter, run)
  execmd(ecmd, emonRawFilePar(cfg, iter, run), wait=False, verbose=verbose)

  edashvf = open(emonDashvFilePar(cfg, iter, run), 'a')
  edashvf.write("emon started: %ld\n" % long((start_time * 1000.0)))
  edashvf.close()
  return

def stopEmonPar(verbose=V1):

  execmd(killemoncmd, wait=False, verbose=verbose)
  return

def startPGovPar(cfg, iter, run, pgovrate=defpgrate, verbose=V1):

  pgovcmd = powergovcmd + (" -e %d" % pgovrate)
  execmd(pgovcmd, pgovRawFilePar(cfg, iter, run), wait=False, verbose=verbose)
  return

def stopPGovPar(verbose=V1):

  execmd(killpgovcmd, verbose=verbose)
  return

def getNumactlOpts(cfg, run):
  bench, runcfg, profcfg = parcfgs[cfg][run]

  opts = []
  if numaopts.has_key(runcfg):
    opts = numaopts[runcfg]
  return " ".join(opts)

def getMcolorctlOpts(cfg, run):
  bench, runcfg, profcfg = parcfgs[cfg][run]

  opts = []
  if mcoloropts.has_key(runcfg):
    opts = mcoloropts[runcfg]
  return " ".join(opts)

def getJavaOptsPartPar(cfg, run, xjopts=[]):
  bench, runcfg, profcfg = parcfgs[cfg][run]

  jopts = []
  if javaRunOpts.has_key(runcfg):
    jopts += javaRunOpts[runcfg]

  if javaProfOpts.has_key(runcfg):
    jopts += javaProfOpts[runcfg]

  return " ".join(jopts + xjopts)

def getJvm2008HoptsPar(cfg, iter, run, pgovrate=defpgrate):
  bench, runcfg, profcfg = parcfgs[cfg][run]

  hopts = []
  hopts.append(bench)
  hopts.append(" ".join([IgnoreKitValidation]))

  if jvm2008RunOpts.has_key(runcfg):
    hopts.append(" ".join(jvm2008RunOpts[runcfg]))

  if jvm2008ProfOpts.has_key(profcfg):
    hopts.append(" ".join(jvm2008ProfOpts[profcfg]))

  if hprofEnabledPar(cfg, runs=[run]):
    rdatadir = getdir(rawDataDirPar(cfg, iter, run))
    hopts.append(specjvmOpt(specjvmRawDataDir, rdatadir.rstrip('/')))

  if hemonEnabledPar(cfg, runs=[run]):
    hopts.append(specjvmOpt(specjvmEmonEventsFile, \
                 emonEventsFilePar(cfg, iter, run)))

  if hpgovEnabledPar(cfg, runs=[run]):
    hopts.append(specjvmOpt(specjvmPowerGovFreq, ("%d" % pgovrate)))

  return " ".join(hopts)

def getDacapoHoptsPar(cfg, iter, run):
  bench, runcfg, profcfg = parcfgs[cfg][run]

  hopts = []
  hopts.append("%s -s %s" % tuple(bench.split("-")))

  if dacapoRunOpts.has_key(runcfg):
    hopts.append(" ".join(dacapoRunOpts[runcfg]))

  if dacapoProfOpts.has_key(profcfg):
    hopts.append(" ".join(dacapoProfOpts[profcfg]))

  hopts.append(scratchdiropt(scratchdirpar(cfg, iter, run)))
  return " ".join(hopts)

def getHarnessPartPar(cfg, iter, run, pgovrate=defpgrate):
  bench = parcfgs[cfg][run][0]

  hparts = []
  if bench in jvm2008s:
    hparts.append(specjvmOpt(specjvmHomeDir, jvm2008sdir))
    hparts.append( ("-jar %s" % jvm2008jar) )
    hparts.append(specjvmOpt(specjvmResDir, jvm2008results))
    hparts.append(specjvmOptFalse(specjvmRunInitialCheck))
    hparts.append( ("%s" % getJvm2008HoptsPar(cfg, iter, run,\
                                              pgovrate=pgovrate)) )

  elif bench in dacapos:
    hparts.append("-jar %s" % dacapojar)
    hparts.append("%s" % getDacapoHoptsPar(cfg, iter, run))

  else:
    print "getHarnessPart: invalid bench: %s" % bench
    raise SystemExit(1)

  return " ".join(hparts)

def javacmdpar(cfg, iter, run, clcfg=None, xjopts=[], pgovrate=defpgrate):

  needCfgDel = False
  if not parcfgs.has_key(cfg):
    if clcfg == None:
      print "you must specify clcfg for unknown parcfg: %s" % cfg
      raise SystemExit(1)
    else:
      needCfgDel = True
      parcfgs[cfg]  = clcfg

  cmdpts = []
  if numaopts.has_key(parcfgs[cfg][run][PC_RUNCFG]):
    cmdpts.append(numactlcmd + " " + getNumactlOpts(cfg, run))
  else:
    cmdpts.append(numactlcmd + " " + " ".join(NUMACFG_NODE1_PREFER))
  if mcoloropts.has_key(parcfgs[cfg][run][PC_RUNCFG]):
    cmdpts.append(mcolorctlcmd + " " + getMcolorctlOpts(cfg, run))
  cmdpts.append(ohotspot)
  cmdpts.append(getJavaOptsPartPar(cfg, run, xjopts=xjopts))
  cmdpts.append(getHarnessPartPar(cfg, iter, run, pgovrate=pgovrate))

  if needCfgDel:
    del parcfgs[cfg]

  return " ".join(cmdpts)

def writeEmonEventsFilePar(cfg, iter, run, emonevts=[], emonrate=deferate, \
                           emonloops=defeloops, verbose=V1):

  try:
    emonf = open(emonEventsFilePar(cfg, iter, run), 'w')
  except:
    print "error opening emon events file: %s" % \
          emonEventsFilePar(cfg, iter, run)
    raise SystemExit(1)

  evtstr  = ""
  evtstr += "-u -t%2.4f -l%d -C (\n" % (emonrate, emonloops)
  for eev in emonevts:
    evtstr += "%s,\n" % eev
  evtstr += ")\n"

  emonf.write(evtstr)
  emonf.close()

def writeEmonEventsPar(cfg, iter, run, emonevf=None, emonevts=[], \
                       emonrate=deferate, emonloops=defeloops, verbose=V1):
  if emonevf:
    if emonevts:
      print "warning: ignoring emonevts and using events file: %s" % emonevf
    try:
      execmd("cp %s %s" % (emonevf, emonEventsFilePar(cfg, iter, run)), \
             verbose=verbose)
    except:
      print "error copying: %s to %s" % \
            (emonevf, emonEventsFilePar(cfg, iter, run))
      raise SystemExit(1)
  else:
    writeEmonEventsFilePar(cfg, iter, run, emonevts=emonevts, \
                           emonrate=emonrate, emonloops=emonloops, \
                           verbose=verbose)


def parrun(cfg, iter, clcfg=None, xjopts=[], emonevf=None, emonevts=[], \
           emonrate=deferate, emonloops=defeloops, pgovrate=defpgrate, \
           ppemets=[], verbose=V1):

  needCfgDel   = False
  needStopEmon = False
  needStopPGov = False
  emonStarted  = False
  rets         = []
  procs        = []

  if not parcfgs.has_key(cfg):
    if clcfg == None:
      print "you must specify clcfg for unknown parcfg: %s" % cfg
      raise SystemExit(1)
    else:
      needCfgDel   = True
      parcfgs[cfg] = clcfg

  runs = range(len(parcfgs[cfg]))

  # error out if emon not installed
  if emonEnabledPar(cfg, runs=runs) and not os.path.exists("/dev/emon"):
    print "Error: /dev/emon not present. Install emon."
    return

  if oprofEnabledPar(cfg, runs=runs):
    getdir(rawDataDirPar(cfg, iter, 0), create=True)

  # start outside emon
  if oemonEnabledPar(cfg, runs=runs):
    try:
      writeEmonEventsPar(cfg, iter, 0, emonevf=emonevf, emonevts=emonevts, \
                         emonrate=emonrate, emonloops=emonloops, \
                         verbose=verbose)
      emonStarted = needStopEmon = True
      startEmonPar(cfg, iter, 0, verbose=verbose)
    except:
      print "error starting outside emon for (%s-i%d)!" \
            % (cfg, iter)
      raise SystemExit(1)

  # start outside power_gov
  if opgovEnabledPar(cfg, runs=runs):
    try:
      pgovStarted = needStopPGov = True
      startPGovPar(cfg, iter, 0, pgovrate=pgovrate, verbose=verbose)
    except:
      print "error starting outside power_gov for (%s-i%d)!" \
            % (cfg, iter)
      raise SystemExit(1)

  for run in runs:
    getdir(pexpdir(cfg, iter, run), create=True)
    if profilingEnabledPar(cfg, runs=[run]):
      getdir(rawDataDirPar(cfg, iter, run), create=True)

    # create the emon events file
    if hemonEnabledPar(cfg, runs=[run]):
      if emonStarted:
        print "warning: %s-i%d-r%d tried to start emon" % (cfg,iter,run), \
              " ... emon is already running!"
      else:
        writeEmonEvents(cfg, iter, run, emonevf=emonevf, emonevts=emonevts, \
                        emonrate=emonrate, emonloops=emonloops, verbose=verbose)
        emonStarted = True

    cmd     = javacmdpar(cfg, iter, run, xjopts=xjopts, pgovrate=pgovrate)
    outfile = rawoutfilepar(cfg, iter, run)

    # these commands are pretty long and complicated - print them out
    cmdoutf = open(cmdoutfilepar(cfg, iter, run),'w')
    print >> cmdoutf, "%s > %s" % (cmd, outfile)
    cmdoutf.close()

    procs.append(execmd(cmd, outfile, wait=False, verbose=verbose))

  for proc in procs:
    rets.append(proc.wait())

  try:
    if needStopEmon:
      stopEmonPar(verbose=verbose)
  except:
    print "error stopping emon for (%s-i%d)!" % (cfg, iter)
    raise SystemExit(1)

  try:
    if needStopPGov:
      stopPGovPar(verbose=verbose)
  except:
    print "error stopping power_gov for (%s-i%d)!" % (cfg, iter)
    raise SystemExit(1)

  if needCfgDel:
    del parcfgs[cfg]

  if ppemets and profilingEnabledPar(cfg, runs=[0]):
    ppemetsPar(cfg, iter, 0, ppemets)

  return rets

def parexp(cfg, clcfg=None, iters=defiters, xjopts=[], miniters=defminiters, \
           emonevf=None, emonevts=[], emonrate=deferate, emonloops=defeloops, \
           pgovrate=defpgrate, ppemets=[], verbose=V1):

  if verbose > V0:
    print "parexp: %s" % (cfg)

  nfailed  = 0
  miniters = min(iters, miniters)

  for iter in range(iters):

    if verbose > V0:
      print "".ljust(2), ("i%d" % iter).ljust(4), "--> ",

    returncodes = parrun(cfg, iter, clcfg=clcfg, xjopts=xjopts,
                         emonevf=emonevf, emonevts=emonevts, emonrate=emonrate, \
                         emonloops=emonloops, pgovrate=pgovrate, \
                         ppemets=ppemets, verbose=verbose)

    if verbose > V0:
      if any([True if not x==0 else False for x in returncodes]):
        print "returned error codes ", returncodes
        nfailed += 1
      else:
        result = getHarnessTimePar(cfg, iter)
        if type(result) is float:
          print "%2.4f" % result
        else:
          print "returned bad result: %s" % result
          nfailed += 1

    if nfailed > (iters - miniters):
      break

  if verbose > V0:
    print "".ljust(2), "avg".ljust(4), "--> ",
    htstats1 = getHarnessTimeStatsPar(cfg, iters=iters, miniters=1)
    if htstats1 is None:
      print "no valid runs"
    else:
      htstats = getHarnessTimeStatsPar(cfg, iters=iters, miniters=miniters)
      if htstats is None:
        print "%2.4f (of %d iterations)" % \
              (htstats1[MEAN], len(getHarnessTimesPar(cfg,iters)))
      else:
        print "%2.4f" % htstats[MEAN]

def parexps(cfgs=defpcfgs, clcfgs=[None for x in defpcfgs], iters=defiters, \
            xjopts=[], emonevf=None, emonevts=[], emonrate=deferate, \
            emonloops=defeloops, pgovrate=defpgrate, ppemets=[], verbose=V1):

  for cfg,clcfg in zip(cfgs,clcfgs):
    parexp(cfg, clcfg=clcfg, iters=iters, xjopts=xjopts, emonevf=emonevf, \
           emonevts=emonevts, emonrate=emonrate, emonloops=emonloops, \
           pgovrate=pgovrate, ppemets=ppemets, verbose=verbose)

