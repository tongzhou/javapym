#############################################################################
# functions for generating parreports
#############################################################################
from lib.globals import *
from lib.utils import *
from lib.report import ppemetric, getedd, getecc
from lib.parreport import *

def getResultsPar(cfgs, metric=HARNESS_TIME, pkg=1, iters=defiters,
                  miniters=defminiters):

  results = {}
  for cfg in cfgs:

    results[cfg] = {}
    if metric in edprocMetrics:
      results[cfg] = getEmonMetricStatsPar(cfg, metric, iters=iters, \
                     miniters=miniters)

    elif metric in pgovMetrics:
      results[cfg] = getPGovMetricStatsPar(cfg, metric, pkg=pkg, \
                     iters=iters, miniters=miniters)
    
    elif metric == HARNESS_TIME:
      results[cfg] = getHarnessTimeStatsPar(cfg, iters=iters, \
                    miniters=miniters)

  return results

def getReportStrsPar(cfgs, basecfg, metric=HARNESS_TIME, absolute=False, \
                     stat=MEAN, pkg=1, iters=defiters, miniters=defminiters):

  results = getResultsPar(cfgs=cfgs, metric=metric, pkg=pkg, iters=iters, \
                          miniters=miniters)

  rstrs = {}
  for cfg in cfgs:

    # get the baseval
    try:
      baseval = results[basecfg][stat]
    except:
      baseval = None

    # and the expval
    try:
      expval = results[cfg][stat]
    except:
      expval = None

    # compute the relative value and if its valid
    if not absolute:
      if expval and baseval:
        myval = (expval / baseval)
      else:
        myval = None
    else:
      if expval:
        myval = expval
      else:
        myval = None
    
    # store valid values into an average list
    if not rstrs.has_key(GEOMEAN):
      rstrs[GEOMEAN] = []
    if myval:
      rstrs[GEOMEAN].append(myval)

    if myval:
      rstrs[cfg] = "%2.4f" % myval
    else:
      rstrs[cfg] = "N/A"

  rstrs[GEOMEAN] = ("%2.4f" % geomean(rstrs[GEOMEAN]))

  return rstrs

def parreport(cfgs, basecfg, metric=HARNESS_TIME, pkg=1, stat=MEAN, \
              absolute=False, iters=defiters, miniters=defminiters):

  # hack to remove old bg procs:
  if bgprocs:
    clearBGProcs()
  
  rstrs = getReportStrsPar(cfgs=cfgs, basecfg=basecfg, metric=metric, \
                           pkg=pkg, stat=stat, absolute=absolute, \
                           iters=iters, miniters=miniters)

  print "Report for metric: %s\n" % metric

  cfgs = [basecfg] + [ x for x in cfgs if not x == basecfg ]
  print "".ljust(30),
  for cfg in cfgs:
    print ("%s" % cfg).ljust(10),
  print ""

  cfgs = cfgs + [GEOMEAN]
  for cfg in cfgs:
    print ("%s" % cfg).ljust(30),
    print ("%s" % rstrs[cfg]).ljust(10),
    print ""

def getEmonDataPar(cfg, iter, run, metrics=[], timeshift=defts):

  rawfile    = emonRawFilePar(cfg, iter, run)
  edashvfile = emonDashvFilePar(cfg, iter, run)
  return getedd(rawfile, edashvfile, metrics=metrics, timeshift=timeshift)

def printEmonMetricDataPar(cfg, iter, run, metric, pkgs=[0,1], \
                           timeshift=defts, outf=sys.stdout):

  edd = getEmonDataPar(cfg, iter, run, metrics=[metric], timeshift=timeshift)

  bench, jcfg, hcfg = parcfgs[cfg][run]
  if not edd:
    print >> outf, "no emon data for exp: (i%d, %s-%s-r%d), metric = %s" % \
          (iter, bench, jcfg, run, metric)
    return

  print >> outf, "Emon metric data for exp: (i%d, %s-%s-r%d), metric = %s" % \
          (iter, bench, jcfg, run, metric)

  ppemetric(edd, metric, pkgs=pkgs, outf=outf)

def printEmonDataPar(cfg, iter, run, metrics=[], pkgs=[0,1], timeshift=defts, \
                     outf=sys.stdout):

  if len(metrics) == 1:
    printEmonMetricDataPar(cfg, iter, run, metrics[0], pkgs=pkgs, \
                           timeshift=timeshift, outf=outf)
  else:
    printEmonMetricsDataPar(cfg, iter, run, metrics[0], pkgs=pkgs, \
                            timeshift=timeshift, outf=outf)

def getCASCountsPar(cfg, iter, run, timeshift=defts):

  rawfile    = emonRawFilePar(cfg, iter, run)
  edashvfile = emonDashvFilePar(cfg, iter, run)
  return getecc(rawfile, edashvfile, timeshift=timeshift)

def printCASCountsPar(cfg, iter, run, metric, pkgs=[0,1], timeshift=defts,
                      outf=sys.stdout):

  ecc = getCASCountsPar(cfg, iter, run, timeshift=timeshift)

  bench, jcfg, hcfg = parcfgs[cfg][run]
  if not ecc:
    print >> outf, "no cas counts for exp: (i%d, %s-%s-r%d), metric=%s" % \
          (iter, bench, jcfg, run, metric)
    return

  print >> outf, "CAS counts for exp: (i%d, %s-%s-r%d), metric=%s" % \
          (iter, bench, jcfg, run, metric)

  ppemetric(ecc, metric, pkgs=pkgs, outf=outf)

def ppemetsPar(cfg, iter, run, ppemets):
  for metric in ppemets:
    outf = open(ppEmonMetricFilePar(cfg, iter, run, metric), 'w')
    if getcpu() == ecuador:
      printCASCountsPar(cfg, iter, run, metric, outf=outf)
    else:
      printEmonMetricDataPar(cfg, iter, run, metric, outf=outf)

