#!/usr/bin/env python

from distutils.core import setup

setup(
  name       = "pyzephyr",
  version    = "0.1",
  py_modules = [ 'lib.globals',
                 'lib.utils',
                 'lib.exp', 
                 'lib.parexp',
                 'lib.report',
                 'lib.parreport',
               ],
  scripts    = [ 'bin/edproc.py',
                 'bin/getrss.py',
                 'bin/free_pages.py',
                 'bin/mem_free.py',
                 'bin/sort_objalloc.py',
                 'bin/build_refmap.py',
                 'bin/cluster_objects.py',
                 'bin/draw_heatmap.py',
                 'bin/klass_cluster_info.py',
                 'bin/dynamic_hotsets.py',
                 'bin/dynamic_coldsets.py',
                 #'bin/dynamic_heatmap.py',
               ],
)

